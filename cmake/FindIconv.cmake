################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
function(package_library_targets libraries package_libdir deps out_libraries out_libraries_target build_type package_name)
    unset(_ACTUAL_TARGETS CACHE)
    unset(_FOUND_SYSTEM_LIBS CACHE)
    foreach(_LIBRARY_NAME ${libraries})
        find_library(FOUND_LIBRARY NAMES ${_LIBRARY_NAME} PATHS ${package_libdir}
                     NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
        if(FOUND_LIBRARY)
            message(STATUS "Library ${_LIBRARY_NAME} found ${FOUND_LIBRARY}")
            list(APPEND _out_libraries ${FOUND_LIBRARY})
            if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
                # Create a micro-target for each lib/a found
                string(REGEX REPLACE "[^A-Za-z0-9.+_-]" "_" _LIBRARY_NAME ${_LIBRARY_NAME})
                set(_LIB_NAME _LIB::${package_name}_${_LIBRARY_NAME}${build_type})
                if(NOT TARGET ${_LIB_NAME})
                    # Create a micro-target for each lib/a found
                    add_library(${_LIB_NAME} UNKNOWN IMPORTED)
                    set_target_properties(${_LIB_NAME} PROPERTIES IMPORTED_LOCATION ${FOUND_LIBRARY})
                    set(_ACTUAL_TARGETS ${_ACTUAL_TARGETS} ${_LIB_NAME})
                else()
                    message(STATUS "Skipping already existing target: ${_LIB_NAME}")
                endif()
                list(APPEND _out_libraries_target ${_LIB_NAME})
            endif()
            message(STATUS "Found: ${FOUND_LIBRARY}")
        else()
            message(STATUS "Library ${_LIBRARY_NAME} not found in package, might be system one")
            list(APPEND _out_libraries_target ${_LIBRARY_NAME})
            list(APPEND _out_libraries ${_LIBRARY_NAME})
            set(_FOUND_SYSTEM_LIBS "${_FOUND_SYSTEM_LIBS};${_LIBRARY_NAME}")
        endif()
        unset(FOUND_LIBRARY CACHE)
    endforeach()

    if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
        # Add all dependencies to all targets
        string(REPLACE " " ";" deps_list "${deps}")
        foreach(_ACTUAL_TARGET ${_ACTUAL_TARGETS})
            set_property(TARGET ${_ACTUAL_TARGET} PROPERTY INTERFACE_LINK_LIBRARIES "${_FOUND_SYSTEM_LIBS};${deps_list}")
        endforeach()
    endif()

    set(${out_libraries} ${_out_libraries} PARENT_SCOPE)
    set(${out_libraries_target} ${_out_libraries_target} PARENT_SCOPE)
endfunction()


include(FindPackageHandleStandardArgs)

# Global approach
set(Iconv_FOUND 1)
set(Iconv_VERSION "1.17")

find_package_handle_standard_args(Iconv REQUIRED_VARS
                                  Iconv_VERSION VERSION_VAR Iconv_VERSION)
mark_as_advanced(Iconv_FOUND Iconv_VERSION)

set(libiconv_STATIC_NAMES
    libiconv.a
)

find_library(libiconv_STATIC_LIBRARY NAMES ${libiconv_STATIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(NOT libiconv_STATIC_LIBRARY)
    message(FATAL_ERROR "libiconv shared library not found")
endif()

get_filename_component(libiconv_STATIC_LIBRARY_DIR "${libiconv_STATIC_LIBRARY}" DIRECTORY)

set(Iconv_INCLUDE_DIRS "${libiconv_STATIC_LIBRARY_DIR}/../include")
set(Iconv_INCLUDE_DIR "${libiconv_STATIC_LIBRARY_DIR}/../include")
set(Iconv_INCLUDES "${libiconv_STATIC_LIBRARY_DIR}/../include")
set(Iconv_RES_DIRS )
set(Iconv_DEFINITIONS )
set(Iconv_LINKER_FLAGS_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(Iconv_COMPILE_DEFINITIONS )
set(Iconv_COMPILE_OPTIONS_LIST "" "")
set(Iconv_COMPILE_OPTIONS_C "")
set(Iconv_COMPILE_OPTIONS_CXX "")
set(Iconv_LIBRARIES_TARGETS "") # Will be filled later, if CMake 3
set(Iconv_LIBRARIES "") # Will be filled later
set(Iconv_LIBS "") # Same as Iconv_LIBRARIES
set(Iconv_SYSTEM_LIBS )
set(Iconv_FRAMEWORK_DIRS )
set(Iconv_FRAMEWORKS )
set(Iconv_FRAMEWORKS_FOUND "") # Will be filled later
set(Iconv_BUILD_MODULES_PATHS )

mark_as_advanced(Iconv_INCLUDE_DIRS
                 Iconv_INCLUDE_DIR
                 Iconv_INCLUDES
                 Iconv_DEFINITIONS
                 Iconv_LINKER_FLAGS_LIST
                 Iconv_COMPILE_DEFINITIONS
                 Iconv_COMPILE_OPTIONS_LIST
                 Iconv_LIBRARIES
                 Iconv_LIBS
                 Iconv_LIBRARIES_TARGETS)

# Find the real .lib/.a and add them to Iconv_LIBS and Iconv_LIBRARY_LIST
set(Iconv_LIBRARY_LIST iconv charset)
set(Iconv_LIB_DIRS "${libiconv_STATIC_LIBRARY_DIR}")

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_Iconv_DEPENDENCIES "${Iconv_FRAMEWORKS_FOUND} ${Iconv_SYSTEM_LIBS} ")

package_library_targets("${Iconv_LIBRARY_LIST}"  # libraries
                              "${Iconv_LIB_DIRS}"      # package_libdir
                              "${_Iconv_DEPENDENCIES}"  # deps
                              Iconv_LIBRARIES            # out_libraries
                              Iconv_LIBRARIES_TARGETS    # out_libraries_targets
                              ""                          # build_type
                              "Iconv")                                      # package_name

set(Iconv_LIBS ${Iconv_LIBRARIES})

foreach(_FRAMEWORK ${Iconv_FRAMEWORKS_FOUND})
    list(APPEND Iconv_LIBRARIES_TARGETS ${_FRAMEWORK})
    list(APPEND Iconv_LIBRARIES ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${Iconv_SYSTEM_LIBS})
    list(APPEND Iconv_LIBRARIES_TARGETS ${_SYSTEM_LIB})
    list(APPEND Iconv_LIBRARIES ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(Iconv_LIBRARIES_TARGETS "${Iconv_LIBRARIES_TARGETS};")
set(Iconv_LIBRARIES "${Iconv_LIBRARIES};")

set(CMAKE_MODULE_PATH "${libiconv_STATIC_LIBRARY_DIR}/../" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "${libiconv_STATIC_LIBRARY_DIR}/../" ${CMAKE_PREFIX_PATH})

if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
    # Target approach
    if(NOT TARGET Iconv::Iconv)
        add_library(Iconv::Iconv INTERFACE IMPORTED)
        if(Iconv_INCLUDE_DIRS)
            set_target_properties(Iconv::Iconv PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
                                  "${Iconv_INCLUDE_DIRS}")
        endif()
        set_property(TARGET Iconv::Iconv PROPERTY INTERFACE_LINK_LIBRARIES
                     "${Iconv_LIBRARIES_TARGETS};${Iconv_LINKER_FLAGS_LIST}")
        set_property(TARGET Iconv::Iconv PROPERTY INTERFACE_COMPILE_DEFINITIONS
                     ${Iconv_COMPILE_DEFINITIONS})
        set_property(TARGET Iconv::Iconv PROPERTY INTERFACE_COMPILE_OPTIONS
                     "${Iconv_COMPILE_OPTIONS_LIST}")

    endif()
endif()

foreach(_BUILD_MODULE_PATH ${Iconv_BUILD_MODULES_PATHS})
    include(${_BUILD_MODULE_PATH})
endforeach()
