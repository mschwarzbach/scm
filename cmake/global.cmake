################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2021 ITK Engineering GmbH
#               2020 Uwe Woessner
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Global cmake file for the build process
#

option(ONLY_DOC "Option to only build the documentation without the actual project" OFF)
option(WITH_TESTS "Build unit tests" ON)
option(WITH_COVERAGE "Generate test coverage report using gcov (needs fastcov)" OFF)

option(WITH_DOC "Build documentation" ON)
option(WITH_API_DOC "Build API documentation (takes pretty long)" OFF)

option(WITH_EXTENDED_OSI "Assume an extended version of OSI is available" ON)
option(WITH_PROTOBUF_ARENA "Make use of protobuf arena allocation" ON)

option(WITH_DEBUG_POSTFIX "Use 'd' binary postfix on Windows platform" ON)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# NOTE: undocumented feature of cmake
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)


if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
  set(CMAKE_C_STANDARD_REQUIRED ON)
  set(CMAKE_C_EXTENSIONS ON)
endif()

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
  set(CMAKE_CXX_STANDARD_REQUIRED ON)
  set(CMAKE_CXX_EXTENSIONS OFF)
endif()

if(USE_CCACHE)
  set(CMAKE_C_COMPILER_LAUNCHER ccache)
  set(CMAKE_CXX_COMPILER_LAUNCHER ccache)
endif()

set(CMAKE_C_FLAGS_DEBUG "-g -O0")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0")

if(MINGW)
  if(CMAKE_BUILD_TYPE STREQUAL Debug)
    # this avoids string table overflow errors during compilation
    add_compile_options(-O1)
  endif()

  # required by some link targets
  # and (almost) all test executables
  add_compile_options(-Wa,-mbig-obj)
endif()

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

if(NOT ONLY_DOC)
  find_package(Protobuf REQUIRED)
  find_package(OSI REQUIRED)
  set(Boost_USE_STATIC_LIBS OFF)
  find_package(Boost COMPONENTS headers filesystem REQUIRED)
  find_package(Iconv REQUIRED)
  set(LIBXML2_WITH_ICONV ON)
  find_package(LibXml2 REQUIRED)
  find_package(units REQUIRED CONFIG)
  if(WITH_PROTOBUF_ARENA)
    add_compile_definitions(USE_PROTOBUF_ARENA)
  endif()

  if(WITH_TESTS)
    find_package(GTest REQUIRED)   # force config mode for better lookup consistency with newer gtest versions
    message(STATUS "Found GTest: ${GTest_DIR}")

    if(WITH_COVERAGE)
      find_package(Gcov REQUIRED)
      find_package(Fastcov REQUIRED)
      find_package(Genhtml REQUIRED)
    endif()
  endif()
endif ()

if(WIN32)
  set(CMAKE_INSTALL_PREFIX "C:/SCM" CACHE PATH "Destination directory")
  add_compile_definitions(WIN32)
  add_compile_definitions(BOOST_ALL_NO_LIB)
  add_compile_definitions(BOOST_ALL_DYN_LINK)
  add_compile_definitions(_USE_MATH_DEFINES)
  if(WITH_DEBUG_POSTFIX)
    set(CMAKE_DEBUG_POSTFIX "d")
  endif()
else()
  set(CMAKE_INSTALL_PREFIX "/SCM" CACHE PATH "Destination directory")
  add_compile_definitions(unix)
  add_link_options(LINKER:-z,defs)   # fail during link time on undefined references (instead of runtime)
endif()

add_compile_definitions($<IF:$<CONFIG:Debug>,DEBUG_POSTFIX="${CMAKE_DEBUG_POSTFIX}",DEBUG_POSTFIX=\"\">)

include(HelperMacros)

#######################################

if(WIN32)
  # dlls have to reside in the directory of the executable loading them
  # an empty string would be replaced by the default ("bin") when running install, thus "." is used here
  set(SUBDIR_LIB_COMMON .)
  set(SUBDIR_LIB_EXTERNAL .)
endif()

if(MSVC)
  add_compile_definitions(_CRT_SECURE_NO_DEPRECATE)
  add_compile_definitions(_CRT_NONSTDC_NO_DEPRECATE)

  # get rid of annoying template needs to have dll-interface warnings on VisualStudio
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -wd4251 -wd4335")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -wd4250")
endif()

###############################################################################
# Documentation
###############################################################################

if(WITH_DOC)
  find_package(Sphinx REQUIRED)
endif()

###############################################################################

