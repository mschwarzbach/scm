################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Find Package Adapter for OSI (Open Simulation Interface)
#
# Creates the follwoing imported targets (if available):
# - osi::static
# - osi::pic

set(OSI_STATIC_NAMES
        open_simulation_interface_static.lib
        libopen_simulation_interface_static.a
        )

set(OSI_PIC_NAMES
        open_simulation_interface_pic.lib
        libopen_simulation_interface_pic.a
        )

find_library(OSI_STATIC_LIBRARY NAMES ${OSI_STATIC_NAMES}
        PATHS
        ${PREFIX_PATH}
        /usr/local
        /usr
        PATH_SUFFIXES
        lib/osi3
        lib
        lib64
        )

find_library(OSI_PIC_LIBRARY NAMES ${OSI_PIC_NAMES}
        PATHS
        ${PREFIX_PATH}
        /usr/local
        /usr
        PATH_SUFFIXES
        lib/osi3
        lib
        lib64
        )

if(OSI_STATIC_LIBRARY)
  message(STATUS "Found OSI (static): ${OSI_STATIC_LIBRARY}")
  get_filename_component(OSI_STATIC_LIBRARY_DIR "${OSI_STATIC_LIBRARY}" DIRECTORY)
  add_library(osi::static IMPORTED STATIC)
  set_target_properties(osi::static
          PROPERTIES
          IMPORTED_LOCATION ${OSI_STATIC_LIBRARY}
          INTERFACE_INCLUDE_DIRECTORIES ${OSI_STATIC_LIBRARY_DIR}/../../include
          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf_static)
else()
  message(STATUS "Didn't find OSI (static)")
endif()

if(OSI_PIC_LIBRARY)
  message(STATUS "Found OSI (pic): ${OSI_PIC_LIBRARY}")
  get_filename_component(OSI_PIC_LIBRARY_DIR "${OSI_PIC_LIBRARY}" DIRECTORY)
  add_library(open_simulation_interface::open_simulation_interface_pic IMPORTED STATIC)
  set_target_properties(open_simulation_interface::open_simulation_interface_pic
          PROPERTIES
          IMPORTED_LOCATION ${OSI_PIC_LIBRARY}
          INTERFACE_INCLUDE_DIRECTORIES ${OSI_PIC_LIBRARY_DIR}/../../include
          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf_static)
  target_link_options(protobuf::libprotobuf_static INTERFACE "SHELL:-Wl,--whole-archive $<TARGET_FILE:protobuf::libprotobuf_static> -Wl,--no-whole-archive")
else()
  message(STATUS "Didn't find OSI (pic)")
endif()


unset(OSI_STATIC_LIBRARY)
unset(OSI_PIC_LIBRARY)
