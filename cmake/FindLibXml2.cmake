################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
function(package_library_targets libraries package_libdir deps out_libraries out_libraries_target build_type package_name)
    unset(_ACTUAL_TARGETS CACHE)
    unset(_FOUND_SYSTEM_LIBS CACHE)
    foreach(_LIBRARY_NAME ${libraries})
        find_library(FOUND_LIBRARY NAMES ${_LIBRARY_NAME} PATHS ${package_libdir}
                     NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
        if(FOUND_LIBRARY)
            message(STATUS "Library ${_LIBRARY_NAME} found ${FOUND_LIBRARY}")
            list(APPEND _out_libraries ${FOUND_LIBRARY})
            if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
                # Create a micro-target for each lib/a found
                string(REGEX REPLACE "[^A-Za-z0-9.+_-]" "_" _LIBRARY_NAME ${_LIBRARY_NAME})
                set(_LIB_NAME _LIB::${package_name}_${_LIBRARY_NAME}${build_type})
                if(NOT TARGET ${_LIB_NAME})
                    # Create a micro-target for each lib/a found
                    add_library(${_LIB_NAME} UNKNOWN IMPORTED)
                    set_target_properties(${_LIB_NAME} PROPERTIES IMPORTED_LOCATION ${FOUND_LIBRARY})
                    set(_ACTUAL_TARGETS ${_ACTUAL_TARGETS} ${_LIB_NAME})
                else()
                    message(STATUS "Skipping already existing target: ${_LIB_NAME}")
                endif()
                list(APPEND _out_libraries_target ${_LIB_NAME})
            endif()
            message(STATUS "Found: ${FOUND_LIBRARY}")
        else()
            message(STATUS "Library ${_LIBRARY_NAME} not found in package, might be system one")
            list(APPEND _out_libraries_target ${_LIBRARY_NAME})
            list(APPEND _out_libraries ${_LIBRARY_NAME})
            set(_FOUND_SYSTEM_LIBS "${_FOUND_SYSTEM_LIBS};${_LIBRARY_NAME}")
        endif()
        unset(FOUND_LIBRARY CACHE)
    endforeach()

    if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
        # Add all dependencies to all targets
        string(REPLACE " " ";" deps_list "${deps}")
        foreach(_ACTUAL_TARGET ${_ACTUAL_TARGETS})
            set_property(TARGET ${_ACTUAL_TARGET} PROPERTY INTERFACE_LINK_LIBRARIES "${_FOUND_SYSTEM_LIBS};${deps_list}")
        endforeach()
    endif()

    set(${out_libraries} ${_out_libraries} PARENT_SCOPE)
    set(${out_libraries_target} ${_out_libraries_target} PARENT_SCOPE)
endfunction()


include(FindPackageHandleStandardArgs)
# Global approach
set(LibXml2_FOUND 1)
set(LibXml2_VERSION "2.11.5")

find_package_handle_standard_args(LibXml2 REQUIRED_VARS
                                  LibXml2_VERSION VERSION_VAR LibXml2_VERSION)
mark_as_advanced(LibXml2_FOUND LibXml2_VERSION)

set(libxml2_STATIC_NAMES
  libxml2.a
)

find_library(libxml2_STATIC_LIBRARY NAMES ${libxml2_STATIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(NOT libxml2_STATIC_LIBRARY)
    message(FATAL_ERROR "libxml2 shared library not found")
endif()

get_filename_component(libxml2_STATIC_LIBRARY_DIR "${libxml2_STATIC_LIBRARY}" DIRECTORY)
message(STATUS "Found libxml2: ${libxml2_STATIC_LIBRARY_DIR}")

set(LibXml2_INCLUDE_DIRS "${libxml2_STATIC_LIBRARY_DIR}/../include" "${libxml2_STATIC_LIBRARY_DIR}/../include/libxml2")
set(LibXml2_INCLUDE_DIR "${libxml2_STATIC_LIBRARY_DIR}/../include;${libxml2_STATIC_LIBRARY_DIR}/../include/libxml2")
set(LibXml2_INCLUDES "${libxml2_STATIC_LIBRARY_DIR}/../include" "${libxml2_STATIC_LIBRARY_DIR}/../include/libxml2")
set(LibXml2_RES_DIRS )
set(LibXml2_DEFINITIONS "-DLIBXML_STATIC")
set(LibXml2_LINKER_FLAGS_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(LibXml2_COMPILE_DEFINITIONS "LIBXML_STATIC")
set(LibXml2_COMPILE_OPTIONS_LIST "" "")
set(LibXml2_COMPILE_OPTIONS_C "")
set(LibXml2_COMPILE_OPTIONS_CXX "")
set(LibXml2_LIBRARIES_TARGETS "") # Will be filled later, if CMake 3
set(LibXml2_LIBRARIES "") # Will be filled later
set(LibXml2_LIBS "") # Same as LibXml2_LIBRARIES
set(LibXml2_SYSTEM_LIBS m pthread ${CMAKE_DL_LIBS})
set(LibXml2_FRAMEWORK_DIRS )
set(LibXml2_FRAMEWORKS )
set(LibXml2_FRAMEWORKS_FOUND "") # Will be filled later
set(LibXml2_BUILD_MODULES_PATHS "${libxml2_STATIC_LIBRARY_DIR}/cmake/libxml2-variables.cmake")

mark_as_advanced(LibXml2_INCLUDE_DIRS
                 LibXml2_INCLUDE_DIR
                 LibXml2_INCLUDES
                 LibXml2_DEFINITIONS
                 LibXml2_LINKER_FLAGS_LIST
                 LibXml2_COMPILE_DEFINITIONS
                 LibXml2_COMPILE_OPTIONS_LIST
                 LibXml2_LIBRARIES
                 LibXml2_LIBS
                 LibXml2_LIBRARIES_TARGETS)

# Find the real .lib/.a and add them to LibXml2_LIBS and LibXml2_LIBRARY_LIST
set(LibXml2_LIBRARY_LIST xml2)
set(LibXml2_LIB_DIRS "${libxml2_STATIC_LIBRARY_DIR}/")

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_LibXml2_DEPENDENCIES "${LibXml2_FRAMEWORKS_FOUND} ${LibXml2_SYSTEM_LIBS} Iconv::Iconv")

package_library_targets("${LibXml2_LIBRARY_LIST}"  # libraries
                              "${LibXml2_LIB_DIRS}"      # package_libdir
                              "${_LibXml2_DEPENDENCIES}"  # deps
                              LibXml2_LIBRARIES            # out_libraries
                              LibXml2_LIBRARIES_TARGETS    # out_libraries_targets
                              ""                          # build_type
                              "LibXml2")                                      # package_name

set(LibXml2_LIBS ${LibXml2_LIBRARIES})

foreach(_FRAMEWORK ${LibXml2_FRAMEWORKS_FOUND})
    list(APPEND LibXml2_LIBRARIES_TARGETS ${_FRAMEWORK})
    list(APPEND LibXml2_LIBRARIES ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${LibXml2_SYSTEM_LIBS})
    list(APPEND LibXml2_LIBRARIES_TARGETS ${_SYSTEM_LIB})
    list(APPEND LibXml2_LIBRARIES ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(LibXml2_LIBRARIES_TARGETS "${LibXml2_LIBRARIES_TARGETS};Iconv::Iconv")
set(LibXml2_LIBRARIES "${LibXml2_LIBRARIES};Iconv::Iconv")

set(CMAKE_MODULE_PATH "${libxml2_STATIC_LIBRARY_DIR}/../" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "${libxml2_STATIC_LIBRARY_DIR}/../" ${CMAKE_PREFIX_PATH})

if(NOT ${CMAKE_VERSION} VERSION_LESS "3.0")
    # Target approach
    if(NOT TARGET LibXml2::LibXml2)
        add_library(LibXml2::LibXml2 INTERFACE IMPORTED)
        if(LibXml2_INCLUDE_DIRS)
            set_target_properties(LibXml2::LibXml2 PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
                                  "${LibXml2_INCLUDE_DIRS}")
        endif()
        set_property(TARGET LibXml2::LibXml2 PROPERTY INTERFACE_LINK_LIBRARIES
                     "${LibXml2_LIBRARIES_TARGETS};${LibXml2_LINKER_FLAGS_LIST}")
        set_property(TARGET LibXml2::LibXml2 PROPERTY INTERFACE_COMPILE_DEFINITIONS
                     ${LibXml2_COMPILE_DEFINITIONS})
        set_property(TARGET LibXml2::LibXml2 PROPERTY INTERFACE_COMPILE_OPTIONS
                     "${LibXml2_COMPILE_OPTIONS_LIST}")

        # Library dependencies
        include(CMakeFindDependencyMacro)

        if(NOT Iconv_FOUND)
            find_dependency(Iconv REQUIRED)
        else()
            message(STATUS "Dependency Iconv already found")
        endif()

    endif()
endif()

foreach(_BUILD_MODULE_PATH ${LibXml2_BUILD_MODULES_PATHS})
    include(${_BUILD_MODULE_PATH})
endforeach()
