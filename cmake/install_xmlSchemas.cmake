################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# sets install command to copy xml Schema files from repository to installation directory
install(DIRECTORY ${CMAKE_SOURCE_DIR}/schemas DESTINATION .)
message("-- xml schema file will be installed in the schema folder of the installation directory")
