/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmFmu.h"

#include <memory>

ScmFmu::ScmFmu(fmi2String theinstanceName, fmi2Type thefmuType, fmi2String thefmuGUID, fmi2String thefmuResourceLocation, const fmi2CallbackFunctions* thefunctions, fmi2Boolean thevisible, fmi2Boolean theloggingOn)
    : FmiConnector{theinstanceName, thefmuType, thefmuGUID, thefmuResourceLocation, thefunctions, thevisible, theloggingOn}
{
}

fmi2Status ScmFmu::InitFmu(const FmiParameters& parameters, const osi3::GroundTruth& initGroundTruth, FmiLogInterface& logger)
{
  scm = std::make_unique<scm::ModuleController>(parameters, initGroundTruth, logger);
  return fmi2OK;
}

fmi2Status ScmFmu::StepFmu(const osi3::SensorView& sensorView, const osi3::TrafficCommand& trafficCommand, osi3::TrafficUpdate& trafficUpdate)
{
  if (!isInitialized)
  {
    isInitialized = scm->InitializeModules(sensorView);
  }
  scm->Trigger(sensorView, trafficCommand, trafficUpdate);
  return fmi2OK;
}

FMI2_Export fmi2Component fmi2Instantiate(fmi2String instanceName,
                                          fmi2Type fmuType,
                                          fmi2String fmuGUID,
                                          fmi2String fmuResourceLocation,
                                          const fmi2CallbackFunctions* functions,
                                          fmi2Boolean visible,
                                          fmi2Boolean loggingOn)
{
  FmiConnector* myc = new ScmFmu(instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
  return myc->ValidateInstance();
}

FMI2_Export void fmi2FreeInstance(fmi2Component c)
{
  FmiConnector* myc = (FmiConnector*)c;
  myc->FreeInstance();
  delete myc;
}
