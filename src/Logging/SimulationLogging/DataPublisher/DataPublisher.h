/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file DataPublisher.h
#pragma once

#include "../../../include/Logging/DataBufferInterface.h"
#include "../../../include/Logging/PublisherInterface.h"

namespace scm::publisher
{

//! @brief The DataPublisher represents the result every Agents in every Timestamp
class DataPublisher : public scm::publisher::PublisherInterface
{
public:
  //! @brief Constructor initializes and creates the data publisher.
  //! @param dataBuffer
  //! @param agentId
  DataPublisher(DataBufferWriteInterface *const dataBuffer, const int agentId)
      : _dataBuffer{dataBuffer},
        agentId{agentId}
  {
  }

  //! @brief Publish the values from the key
  //! @param key
  //! @param value
  void Publish(const scm::databuffer::Key &key, const scm::databuffer::Value &value) override;

private:
  //! @brief References the dataBuffer backend
  DataBufferWriteInterface *const _dataBuffer;  
  //! @brief The id of an agent
  const int agentId;
};

}  // namespace scm::publisher
