/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DataPublisher.h"

namespace scm::publisher {

void DataPublisher::Publish(const scm::databuffer::Key &key, const scm::databuffer::Value &value)
{
  _dataBuffer->PutCyclic(agentId, key, value);
}

} // namespace scm::publisher
