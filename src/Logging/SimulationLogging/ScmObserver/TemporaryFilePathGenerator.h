/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

//! @class TemporaryFilePathGenerator
//! @brief A class for generating temporary file paths with unique names.
class TemporaryFilePathGenerator
{
public:
  //! @brief Constructor for TemporaryFilePathGenerator.
  //! @param filePath The base path for generating temporary file names.
  explicit TemporaryFilePathGenerator(std::filesystem::path filePath)
      : filePath(std::move(filePath)) {}

  ~TemporaryFilePathGenerator() = default;

  //! @brief Create a unique temporary file path.
  //!
  //! If the provided file path exists, this method generates a new file name by appending
  //! a 6-digit number to the original file name.
  //!
  //! @return The generated unique temporary file path.
  std::filesystem::path CreateTemporaryFilePath()
  {
    if (std::filesystem::exists(filePath))
    {
      int counter = 0;
      do
      {
        //! Generate a new filename with a 6-digit number appended
        std::ostringstream newFilenameStream;
        newFilenameStream << std::left << std::setw(6) << std::setfill('0') << counter % 1000000;
        std::string newFilename = filePath.stem().string() + "_" + newFilenameStream.str() + ".tmp";
        filePath = std::filesystem::path(newFilename);
        counter++;
      } while (std::filesystem::exists(filePath));
    }

    return filePath;
  }

private:
  std::filesystem::path filePath;
};