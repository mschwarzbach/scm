/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  ScmFileHandler.cpp */
//-----------------------------------------------------------------------------

#include "ScmFileHandler.h"

#include <sstream>

#include "../../../include/Logging/DataBufferInterface.h"
#include "../../../include/common/XmlParser.h"
#include "src/Logging/SimulationLogging/ScmObserver/TemporaryFilePathGenerator.h"
#include "src/Logging/SimulationLogging/Utils/SimulationLoggingUtils.h"

ScmFileHandler::ScmFileHandler(const scm::DataBufferReadInterface& dataBuffer)
    : dataBuffer{dataBuffer}
{
}

void ScmFileHandler::WriteRun(const Cyclics& cyclics,
                              const std::map<std::string, Cyclics>& perceptionPerAgent,
                              const scm::LoggerOptions& loggerOptions,
                              const std::map<std::string, std::string>& scmParameters)
{
  std::stringstream ss;
  ss << COMPONENTNAME << " append log to file: " << tmpFilePath;
  //    LOG(CbkLogLevel::Debug, ss.str());

  // init new run result
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.RUNRESULT));

  if (loggerOptions.logParameters)
  {
    AddDriverParameters(scmParameters);
  }

  if (loggerOptions.logValues)
  {
    for (auto& [agentId, perception] : perceptionPerAgent)
    {
      WritePerception(perception, folder);
    }

    // write CyclicsTag
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.CYCLICS));

    if (loggerOptions.xmlOutput)  // cyclics as xml output
    {
      AddHeader(cyclics);
      AddSamples(cyclics);
    }
    else  // cyclics as csv output
    {
      const std::string csvFilename = "Cyclics.csv";
      AddReference(csvFilename);
      const std::filesystem::path path = folder / csvFilename;
      WriteCsvCyclics(path, cyclics);
    }

    // close CyclicsTag
    xmlTextWriterEndElement(xmlFileStream);
  }

  // close RunResultTag
  xmlTextWriterEndElement(xmlFileStream);
}

void ScmFileHandler::AddDriverParameters(const std::map<std::string, std::string>& driverParameter)
{
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.AGENTS));

  std::unordered_map<std::string, std::map<std::string, std::string>> attributes{};

  // extract and collect all parameters for each agent
  for (const auto& param : driverParameter)
  {
    const int indexId = param.first.find(":");
    const std::string agentId = param.first.substr(0, indexId);     // get agent id
    const std::string paramName = param.first.substr(indexId + 1);  // get param name
    attributes[agentId].insert(std::pair<std::string, std::string>(paramName, param.second));
  }

  for (const auto& agentsParameters : attributes)
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.AGENT));
    xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.ID), scm::common::toXmlChar(agentsParameters.first));
    for (const auto& parameters : agentsParameters.second)
    {
      xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(parameters.first), scm::common::toXmlChar(parameters.second));
    }
    xmlTextWriterEndElement(xmlFileStream);  // end Agent
  }

  xmlTextWriterEndElement(xmlFileStream);  // end Agents
}

void ScmFileHandler::WritePerception(const Cyclics& perception, const std::filesystem::path& runFolder)
{
  const std::filesystem::path filePath = runFolder.string() + "/" + "Perception.csv";

  WriteCsvCyclics(filePath, perception);
}

void ScmFileHandler::WriteStartOfFile(const std::string& frameworkVersion)
{
  // setup environment
  auto folderPath = std::filesystem::path(folder);
  if (!std::filesystem::exists(folderPath) && !std::filesystem::create_directories(folderPath))
  {
    std::stringstream stream;
    stream << COMPONENTNAME << " could not create folder: " << folder;
    throw std::runtime_error(stream.str());
  }

  if (std::filesystem::exists(finalPath))
  {
    std::filesystem::remove(finalPath);
  }

  RemoveCsvCyclics(folder);

  std::string filePath = folder.string() + "/" + finalFilename.string();
  TemporaryFilePathGenerator filePathCreator(filePath);
  tmpFilePath = filePathCreator.CreateTemporaryFilePath();
  xmlFile.open(tmpFilePath.string());

  if (!xmlFile.is_open())
  {
    std::stringstream stream;
    stream << COMPONENTNAME << ": could not create file: " << tmpFilePath.string();
    throw std::runtime_error(stream.str());
  }

  xmlFileStream = xmlNewTextWriterFilename(tmpFilePath.string().c_str(), 0);
  if (!xmlFileStream)
  {
    xmlFile.close();
    throw std::runtime_error("Could not create XML writer");
  }

  xmlTextWriterSetIndent(xmlFileStream, 1);
  xmlTextWriterStartDocument(xmlFileStream, "1.0", "UTF-8", nullptr);
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.SIMULATIONOUTPUT));
  xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.FRAMEWORKVERSION), scm::common::toXmlChar(frameworkVersion));
  xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.SCHEMAVERSION), scm::common::toXmlChar(outputFileVersion));
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.SCENERYFILE));
  xmlTextWriterWriteString(xmlFileStream, scm::common::toXmlChar(sceneryFile));
  xmlTextWriterEndElement(xmlFileStream);
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.RUNRESULTS));
}

void ScmFileHandler::WriteEndOfFile()
{
  // close RunResultsTag
  xmlTextWriterEndElement(xmlFileStream);

  // close SimulationOutputTag
  xmlTextWriterEndElement(xmlFileStream);

  xmlTextWriterEndDocument(xmlFileStream);
  xmlFreeTextWriter(xmlFileStream);
  xmlFile.flush();
  xmlFile.close();

  // finalize results
  if (std::filesystem::exists(tmpFilePath) && !std::filesystem::exists(finalPath))
  {
    std::filesystem::rename(tmpFilePath, finalPath);
  }
}

void ScmFileHandler::AddEvents(const Events& events)
{
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.EVENTS));

  for (const auto& event : events)
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.EVENT));
    xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.TIME), scm::common::toXmlChar(std::to_string(event.time)));
    xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.SOURCE), scm::common::toXmlChar(event.dataRow.key));
    xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.NAME), scm::common::toXmlChar(event.dataRow.data.name));

    WriteEntities(outputTags.TRIGGERINGENTITIES, event.dataRow.data.triggeringEntities.entities, true);
    WriteEntities(outputTags.AFFECTEDENTITIES, event.dataRow.data.affectedEntities.entities, true);
    WriteParameter(event.dataRow.data.parameter, true);

    xmlTextWriterEndElement(xmlFileStream);  // event
  }

  xmlTextWriterEndElement(xmlFileStream);  // events
}

void ScmFileHandler::AddHeader(const Cyclics& cyclics)
{
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.HEADER));
  xmlTextWriterWriteString(xmlFileStream, scm::common::toXmlChar(cyclics.GetHeader()));
  xmlTextWriterEndElement(xmlFileStream);
}

void ScmFileHandler::AddSamples(const Cyclics& cyclics)
{
  // write SamplesTag
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.SAMPLES));
  const auto& timeSteps = cyclics.GetTimeSteps();

  unsigned int timeStepNumber = 0;
  for (const auto timeStep : timeSteps)
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.SAMPLE));
    xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(outputAttributes.TIME), scm::common::toXmlChar(std::to_string(timeStep)));
    xmlTextWriterWriteString(xmlFileStream, scm::common::toXmlChar(cyclics.GetSamplesLine(timeStepNumber)));

    // close SampleTag
    xmlTextWriterEndElement(xmlFileStream);

    ++timeStepNumber;
  }

  // close SamplesTag
  xmlTextWriterEndElement(xmlFileStream);
}

void ScmFileHandler::AddReference(const std::string& filename)
{
  // write CyclicsFileTag
  xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(outputTags.CYCLICSFILE));

  xmlTextWriterWriteString(xmlFileStream, scm::common::toXmlChar(filename));

  // close CyclicsFileTag
  xmlTextWriterEndElement(xmlFileStream);
}

void ScmFileHandler::RemoveCsvCyclics(const std::filesystem::path& directory)
{
  for (const auto& entry : std::filesystem::directory_iterator(directory))
  {
    const std::filesystem::path& fileInfo = entry.path();
    if (std::filesystem::is_regular_file(fileInfo) && fileInfo.extension() == ".csv" && fileInfo.stem().string().find("Cyclics") == 0)
    {
      std::filesystem::remove(fileInfo);
    }
  }
}

void ScmFileHandler::WriteCsvCyclics(const std::filesystem::path& filepath, const Cyclics& cyclics)
{
  std::ofstream csvFile(filepath.string());
  if (!csvFile.is_open())
  {
    std::stringstream stream;
    stream << COMPONENTNAME << " could not create file: " << tmpFilePath;
    throw std::runtime_error(stream.str());
  }

  csvFile << "Timestep, " << cyclics.GetHeader().c_str() << '\n';

  const auto& timeSteps = cyclics.GetTimeSteps();
  unsigned int timeStepNumber = 0;
  for (const auto timeStep : timeSteps)
  {
    csvFile << std::to_string(timeStep).c_str() << ", " << cyclics.GetSamplesLine(timeStepNumber).c_str() << '\n';
    ++timeStepNumber;
  }

  csvFile.flush();

  csvFile.close();
}

void ScmFileHandler::WriteEntities(const std::string tag, const scm::type::EntityIds& entities, bool mandatory)
{
  if (!entities.empty())
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(tag));
    for (const auto& entity : entities)
    {
      xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(output::tag::ENTITY));
      xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(output::attribute::ID), scm::common::toXmlChar(std::to_string(entity)));
      xmlTextWriterEndElement(xmlFileStream);
    }
    xmlTextWriterEndElement(xmlFileStream);
  }
  else if (mandatory)
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(tag));
    xmlTextWriterEndElement(xmlFileStream);
  }
}

void ScmFileHandler::WriteParameter(const scm::type::FlatParameter& parameters, bool mandatory)
{
  constexpr auto tag = output::tag::PARAMETERS;
  if (!parameters.empty())
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(tag));

    // No structured binding on purpose:
    // see https://stackoverflow.com/questions/46114214/lambda-implicit-capture-fails-with-variable-declared-from-structured-binding
    for (const auto& p : parameters)
    {
      auto parameterWriter = [&](const std::string& value)
      {
        xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(output::tag::PARAMETER));
        xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(output::attribute::KEY), scm::common::toXmlChar(p.first));
        xmlTextWriterWriteAttribute(xmlFileStream, scm::common::toXmlChar(output::attribute::VALUE), scm::common::toXmlChar(value));
        xmlTextWriterEndElement(xmlFileStream);
      };

      std::visit(scm::FlatParameter::to_string(parameterWriter), p.second);
    }
    xmlTextWriterEndElement(xmlFileStream);
  }
  else if (mandatory)
  {
    xmlTextWriterStartElement(xmlFileStream, scm::common::toXmlChar(tag));
    xmlTextWriterEndElement(xmlFileStream);
  }
}