/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
struct OutputAttributes
{
  static constexpr const char* FRAMEWORKVERSION = "FrameworkVersion";
  static constexpr const char* SCHEMAVERSION = "SchemaVersion";
  static constexpr const char* RUNID = "RunId";
  static constexpr const char* FAILUREPROBABITLITY = "FailureProbability";
  static constexpr const char* LATENCY = "Latency";
  static constexpr const char* OPENINGANGLEH = "OpeningAngleH";
  static constexpr const char* OPENINGANGLEV = "OpeningAngleV";
  static constexpr const char* MOUNTINGPOSITIONLONGITUDINAL = "MountingPosLongitudinal";
  static constexpr const char* MOUNTINGPOSITIONLATERAL = "MountingPosLateral";
  static constexpr const char* MOUNTINGPOSITIONHEIGHT = "MountingPosHeight";
  static constexpr const char* ORIENTATIONYAW = "OrientationYaw";
  static constexpr const char* ORIENTATIONPITCH = "OrientationPitch";
  static constexpr const char* ORIENTATIONROLL = "OrientationRoll";
  static constexpr const char* DETECTIONRANGE = "DetectionRange";
  static constexpr const char* VEHICLEMODELTYPE = "VehicleModelType";
  static constexpr const char* DRIVERPROFILENAME = "DriverProfileName";
  static constexpr const char* AGENTTYPEGROUPNAME = "AgentTypeGroupName";
  static constexpr const char* AGENTTYPENAME = "AgentTypeName";
  static constexpr const char* AGENTTYPE = "AgentType";
  static constexpr const char* TIME = "Time";
  static constexpr const char* TYPE = "Type";
  static constexpr const char* NAME = "Name";
  static constexpr const char* KEY = "Key";
  static constexpr const char* VALUE = "Value";
  static constexpr const char* ID = "Id";
  static constexpr const char* SOURCE = "Source";
  static constexpr const char* WIDTH = "Width";
  static constexpr const char* LENGTH = "Length";
  static constexpr const char* HEIGHT = "Height";
  static constexpr const char* LONGITUDINALPIVOTOFFSET = "LongitudinalPivotOffset";
};

struct OutputTags
{
  static constexpr const char* RUNRESULTS = "RunResults";
  static constexpr const char* RUNRESULT = "RunResult";
  static constexpr const char* RUNSTATISTICS = "RunStatistics";
  static constexpr const char* SIMULATIONOUTPUT = "SimulationOutput";
  static constexpr const char* EVENTS = "Events";
  static constexpr const char* EVENT = "Event";
  static constexpr const char* EVENTPARAMETER = "EventParameter";
  static constexpr const char* AGENTS = "Agents";
  static constexpr const char* AGENT = "Agent";
  static constexpr const char* SENSORS = "Sensors";
  static constexpr const char* SENSOR = "Sensor";
  static constexpr const char* CYCLICS = "Cyclics";
  static constexpr const char* CYCLICSFILE = "CyclicsFile";
  static constexpr const char* HEADER = "Header";
  static constexpr const char* SAMPLES = "Samples";
  static constexpr const char* SAMPLE = "Sample";
  static constexpr const char* SCENERYFILE = "SceneryFile";
  static constexpr const char* VEHICLEATTRIBUTES = "VehicleAttributes";
  static constexpr const char* TRIGGERINGENTITIES = "TriggeringEntities";
  static constexpr const char* AFFECTEDENTITIES = "AffectedEntities";
};

namespace output::tag {
static constexpr char ENTITY[] = "Entity";
static constexpr char PARAMETERS[] = "Parameters";
static constexpr char PARAMETER[] = "Parameter";
} // namespace output::tag

namespace output::attribute {
static constexpr char ID[] = "Id";
static constexpr char TIME[] = "Time";
static constexpr char TYPE[] = "Type";
static constexpr char NAME[] = "Name";
static constexpr char KEY[] = "Key";
static constexpr char VALUE[] = "Value";
} // namespace output::attribute

namespace output::event::tag {
static constexpr char EVENTS[] = "NewEvents";
static constexpr char EVENT[] = "Event";
} // namespace output::event::tag
