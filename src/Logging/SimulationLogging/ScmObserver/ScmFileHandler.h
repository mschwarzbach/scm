/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <libxml/xmlwriter.h>

#include <filesystem>
#include <fstream>

#include "../../include/Logging/DataBufferInterface.h"
#include "Cyclics.h"
#include "LogConstants.h"
#include "ScmFileHandler.h"

struct Event
{
  Event (const int& time, const scm::databuffer::ScmAcyclicRow& dataRow) :
        time(time),
        dataRow(dataRow)
  {}

  int time;
  scm::databuffer::ScmAcyclicRow dataRow;
};

using Events = std::vector<Event>;

//-----------------------------------------------------------------------------
/** \brief Provides the basic logging and observer functionality.
*   \details Provides the basic logging functionality.
*            Writes the framework, events, agents and samples into the output.
*            It has two logging modes either event based or as single output.
*            This module also acts as an observer of agent modules.
*
*   \ingroup Observation_Scm
*/
//-----------------------------------------------------------------------------
class ScmFileHandler
{
public:
    const std::string COMPONENTNAME = "ScmFileHandler";

    ScmFileHandler(const scm::DataBufferReadInterface &dataBuffer);
    ScmFileHandler(const ScmFileHandler &) = delete;
    ScmFileHandler(ScmFileHandler &&) = delete;
    ScmFileHandler &operator=(const ScmFileHandler &) = delete;
    ScmFileHandler &operator=(ScmFileHandler &&) = delete;
    ~ScmFileHandler() = default;

    /*!
     * \brief This function gets called after each run and writes all information about this run into the output file
     *
     * \param runStatistik
     * \param cyclics
     * \param dataBuffer
     */
    void WriteRun(const Cyclics &cyclics,
                  const std::map<std::string, Cyclics> &perceptionPerAgent,
                  const scm::LoggerOptions& loggerOptions,
                  const std::map<std::string, std::string> &scmParameters);

    void WritePerception(const Cyclics& perception, const std::filesystem::path& runFolder);
    /*!
     * \brief Sets the directory where the output is written into
     *
     * \param outputDir path of output directory
     */
    void SetOutputLocation(const std::string& outputDir, const std::string& outputFile)
    {
      folder = outputDir;
      finalFilename = outputFile;
      finalPath = folder / finalFilename;
    }

    /*!
     * \brief Creates the output file as simulationOutput.tmp and writes the basic header information
     */
    void WriteStartOfFile(const std::string &frameworkVersion);
    /*!
     * \brief Closes the xml tags flushes the output file, closes it and renames it to simulationOutput.xlm
     */
    void WriteEndOfFile();

  protected:
    std::ofstream xmlFile;

  private:
    /*!
     * \brief Writes the driverParameters into the simulation output during full logging.
     *
     * @param[in]     driverParameter
     */
    void AddDriverParameters(const std::map<std::string, std::string> &driverParameter);

    xmlTextWriterPtr xmlFileStream{};
    const scm::DataBufferReadInterface &dataBuffer;

    std::string sceneryFile;

    OutputAttributes outputAttributes;
    OutputTags outputTags;

    std::filesystem::path folder;
    std::filesystem::path tmpFilename;
    std::filesystem::path finalFilename;
    std::filesystem::path tmpFilePath;
    std::filesystem::path finalPath;

    /*!
     * \brief Writes all events into the simulation output.
     *
     * @param[in]     events     events of the run
     */
    void AddEvents(const Events &events);

    /*!
     * \brief Writes the header into the simulation output during full logging.
     *
     * @param[in]     cyclics    cyclics of the run
     */
    void AddHeader(const Cyclics &cyclics);

    /*!
     * \brief Writes the samples into the simulation output during full logging.
     *
     * @param[in]     cyclics    cyclics of the run
     */
    void AddSamples(const Cyclics &cyclics);

    /*!
     * \brief Writes the filename for the cyclics file into the simulation output during full logging.
     *
     * @param[in]     filename           Name of the file, where cyclics are written to.
     */
    void AddReference(const std::string &filename);

    /*!
     * \brief Removes old cyclic files from directory.
     *
     * @param[in]    directory           directory to delete the cyclic files
     */
    void RemoveCsvCyclics(const std::filesystem::path &directory);

    /*!
     * \brief Writes the cyclics of one run to a csv.
     *
     * @param[in]    filepath            Filepath for current run
     * @param[in]    cyclics             Cyclics of the current run
     */
    void WriteCsvCyclics(const std::filesystem::path &filepath, const Cyclics &cyclics);

    /*!
     * \brief Write entities to XML
     *
     * \param tag       tag name
     * \param entities  list of entities
     * \param mandatory if set, an emtpy tag is added if entities is empty (default false)
     */
    void WriteEntities(const std::string tag, const scm::type::EntityIds &entities, bool mandatory = false);

    /*!
     * \brief Write (event) parameter to XML
     *
     * \remark Might be used for generic parameters in the future - right now, only event parameters
     *
     * \param[in]    parameters  list of parameters
     * \param[in]    mandatory   if set, an emtpy tag is added if parameters are empty (default false)
     */
    void WriteParameter(const scm::type::FlatParameter &parameters, bool mandatory = false);

    static constexpr const char *outputFileVersion = "0.3.1";
};