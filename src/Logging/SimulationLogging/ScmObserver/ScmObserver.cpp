/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmObserver.h"

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "ScmDefinitions.h"
#include "src/Logging/Logging.h"
#include "src/Logging/SimulationLogging/Utils/SimulationLoggingUtils.h"

namespace scm
{
constexpr char parameterPrefix[] = "Driver.Parameter.";
constexpr char perceptionPrefix[] = "Driver.Perception.";
static const std::vector<std::pair<std::string, std::string>> selectedRegexColumns {{"Driver.", ""}, {"Driver.Parameter.", ""}, {"Driver.Perception.", ""}};

ScmObserver::ScmObserver(scm::DataBufferReadInterface *dataBuffer, const scm::RuntimeInformation &runtimeInformation)
    : dataBuffer(dataBuffer), fileHandler{*dataBuffer}, loggerOptions{runtimeInformation.loggerOptions}
{
  outputPath = runtimeInformation.directories.output;
  frameWorkVersion = runtimeInformation.frameworkVersion;
}

void ScmObserver::SimulationPreHook()
{
  if (loggingEnabled())
  {
    std::string filename{"DriverSimulationOutput.xml"};
    fileHandler.SetOutputLocation(outputPath, filename);
    fileHandler.WriteStartOfFile(frameWorkVersion);

    cyclics.Clear();
    perceptionPerAgent.clear();
    driverParameters.clear();
  }
}

void ScmObserver::SimulationUpdateHook(int time)
{
  if (loggingEnabled())
  {
    const auto& dsCyclics = dataBuffer->GetCyclic(std::nullopt, "*");

    for (const scm::databuffer::ScmCyclicRow& dsCyclic : *dsCyclics)
    {
      std::visit(scm::FlatParameter::to_string([&](const std::string& valueStr)
                                               {
                if (std::any_of(selectedRegexColumns.cbegin(), selectedRegexColumns.cend(),
                                [&dsCyclic](const auto& column)
                                {
                                    return dsCyclic.key.find(column.first) == 0 &&
                                           dsCyclic.key.find(column.second, dsCyclic.key.size() - column.second.size()) == dsCyclic.key.size() - column.second.size();
                                }))
                {
                  // filter "Driver.Parameter" from regular cyclic samples
                  if (dsCyclic.key.find(parameterPrefix) != std::string::npos)
                  {
                    driverParameters.insert(std::pair<std::string, std::string>((dsCyclic.entityId < 10 ? "0" : "") + std::to_string(dsCyclic.entityId) + ":" + dsCyclic.key, valueStr));
                  }

                  // Driver.Perception
                  else if (dsCyclic.key.find(perceptionPrefix) != std::string::npos)
                  {
                    InsertPerception(dsCyclic, valueStr, time);
                  }
                  else
                  {
                    cyclics.Insert(time, (dsCyclic.entityId < 10 ? "0" : "") + std::to_string(dsCyclic.entityId) + ":" + dsCyclic.key, valueStr);
                  }
                } }),
                 dsCyclic.value);
    }
  }
}

void ScmObserver::SimulationPostRunHook()
{
  if (loggingEnabled())
  {
    fileHandler.WriteRun(cyclics, perceptionPerAgent, loggerOptions, driverParameters);
    fileHandler.WriteEndOfFile();
  }
}

const std::string ScmObserver::to_string(const scm::databuffer::Values &values)
{
  if (values.empty())
  {
    Logging::Error("Retrieved value from databuffer contains no element");
    return "<error>";
  }

  if (values.size() > 1)
  {
    Logging::Error("Multiple values per databuffer request currently not supported");
  }

  return std::visit(scm::FlatParameter::to_string(), values.front());
}

void ScmObserver::InsertPerception(const ScmCyclicRow& cyclic, const std::string& valueStr, int time)
{
  /*
     * Example key string as input here
     * Scm.Perception.FrontLeft#3:acceleration;
   */

  // reformat key so observed agent id is in front
  int indexAgentIdBegin = cyclic.key.find("#");
  int indexAgentIdEnd = cyclic.key.find(":", indexAgentIdBegin);
  std::string observedAgentId = cyclic.key.substr(indexAgentIdBegin + 1, indexAgentIdEnd - indexAgentIdBegin - 1);  // -1 to get rid off ':'

  if (observedAgentId == "-1")
  {
    return;
  }

  const std::string prefix = perceptionPrefix;
  const std::string aoiKey = observedAgentId + ":" + prefix + "Aoi";

  const int indexDirection = cyclic.key.find(prefix) + prefix.length();
  const int indexEndDirection = cyclic.key.find("#", indexDirection);
  std::string aoiValue = cyclic.key.substr(indexDirection, indexEndDirection - indexDirection);
  std::string payload = cyclic.key.substr(indexAgentIdEnd + 1, std::string::npos);

  if (payload.find("PosLonLat") != std::string::npos)
  {
    const std::string newKey = observedAgentId + ":" + prefix + payload;

    auto tokens = scm::common::TokenizeString(valueStr);

    double surroundingX = std::stod(tokens[0]);
    double surroundingY = std::stod(tokens[1]);

    const std::string baseKey{observedAgentId + ":" + prefix};

    if (surroundingX == ScmDefinitions::DEFAULT_VALUE && surroundingY == ScmDefinitions::DEFAULT_VALUE)
    {
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, baseKey + "PosX", "");
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, aoiKey, aoiValue);

      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, baseKey + "PosY", "");
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, aoiKey, aoiValue);
    }
    else
    {
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, baseKey + "PosX", std::to_string((surroundingX)));
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, aoiKey, aoiValue);

      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, baseKey + "PosY", std::to_string(surroundingY));
      perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, aoiKey, aoiValue);
    }
  }
  else
  {
    const std::string newKey = observedAgentId + ":" + prefix + payload;

    perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, newKey, valueStr);
    perceptionPerAgent[std::to_string(cyclic.entityId)].Insert(time, aoiKey, aoiValue);
  }
}

bool ScmObserver::loggingEnabled()
{
  return loggerOptions.logParameters || loggerOptions.logValues;
}
}