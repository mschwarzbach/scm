/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DataBuffer.h"


using namespace scm::type;

namespace scm
{

bool DataBuffer::Instantiate()
{
  if (!_dataBufferBinding)
  {
    return false;
  }
  else if (!_implementation)
  {
    _implementation = _dataBufferBinding->Instantiate();
    if (!_implementation)
    {
      return false;
    }
  }
  return true;
}

bool DataBuffer::isInstantiated() const
{
  return _implementation != nullptr;
}

void DataBuffer::PutCyclic(const EntityId agentId, const Key &key, const Value &value)
{
  return _implementation->PutCyclic(agentId, key, value);
}

void DataBuffer::PutAcyclic(const EntityId agentId, const Key &key, const ScmAcyclic &acyclic)
{
  return _implementation->PutAcyclic(agentId, key, acyclic);
}

void DataBuffer::PutStatic(const Key &key, const Value &value, bool persist)
{
  return _implementation->PutStatic(key, value, persist);
}

void DataBuffer::ClearRun()
{
  return _implementation->ClearRun();
}

void DataBuffer::ClearTimeStep()
{
  return _implementation->ClearTimeStep();
}

std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> DataBuffer::GetCyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const
{
  return _implementation->GetCyclic(entityId, key);
}

std::unique_ptr<scm::databuffer::ScmAcyclicResultInterface> DataBuffer::GetAcyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const
{
  return _implementation->GetAcyclic(entityId, key);
}

Values DataBuffer::GetStatic(const Key &key) const
{
  return _implementation->GetStatic(key);
}

Keys DataBuffer::GetKeys(const Key &key) const
{
  return _implementation->GetKeys(key);
}

}  // namespace scm