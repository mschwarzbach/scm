/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DataBufferLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        dataBufferInterface.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>

#include "DataBufferBinding.h"
#include "include/common/ScmRuntimeInformation.h"

class DataBufferLibrary
{
public:
    typedef const std::string &(*DataBufferInterface_GetVersion)();
    typedef scm::DataBufferInterface *(*DataBufferInterface_CreateInstanceType)(
            const scm::RuntimeInformation* runtimeInformation);
    typedef void (*DataBufferInterface_DestroyInstanceType)(scm::DataBufferInterface *implementation);


    explicit DataBufferLibrary(const std::string &dataBufferLibraryPath) :
           dataBufferLibraryPath(dataBufferLibraryPath)
    {
    }

    DataBufferLibrary(const DataBufferLibrary&) = delete;
    DataBufferLibrary(DataBufferLibrary&&) = delete;
    DataBufferLibrary& operator=(const DataBufferLibrary&) = delete;
    DataBufferLibrary& operator=(DataBufferLibrary&&) = delete;

    //-----------------------------------------------------------------------------
    //! Destructor, deletes the stored library (unloads it if necessary)
    //-----------------------------------------------------------------------------
    virtual ~DataBufferLibrary();

    //-----------------------------------------------------------------------------
    //! Creates a QLibrary based on the path from the constructor and stores function
    //! pointer for getting the library version, creating and destroying instances
    //! and setting the stochasticsInterface item
    //! (see typedefs for corresponding signatures).
    //!
    //! @return   true on successful operation, false otherwise
    //-----------------------------------------------------------------------------
    bool Init();

    //-----------------------------------------------------------------------------
    //! Delete the dataBufferInterface and the library
    //!
    //! @return   Flag if the release was successful
    //-----------------------------------------------------------------------------
    bool ReleaseDataBuffer();

    //-----------------------------------------------------------------------------
    //! Make sure that the library exists and is loaded, then call the "create instance"
    //! function pointer, which instantiates a DataBufferInterface. The created Interface
    //! is stored.
    //!
    //! @param[in]   runtimeInformation   Reference to the simulation runtime information
    //!
    //! @return   DataBufferInterface created
    //-----------------------------------------------------------------------------
    scm::DataBufferInterface* CreateDataBuffer(const scm::RuntimeInformation& runtimeInformation);

private:
    const std::string DllGetVersionId = "OpenPASS_GetVersion";
    const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
    const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";

    const std::string dataBufferLibraryPath;
    scm::DataBufferInterface* dataBufferInterface = nullptr;
    boost::dll::shared_library library;
    DataBufferInterface_GetVersion getVersionFunc{nullptr};
    DataBufferInterface_CreateInstanceType createInstanceFunc{nullptr};
    DataBufferInterface_DestroyInstanceType destroyInstanceFunc{nullptr};
};

