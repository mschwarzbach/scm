/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DataBufferBinding.h
//! @brief This file contains the interface to the dataBuffer library.
//-----------------------------------------------------------------------------

#pragma once

#include "include/common/ScmRuntimeInformation.h"
#include "include/Logging/DataBufferInterface.h"

class DataBufferLibrary;

namespace scm
{

class DataBufferBinding
{
public:
    DataBufferBinding(std::string libraryPath, const scm::RuntimeInformation& runtimeInformation);
    DataBufferBinding(const DataBufferBinding&) = delete;
    DataBufferBinding(DataBufferBinding&&) = delete;
    DataBufferBinding& operator=(const DataBufferBinding&) = delete;
    DataBufferBinding& operator=(DataBufferBinding&&) = delete;
    virtual ~DataBufferBinding();

    //-----------------------------------------------------------------------------
    //! Gets the dataBuffer instance library and stores it,
    //! then creates a new dataBufferInterface of the library.
    //!
    //! @return   dataBufferInterface created from the library
    //-----------------------------------------------------------------------------
    scm::DataBufferInterface* Instantiate();

    //-----------------------------------------------------------------------------
    //! Unloads the stochasticsInterface binding by deleting the library.
    //-----------------------------------------------------------------------------
    void Unload();

  private:
    const std::string _libraryPath;
    DataBufferLibrary* _library = nullptr;
    const scm::RuntimeInformation& _runtimeInformation;
};

}  // namespace scm