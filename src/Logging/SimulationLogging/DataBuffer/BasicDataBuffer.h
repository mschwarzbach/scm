/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*!
 * \file  BasicDataBuffer.h
 *
 * \brief Stores data sent by publishers and provides acces to this data for observers
 */

#pragma once

#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include "include/common/ScmRuntimeInformation.h"
#include "include/Logging/DataBufferInterface.h"

using namespace scm::databuffer;
namespace scm
{

using Persistence = bool;
using StaticStore = std::unordered_map<Key, std::tuple<Value, Persistence>>;
using CyclicStore = std::vector<scm::databuffer::ScmCyclicRow>;
using AcyclicStore = std::vector<scm::databuffer::ScmAcyclicRow>;

template <typename... Ts>
using StoreIndex = std::multimap<std::tuple<Ts...>, size_t>;

class CyclicResult : public scm::databuffer::ScmCyclicResultInterface
{
public:
    CyclicResult(const CyclicStore& store, const scm::databuffer::ScmCyclicRowRefs& elements);
    ~CyclicResult() override = default;

    size_t size() const override;
    const scm::databuffer::ScmCyclicRow& at(const size_t index) const override;
    scm::databuffer::ScmCyclicRowRefs::const_iterator begin() const override;
    scm::databuffer::ScmCyclicRowRefs::const_iterator end() const override;

private:
    const CyclicStore& store;       //!< Reference to the cyclic databuffer associated with the result set
    const scm::databuffer::ScmCyclicRowRefs elements;   //!< The result set (referencing elements in a databuffer)
};

class AcyclicResult : public scm::databuffer::ScmAcyclicResultInterface
{
public:
    AcyclicResult(const AcyclicStore& store, const scm::databuffer::ScmAcyclicRowRefs& elements);
    ~AcyclicResult() override = default;

    size_t size() const override;
    const scm::databuffer::ScmAcyclicRow& at(const size_t index) const override;
    scm::databuffer::ScmAcyclicRowRefs::const_iterator begin() const override;
    scm::databuffer::ScmAcyclicRowRefs::const_iterator end() const override;

private:
    const AcyclicStore& store;       //!< Reference to the acyclic databuffer associated with the result set
    const scm::databuffer::ScmAcyclicRowRefs elements;   //!< The result set (referencing elements in a databuffer)
};

/*!
 * \brief This class implements a basic version of a data buffer
 *
 * Data is stored in a simple mapping usign the timestamp and agent id as keys,
 * values are stored as key/value pairs.
 */
class BasicDataBuffer : public DataBufferInterface
{
public:
    const std::string COMPONENTNAME = "BasicDataBuffer";

//    BasicDataBuffer();
    BasicDataBuffer(const scm::RuntimeInformation *runtimeInformation);
    BasicDataBuffer(const BasicDataBuffer &) = delete;
    BasicDataBuffer(BasicDataBuffer &&) = delete;
    BasicDataBuffer &operator=(const BasicDataBuffer &) = delete;
    BasicDataBuffer &operator=(BasicDataBuffer &&) = delete;
    ~BasicDataBuffer() override = default;

    void PutCyclic(const scm::type::EntityId entityId, const Key &key, const Value &value) override;

    void PutAcyclic(const scm::type::EntityId entityId, const Key &key, const scm::databuffer::ScmAcyclic &acyclic) override;

    void PutStatic(const Key &key, const Value &value, bool persist = false) override;

    void ClearRun() override;

    void ClearTimeStep() override;

    std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> GetCyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const override;

    std::unique_ptr<scm::databuffer::ScmAcyclicResultInterface> GetAcyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const override;

    Values GetStatic(const Key &key) const override;

    Keys GetKeys(const Key &key) const override;

protected:
    StaticStore staticStore;     //!< Container for DataBuffer static values
    CyclicStore cyclicStore;     //!< Container for DataBuffer cyclic values
    AcyclicStore acyclicStore;   //!< Container for DataBuffer acyclic values

    StoreIndex<scm::type::EntityId> entityIdIndex; //!< Index for entity id based cyclics access

private:
    template <typename... Ts>
    std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> GetIndexed(const std::optional<scm::type::EntityId> entityId, const Tokens &tokens) const;

    std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> GetCyclic(const Key& key) const;
    std::unique_ptr<scm::databuffer::ScmAcyclicResultInterface> GetAcyclic(const Key& key) const;
    scm::databuffer::CyclicRows GetStatic(const Tokens &tokens) const;
    Keys GetStaticKeys(const Tokens &tokens) const;
};

} // namespace scm