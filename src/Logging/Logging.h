/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>

class Logging
{
public:
  enum class LogLevel
  {
    trace = 0,
    debug,
    info,
    warning,
    error,
    critical
  };

  static void Log(LogLevel level, const std::string& message);
  static void Trace(const std::string& message);
  static void Debug(const std::string& message);
  static void Info(const std::string& message);
  static void Warning(const std::string& message);
  static void Error(const std::string& message);
  static void Critical(const std::string& message);
};
