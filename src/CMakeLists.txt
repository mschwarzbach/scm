################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(COMPONENT_NAME AlgorithmScm)
add_compile_definitions(ALGORITHM_SCM_LIBRARY)

set(SCM_PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)
set(SCM_CONTROLLER_DIR ${ROOT_DIR}/src)
set(SCM_LOGGING_DIR ${SCM_CONTROLLER_DIR}/Logging)

add_scm_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE static COMPONENT core

  HEADERS
    ${ROOT_DIR}/include/Logging/PublisherInterface.h
    ${ROOT_DIR}/include/Logging/DataBufferInterface.h
    ${ROOT_DIR}/include/common/AreaOfInterest.h
    ${ROOT_DIR}/include/common/ScmRuntimeInformation.h
    ${ROOT_DIR}/include/common/CommonHelper.h
    ${ROOT_DIR}/include/common/CommonTrafficLights.h
    ${ROOT_DIR}/include/common/CommonTrafficSigns.h
    ${ROOT_DIR}/include/common/LaneMarkings.h
    ${ROOT_DIR}/include/common/Parameters.h
    ${ROOT_DIR}/include/common/StochasticDefinitions.h
    ${ROOT_DIR}/include/common/LightState.h
    ${ROOT_DIR}/include/common/SecondaryDriverTasks.h
    ${ROOT_DIR}/include/common/VehicleProperties.h
    ${ROOT_DIR}/include/common/ProfilesInterface.h
    ${ROOT_DIR}/include/common/ParameterBuilder.h
    ${ROOT_DIR}/include/common/ParameterInterface.h
    ${ROOT_DIR}/include/common/LateralInformation.h
    ${ROOT_DIR}/include/common/DynamicsInformation.h
    ${SCM_PARAMETER_DIR}/src/TrafficRules/TrafficRules.h
    ModuleController.h
    Logging/SimulationLogging/SimulationLogging.h
    Logging/SimulationLogging/DataBuffer/BasicDataBuffer.h
    Logging/SimulationLogging/DataBuffer/DataBuffer.h
    Logging/SimulationLogging/DataBuffer/DataBufferBinding.h
    Logging/SimulationLogging/DataBuffer/DataBufferLibrary.h
    Logging/SimulationLogging/DataPublisher/DataPublisher.h
    Logging/SimulationLogging/ScmObserver/Cyclics.h
    Logging/SimulationLogging/ScmObserver/LogConstants.h
    Logging/SimulationLogging/ScmObserver/ScmFileHandler.h
    Logging/SimulationLogging/ScmObserver/ScmObserver.h
    Logging/SimulationLogging/Utils/SimulationLoggingUtils.h
    Logging/Logging.h
    ParameterProcessing/FmiParameterProcessing.h
    ParameterProcessing/DefaultParameter.h

  SOURCES
    ModuleController.cpp
    Logging/SimulationLogging/SimulationLogging.cpp
    Logging/SimulationLogging/DataBuffer/BasicDataBuffer.cpp
    Logging/SimulationLogging/DataBuffer/DataBuffer.cpp
    Logging/SimulationLogging/DataBuffer/DataBufferBinding.cpp
    Logging/SimulationLogging/DataBuffer/DataBufferLibrary.cpp
    Logging/SimulationLogging/DataPublisher/DataPublisher.cpp
    Logging/SimulationLogging/ScmObserver/Cyclics.cpp
    Logging/SimulationLogging/ScmObserver/ScmFileHandler.cpp
    Logging/SimulationLogging/ScmObserver/ScmObserver.cpp
    Logging/Logging.cpp
    ${ROOT_DIR}/include/common/Parameters.cpp


  INCDIRS
    ${ROOT_DIR}
    ${ROOT_DIR}/include
    ${ROOT_DIR}/include/Logging
    ${ROOT_DIR}/module/parameterParser/include
    ${ROOT_DIR}/module/sensor/include
    ${ROOT_DIR}/module/driver/include
    ${ROOT_DIR}/module/longitudinalController/include
    ${ROOT_DIR}/module/lateralController/include
    ${ROOT_DIR}/module/dynamicsModel/include
    ${ROOT_DIR}/module/driver/src
    ${ROOT_DIR}/module/parameterParser/src/Signals
    ${ROOT_DIR}/module/sensor/src/Signals


  LIBRARIES
    ScmParameterParser
    ScmSensor
    ScmDriver
    ScmLongitudinalController
    ScmLateralController
    ScmDynamicsModel
    Stochastics::Stochastics
    OsiQueryLibrary

  LINKOSI static
)

include("FMU/cmake/FmuBuild.cmake")

pack_fmu(TARGET ${COMPONENT_NAME}
        GLUE_CODE ScmFmu.cpp
)
