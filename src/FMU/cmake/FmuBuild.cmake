################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
function(pack_fmu)
  cmake_parse_arguments(PARSED_ARG "" "TARGET" "GLUE_CODE" ${ARGN})

  set(PUBLIC_LOGGING ON CACHE BOOL "Enable logging via FMI logger")
  set(PRIVATE_LOGGING ON CACHE BOOL "Enable private logging to file")
  if (WIN32)
      set(PRIVATE_LOG_PATH "C:/TEMP/${PROJECT_NAME}.log" CACHE FILEPATH "Path to write private log file to")
  else ()
      set(PRIVATE_LOG_PATH "/tmp/${PROJECT_NAME}.log" CACHE FILEPATH "Path to write private log file to")
  endif ()
  set(VERBOSE_FMI_LOGGING ON CACHE BOOL "Enable detailed FMI function logging")
  set(DEBUG_BREAKS OFF CACHE BOOL "Enable debugger traps for debug builds of FMU")
  set(CMAKE_POSITION_INDEPENDENT_CODE ON)
  
  string(TIMESTAMP FMUTIMESTAMP UTC)
  string(MD5 FMUGUID modelDescription.in.xml)
  set(OSIVERSION "3.6.0")
  set(OSMPVERSION "1.3.0")

  add_library(${PARSED_ARG_TARGET}Fmu SHARED
    ${PROJECT_SOURCE_DIR}/src/FMU/src/OsmpShell/OsmpShell.cpp
    ${PROJECT_SOURCE_DIR}/src/FMU/src/util/fmu_address_helper.cpp
    ${PARSED_ARG_GLUE_CODE}
  )

  set_target_properties(${PARSED_ARG_TARGET}Fmu PROPERTIES PREFIX "")
  set_target_properties(${PARSED_ARG_TARGET}Fmu PROPERTIES INSTALL_RPATH "\$ORIGIN")
  set_target_properties(${PARSED_ARG_TARGET}Fmu PROPERTIES BUILD_RPATH "\$ORIGIN")

  get_target_property(TARGET_INCLUDES ${PARSED_ARG_TARGET} INCLUDE_DIRECTORIES)

  target_include_directories(${PARSED_ARG_TARGET}Fmu
    PRIVATE
      ${PROJECT_SOURCE_DIR}/src/FMU/include
      ${PROJECT_SOURCE_DIR}/src/FMU/src/OsmpShell
      ${PROJECT_SOURCE_DIR}/src/FMU/src/util
      ${TARGET_INCLUDES}
  )
  
  set_property(TARGET ${PARSED_ARG_TARGET} PROPERTY POSITION_INDEPENDENT_CODE ON)
  target_compile_definitions(${PARSED_ARG_TARGET}Fmu PRIVATE "FMU_SHARED_OBJECT")
  target_compile_definitions(${PARSED_ARG_TARGET}Fmu PRIVATE "FMU_GUID=\"${FMUGUID}\"")
  
  get_target_property(TARGET_LIBS ${PARSED_ARG_TARGET} LINK_LIBRARIES)

  target_link_libraries(${PARSED_ARG_TARGET}Fmu
    PUBLIC
      ${PARSED_ARG_TARGET}
      ${TARGET_LIBS}
      open_simulation_interface::open_simulation_interface_pic
  )

  foreach(TARGET_LIB ${TARGET_LIBS})
    get_target_property(TARGET_TYPE ${TARGET_LIB} TYPE)
    if("${TARGET_TYPE}" STREQUAL "SHARED_LIBRARY")
      list(APPEND TARGET_LIB_FILES "$<TARGET_FILE:${TARGET_LIB}>")
    endif()
  endforeach()

  list(APPEND TARGET_LIB_FILES "$<TARGET_FILE:${PARSED_ARG_TARGET}>")

  if (PRIVATE_LOGGING)
      file(TO_NATIVE_PATH ${PRIVATE_LOG_PATH} PRIVATE_LOG_PATH_NATIVE)
      string(REPLACE "\\" "\\\\" PRIVATE_LOG_PATH_ESCAPED ${PRIVATE_LOG_PATH_NATIVE})
      target_compile_definitions(${PARSED_ARG_TARGET}Fmu PRIVATE
              "PRIVATE_LOG_PATH=\"${PRIVATE_LOG_PATH_ESCAPED}\"")
  endif ()
  
  target_compile_definitions(${PARSED_ARG_TARGET}Fmu PRIVATE
      $<$<BOOL:${PUBLIC_LOGGING}>:PUBLIC_LOGGING>
      $<$<BOOL:${VERBOSE_FMI_LOGGING}>:VERBOSE_FMI_LOGGING>
      $<$<BOOL:${DEBUG_BREAKS}>:DEBUG_BREAKS>)
  
  if (WIN32)
    if (CMAKE_SIZEOF_VOID_P EQUAL 8)
      set(FMI_BINARIES_PLATFORM "win64")
    else ()
      set(FMI_BINARIES_PLATFORM "win32")
    endif ()
  elseif (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    if (CMAKE_SIZEOF_VOID_P EQUAL 8)
      set(FMI_BINARIES_PLATFORM "linux64")
    else ()
      set(FMI_BINARIES_PLATFORM "linux32")
    endif ()
  elseif (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    if (CMAKE_SIZEOF_VOID_P EQUAL 8)
      set(FMI_BINARIES_PLATFORM "darwin64")
    else ()
      set(FMI_BINARIES_PLATFORM "darwin32")
    endif ()
  endif ()

  get_target_property(FMUSHELL_SOURCE_FILES ${PARSED_ARG_TARGET} SOURCES)
  get_target_property(FMU_SOURCE_FILES ${PARSED_ARG_TARGET}Fmu SOURCES)
  list(APPEND FMU_SOURCE_FILES ${FMUSHELL_SOURCE_FILES})
  list(FILTER FMU_SOURCE_FILES INCLUDE REGEX "\.cpp$")

  foreach(FMU_SOURCE_FILE ${FMU_SOURCE_FILES})
    get_filename_component(FMU_SOURCE_FILE_ABSOLUTE "${FMU_SOURCE_FILE}" ABSOLUTE)
    file(RELATIVE_PATH FMU_SOURCE_FILE_RELATIVE "${PROJECT_SOURCE_DIR}" "${FMU_SOURCE_FILE_ABSOLUTE}")
    list(APPEND FMU_SOURCE_FILES_XML "${FMU_SOURCE_FILE_RELATIVE}")
  endforeach()

  list(TRANSFORM FMU_SOURCE_FILES_XML PREPEND "      <File name=\"")
  list(TRANSFORM FMU_SOURCE_FILES_XML APPEND "\"/>")
  string(JOIN "\n" FMU_SOURCE_FILES_XML ${FMU_SOURCE_FILES_XML})

  configure_file(modelDescription.in.xml modelDescription.xml @ONLY)

  add_custom_command(
    POST_BUILD
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PARSED_ARG_TARGET}.fmu
    DEPENDS ${PARSED_ARG_TARGET}Fmu
    COMMAND ${CMAKE_COMMAND} -E remove_directory "${CMAKE_CURRENT_BINARY_DIR}/buildfmu"
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_CURRENT_BINARY_DIR}/buildfmu"
    COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/modelDescription.xml" "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/"
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/binaries/${FMI_BINARIES_PLATFORM}"
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${PARSED_ARG_TARGET}Fmu> "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/binaries/${FMI_BINARIES_PLATFORM}"
    COMMAND ${CMAKE_COMMAND} -E copy ${TARGET_LIB_FILES} "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/binaries/${FMI_BINARIES_PLATFORM}"
    COMMAND ${CMAKE_COMMAND} -E chdir "${CMAKE_CURRENT_BINARY_DIR}/buildfmu" ${CMAKE_COMMAND}
                             -E tar "cfv" "${CMAKE_CURRENT_BINARY_DIR}/${PARSED_ARG_TARGET}.fmu"
                               --format=zip "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/modelDescription.xml"
                               "${CMAKE_CURRENT_BINARY_DIR}/buildfmu/binaries/${FMI_BINARIES_PLATFORM}"
  )

  add_custom_target(${PARSED_ARG_TARGET}_packfmu
    ALL
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PARSED_ARG_TARGET}.fmu
  )

  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PARSED_ARG_TARGET}.fmu DESTINATION .)
endfunction()
