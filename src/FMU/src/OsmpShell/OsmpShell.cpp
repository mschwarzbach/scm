/*
 * Author: Daniel Becker
 * Based on: PMSF FMU Framework for FMI 2.0 Co-Simulation FMUs
 *
 * (C) 2019 Institute for Automotive Engineering, RWTH Aachen Univ.
 * (C) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "OsmpShell.h"

#include "fmi2FunctionTypes.h"

/*
 * Debug Breaks
 *
 * If you define DEBUG_BREAKS the FMU will automatically break
 * into an attached Debugger on all major computation functions.
 * Note that the FMU is likely to break all environments if no
 * Debugger is actually attached when the breaks are triggered.
 */
#if defined(DEBUG_BREAKS) && !defined(NDEBUG)
#if defined(__has_builtin) && !defined(__ibmxl__)
#if __has_builtin(__builtin_debugtrap)
#define DEBUGBREAK() __builtin_debugtrap()
#elif __has_builtin(__debugbreak)
#define DEBUGBREAK() __debugbreak()
#endif
#endif
#if !defined(DEBUGBREAK)
#if defined(_MSC_VER) || defined(__INTEL_COMPILER)
#include <intrin.h>
#define DEBUGBREAK() __debugbreak()
#else
#include <signal.h>
#if defined(SIGTRAP)
#define DEBUGBREAK() raise(SIGTRAP)
#else
#define DEBUGBREAK() raise(SIGABRT)
#endif
#endif
#endif
#else
#define DEBUGBREAK()
#endif

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <mutex>
#include <string>

//#include "Environment/EnvironmentBuilder.hh"
//#include "Path/GraphBuilder.hh"
//#include "Path/Graph.hh"
//#include "Environment/Environment.hh"
#include <memory>

#include "fmu_address_helper.h"

/// Mutex for controlling access to the variables below (for changing them in a
/// mutable manner)
std::mutex sEnvironmentMutex;
/// Pointer to an course environment shared by multiple pedestrians
// TODO: This should be made const
// static std::shared_ptr<Env::Environment> sEnvironment(nullptr);
/// Pointer to a strategy level graph shared by multiple pedestrians
// static Path::GraphBuilder::StrategyGraphPtr sStrategyGraph(nullptr);
/**
 * The serialized form of the last ground truth init received. The idea here:
 * multiple instances of this FMU ideally should share various data structures,
 * which are based only on the ground truth received during initialization.
 * To do this, the ground truth is cached. If an instance is initialized with
 * the same ground truth received previously, it can reuse the last Environment
 * and the last StrategyGraph.
 */
static std::vector<char> sCachedGroundTruthInit;

namespace OsmpShell
{

#ifdef PRIVATE_LOG_PATH
std::ofstream FmiConnector::private_log_file;
#endif

FmiConnector::Logger::Logger(FmiConnector::Logger::LogFunction logFunction)
    : logFunction{logFunction}
{
}

void FmiConnector::Logger::Log(FmiLogInterface::LogLevel logLevel, const std::string& message)
{
  logFunction("Logic", message.data());
}

/*
 * ProtocolBuffer Accessors
 */

void FmiConnector::refresh_fmi_sensor_view_config_request()
{
  osi3::SensorViewConfiguration config;
  if (get_fmi_sensor_view_config(config))
    set_fmi_sensor_view_config_request(config);
  else
  {
    config.Clear();
    config.mutable_version()->CopyFrom(osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
    // We want a full view of the environment, in all directions
    config.set_field_of_view_horizontal(2 * M_PI);
    config.set_field_of_view_vertical(2 * M_PI);
    // 400m view. This primarily needs to be far enough to react to vehicles early enough
    config.set_range(400);
    // update rate: 50Hz (20ms). Must be identical to stepSize in the
    // modelDescription.xml.
    // Note: the model doesn't care that much about the exact cycle time.
    config.mutable_update_cycle_time()->set_seconds(0);
    config.mutable_update_cycle_time()->set_nanos(20000000);
    config.mutable_update_cycle_offset()->Clear();
    // The model doesn't need any road information from the sensor view,
    // since it already has this information from the GroundTruthInit
    // message. This can greatly improve performance.
    // config.set_omit_static_information(true);
    set_fmi_sensor_view_config_request(config);
  }
}

bool FmiConnector::get_fmi_sensor_view_config(osi3::SensorViewConfiguration& data)
{
  if (integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX] <= 0)
    return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX]);
  normal_log("OSMP", "Got %08X %08X, reading %d bytes from %p to get SensorViewConfiguration ...", integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX], buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize SensorViewConfiguration");
    return false;
  }
  return true;
}

bool FmiConnector::get_fmi_sensor_view_in(osi3::SensorView& data)
{
  if (integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX] <= 0)
    return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASELO_IDX]);
  normal_log("OSMP", "Got %08X %08X, reading %d bytes from %p to get SensorView ...", integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASELO_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX], buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize SensorView");
    return false;
  }
  return true;
}

bool FmiConnector::get_fmi_traffic_command_in(osi3::TrafficCommand& data)
{
  if (integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX] <= 0)
    return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASEHI_IDX], integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASELO_IDX]);
  normal_log("OSMP", "Got %08X %08X, reading %d bytes from %p to get TrafficCommand ...", integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASEHI_IDX], integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASELO_IDX], integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX], buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize TrafficCommand");
    return false;
  }
  return true;
}

/**
 * Serializes to the given buffer.
 * @return true if successful.
 */
template <typename T>
static bool
serializeToVector(std::vector<char>& buffer, const T& data)
{
#if GOOGLE_PROTOBUF_VERSION >= 3004000
  buffer.resize(data.ByteSizeLong());
#else
  buffer.resize(static_cast<int>(data.ByteSize()));
#endif
  return data.SerializeToArray(buffer.data(), static_cast<int>(buffer.size()));
}

void FmiConnector::set_fmi_traffic_update_out(const osi3::TrafficUpdate& data)
{
  auto res = serializeToVector(currentOutputBuffer, data);
  encode_pointer_to_integer(currentOutputBuffer.data(), integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX], integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX]);
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = (fmi2Integer)currentOutputBuffer.size();
  normal_log("OSMP", "Serialization %d, providing %08X %08X, writing %d bytes from %p to set TrafficUpdate ...", res, integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX], integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX], integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX], currentOutputBuffer.data());
  using std::swap;
  swap(currentOutputBuffer, lastOutputBuffer);
}

void FmiConnector::set_fmi_sensor_view_config_request(const osi3::SensorViewConfiguration& data)
{
  auto res = serializeToVector(currentConfigRequestBuffer, data);
  encode_pointer_to_integer(currentConfigRequestBuffer.data(), integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX]);
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = (fmi2Integer)currentConfigRequestBuffer.size();
  normal_log("OSMP", "Serialization %d, providing %08X %08X, writing %d bytes from %p to set SensorViewConfigurationRequest ...", res, integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX], integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX], currentConfigRequestBuffer.data());
  using std::swap;
  swap(currentConfigRequestBuffer, lastConfigRequestBuffer);
}

void FmiConnector::reset_fmi_traffic_update_out()
{
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX] = 0;
}

void FmiConnector::reset_fmi_sensor_view_config_request()
{
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX] = 0;
}

/*
 * Actual Core Content
 */

fmi2Status FmiConnector::doInit()
{
  DEBUGBREAK();

  /* Booleans */
  normal_log("OSMP", "boolean_vars: %p", boolean_vars);
  for (size_t i = 0; i < FMI_BOOLEAN_VARS; i++)
    boolean_vars[i] = fmi2False;

  /* Integers */
  normal_log("OSMP", "integer_vars: %p", integer_vars);
  for (size_t i = 0; i < FMI_INTEGER_VARS; i++)
    integer_vars[i] = 0;

  /* Reals */
  normal_log("OSMP", "real_vars: %p", real_vars);
  for (size_t i = 0; i < FMI_REAL_VARS; i++)
    real_vars[i] = 0.0;

  /* Strings */
  normal_log("OSMP", "string_vars: %p", string_vars);
  for (size_t i = 0; i < FMI_STRING_VARS; i++)
    string_vars[i] = "";

  return fmi2OK;
}

fmi2Status FmiConnector::doInitParameters()
{
  for (const auto [name, idx] : boolParameterIndicesByName)
  {
    parameters.emplace(name, boolean_vars[idx]);
  }

  for (const auto [name, idx] : intParameterIndicesByName)
  {
    parameters.emplace(name, integer_vars[idx]);
  }

  for (const auto [name, idx] : realParameterIndicesByName)
  {
    parameters.emplace(name, real_vars[idx]);
  }

  for (const auto [name, idx] : stringParameterIndicesByName)
  {
    parameters.emplace(name, string_vars[idx]);
  }

  return fmi2OK;
}

fmi2Status FmiConnector::doStart(fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime)
{
  DEBUGBREAK();
  (void)toleranceDefined;
  (void)tolerance;
  (void)startTime;
  (void)stopTimeDefined;
  (void)stopTime;

  return fmi2OK;
}

fmi2Status FmiConnector::doEnterInitializationMode()
{
  DEBUGBREAK();

  return fmi2OK;
}

fmi2Status FmiConnector::doExitInitializationMode()
{
  DEBUGBREAK();

  if (auto status = doInitParameters(); status != fmi2OK)
  {
    return status;
  }

  if (integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_SIZE_IDX] <= 0)
  {
    normal_log("OSMP", "Did not receive a GroundTruthInit message");
    return fmi2Error;
  }

  {
    const size_t dataSize = static_cast<size_t>(integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_SIZE_IDX]);
    void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_BASEHI_IDX],
                                             integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_BASELO_IDX]);

    std::lock_guard<std::mutex> guard{sEnvironmentMutex};
    if (dataSize == sCachedGroundTruthInit.size() && memcmp(buffer, sCachedGroundTruthInit.data(), sCachedGroundTruthInit.size()) == 0)
    {
      normal_log("OSI", "Received identical configuration than previous version - everything OK");
    }
    else
    {
      if (sCachedGroundTruthInit.empty())
        normal_log("OSI", "Received new ground truth initialization");
      else
        normal_log("OSI", "Received different ground truth initialization - if you didn't load a different map, this is probably a bug in the environment");

      sCachedGroundTruthInit.resize(dataSize);
      memcpy(sCachedGroundTruthInit.data(), buffer, dataSize);

      bool ok = initGroundTruth.ParseFromArray(buffer, static_cast<int>(dataSize));
      if (!ok)
      {
        normal_log("OSI", "Could not deserialize GroundTruthInit message");
        return fmi2Error;
      }
    }
  }

  return fmi2OK;
}

fmi2Status FmiConnector::doCalc(fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  DEBUGBREAK();
  (void)noSetFMUStatePriorToCurrentPointfmi2Component;

  osi3::TrafficCommand currentCommandIn;
  osi3::TrafficUpdate currentOut;
  double time = currentCommunicationPoint;
  normal_log("OSI", "Calculating Trajectory Agent at %g for %g (step size %g)", currentCommunicationPoint, time, communicationStepSize);

  std::swap(currentSensorView, previousSensorView);
  std::swap(currentTrafficCommand, previousTrafficCommand);
  std::swap(currentTrafficUpdate, previousTrafficUpdate);

  if (!get_fmi_sensor_view_in(currentSensorView))
  {
    normal_log("OSI", "No valid sensor view input, therefore providing no valid output.");
    reset_fmi_traffic_update_out();
    set_fmi_valid(false);
    return fmi2OK;
  }

  if (!get_fmi_traffic_command_in(currentTrafficCommand))
  {
    normal_log("OSI", "No valid command input, therefore providing no valid output.");
    reset_fmi_traffic_update_out();
    set_fmi_valid(false);
    return fmi2OK;
  }

  /* Clear Output */
  currentTrafficUpdate.Clear();
  currentTrafficUpdate.mutable_version()->CopyFrom(osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
  /* Adjust Timestamp */
  currentTrafficUpdate.mutable_timestamp()->set_seconds((long long int)floor(time));
  currentTrafficUpdate.mutable_timestamp()->set_nanos((int)((time - floor(time)) * 1000000000.0));

  /* Determine Our Vehicle ID and copy external state */
  osi3::Identifier ego_id = currentSensorView.global_ground_truth().host_vehicle_id();
  normal_log("OSI", "Looking for EgoVehicle with ID: %llu", static_cast<unsigned long long>(ego_id.value()));
  std::for_each(
      currentSensorView.global_ground_truth().moving_object().begin(),
      currentSensorView.global_ground_truth().moving_object().end(),
      [this, ego_id](const osi3::MovingObject& obj)
      {
        if (obj.id().value() == ego_id.value())
        {
          normal_log("OSI", "Found EgoVehicle with ID: %llu", static_cast<unsigned long long>(obj.id().value()));
          currentTrafficUpdate.add_update()->CopyFrom(obj);
        }
      });

  if (currentTrafficUpdate.update_size() < 1)
  {
    normal_log("OSI", "Unable to find EgoVehicle in GroundTruth");
    return fmi2OK;
  }

  auto status = StepFmu(currentSensorView, currentTrafficCommand, currentTrafficUpdate);

  /* Serialize */
  set_fmi_traffic_update_out(currentTrafficUpdate);
  set_fmi_valid(status == fmi2OK);
  if (status != fmi2OK)
    std::cout << "Invalid status in OsmpShell doCalc" << std::endl;

  return status;
}

fmi2Status FmiConnector::doTerm()
{
  DEBUGBREAK();

  return fmi2OK;
}

void FmiConnector::doFree()
{
  DEBUGBREAK();
}

/*
 * Generic C++ Wrapper Code
 */

FmiConnector::FmiConnector(fmi2String theinstanceName, fmi2Type thefmuType, fmi2String thefmuGUID, fmi2String thefmuResourceLocation, const fmi2CallbackFunctions* thefunctions, fmi2Boolean thevisible, fmi2Boolean theloggingOn)
    : instanceName(theinstanceName),
      fmuType(thefmuType),
      fmuGUID(thefmuGUID),
      fmuResourceLocation(thefmuResourceLocation),
      visible(!!thevisible),
      loggingOn(!!theloggingOn),
      functions(*thefunctions),
      simulationStarted(false),
      sensorViewConfigRequestNeedsRefresh(true)
{
  loggingCategories.clear();
  loggingCategories.insert("FMI");
  loggingCategories.insert("OSMP");
  loggingCategories.insert("OSI");
  loggingCategories.insert("Logic");

  logger = std::make_shared<Logger>(
      [this](const char* category, const char* message)
      {
        normal_log(category, message);
      });
}

fmi2Component FmiConnector::ValidateInstance()
{
  if (doInit() != fmi2OK)
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = NULL (doInit failure)",
                           instanceName.data(),
                           fmuType,
                           fmuGUID.data(),
                           fmuResourceLocation.data(),
                           "FUNCTIONS",
                           visible,
                           loggingOn);
    return NULL;
  }
  else
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = %p",
                           instanceName.data(),
                           fmuType,
                           fmuGUID.data(),
                           fmuResourceLocation.data(),
                           "FUNCTIONS",
                           visible,
                           loggingOn,
                           this);
    return (fmi2Component)this;
  }
}

FmiConnector::~FmiConnector()
{
}

fmi2Status FmiConnector::SetDebugLogging(fmi2Boolean theloggingOn, size_t nCategories, const fmi2String categories[])
{
  fmi_verbose_log("fmi2SetDebugLogging(%s)", theloggingOn ? "true" : "false");
  loggingOn = theloggingOn ? true : false;
  if (categories && (nCategories > 0))
  {
    loggingCategories.clear();
    for (size_t i = 0; i < nCategories; i++)
    {
      if (strcmp(categories[i], "FMI") == 0)
        loggingCategories.insert("FMI");
      else if (strcmp(categories[i], "OSMP") == 0)
        loggingCategories.insert("OSMP");
      else if (strcmp(categories[i], "OSI") == 0)
        loggingCategories.insert("OSI");
      else if (strcmp(categories[i], "Logic") == 0)
        loggingCategories.insert("Logic");
    }
  }
  else
  {
    loggingCategories.clear();
    loggingCategories.insert("FMI");
    loggingCategories.insert("OSMP");
    loggingCategories.insert("OSI");
    loggingCategories.insert("Logic");
  }
  return fmi2OK;
}

fmi2Status FmiConnector::SetupExperiment(fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime)
{
  fmi_verbose_log("fmi2SetupExperiment(%d,%g,%g,%d,%g)", toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  return doStart(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
}

fmi2Status FmiConnector::EnterInitializationMode()
{
  fmi_verbose_log("fmi2EnterInitializationMode()");
  return doEnterInitializationMode();
}

fmi2Status FmiConnector::ExitInitializationMode()
{
  fmi_verbose_log("fmi2ExitInitializationMode()");
  simulationStarted = true;
  auto status = doExitInitializationMode();

  if (status != fmi2OK)
  {
    normal_log("FMI", "doExitInitializationMode failed");
    return status;
  }

  normal_log("FMI", "calling InitFmu");
  status = InitFmu(parameters, initGroundTruth, *logger.get());

  if (status != fmi2OK)
  {
    normal_log("FMI", "InitFmu failed");
  }

  return status;
}

fmi2Status FmiConnector::DoStep(fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  fmi_verbose_log("fmi2DoStep(%g,%g,%d)", currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
  return doCalc(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
}

fmi2Status FmiConnector::Terminate()
{
  fmi_verbose_log("fmi2Terminate()");
  return doTerm();
}

fmi2Status FmiConnector::Reset()
{
  fmi_verbose_log("fmi2Reset()");

  doFree();
  simulationStarted = false;
  return doInit();
}

void FmiConnector::FreeInstance()
{
  fmi_verbose_log("fmi2FreeInstance()");
  doFree();
}

fmi2Status FmiConnector::GetReal(const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
  fmi_verbose_log("fmi2GetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
      value[i] = real_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::GetInteger(const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
{
  fmi_verbose_log("fmi2GetInteger(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_INTEGER_VARS)
    {
      if (sensorViewConfigRequestNeedsRefresh && (vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX))
      {
        refresh_fmi_sensor_view_config_request();
        sensorViewConfigRequestNeedsRefresh = false;
      }
      value[i] = integer_vars[vr[i]];
    }
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::GetBoolean(const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
{
  fmi_verbose_log("fmi2GetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      value[i] = boolean_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::GetString(const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
{
  fmi_verbose_log("fmi2GetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      value[i] = string_vars[vr[i]].c_str();
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::SetReal(const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
{
  fmi_verbose_log("fmi2SetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
      real_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::SetInteger(const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
{
  fmi_verbose_log("fmi2SetInteger(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    // log("OSMP", "SetInteger: integer_vars[%d]=%d", vr[i], value[i]);
    if (vr[i] < FMI_INTEGER_VARS)
      integer_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::SetBoolean(const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
{
  fmi_verbose_log("fmi2SetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      boolean_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status FmiConnector::SetString(const fmi2ValueReference vr[], size_t nvr, const fmi2String value[])
{
  fmi_verbose_log("fmi2SetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      string_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

/*
 * FMI 2.0 Co-Simulation Interface API
 */

extern "C"
{
  FMI2_Export const char* fmi2GetTypesPlatform()
  {
    return fmi2TypesPlatform;
  }

  FMI2_Export const char* fmi2GetVersion()
  {
    return fmi2Version;
  }

  FMI2_Export fmi2Status fmi2SetDebugLogging(fmi2Component c, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetDebugLogging(loggingOn, nCategories, categories);
  }

  /*
   * Functions for Co-Simulation
   */

  FMI2_Export fmi2Status fmi2SetupExperiment(fmi2Component c,
                                             fmi2Boolean toleranceDefined,
                                             fmi2Real tolerance,
                                             fmi2Real startTime,
                                             fmi2Boolean stopTimeDefined,
                                             fmi2Real stopTime)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetupExperiment(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  }

  FMI2_Export fmi2Status fmi2EnterInitializationMode(fmi2Component c)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->EnterInitializationMode();
  }

  FMI2_Export fmi2Status fmi2ExitInitializationMode(fmi2Component c)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->ExitInitializationMode();
  }

  FMI2_Export fmi2Status fmi2DoStep(fmi2Component c,
                                    fmi2Real currentCommunicationPoint,
                                    fmi2Real communicationStepSize,
                                    fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->DoStep(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
  }

  FMI2_Export fmi2Status fmi2Terminate(fmi2Component c)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->Terminate();
  }

  FMI2_Export fmi2Status fmi2Reset(fmi2Component c)
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->Reset();
  }

  /*
   * Data Exchange Functions
   */
  FMI2_Export fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->GetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->GetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->GetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->GetString(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2String value[])
  {
    FmiConnector* myc = (FmiConnector*)c;
    return myc->SetString(vr, nvr, value);
  }

  /*
   * Unsupported Features (FMUState, Derivatives, Async DoStep, Status Enquiries)
   */
  FMI2_Export fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t* size)
  {
    (void)c;
    (void)FMUstate;
    (void)size;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate, fmi2Byte serializedState[], size_t size)
  {
    (void)c;
    (void)FMUstate;
    (void)serializedState;
    (void)size;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2DeSerializeFMUstate(fmi2Component c, const fmi2Byte serializedState[], size_t size, fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)serializedState;
    (void)size;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetDirectionalDerivative(fmi2Component c,
                                                      const fmi2ValueReference vUnknown_ref[],
                                                      size_t nUnknown,
                                                      const fmi2ValueReference vKnown_ref[],
                                                      size_t nKnown,
                                                      const fmi2Real dvKnown[],
                                                      fmi2Real dvUnknown[])
  {
    (void)c;
    (void)vUnknown_ref;
    (void)nUnknown;
    (void)vKnown_ref;
    (void)nKnown;
    (void)dvKnown;
    (void)dvUnknown;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetRealInputDerivatives(fmi2Component c,
                                                     const fmi2ValueReference vr[],
                                                     size_t nvr,
                                                     const fmi2Integer order[],
                                                     const fmi2Real value[])
  {
    (void)c;
    (void)vr;
    (void)nvr;
    (void)order;
    (void)value;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c,
                                                      const fmi2ValueReference vr[],
                                                      size_t nvr,
                                                      const fmi2Integer order[],
                                                      fmi2Real value[])
  {
    (void)c;
    (void)vr;
    (void)nvr;
    (void)order;
    (void)value;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2CancelStep(fmi2Component c)
  {
    (void)c;
    return fmi2OK;
  }

  FMI2_Export fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }
}
}  // namespace OsmpShell
