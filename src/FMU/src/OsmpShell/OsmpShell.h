/*
 * Adaptions for Pedestrians: Thomas Bleher and Fabian Pfeuffer
 * Author for OSMP Traffic Agents: Daniel Becker
 * Based on: PMSF FMU Framework for FMI 2.0 Co-Simulation FMUs
 *
 * (C) 2019 Institute for Automotive Engineering, RWTH Aachen Univ.
 * (C) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// #ifndef FMU_SHARED_OBJECT
// #define FMI2_FUNCTION_PREFIX FmuShell_
// #endif
#include <osi3/osi_groundtruth.pb.h>
#include "fmi2Functions.h"
#include "FmiLogInterface.h"
#include "FmiParameterInterface.h"

#include <cstddef>

/*
 * Logging Control
 *
 * Logging is controlled via three definitions:
 *
 * - If PRIVATE_LOG_PATH is defined it gives the name of a file
 *   that is to be used as a private log file.
 * - If PUBLIC_LOGGING is defined then we will (also) log to
 *   the FMI logging facility where appropriate.
 * - If VERBOSE_FMI_LOGGING is defined then logging of basic
 *   FMI calls is enabled, which can get very verbose.
 */

/*
 * Variable Definitions
 *
 * Define FMI_*_LAST_IDX to the zero-based index of the last variable
 * of the given type (0 if no variables of the type exist).  This
 * ensures proper space allocation, initialisation and handling of
 * the given variables in the template code.  Optionally you can
 * define FMI_TYPENAME_VARNAME_IDX definitions (e.g. FMI_REAL_MYVAR_IDX)
 * to refer to individual variables inside your code, or for example
 * FMI_REAL_MYARRAY_OFFSET and FMI_REAL_MYARRAY_SIZE definitions for
 * array variables.
 */

/* Boolean Variables */
static constexpr size_t FMI_BOOLEAN_VALID_IDX = 0;
static constexpr size_t FMI_BOOLEAN_DRIVER_IDEALPERCEPTION_IDX = 1;
static constexpr size_t FMI_BOOLEAN_DRIVER_ENABLEHIGHCOGNITIVEMODE_IDX = 2;
static constexpr size_t FMI_BOOLEAN_LAST_IDX = FMI_BOOLEAN_DRIVER_ENABLEHIGHCOGNITIVEMODE_IDX;
static constexpr size_t FMI_BOOLEAN_VARS = FMI_BOOLEAN_LAST_IDX + 1;

/* Integer Variables */
static constexpr size_t FMI_INTEGER_GROUNDTRUTHINIT_IN_BASELO_IDX = 0;
static constexpr size_t FMI_INTEGER_GROUNDTRUTHINIT_IN_BASEHI_IDX = 1;
static constexpr size_t FMI_INTEGER_GROUNDTRUTHINIT_IN_SIZE_IDX = 2;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_BASELO_IDX = 3;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_BASEHI_IDX = 4;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX = 5;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX = 6;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX = 7;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX = 8;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX = 9;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX = 10;
static constexpr size_t FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX = 11;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMAND_IN_BASELO_IDX = 12;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMAND_IN_BASEHI_IDX = 13;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX = 14;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMANDUPDATE_OUT_BASELO_IDX = 15;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMANDUPDATE_OUT_BASEHI_IDX = 16;
static constexpr size_t FMI_INTEGER_TRAFFICCOMMANDUPDATE_OUT_SIZE_IDX = 17;
static constexpr size_t FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX = 18;
static constexpr size_t FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX = 19;
static constexpr size_t FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX = 20;
static constexpr size_t FMI_INTEGER_SIMULATION_RANDOMSEED_IDX = 21;
static constexpr size_t FMI_INTEGER_VEHICLE_NUMBEROFGEARS_IDX = 22;
static constexpr size_t FMI_INTEGER_LAST_IDX = FMI_INTEGER_VEHICLE_NUMBEROFGEARS_IDX;
static constexpr size_t FMI_INTEGER_VARS = FMI_INTEGER_LAST_IDX + 1;

/* Real Variables */
static constexpr size_t FMI_REAL_DRIVER_PREVIEWTIMEHEADWAY_IDX = 0;
static constexpr size_t FMI_REAL_DRIVER_PREVIEWDISTANCEMIN_IDX = 1;
static constexpr size_t FMI_REAL_DRIVER_THRESHOLDLOOMING_IDX = 2;
static constexpr size_t FMI_REAL_DRIVER_REACTIONBASETIME_MEAN_IDX = 3;
static constexpr size_t FMI_REAL_DRIVER_REACTIONBASETIME_SD_IDX = 4;
static constexpr size_t FMI_REAL_DRIVER_REACTIONBASETIME_MIN_IDX = 5;
static constexpr size_t FMI_REAL_DRIVER_REACTIONBASETIME_MAX_IDX = 6;
static constexpr size_t FMI_REAL_DRIVER_ANTICIPATIONQUOTA_IDX = 7;
static constexpr size_t FMI_REAL_DRIVER_AGENTCOOPERATIONFACTOR_IDX = 8;
static constexpr size_t FMI_REAL_DRIVER_AGENTSUSPICIOUSBEHAVIOUREVASIONFACTOR_IDX = 9;
static constexpr size_t FMI_REAL_DRIVER_OUTERKEEPINGINTENSITY_IDX = 10;
static constexpr size_t FMI_REAL_DRIVER_EGOLANEKEEPINGINTENSITY_IDX = 11;
static constexpr size_t FMI_REAL_DRIVER_RIGHTOVERTAKINGPROHIBITIONIGNORINGQUOTA_IDX = 12;
static constexpr size_t FMI_REAL_DRIVER_LANECHANGEPROHIBITIONIGNORINGQUOTA_IDX = 13;
static constexpr size_t FMI_REAL_DRIVER_VWISH_IDX = 14;
static constexpr size_t FMI_REAL_DRIVER_DELTAVWISH_IDX = 15;
static constexpr size_t FMI_REAL_DRIVER_DELTAVVIOLATION_MEAN_IDX = 16;
static constexpr size_t FMI_REAL_DRIVER_DELTAVVIOLATION_SD_IDX = 17;
static constexpr size_t FMI_REAL_DRIVER_DELTAVVIOLATION_MIN_IDX = 18;
static constexpr size_t FMI_REAL_DRIVER_DELTAVVIOLATION_MAX_IDX = 19;
static constexpr size_t FMI_REAL_DRIVER_DESIREDTIMEATTARGETSPEED_IDX = 20;
static constexpr size_t FMI_REAL_DRIVER_PROPORTIONALITYFACTORFORFOLLOWINGDISTANCE_IDX = 21;
static constexpr size_t FMI_REAL_DRIVER_MAXLONGITUDINALACCELERATION_IDX = 22;
static constexpr size_t FMI_REAL_DRIVER_MAXLONGITUDINALDECELERATION_IDX = 23;
static constexpr size_t FMI_REAL_DRIVER_MAXLATERALACCELERATION_IDX = 24;
static constexpr size_t FMI_REAL_DRIVER_COMFORTLONGITUDINALACCELERATION_IDX = 25;
static constexpr size_t FMI_REAL_DRIVER_COMFORTLONGITUDINALDECELERATION_IDX = 26;
static constexpr size_t FMI_REAL_DRIVER_ACCELERATIONADJUSTMENTTHRESHOLDVELOCITY_IDX = 27;
static constexpr size_t FMI_REAL_DRIVER_VELOCITYFORMAXACCELERATION_IDX = 28;
static constexpr size_t FMI_REAL_DRIVER_MAXCOMFORTACCELERATIONFACTOR_IDX = 29;
static constexpr size_t FMI_REAL_DRIVER_MAXCOMFORTDECELERATIONFACTOR_IDX = 30;
static constexpr size_t FMI_REAL_DRIVER_COMFORTLATERALACCELERATION_IDX = 31;
static constexpr size_t FMI_REAL_DRIVER_CARQUEUINGDISTANCE_IDX = 32;
static constexpr size_t FMI_REAL_DRIVER_HYSTERESISFORRESTART_IDX = 33;
static constexpr size_t FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITIONQUOTA_IDX = 34;
static constexpr size_t FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITION_IDX = 35;
static constexpr size_t FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITIONRESCUELANE_IDX = 36;
static constexpr size_t FMI_REAL_DRIVER_MAXIMUMANGULARVELOCITYSTEERINGWHEEL_IDX = 37;
static constexpr size_t FMI_REAL_DRIVER_COMFORTANGULARVELOCITYSTEERINGWHEEL_IDX = 38;
static constexpr size_t FMI_REAL_DRIVER_TIMEHEADWAYFREELANECHANGE_IDX = 39;
static constexpr size_t FMI_REAL_DRIVER_OUTERKEEPINGQUOTA_IDX = 40;
static constexpr size_t FMI_REAL_DRIVER_EGOLANEKEEPINGQUOTA_IDX = 41;
static constexpr size_t FMI_REAL_DRIVER_USESHOULDERINTRAFFICJAMQUOTA_IDX = 42;
static constexpr size_t FMI_REAL_DRIVER_USESHOULDERINTRAFFICJAMTOEXITQUOTA_IDX = 43;
static constexpr size_t FMI_REAL_DRIVER_USESHOULDERWHENPEERPRESSUREDQUOTA_IDX = 44;
static constexpr size_t FMI_REAL_DRIVER_DISTRACTIONPERCENTAGE_IDX = 45;
static constexpr size_t FMI_REAL_DRIVER_DISTRACTIONQUOTA_IDX = 46;
static constexpr size_t FMI_REAL_VEHICLE_STEERINGRATIO_IDX = 47;
static constexpr size_t FMI_REAL_VEHICLE_MAXSTEERING_IDX = 48;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO1_IDX = 49;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO2_IDX = 50;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO3_IDX = 51;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO4_IDX = 52;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO5_IDX = 53;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO6_IDX = 54;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO7_IDX = 55;
static constexpr size_t FMI_REAL_VEHICLE_GEARRATIO8_IDX = 56;
static constexpr size_t FMI_REAL_LAST_IDX = FMI_REAL_VEHICLE_GEARRATIO8_IDX;
static constexpr size_t FMI_REAL_VARS = FMI_REAL_LAST_IDX + 1;

/* String Variables */
static constexpr size_t FMI_STRING_SIMULATION_OUTPUTPATH_IDX = 0;
static constexpr size_t FMI_STRING_SIMULATION_CONFIGPATH_IDX = 1;
static constexpr size_t FMI_STRING_DRIVER_REACTIONBASETIME_TYPE_IDX = 2;
static constexpr size_t FMI_STRING_DRIVER_DELTAVVIOLATION_TYPE_IDX = 3;
static constexpr size_t FMI_STRING_LAST_IDX = FMI_STRING_DRIVER_DELTAVVIOLATION_TYPE_IDX;
static constexpr size_t FMI_STRING_VARS = FMI_STRING_LAST_IDX + 1;

static const std::unordered_map<std::string, size_t> intParameterIndicesByName = {
  {"Simulation.RandomSeed", FMI_INTEGER_SIMULATION_RANDOMSEED_IDX},
  {"Vehicle.NumberOfGears", FMI_INTEGER_VEHICLE_NUMBEROFGEARS_IDX}
};

static const std::unordered_map<std::string, size_t> stringParameterIndicesByName = {
  {"Simulation.OutputPath", FMI_STRING_SIMULATION_OUTPUTPATH_IDX},
    {"Simulation.ConfigPath", FMI_STRING_SIMULATION_CONFIGPATH_IDX},
    {"Driver.ReactionBaseTime.type", FMI_STRING_DRIVER_REACTIONBASETIME_TYPE_IDX},
  {"Driver.DeltaVViolation.type", FMI_STRING_DRIVER_DELTAVVIOLATION_TYPE_IDX}
};

static const std::unordered_map<std::string, size_t> boolParameterIndicesByName = {
  {"Driver.IdealPerception", FMI_BOOLEAN_DRIVER_IDEALPERCEPTION_IDX},
  {"Driver.EnableHighCognitiveMode", FMI_BOOLEAN_DRIVER_IDEALPERCEPTION_IDX}
};

static const std::unordered_map<std::string, size_t> realParameterIndicesByName = {
  {"Driver.PreviewTimeHeadway", FMI_REAL_DRIVER_PREVIEWTIMEHEADWAY_IDX},
  {"Driver.PreviewDistanceMin", FMI_REAL_DRIVER_PREVIEWDISTANCEMIN_IDX},
  {"Driver.ThresholdLooming", FMI_REAL_DRIVER_THRESHOLDLOOMING_IDX},
  {"Driver.ReactionBaseTime.mean", FMI_REAL_DRIVER_REACTIONBASETIME_MEAN_IDX},
  {"Driver.ReactionBaseTime.sd", FMI_REAL_DRIVER_REACTIONBASETIME_SD_IDX},
  {"Driver.ReactionBaseTime.min", FMI_REAL_DRIVER_REACTIONBASETIME_MIN_IDX},
  {"Driver.ReactionBaseTime.max", FMI_REAL_DRIVER_REACTIONBASETIME_MAX_IDX},
  {"Driver.AnticipationQuota", FMI_REAL_DRIVER_ANTICIPATIONQUOTA_IDX},
  {"Driver.AgentCooperationFactor", FMI_REAL_DRIVER_AGENTCOOPERATIONFACTOR_IDX},
  {"Driver.AgentSuspiciousBehaviourEvasionFactor", FMI_REAL_DRIVER_AGENTSUSPICIOUSBEHAVIOUREVASIONFACTOR_IDX},
  {"Driver.OuterKeepingIntensity", FMI_REAL_DRIVER_OUTERKEEPINGINTENSITY_IDX},
  {"Driver.EgoLaneKeepingIntensity", FMI_REAL_DRIVER_EGOLANEKEEPINGINTENSITY_IDX},
  {"Driver.RightOvertakingProhibitionIgnoringQuota", FMI_REAL_DRIVER_RIGHTOVERTAKINGPROHIBITIONIGNORINGQUOTA_IDX},
  {"Driver.LaneChangeProhibitionIgnoringQuota", FMI_REAL_DRIVER_LANECHANGEPROHIBITIONIGNORINGQUOTA_IDX},
  {"Driver.VWish", FMI_REAL_DRIVER_VWISH_IDX},
  {"Driver.DeltaVWish", FMI_REAL_DRIVER_DELTAVWISH_IDX},
  {"Driver.DeltaVViolation.mean", FMI_REAL_DRIVER_DELTAVVIOLATION_MEAN_IDX},
  {"Driver.DeltaVViolation.sd", FMI_REAL_DRIVER_DELTAVVIOLATION_SD_IDX},
  {"Driver.DeltaVViolation.min", FMI_REAL_DRIVER_DELTAVVIOLATION_MIN_IDX},
  {"Driver.DeltaVViolation.max", FMI_REAL_DRIVER_DELTAVVIOLATION_MAX_IDX},
  {"Driver.DesiredTimeAtTargetSpeed", FMI_REAL_DRIVER_DESIREDTIMEATTARGETSPEED_IDX},
  {"Driver.ProportionalityFactorForFollowingDistance", FMI_REAL_DRIVER_PROPORTIONALITYFACTORFORFOLLOWINGDISTANCE_IDX},
  {"Driver.MaxLongitudinalAcceleration", FMI_REAL_DRIVER_MAXLONGITUDINALACCELERATION_IDX},
  {"Driver.MaxLongitudinalDeceleration", FMI_REAL_DRIVER_MAXLONGITUDINALDECELERATION_IDX},
  {"Driver.MaxLateralAcceleration", FMI_REAL_DRIVER_MAXLATERALACCELERATION_IDX},
  {"Driver.ComfortLongitudinalAcceleration", FMI_REAL_DRIVER_COMFORTLONGITUDINALACCELERATION_IDX},
  {"Driver.ComfortLongitudinalDeceleration", FMI_REAL_DRIVER_COMFORTLONGITUDINALDECELERATION_IDX},
  {"Driver.AccelerationAdjustmentThresholdVelocity", FMI_REAL_DRIVER_ACCELERATIONADJUSTMENTTHRESHOLDVELOCITY_IDX},
  {"Driver.VelocityForMaxAcceleration", FMI_REAL_DRIVER_VELOCITYFORMAXACCELERATION_IDX},
  {"Driver.MaxComfortAccelerationFactor", FMI_REAL_DRIVER_MAXCOMFORTACCELERATIONFACTOR_IDX},
  {"Driver.MaxComfortDecelerationFactor", FMI_REAL_DRIVER_MAXCOMFORTDECELERATIONFACTOR_IDX},
  {"Driver.ComfortLateralAcceleration", FMI_REAL_DRIVER_COMFORTLATERALACCELERATION_IDX},
  {"Driver.CarQueuingDistance", FMI_REAL_DRIVER_CARQUEUINGDISTANCE_IDX},
  {"Driver.HysteresisForRestart", FMI_REAL_DRIVER_HYSTERESISFORRESTART_IDX},
  {"Driver.LateralOffsetNeutralPositionQuota", FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITIONQUOTA_IDX},
  {"Driver.LateralOffsetNeutralPosition", FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITION_IDX},
  {"Driver.LateralOffsetNeutralPositionRescueLane", FMI_REAL_DRIVER_LATERALOFFSETNEUTRALPOSITIONRESCUELANE_IDX},
  {"Driver.MaximumAngularVelocitySteeringWheel", FMI_REAL_DRIVER_MAXIMUMANGULARVELOCITYSTEERINGWHEEL_IDX},
  {"Driver.ComfortAngularVelocitySteeringWheel", FMI_REAL_DRIVER_COMFORTANGULARVELOCITYSTEERINGWHEEL_IDX},
  {"Driver.TimeHeadwayFreeLaneChange", FMI_REAL_DRIVER_TIMEHEADWAYFREELANECHANGE_IDX},
  {"Driver.OuterKeepingQuota", FMI_REAL_DRIVER_OUTERKEEPINGQUOTA_IDX},
  {"Driver.EgoLaneKeepingQuota", FMI_REAL_DRIVER_EGOLANEKEEPINGQUOTA_IDX},
  {"Driver.UseShoulderInTrafficJamQuota", FMI_REAL_DRIVER_USESHOULDERINTRAFFICJAMQUOTA_IDX},
  {"Driver.UseShoulderInTrafficJamToExitQuota", FMI_REAL_DRIVER_USESHOULDERINTRAFFICJAMTOEXITQUOTA_IDX},
  {"Driver.UseShoulderWhenPeerPressuredQuota", FMI_REAL_DRIVER_USESHOULDERWHENPEERPRESSUREDQUOTA_IDX},
  {"Driver.DistractionPercentage", FMI_REAL_DRIVER_DISTRACTIONPERCENTAGE_IDX},
  {"Driver.DistractionQuota", FMI_REAL_DRIVER_DISTRACTIONQUOTA_IDX},
  {"Vehicle.SteeringRatio", FMI_REAL_VEHICLE_STEERINGRATIO_IDX},
  {"Vehicle.MaxSteering", FMI_REAL_VEHICLE_MAXSTEERING_IDX},
  {"Vehicle.GearRatio1", FMI_REAL_VEHICLE_GEARRATIO1_IDX},
  {"Vehicle.GearRatio2", FMI_REAL_VEHICLE_GEARRATIO2_IDX},
  {"Vehicle.GearRatio3", FMI_REAL_VEHICLE_GEARRATIO3_IDX},
  {"Vehicle.GearRatio4", FMI_REAL_VEHICLE_GEARRATIO4_IDX},
  {"Vehicle.GearRatio5", FMI_REAL_VEHICLE_GEARRATIO5_IDX},
  {"Vehicle.GearRatio6", FMI_REAL_VEHICLE_GEARRATIO6_IDX},
  {"Vehicle.GearRatio7", FMI_REAL_VEHICLE_GEARRATIO7_IDX},
  {"Vehicle.GearRatio8", FMI_REAL_VEHICLE_GEARRATIO8_IDX}
};

#include <cstdarg>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>
#include <string>
#include <unordered_map>
#include <variant>

#undef min
#undef max

#include "osi3/osi_sensorview.pb.h"
#include "osi3/osi_trafficupdate.pb.h"
#include "osi3/osi_trafficcommand.pb.h"

namespace OsmpShell {

class FmiConnector {
public:
  /* FMI2 Interface mapped to C++ */
  FmiConnector(fmi2String theinstanceName, fmi2Type thefmuType, fmi2String thefmuGUID, fmi2String thefmuResourceLocation, const fmi2CallbackFunctions* thefunctions, fmi2Boolean thevisible, fmi2Boolean theloggingOn);
  virtual ~FmiConnector();
  fmi2Status SetDebugLogging(fmi2Boolean theloggingOn, size_t nCategories, const fmi2String categories[]);
  fmi2Component ValidateInstance();
    fmi2Status SetupExperiment(fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime);
    fmi2Status EnterInitializationMode();
    fmi2Status ExitInitializationMode();
    fmi2Status DoStep(fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component);
    fmi2Status Terminate();
    fmi2Status Reset();
    void FreeInstance();
    fmi2Status GetReal(const fmi2ValueReference vr[], size_t nvr, fmi2Real value[]);
    fmi2Status GetInteger(const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[]);
    fmi2Status GetBoolean(const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[]);
    fmi2Status GetString(const fmi2ValueReference vr[], size_t nvr, fmi2String value[]);
    fmi2Status SetReal(const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[]);
    fmi2Status SetInteger(const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[]);
    fmi2Status SetBoolean(const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[]);
    fmi2Status SetString(const fmi2ValueReference vr[], size_t nvr, const fmi2String value[]);

    class Logger : public FmiLogInterface
    {
    public:
        typedef std::function<void(const char*, const char*)> LogFunction;

        Logger(LogFunction logFunction);
        ~Logger() override = default;

        void Log(LogLevel logLevel, const std::string& message) override;
    private:
        LogFunction logFunction;
    };

protected:
    /* Internal Implementation */
    fmi2Status doInit();
    fmi2Status doInitParameters();
    fmi2Status doStart(fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime);
    fmi2Status doEnterInitializationMode();
    fmi2Status doExitInitializationMode();
    fmi2Status doCalc(fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component);
    fmi2Status doTerm();
    void doFree();
    
    virtual fmi2Status InitFmu(const FmiParameters& parameters, const osi3::GroundTruth& initGroundTruth, FmiLogInterface& logger) = 0;
    virtual fmi2Status StepFmu(const osi3::SensorView& sensorView, const osi3::TrafficCommand& trafficCommand, osi3::TrafficUpdate& trafficUpdate) = 0;

protected:
    /* Private File-based Logging just for Debugging */
#ifdef PRIVATE_LOG_PATH
    static std::ofstream private_log_file;
#endif

    static void fmi_verbose_log_global(const char* format, ...) {
#ifdef VERBOSE_FMI_LOGGING
#ifdef PRIVATE_LOG_PATH
        va_list ap;
        va_start(ap, format);
        char buffer[1024];
        if (!private_log_file.is_open())
            private_log_file.open(PRIVATE_LOG_PATH, std::ios::out | std::ios::app);
        if (private_log_file.is_open()) {
#ifdef _WIN32
            vsnprintf_s(buffer, 1024, format, ap);
#else
            vsnprintf(buffer, 1024, format, ap);
#endif
            private_log_file << "OSMPDummySensor" << "::Global:FMI: " << buffer << std::endl;
            private_log_file.flush();
        }
#endif
#endif
    }

    void internal_log(const char* category, const char* format, va_list arg)
    {
#if defined(PRIVATE_LOG_PATH) || defined(PUBLIC_LOGGING)
        char buffer[1024];
#ifdef _WIN32
        vsnprintf_s(buffer, 1024, format, arg);
#else
        vsnprintf(buffer, 1024, format, arg);
#endif
#ifdef PRIVATE_LOG_PATH
        if (!private_log_file.is_open())
            private_log_file.open(PRIVATE_LOG_PATH, std::ios::out | std::ios::app);
        if (private_log_file.is_open()) {
            private_log_file << "OSMPDummySensor" << "::" << instanceName << "<" << ((void*)this) << ">:" << category << ": " << buffer << std::endl;
            private_log_file.flush();
        }
#endif
#ifdef PUBLIC_LOGGING
        if (loggingOn && loggingCategories.count(category))
            functions.logger(functions.componentEnvironment,instanceName.c_str(),fmi2OK,category,buffer);
#endif
#endif
    }

    void fmi_verbose_log(const char* format, ...) {
#if  defined(VERBOSE_FMI_LOGGING) && (defined(PRIVATE_LOG_PATH) || defined(PUBLIC_LOGGING))
        va_list ap;
        va_start(ap, format);
        internal_log("FMI",format,ap);
        va_end(ap);
#endif
    }

    /* Normal Logging */
    void normal_log(const char* category, const char* format, ...) {
#if defined(PRIVATE_LOG_PATH) || defined(PUBLIC_LOGGING)
        va_list ap;
        va_start(ap, format);
        internal_log(category,format,ap);
        va_end(ap);
#endif
    }

protected:
    /* Members */
    std::string instanceName;
    fmi2Type fmuType;
    std::string fmuGUID;
    std::string fmuResourceLocation;
    bool visible;
    bool loggingOn;
    std::set<std::string> loggingCategories;
    fmi2CallbackFunctions functions;
    fmi2Boolean boolean_vars[FMI_BOOLEAN_VARS];
    fmi2Integer integer_vars[FMI_INTEGER_VARS];
    fmi2Real real_vars[FMI_REAL_VARS];
    std::string string_vars[FMI_STRING_VARS];
    bool simulationStarted;
    bool sensorViewConfigRequestNeedsRefresh;
    /// serialized storage for TrafficUpdate output
    std::vector<char> currentOutputBuffer;
    /// serialized storage for previous version of TrafficUpdate output (double buffering)
    std::vector<char> lastOutputBuffer;
    /// serialized storage for SensorViewConfiguration output
    std::vector<char> currentConfigRequestBuffer;
    /// serialized storage for previous version SensorViewConfiguration output (double buffering)
    std::vector<char> lastConfigRequestBuffer;

    /* Simple Accessors */
    fmi2Boolean fmi_valid() const { return boolean_vars[FMI_BOOLEAN_VALID_IDX]; }
    void set_fmi_valid(fmi2Boolean value) { boolean_vars[FMI_BOOLEAN_VALID_IDX]=value; }

    /* Protocol Buffer Accessors */
    bool get_fmi_sensor_view_config(osi3::SensorViewConfiguration& data);
    void set_fmi_sensor_view_config_request(const osi3::SensorViewConfiguration& data);
    void reset_fmi_sensor_view_config_request();
    bool get_fmi_sensor_view_in(osi3::SensorView& data);
    bool get_fmi_traffic_command_in(osi3::TrafficCommand& data);
    void set_fmi_traffic_update_out(const osi3::TrafficUpdate& data);
    void reset_fmi_traffic_update_out();

    /* Refreshing of Calculated Parameters */
    void refresh_fmi_sensor_view_config_request();

    FmiParameters parameters;
    std::shared_ptr<FmiLogInterface> logger;
    osi3::GroundTruth initGroundTruth;
    osi3::SensorView currentSensorView;
    osi3::SensorView previousSensorView;
    osi3::TrafficCommand currentTrafficCommand;
    osi3::TrafficCommand previousTrafficCommand;
    osi3::TrafficUpdate currentTrafficUpdate;
    osi3::TrafficUpdate previousTrafficUpdate;
};
} // namespace OsmpShell
