/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "fmu_address_helper.h"

void* decode_integer_to_pointer(fmi2Integer hi, fmi2Integer lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr{};
  myaddr.base.lo = lo;
  myaddr.base.hi = hi;
  return reinterpret_cast<void*>(myaddr.address);
#elif PTRDIFF_MAX == INT32_MAX
  return reinterpret_cast<void*>(lo);
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}

void encode_pointer_to_integer(const void* ptr, fmi2Integer& hi, fmi2Integer& lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr{};
  myaddr.address = reinterpret_cast<unsigned long long>(ptr);
  hi = myaddr.base.hi;
  lo = myaddr.base.lo;
#elif PTRDIFF_MAX == INT32_MAX
  hi = 0;
  lo = reinterpret_cast<int>(ptr);
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}
