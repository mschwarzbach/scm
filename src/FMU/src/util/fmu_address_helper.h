/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "fmi2Functions.h"


void* decode_integer_to_pointer(fmi2Integer hi, fmi2Integer lo);

void encode_pointer_to_integer(const void* ptr,fmi2Integer& hi,fmi2Integer& lo);
