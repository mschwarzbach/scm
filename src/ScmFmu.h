/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "OsmpShell.h"
#include "ModuleController.h"

using namespace OsmpShell;

class ScmFmu : public FmiConnector
{
public:
  ScmFmu(fmi2String theinstanceName, fmi2Type thefmuType, fmi2String thefmuGUID, fmi2String thefmuResourceLocation, const fmi2CallbackFunctions *thefunctions, fmi2Boolean thevisible, fmi2Boolean theloggingOn);
  virtual ~ScmFmu() = default;

  fmi2Status InitFmu(const FmiParameters &parameters, const osi3::GroundTruth &initGroundTruth, FmiLogInterface &logger) override;
  fmi2Status StepFmu(const osi3::SensorView &sensorView, const osi3::TrafficCommand &trafficCommand, osi3::TrafficUpdate &trafficUpdate) override;

private:
  std::unique_ptr<scm::ModuleController> scm;
  bool isInitialized{false};
};
