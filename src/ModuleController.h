/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <OsiQueryLibrary/osiql.h>
#include <Stochastics/Stochastics.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>

#include <unordered_map>
#include <variant>

#include "../include/Logging/PublisherInterface.h"
#include "../include/ScmSignals.h"
#include "../include/common/ParameterInterface.h"
#include "../include/common/Parameters.h"
#include "../include/module.h"
#include "FMU/src/OsmpShell/FmiLogInterface.h"
#include "FMU/src/OsmpShell/FmiParameterInterface.h"
#include "Logging/SimulationLogging/ScmObserver/ScmObserver.h"
#include "Logging/SimulationLogging/SimulationLogging.h"

typedef osi3::MovingObject::VehicleClassification::LightState OsiLightState;
namespace scm
{
static const int DEFAULT_CYCLE_TIME{100};

class ModuleController
{
public:
  const std::string COMPONENTNAME = "AlgorithmScm";
  const units::length::meter_t VISIBILITY_DISTANCE = 400.0_m;  // todo

  ModuleController(const FmiParameters& parameters, const osi3::GroundTruth& initGroundTruth, FmiLogInterface& logger);
  // GazeFollowerFilePath?? // todo

  virtual ~ModuleController();

  void Trigger(const osi3::SensorView& sensorView, const osi3::TrafficCommand& trafficCommand, osi3::TrafficUpdate& trafficUpdate);
  bool InitializeModules(const osi3::SensorView& sensorView);

protected:
  void ExtractHostVehicleProperties(const osi3::MovingObject& moving, FmiParameters& vehicleParameters);
  scm::signal::SystemInput _system_input;

private:
  FmiLogInterface& _logging;
  parameter::PartitionedFmiParameters _fmiParameters{};
  osi3::GroundTruth _groundTruth{};
  units::time::millisecond_t _cycleTime{};
  int _agentId{-1};

  std::unique_ptr<osiql::Query> _osiQl;

  scm::RuntimeInformation _runtimeInformation;
  std::optional<std::string> _outputPath;

  std::unique_ptr<SimulationLogging> _simulationLogging{nullptr};
  bool _newRouteRequestLastTimeStep{false};

  std::unique_ptr<scm::parameter::ParameterInterface> _scmParameters{nullptr};
  std::unique_ptr<scm::parameter::ParameterMapping> _parameterMapping{nullptr};

  // MODULES
  std::unique_ptr<scm::parameter_parser::Interface> _parameter_parser;
  std::unique_ptr<scm::sensor::Interface> _sensor;
  std::unique_ptr<scm::driver::Interface> _driver;
  std::unique_ptr<scm::longitudinal_controller::Interface> _longitudinal_controller;
  std::unique_ptr<scm::lateral_controller::Interface> _lateral_controller;
  std::unique_ptr<scm::dynamics_model::Interface> _dynamics_model;

  std::unique_ptr<Stochastics> _stochastics{};

  // SIGNALS
  scm::signal::SystemOutput _system_output;

  // MODULE INTERCONNECTION
  scm::signal::SensorInput Transform(const scm::signal::SystemInput& systemInput, const signal::ParameterParserOutput& parameterOutput, const osi3::TrafficCommand& trafficCommand);
  scm::signal::SystemOutput Transform(const scm::signal::DynamicsModelOutput& dynamicsOutput, const scm::signal::DriverOutput& driverOutput, const scm::signal::SensorOutput& sensorOutput);

  scm::signal::DriverInput Transform(const scm::signal::ParameterParserOutput& parameterOutput,
                                     const scm::signal::SystemInput& systemInput,
                                     const scm::signal::SensorOutput& sensorOutput);
  scm::signal::LateralControllerInput Transform(const scm::signal::SystemInput& systemInput, const scm::signal::DriverOutput& driverOutput, const OwnVehicleInformationSCM& ownVehicle);
  scm::signal::DynamicsModelInput Transform(const scm::signal::LateralControllerOutput& lateralOutput, const scm::signal::LongitudinalControllerOutput& longitudinalOutput, const scm::signal::SystemInput& systemInput, const scm::signal::DriverOutput& driverOutput);
  scm::signal::LongitudinalControllerInput Transform(const signal::SystemInput& systemInput, const signal::ParameterParserOutput& parameterOutput, const scm::signal::SensorOutput& sensorOutput, const scm::signal::DriverOutput& driverOutput);
  scm::LoggerOptions GetLoggerDetails(const std::string& configPath) const;
  void TranslateToOsiTrafficUpdate(osi3::TrafficUpdate& trafficUpdate);
  static std::optional<osi3::TrafficAction_FollowPathAction> GetFollowPathAction(const osi3::TrafficCommand& trafficCommand);
  HafParameters ParseHafMessage(const osi3::HostVehicleData::VehicleAutomatedDrivingFunction& hafmessage) const;
  void HandleHAFInput(const google::protobuf::RepeatedPtrField<osi3::HostVehicleData_VehicleAutomatedDrivingFunction>& drivingFunction);
  void ParseCustomCommandHAF(const std::vector<std::string>& customCommands);

  bool _isHaf = false;
  int DetermineRandomSeed() const;
  void DetermineOutputPath();
  void DetermineConfigPath();
};

}  // namespace scm