/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "ModuleController.h"

#include <libxml/tree.h>

#include <cmath>
#include <memory>
#include <regex>

#include "../module/driver/include/DriverFactory.h"
#include "../module/dynamicsModel/include/ScmDynamicsModelFactory.h"
#include "../module/lateralController/include/ScmLateralControllerFactory.h"
#include "../module/longitudinalController/include/ScmLongitudinalControllerFactory.h"
#include "../module/parameterParser/include/ScmParameterParserFactory.h"
#include "../module/sensor/include/ScmSensorFactory.h"
#include "ParameterProcessing/DefaultParameter.h"
#include "ParameterProcessing/FmiParameterProcessing.h"
#include "include/common/XmlParser.h"

namespace scm
{
ModuleController::ModuleController(const FmiParameters& parameters, const osi3::GroundTruth& initGroundTruth, FmiLogInterface& logging)
    : _logging(logging), _fmiParameters{fmi::parameter::PartitionFmiParameters(parameters)}
{
  auto fmiCycleTime = fmi::parameter::Get<int>(parameters, "Simulation.CycleTime");
  _cycleTime = units::make_unit<units::time::millisecond_t>(fmiCycleTime.value_or(DEFAULT_CYCLE_TIME));

  _groundTruth.CopyFrom(initGroundTruth);

  _stochastics = std::make_unique<Stochastics>();
  _stochastics->InitGenerator(DetermineRandomSeed());

  DetermineOutputPath();
  DetermineConfigPath();

  _runtimeInformation.loggerOptions = GetLoggerDetails(_runtimeInformation.directories.configuration);

  _parameterMapping = std::make_unique<parameter::ParameterMapping>(GenerateDefaultDriverParameter(_runtimeInformation));

  _osiQl = std::make_unique<osiql::Query>(_groundTruth);
}

void ModuleController::DetermineConfigPath()
{
  auto configPath = fmi::parameter::Get<std::string>(_fmiParameters.simulationParameters, "Simulation.ConfigPath");
  if (configPath.has_value() && !configPath->empty())
  {
    _runtimeInformation.directories.configuration = configPath.value() + "/";
  }
  else
  {
    _logging.Log(FmiLogInterface::LogLevel::Warning, "No config path given!");
  }
}

void ModuleController::DetermineOutputPath()
{
  auto outputPath = fmi::parameter::Get<std::string>(_fmiParameters.simulationParameters, "Simulation.OutputPath");
  if (outputPath.has_value() && !outputPath->empty())
  {
    _runtimeInformation.directories.output = outputPath.value();
  }
  else
  {
    _logging.Log(FmiLogInterface::LogLevel::Warning, "No output path given!");
  }
}

int ModuleController::DetermineRandomSeed() const
{
  static int backupRandomSeed = 0;
  auto fmuRandomSeed = fmi::parameter::Get<int>(_fmiParameters.simulationParameters, "Simulation.RandomSeed");
  auto randomSeed = fmuRandomSeed ? fmuRandomSeed.value() : backupRandomSeed++;
  return randomSeed;
}

bool ModuleController::InitializeModules(const osi3::SensorView& sensorView)
{
  _agentId = static_cast<int>(sensorView.host_vehicle_id().value());
  _groundTruth.mutable_host_vehicle_id()->set_value(_agentId);
  _osiQl->Update(sensorView.global_ground_truth().moving_object());

  auto mapping = fmi::parameter::CreateMappingFromFmiDriverParameters(_fmiParameters.driverParameters);

  for (const auto& [key, value] : mapping)
  {
    try
    {
      _parameterMapping->at(key) = value;
    }
    catch (const std::out_of_range& e)
    {
      std::string message = "'" + key + "'" + " is not a supported parameter in SCM.\n" + e.what();
      Logging::Warning(message);
    }
  }

  auto movingObjects{sensorView.global_ground_truth().moving_object()};
  if (auto ego = std::find_if(movingObjects.begin(),
                              movingObjects.end(),
                              [&](const osi3::MovingObject& movingObject)
                              { return movingObject.id().value() == _agentId; });
      ego != movingObjects.end())
  {
    ExtractHostVehicleProperties(*ego, _fmiParameters.vehicleParameters);
  }

  _simulationLogging = std::make_unique<SimulationLogging>(_agentId, _runtimeInformation);
  _simulationLogging->SimulationPreRun();

  _parameter_parser = scm::parameter_parser::factory::Create(_stochastics.get(),
                                                             _parameterMapping.get(),
                                                             _simulationLogging->GetPublisher(),
                                                             _system_input.vehicleParameters.classification,
                                                             VISIBILITY_DISTANCE);

  _sensor = scm::sensor::factory::Create(_cycleTime.value(),
                                         _runtimeInformation.directories.configuration,
                                         _stochastics.get(),
                                         _osiQl.get(),
                                         VISIBILITY_DISTANCE,
                                         _agentId);

  _driver = scm::driver::factory::Create(_stochastics.get(), _cycleTime, _simulationLogging->GetPublisher());
  _longitudinal_controller = scm::longitudinal_controller::factory::Create(_stochastics.get());
  _lateral_controller = scm::lateral_controller::factory::Create();

  _dynamics_model = scm::dynamics_model::factory::Create(_cycleTime, _osiQl.get());

  return true;
}

ModuleController::~ModuleController()
{
  if (_simulationLogging)
  {
    _simulationLogging->SimulationPostRun();
  }
}

void ModuleController::ExtractHostVehicleProperties(const osi3::MovingObject& moving, FmiParameters& vehicleParameters)
{
  using namespace scm::common;
  vehicle::properties::EntityProperties vehicleProperties = GenerateDefaultVehicleParameter();
  auto hostDimension = moving.base().dimension();
  vehicleProperties.bounding_box.dimension = Dimension3{static_cast<units::length::meter_t>(hostDimension.length()), static_cast<units::length::meter_t>(hostDimension.width()), hostDimension.height()};

  auto hostPosition = moving.base().position();
  vehicleProperties.bounding_box.geometric_center = Vec3{hostPosition.x(), hostPosition.y(), hostPosition.z()};

  auto wheelBase = moving.vehicle_attributes().bbcenter_to_front().x() - moving.vehicle_attributes().bbcenter_to_rear().x();  // assumes that bbcenter_to_rear().x() is negative
  _system_input.wheelBase = static_cast<units::length::meter_t>(wheelBase);                                                   // old, can be part of properties
  vehicleProperties.wheelBase = wheelBase;

  auto bbCenterToFront = moving.vehicle_attributes().bbcenter_to_front();
  vehicleProperties.front_axle.bb_center_to_axle_center = Vec3{bbCenterToFront.x(), bbCenterToFront.y(), bbCenterToFront.z()};
  auto bbCenterToRear = moving.vehicle_attributes().bbcenter_to_rear();
  vehicleProperties.rear_axle.bb_center_to_axle_center = Vec3{bbCenterToRear.x(), bbCenterToRear.y(), bbCenterToRear.z()};

  _system_input.vehicleParameters = fmi::parameter::ParseVehicleParameter(vehicleParameters, vehicleProperties);
  _system_input.steeringRatio = std::stod(_system_input.vehicleParameters.properties[scm::common::vehicle::properties::SteeringRatio]);
  _system_input.frontAxleMaxSteering = _system_input.vehicleParameters.front_axle.max_steering.value() * _system_input.steeringRatio;
}

static std::map<scm::LightState::Indicator, OsiLightState::IndicatorState> OsiLightStateMap = {
    {scm::LightState::Indicator::Left, OsiLightState::INDICATOR_STATE_LEFT},
    {scm::LightState::Indicator::Right, OsiLightState::INDICATOR_STATE_RIGHT},
    {scm::LightState::Indicator::Warn, OsiLightState::INDICATOR_STATE_WARNING},
    {scm::LightState::Indicator::Off, OsiLightState::INDICATOR_STATE_OFF}};

void ModuleController::TranslateToOsiTrafficUpdate(osi3::TrafficUpdate& trafficUpdate)
{
  // base moving
  auto baseMoving = trafficUpdate.mutable_update(0)->mutable_base();
  baseMoving->mutable_velocity()->set_x(_system_output.dynamicsInformation.velocityX.value());
  baseMoving->mutable_velocity()->set_y(_system_output.dynamicsInformation.velocityY.value());
  baseMoving->mutable_acceleration()->set_x(_system_output.dynamicsInformation.acceleration.value() * std::cos(_system_output.dynamicsInformation.yaw.value()));
  baseMoving->mutable_acceleration()->set_y(_system_output.dynamicsInformation.acceleration.value() * std::sin(_system_output.dynamicsInformation.yaw.value()));
  baseMoving->mutable_position()->set_x(_system_output.dynamicsInformation.positionX.value());
  baseMoving->mutable_position()->set_y(_system_output.dynamicsInformation.positionY.value());
  baseMoving->mutable_orientation()->set_yaw(_system_output.dynamicsInformation.yaw.value());
  baseMoving->mutable_orientation()->set_roll(_system_output.dynamicsInformation.roll.value());
  auto orientationRate = baseMoving->mutable_orientation_rate();

  // The recipient(osi) of the traffic update uses the yawRate in radians per millisecond (convert from second to millisecond)
  orientationRate->set_yaw(_system_output.dynamicsInformation.yawRate.value() / 1000);

  auto orientationAcceleration = baseMoving->mutable_orientation_acceleration();

  // The recipient(osi) of the traffic update uses the yawAcceleration in radians per millisecond (convert from second to millisecond)
  orientationAcceleration->set_yaw(_system_output.dynamicsInformation.yawAcceleration.value() / 1000);

  auto movingObject = trafficUpdate.mutable_update(0);
  // vehicle attributes
  movingObject->mutable_vehicle_attributes()->set_steering_wheel_angle(_system_output.dynamicsInformation.steeringWheelAngle.value());

  // vehicle classification (secondaryDriverTasks)
  auto vehicleClassification = movingObject->mutable_vehicle_classification();
  auto vehicleLights = vehicleClassification->mutable_light_state();

  vehicleLights->set_indicator_state(OsiLightStateMap[_system_output.secondaryDriverTasks.indicatorState]);
  vehicleLights->set_head_light(_system_output.secondaryDriverTasks.headLightSwitch
                                    ? osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON
                                    : osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleLights->set_high_beam(_system_output.secondaryDriverTasks.highBeamLightSwitch || _system_output.secondaryDriverTasks.flasherSwitch
                                   ? osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON
                                   : osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleLights->set_brake_light_state(_system_output.dynamicsInformation.acceleration.value() < 0.0
                                           ? osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_NORMAL
                                           : osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF);
}

scm::signal::SensorInput ModuleController::Transform(const scm::signal::SystemInput& systemInput, const signal::ParameterParserOutput& parameterOutput, const osi3::TrafficCommand& trafficCommand)
{
  return {
      parameterOutput.driverParameters.previewDistance,
      systemInput.vehicleParameters,
      _newRouteRequestLastTimeStep,
      GetFollowPathAction(trafficCommand)};
}

scm::signal::SystemOutput ModuleController::Transform(const scm::signal::DynamicsModelOutput& dynamicsOutput, const scm::signal::DriverOutput& driverOutput, const scm::signal::SensorOutput& sensorOutput)
{
  return {
      driverOutput.secondaryDriverTasks,
      dynamicsOutput.dynamicsInformation,
      sensorOutput.ownVehicleInformationSCM};
}

scm::signal::DriverInput ModuleController::Transform(const scm::signal::ParameterParserOutput& parameterOutput,
                                                     const scm::signal::SystemInput& systemInput,
                                                     const scm::signal::SensorOutput& sensorOutput)
{
  return {
      parameterOutput.driverParameters,
      parameterOutput.trafficRulesScm,
      systemInput.vehicleParameters,
      sensorOutput,
      _system_input.hafParameters};
}

scm::signal::LateralControllerInput ModuleController::Transform(const scm::signal::SystemInput& systemInput, const scm::signal::DriverOutput& driverOutput, const OwnVehicleInformationSCM& ownVehicle)
{
  return {
      static_cast<units::velocity::meters_per_second_t>(ownVehicle.longitudinalVelocity),
      static_cast<units::length::meter_t>(driverOutput.lateralInformation.laneWidth),
      static_cast<units::angle::radian_t>(ownVehicle.steeringWheelAngle),
      static_cast<units::length::meter_t>(driverOutput.lateralInformation.deviation),
      static_cast<units::angular_acceleration::radians_per_second_squared_t>(driverOutput.lateralInformation.gainDeviation),
      static_cast<units::angle::radian_t>(driverOutput.lateralInformation.headingError),
      static_cast<units::frequency::hertz_t>(driverOutput.lateralInformation.gainHeadingError),
      static_cast<units::curvature::inverse_meter_t>(driverOutput.lateralInformation.kappaManoeuvre),
      static_cast<units::curvature::inverse_meter_t>(driverOutput.lateralInformation.kappaRoad),
      {},  // curvatureOfSegmentPoints currently never set
      {},  // curvatureOfSegmentPoints currently never set
      systemInput.steeringRatio,
      systemInput.frontAxleMaxSteering,
      static_cast<units::length::meter_t>(systemInput.wheelBase)};
}

scm::signal::LongitudinalControllerInput ModuleController::Transform(const signal::SystemInput& systemInput,
                                                                     const signal::ParameterParserOutput& parameterOutput,
                                                                     const scm::signal::SensorOutput& sensorOutput,
                                                                     const scm::signal::DriverOutput& driverOutput)
{
  return {
      sensorOutput.ownVehicleInformationSCM,
      driverOutput.acceleration,
      parameterOutput.driverParameters,
      systemInput.vehicleParameters,
      _cycleTime};
}

scm::signal::DynamicsModelInput ModuleController::Transform(const signal::LateralControllerOutput& lateralOutput,
                                                            const signal::LongitudinalControllerOutput& longitudinalOutput,
                                                            const scm::signal::SystemInput& systemInput,
                                                            const scm::signal::DriverOutput& driverOutput)
{
  return {
      lateralOutput,
      longitudinalOutput,
      systemInput.vehicleParameters,
      driverOutput.acceleration,
      _cycleTime};
}

std::optional<osi3::TrafficAction_FollowPathAction> ModuleController::GetFollowPathAction(const osi3::TrafficCommand& trafficCommand)
{
  for (const auto& action : trafficCommand.action())
  {
    // assuming there is only one action providing a path
    if (action.has_follow_path_action())
    {
      return action.follow_path_action();
    }
  }
  return std::nullopt;
}

std::vector<std::string> ExtractCustomCommands(const osi3::TrafficCommand& trafficCommand)
{
  static const std::regex SUPPORTED_COMMANDS("SetCustomLaneChange|SetGazeFollower|SetHAF");
  std::vector<std::string> customCommands{};

  for (const auto& action : trafficCommand.action())
  {
    if (action.has_custom_action())
    {
      const auto command = action.custom_action().command();
      if (std::regex_search(command, SUPPORTED_COMMANDS))
      {
        customCommands.push_back(command);
      }
    }
  }
  return customCommands;
}

HafParameters ModuleController::ParseHafMessage(const osi3::HostVehicleData::VehicleAutomatedDrivingFunction& hafMessage) const
{
  HafParameters hafParameter{};
  for (const auto& hafDetail : hafMessage.custom_detail())
  {
    if (hafDetail.key() == "isHafActive")
    {
      std::istringstream(hafDetail.value()) >> std::boolalpha >> hafParameter.activateHaf;
    }
    else if (hafDetail.key() == "isHafTakeoverRequest")
    {
      std::istringstream(hafDetail.value()) >> std::boolalpha >> hafParameter.activateTakeoverRequest;
    }
  }
  return hafParameter;
}

void ModuleController::HandleHAFInput(const google::protobuf::RepeatedPtrField<osi3::HostVehicleData_VehicleAutomatedDrivingFunction>& drivingFunction)
{
  if (auto haf = std::find_if(drivingFunction.begin(), drivingFunction.end(), [](const auto& function)
                              { return function.has_custom_name() && function.custom_name() == "HAF" /*&& function.has_custom_state()*/; });
      haf != drivingFunction.end())
  {
    _system_input.hafParameters = ParseHafMessage(*haf);
  }
}

void ModuleController::ParseCustomCommandHAF(const std::vector<std::string>& customCommands)
{
  _system_input.hafParameters.activateHaf = false;
  _system_input.hafParameters.activateTakeoverRequest = false;
  for (const auto& command : customCommands)
  {
    if (command.find("SetHAF") != std::string::npos)
    {
      auto isHafLast = _isHaf;
      const auto& tokens = scm::common::TokenizeString(command, ' ');
      _isHaf = (bool)std::stoi(tokens.at(1));

      if (_isHaf)
      {
        _system_input.hafParameters.activateHaf = _isHaf;
      }

      if (!_isHaf && isHafLast)
      {
        _system_input.hafParameters.activateHaf = false;
        _system_input.hafParameters.activateTakeoverRequest = true;
      }
    }
  }
}

void ModuleController::Trigger(const osi3::SensorView& sensorView, const osi3::TrafficCommand& trafficCommand, osi3::TrafficUpdate& trafficUpdate)
{
  auto time = units::make_unit<units::time::millisecond_t>(static_cast<int>(sensorView.timestamp().seconds() * 1000 + sensorView.timestamp().nanos() / 1e6));
  _simulationLogging->ClearTimeStep();
  _osiQl->Update(sensorView.global_ground_truth().moving_object());

  HandleHAFInput(sensorView.host_vehicle_data().vehicle_automated_driving_function());

  auto egoVelocity{units::make_unit<units::velocity::meters_per_second_t>(_osiQl->GetHostVehicle().GetVelocity().Length())};
  const auto parameter_parser_output = _parameter_parser->Trigger(egoVelocity);
  const auto sensor_input = Transform(_system_input, parameter_parser_output, trafficCommand);

  std::vector<std::string> customCommands{ExtractCustomCommands(trafficCommand)};

  ParseCustomCommandHAF(customCommands);

  const auto sensor_output = _sensor->Trigger(sensor_input, customCommands);

  const auto driver_input = Transform(parameter_parser_output, _system_input, sensor_output);
  const auto driver_output = _driver->Trigger(driver_input, time);
  _newRouteRequestLastTimeStep = driver_output.triggerNewRouteRequest;

  const auto lateral_controller_input = Transform(_system_input, driver_output, sensor_output.ownVehicleInformationSCM);
  const auto lateral_controller_output = _lateral_controller->Trigger(lateral_controller_input, time);

  const auto longitudinal_controller_input = Transform(_system_input, parameter_parser_output, sensor_output, driver_output);
  const auto longitudinal_controller_output = _longitudinal_controller->Trigger(longitudinal_controller_input, time);

  const auto dynamics_model_input = Transform(lateral_controller_output, longitudinal_controller_output, _system_input, driver_output);
  const auto dynamics_model_output = _dynamics_model->Trigger(dynamics_model_input);

  _system_output = Transform(dynamics_model_output, driver_output, sensor_output);

  TranslateToOsiTrafficUpdate(trafficUpdate);
  _simulationLogging->SimulationUpdate(time);
}

void ThrowIfFalse(bool success, const std::string& message)
{
  if (!success)
  {
    throw std::runtime_error(message);
  }
}

void ParseLoggingParameter(xmlNodePtr config, xmlNodePtr xmlNode, const std::string& key, bool& result)
{
  if (xmlStrEqual(xmlNode->name, scm::common::toXmlChar(key)))
  {
    ThrowIfFalse(scm::common::ParseBool(config, key, result),
                 "an error occurred during DriverParameter import. Invalid xml format of ScmConfig.xml");
  }
}

scm::LoggerOptions ModuleController::GetLoggerDetails(const std::string& configPath) const
{
  xmlDocPtr document = xmlReadFile((configPath + "ScmConfig.xml").c_str(), "UTF-8", 0);

  xmlNodePtr documentRoot = xmlDocGetRootElement(document);

  bool logParameters{false};
  bool logValues{false};
  bool xmlOutput{false};

  while (documentRoot)
  {
    xmlNodePtr config = scm::common::GetFirstChildElement(documentRoot, "LOGGING_DETAILS");
    if (xmlStrEqual(config->name, scm::common::toXmlChar("LOGGING_DETAILS")))
    {
      xmlNodePtr configParameter = config->children;
      while (configParameter)
      {
        ParseLoggingParameter(config, configParameter, "DriverParameter", logParameters);
        ParseLoggingParameter(config, configParameter, "DriverValues", logValues);
        ParseLoggingParameter(config, configParameter, "XmlOutput", xmlOutput);
        configParameter = xmlNextElementSibling(configParameter);
      }
    }
    documentRoot = xmlNextElementSibling(documentRoot);
  }

  xmlFreeDoc(document);

  return {logParameters, logValues, xmlOutput};
}

}  // namespace scm