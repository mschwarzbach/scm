/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "../../include/common/Parameters.h"
#include "../../include/common/VehicleProperties.h"
using namespace units::literals;
namespace scm
{

inline parameter::ParameterMapping ConsolidateScmParameters(const parameter::ParameterInterface* parameters)
{
  auto boolParameters = parameters->GetParametersBool();
  auto intParameters = parameters->GetParametersInt();
  auto doubleParameters = parameters->GetParametersDouble();
  auto distributions = parameters->GetParametersStochastic();
  scm::parameter::ParameterMapping result;

  result.insert(boolParameters.begin(), boolParameters.end());
  result.insert(intParameters.begin(), intParameters.end());
  result.insert(doubleParameters.begin(), doubleParameters.end());
  result.insert(distributions.begin(), distributions.end());

  return result;
}

inline parameter::ParameterMapping GenerateDefaultDriverParameter(const RuntimeInformation& runtimeInformation)
{
  using namespace scm::parameter;
  std::unique_ptr<ParameterInterface> parameters = std::make_unique<scm::parameter::Parameters>(runtimeInformation);
  parameters->AddParameterStochastic("PreviewTimeHeadway", NormalDistribution(15.0, 2.5, 10.0, 20.0));
  parameters->AddParameterDouble("PreviewDistanceMin", 50.0);
  parameters->AddParameterStochastic("ThresholdLooming", NormalDistribution(0.002, 0.001, 0.001, 0.004));
  parameters->AddParameterBool("IdealPerception", false);
  parameters->AddParameterBool("EnableHighCognitiveMode", false);
  parameters->AddParameterStochastic("ReactionBaseTime", LogNormalDistribution(0.5, 0.2, 0.2, 0.89));
  parameters->AddParameterDouble("AnticipationQuota", 1.0);
  parameters->AddParameterStochastic("AgentCooperationFactor", NormalDistribution(0.8, 0.15, 0.0, 1.0));
  parameters->AddParameterStochastic("AgentSuspiciousBehaviourEvasionFactor", NormalDistribution(0.8, 0.15, 0.0, 1.0));
  parameters->AddParameterStochastic("OuterKeepingIntensity", NormalDistribution(0.6, 0.15, 0.0, 1.0));
  parameters->AddParameterStochastic("EgoLaneKeepingIntensity", NormalDistribution(0.2, 0.15, 0.0, 1.0));
  parameters->AddParameterDouble("RightOvertakingProhibitionIgnoringQuota", 0.02);
  parameters->AddParameterDouble("LaneChangeProhibitionIgnoringQuota", 0.03);
  parameters->AddParameterStochastic("VWish", LogNormalDistribution(36.11, 4.17, 22.22, 44.28));
  parameters->AddParameterStochastic("DeltaVWish", LogNormalDistribution(7.0, 2.0, 3.0, 12.0));
  parameters->AddParameterStochastic("DeltaVViolation", LogNormalDistribution(2.78, 0.69, 0.0, 21.56));
  parameters->AddParameterStochastic("DesiredTimeAtTargetSpeed", NormalDistribution(15.0, 2.0, 10.0, 20.0));
  parameters->AddParameterDouble("ProportionalityFactorForFollowingDistance", 7.0);
  parameters->AddParameterStochastic("MaxLongitudinalAcceleration", NormalDistribution(4.0, 0.5, 3.0, 7.0));
  parameters->AddParameterStochastic("MaxLongitudinalDeceleration", NormalDistribution(7.0, 0.5, 6.0, 10.0));
  parameters->AddParameterStochastic("MaxLateralAcceleration", NormalDistribution(4.5, 0.5, 3.5, 10.0));
  parameters->AddParameterStochastic("ComfortLongitudinalAcceleration", NormalDistribution(0.537, 0.216, 0.468, 0.606));
  parameters->AddParameterStochastic("ComfortLongitudinalDeceleration", NormalDistribution(1.099, 0.278, 1.012, 1.185));
  parameters->AddParameterStochastic("AccelerationAdjustmentThresholdVelocity", NormalDistribution(25.0, 0.2, 22.2, 27.78));
  parameters->AddParameterStochastic("VelocityForMaxAcceleration", NormalDistribution(8.33, 0.2, 5.55, 11.11));
  parameters->AddParameterDouble("MaxComfortAccelerationFactor", 4.0);
  parameters->AddParameterDouble("MaxComfortDecelerationFactor", 2.0);
  parameters->AddParameterStochastic("ComfortLateralAcceleration", NormalDistribution(2.5, 0.3, 1.5, 3.09));
  parameters->AddParameterStochastic("CarQueuingDistance", NormalDistribution(2.9, 1.1, 1.0, 4.0));
  parameters->AddParameterStochastic("HysteresisForRestart", NormalDistribution(1.5, 2.0, 0.1, 5.0));
  parameters->AddParameterDouble("LateralOffsetNeutralPositionQuota", 0.2);
  parameters->AddParameterStochastic("LateralOffsetNeutralPosition", NormalDistribution(0.0, 0.3, -1.0, 1.0));
  parameters->AddParameterStochastic("LateralOffsetNeutralPositionRescueLane", NormalDistribution(0.75, 0.25, 0.3, 1.0));
  parameters->AddParameterStochastic("MaximumAngularVelocitySteeringWheel", NormalDistribution(3.142, 0.349, 2.443, 3.840));
  parameters->AddParameterStochastic("ComfortAngularVelocitySteeringWheel", NormalDistribution(1.047, 0.175, 0.873, 1.222));
  parameters->AddParameterStochastic("TimeHeadwayFreeLaneChange", NormalDistribution(7.0, 2.0, 5.0, 10.0));
  parameters->AddParameterDouble("OuterKeepingQuota", 0.8);
  parameters->AddParameterDouble("EgoLaneKeepingQuota", 0.8);
  parameters->AddParameterDouble("UseShoulderInTrafficJamQuota", 0.0);
  parameters->AddParameterDouble("UseShoulderInTrafficJamToExitQuota", 0.0);
  parameters->AddParameterDouble("UseShoulderWhenPeerPressuredQuota", 0.0);
  parameters->AddParameterDouble("DistractionPercentage", 0.0);
  parameters->AddParameterDouble("DistractionQuota", 0.0);
  return ConsolidateScmParameters(parameters.get());
}

inline common::vehicle::properties::EntityProperties GenerateDefaultVehicleParameter()
{
  using namespace scm::common;
  vehicle::properties::EntityProperties properties;

  properties.model = "car_ncap";

  properties.bounding_box = BoundingBox{Vec3<double>(1.25, 0.0, 0.84), Dimension3{3.96_m, 2.04_m, 1.68}};

  properties.properties[vehicle::properties::AirDragCoefficient] = "0.3";
  properties.properties[vehicle::properties::AxleRatio] = "3.0";
  properties.properties[vehicle::properties::DecelerationFromPowertrainDrag] = "0.5";
  properties.properties[vehicle::properties::FrictionCoefficient] = "1.0";
  properties.properties[vehicle::properties::FrontSurface] = "1.9";

  std::string gearRatioBase(vehicle::properties::GearRatio);
  properties.properties[gearRatioBase + "1"] = "4.350";
  properties.properties[gearRatioBase + "2"] = "2.496";
  properties.properties[gearRatioBase + "3"] = "1.665";
  properties.properties[gearRatioBase + "4"] = "1.230";
  properties.properties[gearRatioBase + "5"] = "1.0";
  properties.properties[gearRatioBase + "6"] = "0.851";
  properties.properties[vehicle::properties::NumberOfGears] = "6";

  properties.properties[vehicle::properties::MaximumEngineSpeed] = "6000.0";
  properties.properties[vehicle::properties::MaximumEngineTorque] = "180.0";
  properties.properties[vehicle::properties::MinimumEngineSpeed] = "900.0";
  properties.properties[vehicle::properties::SteeringRatio] = "10.7";
  properties.properties[vehicle::properties::AxleRatio] = "1.0";
  properties.properties[vehicle::properties::FrontAxleMaxSteering] = "1.0";

  properties.wheelBase = 2.565;
  properties.mass = 3000.0_kg;
  properties.classification = VehicleClass::kCompact_car;

  properties.performance.max_speed = 50.0_mps;
  properties.performance.max_acceleration = 9.80665;
  properties.performance.max_deceleration = 9.80665;

  properties.front_axle.bb_center_to_axle_center = Vec3(2.52, 0.0, 0.341);
  properties.front_axle.max_steering = 0.5282_rad;
  properties.front_axle.track_width = 1.8_m;
  properties.front_axle.wheel_diameter = 0.682_m;

  properties.rear_axle.bb_center_to_axle_center = Vec3(0.0, 0.0, 0.341);
  properties.rear_axle.max_steering = 0.0_rad;
  properties.rear_axle.track_width = 1.8_m;
  properties.rear_axle.wheel_diameter = 0.682_m;
  return properties;
}

}  // namespace scm
