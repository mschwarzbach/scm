/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>

#include "include/common/Parameters.h"
#include "include/common/StochasticDefinitions.h"
#include "include/common/VehicleProperties.h"
#include "src/FMU/src/OsmpShell/FmiParameterInterface.h"
#include "src/Logging/Logging.h"

namespace fmi::parameter
{
static const std::string driverPrefix = "Driver.";
static const std::string vehiclePrefix = "Vehicle.";
static const std::string simulationPrefix = "Simulation.";

scm::parameter::PartitionedFmiParameters PartitionFmiParameters(const FmiParameters& parameters)
{
  FmiParameters driverParameters{};
  FmiParameters vehicleParameters{};
  FmiParameters simulationParameters{};

  for (const auto& [key, value] : parameters)
  {
    if (key.find(fmi::parameter::driverPrefix) != std::string::npos)
    {
      driverParameters[key] = value;
    }
    else if (key.find(fmi::parameter::vehiclePrefix) != std::string::npos)
    {
      vehicleParameters[key] = value;
    }
    else if (key.find(fmi::parameter::simulationPrefix) != std::string::npos)
    {
      simulationParameters[key] = value;
    }
  }
  return {driverParameters, vehicleParameters, simulationParameters};
}

scm::parameter::StochasticDistribution CreateDistributionFromParameters(const std::string& paramName, const FmiParameters& parameters)
{
  using namespace scm::parameter;
  StochasticDistribution distribution;
  auto mean = std::get<double>(parameters.at(paramName + ".mean"));
  auto sd = std::get<double>(parameters.at(paramName + ".sd"));
  auto min = std::get<double>(parameters.at(paramName + ".min"));
  auto max = std::get<double>(parameters.at(paramName + ".max"));
  auto distributionType = std::get<std::string>(parameters.at(paramName + ".type"));

  if (distributionType == "NormalDistribution")
  {
    distribution = NormalDistribution(mean, sd, min, max);
  }
  else if (distributionType == "LogNormalDistribution")
  {
    distribution = LogNormalDistribution::CreateWithMeanSd(mean, sd, min, max);
  }
  else
  {
    throw std::out_of_range(distributionType + " is not a supported distribution type.");
  }
  return distribution;
}

scm::parameter::ParameterMapping AddDistributionsToMapping(const ::std::map<std::string, scm::parameter::StochasticDistribution>& distributions, scm::parameter::ParameterMapping mapping)
{
  for (const auto& [key, value] : distributions)
  {
    auto scmParameterKey = key.substr(fmi::parameter::driverPrefix.length());  // scm does not have the 'Driver.' prefix for its parameter
    mapping[scmParameterKey] = value;
  }
  return mapping;
}

scm::parameter::ParameterMapping CreateMappingFromFmiDriverParameters(const FmiParameters& parameters)
{
  using namespace scm::parameter;
  std::map<std::string, StochasticDistribution> distributions;
  ParameterMapping parameterMapping;
  static const auto prefixOffset = fmi::parameter::driverPrefix.length() + 1;
  for (const auto& [key, value] : parameters)
  {
    auto index = key.substr(prefixOffset).find('.');  // if there is another "." found -> parameter should follow distribution convention
    if (index != std::string::npos)
    {
      auto paramName = key.substr(0, prefixOffset + index);

      if (distributions.count(paramName) == 0)  // create distribution only once
      {
        try
        {
          distributions[paramName] = CreateDistributionFromParameters(paramName, parameters);
        }
        catch (const std::out_of_range& e)
        {
          std::string message = key +
                                " is not a properly defined distribution.\n"
                                " A distribution has to provide its parameter individually with a postfix (<x>_mean, <x>_sd, <x>_min, <x>_max).\n" +
                                e.what();
          Logging::Warning(message);
        }
      }
    }
    else
    {
      auto scmParameterKey = key.substr(fmi::parameter::driverPrefix.length());  // scm parameter do not have the 'Driver.' prefix
      if (std::holds_alternative<double>(value))
      {
        parameterMapping[scmParameterKey] = std::get<double>(value);
      }
      else if (std::holds_alternative<bool>(value))
      {
        parameterMapping[scmParameterKey] = std::get<bool>(value);
      }
      else if (std::holds_alternative<int>(value))
      {
        parameterMapping[scmParameterKey] = std::get<int>(value);
      }
    }
  }

  return AddDistributionsToMapping(distributions, parameterMapping);
}

template <typename T>
std::optional<T> Get(const FmiParameters& parameters, const std::string& key)
{
  const auto value = scm::common::map::query(parameters, key);
  if (value.has_value() && std::holds_alternative<T>(value.value()))
  {
    return std::get<T>(value.value());
  }
  return std::nullopt;
}

bool CheckIfAllParametersHaveZeroValue(const FmiParameters& parameters)
{
  return std::all_of(parameters.begin(), parameters.end(), [](const auto& parameter)
                     { if(std::holds_alternative<double>(parameter.second))
                       {
                         return std::get<double>(parameter.second) == 0;
                       }
                       if(std::holds_alternative<int>(parameter.second))
                       {
                         return std::get<int>(parameter.second) == 0;
                       }
                       return false; });
}

scm::common::vehicle::properties::EntityProperties ParseVehicleParameter(const FmiParameters& parameters, scm::common::vehicle::properties::EntityProperties vehicleProperties)
{
  if (CheckIfAllParametersHaveZeroValue(parameters))
  {
    return vehicleProperties;
  }
  auto steeringRatioKey = fmi::parameter::vehiclePrefix + scm::common::vehicle::properties::SteeringRatio;
  if (parameters.count(steeringRatioKey) > 0)
  {
    auto steeringRatio = Get<double>(parameters, steeringRatioKey).value();
    vehicleProperties.properties[scm::common::vehicle::properties::SteeringRatio] = std::to_string(steeringRatio);
  }

  auto maxSteeringKey = fmi::parameter::vehiclePrefix + "MaxSteering";
  if (parameters.count(maxSteeringKey) > 0)
  {
    vehicleProperties.front_axle.max_steering = units::angle::radian_t(std::get<double>(parameters.at(maxSteeringKey)));
  }

  auto numberOfGears = fmi::parameter::vehiclePrefix + scm::common::vehicle::properties::NumberOfGears;
  if (parameters.count(numberOfGears) > 0)
  {
    vehicleProperties.properties[scm::common::vehicle::properties::NumberOfGears] = std::to_string(std::get<int>(parameters.at(numberOfGears)));

    const auto gearRatio = fmi::parameter::vehiclePrefix + scm::common::vehicle::properties::GearRatio;

    auto gearRatioFilter = [](const auto& item)
    { return item.first.find(scm::common::vehicle::properties::GearRatio) != std::string::npos; };

    scm::common::erase_if(vehicleProperties.properties, gearRatioFilter);

    for (int gear = 1; gear <= std::get<int>(parameters.at(numberOfGears)); ++gear)
    {
      auto propertiesKey = scm::common::vehicle::properties::GearRatio + std::to_string(gear);
      auto parametersKey = gearRatio + std::to_string(gear);
      vehicleProperties.properties[propertiesKey] = std::to_string(std::get<double>(parameters.at(parametersKey)));
    }
  }
  return vehicleProperties;
}

}  // namespace fmi::parameter