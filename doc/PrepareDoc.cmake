################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

macro(copy_documentation source destination)
    message(VERBOSE "Copy ${source} to ${destination}")
    file(COPY ${source} DESTINATION ${destination})
endmacro()

copy_documentation(${SRC} ${DST})