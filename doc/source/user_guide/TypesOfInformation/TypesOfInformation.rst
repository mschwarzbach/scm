..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _types_of_information_:

********************
Types of Information
********************

An SCM driver continuously perceives a large amount of information about their own vehicle, other traffic participants and the environment.
This information is processed, sorted and finally stored in the driver's *mental model* of the world.
The following chapter aims to give an overview of the different pieces of information the model processes and remembers.

The relevant information can be thematically sorted into a number of groups as described in the following list.
This division also mirrors how the internal handling of the data in the implementation is structured. 

* :ref:`microscopic_information_` contains all state variables and parameters that can be assigned to individual objects (e.g. ego vehicle, surrounding vehicles and objects).
  Information about the driver themself is also included.

* :ref:`mesoscopic_information_` contains information regarding moving traffic which can only be obtained by viewing a group of objects (e.g. average speeds per lane).

* :ref:`infrastructure_information_` contains information about the traffic infrastructure; it is divided into structural properties of the roadway (e.g. lane widths, lane ends) and traffic rules (e.g. traffic signs, lane markings).

Furthermore, the model has access to a variety of user-adjustable parameters that influence the SCM driver's behaviour.
An overview for them is given in :ref:`driver_parameters_`.


.. toctree::
  :glob:
  :hidden:

  MesoscopicInformation/*
  MicroscopicInformation/*
  InfrastructureInformation/*
  */*
