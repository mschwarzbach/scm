..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _microscopic_information_:

***********************
Microscopic Information
***********************

A data storage for all information on a microscopic traffic level describes the mental representation of the driver's environment.
Microscopic traffic data in SCM is characterised as being associated with a single road user.
The microscopic characteristics processed by the model may relate to the driver's own vehicle or any perceived surrounding object.
Included in the microscopic characteristics are several kinds of mental information of the driver relating to evaluation and control mechanisms of various cognitive processes.
These three types of information are further described below.


Information about a single AreaOfInterest
-----------------------------------------

Two pieces of information about each AreaOfInterest in isolation are remembered:

  * an indicator if any traffic participant is visible in in a certain AreaOfInterest
  * whether or not an object in the AreaOfInterest is classified as an obstacle (moving very slowly or not at all)


Information about a single AreaOfInterest in relation to own vehicle or own lane
--------------------------------------------------------------------------------

A large part of the information the ego agent processes is concerned with AOIs or agents within them may become relevant for interaction with ego.


Features in consideration of orientation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A number of indicators are available to describe the positioning of an object perceived in an AreaOfInterest in relation to the ego vehicle or the ego lane:

  * Is the vehicle besides the ego vehicle according to extrapolated distance?
  * Is the vehicle in the side lane of the ego vehicle?
    And more specifically, is it in the lane to the left or right of the ego lane?
  * Is a collision course detected?
    This is determined by the time gap or the time to collision (depending on the speed range) being below a minimum threshold.
    (A more detailed explanation is given below in :ref:`detection_collision_courses`.)
  * Is the observed object directly to the left or the right of the ego agent?
  * Is an object in the AreaOfInterest suspicious by being a lot slower than either ego, its leading vehicle or the mean lane velocity (see :ref:`mesoscopic_information_`)?
    Being in a traffic jam negates the suspiciousness for being slow.
  * Is an object in the AreaOfInterest noticed as suspiciously slow, identified as an obstacle or involved in a collision?


.. _detection_collision_courses:

Detection of collision courses
""""""""""""""""""""""""""""""

..
  TODO ref to tau dot explanation
  TODO The whole content of this subsection seems very questionable. The described behaviour may likely not be correct. It was also necessary to alter the text significantly to make it readable.

The mechanism for *detecting a collision course* is used to determine whether the time gap or the time to collision is below the minimum threshold.
The speed range of the AOI vehicle determines whether the time gap or TTC is used to decide.
It not only determines actual collision courses (i.e. future collisions with a vehicle directly ahead or behind the ego vehicle) but also indicates to collision courses which possibily occur if an agent in an adjacent lane starts a lane change leading to a collision.

To detect collisions in the front area (EGO_FRONT, EGO_FRONT_FAR, LEFT_FRONT, LEFT_FRONT_FAR, RIGHT_FRONT, RIGHT_FRONT_FAR) and rear area (EGO_REAR, LEFT_REAR, RIGHT_REAR) there are two critera:
TTC and tauDot (:math:`\tau` :math:`\tau_{d}`, which are further explained in *old ref: scm_commons_* TODO).
Additionally, the condition that the vehicle in the AOI is visible needs to be fulfilled.

A possible collision can be detected when :math:`\tau` is greater than 0 and simultaneously the perceived value of :math:`\tau_{d}` is smaller or equal to -0.5.
At this threshold, a full stop by the ego vehicle would at least induce a contact with the observed vehicle, which is of course not desired.

Another possibilty for a detected case would be a value of :math:`\tau` smaller than 0, which means the relative distance of the vehicles increases.
An exception for a detected collision course exists for a relative acceleration smaller than 0, which induces the relative velocity to become smaller than 0 as the situation progresses.
This leads to an approach towards the vehicle in the AOI.
From a kinematic point of view the ego vehicle and the observed vehicle are on a collision course; the value of :math:`\tau_{d}` is smaller than -1 in such a situation.

To detect collision courses in the side areas (LEFT_SIDE, RIGHT_SIDE) there are primarily two criteria to evaluate:
Firstly, the visibility of vehicle on the left or right side, and secondly, the lateral velocity of the adjacent vehicle away from ego lane.
An observed vehicle's lateral motion is recognizable above a certain threshold.

Furthermore, if a collision course is detected, it is necessary to assess if the ego vehicle is able to escape from the collision due to the lateral motion of the observed vehicle away from the ego lane.
This depends on the time needed by the vehicle in the side lane to cross over to the ego lane.
The ego vehicle is able to escape the potential collision if the TTC plus additional time used as a spacing (which is defined as 0.5 seconds at the moment) is greater than the time needed by side vehicle for crossing into the ego lane.

SCM uses a *lateral motion threshold* for the movement of observed vehicles.
It determines whether the lateral velocity of a vehicle in the side lane is recognized by the ego agent as a motion towards the ego lane.
The threshold is defined as a change in the angle between the motion vectors of the two vehicles of 0.003 rad/s, simplified by calculating the quotient of the lateral velocity and the distance between the vehicles (see :numref:`image_lateralmotionandthreshold_`).
Values above this are regarded as motion recognizable by the human eye and interpreted as a lane change of the observed vehicle.

.. _image_lateralmotionandthreshold_:

.. figure:: ../_static/images/lateralmotionandthreshold.png
   :alt: Lateral motion threshold

   Lateral motion threshold


Features with regard to lane change behavior
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some features provide information with regard to lane changes and their safety:

  * Does the observed vehicle intend to or can it be anticipated to cross into the ego lane?
  * Is the agent in the observed AOI changing into the ego lane, considering lateral position and speed respectively to ego lane?
  * If the vehicle is changing into the ego lane, will it be close enough to the ego agent to be dangerous?
  * Does a vehicle that is not in a side AOI have a recognizable lateral motion?
    If so, does it move towards or away from the ego lane?
  * Is an observed vehicle driving two lanes away from the ego lane ("side side lane") changing into a lane adjacent to the one of the ego vehicle?
  * Can ego change into a neighboring lane to evade the end of its current lane?


Features with regard to vehicle following behavior
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If a vehicle ahead of ego meets certain conditions, the SCM driver starts using the vehicle following behaviour for their longitudinal guidance.
In this case, the AreaOfInterest occupied by the leading vehicle chosen is marked in ego's mental model.
Further indicators are available to determine the longitudinal control in following behaviour:

  * Is the minimum following distance is violated?
    This includes different thresholds based on the urgency of a situation.
  * Is the vehicle in the AOI near enough for following?
    This includes different thresholds based on the urgency of a situation.
  * Is the influencing following distance violated?
  * Is the driving the desired speed on the lane corresponding to an AOI not feasible for a sufficient duration?
  * Is the observed agent driving slower than a threshold for traffic jam velocity?

..
  TODO something about time to brake here, no idea how this fits into the text
  "SCM uses a calculation model for the TTB (time to brake) concepts."

When following another vehicle, SCM calculates values for three key distances.
They are used in the longitudinal control to maintain a safe gap to the leading vehicle.
These distances may also be expressed as time headway, i.e. the time needed by the vehicle to overcome the current relative distance to the front vehicle.
An overview is shown in :numref:`image_threecasesoftimeheadway_` and they are explained in the following paragraphs:

.. _image_threecasesoftimeheadway_:

.. figure:: ../_static/images/threecasesoftimeheadway.png
   :alt: Three distance values for following driving

   Three distance values for following driving


Minimum Following Distance
""""""""""""""""""""""""""

This value represents a minimum necessary to ensure there is not an immediate risk of collision with the vehicle ahead.
It is calculated in different ways, depending on whether the speed of ego vehicle is currently under jam velocity (defined as 40 km/h).
If ego vehicle is moving slower than the threshold, the minimum distance to the front vehicle and the extrapolated relative net distance to front vehicle are compared.
Otherwise a calculated minimum time gap and the extrapolated time gap to the vehicle ahead are assessed.
In both situations the minimal following distance is violated if the extrapolated value is smaller then the calculated value.
Both methods include a stochasticly generated urgency factor for the current situation in the calculated minimum.


Maximum Following Distance
""""""""""""""""""""""""""

.. TODO what is the definition of maximum following distance and influencing following distance? (first sentence)

The Maximum Following Distance is the range in which the ego vehicle can fully engage in "following behaviour" and make its speed mostly dependent on that of the leading vehicle.
Similarly to the Minimum Following Distance, it also differentiates between velocities over and under the jam velocity limit.
At speeds above the threshold, an equilibrium time headway where the ego vehicle follows with a convenient gap is calculated.
Otherwise, the extrapolated distance to front vehicle is simply used for this.
For this value, equilibrium time headway is extended somewhat with the addition of 0.5 m (under jam velocity) or 0,5 s (over jam velocity) respectively.
The ego vehicle is regarded as following if the extrapolated distance is under the extended equilibrium time headway modified by the given urgency factor.


Following distance by which the braking behaviour could be influenced
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

..
  TODO what is the definition of maximum following distance and influencing following distance? (first sentence)
  TODO what are the criterea for this value? the text doesnt make much sense

A vehicle ahead of the ego agent, even when beyond the Maximum Following Distance by some margin, may still affect its braking behaviour, which is described by this value.
Its calculation requires the influencing distance and the gap to the front vehicle.
The influencing distance is violated if the calculated value of influencing distance overlaps with the extrapolated gap.
As before, it is also necessary to divide the situation into "under jam velocity" and "over jam velocity".


Features with regard to merge mechanism
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model is able to estimate whether there is enough time to pass a vehicle without collision at a secure distance.
This can be done based on the observed movement of another agent or the anticipated movement of an agent that is likely start a lane change soon.


Features with regard to swerve mechanism
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To enable the swerve mechanism there are a few features that need to be extracted.
First of all, it is necessary to determine if a swerving maneuver is currently possible.
For this purpose, the times to swerve for each side are calculated.
Secondly, the intensities for swerving manoeuvres are determined as described in :ref:`decision_making_`.
The key factors for the decision are:

* Is evading safe with respect to other vehicles?
* Does the side lane ego plans to swerve into exist?
* Has the swerved target been completely passed by the ego vehicle?


Information about two AreasOfInterest in relation to each other
-----------------------------------------------------------------

If there are two adjacent AOIs on the same lane and both contain a vehicle, a feature exists to check whether the near vehicle is approaching the far vehicle.


Information related to gaze allocation
--------------------------------------

SCM remembers the information from the current state of the :ref:`gaze_control_`:

* The *current fovea* is a single AreaOfInterest which represents the current gaze fixation target.
* The *current useful field of view* encompasses a number of AreasOfInterest.
  The AOIs can vary in number and depend on the current gaze fixation target.
  (see :ref:`gaze_control_` for a more detailed explanation)
