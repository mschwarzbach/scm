..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************
.. _scm_enums_:

ScmEnums
########

TODO - move into the code

ScmEnums is a header file, which contains several enumerated lists of verbalized
state declarations, which are used throughout several submodules of SCM. For
instance, the perceived situation of the driver can be distinguished by several
situation patterns, which are identified by specific names. These names are
gathered into a class *situation* of all possible situation patterns, which is
an enumerated list. Enumerated lists provide the advantage that each entry can
be identified by a specific integer (usually, the position within the order of
the list). Therefore, the developer can make us of the better understandable
string declarations, but the program itself only deals in integers.

The section below gives an overview about the enumerated lists,
which are currently declared in this header file and links to the submodules,
where they are primarily used.


.. _scm_enums_critical_action_states_:

CriticalActionState
===================

CriticalActionState is a list of possible actions that the driver can choose
from to prevent collisions within critical situations. The list contains the
following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - COLLISION_UNAVOIDABLE
     - A collision is not avoidable under all circumstances
   * - 1
     - BRAKING
     - A collision can only be avoided by a braking reaction
   * - 2
     - EVADING_LEFT
     - A collision can only be avoided by an evading reaction. This state evaluates the possibility to evade to the left of the collision object
   * - 3
     - EVADING_RIGHT
     - A collision can only be avoided by an evading reaction. This state evaluates the possibility to evade to the right of the collision object
   * - 4
     - BRAKING_OR_EVADING
     - A collision can be avoided by either braking or evading reactions


The list is mainly used for the selection of action patterns for lateral
guidance in the submodule *old ref: action_manager_*.


FieldOfViewAssignment
=====================

FieldOfViewAssignment is a list of the parts of the driver's field of view (see :ref:`gaze_control_`
for further explanations), which is used to indicate the quality of visual
information implemented in the submodule :ref:`microscopic_information_`
by stating, over which part the information in an AreaOfInterest (see also :ref:`gaze_control_`
for further explanations) was updated last. The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - FOVEA
     - The information in the AreaOfInterest was lastly perceived by direct gaze fixation
   * - 1
     - USEFUL_FIELD_OF_VIEW
     - The information in the AreaOfInterest was lastly perceived by overlap with the useful field of view (reduced quality)
   * - 2
     - PERIPHERY
     - The information in the AreaOfInterest was lastly perceived by overlap with the outer periphery (largely reduced quality, mostly down to "there still is an object")
   * - 3
     - NONE
     - The AreaOfInterest was not yet scanned by any part of the field of view or the object in the AreaOfInterest has left the preview range of the driver


The list is used throughout the perception mechanisms between the submodules *old ref: information_acquisition_* and
*old ref: mental_model*, more specifically :ref:`microscopic_information_`.


GazeState
=========

GazeState is a list of states, which the gaze behaviour algorithm in the submodule
:ref:`gaze_control_` can have. It is mostly
identical with the AreasOfInterest (see :ref:`gaze_control_`
for further explanations). The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - SACCADE
     - The gaze algorithm is currently executing a change in gaze target
   * - 1
     - EGO_FRONT
     - The gaze algorithm is currently fixating the AreaOfInterest EGO_FRONT
   * - 2
     - EGO_FRONT_FAR
     - The gaze algorithm is currently fixating the AreaOfInterest EGO_FRONT_FAR
   * - 3
     - RIGHT_FRONT
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHT_FRONT
   * - 4
     - RIGHT_FRONT_FAR
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHT_FRONT_FAR
   * - 5
     - LEFT_FRONT
     - The gaze algorithm is currently fixating the AreaOfInterest LEFT_FRONT
   * - 6
     - LEFT_FRONT_FAR
     - The gaze algorithm is currently fixating the AreaOfInterest LEFT_FRONT_FAR
   * - 7
     - EGO_REAR
     - The gaze algorithm is currently fixating the AreaOfInterest EGO_REAR
   * - 8
     - RIGHT_REAR
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHT_REAR
   * - 9
     - LEFT_REAR
     - The gaze algorithm is currently fixating the AreaOfInterest LEFT_REAR
   * - 10
     - RIGHT_SIDE
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHT_SIDE
   * - 11
     - LEFT_SIDE
     - The gaze algorithm is currently fixating the AreaOfInterest LEFT_SIDE
   * - 12
     - UNDEFINED
     - Dummy value, which is used for exception handling
   * - 13
     - INSTRUMENT_CLUSTER
     - The gaze algorithm is currently fixating the instrument cluster of the vehicle (e.g. fixating the speedometer)
   * - 14
     - INFOTAINMENT
     - The gaze algorithm is currently fixating the infotainment screen of the vehicle
   * - 15
     - HUD
     - The gaze algorithm is currently fixating the head-up display of the vehicle
   * - 16
     - RIGHTRIGHT_FRONT
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHTRIGHT_FRONT
   * - 17
     - LEFTLEFT_FRONT
     - The gaze algorithm is currently fixating the AreaOfInterest LEFTLEFT_FRONT
   * - 18
     - RIGHTRIGHT_REAR
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHTRIGHT_REAR
   * - 19
     - LEFTLEFT_REAR
     - The gaze algorithm is currently fixating the AreaOfInterest LEFTLEFT_REAR
   * - 20
     - RIGHTRIGHT_SIDE
     - The gaze algorithm is currently fixating the AreaOfInterest RIGHTRIGHT_SIDE
   * - 21
     - LEFTLEFT_SIDE
     - The gaze algorithm is currently fixating the AreaOfInterest LEFTLEFT_SIDE
   * - 22
     - NumberOfGazeStates
     - Length value of the enumerated list, which is mostly used in for-loops


The list is mostly used in the gaze behaviour algorithm of the submodule :ref:`gaze_control_`.


HighCognitiveSituation
======================

HighCognitiveSituation is a list of specific situation patterns, which is only
used by the submodule :ref:`high_cognitive_`.
The situations regard the positions and movements of the next two front vehicles
in the adjacent right lane of the driver (see AreasOfInterest RIGHT_FRONT and
RIGHT_FRONT_FAR in :ref:`gaze_control_`.
The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - UNDEFINED
     - Dummy value, which is used if no high-cognitive related situation was detected
   * - 1
     - APPROACH
     - An approach between the two vehicles is perceptable
   * - 2
     - SALIENT_APPROACH
     - An approach between the two vehicles is perceptable and the time headway between the two vehicles is rather small
   * - 3
     - FOLLOW
     - There is no approach between the two vehicles (or it is not perceptable)
   * - 4
     - SALIENT_FOLLOW
     - There is no approach between the two vehicles (or it is not perceptable) and the time headway between the two vehicles is rather small



LaneExistence
=============

LaneExistence is kind of an extended Boolean declaration, according to the
existence of a lane. It has an additional state, which allows the interpretation
that the information is unknown. The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - UNKNOWN
     - The existence of a specific lane cannot be specified
   * - 1
     - TRUE
     - A specific lane definitely exists
   * - 2
     - FALSE
     - A specific lane definitely does not exist


The list is used all over SCM, for instance in the submodules *old ref: information_acquisition_*
and *old ref: mental_model*, more specifically :ref:`microscopic_information_` and *old ref: feature_extractor_*

.. _scm_enums_lateral_action_states_:

LateralActionState
==================

LateralActionState is a list of possible action patterns that the driver can
choose from for the lateral guidance of the vehicle. The list contains the
following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - LANE_KEEPING
     - The driver tries to keep his vehicle in its current lane and has no intention to change lanes
   * - 1
     - INTENT_TO_CHANGE_LANES_LEFT
     - The driver wants to change to his left lane, but he is blocked by other vehicles or does not yet know, if a lane change is possible
   * - 2
     - INTENT_TO_CHANGE_LANES_RIGHT
     - The driver wants to change to his right lane, but he is blocked by other vehicles or does not yet know, if a lane change is possible
   * - 3
     - LANE_CHANGING_LEFT
     - The driver is making a comfortable lane change to his left lane
   * - 4
     - LANE_CHANGING_RIGHT
     - The driver is making a comfortable lane change to his right lane
   * - 5
     - PREPARING_TO_MERGE_LEFT
     - The driver is preparing to merge into the traffic on his left lane
   * - 6
     - PREPARING_TO_MERGE_RIGHT
     - The driver is preparing to merge into the traffic on his right lane
   * - 7
     - URGENT_SWERVING_LEFT
     - The driver is performing an emergency evading manoeuvre to the left
   * - 8
     - URGENT_SWERVING_RIGHT
     - The driver is performing an emergency evading manoeuvre to the right
   * - 9
     - COMFORT_SWERVING_LEFT
     - The driver is performing a planned evading manoeuvre (start and finish in the current ego lane) to the left
   * - 10
     - COMFORT_SWERVING_RIGHT
     - The driver is performing a planned evading manoeuvre (start and finish in the current ego lane) to the right


The list is used all over SCM, but mostly for the lateral action decision in the
submodule *old ref: action_manager_*.

.. _scm_enums_longitudinal_action_states_:

LongitudinalActionState
=======================

LongitudinalActionState is a list of possible action patterns that the driver
can choose from for the longitudinal guidance of the vehicle. The list contains
the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - SPEED_ADJUSTMENT
     - The driver tries to maintain his target velocity
   * - 1
     - BRAKING_TO_END_OF_LANE
     - The driver is performing a target braking towards the end of his current lane
   * - 2
     - APPROACHING
     - The driver approaches another leading vehicle, but is not yet in a car following task
   * - 3
     - FOLLOWING_AT_DESIRED_DISTANCE
     - The driver tries to maintain a desired distance to a specific leading vehicle
   * - 4
     - FALL_BACK_TO_DESIRED_DISTANCE
     - The driver tries to maintain a desired distance to a specific leading vehicle after the minimum following distance was violated
   * - 5
     - STOPPED
     - The vehicle has permanently stopped after a collision


The list is used all over SCM, but mostly for the longitudinal action decision
in the submodule *old ref: action_manager_*.


MinThwPerspective
=================

MinThwPerspective is a list of possible constellations and conditions for the
calculation of the minimum following time headway. The list contains the
following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - NORM
     - The driver evaluates the current situation and state variables
   * - 1
     - EGO_ANTICIPATED_FRONT
     - The driver anticipates a future traffic constellation, in which he will drive in front of the observed vehicle
   * - 2
     - EGO_ANTICIPATED_REAR
     - The driver anticipates a future traffic constellation, in which he will drive behind of the observed vehicle


The list is used within the algorithm for merging into the traffic on other
lanes, which is part of the submodule *old ref: mental_model*.


Risk
====

Risk is a list of names of risk level cluster for different situation patterns.
The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 1
     - LOW
     - The situation has no specific implication on the driver's current behaviour
   * - 2
     - AVERAGE
     - The situation is characterised by some kind of inconvenience for the driver
   * - 4
     - HIGH
     - The situation has either a latent or an actual risk to end up in a collision


The list is used within the situation evaluation algorithm of the submodule
*old ref: situation_manager_*.


Situation
=========

Situation is a list of possible situation patterns that the driver can choose
from for the determination, in which traffic situation he currently is. The list
contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - UNDEFINED
     - Dummy value, mostly used for exception handling
   * - 1
     - FREE_DRIVING
     - The driver has no other vehicle around him, which influences him in any way
   * - 2
     - FOLLOWING_DRIVING
     - The driver has a leading vehicle in front of him in his current lane
   * - 3
     - SUSPICIOUS_OBJECT_IN_LEFT_LANE
     - The driver has detecet a suspicious object (e.g. obstacle or a lot slower than average lane speed) in the lane to his left
   * - 4
     - SUSPICIOUS_OBJECT_IN_RIGHT_LANE
     - The driver has detecet a suspicious object (e.g. obstacle or a lot slower than average lane speed) in the lane to his right
   * - 5
     - LANE_CHANGER_FROM_LEFT
     - The driver perceives that the next vehicle in front of him on the left lane is performing or might (anticipating) lane change towards his current lane
   * - 6
     - LANE_CHANGER_FROM_RIGHT
     - The driver perceives that the next vehicle in front of him on the right lane is performing or might (anticipating) lane change towards his current lane
   * - 7
     - CURRENT_LANE_BLOCKED
     - The driver approaches the end of his current lane
   * - 8
     - MANDATORY_EXIT
     - The driver perceives that the he is approaching his designated exit from the motorway (not yet implemented in SCM)
   * - 9
     - QUEUED_TRAFFIC
     - The driver is currently in a traffic jam
   * - 10
     - SIDE_COLLISION_RISK_FROM_RIGHT
     - The driver perceives a risk of collision with the vehicle right beside him
   * - 11
     - SIDE_COLLISION_RISK_FROM_LEFT
     - The driver perceives a risk of collision with the vehicle left beside him
   * - 12
     - OBSTACLE_ON_CURRENT_LANE
     - The driver perceives an obstacle ahead on his current lane
   * - 13
     - COLLISION
     - The driver is involved in an accident
   * - 14
     - NumberOfSituations
     - Length value of the enumerated list, which is mostly used in for-loops


The list is used all over SCM, mostly within the situation evaluation algorithm
of the submodule *old ref: situation_manager_*.


.. _side_aoi_criteria_:

SideAoiCriteria
===============

Since side AOI may contain several objects there must be a criteria used to determine whose data is processed in subsequent functions. To achieve this the list of SideAoiCriteria was implemented.
The list contains the following entries:

.. list-table::
   :header-rows: 1

   * - Numeration
     - List entry
     - Explanation
   * - 0
     - NONE
     - Dummy value, throws a runtime error; mostly used as standard value for functions to detect places where a SideAoiCriteria has to be defined
   * - 1
     - FRONT
     - The first object in the vector, representing the object longitudinally furthest to the front in the direction of travel
   * - 2
     - REAR
     - The last object in the vector, representing the object longitudinally furthest to the back in the direction of travel
   * - 3
     - CLOSEST
     - The object whose front center (i.e. center of the front of the bounding box) is closest to the ego vehicle laterally
   * - 4
     - FARTHEST
     - The object whose front center is furthest to the ego vehicle laterally
   * - 5
     - ALL_MAX
     - The highes value of all objects in the AOI. SideAoiCriteria must be handled individually in each function depending on its use. It is thus not implemented in all functions
   * - 6
     - ALL_MIN
     - The lowest value of all objects in the AOI. SideAoiCriteria must be handled individually in each function depending on its use. It is thus not implemented in all functions
   * - 7
     - ALL_SEQUENTIAL
     - The object indexed by the sideObjecCounter which can be set using the function MicroscopicCharacteristics::UpdateSideObjectCounter(int counter)
   * - 8
     - IRRELEVANT
     - Throws a runtime error; is used in functions which aren't expected to be called with side AOI.


The list is used all over SCM. THe SideAoiCriteria is mostly passed through functions until it is used in MicroscopicCharacteristics::DetermineSideObjectInformation(std::vector\ <ObjectInformationScmExtended> objectVector, SideAoiCriteria sideCriteria).
