..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _mesoscopic_information_:

**********************
Mesoscopic Information
**********************

A further data storage is used for all information on a mesoscopic traffic level and contains mesoscopic characteristics that can be acquired by the driver via anticipation.

The basic distinction between :ref:`microscopic characteristics<microscopic_information_>` and *mesoscopic characteristics* is shown in :numref:`image_distinctionBetweenMicroscopic_Mesoscopic_`.
(For the sake of completeness, the macroscopic level is included. It is not modeled in SCM.)

.. _image_distinctionBetweenMicroscopic_Mesoscopic_:

.. figure:: ../_static/images/distinctionBetweenMicroscopic_Mesoscopic.png
   :alt: Distinction between Microscopic, Mesoscopic and Macroscopic level

   Distinction between Microscopic, Mesoscopic and Macroscopic level

While the microscopic level deals with the behavior of individual road users (everything that can be directly assigned to individual agents or objects, e.g. the ego vehicle), the mesoscopic level covers the behavior of several road users.
This includes aggregated information on individual traffic areas such as a lane.
Important to note here is that it is a matter of dynamic contents, static aspects are part of :ref:`infrastructure_information_`.

The visual perception of mesoscopic information from the traffic environment currently involves no model of visual perception.
This means that the data concerning mesoscopic characteristics is simply based on the ground truth without being processed.

.. TODO refs to CalculationLaneMeanVelocity - chapters may not exist anymore in the future

The mesoscopic information currently used by SCM consists of three values for the *mean velocity* of individual lanes.
The lanes investigated are the one ego is currently positioned in as well as the ones directly to the left and right of that (left lane, ego lane and right lane).
The mean velocities are considered by the model when calculating lane change wishes (for more details see :ref:`CalculationLaneMeanVelocity<lane_mean_velocity_>` and :ref:`CalculationLaneChangeWishes<lane_change_wishes_>`).

