..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

..
  TODO: replace occurances of "AlgorithmScmMonolithisch" with new references to fit the new documentation structure or, when referring to the module specifically, vaguer wording to hide the code structure
  TODO: give an overview of contents and use (not hidden) toctree

.. _scm_user_guide_:

##############
SCM User Guide
##############

This guide describes the Stochastic Cognitive Model (SCM), a driver behaviour model developed by BMW, which is designed for statistical analyses regarding vehicle and traffic safety by means of stochastic traffic flow simulations.
The purpose of this guide is to provide a broad and fast overview on the model's structures and functionalities and explain the scientific background the model is based on.
This guide is therefore not considered a developer guide (there is a separate section in the documentation for that matter), but rather a handbook for those who apply the model.

SCM focuses on detailed driver behaviour modelling to provide an accurate simulation of realistic, normal (i.e. uncritical) traffic and, on top of that, the occurrence of critical traffic situations and road accidents which may result from those critical situations.

SCM facilitates a traffic simulation displaying realistic driver behaviour based on psychological processes including information acquisition, decision making and action execution.
The authentic model design involves detailed information processing, from individual gaze behaviour to motivational aspects (e.g. lane change wishes with a certain urgency).
Deviations from the general driver mode can be configured, amongst other settings, individually.

By implementing realistic driver behaviour, SCM enables users to simulate actual behaviour variance under clearly defined circumstances, e.g. while applying newly developed driver assistance or automated driving systems.
The human-like behaviour allows users on the one hand to implement components that come into operation on a specific level of the driving task and on the other hand draw conclusions from occurring errors or risks, again coming into effect at a certain point of human behaviour in traffic.

To take into account the inter- and intra-individual differences among drivers, SCM contains stochastic variables as model parameters for the most part. The algorithms themselves are characterised by cause-action mechanisms.
Those are strongly influenced by probabilities derived from the currently perceived traffic situation and the agent’s driver- and vehicle-specific parameterisation.
For a closer look at the algorithm scheme, see :ref:`secondary_modules_`.

.. 
  TODO write short explanation of user guide contents
  "For further information about the single submodules, please use the links in the text or the navigation side bar to the left."

