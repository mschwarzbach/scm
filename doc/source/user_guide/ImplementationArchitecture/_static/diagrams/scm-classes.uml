@startuml
   abstract Class "ScmLifetimeState"{
   + Trigger()
   + UpdateOutput()
   }
   Class LivingState extends ScmLifetimeState{}   
   Class SpawningState extends ScmLifetimeState{}
   Class CollidedState extends ScmLifetimeState{}

   
   Interface RestrictedModelInterface{
   + UpdateInput()
   + Trigger()
   + UpdateOutput()
   }
   
   Class AlgorithmScmMonolithisch implements RestrictedModelInterface{
   - scmModelState: ScmLifetimeState
   - scmSignalCollector : ScmSignalCollector
   - scmComponents : ScmComponentsInterface
   - ProgressScmModelState()
   + UpdateInput()
   }
   
   note right of AlgorithmScmMonolithisch::scmModelState
      std::variant ∈ 
      {std::monostate,
      SpawningState,
      LivingState,
      CollidedState}
   end note
   
   note right of AlgorithmScmMonolithisch::ProgressScmModelState
      advances state machine
   end note
   
   class ScmSignalCollector{
   - scmDependencies : ScmDependenciesInterface
   }
   
   
   Interface ScmDependenciesInterface{
   + UpdateScmSensorData(SensorDriverScmSignal)
   + UpdateParametersScmSignal(ParametersScmSignal)
   + UpdateParametersVehicleSignal(ParametersVehicleSignal)
   } 
   
   
   class ScmDependencies implements ScmDependenciesInterface{}
   
   Interface ScmComponentsInterface{
   + GetMentalModel()
   + GetSituationManager()
   + Get...()
   }
   
   class ScmComponents implements ScmComponentsInterface{
   - scmDependencies : ScmDependenciesInterface
   - mentalModel : MentalModel
   - situationManager : SituationManager
   - ...
   }
   
   AlgorithmScmMonolithisch o-- ScmLifetimeState
   AlgorithmScmMonolithisch o-- ScmSignalCollector
   AlgorithmScmMonolithisch o-- ScmComponentsInterface
   
   ScmSignalCollector o-- ScmDependenciesInterface
   ScmComponents o-- ScmDependenciesInterface
   
@enduml

