..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _tau_theory_:

**********
Tau Theory
**********

Visual perception within SCM draws on the so-called Tau theory by David N. Lee (1976).
The theory is based on geometric considerations, how humans are able to visually perceive time related values such as the TTC or time headway to an object.
Fundamental assumptions of Tau theory are summarized in the :ref:`first subsection<fundamentals_tau_theory_>` of this chapter.
Further explanations on how this theory is implemented in SCM are given in the :ref:`second subsection<tau_theory_SCM_>`.


.. _fundamentals_tau_theory_:

Fundamentals of the Tau Theory according to Lee
===============================================

Lee regards the monocular optical projection of environmental objects on the retina as projections through a pinhole (pupil) on the inside of a sphere with unit radius (retina).
This constitutes the following simplifications:

* the projection occurs through the centre of the lens
* the retina depicts a sphere and its radius is negligibly small compared to the distance from the object

For modelling purposes, the retinal projection of a real world object must be simplified and can be considered by two arbitrary points on the object's surface.
In general, this information is provided by the object's outlines, which can be further simplified and described by the object's width or height.
The geometrical relations by means of an environmental object obj of width :math:`w_{obj}` along the visual axis :math:`x_{v}` are shown in :numref:`image_TauTheoryRetinalProjection_`

.. _image_TauTheoryRetinalProjection_:

.. figure:: ../_static/images/TauTheoryRetinalProjection.png
   :alt: Retinal projection of an environmental object obj of width w_obj along the visual axis x_v

   Retinal projection of an environmental object obj of width w_obj along the visual axis x_v


:math:`\gamma_{obj}` constitutes the retinal size of the projection of :math:`obj` in terms of its real width :math:`w_{obj}`.
It can be expressed by the angular relations of the two right-angled triangles:

.. math::
   \gamma_{obj} = 2 \cdot atan(\frac{w_{obj}}{2 \cdot x_{obj,v}})

Declaring that :math:`w_{obj}` is much smaller than the relative distance :math:`x_{obj,v}` to the object :math:`obj`, this can be simplified to:

.. math::
   \gamma_{obj} = \frac{w_{obj}}{x_{obj,v}}

In case of a relative motion between eye and object :math:`obj`, the relative distance :math:`x_{obj,v}` changes by the relative velocity :math:`x_{obj,v,d}`, while the object's real size :math:`w_{obj}` remains constant.
The angular change :math:`\gamma_{obj,d}` of the retinal size :math:`\gamma_{obj}` is given by the time derivative of the equation above:

.. math::
   \gamma_{obj,d} = - w_{obj} \cdot \frac{x_{obj,v,d}}{x_{obj,v}^2}

The above equation converted to :math:`w_{obj}` and substituted in the equation before that reveals:

.. math::
   \gamma_{obj,d} = - \gamma_{obj} \cdot \frac{x_{obj,v,d}}{x_{obj,v}}

.. math::
   \tau = \frac{\gamma_{obj}}{\gamma_{obj,d}} = - \frac{x_{obj,v}}{x_{obj,v,d}} = TTC

The right side of the equation above complies with the definition of the time to collision :math:`TTC`.
The relative distance :math:`x_{obj,v}` is greater than 0 until colliding with the object :math:`obj`.
In case of an approach, :math:`x_{obj,v}` decreases and :math:`x_{obj,v,d}` is smaller than 0.
According to the equation, the TTC is greater than 0 in that case.
Contrary to that, a negative TTC indicates a motion away from object :math:`obj`.
The border case constitutes a relative velocity :math:`x_{obj,v,d}` equal 0, which induces an infinite TTC.
From a physical point of view, that definition appears to be valid, because in case of a non-existent relative motion, an infinite amount of time is going to pass until a collision with the object :math:`obj` occurs.

What the equation further reveals is the fact that the human eye can most likely perceive the TTC to an object :math:`obj` simply by detecting the kinematical changes of its retinal projection.
To distinguish between kinematical and optical terms, Lee defined the left side of the equation as the optical variable :math:`\tau` (Tau).

According to Lee, humans do not solely use the physiological perception of the TTC in the form of the optical variable :math:`\tau` for coordinating visual-motoric tasks, especially when the relative motion is not determined by a constant relative velocity :math:`x_{obj,v,d}`, but a relative acceleration :math:`x_{obj,v,dd}`.
In those cases, the simple consideration of a temporal value of :math:`\tau` would generate an incomplete picture of how the situation evolves.
:math:`\tau` expresses the time left until colliding with object :math:`obj`, but that amount of time is only correct, if the relative velocity :math:`x_{obj,v,d}` does not change over that period.
If there is any change in :math:`x_{obj,v,d}`, the actual time until a collision can be much greater or smaller than the value expressed by :math:`\tau`.

Lee postulates that the human eye uses the time derivative :math:`\tau_{d}` of the optical variable :math:`\tau` for considering a relative acceleration :math:`x_{obj,v,dd}`.
:math:`\tau_{d}` describes the gradient of the TTC and therefore clarifies the urgency of the situation's evolvement, assuming that the relative acceleration :math:`x_{obj,v,dd}` remains constant.
It can be concatenated with the values of relative motion by deriving :math:`\tau` with respect to time:

.. math::
   \tau_{d} = 1 - \frac{\gamma_{obj} \cdot \gamma_{obj,dd}}{\gamma_{obj,d}^2} = -(1 - \frac{x_{obj,v} \cdot x_{obj,v,dd}}{x_{obj,v,d}^2})

It is commonly known that a perception threshold :math:`\gamma_{obj,d,min}` of the human eye exists for the expansion rate :math:`\gamma_{obj,d}`.
This leads to certain consequences in the perceptibility of the optical variable :math:`\tau`.
At first, :math:`\gamma_{obj,d,min}` is declared as a constant and substituted for :math:`\gamma_{obj,d}`.
By converting the equation for :math:`\gamma_{obj,d}` to :math:`x_{obj,v}` and substituting that into the equation for :math:`\gamma_{obj}`, the relative distance :math:`x_{obj,v}` to the object can firstly be eliminated from the equation system:

.. math::
   \gamma_{obj,d,min} = - \frac{w_{obj} \cdot x_{obj,v,d}}{x_{obj,v}^2}

.. math::
   x_{obj,v} = \sqrt{- \frac{w_{obj} \cdot x_{obj,v,d}}{\gamma_{obj,d,min}}}

.. math::
   \gamma_{obj} = \frac{w_{obj} \cdot \sqrt{\gamma_{obj,d,min}}}{\sqrt{- w_{obj} \cdot x_{obj,v,d}}}


The last equation and substituting :math:`\gamma_{obj,d,min}` again in the equation for :math:`\tau` finally reveals an expression for the perception threshold :math:`\tau_{thr}` of the optical variable :math:`\tau`:

.. math::
   \tau_{thr} = \sqrt{\frac{w_{obj}}{- x_{obj,v,d} \cdot \gamma_{obj,d,min}}}

:math:`\tau_{thr}` is an upper bound of perception.
For all values of the optical variable :math:`\tau` that are greater than :math:`\tau_{thr}` respectively smaller than :math:`- \tau_{thr}`, the human eye is not able to perceive :math:`\tau`.
Its value can be regarded as infinite in those cases (within SCM, the maximum value of :math:`\tau` is set at 99.0 s).

For further information, please see Lee's original paper:

Lee, D. N. (1976). A theory of visual control of braking based on information about time-to-collision. *Perception, 5* (4), 437-459.


.. _tau_theory_SCM_:

Implementation of the Tau Theory in SCM
=======================================

The calculation formulas described previously are utilized for the calculation of :math:`\tau` (perceive the TTC) and :math:`\tau_{d}` (perceive the time derivate of Tau, i.e. the change gradient of the TTC).
The perceptibility of :math:`\tau` expressed by the perception threshold :math:`\tau_{thr}` is checked against the observed object's width and height, considering the given relative velocity.
If :math:`\tau` (calculated by the given relative distance and relative velocity) can be perceived in either direction, it is returned by the function.
Otherwise, the maximum possible value of 99.0 s is returned.
:math:`\tau_{d}` can only be perceived, if :math:`\tau` itself is perceivable.
The perception threshold :math:`\gamma_{obj,d,min}` for the necessary expansion rate of the retinal projection is provided by the driver parameters.
There are two different threshold values regarding foveal perception or perception in the UFOV (see :ref:`gaze_control_` for further explanations).

According to Lee, the mechanism for the perception of the TTC can also be transferred to the perception of the time headway.
This is done for the time headway (perceive the gap) and its time derivative (perceive the gap changing rate).
As Lee does not provide any approaches for the modelling of perception thresholds of the time headway, these functions always return a valid value for their respective variables.
The functions take reasonable considerations about the positioning of the observed object and the driver's own vehicle, because the time headway is always measured from the following vehicle.

Applying the Tau theory, SCM considers longitudinal as well as lateral movements for its calculations.
Tau's time derivate :math:`\tau_{d}` for lateral movements is an exception, since there is no lateral relative acceleration available for a calculation.
