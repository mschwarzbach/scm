..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _extrapolation_:

*************
Extrapolation
*************

Extrapolation is the mechanism used to predict future states and state changes of objects which cannot be observed continually.
The mechansim is applied for surrounding objects in the agent's environment as well as the agent's own vehicle.
Both applications are explained in the following paragraphs.
To what extent extrapolated information differs from directly observed input, is dealt with in the next chapter on :ref:`reliability_`.


Extrapolation of own vehicle data
---------------------------------

When the ego agent is observing the instrument cluster or head-up display (HUD), has just spawned or is currently in ideal perception mode in foveal quality, the ego's velocity can be drawn from ground-truth data.
If this is not case, the information about the ego's velocity has to be extrapolated.

The extrapolated velocity is determined by the sum of the previous velocity and the product of the acceleration, the cycle time (in seconds) and an estimate factor.
This factor represents the misjudgment of the driver, due to the estimation and is currently set to 0.9.


Extrapolation of the surrounding objects data
---------------------------------------------

The extrapolation of the surrounding object data is called in the function UpdateSurroundingVehicleData.
Conditions are that the ego agent is in ideal perception mode, has just spawned or the observed object is not in the current field of view.
The extrapolation itself is separated into its longitudinal and lateral components.

In lateral direction, the acceleration is assumed to be zero and thus the lateral velocity is constant and will remain so until the object reaches an end point for lateral movement (i.e. the lane center).

The longitudinal parameters base their calculations on the last observed acceleration.
The extrapolation of the acceleration itself is achieved with the help of the change of acceleration between the previous two consecutive time steps.
The extrapolated acceleration derives from the previous acceleration plus the acceleration change.
Since SCM does not know the other objects' intentions (the reason why the other object is accelerating or decelerating and thus which value it targets), it is not able to anticipate the final acceleration value.
To compensate, several limitations have been implemented.
Obviously, the object cannot exceed the maximum acceleration or deceleration cap.
In addition, the driver will not assume that the other person exceeds the comfort values (from smaller values).
Since neither the maximum nor the comfort values of another object are known (and cannot be known), the own ego values are used for this purpose.
Additionally, the acceleration change value will diminish over time towards zero, since the driver has to assume, that the other vehicle will eventually reach its desired acceleration.
