..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************
.. _action_secondary_driver_tasks_:

ActionSecondaryDriverTasks
##########################


This module is responsible for the output of the driver's commands. It receives all the actions from 
the module *old ref: algorithm_scm_monolithisch_*, 
which are indirectly associated with the vehicle's guidance and sends them back to the world. 
The input commands from AlgorithmScmMonolithisch are transferred by the signal AScmMonolithischToSecondaryDriverTasksSignal.


Overview of the module's functionalities
========================================

ActionSecondaryDriverTasks gathers the sencondary driving tasks and transfers them to output signals. Three graded driving tasks are defined and exampled as follows:

**Primary driving tasks:** direct control of vehicle movements, like steering, pedal operation, shifting, etc.

**Secondary driving tasks:** indirect control of vehicle movements, but supporting and complementing driving actions, like activation of indicator, horn, light, etc.

**Tertiary driving tasks:** all the other activities, which are not realted to vehicle control, like radio operation, etc.


Table list of the input paramemter 
==================================

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Condition
   * - in_IndicatorState
     - State of the indicator
     - left, center/off, right
   * - in_hornSwitch
     - Horn activation
     - true/false
   * - in_headLightSwitch
     - Light activation
     - true/false
   * - in_highBeamLightSwitch
     - High beam light activation
     - true/false
   * - in_flasherSwitch
     - Flasher activation
     - true/false

