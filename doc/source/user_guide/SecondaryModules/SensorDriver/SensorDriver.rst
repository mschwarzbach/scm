..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _sensor_driver_:

SensorDriver
############

This module collects and distributes data from the WorldInterface and AgentInterface concerning the own vehicle,
mesoscopic information and surrounding object. It creates a data struct to store the world and agent data of the own vehicle,
mesoscopic information, other objects and surrounding vehicles. Own vehicle data examples are ID, velocity, heading angle,
yaw angle, lateral/longitudinal position, ... for the ego vehicle. Mesoscopic information are for example number of lanes,
visibility distance, ... and lane specifics like lane curvature, lane width, ... Surrounding object and other object data
(vehicles or static objects) are for example brake light status, velocity (lateral & longitudinal), indicator state, ...
for each area of interest. Overview of the connections for information distribution is shown in :numref:`image_ConnectionsForInformationdistribution_`.

.. _image_ConnectionsForInformationdistribution_:

.. figure:: _static/images/ConnectionsForInformationdistribution.png
   :alt: Connections for information distribution

   Connections for information distribution


Overview of the module's functionalities
========================================

This module gathers the ground truth informations from the simulation framework
which are going to be perceived by SCM. It serves as a central connection for all
get-functions of the WorldInterface and AgentInterface which updates the external
informations of the agent at the beginning of every single time step. Internal
informations of the agent have to be transfered from other declared modules by
different signals. All these gathered informatons by Updateinput-function of SCM will
be internally sent to the submodule InformationAcquisition. :numref:`image_InformationDistributionBetweenModules_` illustrates
the distribution of collected informations from WorldInterface and AgentInterface
to other Modules and their submodules. The module contains structs which are divided
into five different parts to store and transport the world and agent data of the
own vehicle, mesoscopic informations, other objects, lanes and surrounding vehicles:

.. _image_InformationDistributionBetweenModules_:

.. figure:: _static/images/InformationDistributionBetweenModules.png
   :alt: Information distribution between modules

   Information distribution between modules


Components of the module
========================

The information collected by SensorDriver consist of generally four different data structs (:numref:`image_SensorDriver_`):

* :ref:`sensor_driver_own_vehicle_states_` includes all information about the vehicle of the driver (e.g. speed, acceleration, ...)
* :ref:`sensor_driver_mesoscopic_information_` includes information about the traffic environment concerning dynamic objects
  on the mesoscopic traffic level (e.g. mean velocity of a lane ...)
* :ref:`sensor_driver_infrastructure_information_` includes information about the static environment (e.g. traffic signs, lane markings, ...)
* :ref:`sensor_driver_surrounding_objects_` includes information about objects in all AreasOfInterest (see :ref:`gaze_control_`)

.. _image_SensorDriver_:

.. figure:: _static/images/SensorDriver.png
   :alt: Data structs of SensorDriver

   Data structs of SensorDriver


.. _sensor_driver_own_vehicle_states_:

OwnVehicleStates
----------------

The Information of the own vehicle are based on the following general **struct OwnVehicleInformation** :

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - velocity
     - Velocity of agent
     - -999
   * - acceleration
     - Acceleration of agent
     - -999
   * - lateralPosition
     - t-coordinate
     - -999
   * - heading
     - Heading relative to lane
     - -999
   * - steeringWheelAngle
     - Angle of the steering wheel
     - -999
   * - isCrossingLanes
     - Wether the agent touches multiple neighbouring lanes
     - false
   * - distanceToLaneBoundaryLeft
     - Distance between the left front point and the left boundary of the lane it is in
     - -999
   * - distanceToLaneBoundaryRight
     - Distance between the right front point and the right boundary of the lane it is in
     - -999
   * - collision
     - Wether this agent has collided with another object
     - false

In SCM this struct is extended by the following **struct OwnVehicleInformationSCM** :

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - id
     - Id of the agent
     - -1
   * - mainLaneId
     - Id of the mainLane, i.e. where the middle of the reference point is
     - -99
   * - longitudinalVelocity
     - Velocity in direction of s
     - -999
   * - lateralVelocity
     - Velocity in direction of t
     - -999
   * - longitudinalPosition
     - Longitudinal position from start of read [m]
     - -999
   * - soundPressureLevelInterior
     - Sound pressure level of the own vehicle interior noise
     - -999
   * - forcedLaneChangeStartTrigger
     - Forced lane change action from manipulator
     - none


.. _sensor_driver_mesoscopic_information_:

MesoscopicInformation
---------------------

The struct MesoscopicInformationSCM contains mesoscopic information like information about surrounding lanes.

The general structure is shown in the following table.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - laneLeftLeft
     - struct LaneInformationMesoscopicSCM - Data about the lane two lanes left of the mainLane (in driving direction)
     - -
   * - laneLeft
     - struct LaneInformationMesoscopicSCM - Data about the lane to the left of the mainLane (in driving direction)
     - -
   * - laneEgo
     - struct LaneInformationMesoscopicSCM - Data about the lane where the middle of the front of the agent is (e.g. mainLane)
     - -
   * - laneRight
     - struct LaneInformationMesoscopicSCM - Data about the lane to the right of the mainLane (in driving direction)
     - -
   * - laneRightRight
     - struct LaneInformationMesoscopicSCM - Data about the lane two lanes right of the mainLane (in driving direction)
     - -

The basis for the respective variables is the following struct LaneInformationMesoscopicSCM:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - meanVelocity
     - Mean velocity of all other agents on this lane inside visibility distance
     - -999


.. _sensor_driver_infrastructure_information_:

InfrastructureInformation
-------------------------

The InfrastructureInformation consist of :ref:`sensor_driver_traffic_rule_information_` and :ref:`sensor_driver_geometry_information_`.


.. _sensor_driver_traffic_rule_information_:

TrafficRuleInformation
~~~~~~~~~~~~~~~~~~~~~~

The struct TrafficRuleInformationSCM contains infrastructure information of surrounding lanes that are used to establish traffic rules.

The general structure is shown in the following table.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - laneLeftLeft
     - struct LaneInformationTrafficRulesSCM - Data about the lane two lanes left of the mainLane (in driving direction)
     - -
   * - laneLeft
     - struct LaneInformationTrafficRulesSCM - Data about the lane to the left of the mainLane (in driving direction)
     - -
   * - laneEgo
     - struct LaneInformationTrafficRulesSCM - Data about the lane where the middle of the front of the agent is (e.g. mainLane)
     - -
   * - laneRight
     - struct LaneInformationTrafficRulesSCM - Data about the lane to the right of the mainLane (in driving direction)
     - -
   * - laneRightRight
     - struct LaneInformationTrafficRulesSCM - Data about the lane two lanes right of the mainLane (in driving direction)
     - -

The basis for the respective variables is the following general struct LaneInformationTrafficRules:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - trafficSigns
     - Vector of all traffic signs valid for this lane inside the visibility distance
     - -

In SCM this struct is extended by the following struct LaneInformationTrafficRulesSCM:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - laneMarkingsLeft
     - List of lane markings on the left side of lane
     - -
   * - laneMarkingsRight
     - List of lane markings on the right side of lane
     - -


.. _sensor_driver_geometry_information_:

GeometryInformation
~~~~~~~~~~~~~~~~~~~

The struct GeometryInformationSCM contains infrastructure information of surrounding lanes that describe its geometric features.

The general structure is shown in the following table.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - visibilityDistance
     - Current maximum visibility distance as specified by the world
     - -999
   * - NumberOfLanes
     - Number of lane at the current position
     - -1
   * - distanceToEndOfNextExit
     - Distance from current position to the end of the next (current or coming) exit lane
     - -999
   * - distanceToEndOfNextEntry
     - Distance from current position to the end of the next (current or coming) entry lane
     - -999
   * - laneLeftLeft
     - struct LaneInformationGeometrySCM - Data about the lane two lanes left of the mainLane (in driving direction)
     - -
   * - laneLeft
     - struct LaneInformationGeometrySCM - Data about the lane to the left of the mainLane (in driving direction)
     - -
   * - laneEgo
     - struct LaneInformationGeometrySCM - Data about the lane where the middle of the front of the agent is (e.g. mainLane)
     - -
   * - laneRight
     - struct LaneInformationGeometrySCM - Data about the lane to the right of the mainLane (in driving direction)
     - -
   * - laneRightRight
     - struct LaneInformationGeometrySCM - Data about the lane two lanes right of the mainLane (in driving direction)
     - -

The basis for the last three variables is the following general struct LaneInformationGeometry:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - exist
     - Wether there is a lane on this position
     - false (not existing)
   * - curvature
     - Curvature at current s position
     - -999 (not existing)
   * - width
     - Width at current s position
     - -999 (not existing)
   * - distanceToEndOfLane
     - Distance from current position to the end of the lane
     - -999 (end is farther away than the visibility distance)

In SCM this struct is extended by the following struct LaneInformationGeometrySCM:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - curvatureInPreviewDistance
     - Curvature at previewDistance
     - -999 (not existing)
   * - curvaturesToNearPoint
     - Curvatures at six positions equidistant between rear axle and 2m in front of the vehicles leading edge
     - -999 (not existing)
   * - curvaturesToFarPoint
     - Curvatures at two positions equidistant between NearPoint (2m) and 8m in front of the vehicles leading edge
     - -999 (not existing)


.. _sensor_driver_surrounding_objects_:

SurroundingObjects
------------------

The struct SurroundingObjectsSCM contains data about all the objects in the different AreasOfInterest.

The general structure is shown in the following table.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - objectFront
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest EGO_FRONT
     - -
   * - objectFrontFar
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest EGO_FRONT_FAR
     - -
   * - objectRear
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest EGO_REAR
     - -
   * - objectFrontLeft
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest LEFT_FRONT
     - -
   * - objectFrontFarLeft
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest LEFT_FRONT_FAR
     - -
   * - objectRearLeft
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest LEFT_REAR
     - -
   * - objectSideLeft
     - vector<struct ObjectInformationSCM> - Vector containing data about all objects in the AreasOfInterest LEFT_SIDE
     - -
   * - objectFrontRight
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest RIGHT_FRONT
     - -
   * - objectFrontFarRight
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest RIGHT_FRONT_FAR
     - -
   * - objectRearRight
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest RIGHT_REAR
     - -
   * - objectSideRight
     - vector<struct ObjectInformationSCM> - Vector containing data about all objects in the AreasOfInterest RIGHT_SIDE
     - -
   * - objectFrontLeftLeft
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest LEFTLEFT_FRONT
     - -
   * - objectRearLeftLeft
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest LEFTLEFT_REAR
     - -
   * - objectSideLeftLeft
     - vector<struct ObjectInformationSCM> - Vector containing data about all objects in the AreasOfInterest LEFTLEFT_SIDE
     - -
   * - objectFrontRightRight
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest RIGHTRIGHT_FRONT
     - -
   * - objectRearRightRight
     - struct ObjectInformationSCM - Data about the object in the AreasOfInterest RIGHTRIGHT_REAR
     - -
   * - objectSideRightRight
     - vector<struct ObjectInformationSCM> - Vector containing data about all objects in the AreasOfInterest RIGHTRIGHT_SIDE
     - -

The basis for the respective variables is the following general struct ObjectInformation, which is used to transport data of an object as seen by the driver:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - id
     - Id of the object
     - -1 (not existing)
   * - exist
     - False if there is no object in this position
     - false
   * - isStatic
     - True if stationary object, false if agent
     - false
   * - velocity
     - Absolute velocity of the agent
     - -999 (object is not an agent)
   * - acceleration
     - Acceleration of the agent
     - -999 (object is not an agent)
   * - heading
     - Heading relative to the street
     - -999 (not existing)
   * - length
     - Length of object
     - -999 (not existing)
   * - width
     - Width of object
     - -999 (not existing)
   * - height
     - Heigth of object
     - -999 (not existing)
   * - relativeLongitudinalDistance
     - Relative distance along the road (i.e. in direction s) between own agent and object
     - -999 (not existing)
   * - relativeLateralDistance
     - Relative distance at right angle to the road (i.e in direction t) between own agent and object
     - -999 (not existing)

In SCM this struct is extended by the following struct ObjectInformationSCM:

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Default value
   * - distanceToLaneBoundaryLeft
     - Distance between the left front point and the left boundary of the lane it is in
     - -999
   * - distanceToLaneBoundaryRight
     - Distance between the right front point and the right boundary of the lane it is in
     - -999
   * - collision
     - Wether this object has collided with another object
     - false
   * - isCrossingLanes
     - Wether the agent touches multiple neighbouring lanes
     - false
   * - brakeLightsActive
     - Wether the brake lights of the agent are on (false if not an agent)
     - false
   * - indicatorState
     - State of the indicator of the agent
     - off (not an agent)
   * - mainLaneId
     - Id of the middle of the reference point of the agent
     - -999
   * - obstructionScm
     - Obstruction information between object and own agent (i.e. information about relative lateral distances (left and right))
     - -
   * - lateralPositionInLane
     - t-coordinate
     - -999
   * - longitudinalVelocity
     - Velocity in direction of s
     - -999
   * - lateralVelocity
     - Velocity in direction of t
     - -999


.. _sensor_driver_obstruction_scm_:

ObstructionScm
~~~~~~~~~~~~~~

ObstructionScm expands on the already existing Obstruction information from the agent interface
and adjusts the data in accordance with conventions used in SCM.

While Obstruction represents the lateral distance for the front center (i.e. center of the front of the bounding box) to
pass the observed object on its right or left hand side ObstructionScm will also factor in the
dimensions of the ego agent. The right and left component will contain the lateral displacement
required for the ego agent to completely pass the observed object on its respective side. In
addition, ObstructionScm will also comprise the lateral displacement of the ego vehicle due to a
heading angle. As displayed in the graphic below the frontal displacement component is calculated
by the product of the vehicle half width and the cosine of the heading angle. For the value of the
rear displacement component the frontal value (or distance due to vehicle width) is added to the
product of the vehicle length and the sine of the heading angle. In combination with the
Obstruction, it results in the lateral displacements required to pass the observed object on
either its left or right side.

.. figure:: _static/images/ObstructionScm.png
   :alt: Ego geoometry and heading influence for ObstructionScm

   Ego geoometry and heading influence for ObstructionScm

The second major difference between Obstruction and ObstructionScm is in its sign convention.
ObstructionScm will net a positive value on either side when there is a distance to overcome and
a negative value when the ego agent is already on the desired side.

.. figure:: _static/images/ObstructionScmSignConvention.png
   :alt: ObstructionScm sign convention

   ObstructionScm sign convention


Longitudinal obstruction
~~~~~~~~~~~~~~~~~~~~~~~~

Similar to the lateral obstruction above SensorDriverScm also determines a lateral obstruction. It
contains the values

* front: Longitudinal distance for the ego agent to be ahead of the observed object
* rear: Longitudinal distance for the ego agent to be behind of the observed object
* isOverlapping: Flag, indicating an overlapping of the two objects in longitudinal direction

The sign convention is in line with the lateral counterpart. Positive values indicate that
a distance needs to be overcome, as depicted in the graphic blow.

.. figure:: _static/images/LongitudinalObstructionSignConvention.png
   :alt: Longitudinal obstruction sign convention

   Longitudinal obstruction sign convention

To determine these values the position of the front and rear of the ego agent and the observed
object is calculated. This can be achieved by combination of the distance to start of road and
length of the respective objects. This information are enough to determine whether the observed
object is entirely in front, entirely behind or next to (entirely or partially) the ego agent
and allows for the LongitudinalObstruction values be set accordingly. The longitudinal obstruction
does not factor in the heading of the ego agent in its current form.


Lateral position in lane
~~~~~~~~~~~~~~~~~~~~~~~~

In order to determine the lateral position of an object in its lane the distance between the ego
agent and the lane center the observed object resides in must be determined. This is done with the
help of the relative lateral distance between the two objects, the lane widths of all lanes between
them and the lateral position in lane of the ego agent. The object's lateral position results from
the difference between the relative lateral distance and the distance to its lane center.


Distance to lane boundary
~~~~~~~~~~~~~~~~~~~~~~~~~

First the distance from the ego agent to the lane boundaries of the observed object's lane boundaries
is determined. This can be done with the help of ego's lateral position in lane, the all lane width
between and including ego's lane and the object's lane. With respective difference between the resulting
distance to object's left and right boundary and the Obstruction values left and right the distances
between the object and it's left and right lane boundary can be calculated. A positive value indicates
that the object has not crossed the respective lane marking, while a negative value is the distance
that it has already exceeded.

.. figure:: _static/images/DistanceToLaneBoundary.png
   :alt: Distance to lane boundary components

   Distance to lane boundary components

