..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************
.. _dynamics_regular_driving_:

DynamicsRegularDriving
######################


This module is responsible for the vehicle dynamics under normal driving conditions.
It receives a signal from the lateral dynamics module :ref:`algorithm_lateral_driver_` containing the steering wheel angle.
Accelerator pedal position, brake pedal position and the currently engaged gear are fetched from the signal sent by the longitudinal dynamics module  :ref:`algorithm_longitudinal_driver_`.
The module is using these inputs and the vehicle parameters from :ref:`parameters_agent_` to determine the resulting vehicle speed, yaw angle and absolute position in the road coordinate system. 
The calculated values are passed on to the simulation framework/WorldObjectInterface.

Overview of the module's functionalities
========================================


Longitudinal dynamics
---------------------
The core element of all longitudinal calculations in this model are speed and acceleration. To determine the change of speed :math:`\Delta v` the actual acceleration :math:`a_{act}` has to be known The vehicles velocity :math:`v_{act_{i}}` of the current time step :math:`i`.

The module uses a simplified longitudinal dynamics model e.g. it doesn't factor in rotational inertias.

:math:`a_{act}` is dependend on the state variables and parameters shown in table 1.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Unit
     - Source
   * - :math:`v_{act_{i-1}}`
     - currently saved velocity of agent
     - m/s
     - WorldObjectInterface
   * - :math:`pos_{actuator}`
     - position of accelerator/brake pedal
     - /
     - AlgorithmLongitudinalDriver
   * - :math:`gear`
     - chosen gear
     - /
     - AlgorithmLongitudinalDriver
   * - :math:`i_{g} (gear)`
     - gear ratio of current gear
     - /
     - vehicleModelParameters
   * - :math:`i_{a}`
     - gear ratio of axle
     - /
     - vehicleModelParameters
   * - :math:`M_{eng,max}`
     - maximum torque of the vehicle engine
     - Nm
     - vehicleModelParameters
   * - :math:`n_{eng,max}`
     - maximum engine speed
     - 1/min
     - vehicleModelParameters
   * - :math:`n_{eng,min}`
     - minimum engine speed
     - 1/min
     - vehicleModelParameters
   * - :math:`r_{stat}`
     - static wheel radius
     - Nm
     - vehicleModelParameters
   * - :math:`m_{vehicle}`
     - vehicle mass
     - kg
     - vehicleModelParameters
   * - :math:`c_{W}`
     - air drag coefficient
     - /
     - vehicleModelParameters
   * - :math:`c_{R}`
     - rolling resistance vehicle
     - /
     - vehicleModelParameters
   * - :math:`A_{proj}`
     - front axial surface
     - :math:`m^{2}`
     - vehicleModelParameters


First variables are used to calculate the current engine speed :math:`n_{eng}` . 
To do so the speed of the wheels :math:`n_{wheel}` is determined via the vehicle velocity and static wheel radius:

.. math::

   n_{wheel} = \frac{v_{act_{i-1}}}{r_{stat}} \cdot \frac{60}{2 \cdot \pi}

The powertrain model in :numref:`image_PowertrainModel2_` is used to further derive the engine speed :math:`n_{eng}` using the gear ratio :math:`i_{g} (gear)` of the currently engaged gear and axle ratio :math:`i_{a}` .

.. _image_PowertrainModel2_:

.. figure:: _static/images/PowertrainModel.png
   :alt: Illustration of the powertrain model and its parameters and state variables
   
   Illustration of the powertrain model and its parameters and state variables


.. math::
   n_{eng} = n_{wheel} \cdot i_{a} \cdot i_{g} (gear)
   

Using the engine torque map shown in :numref:`image_M_max_berechnet_` the maximum torque :math:`M_{eng,max} (n_{eng})` at the current engine speed is determined.

.. _image_M_max_berechnet_:

.. figure:: _static/images/M_max_berechnet.png
   :alt: Simplified engine map
   
   Simplified engine map



At this point the module differntiates between deceleration and acceleration by checking wether the value of :math:`pos_{actuator}` is positive or negative.
Negative values stand for the use of the brake pedal between 0-100% while positive values represent the use of the accelerator pedal. 

**Driving resistance:** First of all there are driving resistances to be considered during the drive due to the rolling resistance of tires and air resistance which
could be calculated according to the follwing relations in fluid mechanics, where :math:`\rho_{air}` stands for air density in :math:`km / m^3` :

.. math::
   a_{AirDrag} = - \frac{\rho_{air} \cdot c_{w} \cdot A_{proj} \cdot v_{act_{i-a}}^2}{2 \cdot m_{vehicle}}

.. math::
   a_{RollingDrag} = c_{R} \cdot 9.81 \frac{m}{s^2}


The rolling resistance of vehicle is currently set as a dummy value (0.0125), because the related value is not in the catalog of vehicle models yet and 
therefore it still can not be tapped in interface.

**Decelerating:** When decelerating :math:`a_{act}` is the sum of the acceleration resulting from engine drag :math:`a_{drag}` and acceleration from braking :math:`a_{actuator}` .
Whenever the driver is braking the module assumes that the engine is dragging with a moment equal to 10% of the maximum torque at the current engine speed.

.. math::
   M_{drag}(n_{eng}) = - 0.1 \cdot M_{eng,max}(n_{eng})

.. math::
   M_{wheel} = \frac{M_{drag}(n_{eng})}{i_{a} \cdot i_{g}(gear)}

.. math::
   F_{wheel} = \frac{M_{wheel}}{r_{stat}}
   
.. math::
   a_{drag} = \frac{F_{wheel}}{m_{vehicle}}



The maximum braking deceleration :math:`a_{brake,max}` is set to 9.81m/s². The actual braking power is proportional to the position of the braking pedal.

.. math::
   a_{brake} = pos_{actuator} \cdot a_{brake,max}

.. math::
   a_{act} = a_{drag} + a_{brake}


**Acceleration:**  When the driver applies pressure on the accelerator the applied engine torque :math:`M_{eng,act}` is calculated as proportional to the accelerator pedal position and the span between engine drag and maximum engine torque :math:`M_{eng}`\ (:math:`n_{eng}` ) at the current engine speed.

.. math::
   M_{eng}(n_{eng}) = M_{drag}(n_eng) + pos_{actuator}(M_{eng,max}(n_{eng}) - M_{drag}(n_{eng}))
   
.. math::
   M_{wheel} = \frac{M_{eng,act}}{i_{a} \cdot i_{g}(gear)}

.. math::
   F_{wheel} = \frac{M_{eng,act}}{r_{stat}}
   
.. math::
   a_{act} = \frac{F_{wheel}}{m_{vehicle}}
   

Once the acceleration of the vehicle is known the change in velocity is determined by its discrete time integral.

.. math::
   \Delta v = a_{act} \cdot \frac{\Delta t_{cycle}}{1000}



The sum of the previous velocity and change in velecity result in the actual vehicle velocity of the current time step:

.. math::
   v_{act_{i}} = v_{act_{i-1}} + \Delta v



Lateral dynamics
----------------

The lateral dynamics model is based on the Ackerman model which reduces the steering kinematics to a single surrogate front wheel. Considering the relatively small steering angles when driving on the highway this simplification is deemed suitable. 
The heading is updated using the previous heading and the change in yaw due to the steering wheel angle which is converted into a yaw rate using the simplified Ackermann model shown in :numref:`image_AckermannModel_`.

.. _image_AckermannModel_:

.. figure:: _static/images/AckermannModel.png
   :alt: Illustration of the Ackermann model
   
   Illustration of the Ackermann model


.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Unit
     - Source
   * - :math:`\delta_{h}`
     - steering wheel angle
     - °
     - AlgorithmLateralDriver
   * - :math:`\Phi_{act_{i-1}}`
     - heading at previous time step
     - °
     - WorldObjectInterface
   * - :math:`i_{s}`
     - steering ratio
     - /
     - vehicleModelParameters
   * - :math:`l_{wheelbase}`
     - wheelbase
     - m
     - vehicleModelParameters
   * - :math:`\Delta s`
     - distance travelled since last time step
     - m
     - previously calculated in the module
   * - :math:`\Delta t_{cycle}`
     - cycle time
     - ms
     - ModellInterface


The steering wheel angle :math:`\delta_{h}` is proportional to the front wheel angle :math:`\delta` :

.. math::
   \delta = \frac{\delta_{h}}{i_{s}}


The Ackermann model provides the geometric connection between the front wheel angle and the resulting curvature :math:`\kappa` :

.. math::
   \kappa = \frac{tan(\delta)}{l_{wheelbase}}


The combination of curvatur :math:`\kappa` and driven distance at that curvature :math:`\Delta s` result in the change of the heading :math:`\Delta \Phi`:

.. math::
   \Delta \Phi = atan(\kappa \cdot \Delta s)
   


The new heading :math:`\Phi_{act_{i}}` is the old heading :math:`\Phi_{act{i-1}}` plus the previously calculated change in heading :math:`\Delta \Phi` :

.. math::
   \Phi_{act_{i}} = \Phi_{act_{i-1}} + \Delta \Phi


Updating position
-----------------

Once the new vehicle speed :math:`v_{act_{i}}` is determined the distance travelled during the last timestep :math:`\Delta s` is determined:

.. math::
   \Delta s = v_{act_{i}} \cdot \Delta t_{cycle}
   

Using the actual vehicle yaw angle in the road coordinate :math:`\Phi_{act_{i-1}}` the vehicles new x- and y-coordinates are calculated:

.. math::
   x_{agent_{i}} = x_{agent_{i-1}} + \Delta s \cdot cos(\Phi_{act_{i-1}})
   
.. math::
   y_{agent_{i}} = y_{agent_{i-1}} + \Delta s \cdot sin(\Phi_{act_{i-1}})

