..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _secondary_modules_:

*****************
Secondary Modules
*****************

In terms of openPASS programme paradigms, *old ref: algorithm_scm_monolithisch_*
(the agent module containing SCM) is a pure Algorithm-module. 
As such, it has no direct connections to the world outside its current agent itself.
Therefore, AlgorithmScmMonolithisch is connected to other modules which provide
input data from the world and the simulation framework or further process
AlgorithmScmMonolithisch's output data to fulfil its actions in the world.
These surrounding modules inside the agent are also part of this guide,
as well as a genereal chapter concerning the coordinate system definitions and
frequently used calculations (see :ref:`scm_reference_guide_`).
The connections between AlgorithmScmMonolithisch and all its related agent
modules is illustrated by :numref:`image_ScmOuterConnections_`. 
The additional agent modules are shortly described subsequently. 
For further information about *old ref: algorithm_scm_monolithisch_*
or one of the other related agent modules, please use the links in the
text or the navigation side bar to the left.

.. _image_ScmOuterConnections_:

.. figure:: _static/images/ScmOuterConnections.png
   :alt: Connections of AlgorithmScmMonolithisch to other modules within an agent
   
   Connections of AlgorithmScmMonolithisch to other modules within an agent


* :ref:`parameters_agent_` gathers all agent related parameters from the simulation framework and provides them to all other modules of the agent which require those parameters (that also includes DynamicsRegularDriving, but the arrow is neglected in :numref:`image_ScmOuterConnections_` for the sake of clearness). 
  The parameters are structured by driver behaviour parameters which are transferred by the signal ParametersAgentHumanSignal, and vehicle model parameters which are transferred by the signal ParametersAgentMachineSignal.
  The module also provides the driver's current preview distance to the input module SensorDriver.
  Therefore, it is called before SensorDriver within one time step.

* :ref:`sensor_driver_` gathers all necessary state variables from the world and provides them to all driver behaviour related Algorithm-modules.
  The state variables are structured by data about the driver's own vehicle, data about the mesoscopic traffic attributes and static traffic environment, and data about surrounding objects and agents.
  These three structures are all transferred by the signal SensorDriverHumanSignal.

* :ref:`algorithm_lateral_driver_` receives the command variables regarding the vehicle's lateral guidance from AlgorithmScmMonolithisch and generates the necessary steering wheel angle of the driver to match these command variables. 
  The command variables from AlgorithmScmMonolithisch are transferred by the signal LateralSignal and the steering wheel angle is transferred by the signal SteeringSignal to DynamicsRegularDriving.

* :ref:`algorithm_longitudinal_driver_` receives the command variables regarding the vehicle's longitudinal guidance from AlgorithmScmMonolithisch and generates the necessary pedal positions and gear to match these command variables.
  The command variables from AlgorithmScmMonolithisch are transferred by the signal AccelerationSignal and the pedal positions and gear are transferred by the signal LongitudinalSignal to DynamicsRegularDriving.

* :ref:`action_secondary_driver_tasks_` receives all the actions from AlgorithmScmMonolithisch which are not directly associated with the vehicle's guidance (e.g. actuation of indicators or flasher) and sends them back to the world. 
  The input commands from AlgorithmScmMonolithisch are transferred by the signal ScmMonolithischToSecondaryDriverTasksSignal.

* :ref:`dynamics_regular_driving_` contains the vehicle's dynamic model for the regular movements in the world (that means crash dynamics are not handled inside this module, for instance). 
  It receives the driver's inputs regarding lateral guidance (steering wheel angle) from AlgorithmLateralDriver and the driver's inputs regarding longitudinal guidance (pedal positions and gear) from AlgorithmLongitudinalDriver, calculates the vehicle's movements according to those driver inputs, and sends the changed state variables of the vehicle back to the world.


.. toctree::
    :hidden:
    :glob:
    :maxdepth: 1

    */*
