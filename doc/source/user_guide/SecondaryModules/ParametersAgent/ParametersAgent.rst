..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************
.. _parameters_agent_:

ParametersAgent
###############

This module is responsible for gathering all agent related parameters from the simulation framework 
and providing them to all other modules of the agent, like *old ref: algorithm_scm_monolithisch_*. The module also provides the driver's current 
preview distance to the input module :ref:`sensor_driver_`.
Functionally und structurally, this module consists of two separate parts for the current agent's parameterization. 
The first section specializes in the parameterization of the vehicle model, while the second section focuses on the 
parameterization for the ParametersScm module, which operates the total driving behaviours of the current agent.


Vehicle Parameterization
========================

This section includes all vehicle system related parameters, power train system related parameters and steering system related parameters.
Relevant parameters are presented in the following three tables with the coresponding explanations.


Table list of the vehicle system related parameters for vehicle parameterization
--------------------------------------------------------------------------------

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Unit
   * - width
     - The maximum width of the vehicle
     - m
   * - length
     - The maximum length of the vehicle
     - m
   * - height
     - The maximum height of the vehicle
     - m
   * - wheelbase
     - The wheelbase of the vehicle
     - m
   * - trackwidth
     - The trackwidth of the vehicle
     - m
   * - distanceReferencePointToLeadingEdge
     - The distance between the vehicle coordinate system's reference point (rear axle) and the front bumper
     - m
   * - distanceReferencePointToFrontAxle
     - The distance between the vehicle coordinate system's reference point (rear axle) and the front axle
     - m
   * - maxVelocity
     - The maximum velocity of the vehicle
     - m/s
   * - weight
     - The overall mass of the vehicle
     - kg
   * - heightCOG
     - The height of the center of gravity above ground
     - m
   * - momentInertiaRoll
     - The moment of inertia along the vehicle's longtudinal axes
     - kgm2
   * - momentInertiaYaw
     - The moment of inertia along the vehicle's vertical axes
     - kgm2
   * - frontSurface
     - The projected front surface of the vehicle
     - m2
   * - airDragCoefficient
     - The air drag coefficient of the vehicle
     - /


Table list of the power train system related parameters for vehicle parameterization
------------------------------------------------------------------------------------

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Unit
   * - minimumEngineSpeed
     - The idle speed of the engine
     - 1/min
   * - maximumEngineSpeed
     - The maximum engine speed
     - 1/min
   * - minimumEngineTorque
     - The maximum height of the vehicle
     - Nm
   * - maximumEngineTorque
     - maximum torque of the engine
     - Nm
   * - numberOfGears
     - The number of gears in the gearbox
     - /
   * - axleRatio
     - The ratio of the axle gear
     - /
   * - decelerationFromPowertrainDrag
     - The deceleration caused by the overall powertrain drag torque
     - m/s2



Table list of the steering system related parameters for vehicle parameterization
---------------------------------------------------------------------------------

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     - Unit
   * - steeringRatio
     - he ratio of the steering gear
     - /
   * - maximumSteeringWheelAngleAmplitude
     - maximum amplitude of the steering wheel angle
     - degree
   * - maxCurvature
     - The maximum curavture the vehicle is able to drive
     - 1/m


Driving Behaviour Parameterization
==================================

The parameters relating to the behaviour of SCM drivers are described in :ref:`driver_parameters_`.


Regression model for driver behaviour parameterization
------------------------------------------------------

When required, this module can use a regression model (DriverCharacteristicsSampler) for the targeted determination 
of some behavioral parameters from driver characteristics. The regression model and its graphical representation (see Figure 1)
can be sourced from Manuela Witt's PhD thesis
(Regressionsmodelle zur Berücksichtigung von Fahrerindividualität in der kognitiven Fahrerverhaltensmodellierung, 2021, Technische Universität Dresden).
Following behavior parameters are concerned in the model:


* desiredVelocity
* amountOfSpeedLimitViolation
* agentCooperationFactor
* comfortLongitudinalAcceleration
* comfortLongitudinalDeceleration
* rightKeepingFactor


.. figure:: _static/images/RegressionModelWitt.png
   :target: RegressionModelWitt.png
   :alt: Graphical representation of the regression model
   
   Graphical representation of the regression model

