..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************
.. _algorithm_longitudinal_driver_:

AlgorithmLongitudinalDriver
###########################


This module is responsible for the control of the vehicle's longitudinal
dynamics. It receives the command variables from a driver behaviour model like
*old ref: algorithm_scm_monolithisch_* and generates the
accelerator and brake pedal position and the current gear of the driver to match
these command variables. The pedal positions and the gear have than to be send
to a vehicle dynamics module like :ref:`dynamics_regular_driving_`.
The actuation of the clutch pedal is currently not simulated by the module.


.. _algorithm_longitudinal_driver_overview_:

Overview of the module's functionalities
========================================

AlgorithmLongitudinalDriver models human longitudinal guidance according to the
two-level-model of vehicle guidance from Donges. It is the original form of his
later three-level-model of vehicle guidance, which is expressing driver
behaviour in a more phenomenological context, and was developed as part of
Donges' Ph.D. thesis (Donges only focussed on lateral guidance in this work,
which is the base for the algorithms in :ref:`algorithm_lateral_driver_`;
the phenomenology behind this model is transferred to longitudinal guidance
in AlgorithmLongitudinalDriver). The model takes two overlapping mechanisms of
human behaviour in human-machine-interactions into account:

**Anticipatory open-loop control on guidance level:** Every driver has knowledge
about how his vehicle works, which is based on driving experience and
familiarity with the vehicle. This can be expressed as a mental model
representation about the vehicle. If the driver formulates a desired
acceleration, he can use his knowledge about the vehicle to estimate, how much
he has to press the accelerator pedal, so the vehicle's powertrain generates
the desired acceleration. This is a typically open-loop control process, where
the mental representation of the vehicle represents a feedforward control, which
generates a pedal position according to a desired acceleration, but the actual
acceleration, which is produced by the vehicle, is not monitored. The better the
mental model of the vehicle is, the better is this feedforward control, but even
the best mental model cannot take into account external disturbances like slope.
For influences like this, a second underlying control mechanism is needed.

**Compensatory closed-loop control on stabilization level:** As already
mentioned above, the guidance level cannot take into account any external
disturbance, because an open-loop control does not monitor the difference
between command variable and its corresponding system state. For this reason
(and to compensate the driver's necessarily imperfect mental model about
his vehicle), an underlying closed-loop control is needed. For instance,
a driver is driving at his target speed on a plane road, which means that the
required acceleration to reach his target speed is equal zero. In this case,
the guidance level will also produce an accelerator pedal position equal to zero
engine torque (compensation of powertrain drag can be part of the anticipatory
open-loop control, if the driver's mental model is sophisticated enough).
If an external disturbance like a slope occurs, the state of the anticipatory
level will not change, because this kind of driving resistance cannot be
addressed by a feedforward control. The vehicle will of cause start to
decelerate and therefore deviate from the target speed of the driver, which must
be compensated by a closed-loop control that monitors the actual velocity of
the vehicle.

Contrary to lateral guidance (:ref:`algorithm_lateral_driver_`),
longitudinal guidance of a passenger car requires the actuation of two pedals,
namely accelerator and brake pedal. As these two are furthermore controlled with
the same foot, there is a necessary pedal change time when switching between
pedals, which is modelled as a stochastic delay in the module's signal chain.

AlgorithmLongitudinalDriver is currently modelled for the control of vehicles
with manual transmissions, which incorporates an algorithm for the selection of
the optimal gear. The actuation of a clutch pedal is not simulated by the module.


Detailed description of the module's features
=============================================

AlgorithmLongitudinalDriver receives its command variables from a driver
behaviour model like *old ref: algorithm_scm_monolithisch_*
and generates the pedal positions and the gear of the driver to match these
command variables. The pedal positions of all controllers are summed up to
the overall pedal position of the driver, which is complemented by a maybe
necessary pedal change time and is than send to a vehicle dynamics module like
:ref:`dynamics_regular_driving_`. The
pedal positions within the module's algorithms are defined as double values
between -1 and 1, where values between 0 and 1 indicate accelerator actuation
and values below 0 indicate brake pedal actuation. This is split up into two
seperate output signals for accelerator and brake pedal by the function
UpdateOutputParameters, but it makes handling inside the algorithms much easier.
The gear is calculated within the inverted vehicle model of the open-loop
controller. The control loop is closed by the movement of the vehicle. This
overall control loop of vehicle longitudinal guidance involving
AlgorithmLongitudinalDriver is illustrated in :numref:`image_LongitudinalControllerSignalFlow_` with the following variables:

* :math:`a_{set}` is the desired acceleration of the vehicle
* :math:`v_{set}` is the desired velocity of the vehicle, which is :math:`a_{set}` integrated over time
* :math:`v_{act}` is the actual velocity of the vehicle
* :math:`\Delta v` is the velocity error
* :math:`act_{olc}` is the pedal position signal from the acceleration controller
* :math:`act_{clc}` is the pedal position signal from the velocity controller
* :math:`act` is the overall pedal position of the controllers
* :math:`p_{a}` is the output signal for the accelerator pedal position
* :math:`p_{b}` is the output signal for the brake pedal position
* :math:`gear` is the current gear of the driver

.. _image_LongitudinalControllerSignalFlow_:

.. figure:: _static/images/LongitudinalControllerSignalFlow.png
   :alt: Components and signal flow of the longitudinal guidance control loop
   
   Components and signal flow of the longitudinal guidance control loop

The following subsections describe the theoretical background and the transfer
functions of the different controllers.

.. _algorithm_longitudinal_driver_open_loop_:

Anticipatory control on guidance level
--------------------------------------

As described in the :ref:`overview section<algorithm_longitudinal_driver_overview_>`,
the driver's controls on guidance level are characterized by anticipatory
mechanisms regarding the expected vehicle's behaviour. In terms of control
theory and longitudinal guidance, this is modelled as an open-loop feedforward
controller, which generates an estimated pedal position :math:`act_{olc}` that
fits a required acceleration :math:`a_{set}` from the driver behaviour model.
This model-predictive controller is modelled as an inverted longitudinal
dynamics model of the vehicle's powertrain and brake system. In terms of control
theory, this is a complex proportional controller, which is further complemented
by a first order transient behaviour. Therefore, the whole anticipatory
open-loop controller can be interpreted as a :math:`PT_{1}` unit. The inverted
longitudinal dynamics model also includes an algorithm for the selection of the
optimal gear, regarding the required torque on the wheels to generate :math:`a_{set}`. The several aspects of this model are described below.

.. _algorithm_longitudinal_driver_inverted_dynamics_model_:

Inverted longitudinal dynamics model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The inverted longitudinal dynamics model produces the proportional part :math:`act_{olc,stat}`
of the open-loop acceleration controller by utilizing an inverted powertrain and
brake system model (see :ref:`here<algorithm_longitudinal_driver_transient_behaviour_>` for the application of the transient behaviour to calculate :math:`act_{olc}`
from :math:`act_{olc,stat}`). The powertrain model and all of its parameters and
state variables are illustrated in :numref:`image_PowertrainModel1_`. The brake model is not worth
illustrating, as it directly applies on the required acceleration :math:`a_{set}`
of the vehicle.

.. _image_PowertrainModel1_:

.. figure:: _static/images/PowertrainModel.png
   :alt: Illustration of the powertrain model and its parameters and state variables
   
   Illustration of the powertrain model and its parameters and state variables


The core element of all calculations in this model is speed and acceleration.
The speed :math:`n_{wheel}` at the wheels is constraint by the current velocity
:math:`v_{act}` of the vehicle and the static wheel radius :math:`r_{stat}`:

.. math::
   n_{wheel} = \frac{v_{act}}{r_{stat}} \cdot \frac{60}{2 \cdot \pi}
   

where :math:`n_{wheel}` is defined in rotations/min and :math:`v_{act}` is
defined in m/s, which requires the conversion term at the end of the equation.
As the equation also illustrates, only a simple rolling relation defines the
model and there is no simulation of tire-road-friction and tire slip involved.

The speed :math:`n_{eng}` of the engine is constraint by the speed
:math:`n_{wheel}` of the wheels, the ratio :math:`i_{a}` of the axle, and the
current ratio :math:`i_{g}` of the gearbox, depending of the current gear:

.. math::
   n_{eng} = n_{wheel} \cdot i_{a} \cdot i_{g}
   

The required acceleration :math:`a_{set}` determines the torque :math:`M_{wheel}`
at the wheels, as the overall mass :math:`m` of the vehicle has to be accelerated by
this torque:

.. math::
   M_{wheel} = m \cdot a_{set} \cdot r_{stat}


As the equation illustrates, no driving resistances other than the inertia of
the vehicle are simulated by this model. The required torque :math:`M_{wheel}`
at the wheels must be generated by the engine, considering the ratios of the
powertrain:

.. math::
   M_{eng} = \frac{M_{wheel}}{i_{a} \cdot i_{g}}


As the current speed :math:`n_{eng}` of the engine, the required acceleration
:math:`a_{set}` of the vehicle, and the required torque :math:`M_{eng}` of the
engine are known or defined by the above equations, this knowledge can be used
to determine the estimated positions of the accelerator and the brake pedal or
to calculate the optimal gear for the required acceleration :math:`a_{set}`,
if a positive acceleration is needed. For the accelerator pedal and optimal gear
calculation, there is still some missing knowledge about the engine itself.
The relationship between the current engine speed :math:`n_{eng}` and the
possible engine torque :math:`M_{eng}` is determined by a simple engine map,
which is illustrated in :numref:`image_EngineMap_`:

.. _image_EngineMap_:

.. figure:: _static/images/EngineMap.png
   :alt: Simplified engine map used for the inverted powertrain model
   
   Simplified engine map used for the inverted powertrain model


The engine map is defined by two characteristic curves:


* :math:`M_{eng,max}` defines the maximum torque, which can be produced by the
  engine at a specific engine speed :math:`n_{eng}`. This is directly connected
  with the maximum possible actuation of the accelerator pedal.
* :math:`M_{eng,drag}` defines the drag torque, which is produced by the engine
  at a specific engine speed :math:`n_{eng}`, if the accelerator pedal is not
  actuated at all.

The two characteristic curves are only defined between the minimum and maximum
possible speed of the engine. As the two characteristic curves are directly
related to a full actuation (:math:`act_{olc,stat} = 1`) and no actuation
(:math:`act_{olc,stat} = 0`) of the accelerator pedal, the estimated pedal
position for the accelerator pedal :math:`act_{olc,stat}`, which produces the
currently required engine torque :math:`M_{eng}`, can be calculated by linear
interpolation between these two characteristic curves at the current engine
speed :math:`n_{eng}`.

The same thing is done for the calculation of the estimated brake pedal position:


* The maximum acuation of the brake pedal (:math:`act_{olc,stat} = -1`) is
  directly connected with the maximum possible deceleration of the vehicle, which
  is simply set to 1G (:math:`= 9.81 m/s^2`).
* No actuation of the brake pedal (:math:`act_{olc,stat} = 0`) also produces no
  deceleration.

Between these two possible decelerations, the estimated brake pedal position to
produce the required acceleration :math:`a_{set}` is calculated by linear
interpolation.

The logic for the calculation of the static controller output :math:`act_{olc,stat}`,
utilizing the considerations above, is defined as follows:

If the required acceleration :math:`a_{set}` is smaller than zero, it can be
produced by the engine drag torque or the brake system. If the engine drag
torque :math:`M_{eng,drag}` at the current engine speed :math:`n_{eng}` is not
strong enough to meet the engine torque :math:`M_{eng}`, which would be
necessary to produce :math:`a_{set}`, an **additional** actuation of the brake
pedal is applied (the driver does not open the clutch, so the engine drag torque
is also applied to the wheels). This mechanism is not considering a change in
gears, but uses the current gear of the gearbox (the driver does not consider to
shift down for a stronger effect of the engine drag torque).

If the required acceleration :math:`a_{set}` is greater than or equal to zero,
it can only be produced by the powertrain. First of all, the optimal gear to
produce the engine torque :math:`M_{eng}` required for :math:`a_{set}` is
calculated, which is described in :ref:`the subsequent subsection<algorithm_longitudinal_driver_openloop_optimal_gear_>`.
With this chosen gear, the current engine speed :math:`n_{eng}` is calculated.
:math:`n_{eng}` is used to determine the maximum engine torque :math:`M_{eng,max}`
and the engine drag torque :math:`M_{eng,drag}` from the characteristic curves.
As these two values are connected to a full actuation and no actuation of the
accelerator pedal, the estimated accelerator pedal position, which is necessary
to produce the required acceleration :math:`a_{set}`, can be calculated by
linear interpolation.

The state variable :math:`v_{act}` is received from the module :ref:`sensor_driver_`,
the required acceleration :math:`a_{set}` is received from a driver behaviour
model like *old ref: algorithm_scm_monolithisch_* , and the
vehicle parameters m, :math:`r_{stat}`, :math:`i_{a}, :math:`i_{g}` per gear,
and the information about the engine map are received from the module :ref:`parameters_agent_`.

The calculations above are done in the function PedalPosition of the sub-module
ALongitudinalDataProvider, which is called in the function CalculatePedalPositionAndGear
of AlgorithmLongitudinalDriver. 

.. _algorithm_longitudinal_driver_openloop_optimal_gear_:

Algorithm for optimal gear determination
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The algorithm for the determination of the optimal gear for the required
acceleration :math:`a_{set}` is implemented in the function GetBestGear of the
sub-module ALongitudinalDataProvider. It is called within the function
PedalPosition of the same sub-module, which applies the :ref:`inverted longitudinal dynamics model<algorithm_longitudinal_driver_inverted_dynamics_model_>`
for the open-loop acceleration controller. The algorithm receives the current
velocity :math:`v_{act}` of the vehicle and the required acceleration :math:`a_{set}`.

Within a for-loop over all gears, the engine speed :math:`n_{eng}` is calculated
for each gear, which would result from the current velocity :math:`v_{act}`,
as well as the maximum engine torque :math:`M_{eng,max}` and the engine drag
torque :math:`M_{eng,drag}` resulting from these engine speeds. Violating the
minimum engine speed :math:`n_{eng,min}` and the maximum engine speed :math:`n_{eng,max}`
is not considered in this step (resulting engine speeds are written as they result
from the powertrain calculation and the engine torques are set to the lower or
upper limits of the characteristic curves respectively, if the engine speed
range is violated).

The results above are further processed in a second for-loop over all gears.
The following aspects are checked to determine, if a gear fits the current
required state:


* Is the resulting engine speed :math:`n_{eng}` within the range of :math:`n_{eng,min}`
  and :math:`n_{eng,max}`?
* Is the resulting engine torque :math:`M_{eng}` to produce the required
  acceleration :math:`a_{set}` within the range of :math:`M_{eng,drag}` and :math:`M_{eng,max}`?

The lowest gear that fits these criterions sets a Boolean foundGear true and the
results (gear number and resulting engine speed :math:`n_{eng}`) are written in
the result vector. This is done for all further gears, until a gear does not fit
the requirements anymore. This initiates the return of the last sufficient gear
and its resulting engine speed :math:`n_{eng}`. A short example should
illustrate this logic:


#. The 2nd gear activates the Boolean foundGear, because :math:`n_{eng}` does
   not exceed :math:`n_{eng,max}` anymore
#. The 4th gear activates the return of the results, because the required engine
   torque :math:`M_{eng}` would exceed the maximum possible engine torque :math:`M_{eng,max}`
   at the corresponding engine speed :math:`n_{eng}`
#. The algorithm chooses the 3rd gear as optimum and returns it and its
   corresponding engine speed :math:`n_{eng}`

This logic therefore guarantees, that always the highest possible gear is chosen
by the driver.


.. _algorithm_longitudinal_driver_transient_behaviour_:

Transient behaviour application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The application of the transient behaviour is modelled as a discrete first order
controller with an amplification equal one. The input signal is the static pedal
position :math:`act_{olc,stat}` from the :ref:`inverted longitudinal dynamics model<algorithm_longitudinal_driver_inverted_dynamics_model_>`
and the output signal is the real pedal position :math:`act_{olc}` of the
open-loop acceleration controller. A discrete first order controller calculates
the value of the output signal for the current time step :math:`i`, depending on the
current value of the input signal and the value of the output signal from the
last time step :math:`i-1` by using two factors :math:`\alpha` and :math:`\beta`.
Its transfer function is described as:

.. math::
  act_{olc,i} = \alpha \cdot act_{olc,stat,i} + \beta \cdot act_{olc,i-1}
  

The more common form of a first order controller is the continuous transfer
function, including a time constant :math:`T_{1}` and the time derivative
:math:`dact_{olc} / dt` of the output signal (amplification is also neglected):

.. math::
   \frac{T_{1} \cdot dact_{olc}}{dt} + act_{olc} = act_{olc,stat}
   

:math:`T_{1}` expresses, how long the output signal needs to exceed 63 % of the
input signal. The change :math:`dact_{olc}` of the output signal can also be
written in a discrete form and the change dt of time can be interpreted as the
step size of the discrete simulation. The ratio of the step size towards the
time constant :math:`T_{1}` is furthermore substituted with :math:`T_{trans}`,
which is dimensionless:

.. math::
   T_{trans} = \frac{T_{1}}{dt}
   
.. math::
   dact_{olc} = act_{olc,i} - act_{olc, i-1}


The continuous form can therefore be transformed to match the discrete form above:

.. math::
   T_{trans} \cdot (act_{olc,i} - act_{olc,i-1}) + act_{olc,i} = act_{olc,stat,i}
   
.. math::
   T_{trans} \cdot act_{olc,i} - T_{trans} \cdot act_{olc,i-1} + act_{olc,i} &= act_{olc,stat,i}

.. math::
   (T_{trans} + 1 ) \cdot act_{olc,i} = act_{olc,stat,i} + T_{trans} \cdot act_{olc,i-1}
   
.. math::
   act_{olc,i} &= \frac{1}{(T_{trans} + 1) \cdot act_{olc,stat,o}} + \frac{T_{trans}}{T_{trans} + 1} \cdot act_{olc,i-1}



A comparison of coefficients between the two transfer functions result in the
following equations for the factors :math:`\alpha` and :math:`beta`:

.. math::
   \alpha = \frac{1}{T_{trans} + 1}
   
.. math::
   \beta = \frac{T_{trans}}{T_{trans} + 1}



The behaviour is parameterized through :math:`T_{1}` to make it independent
from the simulation step size. For normal situations, :math:`T_{1}` is set to
0.12 s. If the controller detects a major brake manoeuvre, :math:`T_{1}` is set
to 0.08 s. A major brake manoeuvre is declared by a desired brake pedal position
greater or equal to 60 % of the maximum possible pedal actuation way.
The calculations above are done in the function ApplyFirstOrderTransientBehaviour
of the sub-module ALongitudinalDataProvider. It is applied to the
:ref:`inverted longitudinal dynamics model<algorithm_longitudinal_driver_inverted_dynamics_model_>`
in the function ControllerPedalPosition of AlgorithmLongitudinalDriver.

.. _algorithm_longitudinal_driver_closedloop_compensatory_control_:

Compensatory control on stabilization level
-------------------------------------------

The velocity controller is designed as a simple proportional controller. The
general transfer function of a P-controller with the input signal velocity error
:math:`\Delta v` and the output signal pedal position :math:`act_{clc}` is described as:

.. math::
   act_{clc} = P_{clc} \cdot \Delta v
   

The controller amplification :math:`P_{clc}` is therefore described as the ratio
between input signal and output signal:

.. math::
   P_{clc} = \frac{act_{clc}}{\Delta v}


The controller amplification :math:`P_{clc}` therefore expresses how many
percent of accelerator or brake pedal position are generated by 1 m/s velocity
error. :math:`P_{clc}` is currently a constant with a value of 0.5 s/m, which
was empirically determined in the Ph.D. thesis of Marcus Mai, Dresden University
of Technology. A dead zone of :math:`\pm 0.3 \frac{m}{s}` for :math:`\Delta v` is also implemented
within the controller, which has to be exceeded for the controller even doing
anything.

As :numref:`image_LongitudinalControllerSignalFlow_` illustrates, the calculation of :math:`\Delta v` is done by subtracting
the actual velocity :math:`v_{act}` of the vehicle from the desired velocity
:math:`v_{set}` by the driver behaviour model, which is the desired acceleration
:math:`a_{set}` integrated over time:

.. math::
   \Delta v = v_{set} - v_{act}

As openPASS is a time discrete simulation platform, integration of values over
time must also be done in incremental steps. In relation to :math:`v_{set}`,
this means which value the velocity in the current time step i should have,
if :math:`a_{set}` from the last time step :math:`i - 1` would have been applied
properly to the actual velocity :math:`v_{act}` in this last time step,
considering the step size dt:

.. math::
   v_{set,i} = a_{set,i-1} \cdot dt + v_{act,i-1}


The calculations above are done in the function ClosedLoopController of the
sub-module ALongitudinalDataProvider. It is applied to the :ref:`open-loop controller<algorithm_longitudinal_driver_open_loop_>`
in the function ControllerPedalPosition of AlgorithmLongitudinalDriver.


Application of the pedal change time
------------------------------------

The application of the pedal change time is done by the function RegardPedalChange,
which calls further sub-functions. It checks, if a sign change in the controller
output act occurs and counter checks it towards already ongoing pedal changes.
If a pedal change have to be applied, a pedal change time is sampled from a
lognormal distribution, which is defined by a minimum value, a mean value and a
standard distribution. These parameters are received from the module
:ref:`parameters_agent_`. The sampling of a
new value from this distribution is done for each pedal change to consider
intra-individual differences of the driver. Over the next time steps, the time
since the beginning of the pedal change is summed up, until the pedal change
time is met. In this time frame, no actuation of any pedal is possible and the
output values :math:`p_{a}` and :math:`p_{b}` are set to zero. After the
completion of the pedal change, the actuations from the controllers are further
processed and split up into the output signals.
