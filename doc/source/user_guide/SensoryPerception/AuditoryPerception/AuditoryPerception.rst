..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _auditory_perception_:

*******************
Auditory Perception
*******************

Although primarily relying on vision, SCM also models the driver's perception of acoustic signals.
The part of the model concerned with the driver's hearing abilities is refered to as the auditory perception.
It detects ADAS warnings and information from inside of the driver's own vehicle based on HMI signals.
The perceived HMI signals are transferred to the gaze control algortihm as a bottom-up request.
Thereby, simplified sound information processing enables the auditory perception to influence the driver's gaze behaviour (see :ref:`gaze_control_bottom_up_`).

.. TODO delete phrase?

The general definitions of *signal*, *stimulus* and *impulse* can be found :ref:`here<gaze_control_reception_definition_>`.

.. TODO: delete from here?

Modelling the driver's perception of acoustic signals is done in several key steps:
The main function is to check for the perception of acoustic signals with parameters of the own vehicle from :ref:`sensor_driver_`.
There are functions that check for the acoustic impulse perception from the driver's own vehicle. Further, they stochastically decide whether to accept the stimulus as basis for actions.

* Check for the perception of acoustic signals.
  The perceptiveness of the driver depends on some of the configured driver parameters.

* Check for the acoustic stimulus perception from the driver's own vehicle.

* Use a stochastic routine to decide if the stimulus will lead to an action.

Besides the steps above there are also functionalities which are currently being considered and included only as stub implementations (placeholder code).
Receiving acoustic stimuli from surrounding vehicles is intended to be possible in the future, but has not been part of the model yet.

.. TODO: they actually don't exist --> remove??

To guarantee an accurate simulation of general human hearing properties, two functions are intended to check sounds and noise accordingly.
Firstly, the human hearing threshold imposes certain limitations regarding amplitude and frequency of a sound that can be heard.
Secondly, an coustic signal has to exceed the listening threshold of the human sensory system over the surrounding noise to be heard.
The latter has to compare the amplitude for the investigated sound and the amplitude of the noise in the environment at the relevant frequency.
Both mechanisms have not been implemented in SCM yet.

