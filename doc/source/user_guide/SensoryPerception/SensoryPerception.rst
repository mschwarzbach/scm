..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _sensory_perception_:

******************
Sensory Perception
******************

Modelling human-like behaviour starts with implementing authentic sensory perception processes.
How information is perceived and what mechanisms are affected by the sensory perception, is described in the following sections.
Since the vast majority of information is noticed visually for car drivers, the :ref:`visual_perception_` also plays a superior role within SCM.
A chapter on :ref:`auditory_perception_` describes how and what kind of acoustic signals SCM deals with.


.. _gaze_control_reception_definition_:

**General definition of perceived information**

.. TODO check section

First of all the meanings of *signal*, *stimulus* and *impulse* need to be further clarified in the context of :ref:`optical<visual_perception_>` and :ref:`auditory<auditory_perception_>` perception.
It names the information at different stages of the agent's perception process.
A signal is described as the raw input, as perceived by the driver (which means how it is presented to the ear or eyes).
After the signal has been processed by the human sensory system (which means how it is detected in the brain but the meaning is not yet processed) it is called a stimulus.
Once the final interpretation by the human cognition (only in case of strongly habituated stimulus response tasks which have a direct feedback) happened the information is called an impulse.


.. toctree::
    :glob:
    :hidden:

    VisualPerception/*
    AuditoryPerception/*
