..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _action_states_:

*************
Action States
*************


.. _longitudinal_action_states_:

Longitudinal Action States
==========================


.. _action_manager_leading_vehicle_:
 
Determination of the current leading vehicle for longitudinal guidance 
----------------------------------------------------------------------

The determination takes place in two steps:
First, all AOIs are determined which might contain the leading vehicle.
Secondly, the current leading vehicle is selected from the list that was created in the first step.
This process is only performed if the current microscopic or mesoscopic situation or the current *lateral action state* has changed.
Possible AOIs considered for the current leading vehicle depend on the current lateral action state:

.. TODO update term for MergeRegulate when merging chapter is finished

* **Lane change**: AOIs ahead of ego in the current lane and the lane that is being changed into are considered.

* **Merging**: AOIs directly ahead of ego in the current lane and the AOI used as MergeRegulate are considered.

* **Otherwise**: AOIs ahead of ego in the current lane are considered.

Among the possible AOIs that contain observed vehicles, the one with the lowest TTC is selected.
If no TTC is available, the logic first defaults to the smallest time gap and failing that, the shortest logitudinal distance.


Determination process for the longitudinal action state 
-------------------------------------------------------

.. TODO: move all decision trees to formula chapter (or similar) and ref them here

The decision tree of the longitudinal action state is shown as following.

.. figure:: ../_static/images/LongitudinalActionStateSelection.png
   :alt: Decision tree for the longitudinal action state

   Decision tree for the longitudinal action state


.. _lateral_action_states_:

Lateral Action States
=====================


Determination process for the lateral action state
--------------------------------------------------

In this chapter, the decision possibilities regarding LateralActionState for each individual situation are presented in the form of decision trees.

.. figure:: ../_static/images/LateralActionStatesCurrentLaneBlocked.png
   :alt: Decision tree for the situation CURRENT_LANE_BLOCKED

   Decision tree for the situation CURRENT_LANE_BLOCKED

.. figure:: ../_static/images/LateralActionStatesFreeDriving.png
   :alt: Decision tree for the situation FREE_DRIVING

   Decision tree for the situation FREE_DRIVING

.. figure:: ../_static/images/LateralActionStatesFollowingDriving.png
   :alt: Decision tree for the situation FOLLOWING_DRIVING

   Decision tree for the situation FOLLOWING_DRIVING

.. figure:: ../_static/images/LateralActionStatesObstacleOnCurrentLane.png
   :alt: Decision tree for the situation OBSTACLE_ON_CURRENT_LANE

   Decision tree for the situation OBSTACLE_ON_CURRENT_LANE

.. figure:: ../_static/images/LateralActionStatesLaneChangerFromLeft.png
   :alt: Decision tree for the situation LANE_CHANGER_FROM_LEFT

   Decision tree for the situation LANE_CHANGER_FROM_LEFT

.. figure:: ../_static/images/LateralActionStatesLaneChangerFromRight.png
   :alt: Decision tree for the situation LANE_CHANGER_FROM_RIGHT

   Decision tree for the situation LANE_CHANGER_FROM_RIGHT

.. figure:: ../_static/images/LateralActionStatesSuspiciousObjectInLeftLane.png
   :alt: Decision tree for the situation SUSPICIOUS_OBJECT_IN_LEFT_LANE

   Decision tree for the situation SUSPICIOUS_OBJECT_IN_LEFT_LANE

.. figure:: ../_static/images/LateralActionStatesSuspiciousObjectInRightLane.png
   :alt: Decision tree for the situation SUSPICIOUS_OBJECT_IN_RIGHT_LANE

   Decision tree for the situation SUSPICIOUS_OBJECT_IN_RIGHT_LANE

.. figure:: ../_static/images/LateralActionStatesSideCollisionRiskFromLeft.png
   :alt: Decision tree for the situation SIDE_COLLISION_RISK_FROM_LEFT

   Decision tree for the situation SIDE_COLLISION_RISK_FROM_LEFT

.. figure:: ../_static/images/LateralActionStatesSideCollisionRiskFromRight.png
   :alt: Decision tree for the situation SIDE_COLLISION_RISK_FROM_RIGHT

   Decision tree for the situation SIDE_COLLISION_RISK_FROM_RIGHT

In case of a collision, the current lateral action state is simply maintained.
