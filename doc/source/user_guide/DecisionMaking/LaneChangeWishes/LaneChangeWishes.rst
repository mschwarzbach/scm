..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _lane_change_wishes_:

******************
Lane Change Wishes
******************

Convenience with the present lane is primarily determined by considering whether the driver is able to drive at the desired velocity on a particular lane or not.
This is directly linked to the formation of a wish for a lane change or to remain on the ego lane (respective values between 0 and 1).
The results are used later to determine the actual lane change request depending on further boundary conditions.
Eventually, this is a major factor in the selection of the current *lateral action state*.


Lane Convenience
----------------

The following conditions lead directly to the assignment of a lane change intensity or a remaining intensity of 0:

* an obstacle was detected in the corresponding FRONT-AOI (or SIDE-AOI in adjacent lanes) *or*

* a lane ending can be seen within the influence distance for a general end of lane *or*

* the speed limit on the lane is too low for the own target velocity *or*

* the own target velocity cannot be driven for longer than 20 seconds on the lane (calculation in relation to the respective FRONT-AOI; guide value for the right-keeping rule from StVO).

The following conditions, on the other hand, lead directly to the assignment of a lane change intensity or a remaining intensity of 1:

* the speed limit on the lane is greater than or equal to the own target velocity *and*

* the own target velocity corresponds to the average lane velocity *or*

* the velocity of the FRONT-AOI is greater than or equal to the target velocity *or*

* the FRONT-AOI is empty, i.e. the lane is free of preceding vehicles.

If the conditions described above do not apply, the intensity of the lane change or remaining intensity is calculated as a function of a maximum permissible velocity difference between the target velocity and the average lane velocity:

* If the target velocity is more than 10 km/h above the mean lane velocity, the resulting intensity is equal to 0.

* If the target velocity is more than 25 km/h below the mean lane velocity, the resulting intensity is also equal to 0.

* In between, values are interpolated linearly, with the maximum on both sides being *target velocity = mean lane velocity*.


Lane change wish
----------------

First, the principal lane change intensities or the remaining intensity on the ego lane are calculated as explained previously.
If the following conditions are met, the lane change intensities to the left and to the right are set directly to 0:

* the respective lane does not exist at all

* the respective lane is too narrow for the own vehicle (comparison of lane width and vehicle width)

Also, the lane change intensity to the right is set to 0, if the :ref:`prohibition of right overtaking<right_overtaking_prohibition_>`
applies for the current leading vehicle on the ego lane. The calculation of the lane change request does not only use the directly returned intensities, but also scales them by certain factors if necessary:

* *ego keeping factor*: scaling of the lane change intensities to strengthen the own lane

* *right keeping factor*: scaling of the remaining intensity on the ego lane to strengthen the right-keeping rule (only for comparison between *Ego* and *Right*; via *DriverProfile* adjustable behavior parameter)

A lane change request generally exists if the following conditions are fulfilled:

* the remaining intensity on the ego lane is less than or equal to 0 and the lane change intensity to the left or to the right is greater than 0 *or*

* the remaining intensity on the ego lane is greater than 0 *but* the lane change intensity to the left multiplied by *ego keeping factor* is greater than
  the remaining intensity *or*

* the remaining intensity on the ego lane is greater than 0 *but* the lane change intensity to the right multiplied by *ego keeping factor* is greater than or equal to the remaining intensity multiplied by *right keeping factor*.

In general, this means either there is no reason to stay on the ego lane or a lane change would be worthwhile, since the ego vehicle would make much better progress on the other lanes or compliance with the right-keeping rule would not be obstructive.


.. _right_overtaking_prohibition_:

Application of right overtaking prohibition
===========================================

A traffic rule on German motorways and in many other countries that have a right keeping rule is the prohibition to overtake other vehicles on the outer lane.
It is only permitted in case of a traffic jam.
The 'outer lane' refers to the national traffic rules, i.e. if there is left-hand or right-hand traffic.
As the majority of countries follow the right-hand traffic model, the right lane would be considered as the outer lane.
In the UK for example, the opposite would be true and the left lane becomes the outer lane.
The application of the prohibition rule can be evaluated for two scenarios:

#. The driver approaches a slower vehicle on the left/right lane (AOI LEFT_FRONT or RIGHT_FRONT, see :ref:`gaze_control_`) and must decelerate to not overtake it on the outer side.
#. The driver approaches a slower vehicle on their own lane (AOI EGO_FRONT, see :ref:`gaze_control_`) and must not change to the right lane to overtake it.

The first scenario is considered as a traffic condition within the algorithm for the calculation of the driver's target velocity (see :ref:`Calculation of the legal velocity<legal_velocity_>`).
The second scenario is considerd in the evaluation of :ref:`lane_change_wishes_`.

The logic for preventing outer lane overtaking is only applied if **none** of the following conditions are met:

General driver related conditions:

* the driver is currently performing a :ref:`merge preparation manoeuvre<merge_preparation_>`
* the driver is willingly violating the right overtaking prohibition (see driver behaviour parameter :ref:`rightOvertakingProhibitionIgnoringQuota<driver_parameters_simplified_cognition_>`)
* the driver is currently on a highway entry or exit lane.

General AOI-related conditions, independent from the chosen AOI:

* the AOI to evaluate does not contain a vehicle
* the vehicle within the AOI is still too far away, which means that the :ref:`influencing distance<time_headways_>` is not violated.

.. TODO: check list again

Driver related conditions depending on the AOI:

* the current situation of the driver is irrelevant for the application of the right overtaking prohibition, which is true, if it is not one of the following situations (or vice versa: the traffic rule is only considered in the mentioned situations):

  * LEFT_FRONT:

    * FREE_DRIVING
    * NORMAL_DRIVING
    * TOO_CLOSE
    * ANTICIPATED_LANE_CHANGER_FROM_LEFT
    * DETECTED_LANE_CHANGER_FROM_LEFT

  * EGO_FRONT:

    * FREE_DRIVING
    * NORMAL_DRIVING
    * TOO_CLOSE
    * *not* CURRENT_LANE_BLOCKED

AOI-related conditions dependening on the chosen AOI:

LEFT_FRONT:

* the vehicle is slower than 60 km/h
* the mean velocity of the left lane is slower then the expected velocity minus 20 km/h
* the velocity of the vehicle is slower then the expected velocity minus 20 km/h
* the velocity of the vehicle is slower than 80 % of the mean velocity of the left lane

The expected velocity is considered as the speed limit of the left lane or, in case there is no speed limit, as the recommended velocity on German motorways of 130 km/h.

EGO_FRONT:

* the lane convenience on the right lane is higher than on the ego lane **and**
* the right lane is empty, which means, there are no objects in the AOIs RIGHT_FRONT_FAR, RIGHT_FRONT and RIGHT_SIDE.

If both conditions apply, the possibility to overtake on the right is given and preferable, but must be prevented by the traffic rule.


Lane change prevention
======================

A legitimate basis for an agent to conduct a lane change close to the spawn point cannot be guaranteed.
This is due to the fact, that the surrounding vehicles are unknown to the agent until they spawn themselves.
This is especially problematic when the other agent is significantly faster than the lane changer, since it may lead to unwanted collision cases.
To prevent agents from making a decision to change lanes due to incomplete data, an initial section of restricted lateral movement has to be passed.

**Spawn Zone:**
The mentioned section that spans over a distance, determined by the velocity at spawn multiplied by one second.

**Prevention mechanism:**
The prevention of lane changes is achieved by manipulating the intensities of the lateral action states.
After assigning the maximum value of the cases lane change, swerving and urgent lane change (either to the right or to the left side) to the intent to change left or right action state, they are reset to 0.
Thus, the subsequent sampling will choose no lateral displacing action.

The lane change prevention is bypassed in case of a frontal collision with no possibility to defuse the situation by braking.
The reliability requirement for these actions is adjusted due to the special circumstances.
The urgent update threshold (the basis for the calculation of the individual reliability thresholds) scales linearly from 0 at spawn to the default value at the end of the spawn zone.
This requires the agent to check the side lane more often the closer to the spawn.

Exiting the spawn zone undoes all alterations by the mechanism and additionally triggers a sampling of the lateral action state the next time it is requested.
In low-density traffic situations, an absence of change in the surrounding environment could otherwise block a new sampling for long durations and thus circumvent mechanisms like right keeping.
