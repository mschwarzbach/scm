..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _decision_making_:

***************
Decision Making
***************


**ActionManager**

The SCM agent regularly evaluates whether a change needs to be made to the current actions on the basis of the recognized situation.
The decision is made separately through longitudinal and lateral guidance.
The lateral guidance is defined by selecting a *lateral action state* and the longitudinal guidance is defined by selecting a *longitudinal action state*.
Both states only describe the respective action pattern.
The detailed execution of the actions in the sense of vehicle dynamics control or the secondary driving tasks is performed in a later step.

In general, lateral action states can be divided into lane keeping and lane change.
The longitudinal guidance is not yet taken into account.
Longitudinal action states differ in free driving, following driving and target braking.
See :ref:`action_states_` for a detailed description of the action states.
For more detailed information on action execution, refer to the documentation of the :ref:`driving_behaviour_`.

The lateral action states can be subdivided into lane keeping and lane changing.
Lane keeping includes the states of lane keeping itself as well as the intent to change lanes and preparation (to merge) states, both to the left and right.
Lane change actions contain lane changes themselves as well as urgent and comfort swerving acts for both directions.

Subdivisions of the longitudinal action states are free driving, following driving and target braking.
Free driving only contains the SPEED_ADJUSTMENT action state, while following driving is made up of APPROACHING, FOLLOWING_AT_DESIRED_DISTANCE and FALL_BACK_TO_DESIRED_DISTANCE.
Finally, target braking can be archived with the action states BRAKING_TO_END_OF_LANE or STOPPED.


**Modifying or aborting a lane change**

After each new decision about the lateral action state, the model needs to determine whether the current lane change is modified or aborted.
This information is necessary for the execution of the action.
The new lateral action state is compared with the last one.
If a regular lane change becomes an urgent lane change or vice versa and the direction of the lane change does not change, the lane change counts as modified.
If the direction of a lane change changes, the lane change counts as aborted; the distinction between regular and urgent lane change is irrelevant.


**Decision-making model for braking or evading**

A central mechanism for the decision-making on actions within critical situations is the evaluation whether a collision with another object can be prevented by braking, evading, both actions, or if it is unavoidable.
For this function, the vehicle ahead of the ego agent is investigated.
The model focusses on the current state variables of the driver's own vehicle and the vehicle under evaluation and neglects any consideration of further surroundings (for instance, if a neighbouring lane to evade into even exists).
Those further considerations have to be done later.
The calculated result is a set of intensities for the following options:

* braking
* evading left
* evading right
* braking or evading
* collsion unavoidable

The evaluation basically runs down two separate calculations for the possibility of evading and braking.
In both cases, there are dynamically calculated thresholds for the TTC towards the evaluated object, under which a collision is no longer preventable by the corresponding action.
This is derived from basic kinematic calculations, but is strongly supported by the :ref:`tau_theory_`.

For braking, there is a relative longitudinal velocity :math:`\Delta v_{lon}` between the driver's vehicle and the surrounding object, which must be reduced to zero by means of a longitudinal deceleration :math:`d_{lon}` of the driver (assuming that the other object has no longitudinal acceleration) before the relative distance :math:`\Delta s` reaches 0.
This leads to a simple formulation for the TTC threshold for the braking reaction:

.. math::
   TTC_{brake} = - \frac{\Delta v_{lon}}{2 \cdot d_{lon}}

For a fixed value for :math:`d_{lon}`, which can be provided by the driver, :math:`TTC_{brake}` increases with higher relative velocities :math:`\Delta v_{lon}`.

.. TODO: next passage: can we add a ref to obstruction after "assigned to the surrounding object"?

For steering, there is a certain lateral distance :math:`\Delta t` the driver must overcome to pass the other object without collision.
This distance is calculated by the current lateral obstruction values assigned to the surrounding object and a driver-specific safety distance for evading (see :ref:`driver_parameters_`).
For the lateral distance to travel, the current lateral velocity :math:`v_{lat,ego}` of the driver's own vehicle is taken into account, but also a constant lateral acceleration :math:`a_{lat}` is assumed.
This leads to an equation of motion with constant acceleration.
The TTC threshold for the braking reaction is simply the time value of this equation, for when the targeted lateral distance :math:`\Delta t`
becomes 0:

.. math::
   TTC_{steer} = \frac{v_{lat,ego}}{a_{lat}} + \sqrt{\frac{v_{lat,ego}^2}{a_{lat}^2} + \frac{\Delta t}{a_{lat}}}

For a fixed value for :math:`a_{lat}`, which can be provided by the driver, and neglecting the lateral velocity :math:`v_{lat,ego}`, :math:`TTC_{steer}` increases the higher the targeted lateral distance :math:`\Delta t` is.

If both of these thresholds are plotted into a diagram over the relative velocity :math:`\Delta v_{lon}` and the targeted lateral distance :math:`\Delta t` is kept constant for a hypothetical scenario, the areas of possible actions can be
distinguished depending on the current value of the TTC:

.. figure:: _static/images/BrakingOrEvadingBasic.png
   :target: BrakingOrEvadingBasic.png
   :alt: Possible actions to prevent a collision depending on the current relative longitudinal velocity

   Possible actions to prevent a collision depending on the current relative longitudinal velocity

Using these strict thresholds would lead to clear distinguishable actions and there would be no need for an action intensity model here, but this is not realistic for human decision making, which can be prone to misjudgements.
Therefore, upper and lower thresholds are implemented in the model, which take into account a certain degree of uncertainty.
This is done by using different values for the acceleration terms in the above stated equations.
First, the basic values are taken from the driver behaviour parameters, whereas for braking, the driver's maximum longitudinal deceleration is considered and for evading, the driver's maximum lateral acceleration is considered (see :ref:`driver_parameters_`).
For the upper TTC threshold, the associated acceleration is reduced by 20% and for the lower TTC threshold, the associated acceleration is increased by 20%.
This leads to a decision field for the model like depicted in the following figure:

.. figure:: _static/images/BrakingOrEvadingFuzzy.png
   :alt: Evaluation thresholds of the braking or evading mechanism

   Evaluation thresholds of the braking or evading mechanism

The uncertainty expressed by the upper and lower thresholds are considered by divided intensities between the possible actions, which is done by linear interpolation between the TTC values of the upper and lower threshold and the current value of the actual TTC.
For instance, in case of distinguishing between *braking* and *collision unavoidable*:

* If the current :math:`TTC` is higher than the upper limit for :math:`TTC_{brake}`, the intensity for *braking* is clearly 1 and the intensity for *collision unavoidable* is clearly 0.
* If the current :math:`TTC` falls below the upper limit for :math:`TTC_{brake}`, the intensity for *braking* is decreased towards 0 by linearly interpolating between the two thresholds. The intensity for *collision unavoidable* is determined by the remaining intensity, which adds up to 1.
* If the current :math:`TTC` falls below the lower limit for :math:`TTC_{brake}` , the intensity for *braking* is clearly 0 and the intensity for *collision unavoidable* is clearly 1.


**Decision-making model for urgent swerving**

Swerving constitutes an evading maneuver which does not necessarily require a complete lane change.
The execution of such a maneuver is explained in more detail in :ref:`manoeuvres_swerving_`.
Here, only the decision-making process for swerving is explained: First, for an emergency maneuver, afterwards for a comfort maneuver.

The overall principle is very similar to the evaluation for an evading maneuver in the section above.
The only difference is, that there are no uncertainties regarding the driver's maximum lateral acceleration, because the evaluation of a swerving maneuver is always done after the generic assessment of the braking or evading model and only if this model indicates that an evading action is a feasible action to prevent a collision.
As already stated in the section above, this model does not incorporate any other elements of the environment than the opponent.

.. TODO check implementation status of less severe collisions in the side lane as mentioned in the last senstence

First, the general possibilities to swerve around an obstacle on the left or on the right are evaluated by the same equation as for the TTC threshold for steering in the braking or evading model.
If at least one of these two options is possible, the model checks, if this requires the driver's own vehicle to cross its lane markings, or if the evading action can be accomplished within the boundaries of the current ego lane.
If this is not the case, the neighbouring lane, which has to be intruded by the driver is checked for existence and if there is no lane, the swerving maneuver in this direction is discarded.
However, if there is a lane, the driver can not simply swerve right into it, if they hit another vehicle of object by doing so.
Therefore, the AOIs SIDE and FRONT of the considered lane are evaluated, whether the planned swerving maneuver results in a side collision with either of these objects.
The driver-specific safety distance for evading is also considered in this case (see :ref:`driver_parameters_`).
The evaluation of side collision with vehicles on the neighbouring lane can be neglected in certain situations, if this would be the less severe crash compared to the obstacle, which is to be swerved around.

.. TODO: last sentence above --> this is prepared in the code, but not yet used by the ActionManager.

This assessment is done for both directions of possible swerving maneuvers (left and right).
If neither of them is possible, the driver cannot swerve around the obstacle and therefore decides for an emergency braking, even if the crash is not preventable by braking.
If only one of the two options is possible, a swerving maneuver to that side will be selected.
If both options are possible, the intensities  for the lateral action states of swerving to the left and swerving to the right are calculated based on the comparison of the times needed to perform the evading maneuver on each side:

.. math::
   \text{Intensity(swerving\_left)} = 1 - \frac{t_{\text{swerving\_left}}}{t_{\text{swerving\_left}} + t_{\text{swerving\_right}}}

.. math::
   \text{Intensity(swerving\_right)} = 1 - \frac{t_{\text{swerving\_right}}}{t_{\text{swerving\_left}} + t_{\text{swerving\_right}}}

Lane keeping is disregarded as an option in this case.
The decision for a side is then the result of the sampler for the lateral action state.


.. _comfort_swerving_decision_:

**Decision-making model for comfort swerving**

Comfort swerving constitutes a maneuver similar to the emergency swerving above but planned in advance and currently completely inside its own lane.
This maneuver requires the following aspects:

* relevant *situation* (*following driving* and *obstacle on current lane*)
* relevant *action state* (*intent to change lanes* (left/right) and *lane keeping*)
* enough space to complete the maneuver (longitudinally to the swerved object and laterally between object and lane boundary)
* a velocity difference to the object that is being approached of 10% compared to the ego's velocity

Comfort swerving will only be initiated once the agent's relative net distance falls below 1.2 times the maneuver length.
This threshold will prevent a too early lateral displacement.
Between the detection of comfort swerving and the execution, ego may decide to enact another maneuver to resolve the issue (i.e. a lane change) and subsequently cancel the swerving.
After the maneuver is started it will be executed and can not be canceled.
The chosen lateral action state will be either *comfort swerving* (left/right).
The execution is the same mechanic as in an emergency swerving maneuver (see :ref:`manoeuvres_swerving_`).

The main differences to the emergency swerving (other than the trigger mechanism) can be found in the parameter of the lateral acceleration.
The comfort maneuver uses the lateral comfort acceleration to calculate the necessary components (like maneuver length) instead of the possible maximum.
Another difference is the greater applied lateral safety distance between ego and the object while passing.
As mentioned in the beginning, planned swerving is only conducted when the whole process does not require the agent to leave the ego lane.


.. toctree::
    :glob:
    :hidden:

    HighCognitive/*
    */*
