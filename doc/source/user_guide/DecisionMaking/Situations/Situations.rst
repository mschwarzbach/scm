..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _situations_:

**********
Situations
**********

The SCM driver assesses the current traffic situation based on their current knowledge about the real world.
Situation assessment means the analysis of other road users' behaviour in relation to the driver's own vehicle or changes in the traffic environment in general, which may or may not require a change in the driver's current actions.
The driver's current actions themselves are not considered in the assessment of the situation, because a situation is solely described by terms of "What is going on around me?"


.. _overview_situation_assessment_:

Overview situation assessment
=============================

The assessment mechanism for situations is based on the recognition of predefined situation patterns.
Each situation pattern is described by a number of features which must be present in the current traffic environment.
The presence of those features also influences the base intensity for the recognition of the associated situation pattern.
The base intensity is derived from the product of the entry probability of a situation and an intensity derived from the necessary acceleration action required to defuse the threat.

The situation assessment algorithm is structured into :ref:`microscopic_situations_` and :ref:`mesoscopic_situations_` which are further explained below.
The most intense microscopic and mesoscopic traffic situation patterns are sampled under specific circumstances to choose the current microscopic traffic situation and the current mesoscopic traffic situation the driver decides to focus on (see :ref:`decision_making_mechanism_` for more information about this mechanism).

To structure the decision-making process on the current microscopic and mesoscopic situation even further, the base intensity of each situation pattern is multiplied by a risk class (see :ref:`base_intensity_calculation_`).
This enables SCM to take into account, that some situation patterns require more attention than others do.
More information on these risk-classes and their concatenations with the situation patterns are given further below.


.. _microscopic_situations_:

Microscopic situation clusters
------------------------------

A microscopic situation cluster focuses on vehicles or objects, which are paired with specific AOIs (see :ref:`gaze_control_`).
The currently existing clusters are described in the table below in terms of their relevant AOIs and the recognizable situation patterns for each microscopic situation cluster.
For more detailed information about the situation patterns please see :ref:`detailed_description_situation_cluster_`.

.. TODO: create illustration instead of the table below

.. list-table::
  :header-rows: 1

  * - Cluster
    - Connected AOIs
    - Recognizable situation patterns
  * - FRONT
    - EGO_FRONT, EGO_FRONT_FAR, LEFT_FRONT, LEFT_FRONT_FAR, RIGHT_FRONT, RIGHT_FRONT_FAR
    - FOLLOWING_DRIVING, OBSTACLE_ON_CURRENT_LANE
  * - LEFT
    - LEFT_SIDE, LEFT_FRONT, LEFT_FRONT_FAR
    - LANE_CHANGER_FROM_LEFT, SUSPICIOUS_OBJECT_IN_LEFT_LANE, SIDE_COLLISION_RISK_FROM_LEFT
  * - RIGHT
    - RIGHT_SIDE, RIGHT_FRONT, RIGHT_FRONT_FAR
    - LANE_CHANGER_FROM_RIGHT, SUSPICIOUS_OBJECT_IN_RIGHT_LANE, SIDE_COLLISION_RISK_FROM_RIGHT
  * - REAR
    - EGO_REAR
    -


.. _mesoscopic_situations_:

Mesoscopic situation clusters
-----------------------------

A mesoscopic situation cluster focuses on mesoscopic aspects of the traffic, such as the recognition of a traffic jam or the end of a lane.
The mesoscopic situation cluster is therefore (currently) not connected with specific AOIs (see :ref:`gaze_control_`).
The currently existing clusters are described in the table below in terms of the recognizable situation patterns for each mesoscopic situation cluster.
For more detailed information about the situation patterns, please see :ref:`detailed_description_situation_cluster_`.

.. list-table::
  :header-rows: 1

  * - Cluster
    - Relevant situation patterns
  * - MESO
    - CURRENT_LANE_BLOCKED, MANDATORY_EXIT, QUEUED_TRAFFIC


.. _base_intensity_calculation_:

Base intensity calculation
--------------------------

The calculation of the base intensity is made up by the product of two components.
The first one is the probability of a situation pattern to happen.
This probability is displayed by a value between 0 and 1 and is derived from one or multiple features.
The features, their meaning and requirements are described in more detail in the following chapter for each cluster.
The second required component is the intensity of the necessary acceleration (including deceleration) to prevent any threat.
After the calculation of the acceleration value (i.e. situation pattern following driving: "the deceleration required to match the leading vehicles speed within the boundaries of the net distance between the objects"), it is assigned a value between 0 and 8 corresponding to its "risk level".

.. list-table::
  :header-rows: 1

  * - Risk class
    - Intensity
    - Meaning
  * - No risk
    - 0
    - No risk situation (accelerating with a frontal threat or decelerating with a rear threat)
  * - Low risk
    - 1
    - Constant value for all accelerations below comfort acceleration / deceleration
  * - Medium risk
    - 1 ... 4
    - Linear increase between comfort and double the comfort acceleration / deceleration
  * - High risk
    - 4 ... 8
    - Linear increase between double comfort and maximum acceleration / deceleration

.. _image_IntensityOfAcceleration_:

.. figure:: ../_static/images/IntensityOfAcceleration.png
  :alt: Intensity of acceleration action

  Intensity of acceleration action

The resulting base intensity is determined for each situation pattern and later used to sample a situation.


.. _detailed_description_situation_cluster_:

Detailed Description
====================

This section gives more detailed information about the features of situation recognition algorithms, in addition to :ref:`the more general overview above<overview_situation_assessment_>`.


.. _decision_making_mechanism_:

Decision-making mechanism for current situations
------------------------------------------------

As described in the overview section, the situation assessment algorithm is structured into four :ref:`microscopic_situations_` and one :ref:`mesoscopic_situations_`.
Situation assessment is at first done within each cluster.
All situation pattern intensities are calculated according to their probability and magnitude of acceleration (see :ref:`base_intensity_calculation_` for a detailed description).
In case of multiple occurrences of the same situation pattern (detected in more than one area of interest) only the one with the highest intensity is retained.
The most intense situation patterns are further processed according to a specific maximum number of situation patterns per situation cluster, which is stated in the table below.

.. list-table::
  :header-rows: 1

  * - Cluster
    - Maximum number of processable situation patterns
  * - FRONT
    - 4
  * - LEFT
    - 2
  * - RIGHT
    - 2
  * - REAR
    - 4
  * - MESO
    - 2

Thereafter, a maximum of four microscopic situation patterns (also the four most intense) are gathered from the microscopic clusters and are further processed for the decision-making on the microscopic situation.
This whole ranking and reduction mechanism is taking into account the limited processing capabilities of a human driver while being confronted with a multitude of different stimuli.

The four most intense microscopic and the two most intense mesoscopic traffic situation patterns are sampled under specific circumstances to choose the current microscopic traffic situation and the current mesoscopic traffic situation the driver decides to focus on.
The circumstances for a situation pattern sampling and therefore a decision-making are as follows:

* the base reaction time since the last decision-making process is over *and*
* the corresponding situation intensity maps have changed in terms of size (number of recognized situation patterns) *or* sequence (a situation pattern has become more intense than another)


Detailed descriptions of situation clusters
-------------------------------------------

The assessment mechanism of the submodule SituationManager is based on the recognition of predefined situation patterns.
Each situation pattern is described by a number of features, which must be present in the current traffic environment.
The presence of those features also influences the base intensity for the recognition of the associated situation pattern.
There are several possibilities, how the base intensity can be calculated from the presence of their features:

* Basic features must all be present to allow the calculation of a situation pattern intensity at all. If only one basic feature is missing, the corresponding situation pattern intensity is always 0.
* Additional features can amplify the base intensity for a situation pattern, but do not have any influence if the basic features are not present.

As these features are representations of the environment the driver has perceived, the quality and the age of those mental information is crucial for the situation pattern intensity calculation (time since last information update by the several parts of the human's field of view; see :ref:`gaze_control_` and :ref:`information_requests_` for more information).
If a feature is calculated using non-reliable mental information, it is not taken into account for the situation pattern intensity calculation and a top-down-request is then sent.
The sufficiency of data age is handled within the central top-down-mechanism of the Mental Model itself; the requested quality per feature is described in the feature tables of the subsequent chapters.

The following subsections give descriptions on the features per situation cluster and thereafter the associations of features and the situation patterns, so a definition of the situation patterns themselves can be drawn by comparing the two respective tables per situation cluster.


Features and associated situation patterns for front cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. TODO should the feature names be preserved in this or similar form?

The following situation pattern (through their respective features) are checked for the frontal ego lane AOIs (ego front, ego front far) as well as the frontal AOIs of the direct side lanes (left front, right front, left front far, right front far).
The later ones are only checked in case of a detected lateral obstruction between the object and ego agent.

.. list-table::
  :header-rows: 1

  * - Feature
    - Meaning
    - Required quality
  * - IsInfluencingDistanceViolated
    - The vehicle in EGO_FRONT is near enough to have influence on the driver's longitudinal guidance
    - Periphery
  * - IsNearEnoughForFollowing
    - The distance to the vehicle in EGO_FRONT is smaller than or equal to the following equilibrium distance plus 0.5 seconds
    - UFOV
  * - IsMinimumFollowingDistanceViolated
    - The distance to the vehicle in EGO_FRONT is smaller than or equal to the minimum following distance
    - UFOV
  * - IsObstacle
    - The object in EGO_FRONT is an obstacle (stationary object or very slow vehicle)
    - UFOV
  * - HasJamVelocityFront
    - The vehicle in EGO_FRONT is slower than the boundary speed for a traffic jam
    - UFOV
  * - HasJamVelocityEgo
    - The driver's own vehicle is slower than the boundary speed for a traffic jam
    - --

HasJamVelocityEgo has no required quality, because it is related to the driver's own vehicle.
Information about the driver's own vehicle is currently independent from the driver's visual perception and therefore, there is no need for a top-down-request.
HasJamVelocityFront is also further directed to the situation pattern intensity calculation of the mesoscopic cluster (see :ref:`mesoscopic_cluster_detailed_`).

.. list-table::
  :header-rows: 1

  * - Situation pattern
    - Basic features
    - Additional features
  * - FOLLOWING_DRIVING
    - IsInfluencingDistanceViolated
    - --
  * - OBSTACLE_ON_CURRENT_LANE
    - IsObstacle
    - --


Features and associated situation patterns for rear cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Only the rear area of the ego lane is relevant for this cluster.
Since there currently are no situations associated with the rear cluster, it is not active.


Features and associated situation patterns for left/right cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Most of the situation patterns are checked for either the vehicle in LEFT_FRONT / RIGHT_FRONT and the vehicle in LEFT_FRONT_FAR / RIGHT_FRONT_FAR.
But the direct left and right side areas are checked as well.

.. list-table::
  :header-rows: 1

  * - Features
    - Meaning
    - Required quality
  * - IsLaneDriveablePerceived
    - The end of the left lane is not visible
    - --
  * - IsInfluencingDistanceViolated
    - The vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_FRONT_FAR/RIGHT_FRONT_FAR is near enough to have influence on the driver's longitudinal guidance
    - Periphery
  * - IsNearEnoughForFollowing
    - The distance to the vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_FRONT_FAR/RIGHT_FRONT_FAR is smaller than or equal to the following equilibrium distance plus 0.5 seconds
    - UFOV
  * - IsMinimumFollowingDistanceViolated
    - The distance to the vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_FRONT_FAR/RIGHT_FRONT_FAR is smaller than or equal to the minimum following distance
    - UFOV
  * - HasIntentionalLaneCrossingConflict
    - The vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_SIDE/RIGHT_SIDE shows an intention to cross into ego lane by showing respective indicator signals
    - Periphery
  * - HasActualLaneCrossingConflict
    - The vehicle in LEFT_FRONT/RIGHT_FRONT, LEFT_FRONT_FAR/RIGHT_FRONT_FAR or LEFT_SIDE/RIGHT_SIDE is already lane changing into ego lane and a collision course is also detected
    - Fovea
  * - HasAnticipatedLaneCrossingConflict
    - If the vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_FRONT_FAR/RIGHT_FRONT_FAR started changing into ego lane right now, this would result in an actual lane crossing conflict
    - UFOV
  * - IntensityForNeedToChangeLane
    - The anticipated intensity for a lane change of the vehicle in LEFT_FRONT/RIGHT_FRONT or LEFT_FRONT_FAR/RIGHT_FRONT_FAR induced by an ending left lane
    - UFOV
  * - IntensityForSlowerLeadingVehicle
    - The anticipated intensity for a lane change of the vehicle in LEFT_FRONT/RIGHT_FRONT induced by a slower vehicle in LEFT_FRONT_FAR/RIGHT_FRONT_FAR
    - UFOV
  * - HasJamVelocityLeft
    - The vehicle in LEFT_FRONT/RIGHT_FRONT is slower than the boundary speed for a traffic jam
    - UFOV
  * - HasSuspiciousBehaviour
    - The vehicle in LEFT_FRONT/RIGHT_FRONT is slower than ego, meanLaneVelocity and is not in traffic jam
    - UFOV

IsLaneDriveablePerceived has no required quality, because it is related to the mesoscopic characteristics.
Information about the mesoscopic characteristics is currently independent from the driver's visual perception and therefore, there is no need for a top-down-request.
Also, HasJamVelocityLeft is only calculated in the left cluster and is further directed to the situation pattern intensity calculation of the mesoscopic cluster (:ref:`mesoscopic_cluster_detailed_`).

Note: The following situation pattern are stated for the left side but do apply equally for the right side.

.. list-table::
  :header-rows: 1

  * - Situation pattern
    - Basic features
    - Additional features
  * - LANE_CHANGER_FROM_LEFT
    - IsInfluencingDistanceViolated, HasActualLaneCrossingConflict
    - HasIntentionalLaneCrossingConflict
  * - SIDE_COLLISION_RISK_FROM_LEFT
    - HasActualLaneCrossingConflict
    - HasIntentionalLaneCrossingConflict
  * - SUSPICIOUS_OBJECT_IN_LEFT_LANE
    - HasSuspiciousBehaviour
    -

For further handling of the chosen situation it is also saved, whether a lane changer is a high risk situation (minimum following distance violated) and if it is an actual detected lane changer or an anticipated one.


.. _mesoscopic_cluster_detailed_:

Features and associated situation patterns for mesoscopic cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Information about the mesoscopic characteristics is currently independent from the driver's visual perception and therefore, there is no need for top-down-requests in this cluster.

.. list-table::
   :header-rows: 1

   * - Features
     - Meaning
   * - IsLaneDriveablePerceived
     - The end of the ego lane is not visible
   * - IsApproachingMotorwayExit
     - The driver is approaching their target motorway exit
   * - HasJamVelocityFront
     - The vehicle in EGO_FRONT is slower than the boundary speed for a traffic jam
   * - HasJamVelocityLeft
     - The vehicle in LEFT_FRONT is slower than the boundary speed for a traffic jam
   * - HasJamVelocityRight
     - The vehicle in RIGHT_FRONT is slower than the boundary speed for a traffic jam

The features HasJamVelocityFront, HasJamVelocityLeft and HasJamVelocityRight are calculated in there respective clusters (and are thereby checked for sufficient information quality) and are only transferred to the mesoscopic cluster for further calculations.

.. list-table::
  :header-rows: 1

  * - Situation pattern
    - Basic features
    - Additional features
  * - CURRENT_LANE_BLOCKED
    - IsLaneDriveablePerceived (must not be true!)
    - --
  * - MANDATORY_EXIT
    - IsApproachingMotorwayExit
    - --
  * - QUEUED_TRAFFIC
    - HasJamVelocityFront
    - HasJamVelocityLeft, HasJamVelocityRight

The calculation the situation pattern QUEUED_TRAFFIC (equal traffic jam) is a more complicated than it might appear in the table above.
Actually, all lanes that can be perceived by the driver must have jam velocity.


Other situation patterns
~~~~~~~~~~~~~~~~~~~~~~~~

There are two more situation patterns with special purposes, as they are not
directly related to any of the situation clusters:

* FREE_DRIVING is the standard situation, which is selected for the current microscopic traffic situation or the current mesoscopic traffic situation, if the situation assessment mechanism was not able to select any other of the respective situation patterns (e.g. on an empty road).
* COLLISION is a failsafe situation, which is selected for the current microscopic traffic situation and the current mesoscopic traffic situation, if the agent had an accident (this is done to prevent any further actions by the agent).
