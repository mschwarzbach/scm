..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _scm_reference_guide_:

Coordinate systems and formulary
################################

This chapter serves as an introductory reference guide for SCM. It shows the definitions of the coordinate systems, which are used and required by the model, and their transformations (see :ref:`scm_reference_guide_coordinate_systems`). Subsequently, a formulary for frequently used calculations within the model is given (see :ref:`scm_reference_guide_formulary`).


.. _scm_reference_guide_coordinate_systems:

Coordinate systems
==================

There are three coordinate systems, which are used by SCM:

*
  Inertial coordinates
*
  Road coordinates
*
  Vehicle coordinates

The inertial system (also called world system) is a stationary, planar, Cartesian coordinate system, which is defined by its two coordinates *x* and *y*.

The road system is a stationary, planar, path coordinate system, which is defined within the inertial system by drawing a curvature progression *RoadCurvature(s)* (inverse of the curve radius) along its path coordinate *s* (see :numref:`image_RoadCoordinateSystem_`). This results in two additional system-related coordinates:

*
  Lateral coordinate *t* (always orthogonal to the path coordinate *s*)
*
  Angle of orientation *Heading* (torsion of the local path coordinate *s* towards the inertial coordinate *x*)

.. _image_RoadCoordinateSystem_:

.. figure:: _static/images/RoadCoordinateSystem.png
   :alt: Definition of the road system in reference to the inertial system

   Definition of the road system in reference to the inertial system

The vehicle system is a body-fixed, planar, Cartesian coordinate system, which is defined by the vehicle’s longitudinal axis *Lon* and lateral axis *Lat*. Its orientation is defined by:

*
  the angle *Yaw* towards the inertial coordinate *x*
*
  the angle *RelativeYaw* towards the road coordinate *s*

The point of reference for an agent, i.e. the origin of the vehicle system, is set in the centre of the rear axle, but the *MainLocatePoint*, which is primarily used for the lane assignment of an agent, is set in the centre of the vehicle’s front-bumper (see :numref:`image_VehicleCoordinateSystem_`).

.. _image_VehicleCoordinateSystem_:

.. figure:: _static/images/VehicleCoordinateSystem.png
   :alt: Definition of the vehicle system in reference to the inertial (left) and road system (right)

   Definition of the vehicle system in reference to the inertial (left) and road system (right)

The calculations for the transformation of these three coordinate systems are given subsequently.


.. _scm_reference_guide_coordinate_systems_road_to_inertial:

Transforming road to inertial coordinates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The road system is defined within the inertial system by drawing a curvature progression *RoadCurvature(s)* (inverse of the curve radius) along its path coordinate *s*. In mathematical terms, this constitutes a numerical integration over *s*, which is defined according to the generating system of planar paths by Kramer (Kraftfahrzeugführung: Modelle - Simulation - Regelung, Munich, Carl Hanser, 2008; see :numref:`image_TransformatingRoadToInertialSystem_`).

.. _image_TransformatingRoadToInertialSystem_:

.. figure:: _static/images/TransformatingRoadToInertialSystem.png
   :alt: Transforming road to inertial coordinates (generating system of planar paths by Kramer)

   Transforming road to inertial coordinates (generating system of planar paths by Kramer)

The incremental change of the path coordinate *s* between the current integration step *i* and the previous integration step *i – 1* is defined as *ds*\ :sub:`i`. The progression of this increment along the orientation *Heading*\ :sub:`i – 1` from the previous step results in incremental changes of the inertial coordinates *x* and *y*:

.. math::
   dx_{i} = cos(Heading_{i – 1}) \cdot ds_{i}

.. math::
   dy_{i} = sin(Heading_{i – 1}) \cdot ds_{i}

The new inertial coordinates are therefore:

.. math::
   x_{i} = x_{i – 1} + dx_{i}

.. math::
   y_{i} = y_{i – 1} + dy_{i}

The progression of the current *RoadCurvature*\ :sub:`i` along the incremental change *ds*\ :sub:`i` also results in an incremental change *dHeading*\ :sub:`i` of the orientation:

.. math::
   dHeading_{i} = arcsin(RoadCurvature_{i} \cdot ds_{i})

The new orientation is therefore:

.. math::
   Heading_{i} = Heading_{i – 1} + dHeading_{i}


.. _scm_reference_guide_coordinate_systems_vehicle_to_inertial:

Transforming vehicle to inertial coordinates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As the vehicle system is body-fixed, the transformation of the position and orientation of a vehicle into stationary coordinate systems can only be realized by means of the equations of motion of the vehicle’s body along the simulation time. The *LongitudinalAcceleration* along the vehicle’s longitudinal axis *Lon* (see :numref:`image_VehicleCoordinateSystem_`) is fundamental for the motion of an agent. It is generated by the module :ref:`dynamics_regular_driving_`:

*
  Accelerating due to a driving torque from the powertrain results in a positive *LongitudinalAcceleration*
*
  Decelerating due to driving resistances or a braking torque from the brake system results in a negative *LongitudinalAcceleration*

The lateral dynamics of SCM are based on a simple rolling relationship according to Ackermann, i.e. neither tire slip nor external disturbances (e.g. side wind, cross slope) are considered. Therefore, the *LateralAcceleration* along the vehicle’s lateral axis *Lat* (see :numref:`image_VehicleCoordinateSystem_`) is defined by the constraint of the centrifugal acceleration:

.. math::
   LateralAcceleration = LongitudinalVelocity^2 \cdot VehicleCurvature

The *LongitudinalVelocity* along the vehicle’s longitudinal axis *Lon* is defined by the numerical integration of the *LongitudinalAcceleration* over *time*:

.. math::
   LongitudinalVelocity_{i} = LongitudinalAcceleration_{i} \cdot dtime + LongitudinalVelocity_{i – 1}

The *VehicleCurvature* of the trajectory, which is drawn by the vehicle’s rear axle, is defined by the Ackermann condition of *WheelBase*, *SteeringWheelAngle*, and the overall *SteeringRatio* of the steering system:

.. math::
   VehicleCurvature = \frac{tan(\frac{SteeringWheelAngle}{SteeringRatio})}{WheelBase}

The *LateralAcceleration* is the result of the centrifugal force, which the vehicle’s body experiences due to cornering and which is countered by the lateral forces of the tires. It is therefore not causing the body’s motion, but it is resulting from it. The representation of the vehicle body’s motion in the inertial system must therefore not be realized by simply integrating *LongitudinalAcceleration* and *LateralAcceleration* and vectorising the velocities by means of the *Yaw* angle. It is also done by numerical integration over *time* by means of the generating system of planar paths by Kramer (see :ref:`scm_reference_guide_coordinate_systems_road_to_inertial`). It must thereby be considered that the **kinematical** lateral acceleration and velocity along the vehicle’s lateral axis *Lat* is equal to zero, because tire slip is neglected (i.e. the vehicle cannot just slip sideways). Therefore, the above mentioned *LongitudinalAcceleration* and *LongitudinalVelocity* of the vehicle system are also equal to the vectorised *AbsoluteAcceleration* and *AbsoluteVelocity* of the vehicle’s body:

.. math::
   AbsoluteAcceleration/Velocity = \sqrt{LongitudinalAcceleration/Velocity^2 + LateralAcceleration/Velocity^2}

.. math::
   AbsoluteAcceleration/Velocity = \sqrt{LongitudinalAcceleration/Velocity^2 + 0}

.. math::
   AbsoluteAcceleration/Velocity = LongitudinalAcceleration/Velocity

As the terms *LongitudinalAcceleration/Velocity* and *LateralAcceleration/Velocity* are also used to describe accelerations and velocities in the road system (see :ref:`scm_reference_guide_formulary_road_system`), *AbsoluteAcceleration/Velocity* is primarily used for accelerations and velocities caused by powertrain, brake system, or driving resistances.

The incremental change of the vehicle position along its longitudinal position *Lon* between the current time step *i* and the previous time step *i – 1* is defined as *dLon*\ :sub:`i`:

.. math::
   dLon_{i} = AbsoluteVelocity_{i} \cdot dtime

The progression of this increment along the orientation *Yaw*\ :sub:`i – 1` from the previous time step results in incremental changes of the inertial coordinates *x* and *y*:

.. math::
   dx_{i} = cos(Yaw_{i – 1}) \cdot dLon_{i}

.. math::
   dy_{i} = sin(Yaw_{i – 1}) \cdot dLon_{i}

The new inertial coordinates are therefore:

.. math::
   x_{i} = x_{i – 1} + dx_{i}

.. math::
   y_{i} = y_{i – 1} + dy_{i}

The progression of the current *VehicleCurvature*\ :sub:`i` along the incremental change *dLon*\ :sub:`i` also results in an incremental change *dYaw*\ :sub:`i` of the orientation:

.. math::
   dYaw_{i} = arctan(VehicleCurvature_{i} \cdot dLon_{i})

The new orientation is therefore:

.. math::
   Yaw_{i} = Yaw_{i – 1} + dYaw_{i}

Contrary to the generating system of planar paths for the road system (see :ref:`scm_reference_guide_coordinate_systems_road_to_inertial`), arctangent is used for the calculation of *dYaw*\ :sub:`i` instead of arcsine here, because *VehicleCurvature* is defined for the vehicle’s rear axle and not its front axle.


.. _scm_reference_guide_coordinate_systems_vehicle_to_road:

Transforming vehicle to road coordinates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Transforming vehicle coordinates into the road system is very similar to :ref:`scm_reference_guide_coordinate_systems_vehicle_to_inertial`. The most crucial thing that must be considered is that the road system is not a purely Cartesian coordinate system, but is determined by a *RoadCurvature* itself. Focus point of the transformation is the angle of orientation *RelativeYaw* of the vehicle’s body towards the local path coordinate *s*. The incremental change *dRelativeYaw* results from the difference between the *RoadCurvature* and the *VehicleCurvature*. This difference in curvature results in different local values for the angles of orientation *Heading* (road) and *Yaw* (vehicle) towards the inertial system. The incremental change *dRelativeYaw* is therefore defined by the difference of the incremental changes *dYaw* and *dHeading*:

.. math::
   dRelativeYaw = dYaw – dHeading

Considering this condition, the transformation can be done by numerical integration over *time* by means of the generating system of planar paths by Kramer (see :ref:`scm_reference_guide_coordinate_systems_road_to_inertial`). The incremental change of the vehicle position along its longitudinal position *Lon* between the current time step *i* and the previous time step *i – 1* is defined as *dLon*\ :sub:`i`:

.. math::
   dLon_{i} = AbsoluteVelocity_{i} \cdot dtime

The progression of this increment along the orientation *RelativeYaw*\ :sub:`i – 1` from the previous time step results in incremental changes of the road coordinates *s* and *t*:

.. math::
   ds_{i} = cos(RelativeYaw_{i – 1}) \cdot dLon_{i}

.. math::
   dt_{i} = sin(RelativeYaw_{i – 1}) \cdot dLon_{i}

The new road coordinates are therefore:

.. math::
   s_{i} = s_{i – 1} + ds_{i}

.. math::
   t_{i} = t_{i – 1} + dt_{i}

The progression of the current *VehicleCurvature*\ :sub:`i` along the incremental change *dLon*\ :sub:`i` results in an incremental change *dYaw*\ :sub:`i` of the vehicle’s orientation towards the inertial system:

.. math::
   dYaw_{i} = arctan(VehicleCurvature_{i} \cdot dLon_{i})

The progression of the current *RoadCurvature*\ :sub:`i` along the incremental change *ds*\ :sub:`i` results in an incremental change *dHeading*\ :sub:`i` of the road’s orientation towards the inertial system:

.. math::
   dHeading_{i} = arcsin(RoadCurvature_{i} \cdot ds_{i})

This means that the incremental change *dRelativeYaw* is calculated by:

.. math::
   dRelativeYaw_{i} = arctan(VehicleCurvature_{i} \cdot dLon_{i}) – arcsin(RoadCurvature_{i} \cdot ds_{i})

The new orientation of the vehicle’s body towards the road is therefore:

.. math::
   RelativeYaw_{i} = RelativeYaw_{i – 1} + dRelativeYaw_{i}

.. _scm_reference_guide_formulary:


Formulary
=========

This section provides a collection of frequently used calculations for state variables within SCM. It is a summary of the explanations from the previous section for a huge part, but it also provides additional information. The chapter is organized along the coordinate systems (see :ref:`scm_reference_guide_coordinate_systems`) in which the state variables are defined. Numerical integrations over *time* or the path coordinate *s* of the road are indicated by subscripts and prefixes:

*
  *i* indicates values from the current integration step
*
  *i – 1* indicates values from the previous integration step
*
  Values prefixed with a *d* indicate incremental changes of this state from the previous to the current integration step


.. _scm_reference_guide_formulary_vehicle_system:

State variables defined in the vehicle system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :header-rows: 1

   * - State variable
     - Calculation
     - SI-Unit
     - Annotation
   * - *LongitudinalAcceleration* resp. **AbsoluteAcceleration**
     - [From :ref:`dynamics_regular_driving_`]
     - m/s²
     - As SCM’s lateral dynamics model is a simple rolling relationship, the *LongitudinalAcceleration* is equal to the vectorised *AbsoluteAcceleration* in kinematical terms
   * - *LateralAcceleration*
     - = *LongitudinalVelocity*\ :sup:`2` ∙ *VehicleCurvature*
     - m/s²
     - Centrifugal acceleration, i.e. **no** kinematic acceleration. Because of the slip-less lateral dynamics model, this acceleration is not causal for the lateral motion of the vehicle’s body.
   * - *LongitudinalVelocity* resp. **AbsoluteVelocity**
     - *AbsoluteVelocity*\ :sub:`i` = *AbsoluteAcceleration*\ :sub:`i` ∙ *dtime* + *AbsoluteVelocity*\ :sub:`i – 1`
     - m/s
     - As for the accelerations, the *LongitudinalVelocity* is equal to the vectorised *AbsoluteVelocity* in kinematical terms
   * - *LateralVelocity*
     - = 0
     - m/s
     - Because of the slip-less lateral dynamics model, the *LateralVelocity* **in the vehicle coordinate system** is always equal zero, i.e. the vehicle cannot just slip sideways
   * - *VehicleCurvature*
     - = tan(*SteeringWheelAngle* / *SteeringRatio*) / *WheelBase*
     - 1/m
     - Ackermann conditions for the vehicle’s rear axle
   * - *Yaw*
     - *Yaw*\ :sub:`i` = *Yaw*\ :sub:`i – 1` + arctan(*VehicleCurvature*\ :sub:`i` ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*)
     - rad
     - Angle of orientation of the vehicle’s longitudinal axes *Lon* towards the inertial coordinate *x*
   * - *RelativeYaw*
     - *RelativeYaw*\ :sub:`i` = *RelativeYaw*\ :sub:`i – 1` + arctan(*VehicleCurvature*\ :sub:`i` ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*) – arcsin(*RoadCurvature*\ :sub:`i` ∙ cos(*RelativeYaw*\ :sub:`i – 1`) ∙ *VehicleCurvature*\ :sub:`i` ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*)
     - rad
     - Angle of orientation of the vehicle’s longitudinal axes *Lon* towards the local road coordinate *s*


.. _scm_reference_guide_formulary_road_system:

State variables defined in the road system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The state variables *RelativeYaw*, *AbsoulteVelocity*, and *AbsoluteAcceleration* are defined under :ref:`scm_reference_guide_formulary_vehicle_system`.

The following table shows state variables for regarding a single agent in the road system. The definition of the *DistancesToLaneBoundary* and the relevant measurements for their calculation are illustrated in :numref:`image_DistanceToLaneBoundaryIllustration_`.

.. list-table::
   :header-rows: 1

   * - State variable
     - Calculation
     - SI-Unit
     - Annotation
   * - *LongitudinalAcceleration*
     - = *AbsoluteAcceleration* ∙ cos(*RelativeYaw*)
     - m/s²
     - Acceleration of an agent along the path coordinate *s*
   * - *LateralAcceleration*
     - = *AbsoluteAcceleration* ∙ sin(*RelativeYaw*)
     - m/s²
     - Acceleration of an agent along the lateral coordinate *t*
   * - *LongitudinalVelocity*
     - = *AbsoluteVelocity* ∙ cos(*RelativeYaw*)
     - m/s
     - Velocity of an agent along the path coordinate *s*
   * - *LateralVelocity*
     - = *AbsoluteVelocity* ∙ sin(*RelativeYaw*)
     - m/s
     - Velocity of an agent along the lateral coordinate *t*
   * - *s*
     - *s*\ :sub:`i` = *s*\ :sub:`i – 1` + cos(*RelativeYaw*\ :sub:`i – 1`) ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*
     - m
     - Path coordinate of an agent
   * - *t*
     - *t*\ :sub:`i` = *t*\ :sub:`i – 1` + sin(*RelativeYaw*\ :sub:`i – 1`) ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*
     - m
     - Lateral coordinate of an agent
   * - *Heading*
     - *Heading*\ :sub:`i` = *Heading*\ :sub:`i – 1` + arcsin(*RoadCurvature*\ :sub:`i` ∙ *ds*\ :sub:`i`)
     - rad
     - Angle of orientation of the local path coordinate *s* towards the inertial coordinate *x*
   * - *DistanceToLaneBoundaryLeft*
     - **For 0 ≤ RelativeYaw ≤ π / 2:** *laneWidth* / 2 – *t*\ :sub:`veh` – *width* / 2 ∙ cos(*RelativeYaw*) – (*x* + *length* / 2) ∙ sin(*RelativeYaw*); **For –π / 2 ≤ RelativeYaw ≤ 0:** *laneWidth* / 2 – *t*\ :sub:`veh` – *width* / 2 ∙ cos(*RelativeYaw*) – (*x* – *length* / 2) ∙ sin(*RelativeYaw*)
     - m
     - Smallest, lateral distance of the vehicle’s *BoundingBox* to the left lane boundary of the agent’s *MainLane*. Values greater zero mean the vehicle has not crossed the boundary. Values smaller zero indicate how far the vehicle is already intruding the adjacent left lane. Calculation is only valid for road without curvature!
   * - *DistanceToLaneBoundaryRight*
     - **For 0 ≤ RelativeYaw ≤ π / 2:** *laneWidth* / 2 + *t*\ :sub:`veh` – *width* / 2 ∙ cos(*RelativeYaw*) + (*x* – *length* / 2) ∙ sin(*RelativeYaw*); **For –π / 2 ≤ RelativeYaw ≤ 0:** *laneWidth* / 2 + *t*\ :sub:`veh` – *width* / 2 ∙ cos(*RelativeYaw*) + (*x* + *length* / 2) ∙ sin(*RelativeYaw*)
     - m
     - Smallest, lateral distance of the vehicle’s *BoundingBox* to the right lane boundary of the agent’s *MainLane*. Values greater zero mean the vehicle has not crossed the boundary. Values smaller zero indicate how far the vehicle is already intruding the adjacent right lane. Calculation is only valid for road without curvature!
   * - *TimeToLineCrossingLeft*
     - **For DistanceToLaneBoundaryLeft ≤ 0:** 0; **For DistanceToLaneBoundaryLeft > 0:** *DistanceToLaneBoundaryLeft* / *LateralVelocity*
     - s
     - Negative values indicate a motion to the right, i.e. the vehicle is moving away from the left lane boundary
   * - *TimeToLineCrossingRight*
     - **For DistanceToLaneBoundaryRight ≤ 0:** 0; **For DistanceToLaneBoundaryRight > 0:** –\ *DistanceToLaneBoundaryRight* / *LateralVelocity*
     - s
     - Negative values indicate a motion to the left, i.e. the vehicle is moving away from the right lane boundary

.. _image_DistanceToLaneBoundaryIllustration_:

.. figure:: _static/images/DistanceToLaneBoundaryIllustration.png
   :alt: Definition of the DistancesToLaneBoundary (left) and relevant measurements for their calculation (right)

   Definition of the DistancesToLaneBoundary (left) and relevant measurements for their calculation (right)

Evaluations of the relative motion between two agents are primarily done in the road system. The most important premise for these evaluations is that relative distances are always greater zero, as long as there are no overlaps along the coordinate under consideration. As the relative velocity is the time derivative of the relative distance, this results in the following definitions:

* A relative velocity less than zero means that the relative distance is decreasing
* A relative velocity equal zero means that the relative distance remains constant
* A relative velocity greater than zero means that the relative distance is increasing

The same definitions apply for the relative acceleration, because it is the time derivative of the relative velocity:

* A relative acceleration less than zero means that the relative velocity is decreasing
* A relative acceleration equal zero means that the relative velocity remains constant
* A relative acceleration greater than zero means that the relative velocity is increasing

In the following table, index *1* stands for the leading vehicle resp. the vehicle, which is driving more left, and index *2* stands for the following vehicle resp. the vehicle, which is driving more right (see :numref:`image_DefinitionsForRegardingTwoAgents_`). This means that object *1* always has the greater value of the regarded road coordinate *s* or *t*. The calculations for relative distances shown in the following table neglect rotations of the vehicles’ *BoundingBoxes* and therefore serve only as estimates!

.. list-table::
   :header-rows: 1

   * - State variable
     - Calculation
     - SI-Unit
     - Annotation
   * - *RelativeAccelerationLongitudinal*
     - = *LongitudinalAcceleration*\ :sub:`1` – *LongitudinalAcceleration*\ :sub:`2`
     - m/s²
     - Relative acceleration along the path coordinate *s*
   * - *RelativeAccelerationLateral*
     - = *LateralAcceleration*\ :sub:`1` – *LateralAcceleration*\ :sub:`2`
     - m/s²
     - Relative acceleration along the lateral coordinate *t*
   * - *RelativeVelocityLongitudinal*
     - = *LongitudinalVelocity*\ :sub:`1` – *LongitudinalVelocity*\ :sub:`2`
     - m/s
     - Relative velocity along the path coordinate *s*
   * - *RelativeVelocityLateral*
     - = *LateralVelocity*\ :sub:`1` – *LateralVelocity*\ :sub:`2`
     - m/s
     - Relative velocity along the lateral coordinate *t*
   * - *RelativeNetDistanceLongitudinal*
     - = (*s*\ :sub:`1` + *DistanceReferencePointToLeadingEdge*\ :sub:`1` – *Length*\ :sub:`1`) – (*s*\ :sub:`2` + *DistanceReferencePointToLeadingEdge*\ :sub:`2`)
     - m
     - Net-distance along the path coordinate *s* (rear-bumper to front-bumper)
   * - *RelativeNetDistanceLateral*
     - = (*t*\ :sub:`1` – 0,5 ∙ *Width*\ :sub:`1`) – (*t*\ :sub:`2` + 0,5 ∙ *Width*\ :sub:`2`)
     - m
     - Net-distance along the lateral coordinate *t* (side-mirror to side-mirror)
   * - *TimeToCollisionLongitudinal* resp. *TauLongitudinal*
     - **For RelativeNetDistanceLongitudinal ≤ 0:** 0; **For RelativeNetDistanceLongitudinal > 0:** –\ *RelativeNetDistanceLongitudinal* / *RelativeVelocityLongitudinal*
     - s
     - Negative values indicate an increase of *RelativeNetDistanceLongitudinal*, because the *RelativeVelocityLongitudinal* has to be positive in this case
   * - *TimeToCollisionLateral* resp. *TauLateral*
     - **For RelativeNetDistanceLateral ≤ 0:** 0; **For RelativeNetDistanceLateral > 0:** –\ *RelativeNetDistanceLateral* / *RelativeVelocityLateral*
     - s
     - Negative values indicate an increase of *RelativeNetDistanceLateral*, because the *RelativeVelocityLateral* has to be positive in this case
   * - *TauDotLongitudinal*
     - **For RelativeNetDistanceLongitudinal ≤ 0:** 0; **For RelativeNetDistanceLongitudinal > 0:** *RelativeNetDistanceLongitudinal* ∙ *RelativeAccelerationLongitudinal* / *RelativeVelocityLongitudinal*\ :sup:`2` – 1
     - –
     - Time derivative of *TauLongitudinal* (quotient rule)
   * - *TauDotLateral*
     - **For RelativeNetDistanceLateral ≤ 0:** 0; **For RelativeNetDistanceLateral > 0:** *RelativeNetDistanceLateral* ∙ *RelativeAccelerationLateral* / *RelativeVelocityLateral*\ :sup:`2` – 1
     - –
     - Time derivative of *TauLateral* (quotient rule)
   * - *TimeHeadway*
     - **For RelativeNetDistanceLongitudinal ≤ 0:** 0; **For RelativeNetDistanceLongitudinal > 0:** –\ *RelativeNetDistanceLongitudinal* / *LongitudinalVelocity*\ :sub:`2`
     - s
     - –

.. _image_DefinitionsForRegardingTwoAgents_:

.. figure:: _static/images/DefinitionsForRegardingTwoAgents.png
   :alt: Definitions for evaluations regarding the relative motion between two agents in the road system

   Definitions for evaluations regarding the relative motion between two agents in the road system


.. _scm_reference_guide_formulary_inertial_system:

State variables defined in the inertial system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following table shows state variables for regarding an agent in the inertial system. The state variables *Yaw* and *AbsoluteVelocity* are defined under :ref:`scm_reference_guide_formulary_vehicle_system`.

.. list-table::
   :header-rows: 1

   * - State variable
     - Calculation
     - SI-Unit
     - Annotation
   * - *x*
     - *x*\ :sub:`i` = *x*\ :sub:`i – 1` + cos(*Yaw*\ :sub:`i – 1`) ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*
     - m
     - Inertial *x*-position of an agent
   * - *y*
     - *y*\ :sub:`i` = *y*\ :sub:`i – 1` + sin(*Yaw*\ :sub:`i – 1`) ∙ *AbsoluteVelocity*\ :sub:`i` ∙ *dtime*
     - m
     - Inertial *y*-position of an agent

The following table shows state variables for regarding the road in the inertial system. The state variables *Heading* and *s* are defined under :ref:`scm_reference_guide_formulary_road_system`.

.. list-table::
   :header-rows: 1

   * - State variable
     - Calculation
     - SI-Unit
     - Annotation
   * - *x*
     - *x*\ :sub:`i` = *x*\ :sub:`i – 1` + cos(*Heading*\ :sub:`i – 1`) ∙ *ds*\ :sub:`i`
     - m
     - Inertial *x*-position of the road’s reference line (i.e. *t* = 0)
   * - *y*
     - *y*\ :sub:`i` = *y*\ :sub:`i – 1` + sin(*Heading*\ :sub:`i – 1`) ∙ *ds*\ :sub:`i`
     - m
     - Inertial *y*-position of the road’s reference line (i.e. *t* = 0)
