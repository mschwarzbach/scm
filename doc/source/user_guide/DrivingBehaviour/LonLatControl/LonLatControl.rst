..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _lon_lat_control_:

********************************
Longitudinal and Lateral Control
********************************

Driving behaviour in SCM is mainly characterised by lateral and longitudinal actions.
The following chapter provides an overview on how speed and following distances are adjusted, how the lane keeping action works and also explains the special case of behaviour in traffic jams and how emergency lanes are considered. 

Speed Adjustment
================

The set value of longitudinal acceleration is used in case of the action *speed adjustment* to regulate the target speed of the driver.
For all other actions, it is a limited value for the calculation of accelerations, so that the own target speed will never be overridden.
The calculation of the set value for acceleration :math:`a_{set}` is dependent on the target speed :math:`v_{target}` and the current speed of the agent :math:`v_{ego}`:

.. math::
  a_{set} = \frac{v_{target} - v_{ego}}{2}

The calculated values of :math:`a_{set}` have an upper limit given by the comfort longitudinal acceleration and a lower one by the comfort longitudinal deceleration (see :ref:`parameters_agent_`).


Following Distance
==================

The car following behaviour is divided into three different longitudinal actions:

* *approaching* a vehicle that can be followed
* *following* another vehicle at a desired distance
* *falling back* to the desired following distance when too close to the vehicle ahead

.. TODO check terms for "InfluencingDistance" and "EquilibriumDistance"

*Approaching* describes the behaviour between :ref:`InfluencingDistance and EquilibriumDistance<time_headways_>`.
The set value of acceleration during *approaching* :math:`a_{equilibrium}` is realized as a target braking to EquilibriumDistance.
:math:`a_{equilibrium}` is the sum of three parts:

.. math::
  a_{equilibrium} = a_{eqpartvelocity} + a_{eqpartdistance} + a_{eqpartacceleration}

:math:`a_{eqpartvelocity}` is defined as follows:

.. math::
  a_{eqpartvelocity} = a_{constvel} \cdot (propAppDist + 0.1)

*propAppDist* is defined as the propotional value for the *approaching* case.
It is calculated by the :math:`\Delta s` with respect to the reduction factor for following distance and the distance to regulate :math:`s_{regulation}`.
:math:`a_{constvel}` is a function of :math:`\Delta v` to leading vehicle and :math:`s_{regulation}`:

.. math::
  a_{constvel} = \frac{sin(\Delta v) \cdot \Delta v^2}{2 \cdot s_{regulation}}

.. TODO check terms for "reactionBaseTimeMean" and "pedalChangeTimeMean"

The :math:`\Delta v` for the *approaching* case is with respect to the situation always in consideration.
It is calculated with a gain of 25% of the :math:`v_{Front}`, where the tolerance area of the value ranges from 10 km/h to 25 km/h.
The :math:`s_{regulation}` is calculated with the parameters reactionBaseTimeMean and pedalChangeTimeMean (:ref:`driver_parameters_`):

.. math::
  s_{regulation} = v_{ego} \cdot (1 + reactionBaseTimeMean + pedalChangeTimeMean)

:math:`a_{eqpartdistance}` and :math:`a_{eqpartacc}` are defined as follows:

.. math::
  a_{eqpartvelocity} = a_{constdist} \cdot sin(\Delta s) \cdot \frac{\Delta s}{s_{regulation}}

.. math::
  a_{eqpartacc} = a_{constacc} \cdot propAppDist

In this case, the :math:`a_{constacc}` is equal to the acceleration of the observed vehicle.

.. TODO as above, check term "EquilibriumDistance"

The actions of *following* at and *falling back* to a desired distance describe the actual following behaviour of the vehicle.
Here, the following distance is regulated to approach the EquilibriumDistance.
For the calculation of the set value of accelaration :math:`a_{set}`, the Wiedemann model is used in both cases.
It is composed of three different acceleration values:

.. math::
  a_{set} = a_{distance} + a_{velocity} + a_{acceleration}

:math:`a_{distance}` serves as a function of the current relative distance :math:`\Delta s` to the leading vehicle, the EquilibriumDistance :math:`\Delta s_{queuing}` and the maximal amount of the acceleration :math:`a_{distance,max}` for the pure distance regulation:

.. math::
  a_{distance} = a_{distance,max} \cdot (\frac{\Delta s - \Delta s_{equilibrium}}{\Delta s_{equilibrium} - \Delta s_{queuing}})

:math:`a_{velocity}` is a function of the current relative velocity :math:`\Delta v` to the leading vehicle, the current relative distance :math:`\Delta s` to leading vehicle and the queuing distance :math:`\Delta s_{queuing}`:

.. math::
  a_{velocity} = - sign(\Delta v) \cdot 0.5 \cdot \Delta v^2 \cdot (\Delta s_{queuing} - \Delta s)

:math:`a_{accelaration}` is equal to the accelaration :math:`a_{leading}` of the leading vehicle:

.. math::
  a_{acceleration} = a_{leading}

The differentiation between *following* and *falling back* only exists in the value used in the calculations, the parameterization and the permitted value ranges for the calculated value :math:`a_{set}`:
(The behavioral parameters are explained in :ref:`driver_parameters_`.)

.. list-table::
  :header-rows: 1

  * -
    - *following* at a desired distance
    - *falling back* to the desired distance

  * - used values
    - :math:`a_{distance}`
    - :math:`a_{distance}`, :math:`a_{velocity}`, :math:`a_{accelaration}`

  * - value for :math:`a_{distance,max}`
    - comfortLongitudinalDeceleration
    - maximumLongitudinalDeceleration

  * - upper value for :math:`a_{set}`
    - comfortLongitudinalAcceleration
    - decelerationFromPowertrainDrag

  * - botttom value for :math:`a_{set}`
    - comfortLongitudinalDeceleration
    - maximumLongitudinalDeceleration


Time To Collision
=================

.. TODO: add some sentences on how TTC is implemented in SCM

The time to collision (TTC) is a time-based measure to track collision courses of two ore more objects. 
The TTC measure makes the modeling of human behaviour in SCM possible. 
Real-world data can be used to parametrise the simulation model.


Time To Brake
=============

Target braking
~~~~~~~~~~~~~~

The set value of longitudinal accelaration for a target braking is calculated in case of the longitudinal action of *braking to the end of the lane*.
The calculation of the set accelaration :math:`a_{set}` depends on the agent's current velocity :math:`v_{ego}` and the current relative distance :math:`\Delta s_{stop}` to the target point on the road:

.. math::
  a_{set} = - \frac{v_{ego}^2}{\Delta s_{stop}}

The calculated values of :math:`a_{set}` are limited to stay between the comfort longitudinal deceleration and the maximum longitudinal deceleration (see :ref:`driver_parameters_`).


Lane Centering
==============

.. _variables_lateral_guidance_:

Command variable calculations for lateral guidance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The command variables of the lateral control are the set course curvature :math:`\kappa_{set}`, the course angle error :math:`\Delta \Phi` and the lateral deviation :math:`\Delta w` with respect to the set trajectory.
The set course curvature :math:`\kappa_{set}` is constantly calculated from the current course curvature :math:`\kappa_{road}` and the planned course curvature :math:`\kappa_{maneuver}` from the current driving maneuver:

.. math::
  \kappa_{set} = \kappa_{road} + \kappa_{maneuver}

The controlling deviations :math:`\Delta \Phi` and :math:`\Delta w` are the respective difference between set and actual value:

.. math::
  \Delta \Phi = \Phi_{set} - \Phi_{act}

.. math::
  \Delta w = w_{set} - w_{act}

The actual values refer to the road coordinate system and are queried earlier.
The necessary values for the set values :math:`\kappa_{set}`, :math:`\Phi_{set}` and :math:`w_{set}` are calculated depending on the current lateral action.
Gains of the regulation :math:`P_{\Phi}` and :math:`P_{w}` are currently constant (:math:`P_{\Phi} = 7.5 rad/s, P_{w} = 20.0 rad/s^{2}`).


Lane keeping
~~~~~~~~~~~~

The lateral control for lane keeping is applied for the lateral actions *lane keeping*, *intent to change lanes* and *planned merging maneuver*.
The set values :math:`\kappa_{maneuver}`, :math:`\Phi_{set}` and :math:`w_{set}` mentioned earlier are constant at 0 due to the fact that the maneuver only follows the course of the road.
This has the following consequences for the output values of the lateral control in case of lane keeping:

.. math::
  \kappa_{set} = \kappa_{road}

.. math::
  \Delta \Phi = - \Phi_{act}

.. math::
  \Delta w = - w_{act}


.. _lateral_positioning_:

Lateral positioning
~~~~~~~~~~~~~~~~~~~

.. TODO update ref when parameters chapter is finished

The neutral lateral position, i.e. the lateral position the agent tries to reach with respect to the lane center, is determined individually for each agent.
It can be manipulated through the :ref:`lateralOffsetNeutralPosition-parameters<driver_parameters_lateral_positioning_>`.
Currently the offset from the lane center only applies to vehicles in queued traffic (below jam velocity).
Therefore, the actual lateral offset is not suddenly applied at jam velocity but ramps up to reach its full value at 20 km/h below jam velocity.

.. figure:: ../_static/images/LateralOffsetNeutralPositionScaled.png
  :alt: Lateral offset neutral position scaled

  Lateral offset neutral position scaled

Since this is the driver's desired lateral positioning, it is also considered for all other lateral maneuvers, such as lane changes, swerving etc.
The maneuver will end at that specific lateral offset.


Traffic Jam Behaviour
=====================

.. _emergency_lane:

Emergency lane
~~~~~~~~~~~~~~

.. TODO update ref when parameters chapter is finished

Depending on road type and country specific criteria, agents will form a rescue lane when traffic gets close to a standstill (below 10 km/h).
This applies to agents on the left most lane, where they move to the left side of the lane and its right neighbouring lane, where vehicles move to the right.
This behaviour, consisting of the amount of lateral displacement from the lane center and the compliance quota (cooperative behaviour) can be manipulated via the corresponding :ref:`parameters<driver_parameters_lateral_positioning_>`.
This mechanic supersedes the lateral positioning mentioned above and will lead to an extended final lateral position as seen below.

.. image_lateralOffsetScaled_EmergencyLane_:

.. figure:: ../_static/images/LateralOffsetNeutralPositionScaledEmergencyLane.png
  :alt: Lateral offset neutral position scaled with emergency lane

  Lateral offset neutral position scaled with emergency lane
  (vJam - jam velocity)

There is also a linear ramp up between 20 and 10 km/h to avoid big sudden changes for the lateral controller.

Note: The current implementation always considers the road to be a motorway and the country specific rules to require the formation of a rescue lane.

.. TODO add a section "Command variable calculations for longitudinal guidance" similar to the lateral one
