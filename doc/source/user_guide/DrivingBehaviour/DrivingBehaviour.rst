..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _driving_behaviour_:

*****************
Driving Behaviour
*****************

After choosing action patterns as described in :ref:`decision_making_`, SCM prepares the execution of those actions.
For this purpose, command variables in accordance with the driver's chosen action patterns are created for dynamics regulation.
The delivered action patterns are primarily in the form of a longitudinal action and a lateral action.
Furthermore, secondary driving tasks are selected appropriately for the action patterns (primary driving tasks).

The command variable of longitudinal guidance is the set value of longitudinal acceleration and it is calculated every time when a cycle of the *adjustment base time* expires (:ref:`algorithm_longitudinal_driver_`).

More on this is explained in the chapter :ref:`lon_lat_control_`.

  .. TODO check following sentence

The command variables of lateral guidance are set course curvature, course angle failure and lateral deviation regarding set trajectories.
Furthermore, the regulation gains for course angle and lateral deviation controller are delivered and the output variables are calculated continuously.
This is further explained in the chapter :ref:`variables_lateral_guidance_`.
The secondary driving tasks the driver currently executes include flasher and indicator settings.
More information on this can be found in the section :ref:`action_secondary_driver_tasks_`.

.. TODO create sub chapter Secondary driving tasks

Not only is the driver behaviour characterised by longitudinal and lateral control and different maneuvers, but there are also secondary driving tasks which may have an impact on the simulation output.
The processes of indicator setting and headlight flasher setting are explained in this section.

**Secondary driver tasks - Indicator setting**

Setting the indicators is based on a stochastic model.
The activation of an indicator will be checked in the event of one of the following lateral actions:

* a lane change
* an intent to change lane
* a planned merging maneuver

Each of these actions has an assigned direction representing the relative position of the lane the driver wants to change into.
This direction determines whether the left or right indicator has to be activated.
For critical actions, such as swerving, there is no activation of any indicators.
The model varies the probabilities and the latency of the indicator activation based on empirical data.
After a lateral action has been triggered, the duration of the action is measured continuously.

.. TODO: check sentence above - why should we measure the duration of what action?

A threshold P is drawn for the intensity of the indicator operation for this action (uniform distribution between 0 and 100%).
The model defines a minimal duration :math:`t_{min}` from which an activation of the indicator is possible and a maximum duration :math:`t_{max}` after which an activation of the indicator is not necessary to consider anymore.
These durations are assigned to intensities :math:`P_{min}` and :math:`P_{max}` which are comparable to the chance of activation.
If the threshold :math:`P` is higher than :math:`P_{max}`, there is no activation of the indicator for this action.
Otherwise, the intensity increase of :math:`P_{min}` and :math:`P_{max}` during the timings of :math:`t_{min}` and :math:`t_{max}` along the action duration will be evaluated by linear interpolation and the indicator will be activated as soon as the intensity overrides the threshold P.
The following parameter values are applied for this model (:numref:`image_Indicatorsetting_`):

.. _image_Indicatorsetting_:

.. figure:: _static/images/Indicatorsetting.png
  :alt: Indicator setting

  Indicator setting

.. math::
  t_{min} = 0

.. math::
  t_{max} = 2s

.. math::
  P_{min} = 15\%

.. math::
  P_{max} = 95\%


Knowing the information above, the following conclusions can be derived:
15% of the action initiations mentioned at the beginning will be accompanied by the activation of the appropriate indicator directly and the activation of the indicator takes place at maximum 2 seconds after the action initiation.
For 10%, there is no indicator activated during the execution of one of the actions mentioned at the beginning.


**Secondary driver tasks - Headlight flasher setting**

The activation of the headlight flasher uses the same modelling mechanism as the indicator setting.
Activation of the headlight flasher will be checked in the following situations:

* There is a high risk lane changer.
* A high risk lane changer is anticipated.

The following parameter values for this model are applied:

.. math::
  t_{min} = 0.5s

.. math::
  t_{max} = 1.5s

.. math::
  P_{min} = 10\%

.. math::
  P_{max} = 70\%

Based on the information above, the following conclusions can be drawn:
The headlight flasher is activated earliest 0.5 second and latest 1.5 seconds after detection of one of the situations mentioned at the beginning.
For 30%, no headlight flasher is activated after detecting one of the mentioned situations.


.. toctree::
  :glob:
  :hidden:

  */*
