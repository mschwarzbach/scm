################################################################################
# Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
import datetime

# -- Path setup --------------------------------------------------------------
sys.path.append(os.path.abspath("_ext"))

# -- Project information -----------------------------------------------------
project = 'SCM (BMW)'
copyright = f'{datetime.datetime.now().year} BMW AG'
author = 'BMW'

# -- Version is generated via cmake
version_file = 'version.txt'
if os.path.exists(version_file):
    with open(version_file) as vf:
        version = vf.read().strip()
        release = version

# -- General configuration ---------------------------------------------------
def setup(app):
   app.add_css_file('css/custom.css')

extensions = []
extensions.append("sphinx_rtd_theme")
extensions.append('sphinx.ext.todo')
extensions.append('sphinx_tabs.tabs')
extensions.append('sphinx.ext.imgmath')
extensions.append('api_doc')
extensions.append('myst_parser')

templates_path = ['_templates']

exclude_patterns = ['README.md']

todo_include_todos = True

pdf_stylesheets = ['sphinx', 'kerning', 'a4']
pdf_style_path = ['.', '_styles']
pdf_fit_mode = "shrink" # literal blocks wider than frame
pdf_language = "en_US"
pdf_page_template = 'cutePage'

numfig = True

# -- Options for HTML output -------------------------------------------------

html_static_path = ['_static']
html_theme = 'sphinx_rtd_theme'
html_title = 'SCM Documentation'
html_short_title = 'SCM|Doc'
html_favicon = '_static/openPASS.ico'
html_logo = '_static/openPASS.png'

# -- Options for API DOC -----------------------------------------------------
api_doc_title = "Source Code Documentation"
api_doc_root = "../../.."

# -- Define global replacements ----------------------------------------------
# See https://documentation.help/Sphinx/config.html
rst_epilog = """

.. |op| replace:: **openPASS**
.. |Op| replace:: **OpenPASS**
.. |opwg| replace:: **openPASS** Working Group
.. |op_oss| replace:: **openPASS** (Open Source)
.. |Op_oss| replace:: **OpenPASS** (Open Source)
.. |mingw_shell| replace:: ``MinGW 64-bit`` shell

.. |op_bmw| replace:: **openPASS** (BMW)
.. |Op_bmw| replace:: **OpenPASS** (BMW)

"""
