..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _prepare_scm_build_:


Prerequisites
=============

Download the project::

    git clone https://gitlab.eclipse.org/eclipse/scm/scm.git

Most needed dependencies are handled with ``Conan``.
To install Conan, follow the official guide https://docs.conan.io/2/installation.html.


If you don't already have a conan profile, you can let Conan detect and create a suitable default profile for your current environment with::

    conan profile detect

or use one of the provided profiles in `utils/conan`.

The easiest way to get started is to call a script::

    cd utils/ci/scripts && ./15_prepare-thirdParty.sh

This script checks out needed third party dependencies and if they're not already present in local cache or available in the conancenter it builds them one after the other.


Additionally, download ``boost`` and all packages needed for your operating system to build C++ programs.

.. tabs::

    .. tab:: Linux

        .. code-block::

            sudo apt install libboost-all-dev
            sudo apt install build-essential
            sudo apt install ccache
            sudo apt install cmake


    .. tab:: Windows

        For Windows, MSYS2 is recommended as build environment. You may need to install some additional packages in order to compile SCM properly.

        .. code-block::

            pacman -S mingw-w64-x86_64-boost
            pacman -S mingw-w64-x86_64-gcc
            pacman -S mingw-w64-x86_64-toolchain
            pacman -S base-devel
            pacman -S mingw-w64-x86_64-ccache
            pacman -S mingw-w64-x86_64-cmake
            pacman -S mingw-w64-x86_64-doxygen
            pacman -S mingw-w64-x86_64-gdb
            pacman -S mingw-w64-x86_64-graphviz
            pacman -S mingw-w64-x86_64-gtest
            pacman -S mingw-w64-x86_64-make
