..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _scm_developer_guide_:

###################
SCM Developer Guide
###################

Visual Perception
^^^^^^^^^^^^^^^^^

The *top-down* process in SCM is illustrated in :numref:`image_InformationRequestMechanism_`.

.. _image_InformationRequestMechanism_:

.. figure:: _static/images/InformationRequestMechanism.png
   :alt: Top-down information request mechanism

   Top-down information request mechanism



InformationAcquisition - own vehicle information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PerceiveEgoData is called by SCM in every time step, regardless of any perception states of the driver's sensory system.
It transfers the following information about the driver's own vehicle to the submodule Mental Model, more specifically old ref: microscopic_characteristics :

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
   * - AbsoluteVelocity
     - Absolute velocity of the vehicle in the road coordinate system
   * - LongitudinalAcceleration
     - Longitudinal acceleration of the vehicle in the road coordinate system
   * - LongitudinalVelocity
     - Longitudina velocity of the vehicle in the road coordinate system
   * - LongitudinalPosition
     - Longitudina position of the vehicle in the road coordinate system (current track coordinate)
   * - LateralVelocity
     - Lateral velocity of the vehicle in the road coordinate system
   * - LateralPosition
     - Lateral position of the vehicle in the road coordinate system
   * - HeadingAngle
     - Heading angle of the vehicle to the road coordinate system
   * - isCrossingLanes
     - Boolean indicating, if the driver's own vehicle is currently attached two more than one lane (e.g. in case of a lane change)
   * - Collision
     - Boolean indicating, if the driver's own vehicle has collided with another object
   * - laneId
     - ID of the driver's current (main) lane
   * - LeftLaneBoundaryDepartureEgo
     - Minimum lateral distance of the vehicle to its current lane's left marking
   * - RightLaneBoundaryDepartureEgo
     - Minimum lateral distance of the vehicle to its current lane's left marking


InformationAcquisition - surrounding vehicle information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This information is stored/update in MentalModel for every AreaOfInterest in which an object is observed.
With exception to the AreaOfInterest::LEFT_SIDE / RIGHT_SIDE / LEFTLEFT_SIDE and RIGHTRIGHT_SIDE only one object, the one closest to the ego vehicle, is saved to the driver's mental model. 
For these mentioned side areas, all vehicles are stored in a vector.

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
   * - acceleration
     - Absolute acceleration of the object
   * - accelerationDelta
     - Relative longitudinal acceleration of the surrounding object and the driver's own vehicle in the road coordinate system
   * - brakeLightsActive
     - A Boolean indicating, if the surrounding object has activated brake lights or not
   * - collision
     - A Boolean indicating, if the surrounding object was involved in a collision
   * - distanceToLaneBoundaryLeft
     - Minimum lateral distance of the surrounding object to its current lane's left marking
   * - distanceToLaneBoundaryRight
     - Minimum lateral distance of the surrounding object to its current lane's right marking
   * - exist
     - A Boolean indicating, if the surrounding object exists or not
   * - gap
     - Time headway between the surrounding object and the driver's own vehicle in the road coordinate system
   * - gapDot
     - Time derivative of the time headway between the surrounding object and the driver's own vehicle in the road coordinate system
   * - heading
     - Heading angle of the surrounding object to the road coordinate system
   * - height
     - Maximum height of the surrounding object
   * - id
     - ID of the surrounding object
   * - indicatorState
     - IndicatorState of the surrounding object
   * - isCrossingLanes
     - A Boolean indicating, if the surrounding object is currently crossing lanes
   * - isStatic
     - A Boolean indicating, if the surrounding object is a static object or a moving agent
   * - lateralDistanceDelta
     - Relative lateral distance of coordinate reference points between the surrounding object and the driver's own vehicle in the road coordinate system
   * - lateralPositionInLane
     - Lateral position of the surrounding object in the road coordinate system
   * - lateralVelocity
     - Lateral velocity of the surrounding object in the road coordinate system
   * - lateralVelocityDelta
     - Relative lateral velocity between the surrounding object and the driver's own vehicle in the road coordinate system
   * - length
     - Maximum length of the surrounding object
   * - longitudinalObstruction
     - Obstruction between the object and ego
   * - longitudinalVelocity
     - Longitudinal velocity of the surrounding object in the road coordinate system
   * - mainLaneId
     - Main lane ID of the surrounding object
   * - relativeLateralDistance
     - Relative lateral net distance between the surrounding object and the driver's own vehicle in the road coordinate system
   * - relativeLongitudinalDistance
     - Relative longitudinal net-distance of the surrounding object to the driver's own vehicle in the road coordinate system
   * - tauDot
     - Time derivative of the time to collision between the surrounding object and the driver's own vehicle in the road coordinate system
   * - ttc
     - Time to collision between the surrounding object and the driver's own vehicle in the road coordinate system
   * - velocity
     - Absolute velocity of the surrounding object in the road coordinate system
   * - velocityDelta
     - Relative longitudinal velocity between the surrounding object and the driver's own vehicle in the road coordinate system
   * - width
     - Maximum width of the surrounding object

.. figure:: _static/images/CallingSequenceSurroundingOpticalData.png
   :alt: Calling sequence for the visual perception of microscopic information from an AreaOfInterest
   
   Calling sequence for the visual perception of microscopic information from an AreaOfInterest


For AOIs in which no vehicle is observed, the values of the struct are reset to the following:

.. list-table::
   :header-rows: 1

   * - Variable
     - Dummy value for empty AreaOfInterest
   * - acceleration
     - 0.0
   * - accelerationDelta
     - 0.0
   * - brakeLightsActive
     - false
   * - collision
     - false
   * - distanceToLaneBoundaryLeft
     - 0.5
   * - distanceToLaneBoundaryRight
     - 0.5
   * - exist
     - false
   * - gap
     - PreviewDistance of the driver divided by the longitudinal velocity of the driver's own vehicle (the denominator is bound to 1.0 below)
   * - heading
     - 0.0
   * - height
     - 0.0
   * - id
     - -1
   * - indicatorState
     - IndicatorState_Off
   * - isCrossingLanes
     - false
   * - isStatic
     - false
   * - lateralDistanceDelta
     - PreviewDistance of the driver
   * - lateralPositionInLane
     - 0.0
   * - lateralVelocity
     - 0.0
   * - lateralVelocityDelta
     - 0.0
   * - length
     - 0.0
   * - longitudinalObstruction
     - NoOpponent
   * - longitudinalVelocity
     - 0.0
   * - mainLaneId
     - 0
   * - relativeLateralDistance
     - PreviewDistance of the driver
   * - relativeLongitudinalDistance
     - PreviewDistance of the driver
   * - tauDot
     - 0.0
   * - ttc
     - 99.0
   * - velocity
     - 0.0
   * - velocityDelta
     - 0.0
   * - width
     - 0.0

The following OpticalData information is corrected by the function
AssignAoiSpecificDataForObject, if the specified AreaOfInterest is one of the
SIDE areas:

.. list-table::
   :header-rows: 1

   * - Variable
     - Correction for AreaOfInterest LEFT_SIDE or RIGHT_SIDE
   * - DeltaLongitudinalAcceleration
     - 0.0
   * - DeltaLongitudinalVelocity
     - 0.0
   * - RelativeNetDistance
     - 0.0
   * - DeltaLateralVelocity
     - 0.0
   * - Gap
     - 0.0
   * - GapDot
     - 0.0
   * - Ttc
     - 99.0 s
   * - TauDot
     - 0.0   


InformationAcquisition - information about infrastructure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The information from struct :ref:`TrafficRuleInformationSCM<sensor_driver_traffic_rule_information_>`
consist of the information shown below (see :ref:`InfrastructureInformation<sensor_driver_infrastructure_information_>`
in :ref:`sensor_driver_`).

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
     
   * - | laneLeftLeft 
       | - trafficSigns 
       | - laneMarkingsLeft 
       | - laneMarkingsRight
       
     - | Data about the lane two lanes left of the mainLane (in driving direction) 
       | - All traffic signs on this lane inside the visibility distance 
       | - Lane markings on the left side of the lane 
       | - Lane markings on the right side of the lane
     
   * - | laneLeft 
       | - trafficSigns 
       | - laneMarkingsLeft 
       | - laneMarkingsRight
       
     - | Data about the lane to the left of the mainLane (in driving direction) 
       | - All traffic signs on this lane inside the visibility distance 
       | - Lane markings on the left side of the lane 
       | - Lane markings on the right side of the lane
     
   * - | laneEgo 
       | - trafficSigns 
       | - laneMarkingsLeft 
       | - laneMarkingsRight
       
     - | Data about the lane where the middle of the front of the agent is (e.g. mainLane) 
       | - All traffic signs on this lane inside the visibility distance 
       | - Lane markings on the left side of the lane 
       | - Lane markings on the right side of the lane
     
   * - | laneRight 
       | - trafficSigns 
       | - laneMarkingsLeft 
       | - laneMarkingsRight
       
     - | Data about the lane to the right of the mainLane (in driving direction) 
       | - All traffic signs on this lane inside the visibility distance 
       | - Lane markings on the left side of the lane 
       | - Lane markings on the right side of the lane
     
   * - | laneRightRight 
       | - trafficSigns 
       | - laneMarkingsLeft 
       | - laneMarkingsRight
       
     - | Data about the lane two lanes right of the mainLane (in driving direction) 
       | - All traffic signs on this lane inside the visibility distance 
       | - Lane markings on the left side of the lane 
       | - Lane markings on the right side of the lane


The information of struct :ref:`GeometryInformationSCM<sensor_driver_geometry_information_>` is composed as follows 
(see :ref:`InfrastructureInformation<sensor_driver_infrastructure_information_>`) in :ref:`sensor_driver_`

.. list-table::
   :header-rows: 1

   * - Variable
     - Meaning
   * - visibilityDistance
     - Current maximum visibility distance as specified by the world
   * - numberOfLanes
     - Number of lane at the current position
   * - distanceToEndOfNextExit
     - Distance from current position to the end of the next (current or coming) exit lane
   * - distanceToEndOfNextEntry
     - Distance from current position to the end of the next (current or coming) entry lane
   * - | laneLeftLeft 
       | - exist 
       | - curvature 
       | - width 
       | - distanceToEndOfLane 
       | - curvatureInPreviewDistance 
       | - curvatureNextPosition
       
     - | Data about the lane two lanes left of the mainLane (in driving direction) 
       | - Wether there is a lane on this position 
       | - Curvature at current s position 
       | - Width at current s position 
       | - Distance from current position to the end of the lane 
       | - Curvature at previewDistance 
       | - Curvature at position the agent will be in the next time step (it constant velocity)
     
   * - | laneLeft 
       | - exist 
       | - curvature 
       | - width 
       | - distanceToEndOfLane 
       | - curvatureInPreviewDistance 
       | - curvatureNextPosition
       
     - | Data about the lane to the left of the mainLane (in driving direction) 
       | - Wether there is a lane on this position 
       | - Curvature at current s position 
       | - Width at current s position 
       | - Distance from current position to the end of the lane 
       | - Curvature at previewDistance 
       | - Curvature at position the agent will be in the next time step (it constant velocity)
       
   * - | laneEgo 
       | - exist 
       | - curvature 
       | - width 
       | - distanceToEndOfLane 
       | - curvatureInPreviewDistance 
       | - curvatureNextPosition
       
     - | Data about the lane where the middle of the front of the agent is (e.g. mainLane) 
       | - Wether there is a lane on this position 
       | - Curvature at current s position 
       | - Width at current s position 
       | - Distance from current position to the end of the lane 
       | - Curvature at previewDistance 
       | - Curvature at position the agent will be in the next time step (it constant velocity)
       
   * - | laneRight 
       | - exist 
       | - curvature 
       | - width 
       | - distanceToEndOfLane 
       | - curvatureInPreviewDistance 
       | - curvatureNextPosition
       
     - | Data about the lane to the right of the mainLane (in driving direction) 
       | - Wether there is a lane on this position 
       | - Curvature at current s position 
       | - Width at current s position 
       | - Distance from current position to the end of the lane 
       | - Curvature at previewDistance 
       | - Curvature at position the agent will be in the next time step (it constant velocity)
       
   * - | laneRightRight 
       | - exist 
       | - curvature 
       | - width 
       | - distanceToEndOfLane 
       | - curvatureInPreviewDistance 
       | - curvatureNextPosition
       
     - | Data about the lane two lanes right of the mainLane (in driving direction) 
       | - Wether there is a lane on this position 
       | - Curvature at current s position 
       | - Width at current s position 
       | - Distance from current position to the end of the lane 
       | - Curvature at previewDistance 
       | - Curvature at position the agent will be in the next time step (it constant velocity)


Features about lane safety and lane change safety
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Features about lane safety**

.. list-table::
   :header-rows: 1

   * - Feature
     - Meaning
   * - IsLaneSafe
     - A lane is safe to change to
   * - IsSideLaneSafe
     - A lane is safe to change into from a normal situation
   * - IsCurrentLaneSafe
     - The distance to the front and rear vehicle exceedes the minimum time headway


**Features about lane change safety**

.. list-table::
   :header-rows: 1

   * - Feature
     - Meaning
   * - IsLaneChangeSafe
     - The  current lane change is still save to perform
   * - IsLaneSafeBeingQueued
     - A lane is safe to change to within traffic jams



.. _scmDependenciesInterface_:

ScmDependenciesInterface
------------------------

Provides methods to update dependencies for SCM and grants access to them.

.. _scmDependencies_:

ScmDependencies
~~~~~~~~~~~~~~~

* DriverParameters
* VehicleModelParameters
* StochasticsInterface
* SpawnVelocity
* CycleTime
* OwnVehicleInformationSCM
* SurroundingObjectsSCM
* MesoscopicInformationSCM
* TrafficRuleInformationSCM
* GeometryInformationSCM


.. _scmComponents_:

ScmComponents
-------------

ScmComponents is a container for components or modules SCM consists of (MentalModel, SituationManager, etc..) and is reponsible to provide necessary dependencies for these. For example, the MentalModel needs all ScmDependencies but the GazeFollower only needs OwnVehicleInformationScm.
