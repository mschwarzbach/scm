..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _building_scm_:


Building SCM
============

SCM is a simple CMake Project and requires a minimum version of 3.14.

From within your root directory create a build directory and change into it::

    mkdir build && cd build

Call cmake and provide the dependencies via the ``CMAKE_PREFIX_PATH``

.. tabs::


   .. tab:: Linux

      .. code-block::

         cmake ..
         -G "Unix Makefiles"
         -DUSE_CCACHE=ON
         -DCMAKE_INSTALL_PREFIX="/home/$USER/scm-install"
         -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface; \
                              deps/conan/direct_deploy/protobuf; \
                              deps/conan/direct_deploy/gtest; \
                              deps/conan/direct_deploy/stochastics; \
                              deps/conan/direct_deploy/units; \
                              deps/conan/direct_deploy/libiconv; \
                              deps/conan/direct_deploy/libxml2; \
                              deps/conan/direct_deploy/osiquerylibrary"

   .. tab:: Windows

      Make sure you have a MSYS installation with at least gtest and boost installed.

      .. code-block::

         cmake ..
         -G "MSYS Makefiles"
         -DUSE_CCACHE=ON
         -DCMAKE_INSTALL_PREFIX="C:/scm-install"
         -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface; \
                              deps/conan/direct_deploy/protobuf; \
                              deps/conan/direct_deploy/gtest; \
                              deps/conan/direct_deploy/stochastics; \
                              deps/conan/direct_deploy/units; \
                              deps/conan/direct_deploy/libiconv; \
                              deps/conan/direct_deploy/libxml2; \
                              deps/conan/direct_deploy/osiquerylibrary"


or as a one-liner for easy copy & paste

.. tabs::

   .. tab:: Linux

      .. code-block::

         cmake .. -G "Unix Makefiles" -DUSE_CCACHE=ON -DCMAKE_INSTALL_PREFIX="/home/$USER/scm-install" -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface;deps/conan/direct_deploy/protobuf;deps/conan/direct_deploy/gtest;deps/conan/direct_deploy/stochastics;deps/conan/direct_deploy/units;deps/conan/direct_deploy/libiconv;deps/conan/direct_deploy/libxml2;deps/conan/direct_deploy/osiquerylibrary"

   .. tab:: Windows

      .. code-block::

         cmake .. -G "MSYS Makefiles" -DUSE_CCACHE=ON -DCMAKE_INSTALL_PREFIX="C:/scm-install" -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface;deps/conan/direct_deploy/protobuf;deps/conan/direct_deploy/gtest;deps/conan/direct_deploy/stochastics;deps/conan/direct_deploy/units;deps/conan/direct_deploy/libiconv;deps/conan/direct_deploy/libxml2;deps/conan/direct_deploy/osiquerylibrary"

.. note::

    If you don't have sphinx installed or simply don't want to build the documentation, add the following options to your cmake call::

    -DWITH_DOC=OFF -DWITH_API_DOC=OFF

    Otherwise you may need to install some additional modules::

        pip3 install sphinx sphinx-rtd-theme sphinx-tabs breathe exhale sphinxcontrib-spelling myst-parser

    Contrary, if you just want to build the documentation and not the whole project, call cmake with the following arguments::

        -DWITH_DOC=ON -DONLY_DOC=ON

Then simply call::

    make install -j4

to build SCM and install it to the directory provided by the given ``CMAKE_INSTALL_PREFIX``. Adjust this installation directory to your needs.

The build generates the main output file **AlgorithmScm.fmu**.



Use SCM with OpenPASS
---------------------

Make yourself familiar with how OpenPASS works in general. It comes with a few config files, see their default config set for a standard simulation.

.. note::

    Get and install OpenPASS following their instructions from
    https://gitlab.eclipse.org/eclipse/openpass/opSimulation

As SCM is built as a Functional Mockup Unit (FMU) it requires some tweaks to the default OpenPASS configs to work properly. An example set is included in this repository and can be found in ``<root>/e2eTests/Resources/Common``.
The following subsections highlight some differences to the default set.

ProfilesCatalog
~~~~~~~~~~~~~~~

In the ProfilesCatalog, add a new ProfileGroup ``FMU1``, if it isn't there already and add a new ``Profile`` called ``RegularScm``.
The profile is the place to adjust all driver parameter SCM supports - here they can be adjusted to your needs (see examples for a whole example file).

Additionally to these parameters specific to the driver model behavior, there are some special parameters needed.
They are prefixed with ``Parameter_AssignSpecial_`` and are followed by ``Simulation`` or ``Vehicle`` respectively.

The ``simulation parameters`` are useful for the overall simulation, the ``vehicle parameters`` are necessary so that SCMs internal dynamics module can work properly.


.. list-table:: Additional ``Simulation``-prefixed parameter expected by SCM

    * - **Parameter**
      - **Description**
    * - RandomSeed
      - As SCM is a stochastic model, it is advisable to provide a random seed for the simulation.
    * - OutputPath
      - If configured, SCM produces some output files regarding its internal state / mental model. This path should be configured so that each agent has an individual place to write data.
    * - ConfigPath
      - Path to configs (e.g. ScmConfig.xml or GazeFollowerTimeSeries.xml)



.. list-table:: Additional ``Vehicle``-prefixed parameter expected by SCM

    * - **Parameter**
      - **Description**
    * - SteeringRatio
      - The steering ratio of the vehicle
    * - MaxSteering
      - The maximum possible steering wheel angle in both directions in radian
    * - NumberOfGears
      - The number of gears the vehicle has
    * - GearRatio<X>
      - Ratio for each gear. The number <X> of GearRatio entries should match the ``NumberOfGears`` from above. If a vehicle has 8 gears, there should be GearRatio1, GearRatio2, ..., GearRatio8.

Older ProfilesCatalog
---------------------
You may find older versions of the ProfilesCatalog from a time before SCM was build as a FMU. For theses configs, there exists a converter script in the ``OpenPASS project`` to rewrite the ProfilesCatalog to the needed format.
The conversion is done via XSL transformation which can be found in ``utils/rules/v2.0.8_1_to_v2.0.9_SCM_FMU``. Although this is a useful tool, it may not cover every possible edge case. Thus it might be necessary to manually check the converted config file afterwards.
One known limitation is that there is only one supported motorbike model ``BMW R 1200 GS`` which can be related to the ``RegularMotorbikeSCM profile`` and bus and truck models have to have these words actually written in their profile names.

ScmConfig
~~~~~~~~~

SCM comes with a config file including different options to log specific simulation data of the agents to files.

.. list-table:: ScmConfig Parameters

    * - **Parameter / LoggingDetails**
      - **Description**
    * - DriverParameter
      - If set to true (default) writes SCM-related driver parameter (e.g. comfortLateralAcceleration, outerKeepingIntensity, ...) to the *DriverSimulationOutput.xml* file.
    * - DriverValues
      - If set to true (default), writes SCM-related cyclic data to a file. This includes information about the ego vehicle as well as information about its surroundings (**Perception.csv**).
    * - XmlOutput
      - If set to true (default false), writes the DriverValues into a XML file instead of csv.


Running a simulation
--------------------

First of all, the simulator ``OpenPASS`` needs to be downloaded or manually build (see official OpenPASS documentation). It is recommended though to rely on an official release version of OpenPASS.

``OpenPASS`` needs a specific set of config files in a directory called ``configs`` right besides the executable. The provided default configurations should be copied to this directory.

The artifact of SCM (``AlgorithmScm.fmu``) must be made available for ``OpenPASS``. It is advisable to create a new directory which will match the resource path referenced in the ProfilesCatalog (in this case ``configs/resources/``).
However, this is not mandatory as long as the path chosen will match the actual position of ``AlgorithmScm.fmu``.

.. note::

    Of course it is possible to set the ``CMAKE_PREFIX_PATH`` for the SCM build to the ``resource`` directory so that the SCM FMU will be put in the correct place automatically.
