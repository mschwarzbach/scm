# SCM

We are very thrilled and proud to announce the open source release of the Stochastic Cognitive Model (SCM)!
The SCM represents a significant step in simulating driver behavior and creating realistic traffic simulation environments. By understanding human driving, this model allows us to assess traffic safety even better via simulations. Making SCM available to a broad community will certainly catalyze further research, innovation, and collaboration!

## What is SCM

The Stochastic Cognitive Model (SCM) is a driver behaviour model developed by BMW, which is designed for statistical analyses regarding vehicle and traffic safety by means of stochastic traffic flow simulations. The purpose of this guide is to provide a broad and fast overview on the model’s structures and functionalities and also show the scientific background, which is described by the model. 

SCM focuses on detailed driver behaviour modelling to provide an accurate simulation of the occurrence of critical traffic situations and road accidents, which may result from those critical situations. The model parameters of SCM are mostly stochastic variables to take into account the inter- and intra-individual differences among drivers. The algorithms themselves are characterised by cause-action mechanisms, which are strongly influenced by probabilities, derived from the currently perceived traffic situation and the agent’s driver- and vehicle-specific parameterisation.

Further information about the model is available here: https://www.bmwgroup.com/scm-driver/en.html


## How can SCM be used

SCM is designed as a standalone simulation model, which can be individually integrated into any Simulator. To allow for modular integration, SCM uses the [OSI standard](https://github.com/OpenSimulationInterface/open-simulation-interface) for communication and can be wrapped as an [FMU](https://fmi-standard.org/). Thus, it follows the [OSMP convention](https://opensimulationinterface.github.io/osi-documentation/#_osi_sensor_model_packaging). 

SCM models the behavior of a human driver. As such it controls a vehicle in simulated traffic and interacts with surrounding traffic participants. The behavior can be influenced through a number of behavioral parameters. Further, the model makes strong usage of stochastics without loosing its determinism. 

## Highlights of the first release:
 
- Standalone simulation model that is wrapped as an FMU
- Developed and tested within the openPASS simulator. But due to its modular integration it should be possible to integrate SCM into any simulator following the OSMP convention (communication via OSI messages)
- Explicit modeling of the driver cognitive processes for a full traceability of the decision chain
- Stochastic simulation of different driver types through detailed parametrization, e.g. reaction times, comfort accelerations, traffic rules compliance, etc.
- Simulation of a realistic gaze behavior 
- Possibility to simulate driver distraction
- Simulation of a wide range of highway traffic scenarios, e.g. highway entry/exit, merging, swerving, end of lane, etc.
- Visualization of the mentally perceived agents from a driver perspective