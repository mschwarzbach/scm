/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file PublisherInterface.h

#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

#include "DataBufferInterface.h"

namespace scm::publisher
{
//! @brief Interface which has to be provided by observation modules
class PublisherInterface
{
public:
  virtual ~PublisherInterface() = default;

  //! @brief Writes information into a data buffer backend
  //! @param key     Unique topic identification
  //! @param value   Value to be written
  virtual void Publish(const scm::databuffer::Key& key, const scm::databuffer::Value& value) = 0;
};

}  // namespace scm::publisher