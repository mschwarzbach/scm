/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file DataBufferInterface.h
#pragma once

#include <map>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

#include "include/common/CommonHelper.h"
#include "include/common/ScmRuntimeInformation.h"

namespace scm::type
{
template <typename T, typename Tag>
//! @brief Struct definition for the description of a NamedType
struct NamedType
{
  //! @brief The value
  T value;

  //! @brief Constructor initializes and creates NamedType.
  //! @param value
  constexpr NamedType(T value) noexcept
      : value(value)
  {
  }

  //! @brief Operator() overload
  //! @return value
  constexpr operator T() const
  {
    return value;
  }

  //! @brief Operator== overload to check is value equal
  //! @param other
  //! @return true when is equal, false when not
  constexpr bool operator==(NamedType<T, Tag> const& other) const noexcept
  {
    return this->value == other.value;
  }

  //! @brief Operator!= overload to check isn't value equal
  //! @param other
  //! @return true when isn't equal, false when equal
  constexpr bool operator!=(NamedType<T, Tag> const& other) const
  {
    return this->value != other.value;
  }

  //! @brief Operator== overload to check is value equal
  //! @param other
  //! @return true when is equal, false when not
  constexpr bool operator==(T const& other) const
  {
    return this->value == other;
  }

  //! @brief Operator!= overload to check isn't value equal
  //! @param other
  //! @return true when isn't equal, false when equal
  constexpr bool operator!=(T const& other) const
  {
    return this->value != other;
  }
};

//! @brief AgentId for type Integer
using AgentId = int;
//! @brief EntityId for struct NamedType with AgentId and EntityIdType
using EntityId = NamedType<AgentId, struct EntityIdType>;
//! @brief Timestamp for struct NamedType with integer and struct TimestampType
using Timestamp = NamedType<int, struct TimestampType>;
//! @brief EntityIds vector of EntityIds
using EntityIds = std::vector<EntityId>;

//! @brief Contains the triggering entities of an acyclic
struct TriggeringEntities
{
  //! @brief The entities
  type::EntityIds entities;
};

//! @brief Contains the affected entities of an acyclic
struct AffectedEntities
{
  //! @brief The entities
  type::EntityIds entities;
};

//! @brief std::string
using FlatParameterKey = std::string;
//! @brief std::variant
using FlatParameterValue = std::variant<
    bool,
    std::vector<bool>,
    char,
    std::vector<char>,
    int,
    std::vector<int>,
    size_t,
    std::vector<size_t>,
    float,
    std::vector<float>,
    double,
    std::vector<double>,
    std::string,
    std::vector<std::string>>;

//! @brief Map with flatParameterKey and flatParametervalue
using FlatParameter = std::map<FlatParameterKey, FlatParameterValue>;

}  // namespace scm::type

namespace scm::databuffer
{

//! @brief scm::type::FlatParameterKey
using Key = scm::type::FlatParameterKey;
//! @brief scm::type::FlatParameterValue
using Value = scm::type::FlatParameterValue;
//! @brief scm::type::FlatParameter
using Parameter = scm::type::FlatParameter;
//! @brief std::vector<Key>
using Tokens = std::vector<Key>;

//! @brief Wildcard to match any token inside a key string.Length of 1 is mandatory.static
static const std::string WILDCARD = "*";
//! @brief Separator for hierarchical key strings. Length of 1 is mandatory.
constexpr char SEPARATOR = '/';

//! @brief Representation of an component event
class ComponentEvent
{
public:
  //! @brief specialized constructor ComponentEvent
  //! @param  parameter scm::type::FlatParameter
  ComponentEvent(scm::type::FlatParameter parameter)
      : parameter{std::move(parameter)}
  {
  }

public:
  //! @brief Generic parameter set associated with this event
  scm::type::FlatParameter parameter;
};

//! @brief Represents an acyclic occurence, like an event
class ScmAcyclic
{
public:
  //! @brief default constructor for ScmAcyclic
  ScmAcyclic() = default;

  //! @brief constuctor with parameter for ScmAcyclic
  //! @param name
  //! @param triggeringEntities TriggeringEntities
  //! @param affectedEntities AffectedEntities
  //! @param parameter Parameter
  ScmAcyclic(std::string name, scm::type::TriggeringEntities triggeringEntities, scm::type::AffectedEntities affectedEntities, Parameter parameter)
      : name{std::move(name)},
        triggeringEntities{std::move(triggeringEntities)},
        affectedEntities{std::move(affectedEntities)},
        parameter{std::move(parameter)}
  {
  }

  //! @brief constuctor with parameter for ScmAcyclic
  //! @param name
  //! @param entity scm::type::EntityId
  //! @param parameter scm::type::FlatParameter
  ScmAcyclic(std::string name, scm::type::EntityId entity, scm::type::FlatParameter parameter)
      : name{std::move(name)},
        parameter{std::move(parameter)}
  {
    triggeringEntities.entities.push_back(entity);
  }

  //! @brief Operator== overload to check is value equal
  //! @param other
  //! @return true when is equal, false when not
  bool operator==(const ScmAcyclic& other) const
  {
    return name == other.name &&
           triggeringEntities.entities == other.triggeringEntities.entities &&
           affectedEntities.entities == other.affectedEntities.entities &&
           parameter == other.parameter;
  }

  //! @brief Name (or identifier) of this occurence
  std::string name;
  //! @brief List of entities causing this occurence
  scm::type::TriggeringEntities triggeringEntities;
  //! @brief List of entities affected by this occurence
  scm::type::AffectedEntities affectedEntities;
  //! @brief Generic parameter set associated with this occurence
  Parameter parameter;
};

//! @brief Representation of an entry in the acyclics
struct ScmAcyclicRow
{
  //! @brief specialized constructor
  //! @param id scm::type::EntityId
  //! @param k Key
  //! @param data ScmAcyclic
  ScmAcyclicRow(scm::type::EntityId id, Key k, ScmAcyclic data)
      : entityId{id},
        key{k},
        data{data}
  {
  }

  //! @brief Operator== overload to check is value equal
  //! @param other
  //! @return true when is equal, false when not
  bool operator==(const ScmAcyclicRow& other) const
  {
    return entityId == other.entityId &&
           key == other.key &&
           data == other.data;
  }

  //! @brief Id of the entity (agent or object)
  scm::type::EntityId entityId;
  //! @brief Key (topic) associated with the data
  Key key;
  //! @brief  ScmAcyclic data container
  ScmAcyclic data;
};

//! @brief Representation of an entry in the cyclics
struct ScmCyclicRow
{
  //! @brief specialized constructor
  //! @param id scm::type::EntityId
  //! @param k Key
  //! @param v Value
  ScmCyclicRow(scm::type::EntityId id, Key k, Value v)
      : entityId{id},
        key{k},
        tokens{scm::common::TokenizeString(key, SEPARATOR)},
        value{v}
  {
  }

  //! @brief Operator== overload to check is value equal
  //! @param other
  //! @return true when is equal, false when not
  bool operator==(const ScmCyclicRow& other) const
  {
    return entityId == other.entityId &&
           key == other.key &&
           value == other.value;
  }

  //! @brief Id of the entity (agent or object)
  scm::type::EntityId entityId;
  //! @brief Key (topic) associated with the data
  Key key;
  //! @brief Tokenized representation of key
  Tokens tokens;
  //! @brief Data value
  Value value;
};

//! @brief List of keys
using Keys = std::vector<Key>;
//! @brief List of values
using Values = std::vector<Value>;
//! @brief List of data rows
using CyclicRows = std::vector<scm::databuffer::ScmCyclicRow>;

//! @brief List of references to rows
using ScmCyclicRowRefs = std::vector<std::reference_wrapper<const scm::databuffer::ScmCyclicRow>>;
//! @brief List of references to acyclic rows
using ScmAcyclicRowRefs = std::vector<std::reference_wrapper<const scm::databuffer::ScmAcyclicRow>>;

//! @brief A set of cyclic data elements representing a DataInterface query result
//! @details Basic forward iterator properties are provided for convenient result iteration.
class ScmCyclicResultInterface
{
public:
  virtual ~ScmCyclicResultInterface() = default;

  //! @brief Give the size
  //! @return Size
  virtual size_t size() const = 0;
  //! @brief Give the ScmCyclicRow at index
  //! @param index
  //! @return The ScmCyclicRow at index
  virtual const ScmCyclicRow& at(const size_t index) const = 0;
  //! @brief Give the const iterator begin
  //! @return const iterator begin
  virtual ScmCyclicRowRefs::const_iterator begin() const = 0;
  //! @brief Give the const iterator end
  //! @return const iterator end
  virtual ScmCyclicRowRefs::const_iterator end() const = 0;
};

//! @brief A set of acyclic data elements representing a DataInterface query result
//! @details Basic forward iterator properties are provided for convenient result iteration.
class ScmAcyclicResultInterface
{
public:
  virtual ~ScmAcyclicResultInterface() = default;

  //! @brief Give the size
  //! @return Size
  virtual size_t size() const = 0;
  //! @brief Give the ScmAcyclicRow at index
  //! @param index
  //! @return The ScmAcyclicRow at index
  virtual const ScmAcyclicRow& at(const size_t index) const = 0;
  //! @brief Give the const iterator begin
  //! @return const iterator begin
  virtual ScmAcyclicRowRefs::const_iterator begin() const = 0;
  //! @brief Give the const iterator end
  //! @return const iterator end
  virtual ScmAcyclicRowRefs::const_iterator end() const = 0;
};
}  // namespace scm::databuffer

using namespace scm::databuffer;

namespace scm
{

//! @brief The DataReadInterface provides read-only access to the data
class DataBufferReadInterface
{
public:
  DataBufferReadInterface() = default;
  DataBufferReadInterface(const DataBufferReadInterface&) = delete;
  DataBufferReadInterface(DataBufferReadInterface&&) = delete;
  DataBufferReadInterface& operator=(const DataBufferReadInterface&) = delete;
  DataBufferReadInterface& operator=(DataBufferReadInterface&&) = delete;
  virtual ~DataBufferReadInterface() = default;

  //! @brief The no descend flag
  static constexpr bool NO_DESCEND = false;

  //! @brief Retrieves stored cyclic values
  //! @param  entityId   Entity's id
  //! @param  key        Unique topic identification
  //! @return unique_ptr from  scm::databuffer::ScmCyclicResultInterface
  virtual std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> GetCyclic(const std::optional<scm::type::EntityId> entityId, const Key& key) const = 0;

  //! @brief Retrieves stored acyclic values
  //! @param   entityId   Entity's id
  //! @param   key        Unique topic identification
  //! @details Current implementation ignores time and entityId
  //! @return unique_ptr from  scm::databuffer::ScmAcyclicResultInterface
  virtual std::unique_ptr<scm::databuffer::ScmAcyclicResultInterface> GetAcyclic(const std::optional<scm::type::EntityId> entityId, const Key& key) const = 0;

  //! @brief Retrieves stored static values
  //! @param   key        Unique topic identification
  //! @return Values
  virtual Values GetStatic(const Key& key) const = 0;

  //! @brief Retrieves keys at a specific node in the hierarchy.
  //! @details The key parameter has to be prefixed with "Cyclics/", "Acyclics/" or "Statics/" to
  //! get access to the different types of stored elements.
  //! @param   key  Unique topic identification, including prefix
  //! @return Keys
  virtual Keys GetKeys(const Key& key) const = 0;
};

//! @brief The DataWriteInterface provides write-only access to the data
//! @details description (see Put* methods) for cyclics, acyclics and statics are independent of each other.
class DataBufferWriteInterface
{
public:
  DataBufferWriteInterface() = default;
  DataBufferWriteInterface(const DataBufferWriteInterface&) = delete;
  DataBufferWriteInterface(DataBufferWriteInterface&&) = delete;
  DataBufferWriteInterface& operator=(const DataBufferWriteInterface&) = delete;
  DataBufferWriteInterface& operator=(DataBufferWriteInterface&&) = delete;
  virtual ~DataBufferWriteInterface() = default;

  //! @brief Writes cyclic information
  //! @param   entityId   Id of the associated agent or object
  //! @param   key        Unique topic identification
  //! @param   value      Value to be written
  virtual void PutCyclic(const scm::type::EntityId entityId, const Key& key, const Value& value) = 0;

  //! @brief Writes acyclic information
  //! @param   entityId   Id of the associated agent
  //! @param   key        Unique topic identification
  //! @param   acyclic    The acyclic element to be written
  virtual void PutAcyclic(const scm::type::EntityId entityId, const Key& key, const scm::databuffer::ScmAcyclic& acyclic) = 0;

  //! @brief Writes static information
  //! @param  key       Unique topic identification
  //! @param  value     Value to be written
  //! @param  persist   Make value persistent (not affected by Clear())
  virtual void PutStatic(const Key& key, const Value& value, bool persist = false) = 0;

  //! @brief Clears the data contents, except persistent static data
  virtual void ClearRun() = 0;
};

//! @brief The DataInterface provides read/write access to the data
//! @details This interface combines DataReadInterface and DataWriteInterface and adds some additional
//! methods required for instantiation by the framework.
class DataBufferInterface : public DataBufferReadInterface, public DataBufferWriteInterface
{
public:
  DataBufferInterface() = default;

  //! @brief specialized constructor for DataBufferInterface
  //! @param runtimeInformation   const scm::RuntimeInformation *
  DataBufferInterface(const scm::RuntimeInformation* runtimeInformation)
      : runtimeInformation(runtimeInformation)
  {
  }
  DataBufferInterface(const DataBufferInterface&) = delete;
  DataBufferInterface(DataBufferInterface&&) = delete;
  DataBufferInterface& operator=(const DataBufferInterface&) = delete;
  DataBufferInterface& operator=(DataBufferInterface&&) = delete;
  ~DataBufferInterface() override = default;

  //! @brief Instantiates the data buffer
  //! @return true if instantiation was successful, false otherwise
  virtual bool Instantiate()
  {
    return false;
  }

  //! @brief Determines the instantiation status
  //! @return true if data buffer is instantiated, false otherwise
  virtual bool isInstantiated() const
  {
    return false;
  }

  //! @brief Clears the data of the last timestep, but keeps static data
  virtual void ClearTimeStep() = 0;

protected:
  //! @brief References the configuration parameters
  const scm::RuntimeInformation* runtimeInformation;
};

}  // namespace scm