/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file lateralControllerOutput.h
#pragma once

#include <units.h>

#include <sstream>
#include <string>

namespace scm::signal
{
//! @brief Struct definition for the description of a LateralControllerOutput
struct LateralControllerOutput
{
  //! @brief information about the desiredSteeringWheelAngle
  units::angle::radian_t desiredSteeringWheelAngle;
};

}  // namespace scm::signal