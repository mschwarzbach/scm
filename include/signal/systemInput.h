/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <sstream>
#include <string>
#include <vector>

#include "../module/parameterParser/src/Signals/ParametersHaf.h"
#include "include/common/VehicleProperties.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "module/parameterParser/src/TrafficRules/TrafficRules.h"

namespace scm::signal
{
//! @brief Collection of data SCM needs as input
struct SystemInput
{
  //! @brief DriverParameters of SCM
  DriverParameters driverParameters;
  //! @brief Current valid set traffic rules
  TrafficRulesScm trafficRulesScm;
  //! @brief Set of vehicle specific parameters
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  //! @brief Flag indicating if SCM needs a new route in future
  bool triggerNewRouteRequest;
  //! @brief the steering ratio
  double steeringRatio;
  //! @brief maximum steering at front axle (todo: can be consolidated with vehicleParameters)
  double frontAxleMaxSteering;
  //! @brief distance between front and rear tires/axles (todo: can be consolidated with vehicleParameters)
  units::length::meter_t wheelBase;
  //! @brief cycle time of the simulation
  int cycleTime;
  //! @brief Set of parameters when a HAF system is present
  HafParameters hafParameters;
};

}  // namespace scm::signal