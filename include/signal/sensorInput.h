/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <optional>
#include <sstream>
#include <string>

#include "include/common/VehicleProperties.h"

namespace scm::signal
{
//! @brief Input data needed for the sensor
struct SensorInput
{
  //! @brief preview distance
  units::length::meter_t previewDistance;
  //! @brief vehicle properties of own vehicle
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  //! @brief flag indicating if there was a previous request for a new route
  bool triggerNewRouteRequest;
  //! @brief May contain a customized/pre-defined route
  std::optional<osi3::TrafficAction_FollowPathAction> followPathAction;
};

}  // namespace scm::signal