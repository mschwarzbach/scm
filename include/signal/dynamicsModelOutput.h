/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file dynamicsModelOutput.h
#pragma once

#include <sstream>
#include <string>

#include "include/common/DynamicsInformation.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a DynamicsModelOutput
struct DynamicsModelOutput
{
  //! @brief information about the dynamicsInformation
  scm::DynamicsInformation dynamicsInformation;
};

}  // namespace scm::signal