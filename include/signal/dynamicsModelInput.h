/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file dynamicsModelInput.h
#pragma once

#include <sstream>
#include <string>

#include "include/common/VehicleProperties.h"
#include "include/signal/lateralControllerOutput.h"
#include "include/signal/longitudinalControllerOutput.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a DynamicsModelInput
struct DynamicsModelInput
{
  //! @brief information about the lateralControllerOutput
  LateralControllerOutput lateralControllerOutput;
  //! @brief information about the longitudinalControllerOutput
  LongitudinalControllerOutput longitudinalControllerOutput;
  //! @brief information about the vehicleParameters
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  //! @brief information about the acceleration
  units::acceleration::meters_per_second_squared_t acceleration;
  //! @brief information about the cycleTime
  units::time::millisecond_t cycleTime;
};

}  // namespace scm::signal