/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file driverInput.h
#pragma once

#include <map>
#include <sstream>
#include <string>

#include "include/common/VehicleProperties.h"
#include "module/parameterParser/src/Signals/ParametersHaf.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "module/parameterParser/src/TrafficRules/TrafficRules.h"
#include "sensorOutput.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a DriverInput
struct DriverInput
{
  //! @brief information about the driverParameters
  DriverParameters driverParameters;
  //! @brief information about the trafficRulesScm
  TrafficRulesScm trafficRulesScm;
  //! @brief information about the vehicleParameters
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  //! @brief information about the sensorOutput
  scm::signal::SensorOutput sensorOutput;
  //! @brief information about the automatedDriving
  HafParameters automatedDriving;
};

}  // namespace scm::signal