/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <sstream>
#include <string>

namespace scm::signal
{
struct LongitudinalControllerOutput
{
  /// position of acceleration pedal
  double accPedalPos{};
  /// position of brake pedal
  double brakePedalPos{};
  /// gear
  int gear{};
};

}  // namespace scm::signal