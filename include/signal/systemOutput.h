/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <sstream>
#include <string>

#include "../common/LightState.h"
#include "../common/SecondaryDriverTasks.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm::signal
{
//! @brief Collection of relevant output information of SCM before translating to osi
struct SystemOutput
{
  //! @brief Any secondary driver tasks (indicator, horn, switches, etc)
  SecondaryDriverTasks secondaryDriverTasks;
  //! @brief dynamics information (velocityXY, positionXY, etc)
  DynamicsInformation dynamicsInformation;
  //! @brief own vehicle information
  OwnVehicleInformationSCM ownVehicleInformation;
  //! @brief the output of the longitudinal controller (gear and pedal positions)
  LongitudinalControllerOutput longitudinalControllerOutput;
  //! @brief desired steering wheel angle (output of lateral controller)
  double steeringWheelAngle;
  //! @brief desired acceleration
  double acceleration;
};

}  // namespace scm::signal