/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <sstream>
#include <string>

#include "include/common/OsiQlData.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm::signal
{
//! brief Output collection of sensor data
struct SensorOutput
{
  //! @brief information about the own vehicle
  OwnVehicleInformationSCM ownVehicleInformationSCM;
  //! @brief information about the current traffic rules
  TrafficRuleInformationSCM trafficRuleInformationSCM;
  //! @brief information about the current geometry
  GeometryInformationSCM geometryInformationSCM;
  //! @brief information about the surrounding objects of ego agent
  SurroundingObjectsSCM surroundingObjectsSCM;
  //! @brief information about the route and pose of ego agent
  OwnVehicleRoutePose ownVehicleRoutePose;
};

}  // namespace scm::signal