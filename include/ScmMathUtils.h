/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <units.h>

#include <cmath>
#include <sstream>

namespace ScmMathUtils
{

template <typename X, typename Y>
static Y LinearInterpolation(X x, X x_min, X x_max, Y y_min, Y y_max)
{
  if (x_min > x_max || std::abs(static_cast<double>(x_min) - static_cast<double>(x_max)) < std::numeric_limits<double>::epsilon())
  {
    std::stringstream ss;
    ss << "LinearInterpolation called with invalid values. x_min = " << x_min << ", x_max = " << x_max;
    throw std::runtime_error(ss.str());
  }
  if (y_min > y_max)
  {
    std::stringstream ss;
    ss << "LinearInterpolation called with invalid values. y_min = " << y_min << ", y_max = " << y_max;
    throw std::runtime_error(ss.str());
  }
  return y_min + (y_max - y_min) * (x - x_min) / (x_max - x_min);
}

// Borges, Carlos F. "An Improved Algorithm for hypot (a, b)." arXiv preprint arXiv:1904.09481 (2019).
[[nodiscard]] double constexpr hypot(double a, double b)
{
  if (a == 0.0) return std::abs(b);
  if (b == 0.0) return std::abs(a);
  const auto h = std::sqrt(std::fma(a, a, b * b));
  const auto h_sq = h * h;
  const auto a_sq = a * a;
  const auto x = std::fma(-b, b, h_sq - a_sq) + std::fma(h, h, -h_sq) - std::fma(a, a, -a_sq);
  return h - x / (2 * h);
}

}  // namespace ScmMathUtils