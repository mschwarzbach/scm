/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "signal/driverInput.h"
#include "signal/driverOutput.h"
#include "signal/dynamicsModelInput.h"
#include "signal/dynamicsModelOutput.h"
#include "signal/lateralControllerInput.h"
#include "signal/lateralControllerOutput.h"
#include "signal/longitudinalControllerInput.h"
#include "signal/longitudinalControllerOutput.h"
#include "signal/parameterParserOutput.h"
#include "signal/sensorInput.h"
#include "signal/sensorOutput.h"
#include "signal/systemInput.h"
#include "signal/systemOutput.h"
