/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "include/common/CommonHelper.h"

namespace scm
{
namespace CommonTrafficLight
{
//! Type of a traffic light
enum class Type
{
  Undefined,
  ThreeLights,                 //! Standard red, yellow, green without arrows or symbols
  ThreeLightsLeft,             //! red, yellow, green with arrows pointing left
  ThreeLightsRight,            //! red, yellow, green with arrows pointing right
  ThreeLightsStraight,         //! red, yellow, green with arrows pointing upwards
  ThreeLightsLeftStraight,     //! red, yellow, green with arrows pointing left and upwards
  ThreeLightsRightStraight,    //! red, yellow, green with arrows pointing right and upwards
  TwoLights,                   //! red, green, yellow green, red yellow, without arrows or symbols
  TwoLightsPedestrian,         //! red, green with pedestrian symbol
  TwoLightsBicycle,            //! red, green with bicycle symbol
  TwoLightsPedestrianBicycle,  //! red, green with pedestrian and bicycle symbol
  OneLight,                    //! red, yellow, or green with no symbol
  OneLightPedestrian,          //! red, yellow, or green with pedestrian symbol
  OneLightBicycle,             //! red, yellow, or green with bicycle symbol
  OneLightPedestrianBicycle    //! red, yellow, or green with pedestrian and bicycle symbol
};

//! State of a traffic light
enum class State
{
  Off,
  Green,
  Yellow,
  Red,
  RedYellow,
  YellowFlashing,
  Unknown
};

//! Represents a single traffic light as seen from an agent
struct Entity
{
  Type type{Type::ThreeLights};  ///< type of the traffic light as seen from an agent
  State state{State::Red};       ///< state of the traffic light as seen from an agent
  double relativeDistance{0.0};  ///< distance between the agent and the traffic light

  bool operator==(const Entity& entity) const
  {
    return type == entity.type && state == entity.state && scm::common::DoubleEquality(relativeDistance, entity.relativeDistance);
  }
};
}  // namespace CommonTrafficLight
}  // namespace scm