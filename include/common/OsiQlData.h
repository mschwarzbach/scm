/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  OsiQlData.h

#pragma once
#include <memory>

namespace osiql
{
template <typename>
struct Pose;
struct Route;

template <typename, typename>
struct Point;
struct Lane;
struct ReferenceLineCoordinates;
}  // namespace osiql

//! @brief information about the route and pose of ego agent
struct OwnVehicleRoutePose
{
  std::shared_ptr<osiql::Route> route;
  std::shared_ptr<osiql::Pose<osiql::Point<const osiql::Lane, osiql::ReferenceLineCoordinates>>> pose;
};