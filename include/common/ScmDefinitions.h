/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <units.h>

#include <array>
#include <cmath>
#include <list>
#include <map>
#include <ostream>
#include <string>
#include <unordered_map>

#include "AreaOfInterest.h"
#include "CommonHelper.h"

using namespace units::literals;
namespace ScmDefinitions
{
//! @brief Special value representing an undefined or invalid value
static constexpr double DEFAULT_VALUE = -999.0;

//! @brief Special value representing a limit to valid 'time to collisions'
static constexpr units::time::second_t TTC_LIMIT = 99.0_s;

//! @brief Every velocity below this threshold counts as traffic jam [m/s].
static constexpr units::velocity::meters_per_second_t TRAFFIC_JAM_VELOCITY_THRESHOLD = units::velocity::meters_per_second_t(60.0_kph);

//! @brief When a traffic jam is detected (QUEUED_TRAFFIC), this value represents the minimum reasonable approaching velocity [m/s].
static constexpr units::velocity::meters_per_second_t MIN_TRAFFIC_JAM_APROACHING_VELOCITY = TRAFFIC_JAM_VELOCITY_THRESHOLD / 2.0;

//! @brief If agents driver slower than this threshold they should form a rescue lane [m/s].
//! @details Additional condition: vRescueLane * 2 <= _vJam * 2/3 [m/s].
static constexpr units::velocity::meters_per_second_t FORM_RESCUELANE_VELOCITY_THRESHOLD = units::velocity::meters_per_second_t(10.0_kph);

static constexpr units::velocity::kilometers_per_hour_t NORMATIVE_VELOCITY = 130_kph;  // 130 km/h represents the normative velocity value

//! @brief Factor for small numeric differences between values when comparing these.
static constexpr double EPSILON = 1e-4;

//! @brief Lower limit for Tau Dot, below which a collision is detected.
static constexpr double TAU_DOT_LIMIT = -0.5;

//! @brief Acceleration due to gravity
static constexpr auto ACCELERATION_OF_GRAVITY = 9.81_mps_sq;

//! @brief Defines a factor multiplied with the agent's comfort acceleration or deceleration to determine a threshold value.
//! @details If an acceleration or deceleration greater than this value is required to avoid a collision, the current situation
//! is interpreted as high risk, below the threshold it is medium risk.
static constexpr double COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER{3.5};

static constexpr auto INF_DISTANCE{units::make_unit<units::length::meter_t>(std::numeric_limits<double>::infinity())};
static constexpr auto INF_ACCELELERATION{units::make_unit<units::acceleration::meters_per_second_squared_t>(std::numeric_limits<double>::infinity())};
static constexpr auto INF_VELOCITY{units::make_unit<units::velocity::meters_per_second_t>(std::numeric_limits<double>::infinity())};
static constexpr auto INF_TIME{units::make_unit<units::time::second_t>(std::numeric_limits<double>::infinity())};

}  // namespace ScmDefinitions

enum class Side
{
  Left,
  Right
};

enum class LaneChangeAction
{
  None = 0,
  IntentPositive,
  IntentNegative,
  ForcePositive,
  ForceNegative
};

static constexpr std::array<AreaOfInterest, 17> SURROUNDING_AREAS_OF_INTEREST{
    AreaOfInterest::LEFT_FRONT,
    AreaOfInterest::LEFT_FRONT_FAR,
    AreaOfInterest::RIGHT_FRONT,
    AreaOfInterest::RIGHT_FRONT_FAR,
    AreaOfInterest::LEFT_REAR,
    AreaOfInterest::RIGHT_REAR,
    AreaOfInterest::EGO_FRONT,
    AreaOfInterest::EGO_FRONT_FAR,
    AreaOfInterest::EGO_REAR,
    AreaOfInterest::LEFT_SIDE,
    AreaOfInterest::RIGHT_SIDE,
    AreaOfInterest::LEFTLEFT_FRONT,
    AreaOfInterest::RIGHTRIGHT_FRONT,
    AreaOfInterest::LEFTLEFT_REAR,
    AreaOfInterest::RIGHTRIGHT_REAR,
    AreaOfInterest::LEFTLEFT_SIDE,
    AreaOfInterest::RIGHTRIGHT_SIDE,
};

static constexpr std::array<AreaOfInterest, 14> SURROUNDING_AREAS_OF_INTEREST_VECTOR{
    AreaOfInterest::LEFT_FRONT,
    AreaOfInterest::RIGHT_FRONT,
    AreaOfInterest::LEFT_REAR,
    AreaOfInterest::RIGHT_REAR,
    AreaOfInterest::EGO_FRONT,
    AreaOfInterest::EGO_REAR,
    AreaOfInterest::LEFT_SIDE,
    AreaOfInterest::RIGHT_SIDE,
    AreaOfInterest::LEFTLEFT_FRONT,
    AreaOfInterest::RIGHTRIGHT_FRONT,
    AreaOfInterest::LEFTLEFT_REAR,
    AreaOfInterest::RIGHTRIGHT_REAR,
    AreaOfInterest::LEFTLEFT_SIDE,
    AreaOfInterest::RIGHTRIGHT_SIDE,
};

static constexpr std::array<AreaOfInterest, 21> ALL_AREAS_OF_INTEREST{
    AreaOfInterest::LEFT_FRONT,
    AreaOfInterest::LEFT_FRONT_FAR,
    AreaOfInterest::RIGHT_FRONT,
    AreaOfInterest::RIGHT_FRONT_FAR,
    AreaOfInterest::LEFT_REAR,
    AreaOfInterest::RIGHT_REAR,
    AreaOfInterest::EGO_FRONT,
    AreaOfInterest::EGO_FRONT_FAR,
    AreaOfInterest::EGO_REAR,
    AreaOfInterest::LEFT_SIDE,
    AreaOfInterest::RIGHT_SIDE,
    AreaOfInterest::LEFTLEFT_FRONT,
    AreaOfInterest::RIGHTRIGHT_FRONT,
    AreaOfInterest::LEFTLEFT_REAR,
    AreaOfInterest::RIGHTRIGHT_REAR,
    AreaOfInterest::LEFTLEFT_SIDE,
    AreaOfInterest::RIGHTRIGHT_SIDE,
    AreaOfInterest::HUD,
    AreaOfInterest::INFOTAINMENT,
    AreaOfInterest::INSTRUMENT_CLUSTER,
    AreaOfInterest::DISTRACTION,
};

static constexpr std::array<AreaOfInterest, 12> FRONT_SIDE_AREAS_OF_INTEREST{
    AreaOfInterest::LEFT_FRONT,
    AreaOfInterest::LEFT_FRONT_FAR,
    AreaOfInterest::RIGHT_FRONT,
    AreaOfInterest::RIGHT_FRONT_FAR,
    AreaOfInterest::EGO_FRONT,
    AreaOfInterest::EGO_FRONT_FAR,
    AreaOfInterest::LEFT_SIDE,
    AreaOfInterest::RIGHT_SIDE,
    AreaOfInterest::LEFTLEFT_FRONT,
    AreaOfInterest::RIGHTRIGHT_FRONT,
    AreaOfInterest::LEFTLEFT_SIDE,
    AreaOfInterest::RIGHTRIGHT_SIDE};

enum class RelativeLongitudinalPosition
{
  FRONT_FAR,
  FRONT,
  SIDE,
  REAR,
  REAR_FAR
};

enum class RelativeLane
{
  LEFTLEFT,
  LEFT,
  EGO,
  RIGHT,
  RIGHTRIGHT
};

static const std::map<RelativeLane, RelativeLane> RightAndLeftRelativeLanes{
    {RelativeLane::RIGHT, RelativeLane::RIGHTRIGHT},
    {RelativeLane::RIGHTRIGHT, RelativeLane::RIGHT},
    {RelativeLane::LEFT, RelativeLane::LEFTLEFT},
    {RelativeLane::LEFTLEFT, RelativeLane::LEFT}};

static const std::map<AreaOfInterest, RelativeLane> AreaOfInterest2RelativeLane{
    {AreaOfInterest::LEFT_FRONT, RelativeLane::LEFT},
    {AreaOfInterest::LEFT_FRONT_FAR, RelativeLane::LEFT},
    {AreaOfInterest::LEFT_REAR, RelativeLane::LEFT},
    {AreaOfInterest::LEFT_SIDE, RelativeLane::LEFT},
    {AreaOfInterest::RIGHT_FRONT, RelativeLane::RIGHT},
    {AreaOfInterest::RIGHT_FRONT_FAR, RelativeLane::RIGHT},
    {AreaOfInterest::RIGHT_REAR, RelativeLane::RIGHT},
    {AreaOfInterest::RIGHT_SIDE, RelativeLane::RIGHT},
    {AreaOfInterest::EGO_FRONT, RelativeLane::EGO},
    {AreaOfInterest::EGO_FRONT_FAR, RelativeLane::EGO},
    {AreaOfInterest::EGO_REAR, RelativeLane::EGO},
    {AreaOfInterest::LEFTLEFT_FRONT, RelativeLane::LEFTLEFT},
    {AreaOfInterest::LEFTLEFT_REAR, RelativeLane::LEFTLEFT},
    {AreaOfInterest::LEFTLEFT_SIDE, RelativeLane::LEFTLEFT},
    {AreaOfInterest::RIGHTRIGHT_FRONT, RelativeLane::RIGHTRIGHT},
    {AreaOfInterest::RIGHTRIGHT_REAR, RelativeLane::RIGHTRIGHT},
    {AreaOfInterest::RIGHTRIGHT_SIDE, RelativeLane::RIGHTRIGHT}};

constexpr std::array<RelativeLane, 5> ALL_RELATIVE_LANES{
    RelativeLane::LEFTLEFT, RelativeLane::LEFT, RelativeLane::RIGHT, RelativeLane::RIGHTRIGHT, RelativeLane::EGO};

static const std::map<RelativeLane, int> RelativeLane2RelativeLaneId{
    {RelativeLane::LEFTLEFT, 2},
    {RelativeLane::LEFT, 1},
    {RelativeLane::EGO, 0},
    {RelativeLane::RIGHT, -1},
    {RelativeLane::RIGHTRIGHT, -2}};

enum class SurroundingLane
{
  LEFT,
  EGO,
  RIGHT
};

static const std::map<SurroundingLane, RelativeLane> SurroundingLane2RelativeLane{
    {SurroundingLane::LEFT, RelativeLane::LEFT},
    {SurroundingLane::EGO, RelativeLane::EGO},
    {SurroundingLane::RIGHT, RelativeLane::RIGHT}};

static const std::map<RelativeLane, SurroundingLane> RelativeLane2SurroundingLane{
    {RelativeLane::LEFT, SurroundingLane::LEFT},
    {RelativeLane::EGO, SurroundingLane::EGO},
    {RelativeLane::RIGHT, SurroundingLane::RIGHT}};

static const std::map<SurroundingLane, AreaOfInterest> SurroundingLane2AreaOfInterest{
    {SurroundingLane::LEFT, AreaOfInterest::LEFT_FRONT},
    {SurroundingLane::EGO, AreaOfInterest::EGO_FRONT},
    {SurroundingLane::RIGHT, AreaOfInterest::RIGHT_FRONT}};

static const std::map<RelativeLane, Side> RelativeLane2Side{
    {RelativeLane::LEFT, Side::Left},
    {RelativeLane::LEFTLEFT, Side::Left},
    {RelativeLane::RIGHT, Side::Right},
    {RelativeLane::RIGHTRIGHT, Side::Right}};

//! @brief The acoustic warning for the driver of an ADAS (e.g. a blind spot assistant)
struct AcousticDriverWarning
{
  //! @brief is this warning active
  bool activity;
  //! @brief numbers from clock-face, use 0 for 12, undefined direction: -1
  int direction;
  //! @brief frequency of oscillation
  double frequency;
  //! @brief amplitude in dB (at driver's position)
  double amplitude;
};

//! @brief The optical warning for the driver of an ADAS (e.g. a blind spot assistant)
struct OpticalDriverWarning
{
  //! @brief is this warning active
  bool activity;
  //! @brief location of the warning
  AreaOfInterest position;
  //! @brief type t.b.d.
  int type;
  //! @brief luminance of the warning in cd/m^2
  double luminance;
  //! @brief luminance of the environment
  double luminanceEnv;
  //! @brief perceived (?) dimension of the light
  double maxWidth;
};

//! @brief This struct represents a set of parameters needed for computation of critical action intensities
struct CriticalActionIntensitiesParameters
{
  //! @brief Lateral velocity of ego
  units::velocity::meters_per_second_t lateralVelocityEgo;
  //! @brief Anticipated lateral acceleration (upper bound)
  units::acceleration::meters_per_second_squared_t lateralAccelerationAnticipatedUpper;
  //! @brief Anticipated lateral acceleration (lower bound)
  units::acceleration::meters_per_second_squared_t lateralAccelerationAnticipatedLower;
  //! @brief Lateral position in lane of ego
  units::length::meter_t lateralPositionEgo;
  //! @brief Index for side AOIs
  int sideAoiIndex;
  //! @brief Lateral position in lane needed to pass on the left
  units::length::meter_t lateralPositionToPassLeft;
  //! @brief Lateral position in lane needed to pass on the right
  units::length::meter_t lateralPositionToPassRight;
  //! @brief Flag indicating if the current velocity is sufficient to resolve the upper bound
  bool currentVelocityWillResolveRightUpperLimit;
  //! @brief Flag indicating if the current velocity is sufficient to resolve the lower bound
  bool currentVelocityWillResolveLeftUpperLimit;

  //! @brief Custom out stream operator
  //! @param os
  //! @param rhs
  //! @return All values of this struct separated with a blank character
  friend std::ostream& operator<<(std::ostream& os, const CriticalActionIntensitiesParameters& rhs)
  {
    os << rhs.lateralVelocityEgo << " "
       << rhs.lateralAccelerationAnticipatedUpper << " "
       << rhs.lateralAccelerationAnticipatedLower << " "
       << rhs.lateralPositionEgo << " "
       << rhs.sideAoiIndex << " "
       << rhs.lateralPositionToPassLeft << " "
       << rhs.lateralPositionToPassRight << " "
       << rhs.currentVelocityWillResolveRightUpperLimit << " "
       << rhs.currentVelocityWillResolveLeftUpperLimit;
    return os;
  }

  //! @brief Custom equality operator for its struct
  //! @param lhs
  //! @param rhs
  //! @return true if all values within are considered equal, false otherwise
  friend bool operator==(const CriticalActionIntensitiesParameters& lhs, const CriticalActionIntensitiesParameters& rhs)
  {
    return scm::common::DoubleEquality(lhs.lateralVelocityEgo.value(), rhs.lateralVelocityEgo.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.lateralAccelerationAnticipatedUpper.value(), rhs.lateralAccelerationAnticipatedUpper.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.lateralAccelerationAnticipatedLower.value(), rhs.lateralAccelerationAnticipatedLower.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.lateralPositionEgo.value(), rhs.lateralPositionEgo.value(), ScmDefinitions::EPSILON) &&
           (lhs.sideAoiIndex == rhs.sideAoiIndex) &&
           scm::common::DoubleEquality(lhs.lateralPositionToPassLeft.value(), rhs.lateralPositionToPassLeft.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.lateralPositionToPassRight.value(), rhs.lateralPositionToPassRight.value(), ScmDefinitions::EPSILON) &&
           (lhs.currentVelocityWillResolveRightUpperLimit == rhs.currentVelocityWillResolveRightUpperLimit) &&
           (lhs.currentVelocityWillResolveLeftUpperLimit == rhs.currentVelocityWillResolveLeftUpperLimit);
  }
};

//! @brief Struct containing limits for braking and steering
struct TTCActionLimits
{
  //! @brief Upper limit for braking
  units::time::second_t brakeUpperLimit;
  //! @brief Lower limit for braking
  units::time::second_t brakeLowerLimit;
  //! @brief Upper limit for steering
  units::time::second_t steerUpperLimit;
  //! @brief Lower limit for steering
  units::time::second_t steerLowerLimit;

  //! @brief Custom out stream operator
  //! @param os
  //! @param rhs
  //! @return All values of this struct separated with a blank character
  friend std::ostream& operator<<(std::ostream& os, const TTCActionLimits& rhs)
  {
    os << rhs.brakeUpperLimit << " "
       << rhs.brakeLowerLimit << " "
       << rhs.steerUpperLimit << " "
       << rhs.steerLowerLimit;
    return os;
  }

  //! @brief Custom equality operator for its struct
  //! @param lhs
  //! @param rhs
  //! @return true if all values within are considered equal, false otherwise
  friend bool operator==(const TTCActionLimits& lhs, const TTCActionLimits& rhs)
  {
    return scm::common::DoubleEquality(lhs.brakeUpperLimit.value(), rhs.brakeUpperLimit.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.brakeLowerLimit.value(), rhs.brakeLowerLimit.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.steerUpperLimit.value(), rhs.steerUpperLimit.value(), ScmDefinitions::EPSILON) &&
           scm::common::DoubleEquality(lhs.steerLowerLimit.value(), rhs.steerLowerLimit.value(), ScmDefinitions::EPSILON);
  }
};

//! @brief The ExternalControlState struct represents if the longitudinal and/or lateral regulation of SCM should be controlled externally.
struct ExternalControlState
{
  //! @brief Flag indicating if external longitudinal regulation is active
  bool longitudinalActive{false};
  //! @brief Flag indicating if external lateral regulation is active
  bool lateralActive{false};
  //! @brief timestep if/when the lateral external control was deactivated
  units::time::millisecond_t lateralDeactivationTime{-999};
};

//! @brief Represents the estimated time until an overlap with an observed vehicle is first detected and the estimated time until the overlap is cleared again.
struct ObstructionDynamics
{
  //! @brief How much time remaining (estimated) till the obstruction will occur
  units::time::second_t timeToEnter;
  //! @brief How much time remaining (estimated) till the the obstruction is cleared
  units::time::second_t timeToLeave;
};

namespace units
{
//! Adds a single new unit to the given namespace, as well as a literal definition and `cout` support based on the given
//! `abbreviation`.
UNIT_ADD(
    curvature, inverse_meter, inverse_meters, i_m, units::unit<std::ratio<1>, units::inverse<units::length::meter>>)

namespace category
{
typedef base_unit<detail::meter_ratio<-1>> curvature_unit;
}

UNIT_ADD_CATEGORY_TRAIT(curvature)

using inertia = unit_t<compound_unit<mass::kilogram, squared<length::meter>>>;  //!< The unit of inertia
using impulse = unit_t<compound_unit<force::newton, time::second>>;             //!< The unit of impulse

/// Radian are by default unitless, but the units.h explicitly defines it as a unit. This leads to several typematch
/// errors in calculations. Therefore the inverse_radian is used to remove the radian type.
using inverse_radian = units::unit_t<units::inverse<units::angle::radian>>;

UNIT_ADD(angular_acceleration,
         radians_per_second_squared,
         radians_per_second_squared,
         rad_per_s_sq,
         compound_unit<angle::radians, inverse<squared<time::seconds>>>)

namespace category
{
using angular_acceleration_unit = base_unit<detail::meter_ratio<0>, std::ratio<0>, std::ratio<-2>, std::ratio<1>>;
}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(angular_acceleration)

UNIT_ADD(angular_jerk,
         radians_per_second_cubed,
         radians_per_second_cubed,
         rad_per_s_cu,
         compound_unit<angle::radians, inverse<cubed<time::seconds>>>)

namespace category
{
using angular_jerk_unit = base_unit<detail::meter_ratio<0>, std::ratio<0>, std::ratio<-3>, std::ratio<1>>;
}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(angular_jerk)

}  // namespace units

namespace units::math
{

template <class UnitType, class = std::enable_if_t<traits::is_unit_t<UnitType>::value>>

bool isinf(const UnitType x) noexcept

{
  return std::isinf(x());
}
}  // namespace units::math
