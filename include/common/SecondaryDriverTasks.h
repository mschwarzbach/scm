/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "LightState.h"

class SecondaryDriverTasks
{
public:
  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  SecondaryDriverTasks() = default;

  /**
   * @brief Construct a new Secondary Driver Tasks Object
   *
   * @param indicatorState        State of the turning indicator (left, off, right, warning)
   * @param hornSwitch            Switch for activation of horn
   * @param headLightSwitch       Switch for activation of headlight
   * @param highBeamLightSwitch   Switch for activation of Highbeamlight
   * @param flasher               Switch for activation of Flasher
   */
  SecondaryDriverTasks(
      scm::LightState::Indicator indicatorState,
      bool hornSwitch,
      bool headLightSwitch,
      bool highBeamLightSwitch,
      bool flasher)
      : indicatorState(indicatorState),
        hornSwitch(hornSwitch),
        headLightSwitch(headLightSwitch),
        highBeamLightSwitch(highBeamLightSwitch),
        flasherSwitch(flasher)
  {
  }

  virtual ~SecondaryDriverTasks() = default;

  //! State of the turning indicator (left, center/off, right)
  scm::LightState::Indicator indicatorState;
  //! Switch for activation of horn
  bool hornSwitch;
  //! Switch for activation of headlight
  bool headLightSwitch;
  //! Switch for activation of Highbeamlight
  bool highBeamLightSwitch;
  //! Switch for activation of Flasher
  bool flasherSwitch;
};