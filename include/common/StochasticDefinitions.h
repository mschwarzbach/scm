/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cmath>
#include <limits>
#include <variant>

#include "CommonHelper.h"

namespace scm::parameter
{

/// class representing normal distribution
class NormalDistribution
{
public:
  NormalDistribution() = default;
  NormalDistribution(const NormalDistribution&) = default;  ///< copy constructor
  NormalDistribution(NormalDistribution&&) = default;       ///< move constructor

  /// @return copy assignment operator
  NormalDistribution& operator=(const NormalDistribution&) = default;

  /// @return move assignment operator
  NormalDistribution& operator=(NormalDistribution&&) = default;

  /**
   * @brief Construct a new Normal Distribution object
   *
   * @param mean              mean
   * @param standardDeviation standard deviation
   * @param min               minimum deviation
   * @param max               maximum deviation
   */
  NormalDistribution(double mean, double standardDeviation, double min, double max)
      : mean{mean}, standardDeviation{standardDeviation}, min{min}, max{max} {}

private:
  double mean{0.0};
  double standardDeviation{0.0};
  double min{std::numeric_limits<double>::lowest()};
  double max{std::numeric_limits<double>::max()};

public:
  /**
   * @brief Function to compare equality of two normal distribution
   *
   * @param rhs  another normal distribution
   * @return true if mean, standardDeviation, min and max are equal in both normal distributions
   */
  bool operator==(const NormalDistribution& rhs) const
  {
    return this == &rhs || (scm::common::DoubleEquality(mean, rhs.mean) &&
                            scm::common::DoubleEquality(standardDeviation, rhs.standardDeviation) &&
                            scm::common::DoubleEquality(min, rhs.min) &&
                            scm::common::DoubleEquality(max, rhs.max));
  }

  /**
   * @brief Function to compare non equality of two normal distribution
   *
   * @param rhs  another normal distribution
   * @return true if equality function returns false
   */
  bool operator!=(const NormalDistribution& rhs) const
  {
    return !operator==(rhs);
  }

  /**
   * @brief Get the Min standard deviation
   *
   * @return minimum deviation
   */
  double GetMin() const
  {
    return min;
  }

  /**
   * @brief Get the Max deviation
   *
   * @return maximum deviation
   */
  double GetMax() const
  {
    return max;
  }

  /**
   * @brief Get the Mean
   *
   * @return mean
   */
  double GetMean() const
  {
    return mean;
  }

  /**
   * @brief Get the Standard Deviation
   *
   * @return standard deviation
   */
  double GetStandardDeviation() const
  {
    return standardDeviation;
  }

  /**
   * @brief Get the Mu object
   *
   * @return mean
   */
  double GetMu() const
  {
    return mean;
  }

  /**
   * @brief Get the Sigma
   *
   * @return standard deviation
   */
  double GetSigma() const
  {
    return standardDeviation;
  }

  /**
   * @brief Set the Min
   *
   * @param min minimum deviation
   */
  void SetMin(double min)
  {
    this->min = min;
  }

  /**
   * @brief Set the Max
   *
   * @param max maximum deviation
   */
  void SetMax(double max)
  {
    this->max = max;
  }

  /**
   * @brief Set the Mean
   *
   * @param mean mean distribution
   */
  void SetMean(double mean)
  {
    this->mean = mean;
  }

  /**
   * @brief Set the Standard Deviation
   *
   * @param standardDeviation standard deviation
   */
  void SetStandardDeviation(double standardDeviation)
  {
    this->standardDeviation = standardDeviation;
  }

  /**
   * @brief Set the average value (mean)
   *
   * @param mu Average value (mean) of the random variable
   */
  void SetMu(double mu)
  {
    mean = mu;
  }

  /**
   * @brief Set the Sigma
   *
   * @param sigma standard deviation
   */
  void SetSigma(double sigma)
  {
    standardDeviation = sigma;
  }
};

/// class representing logarthmic normal distribution
class LogNormalDistribution
{
public:
  LogNormalDistribution() = default;
  LogNormalDistribution(const LogNormalDistribution&) = default;  ///< copy constructor
  LogNormalDistribution(LogNormalDistribution&&) = default;       ///< move constructor

  /// @return copy assignment operator
  LogNormalDistribution& operator=(const LogNormalDistribution&) = default;

  /// @return move assignment operator
  LogNormalDistribution& operator=(LogNormalDistribution&&) = default;

  /**
   * @brief Construct a new Log Normal Distribution object
   *
   * @param mu    Mean
   * @param sigma Standard deviation
   * @param min   Lower limit of a range for a distribution (minimum value of stochastic parameter)
   * @param max   Upper limit of a range for a distribution (maximum value of stochastic parameter)
   */
  LogNormalDistribution(double mu, double sigma, double min, double max)
      : mu{mu}, sigma{sigma}, min{min}, max{max}
  {
    mean = exp(mu + 0.5 * sigma * sigma);
    standardDeviation = mean * sqrt(exp(sigma * sigma) - 1);
  }

  /**
   * @brief Create a With Mean Sd object
   *
   * @param mean              Mean value
   * @param standardDeviation Standard deviation from mean value
   * @param min               Lower limit of a range for a distribution (minimum value of stochastic parameter)
   * @param max               Upper limit of a range for a distribution (maximum value of stochastic parameter)
   * @return LogNormalDistribution
   */
  static LogNormalDistribution CreateWithMeanSd(double mean, double standardDeviation, double min, double max)
  {
    double s2 = log(pow(standardDeviation / mean, 2) + 1);
    double mu = log(mean) - s2 / 2;
    double sigma = sqrt(s2);
    return LogNormalDistribution(mu, sigma, min, max);
  }

private:
  double mu{0.0};
  double sigma{0.0};
  double mean{0.0};
  double standardDeviation{0.0};
  double min{std::numeric_limits<double>::lowest()};
  double max{std::numeric_limits<double>::max()};

public:
  /**
   * @brief Function to compare equality of two LogNormalDistribution objects
   *
   * @param rhs Another LogNormalDistribution object
   * @return true if mu, sigma, min, max are equal in both LogNormalDistribution objects
   */
  bool operator==(const LogNormalDistribution& rhs) const
  {
    return this == &rhs || (scm::common::DoubleEquality(mu, rhs.mu) &&
                            scm::common::DoubleEquality(sigma, rhs.sigma) &&
                            scm::common::DoubleEquality(min, rhs.min) &&
                            scm::common::DoubleEquality(max, rhs.max));
  }

  /**
   * @brief Function to compare inequality of two LogNormalDistribution objects
   *
   * @param rhs   Another LogNormalDistribution object
   * @return true if the equality function returns false
   */
  bool operator!=(const LogNormalDistribution& rhs) const
  {
    return !operator==(rhs);
  }

  /**
   * @brief Get the Min
   *
   * @return minimum deviation
   */
  double GetMin() const
  {
    return min;
  }

  /**
   * @brief Get the Max
   *
   * @return maximum deviation
   */
  double GetMax() const
  {
    return max;
  }

  /**
   * @brief Get the Mean
   *
   * @return mean distribution
   */
  double GetMean() const
  {
    return mean;
  }

  /**
   * @brief Get the Standard Deviation
   *
   * @return standard deviation
   */
  double GetStandardDeviation() const
  {
    return standardDeviation;
  }

  /**
   * @brief Get the Mu
   *
   * @return mean
   */
  double GetMu() const
  {
    return mu;
  }

  /**
   * @brief Get the Sigma
   *
   * @return sigma
   */
  double GetSigma() const
  {
    return sigma;
  }

  /**
   * @brief Set the Min
   *
   * @param min minimum deviation
   */
  void SetMin(double min)
  {
    this->min = min;
  }

  /**
   * @brief Set the Max
   *
   * @param max maximum deviation
   */
  void SetMax(double max)
  {
    this->max = max;
  }

  /**
   * @brief Set the Mean
   *
   * @param mean mean
   */
  void SetMean(double mean)
  {
    this->mean = mean;

    double s2 = log(pow(standardDeviation / mean, 2) + 1);
    mu = log(mean) - s2 / 2;
    sigma = sqrt(s2);
  }

  /**
   * @brief Set the Standard Deviation object
   *
   * @param standardDeviation standard deviation
   */
  void SetStandardDeviation(double standardDeviation)
  {
    this->standardDeviation = standardDeviation;

    double s2 = log(pow(standardDeviation / mean, 2) + 1);
    mu = log(mean) - s2 / 2;
    sigma = sqrt(s2);
  }

  /**
   * @brief Set the average value (mean)
   *
   * @param mu Average value (mean) of the random variable
   */
  void SetMu(double mu)
  {
    this->mu = mu;

    mean = exp(mu + 0.5 * sigma * sigma);
    standardDeviation = mean * sqrt(exp(sigma * sigma) - 1);
  }

  /**
   * @brief Set the Sigma
   *
   * @param sigma sigma
   */
  void SetSigma(double sigma)
  {
    this->sigma = sigma;

    mean = exp(mu + 0.5 * sigma * sigma);
    standardDeviation = mean * sqrt(exp(sigma * sigma) - 1);
  }
};

/// class representing uniform distribution
class UniformDistribution
{
public:
  UniformDistribution() = default;
  UniformDistribution(const UniformDistribution&) = default;  ///< copy constructor
  UniformDistribution(UniformDistribution&&) = default;       ///< move constructor

  /// @return copy assignment operator
  UniformDistribution& operator=(const UniformDistribution&) = default;

  /// @return move assignment operator
  UniformDistribution& operator=(UniformDistribution&&) = default;

  /**
   * @brief Construct a new Uniform Distribution object
   *
   * @param min minimum deviation
   * @param max maximum deviation
   */
  UniformDistribution(double min, double max)
      : min{min}, max{max} {}

private:
  double min{std::numeric_limits<double>::lowest()};
  double max{std::numeric_limits<double>::max()};

public:
  /**
   * @brief Function to compare equality of two UniformDistribution objects
   *
   * @param rhs Another UniformDistribution object
   * @return true if min and max are equal in both UniformDistribution objects
   */
  bool operator==(const UniformDistribution& rhs) const
  {
    return this == &rhs || (scm::common::DoubleEquality(min, rhs.min) &&
                            scm::common::DoubleEquality(max, rhs.max));
  }

  /**
   * @brief Get the Min
   *
   * @return minimum deviation
   */
  double GetMin() const
  {
    return min;
  }

  /**
   * @brief Get the Max
   *
   * @return maximum deviation
   */
  double GetMax() const
  {
    return max;
  }

  /**
   * @brief Set the Min
   *
   * @param min minimum deviation
   */
  void SetMin(double min)
  {
    this->min = min;
  }

  /**
   * @brief Set the Max
   *
   * @param max maximum deviation
   */
  void SetMax(double max)
  {
    this->max = max;
  }
};

/// class representing exponential distribution
class ExponentialDistribution
{
public:
  ExponentialDistribution() = default;
  ExponentialDistribution(const ExponentialDistribution&) = default;  ///< copy constructor
  ExponentialDistribution(ExponentialDistribution&&) = default;       ///< move constructor

  /// @return copy assignment operator
  ExponentialDistribution& operator=(const ExponentialDistribution&) = default;

  /// @return move assignment operator
  ExponentialDistribution& operator=(ExponentialDistribution&&) = default;

  /**
   * @brief Construct a new Exponential Distribution object
   *
   * @param lambda    Rate parameter
   * @param min       Lower limit of a range for a distribution (minimum value of stochastic parameter)
   * @param max       Upper limit of a range for a distribution (maximum value of stochastic parameter)
   */
  ExponentialDistribution(double lambda, double min, double max)
      : lambda{lambda}, min{min}, max{max} {}

private:
  double lambda{1.0};
  double min{std::numeric_limits<double>::lowest()};
  double max{std::numeric_limits<double>::max()};

public:
  /**
   * @brief Function to check if two ExponentialDistribution objects are equal
   *
   * @param rhs Another ExponentialDistribution object
   * @return true if lambda, min, max are equal in both ExponentialDistribution objects
   */
  bool operator==(const ExponentialDistribution& rhs) const
  {
    return this == &rhs || (scm::common::DoubleEquality(lambda, rhs.lambda) &&
                            scm::common::DoubleEquality(min, rhs.min) &&
                            scm::common::DoubleEquality(max, rhs.max));
  }

  /**
   * @brief Get the Min
   *
   * @return minimum deviation
   */
  double GetMin() const
  {
    return min;
  }

  /**
   * @brief Get the Max
   *
   * @return maximum deviation
   */
  double GetMax() const
  {
    return max;
  }

  /**
   * @brief Get the Lambda
   *
   * @return lambda
   */
  double GetLambda() const
  {
    return lambda;
  }

  /**
   * @brief Get the Mean
   *
   * @return mean as inverse of lambda
   */
  double GetMean() const
  {
    return 1 / lambda;
  }

  /**
   * @brief Get the Standard Deviation
   *
   * @return standard deviation as inverse of lambda
   */
  double GetStandardDeviation() const
  {
    return 1 / lambda;
  }

  /**
   * @brief Set the Min
   *
   * @param min minimum deviation
   */
  void SetMin(double min)
  {
    this->min = min;
  }

  /**
   * @brief Set the Max
   *
   * @param max maximum deviation
   */
  void SetMax(double max)
  {
    this->max = max;
  }

  /**
   * @brief Set the Lambda
   *
   * @param lambda Rate parameter
   */
  void SetLambda(double lambda)
  {
    this->lambda = lambda;
  }

  /**
   * @brief Set the lambda using mean
   *
   * @param mean mean as inverse of lambda
   */
  void SetMean(double mean)
  {
    lambda = 1 / mean;
  }
};

/// class representing gamma distribution
class GammaDistribution
{
public:
  GammaDistribution() = default;
  GammaDistribution(const GammaDistribution&) = default;  ///< copy constructor
  GammaDistribution(GammaDistribution&&) = default;       ///< move constructor

  /// @return copy assignment operator
  GammaDistribution& operator=(const GammaDistribution&) = default;

  /// @return move assignment operator
  GammaDistribution& operator=(GammaDistribution&&) = default;

  /**
   * @brief Construct a new Gamma Distribution object
   *
   * @param shape     Shape parameter of gamma distribution
   * @param scale     Scale parameter of gamma distribution
   * @param min       Lower limit of a range for a distribution (minimum value of stochastic parameter)
   * @param max       Upper limit of a range for a distribution (maximum value of stochastic parameter)
   */
  GammaDistribution(double shape, double scale, double min, double max)
      : shape{shape}, scale{scale}, min{min}, max{max}
  {
    mean = shape * scale;
    standardDeviation = sqrt(shape) * scale;
  }

  /**
   * @brief Create a With Mean Sd object
   *
   * @param mean              Mean value
   * @param standardDeviation Standard deviation from mean value
   * @param min               Lower limit of a range for a distribution (minimum value of stochastic parameter)
   * @param max               Upper limit of a range for a distribution (maximum value of stochastic parameter)
   * @return GammaDistribution
   */
  static GammaDistribution CreateWithMeanSd(double mean, double standardDeviation, double min, double max)
  {
    double shape = mean * mean / standardDeviation / standardDeviation;
    double scale = standardDeviation * standardDeviation / mean;
    return GammaDistribution(shape, scale, min, max);
  }

private:
  double shape{0.0};
  double scale{0.0};
  double mean{0.0};
  double standardDeviation{0.0};
  double min{std::numeric_limits<double>::lowest()};
  double max{std::numeric_limits<double>::max()};

public:
  /**
   * @brief Function to check the equality of two GammaDistribution objects
   *
   * @param rhs Another GammaDistribution object
   * @return true if shape, scale, min and max of two GammaDistribution objects are equal
   */
  bool operator==(const GammaDistribution& rhs) const
  {
    return this == &rhs || (scm::common::DoubleEquality(shape, rhs.shape) &&
                            scm::common::DoubleEquality(scale, rhs.scale) &&
                            scm::common::DoubleEquality(min, rhs.min) &&
                            scm::common::DoubleEquality(max, rhs.max));
  }

  /**
   * @brief Function to check inequality of two GammaDistribution objects
   *
   * @param rhs Another GammaDistribution object
   * @return true if equality function returns false
   */
  bool operator!=(const GammaDistribution& rhs) const
  {
    return !operator==(rhs);
  }

  /**
   * @brief Get the Min
   *
   * @return minimum deviation
   */
  double GetMin() const
  {
    return min;
  }

  /**
   * @brief Get the Max
   *
   * @return maximum deviation
   */
  double GetMax() const
  {
    return max;
  }

  /**
   * @brief Get the Mean
   *
   * @return mean
   */
  double GetMean() const
  {
    return mean;
  }

  /**
   * @brief Get the Standard Deviation
   *
   * @return standard deviation
   */
  double GetStandardDeviation() const
  {
    return standardDeviation;
  }

  /**
   * @brief Get the Shape
   *
   * @return shape
   */
  double GetShape() const
  {
    return shape;
  }

  /**
   * @brief Get the Scale
   *
   * @return scale
   */
  double GetScale() const
  {
    return scale;
  }

  /**
   * @brief Set the Min
   *
   * @param min minimum deviation
   */
  void SetMin(double min)
  {
    this->min = min;
  }

  /**
   * @brief Set the Max
   *
   * @param max maximum deviation
   */
  void SetMax(double max)
  {
    this->max = max;
  }

  /**
   * @brief Set the Mean, shape and scale
   *
   * @param mean mean
   */
  void SetMean(double mean)
  {
    this->mean = mean;

    shape = mean * mean / standardDeviation / standardDeviation;
    scale = standardDeviation * standardDeviation / mean;
  }

  /**
   * @brief Set the Standard Deviation, shape, scale
   *
   * @param standardDeviation standard deviation
   */
  void SetStandardDeviation(double standardDeviation)
  {
    this->standardDeviation = standardDeviation;

    shape = mean * mean / standardDeviation / standardDeviation;
    scale = standardDeviation * standardDeviation / mean;
  }

  /**
   * @brief Set the Shape
   *
   * @param shape shape
   */
  void SetShape(double shape)
  {
    this->shape = shape;

    mean = shape * scale;
    standardDeviation = sqrt(shape) * scale;
  }

  /**
   * @brief Set the Scale
   *
   * @param scale scale
   */
  void SetScale(double scale)
  {
    this->scale = scale;

    mean = shape * scale;
    standardDeviation = sqrt(shape) * scale;
  }
};

/// @brief set StochasticDistribution as one of NormalDistribution, LogNormalDistribution, UniformDistribution, ExponentialDistribution, GammaDistribution
using StochasticDistribution = std::variant<NormalDistribution, LogNormalDistribution, UniformDistribution, ExponentialDistribution, GammaDistribution>;

}  // namespace scm::parameter
