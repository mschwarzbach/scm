/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

namespace scm
{
//! Type of a lane
enum class LaneType
{
  Undefined = 0,
  Shoulder,
  Border,
  Driving,
  Stop,
  None,
  Restricted,
  Parking,
  Median,
  Biking,
  Sidewalk,
  Curb,
  Exit,
  Entry,
  OnRamp,
  OffRamp,
  ConnectingRamp,
  Tram,
  RoadWorks,
  Bidirectional
};
}  // namespace scm