/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  SensorDriverScmDefinitions.h
//! @brief This file contains several classes for sensor driver purposes
//-----------------------------------------------------------------------------

#pragma once

#include <iostream>
#include <optional>
#include <set>
#include <string>
#include <utility>

#include "CommonLaneTypes.h"
#include "CommonTrafficLights.h"
#include "CommonTrafficSigns.h"
#include "LaneMarkings.h"
#include "LightState.h"
#include "ScmDefinitions.h"
#include "VehicleProperties.h"
#include "include/common/ScmEnums.h"

namespace scm
{

//! @brief container that allows uniform access to center element regardless of the number of regions
template <typename T, size_t N, std::enable_if_t<(N > 0), bool> = true>
struct Categorization
{
  //! @brief Array of regions
  std::array<T, N> regions;

  //! @brief SIZE
  static constexpr const size_t SIZE = N;
  // clang-format off

  //! @brief return the region at the given index
  //! @param i
  //! @return constexpr T&
  constexpr T& operator[](size_t i) { return regions[i]; }
  
  //! @brief return the const region at the given index
  //! @param i
  //! @return constexpr const T&
  constexpr const T& operator[](size_t i) const { return regions[i]; }

  //! @brief return the iterator to the first region
  //! @return constexpr typename std::array<T, N>::iterator
  constexpr typename std::array<T, N>::iterator begin() { return regions.begin(); }
  //! @brief return the const iterator to the first region 
  //! @return constexpr typename std::array<T, N>::const_iterator
  constexpr typename std::array<T, N>::const_iterator begin() const { return regions.begin(); }
  //! @brief return the iterator to the last region 
  //! @return constexpr typename std::array<T, N>::iterator
  constexpr typename std::array<T, N>::iterator end() { return regions.end(); }
  //! @brief return the const iterator to the last region 
  //! @return constexpr typename std::array<T, N>::const_iterator
  constexpr typename std::array<T, N>::const_iterator end() const { return regions.end(); }

  //! @brief return the reverse iterator to the first region
  //! @return constexpr typename std::array<T, N>::reverse_iterator
  constexpr typename std::array<T, N>::reverse_iterator rbegin() { return regions.rbegin(); }
  //! @brief return the const reverse iterator to the first region
  //! @return constexpr typename std::array<T, N>::const_reverse_iterator
  constexpr typename std::array<T, N>::const_reverse_iterator rbegin() const { return regions.rbegin(); }
  //! @brief return the reverse iterator to the last region
  //! @return constexpr typename std::array<T, N>::reverse_iterator
  constexpr typename std::array<T, N>::reverse_iterator rend() { return regions.rend(); }
  //! @brief return the const reverse iterator to the last region
  //! @return constexpr typename std::array<T, N>::const_reverse_iterator
  constexpr typename std::array<T, N>::const_reverse_iterator rend() const { return regions.rend(); }

  //! @brief return the size 
  //! @return constexpr size_t
  constexpr size_t size() const { return N; }

  //! @brief returns the middle elements of the array of regions
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && (N % 2), bool> = true>
  constexpr T& Close() { return regions[N >> 1]; }
  
  //! @brief returns the const middle elements of the array of regions
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && (N % 2), bool> = true>
  constexpr const T& Close() const { return regions[N >> 1]; }

  // clang-format on
};

//! @brief Array of regions sorted from right to left
//! @tparam T Item of which there will be one per region
//! @tparam N Number of regions
template <typename T, size_t N>
struct LateralCategorization : Categorization<T, N>
{
  // clang-format off

  //! @brief Struct definition for the description of a Index
  struct Index {
    Index() = delete;
    //! @brief the index to element two steps removed to the rigth from the center 
    static constexpr const size_t FAR_RIGHT{(N > 3) ? ((N >> 1) - 2) : N};
    //! @brief the index to element one step removed to the rigth from the center 
    static constexpr const size_t RIGHT{(N > 1) ? ((N >> 1) - 1) : N};
    //! @brief the index to center element 
    static constexpr const size_t CLOSE{(N % 2) ? (N >> 1) : N};
    //! @brief the index to element one step removed to the left from the center 
    static constexpr const size_t LEFT{(N > 1) ? (((N - 1) >> 1) + 1) : N};
    //! @brief the index to element two steps removed to the left from the center 
    static constexpr const size_t FAR_LEFT{(N > 3) ? (((N - 1) >> 1) + 2) : N};

  };

  inline static const std::map<GazeState, const size_t> gazeStateIndexMap{
        {GazeState::RIGHTRIGHT_REAR, Index::FAR_RIGHT},
        {GazeState::RIGHTRIGHT_SIDE, Index::FAR_RIGHT},
        {GazeState::RIGHTRIGHT_FRONT, Index::FAR_RIGHT},
        {GazeState::RIGHT_REAR, Index::RIGHT},
        {GazeState::RIGHT_SIDE, Index::RIGHT},
        {GazeState::RIGHT_FRONT, Index::RIGHT},
        {GazeState::RIGHT_FRONT_FAR, Index::RIGHT},
        {GazeState::EGO_REAR, Index::CLOSE},
        {GazeState::EGO_FRONT, Index::CLOSE},
        {GazeState::EGO_FRONT_FAR, Index::CLOSE},
        {GazeState::LEFT_REAR, Index::LEFT},
        {GazeState::LEFT_SIDE, Index::LEFT},
        {GazeState::LEFT_FRONT, Index::LEFT},
        {GazeState::LEFT_FRONT_FAR, Index::LEFT},
        {GazeState::LEFTLEFT_REAR, Index::FAR_LEFT},
        {GazeState::LEFTLEFT_SIDE, Index::FAR_LEFT},
        {GazeState::LEFTLEFT_FRONT, Index::FAR_LEFT} //
      };

   inline static const std::map<AreaOfInterest, const size_t> areaOfInterestIndexMap{
        {AreaOfInterest::RIGHTRIGHT_REAR, Index::FAR_RIGHT},
        {AreaOfInterest::RIGHTRIGHT_SIDE, Index::FAR_RIGHT},
        {AreaOfInterest::RIGHTRIGHT_FRONT, Index::FAR_RIGHT},
        {AreaOfInterest::RIGHT_REAR, Index::RIGHT},
        {AreaOfInterest::RIGHT_SIDE, Index::RIGHT},
        {AreaOfInterest::RIGHT_FRONT, Index::RIGHT},
        {AreaOfInterest::RIGHT_FRONT_FAR, Index::RIGHT},
        {AreaOfInterest::EGO_REAR, Index::CLOSE},
        {AreaOfInterest::EGO_FRONT, Index::CLOSE},
        {AreaOfInterest::EGO_FRONT_FAR, Index::CLOSE},
        {AreaOfInterest::LEFT_REAR, Index::LEFT},
        {AreaOfInterest::LEFT_SIDE, Index::LEFT},
        {AreaOfInterest::LEFT_FRONT, Index::LEFT},
        {AreaOfInterest::LEFT_FRONT_FAR, Index::LEFT},
        {AreaOfInterest::LEFTLEFT_REAR, Index::FAR_LEFT},
        {AreaOfInterest::LEFTLEFT_SIDE, Index::FAR_LEFT},
        {AreaOfInterest::LEFTLEFT_FRONT, Index::FAR_LEFT}  //
    };

  //! @brief return the element two indices to the right from the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_RIGHT != N, bool> = true>
  constexpr T& FarRight() { return this->regions[Index::FAR_RIGHT]; }

  //! @brief return the const element two indices to the right from the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_RIGHT != N, bool> = true>
  constexpr const T& FarRight() const { return this->regions[Index::FAR_RIGHT]; }

  //! @brief return the element one index to the right from the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::RIGHT != N, bool> = true>
  constexpr T& Right() { return this->regions[Index::RIGHT]; }

  //! @brief return the const element one index to the right from the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::RIGHT != N, bool> = true>
  constexpr const T& Right() const { return this->regions[Index::RIGHT]; }

  //! @brief return the element one index to the left from the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::LEFT != N, bool> = true>
  constexpr T& Left() { return this->regions[Index::LEFT]; }

  //! @brief return the const element one index to the left from the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::LEFT != N, bool> = true>
  constexpr const T& Left() const { return this->regions[Index::LEFT]; }

  //! @brief return the element two indices to the left from the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_LEFT != N, bool> = true>
  constexpr T& FarLeft() { return this->regions[Index::FAR_LEFT]; }

  //! @brief return the const element two indices to the left from the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_LEFT != N, bool> = true>
  constexpr const T& FarLeft() const { return this->regions[Index::FAR_LEFT]; }

  //! @brief return the element from GazeState
  //! @param state
  //! @return size_t 
  size_t GetIndexFrom(GazeState state) const {
      const auto it{gazeStateIndexMap.find(state)};
      return it != gazeStateIndexMap.end() ? it->second : N;
  }

  //! @brief return the const element from GazeState
  //! @param state
  //! @return const T*
  const T* at(GazeState state) const {
    const size_t i{GetIndexFrom(state)};
    return i < N ? &this->regions[i] : nullptr;
  }

  //! @brief return the element from GazeState
  //! @param state
  //! @return T*
  T* at(GazeState state) {
    return const_cast<T*>(std::as_const(*this).at(state));
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return size_t
  size_t GetIndexFrom(AreaOfInterest area) const {
    const auto it{areaOfInterestIndexMap.find(area)};
    return it != areaOfInterestIndexMap.end() ? it->second : N;
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return const T*
  const T* at(AreaOfInterest area) const {
    const size_t i{GetIndexFrom(area)};
    return i < N ? &this->regions[i] : nullptr;
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return T*, if not relevant AreaOfInterest return nullptr
  T* at(AreaOfInterest area) { return const_cast<T*>(std::as_const(*this).at(area)); }

  // clang-format on
};

//! @brief Array of regions sorted from back to front
//! @tparam T
//! @tparam N Number of regions
template <typename T, size_t N>
struct LongitudinalCategorization : Categorization<T, N>
{
  // clang-format off

  //! @brief Struct definition for the description of a Index
  struct Index {
    Index() = delete;

    //! @brief the index to element two steps behind the center
    static constexpr const size_t FAR_BEHIND{(N > 3) ? ((N >> 1) - 2) : N};
     //! @brief the index to element one step behind the center
    static constexpr const size_t BEHIND{(N > 1) ? ((N >> 1) - 1) : N};
     //! @brief the index to the center element
    static constexpr const size_t CLOSE{(N % 2) ? (N >> 1) : N};
     //! @brief the index to element one step ahead of the center
    static constexpr const size_t AHEAD{(N > 1) ? (((N-1) >> 1) + 1) : N};
    //! @brief the index to element two steps ahead of the center
    static constexpr const size_t FAR_AHEAD{(N > 3) ? (((N-1) >> 1) + 2) : N};
  };

  inline static const std::map<GazeState, const size_t> gazeStateIndexMap{
      {GazeState::RIGHTRIGHT_REAR, Index::BEHIND},
      {GazeState::RIGHT_REAR, Index::BEHIND},
      {GazeState::EGO_REAR, Index::BEHIND},
      {GazeState::LEFT_REAR, Index::BEHIND},
      {GazeState::LEFTLEFT_REAR, Index::BEHIND},
      {GazeState::RIGHTRIGHT_SIDE, Index::CLOSE},
      {GazeState::RIGHT_SIDE, Index::CLOSE},
      {GazeState::LEFT_SIDE, Index::CLOSE},
      {GazeState::LEFTLEFT_SIDE, Index::CLOSE},
      {GazeState::RIGHTRIGHT_FRONT, Index::AHEAD},
      {GazeState::RIGHT_FRONT, Index::AHEAD},
      {GazeState::EGO_FRONT, Index::AHEAD},
      {GazeState::LEFT_FRONT, Index::AHEAD},
      {GazeState::LEFTLEFT_FRONT, Index::AHEAD},
      {GazeState::RIGHT_FRONT_FAR, Index::FAR_AHEAD},
      {GazeState::EGO_FRONT_FAR, Index::FAR_AHEAD},
      {GazeState::LEFT_FRONT_FAR, Index::FAR_AHEAD} //
    };

  inline static const std::map<AreaOfInterest, const size_t> areaOfInterestIndexMap{
      {AreaOfInterest::RIGHTRIGHT_REAR, Index::BEHIND},
      {AreaOfInterest::RIGHT_REAR, Index::BEHIND},
      {AreaOfInterest::EGO_REAR, Index::BEHIND},
      {AreaOfInterest::LEFT_REAR, Index::BEHIND},
      {AreaOfInterest::LEFTLEFT_REAR, Index::BEHIND},
      {AreaOfInterest::RIGHTRIGHT_SIDE, Index::CLOSE},
      {AreaOfInterest::RIGHT_SIDE, Index::CLOSE},
      {AreaOfInterest::LEFT_SIDE, Index::CLOSE},
      {AreaOfInterest::LEFTLEFT_SIDE, Index::CLOSE},
      {AreaOfInterest::RIGHTRIGHT_FRONT, Index::AHEAD},
      {AreaOfInterest::RIGHT_FRONT, Index::AHEAD},
      {AreaOfInterest::EGO_FRONT, Index::AHEAD},
      {AreaOfInterest::LEFT_FRONT, Index::AHEAD},
      {AreaOfInterest::LEFTLEFT_FRONT, Index::AHEAD},
      {AreaOfInterest::RIGHT_FRONT_FAR, Index::FAR_AHEAD},
      {AreaOfInterest::EGO_FRONT_FAR, Index::FAR_AHEAD},
      {AreaOfInterest::LEFT_FRONT_FAR, Index::FAR_AHEAD},  //
  };

  //! @brief return the element two indices behind the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_BEHIND != N, bool> = true>
  constexpr T& FarBehind() { return this->regions[Index::FAR_BEHIND]; }

  //! @brief return the const element two indices behind the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_BEHIND != N, bool> = true>
  constexpr const T& FarBehind() const { return this->regions[Index::FAR_BEHIND]; }

  //! @brief return the element one index behind the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::BEHIND != N, bool> = true>
  constexpr T& Behind() { return this->regions[Index::BEHIND]; }

  //! @brief return the const element one index behind the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::BEHIND != N, bool> = true>
  constexpr const T& Behind() const { return this->regions[Index::BEHIND]; }

  //! @brief return the element one index ahead of the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::AHEAD != N, bool> = true>
  constexpr T& Ahead() { return this->regions[Index::AHEAD]; }

  //! @brief return the const element one index ahead of the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::AHEAD != N, bool> = true>
  constexpr const T& Ahead() const { return this->regions[Index::AHEAD]; }

  //! @brief return the element element two indices ahead of the center
  //! @return constexpr T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_AHEAD != N, bool> = true>
  constexpr T& FarAhead() { return this->regions[Index::FAR_AHEAD]; }

  //! @brief return the const element element two indices ahead of the center
  //! @return constexpr const T&
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && Index::FAR_AHEAD != N, bool> = true>
  constexpr const T& FarAhead() const { return this->regions[Index::FAR_AHEAD]; }

  //! @brief return the element from GazeState
  //! @param state
  //! @return size_t 
  size_t GetIndexFrom(GazeState state) const {
    const auto it{gazeStateIndexMap.find(state)};
    return it != gazeStateIndexMap.end() ? it->second : N;
  }
  //! @brief return the element from GazeState
  //! @param state
  //! @return T* 
  const T* at(GazeState state) const {
    const size_t i{GetIndexFrom(state)};
    return i != N ? &this->regions[i] : nullptr;
  }

  //! @brief return the element from GazeState
  //! @param state
  //! @return T*, if not relevant GazeState return nullptr
  T* at(GazeState state) { return const_cast<T*>(std::as_const(*this).at(state)); }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return size_t 
  size_t GetIndexFrom(AreaOfInterest area) const {
    const auto it{areaOfInterestIndexMap.find(area)};
    return it != areaOfInterestIndexMap.end() ? it->second : N;
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return T* 
  const T* at(AreaOfInterest area) const {
    const size_t i{GetIndexFrom(area)};
    return i < N ? &this->regions[i] : nullptr;
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return T*, if not relevant AreaOfInterest return nullptr
  T* at(AreaOfInterest area) { return const_cast<T*>(std::as_const(*this).at(area)); }
  // clang-format on
};

//! @brief a matrix of regions
//! @tparam T
//! @tparam Rows fixed number of rows in the matrix
//! @tparam Columns fixed number of columns in the matrix
template <typename T, size_t Rows, size_t Columns = Rows>
struct Grid : LongitudinalCategorization<LateralCategorization<T, Columns>, Rows>
{
  //! @brief return the const element from AreaOfInterest
  //! @param area
  //! @return const T*, if not relevant AreaOfInterest return nullptr
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && (Rows >= 5 && Columns >= 5), bool> = true>
  const T* FromAreaOfInterest(AreaOfInterest area) const
  {
    auto row = this->GetIndexFrom(area);
    if (row == Rows) return nullptr;

    auto& arrayTest = this->regions[row];

    auto column = arrayTest.GetIndexFrom(area);
    if (column == Columns) return nullptr;

    return &arrayTest[column];
  }

  //! @brief return the element from AreaOfInterest
  //! @param area
  //! @return T*, if not relevant AreaOfInterest return nullptr
  template <typename U = T, std::enable_if_t<std::is_same_v<T, U> && (Rows >= 5 && Columns >= 5), bool> = true>
  T* FromAreaOfInterest(AreaOfInterest area)
  {
    return const_cast<T*>(std::as_const(*this).FromAreaOfInterest(area));
  }
};

//! @brief Struct that stores highway exit related information
struct HighwayExitInformation
{
  //! @brief distanceToStartOfExit
  units::length::meter_t distanceToStartOfExit{ScmDefinitions::DEFAULT_VALUE};
  //! @brief distanceToEndOfExit
  units::length::meter_t distanceToEndOfExit{ScmDefinitions::DEFAULT_VALUE};
  //! @brief numberOfLanesToCross
  int relativeLaneIdForJunctionIngoing{-1};
};
}  // namespace scm

//! @brief Obstruction according to the logic used by scm
struct ObstructionScm
{
public:
  //! @brief True, if obstruction could be calculated
  bool valid{false};
  //! @brief Lateral distance required to pass the object on the left. Negative when already to objects left.
  units::length::meter_t left{0.};
  //! @brief Lateral distance required to pass the object on the right. Negative when already to objects left.
  units::length::meter_t right{0.};
  //! @brief Lateral distance of the mainLaneLocator
  units::length::meter_t mainLaneLocator{0.};
  //! @brief True, if object obstructs
  bool isOverlapping{false};

  //! @brief specialized constructor for ObstructionScm
  //! @param left
  //! @param right
  //! @param mainLaneLocator
  ObstructionScm(units::length::meter_t left, units::length::meter_t right, units::length::meter_t mainLaneLocator)
      : valid{true},
        left{left},
        right{right},
        mainLaneLocator(mainLaneLocator),
        isOverlapping{left > 0._m && right > 0._m}
  {
  }

  //! @brief Invalid longitudinal obstruction object
  //! @return empty ObstructionLongitudinal
  static ObstructionScm Invalid()
  {
    return {};
  }

  //! @brief Default for no opponent
  //! @return default ObstructionScm with zero values
  static ObstructionScm NoOpponent()
  {
    return ObstructionScm(0._m, 0._m, 0._m);
  }

  //! @brief Operator== overload to simplify is all values in ObstructionScm == obs
  //! @param obs
  //! @return true when left, right, isOverlapping, valid and mainLaneLocator == obs
  bool operator==(const ObstructionScm obs) const
  {
    return obs.left == left && obs.right == right && obs.isOverlapping == isOverlapping && obs.valid == valid &&
           obs.mainLaneLocator == mainLaneLocator;
  }

private:
  ObstructionScm() = default;
};

//! @brief Describes the longitudinal obstruction of an opposing object within the driving lanes
class ObstructionLongitudinal
{
public:
  //! @brief True, if longitudinal obstruction could be calculated
  bool valid{false};
  //! @brief How far do I need to drive to be completely in front of the other object
  units::length::meter_t front{0._m};
  //! @brief How far do I need to drive to be completely behind the other object
  units::length::meter_t rear{0._m};
  //! @brief Longitudinal distance of the mainLaneLocator
  units::length::meter_t mainLaneLocator{0._m};
  //! @brief True, if object obstructs
  bool isOverlapping{false};

  ObstructionLongitudinal() = default;
  //! @brief specialized constructor for ObstructionLongitudinal
  //! @param front
  //! @param rear
  //! @param mainLaneLocator
  ObstructionLongitudinal(units::length::meter_t front, units::length::meter_t rear, units::length::meter_t mainLaneLocator)
      : valid{true},
        front{front},
        rear{rear},
        mainLaneLocator{mainLaneLocator},
        isOverlapping{front > 0._m && rear > 0._m}
  {
  }

  //! @brief Invalid longitudinal obstruction object
  //! @return empty ObstructionLongitudinal
  static ObstructionLongitudinal Invalid()
  {
    return {};
  }

  //! @brief Default for no opponent
  //! @return default ObstructionScm with zero values
  static ObstructionLongitudinal NoOpponent()
  {
    return ObstructionLongitudinal(0._m, 0._m, 0._m);
  }

  units::length::meter_t GetNetDistance() const;
};

//! @brief This struct is used to transport data of an crossing object as seen by the driver
struct ProjectedSpacialDimensions  // TODO A struct full of optionals is nonsense!
{
  ProjectedSpacialDimensions() = default;
  //! @brief specialized constructor for ProjectedSpacialDimensions
  //! @param heightProjected
  //! @param widthProjected
  //! @param widthProjectedLeft
  //! @param widthProjectedRight
  ProjectedSpacialDimensions(units::length::meter_t heightProjected, units::length::meter_t widthProjected, units::length::meter_t widthProjectedLeft, units::length::meter_t widthProjectedRight)
      : heightProjected(heightProjected),
        widthProjected(widthProjected),
        widthProjectedLeft(widthProjectedLeft),
        widthProjectedRight(widthProjectedRight)
  {
  }

  //! @brief Projected height of object due to roll angle [m]
  units::length::meter_t heightProjected{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Projected width of object due to roll angle [m]
  units::length::meter_t widthProjected{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Projected width of object to the left of the main locate point due to roll angle [m]
  units::length::meter_t widthProjectedLeft{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Projected width of object to the right of the main locate point due to roll angle [m]
  units::length::meter_t widthProjectedRight{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief Struct definition for the description of a OrientationRelativeToDriver
struct OrientationRelativeToDriver
{
  //! @brief rearRight
  units::angle::radian_t rearRight{ScmDefinitions::DEFAULT_VALUE};
  //! @brief rearLeft
  units::angle::radian_t rearLeft{ScmDefinitions::DEFAULT_VALUE};
  //! @brief frontLeft
  units::angle::radian_t frontLeft{ScmDefinitions::DEFAULT_VALUE};
  //! @brief frontRight
  units::angle::radian_t frontRight{ScmDefinitions::DEFAULT_VALUE};
  //! @brief frontCenter
  units::angle::radian_t frontCenter{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief This struct is used to transport data of an object as seen by the driver
struct ObjectInformationSCM
{
  //! @brief Id of the object (default if not existing)
  int id{-1};
  //! @brief false if there is no object in this position
  bool exist{false};
  //! @brief true if stationary object, false if agent
  bool isStatic{false};
  //! @brief Absolute velocity of the agent (default if object is not an agent)
  units::velocity::meters_per_second_t absoluteVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Acceleration of the agent (default if object is not an agent)
  units::acceleration::meters_per_second_squared_t acceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Heading relative to the street (default if not existing)
  double heading{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Length of object (default if not existing)
  units::length::meter_t length{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Width of object (default if not existing)
  units::length::meter_t width{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Height of object (default if not existing)
  units::length::meter_t height{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Relative distance along the road (i.e in direction s) between own agent and object (default if not existing)
  units::length::meter_t relativeLongitudinalDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Relative distance at right angle to the road (i.e. in direction t) between own agent and object (default if not existing)
  units::length::meter_t relativeLateralDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Distance between the left front point and the left boundary of the lane it is in m.
  units::length::meter_t distanceToLaneBoundaryLeft{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Distance between the right front point and the right boundary of the lane it is in m.
  units::length::meter_t distanceToLaneBoundaryRight{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Wether this object has collided with another object
  bool collision{false};
  //! @brief Wether the brake lights of the agent are on (false if not an agent)
  bool brakeLightsActive{false};
  //! @brief State of the indicator of the agent (default if not an agent)
  scm::LightState::Indicator indicatorState{scm::LightState::Indicator::Off};
  //! @brief Obstruction information between object and own agent, i.e. information about relative lateral distances (left and right)
  ObstructionScm obstruction{ObstructionScm::Invalid()};
  //! @brief t-coordinate
  units::length::meter_t lateralPositionInLane{};
  //! @brief Velocity in direction of s
  units::velocity::meters_per_second_t longitudinalVelocity{};
  //! @brief Velocity in direction of t
  units::velocity::meters_per_second_t lateralVelocity{};
  //! @brief Relative orientation to driver
  OrientationRelativeToDriver orientationRelativeToDriver;
  //! @brief Information about Vehicle Type
  scm::common::VehicleClass vehicleClassification{scm::common::VehicleClass::kInvalid};
  //! @brief Obstruction information between object and own agent in longitudinal direction. Positive obstruction is the distance to completely pass the object. Negative obstruction is the distance between agent and object.
  ObstructionLongitudinal longitudinalObstruction{ObstructionLongitudinal::Invalid()};
  //! @brief projectedDimensions
  ProjectedSpacialDimensions projectedDimensions;
  //! @brief Horizontal angular position of object's projection on driver's retina in relation to view axis in rad
  units::angle::radian_t opticalAnglePositionHorizontal{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Horizontal angular size of object's projection on driver's retina in rad
  units::angle::radian_t opticalAngleSizeHorizontal{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Current threshold above which a change in the tangent of the angle between the driver and to object is regarded as a lane change of the object
  double thresholdLooming{ScmDefinitions::DEFAULT_VALUE};
  //! @brief lanetype
  scm::LaneType lanetype;  // Todo This does not belong here. Create derived class for virtual agents that need this information!

  //! @brief Distance between EgoFrontBumper to surroundingVehicle rearAxle
  units::length::meter_t longitudinalDistanceFrontBumperToRearAxle{ScmDefinitions::DEFAULT_VALUE};

  //! @brief Longitudinal distance to Ego in global coordinates
  units::length::meter_t relativeX{ScmDefinitions::DEFAULT_VALUE};

  //! @brief Lateral distance to Ego in global coordinates
  units::length::meter_t relativeY{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief This struct is used to transport data of a lane as seen be the driver
struct LaneInformationTrafficRulesSCM
{
  //! @brief Vector of all traffic signs valid for this lane inside the visibility distance
  std::vector<scm::CommonTrafficSign::Entity> trafficSigns;
  //! @brief Vector of all traffic lights valid for this lane inside the visibility distance
  std::vector<scm::CommonTrafficLight::Entity> trafficLights;
  //! @brief List of lane markings on the left side of lane
  std::vector<scm::LaneMarking::Entity> laneMarkingsLeft{scm::LaneMarking::Entity{}};
  //! @brief List of lane markings on the right side of lane
  std::vector<scm::LaneMarking::Entity> laneMarkingsRight{scm::LaneMarking::Entity{}};
};

//! @brief This struct contains infrastructure information of surrounding lanes that are used to establish traffic rules
using TrafficRuleInformationSCM = scm::LateralCategorization<LaneInformationTrafficRulesSCM, 5>;

//! @brief This struct is used to transport data of a lane as seen be the driver
struct LaneInformationGeometrySCM
{
  //! @brief Whether there is a lane on this position
  bool exists{false};
  //! @brief Curvature at current s position (default if not existing)
  units::curvature::inverse_meter_t curvature{-999.0_i_m};
  //! @brief Width at current s position (default if not existing)
  units::length::meter_t width{-999.0_m};
  //! @brief Distance from current position to the end of the lane or infinity if the end is farther away than the visibility distance
  units::length::meter_t distanceToEndOfLane{-999.0};

  //! @brief Curvature at previewDistane (default if not existing)
  units::curvature::inverse_meter_t curvatureInPreviewDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Type of the lane (default if not existing)
  scm::LaneType laneType{scm::LaneType::Undefined};
  //! @brief Distance from current position to the end of the lane or infinity if the end is farther away than the visibility distance fo all lanetypes excep None & Undefined
  units::length::meter_t distanceToEndOfLaneDuringEmergency{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Type of the lane Successor
  std::optional<scm::LaneType> laneTypeSuccessor;
};

//! @brief This struct contains infrastructure information of surrounding lanes that describe its geometric features
struct GeometryInformationSCM : scm::LateralCategorization<LaneInformationGeometrySCM, 5>
{
  //! @brief Id of the current road the agent is driving on
  std::string currentRoadId{"-1"};
  //! @brief Current maximum visibility distance as specified by the world
  units::length::meter_t visibilityDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Number of lane at the current position
  int numberOfLanes{-1};
  //! @brief Distance from current position to the end of the next (current or coming) exit lane
  units::length::meter_t distanceToEndOfNextExit{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Distance from current position to the end of the next (current or coming) exit lane
  units::length::meter_t distanceToStartOfNextExit{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Id the agent needs to change to in order to access the connector of a junction
  std::vector<int> relativeLaneIdForJunctionIngoing{};

  //! @brief return one element of the given lane
  //! @param lane
  //! @return const LaneInformationGeometrySCM&
  const LaneInformationGeometrySCM& FromLane(SurroundingLane lane) const
  {
    if (lane == SurroundingLane::RIGHT)
    {
      return Right();
    }
    else if (lane == SurroundingLane::EGO)
    {
      return Close();
    }
    else
    {
      return Left();
    }
  }

  //! @brief return one element of the given lane
  //! @param lane
  //! @return LaneInformationGeometrySCM&
  LaneInformationGeometrySCM& FromLane(SurroundingLane lane)
  {
    return const_cast<LaneInformationGeometrySCM&>(std::as_const(*this).FromLane(lane));
  }
};

//! @brief Additional data concerning submodule GazeFollower in AlgorihmScmMonolithisch
struct GazeFollowerInformation
{
  //! @brief Path of the file containing the GazeFollower time series
  std::string gazeFilePath{""};
  //! @brief Full name of the file containing the GazeFollower time series under gazeFilePath
  std::string gazeFileName{""};
};

//! @brief Data about the lane to left (in driving direction) of the mainLane
struct OwnVehicleInformationSCM
{
  //! @brief Absolute velocity of agent
  units::velocity::meters_per_second_t absoluteVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Acceleration of agent
  units::acceleration::meters_per_second_squared_t acceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief t-coordinate
  units::length::meter_t lateralPosition{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Heading relative to lane
  units::angle::radian_t heading{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Angle of the steering wheel
  units::angle::radian_t steeringWheelAngle{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Distance between the left front point and the left boundary of the lane it is in.
  units::length::meter_t distanceToLaneBoundaryLeft{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Distance between the right front point and the right boundary of the lane it is in.
  units::length::meter_t distanceToLaneBoundaryRight{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Whether this agent has collided with another object
  bool collision{false};

  //! @brief Id of the agent
  int id{-1};
  //! @brief Id of the mainLane, i.e. where the middle of the front is
  int mainLaneId{-999};
  //! @brief Velocity in direction of s
  units::velocity::meters_per_second_t longitudinalVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Velocity in direction of t
  units::velocity::meters_per_second_t lateralVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief lateralVelocityFrontAxle in direction of t
  units::velocity::meters_per_second_t lateralVelocityFrontAxle{ScmDefinitions::DEFAULT_VALUE};
  //! @brief lateralPositionFrontAxle in direction of t
  units::length::meter_t lateralPositionFrontAxle{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Longitudinal position from start of road [m]
  units::length::meter_t longitudinalPosition{ScmDefinitions::DEFAULT_VALUE};

  //! @brief Horizontal angle of interior mirror relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t InteriorMirrorOrientationRelativeToDriver{10. / 57.3};  // Dummy values until defined in vehicles catalogue
  //! @brief Horizontal angle of left mirror relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t LeftMirrorOrientationRelativeToDriver{50. / 57.3};  // Dummy values until defined in vehicles catalogue
  //! @brief Horizontal angle of right mirror relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t RightMirrorOrientationRelativeToDriver{-50. / 57.3};  // Dummy values until defined in vehicles catalogue
  //! @brief Horizontal angle of HUD relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t HeadUpDisplayOrientationRelativeToDriver{-22. / 57.3};  // Dummy values until defined in vehicles catalogue
  //! @brief Horizontal angle of instrument dash board relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t InstrumentClusterOrientationRelativeToDriver{-22. / 57.3};  // Dummy values until defined in vehicles catalogue
  //! @brief Horizontal angle of infotainment system relative to the drivers head[ position from start of road [rad]
  units::angle::radian_t InfotainmentOrientationRelativeToDriver{-22. / 57.3};  // Dummy values until defined in vehicles catalogue

  //! @brief Trigger to start a new forced lane change
  LaneChangeAction forcedLaneChangeStartTrigger{LaneChangeAction::None};
  //! @brief Indicator if the current lane change was forced externally
  LaneChangeAction currentLaneChangeForced{LaneChangeAction::None};
  //! @brief Id the agent needs to change to in order to access the connector of a junction
  std::vector<int> relativeLaneIdForJunctionIngoing{};
  //! @brief Additional data concerning submodule GazeFollower in AlgorithmScmMonolithisch
  GazeFollowerInformation gazeFollowerInformation;
  //! @brief curvature of own Connector
  double ownConnectorCurvature{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Is the driver supposed to take the next highway exit or not?
  bool takeNextExit{false};
  //! @brief projectedDimensions
  ProjectedSpacialDimensions projectedDimensions;
  //! @brief distributionForHighwayExit
  double distributionForHighwayExit{ScmDefinitions::DEFAULT_VALUE};
  //! @brief highwayExitInformation
  std::optional<scm::HighwayExitInformation> highwayExitInformation;

  //! @brief Absolute ego position x in global coordinates
  units::length::meter_t posX{ScmDefinitions::DEFAULT_VALUE};
  //! @brief Absolute ego position y in global coordinates
  units::length::meter_t posY{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief Data about the objects in the different areas of interest going in the same direction as the current agent
struct OngoingTrafficObjectsSCM : scm::Grid<std::vector<ObjectInformationSCM>, 5>
{
  //! @brief allObjects
  std::set<ObjectInformationSCM*> allObjects;
};

//! @brief A type defined structure for VirtualAgentsSCM
using VirtualAgentsSCM = std::vector<ObjectInformationSCM>;

//! @brief Data about all the objects in the different areas of interest
struct SurroundingObjectsSCM
{
  //! @brief ongoingTraffic
  OngoingTrafficObjectsSCM ongoingTraffic;
  //! @brief virtualTraffic
  VirtualAgentsSCM virtualTraffic;
};