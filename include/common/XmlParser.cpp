/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2018 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "XmlParser.h"

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <optional>
#include <sstream>

#include "CommonHelper.h"

namespace scm::common
{

std::string toString(const unsigned char *input)
{
  if (input == nullptr)
  {
    return {};
  }
  return reinterpret_cast<const char *>(input);  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
}

const unsigned char *toXmlChar(const std::string &input)
{
  return reinterpret_cast<const unsigned char *>(input.c_str());  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
}

xmlNodePtr GetFirstChildElement(xmlNodePtr rootElement, const std::string &tag)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        return node;
      }
    }
  }
  return nullptr;
}

xmlNodePtr GetLastChildElement(xmlNodePtr rootElement, const std::string &tag)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->last; node; node = node->prev)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        return node;
      }
    }
  }

  return nullptr;
}

xmlNodePtr GetFirstChild(xmlNodePtr rootElement)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE)
      {
        return node;
      }
    }
  }
  return nullptr;
}

bool ParseString(xmlNodePtr rootElement, const std::string &tag, std::string &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          result = toString(textNode->content);
        }
        return true;
      }
    }
  }

  return false;
}

bool ParseCurrentInt(xmlNodePtr currentElement, int &result)
{
  if (currentElement && currentElement->type == XML_ELEMENT_NODE)
  {
    xmlNodePtr textNode = currentElement->children;
    if (textNode && textNode->type == XML_TEXT_NODE)
    {
      try
      {
        result = std::stoi(toString(textNode->content));
      }
      catch (...)
      {
        return false;
      }
    }
  }
  return true;
}

bool ParseDouble(xmlNodePtr rootElement, const std::string &tag, double &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          try
          {
            result = std::stod(toString(textNode->content));
          }
          catch (...)
          {
            return false;
          }
        }
        return true;
      }
    }
  }

  return false;
}

bool ParseDoubleVector(xmlNodePtr rootElement, const std::string &tag, std::vector<double> &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          try
          {
            std::stringstream valueStream(toString(textNode->content));

            double item{};
            while (valueStream >> item)
            {
              result.push_back(item);

              if (valueStream.peek() == ',')
              {
                valueStream.ignore();
              }
            }
          }
          catch (...)
          {
            return false;
          }
        }
        return true;
      }
    }
  }

  return false;
}

bool ParseInt(xmlNodePtr rootElement, const std::string &tag, int &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          try
          {
            result = std::stoi(toString(textNode->content));
          }
          catch (...)
          {
            return false;
          }
          return true;
        }
      }
    }
  }

  return false;
}

bool ParseULong(xmlNodePtr rootElement, const std::string &tag, std::uint64_t &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          try
          {
            result = std::stoul(toString(textNode->content));
          }
          catch (...)
          {
            return false;
          }
        }
        return true;
      }
    }
  }

  return false;
}

bool ParseBool(xmlNodePtr rootElement, const std::string &tag, bool &result)
{
  if (rootElement)
  {
    for (xmlNodePtr node = rootElement->children; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, toXmlChar(tag)))
      {
        xmlNodePtr textNode = node->children;
        if (textNode && textNode->type == XML_TEXT_NODE)
        {
          try
          {
            std::string value = toString(textNode->content);
            std::transform(value.begin(), value.end(), value.begin(), ::tolower);
            std::istringstream is(value);
            is >> std::boolalpha >> result;
          }
          catch (...)
          {
            return false;
          }
        }
        return true;
      }
    }
  }

  return false;
}

template <typename T>
bool Parse(xmlNodePtr, const std::string &, T &)
{
  throw std::runtime_error("xmlParser::Parse<T> not implemented yet");
}

//! @brief Template specialization of Parse for std::string
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the std::string where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, std::string &result)
{
  return ParseString(rootElement, tag, result);
}

//! @brief Template specialization of Parse for double
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the double where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, double &result)
{
  return ParseDouble(rootElement, tag, result);
}

//! @brief Template specialization of Parse for vector of doubles
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the std::vector<double> where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, std::vector<double> &result)
{
  return ParseDoubleVector(rootElement, tag, result);
}

//! @brief Template specialization of Parse for integer
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the int where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, int &result)
{
  return ParseInt(rootElement, tag, result);
}

//! @brief Template specialization of Parse for 64-bit unsigned integer
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the std::uint64_t where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, std::uint64_t &result)
{
  return ParseULong(rootElement, tag, result);
}

//! @brief Template specialization of Parse for boolean
//!
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       The reference to the bool where the parsed value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool Parse(xmlNodePtr rootElement, const std::string &tag, bool &result)
{
  return ParseBool(rootElement, tag, result);
}

//! @brief Template specialization of ParseAttribute for boolean
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the bool where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<bool>(xmlNodePtr element, const std::string &attributeName, bool &result)
{
  return ParseAttributeBool(element, attributeName, result);
}

//! @brief Template specialization of ParseAttribute for integer vector
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the std::vector<int> where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<>(xmlNodePtr element, const std::string &attributeName, std::vector<int> &result)
{
  return ParseAttributeIntVector(element, attributeName, &result);
}

//! @brief Template specialization of ParseAttribute for double vector
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the std::vector<double> where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<>(xmlNodePtr element, const std::string &attributeName, std::vector<double> &result)
{
  return ParseAttributeDoubleVector(element, attributeName, &result);
}

//! @brief Template specialization of ParseAttribute for string vector
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the std::vector<std::string> where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<>(xmlNodePtr element, const std::string &attributeName, std::vector<std::string> &result)
{
  return ParseAttributeStringVector(element, attributeName, &result);
}

//! @brief Template specialization of ParseAttribute for integer
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the int where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<int>(xmlNodePtr element, const std::string &attributeName, int &result)
{
  return ParseAttributeInt(element, attributeName, result);
}

//! @brief Template specialization of ParseAttribute for double
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the double where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<double>(xmlNodePtr element, const std::string &attributeName, double &result)
{
  return ParseAttributeDouble(element, attributeName, result);
}

//! @brief Template specialization of ParseAttribute for string
//!
//! @param element       The XML element holding the attribute
//! @param attributeName The name of the attribute to parse the value from
//! @param result        A reference to the std::string where the parsed attribute value will be stored
//! @return true if the parsing is successful; otherwise, false
template <>
bool ParseAttribute<std::string>(xmlNodePtr element, const std::string &attributeName, std::string &result)
{
  return ParseAttributeString(element, attributeName, result);
}

bool ParseAttributeString(xmlNodePtr element,
                          const std::string &attributeName,
                          std::string &result,
                          std::optional<std::string> defaultValue /* = std::nullopt */)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    if (defaultValue.has_value())
    {
      result = defaultValue.value();
      return true;
    }
    return false;
  }

  result = toString(attribute);
  xmlFree(attribute);

  return true;
}

bool ParseAttributeDouble(xmlNodePtr element,
                          const std::string &attributeName,
                          double &result,
                          std::optional<double> defaultValue /* = std::nullopt */)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName.c_str()));
  if (attribute == nullptr)
  {
    if (defaultValue.has_value())
    {
      result = defaultValue.value();
      return true;
    }
    return false;
  }

  try
  {
    result = std::stod(toString(attribute));
    xmlFree(attribute);
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }

  return true;
}

int ParseAttributeInt(xmlNodePtr element, const std::string &attributeName, int &result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    result = std::stoi(toString(attribute));
    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

bool ParseAttributeBool(xmlNodePtr element, const std::string &attributeName, bool &result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    std::string value = toString(attribute);
    std::transform(value.begin(), value.end(), value.begin(), ::tolower);
    std::istringstream is(value);
    is >> std::boolalpha >> result;
    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

bool ParseAttributeStringVector(xmlNodePtr element, const std::string &attributeName, std::vector<std::string> *result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    *result = scm::common::TokenizeString(toString(attribute), ',');

    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

bool ParseAttributeDoubleVector(xmlNodePtr element, const std::string &attributeName, std::vector<double> *result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    std::stringstream valueStream(toString(attribute));

    double item{};
    while (valueStream >> item)
    {
      result->push_back(item);

      if (valueStream.peek() == ',')
      {
        valueStream.ignore();
      }
    }

    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

bool ParseAttributeIntVector(xmlNodePtr element, const std::string &attributeName, std::vector<int> *result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    std::stringstream valueStream(toString(attribute));

    int item{};
    while (valueStream >> item)
    {
      result->push_back(item);

      if (valueStream.peek() == ',')
      {
        valueStream.ignore();
      }
    }

    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

bool ParseAttributeBoolVector(xmlNodePtr element, const std::string &attributeName, std::vector<bool> *result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  try
  {
    std::stringstream valueStream(toString(attribute));

    std::string value;

    while (std::getline(valueStream, value, ','))
    {
      bool item{};
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      std::istringstream is(value);
      is >> std::boolalpha >> item;

      result->push_back(item);
    }

    xmlFree(attribute);
    return true;
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }
}

}  // namespace scm::common