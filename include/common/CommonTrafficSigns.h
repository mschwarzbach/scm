/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <units.h>

#include <string>
#include <vector>

#include "CommonHelper.h"

namespace scm
{
namespace CommonTrafficSign
{
enum Type
{
  Undefined = 0,
  GiveWay = 205,
  Stop = 206,
  DoNotEnter = 267,
  EnvironmentalZoneBegin = 2701,  // 270.1
  EnvironmentalZoneEnd = 2702,    // 270.2
  MaximumSpeedLimit = 274,
  SpeedLimitZoneBegin = 2741,  // 274.1
  SpeedLimitZoneEnd = 2742,    // 274.2
  MinimumSpeedLimit = 275,
  OvertakingBanBegin = 276,
  OvertakingBanTrucksBegin = 277,
  EndOfMaximumSpeedLimit = 278,
  EndOfMinimumSpeedLimit = 279,
  OvertakingBanEnd = 280,
  OvertakingBanTrucksEnd = 281,
  EndOffAllSpeedLimitsAndOvertakingRestrictions = 282,
  PedestrianCrossing = 293,
  RightOfWayNextIntersection = 301,
  RightOfWayBegin = 306,
  RightOfWayEnd = 307,
  TownBegin = 310,
  TownEnd = 311,
  TrafficCalmedDistrictBegin = 3251,  // 325.1
  TrafficCalmedDistrictEnd = 3252,    // 325.2
  HighWayBegin = 3301,                // 330.1
  HighWayEnd = 3302,                  // 330.2
  HighWayExit = 333,
  HighwayExitPole = 450,  // 450-(50/51/52),
  AnnounceHighwayExit = 448,
  PreannounceHighwayExitDirections = 449,
  AnnounceRightLaneEnd = 5311,  // 531-(10/11/12/13)
  AnnounceLeftLaneEnd = 5312,   // 531-(20/21/22/23)
  DistanceIndication = 1004     // 1004-(30/31/32/33)
};

enum Unit
{
  None = 0,
  Kilogram,
  MeterPerSecond,
  Meter,
  Percentage,
  Second
};

/// @brief Structure defining an entity
struct Entity
{
  Type type{Type::Undefined};                       ///< type of the entity
  Unit unit{Unit::None};                            ///< unit
  units::length::meter_t distanceToStartOfRoad{0};  ///< distance to the start of the road
  units::length::meter_t relativeDistance{0};       ///< relative distance
  double value{0};                                  ///< value
  std::string text{""};                             ///< text TODO
  std::vector<Entity> supplementarySigns{};         ///< list of additional signs
};
inline bool operator==(const Entity& lhs, const Entity& rhs)
{
  return lhs.type == rhs.type && lhs.unit == rhs.unit && scm::common::DoubleEquality(lhs.distanceToStartOfRoad.value(), rhs.distanceToStartOfRoad.value()) && scm::common::DoubleEquality(lhs.relativeDistance.value(), rhs.relativeDistance.value()) && scm::common::DoubleEquality(lhs.value, rhs.value) && lhs.text == rhs.text && lhs.supplementarySigns == rhs.supplementarySigns;
}
}  // namespace CommonTrafficSign
}  // namespace scm