/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2018 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @brief This file contains helper functions to parse the configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <math.h>

#include <cstdint>
#include <functional>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

namespace scm::common
{

//! @brief Convert unsigned char* to std::string
//!
//! @param  input A pointer to unsigned char data to be converted
//! @return string representation of the input. If input is nullptr, an empty string is returned
extern std::string toString(const unsigned char *input);

//! @brief Convert std::string to unsigned char*
//!
//! @param input  The input string to be converted
//! @return const unsigned char pointer pointing to the internal buffer of the input string
extern const unsigned char *toXmlChar(const std::string &input);

//! Writes a message into the log including the line number of the erronous xml element
//!
//! \param element      erronous xml element
//! \param message      message describing error
//! \param logFunction  function to use for logging
static void LogMessage(const xmlNodePtr element,
                       const std::string &message,
                       std::function<void(const std::string &)> logFunction)
{
  logFunction("Could not import element " + toString(element->name) + " (line " + std::to_string(element->line) + "): " + message);
}

//! @brief Get the first child element of a given XML node that matches the provided tag
//!
//! This function searches for the first child element node under the given root XML node
//! that matches the provided tag name.
//!
//! @param rootElement  The XML node from which to start the search for child elements
//! @param tag          The tag name of the child element to search for
//! @return pointer to the first child element node matching the tag if found; otherwise, nullptr
extern xmlNodePtr GetFirstChildElement(xmlNodePtr rootElement, const std::string &tag);

//! @brief Get the first child element node of a given XML node
//!
//! This function searches for the first child element node under the given root XML node
//!
//! @param rootElement  The XML node from which to retrieve the first child element node.
//! @return pointer to the first child element node if found; otherwise, nullptr.
extern xmlNodePtr GetFirstChild(xmlNodePtr rootElement);

//! @brief Get the last child element of a given XML node that matches the provided tag
//!
//! This function searches for the last child element node under the given root XML node
//! that matches the provided tag name.
//!
//! @param rootElement  The XML node from which to start the search for the last child element
//! @param tag          The tag name of the child element to search for
//! @return pointer to the last child element node matching the tag if found; otherwise, nullptr
extern xmlNodePtr GetLastChildElement(xmlNodePtr rootElement, const std::string &tag);

//! @brief Templated wrapper for Parse* functions
//!
//! Depending on the datatype of the result parameter, different parsing techniques are applied
//!
//! @tparam T           The type for which the parsing functionality is to be implemented
//! @param rootElement  The XML node pointer to start parsing from
//! @param tag          The tag name used to identify the data to be parsed
//! @param result       Container holding the result
//! @return true if the parsing is successful; otherwise, false or an exception may be thrown
template <typename T>
extern bool Parse(xmlNodePtr rootElement, const std::string &tag, T &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as a string
//!
//! This function searches for a child element node under the given root XML node that matches
//! the provided tag name. If found, it retrieves the text content of the matching node and stores
//! it in the 'result' string parameter.
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to a string where the text content of the found tag will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseString(xmlNodePtr rootElement, const std::string &tag, std::string &result);

//! @brief Parse the text content of the current XML element node as an integer
//!
//! This function attempts to parse the text content of the provided current XML element node
//! as an integer and stores the result in the 'result' parameter
//!
//! @param currentElement   The current XML element node to parse
//! @param result           The reference to an integer where the parsed value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseCurrentInt(xmlNodePtr currentElement, int &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as a double value
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to a double where the text content of the found tag will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseDouble(xmlNodePtr rootElement, const std::string &tag, double &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as a vector of doubles
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to a vector of doubles where the parsed values will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseDoubleVector(xmlNodePtr rootElement, const std::string &tag, std::vector<double> &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as an integer
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to an integer where the text content of the found tag will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseInt(xmlNodePtr rootElement, const std::string &tag, int &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as an unsigned long
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to an unsigned long where the text content of the found tag will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseULong(xmlNodePtr rootElement, const std::string &tag, std::uint64_t &result);

//! @brief Parse an XML node for a specific tag and retrieve its text content as a boolean value
//!
//! @param rootElement  The XML node from which to start the search for the specified tag
//! @param tag          The tag name of the child element to search for
//! @param result       The reference to a boolean where the text content of the found tag will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseBool(xmlNodePtr rootElement, const std::string &tag, bool &result);

//! @brief Parses the value of an XML attribute into the provided string container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The reference to a string container where the attribute value will be stored
//! @param defaultValue   Optional default value to be used if the attribute is missing or empty (default: std::nullopt)
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeString(xmlNodePtr element,
                                 const std::string &attributeName,
                                 std::string &result,
                                 std::optional<std::string> defaultValue = std::nullopt);

//! @brief Parses the value of an XML attribute into the provided double container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The reference to a double container where the attribute value will be stored
//! @param defaultValue   Optional default value to be used if the attribute is missing or empty (default: std::nullopt)
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeDouble(xmlNodePtr element,
                                 const std::string &attributeName,
                                 double &result,
                                 std::optional<double> defaultValue = std::nullopt);

//! @brief Parses the value of an XML attribute into the provided container
//!
//! @tparam T             The type for which the parsing functionality is to be implemented
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         Container holding the result
//! @param defaultValue   Optional default value to be used if the attribute is missing or empty (default: std::nullopt)
//! @return true on successful parsing, false otherwise
// template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
template <typename T>
extern bool ParseAttributeDouble(xmlNodePtr element,
                                 const std::string &attributeName,
                                 T &result,
                                 std::optional<T> defaultValue = std::nullopt)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar *attribute = xmlGetProp(element, toXmlChar(attributeName.c_str()));
  if (attribute == nullptr)
  {
    if (defaultValue.has_value())
    {
      result = defaultValue.value();
      return true;
    }
    return false;
  }

  try
  {
    result = T(std::stod(toString(attribute)));
    xmlFree(attribute);
  }
  catch (...)
  {
    xmlFree(attribute);
    return false;
  }

  return true;
}

//! @brief Parses the value of an XML attribute into the provided integer container 'result'
//!
//! @param node           The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The reference to an integer container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern int ParseAttributeInt(xmlNodePtr node, const std::string &attributeName, int &result);

//! @brief Parses the value of an XML attribute into the provided boolean container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The reference to a boolean container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeBool(xmlNodePtr element, const std::string &attributeName, bool &result);

//! @brief Parses the value of an XML attribute into the provided string vector container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The pointer to a string vector container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeStringVector(xmlNodePtr element,
                                       const std::string &attributeName,
                                       std::vector<std::string> *result);

//! @brief Parses the value of an XML attribute into the provided double vector container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The pointer to a double vector container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeDoubleVector(xmlNodePtr element,
                                       const std::string &attributeName,
                                       std::vector<double> *result);

//! @brief Parses the value of an XML attribute into the provided integer vector container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The pointer to a integer vector container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeIntVector(xmlNodePtr element, const std::string &attributeName, std::vector<int> *result);

//! @brief Parses the value of an XML attribute into the provided boolean vector container 'result'
//!
//! @param element        The XML element holding the attribute
//! @param attributeName  The name of the attribute to parse the value from
//! @param result         The pointer to a boolean vector container where the attribute value will be stored
//! @return true on successful parsing, false otherwise
extern bool ParseAttributeBoolVector(xmlNodePtr element, const std::string &attributeName, std::vector<bool> *result);

/*!
 *  \brief Parses the value of an XML attribute into the provided container.
 *
 *  Depending on the datatype of the result parameter, different parsing techniques are applied.
 *  \tparam     T               TypeName
 *  \param[in]  element         XML element holding the attribute
 *  \param[in]  attributeName   Name of the attribute to parse the value from
 *  \param[out] result          Container holding the result
 *
 *  \return     True on successful parsing, false otherwise.
 */
template <typename T>
bool ParseAttribute(xmlNodePtr element, const std::string &attributeName, T &result);

/*!
 * \brief Imports a probability map
 *
 * \details This method imports a probability map for some value of type T. T can either be int, double or string
 *
 *
 * @param[in]     parentElement          Element containing the information
 * @param[in]     key                    Name how the value is specified in the xml file
 * @param[in]     tag                    Name of the tags that should be imported
 * @param[out]    probabilities          Map where the probabilities get saved
 * @param[in]     logFunction            Function to use for logging
 * @param[in]     mustAddUpToOne         flag which specifies whether the sum of all probalities must be 1
 * @return	     true, if successful
 */
template <typename T>
bool ImportProbabilityMap(xmlNodePtr parentElement,
                          const std::string key,
                          const std::string &tag,
                          std::vector<std::pair<T, double>> &probabilities,
                          std::function<void(const std::string &)> logFunction,
                          bool mustAddUpToOne = true)
{
  double probabilitySum = 0.0;

  xmlNodePtr childElement = GetFirstChildElement(parentElement, tag);
  if (!childElement)
  {
    LogMessage(parentElement, "At least one element is required.", logFunction);
    return false;
  }

  while (childElement && xmlStrEqual(childElement->name, toXmlChar(tag)))
  {
    T keyValue;
    double probability{};

    if (!ParseAttribute<T>(childElement, key, keyValue))
    {
      LogMessage(childElement, "Key is invalid.", logFunction);
      return false;
    }

    if (!ParseAttributeDouble(childElement, "Probability", probability))
    {
      LogMessage(childElement, "Probability is invalid.", logFunction);
      return false;
    }

    probabilities.push_back({keyValue, probability});

    probabilitySum += probability;

    childElement = xmlNextElementSibling(childElement);
  }

  // Checks probabilities
  if (mustAddUpToOne && std::abs(probabilitySum - 1.0) > 1e-6)
  {
    LogMessage(parentElement, "Probabilities do not add up to 1.0.", logFunction);
    return false;
  }

  if (probabilitySum > 1.0 + 1e-6)
  {
    LogMessage(parentElement, "Probabilities add up to more than 1.0.", logFunction);
    return false;
  }

  return true;
}

}  // namespace scm::common
