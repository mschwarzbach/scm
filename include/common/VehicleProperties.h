/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file VehicleProperties.h
#pragma once
#include <units.h>

#include <array>
#include <cmath>
#include <map>
#include <string>
#include <vector>

using namespace units::literals;
namespace scm::common
{
//! @brief Struct definition for the description of a Vec3
template <typename T>
struct Vec3
{
  Vec3() = default;

  //! @brief specialized constructor
  //! @param x_in T
  //! @param y_in T
  //! @param z_in T
  Vec3(T x_in, T y_in, T z_in)
      : x{x_in}, y{y_in}, z{z_in}
  {
  }

  //! @brief x T
  T x{0};
  //! @brief y T
  T y{0};
  //! @brief y T
  T z{0};

  //! @brief calculate the length of Vec3
  //! @return length
  inline T Length() const { return std::sqrt((x * x) + (y * y) + (z * z)); }

  //! @brief operator- overload to given negative x y z
  //! @return negativ x y z
  inline Vec3<T> operator-() const noexcept
  {
    return {-x, -y, -z};
  }
};

//! @brief Struct definition for the description of a Dimension3
struct Dimension3
{
  //! @brief length
  units::length::meter_t length{0};
  //! @brief width
  units::length::meter_t width{0};
  //! @brief height
  double height{0};
};

//! @brief Struct definition for the description of a BoundingBox
struct BoundingBox
{
  //! @brief geometric_center
  Vec3<double> geometric_center{};
  //! @brief dimension
  Dimension3 dimension{};
};

//! @brief Struct definition for the description of a Performance
struct Performance
{
  //! @brief max_speed
  units::velocity::meters_per_second_t max_speed{0.0};
  //! @brief max_acceleration
  double max_acceleration{0.0};
  //! @brief max_deceleration
  double max_deceleration{0.0};
};

//! @brief Struct definition for the description of a Axle
struct Axle
{
  //! @brief max_steering
  units::angle::radian_t max_steering{0.0_rad};
  //! @brief wheel_diameter
  units::length::meter_t wheel_diameter{0.0_m};
  //! @brief track_width
  units::length::meter_t track_width{0.0_m};
  //! @brief bb_center_to_axle_center
  Vec3<double> bb_center_to_axle_center{};
};

//! @brief enumeration of different vehicle classes
enum class VehicleClass
{
  kOther = 1,  // Other (unspecified but known) type of vehicle.
  // Vehicle is a small car.
  // Definition: Hatchback car with maximum length 4 m.
  kSmall_car = 2,
  // Vehicle is a compact car.
  // Definition: Hatchback car with length between 4 and 4.5 m.
  kCompact_car = 3,
  // Vehicle is a medium car.
  // Definition: Hatchback or sedan with length between 4.5 and 5 m.
  kMedium_car = 4,
  // Vehicle is a luxury car.
  // Definition: Sedan or coupe that is longer then 5 m.
  kLuxury_car = 5,
  // Vehicle is a delivery van.
  // Definition: A delivery van.
  kDelivery_van = 6,
  // Vehicle is a heavy truck.
  kHeavy_truck = 7,
  // Vehicle is a truck with semitrailer.
  kSemitrailer = 8,
  // Vehicle is a trailer (possibly attached to another vehicle).
  kTrailer = 9,
  // Vehicle is a motorbike or moped.
  kMotorbike = 10,
  // Vehicle is a bicycle (without motor and specific lights).
  kBicycle = 11,
  // Vehicle is a bus.
  kBus = 12,
  // Vehicle is a tram.
  kTram = 13,
  // Vehicle is a train.
  kTrain = 14,
  // Vehicle is a wheelchair.
  kWheelchair = 15,
  // Vehicle type not specified properly.
  kInvalid = -1
};

namespace vehicle::properties
{
//! @brief Struct definition for the description of a EntityProperties
struct EntityProperties
{
  virtual ~EntityProperties() = default;
  //! @brief bounding_box
  BoundingBox bounding_box{Vec3<double>{}, Dimension3{}};
  //! @brief model
  std::string model{};
  //! @brief properties
  std::map<std::string, std::string> properties{};
  //! @brief mass
  units::mass::kilogram_t mass{};
  //! @brief wheelBase
  double wheelBase{};
  //! @brief classification
  VehicleClass classification{VehicleClass::kOther};

  //! @brief performance
  Performance performance{};
  //! @brief front_axle
  Axle front_axle{};
  //! @brief rear_axle
  Axle rear_axle{};
};

//! @brief The AirDragCoefficient
constexpr char AirDragCoefficient[]{"AirDragCoefficient"};
//! @brief The AxleRatio
constexpr char AxleRatio[]{"AxleRatio"};
//! @brief The DecelerationFromPowertrainDrag
constexpr char DecelerationFromPowertrainDrag[]{"DecelerationFromPowertrainDrag"};
//! @brief The FrictionCoefficient
constexpr char FrictionCoefficient[]{"FrictionCoefficient"};
//! @brief The FrontAxleMaxSteering
constexpr char FrontAxleMaxSteering[]{"FrontAxleMaxSteering"};
//! @brief The FrontSurface
constexpr char FrontSurface[]{"FrontSurface"};
//! @brief The GearRatio
constexpr char GearRatio[]{"GearRatio"};
//! @brief The NumberOfGears
constexpr char NumberOfGears[]{"NumberOfGears"};
//! @brief The MaximumEngineSpeed
constexpr char MaximumEngineSpeed[]{"MaximumEngineSpeed"};
//! @brief The MaximumEngineTorque
constexpr char MaximumEngineTorque[]{"MaximumEngineTorque"};
//! @brief The MinimumEngineSpeed
constexpr char MinimumEngineSpeed[]{"MinimumEngineSpeed"};
//! @brief The SteeringRatio
constexpr char SteeringRatio[]{"SteeringRatio"};

}  // namespace vehicle::properties

}  // namespace scm::common