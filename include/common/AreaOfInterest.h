/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

//! @brief Areas of interest for surrounding data.
enum class AreaOfInterest
{
  LEFT_FRONT = 0,
  LEFT_FRONT_FAR,
  RIGHT_FRONT,
  RIGHT_FRONT_FAR,
  LEFT_REAR,
  RIGHT_REAR,
  EGO_FRONT,
  EGO_FRONT_FAR,
  EGO_REAR,
  LEFT_SIDE,
  RIGHT_SIDE,
  INSTRUMENT_CLUSTER,
  INFOTAINMENT,
  HUD,  // head up display
  LEFTLEFT_FRONT,
  RIGHTRIGHT_FRONT,
  LEFTLEFT_REAR,
  RIGHTRIGHT_REAR,
  LEFTLEFT_SIDE,
  RIGHTRIGHT_SIDE,
  DISTRACTION,
  //
  NumberOfAreaOfInterests
};