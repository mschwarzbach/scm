/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <optional>
#include <string>
#include <utility>
#include <variant>
#include <vector>

#include "ParameterInterface.h"
#include "ScmRuntimeInformation.h"
#include "StochasticDefinitions.h"
#include "src/FMU/src/OsmpShell/FmiParameterInterface.h"

namespace scm::parameter
{
using ParameterMapping = std::map<std::string, std::variant<bool, double, int, StochasticDistribution>>;

class Parameters : public ParameterInterface
{
public:
  ~Parameters() override = default;

  /**
   * @brief Parameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  Parameters(const scm::RuntimeInformation& runtimeInformation)
      : runtimeInformation(runtimeInformation)
  {
  }

  /**
   * @brief Parameters constructor
   *
   * @param[in] other Configuration parameters
   */
  Parameters(const ParameterInterface& other)
      : runtimeInformation{other.GetRuntimeInformation()},
        parametersDouble{other.GetParametersDouble()},
        parametersInt{other.GetParametersInt()},
        parametersBool{other.GetParametersBool()},
        parametersString{other.GetParametersString()},
        parametersDoubleVector{other.GetParametersDoubleVector()},
        parametersIntVector{other.GetParametersIntVector()},
        parametersBoolVector{other.GetParametersBoolVector()},
        parametersStringVector{other.GetParametersStringVector()},
        parametersStochastic{other.GetParametersStochastic()},
        parameterLists{other.GetParameterLists()}
  {
  }

  bool AddParameterDouble(std::string name, double value) override;
  bool AddParameterInt(std::string name, int value) override;
  bool AddParameterBool(std::string name, bool value) override;
  bool AddParameterString(std::string name, const std::string& value) override;
  bool AddParameterDoubleVector(std::string name, const std::vector<double> value) override;
  bool AddParameterIntVector(std::string name, const std::vector<int> value) override;
  bool AddParameterBoolVector(std::string name, const std::vector<bool> value) override;
  bool AddParameterStringVector(std::string name, const std::vector<std::string> value) override;
  bool AddParameterStochastic(std::string name, const scm::parameter::StochasticDistribution value) override;

  ParameterInterface& InitializeListItem(std::string key) override;

  // NOTE: The primitive getters are in header on purpose

  const scm::RuntimeInformation& GetRuntimeInformation() const override
  {
    return runtimeInformation;
  }

  const std::map<std::string, double>& GetParametersDouble() const override
  {
    return parametersDouble;
  }

  const std::map<std::string, int>& GetParametersInt() const override
  {
    return parametersInt;
  }

  const std::map<std::string, bool>& GetParametersBool() const override
  {
    return parametersBool;
  }

  const std::map<std::string, const std::string>& GetParametersString() const override
  {
    return parametersString;
  }

  const std::map<std::string, const std::vector<double>>& GetParametersDoubleVector() const override
  {
    return parametersDoubleVector;
  }

  const std::map<std::string, const std::vector<int>>& GetParametersIntVector() const override
  {
    return parametersIntVector;
  }

  const std::map<std::string, const std::vector<bool>>& GetParametersBoolVector() const override
  {
    return parametersBoolVector;
  }

  const std::map<std::string, const std::vector<std::string>>& GetParametersStringVector() const override
  {
    return parametersStringVector;
  }

  const std::map<std::string, const scm::parameter::StochasticDistribution>& GetParametersStochastic() const override
  {
    return parametersStochastic;
  }

  const std::map<std::string, ParameterLists>& GetParameterLists() const override
  {
    return parameterLists;
  }

protected:
  /// Runtime information from the interface
  const scm::RuntimeInformation& runtimeInformation;

  /// Mapping of "id" to "value"
  std::map<std::string, double> parametersDouble;                                            ///< Parameters of type double
  std::map<std::string, int> parametersInt;                                                  ///< Parameters of type int
  std::map<std::string, bool> parametersBool;                                                ///< Parameters of type bool
  std::map<std::string, const std::string> parametersString;                                 ///< Parameters of type string
  std::map<std::string, const std::vector<double>> parametersDoubleVector;                   ///< Parameters of type double vector
  std::map<std::string, const std::vector<int>> parametersIntVector;                         ///< Parameters of type int vector
  std::map<std::string, const std::vector<bool>> parametersBoolVector;                       ///< Parameters of type bool vector
  std::map<std::string, const std::vector<std::string>> parametersStringVector;              ///< Parameters of type string vector
  std::map<std::string, const scm::parameter::StochasticDistribution> parametersStochastic;  ///< Parameters of stochastic type
  std::map<std::string, ParameterLists> parameterLists;                                      ///< Parameter lists
};

struct PartitionedFmiParameters
{
  FmiParameters driverParameters{};
  FmiParameters vehicleParameters{};
  FmiParameters simulationParameters{};
};

/// This class represents a model parameters
class ModelParameters : public Parameters
{
public:
  /**
   * @brief ModelParameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  ModelParameters(const scm::RuntimeInformation& runtimeInformation)
      : Parameters(runtimeInformation)
  {
  }
};

namespace internal
{

using ParameterKey = std::string;
using ParameterValue = std::variant<
    bool,
    std::vector<bool>,
    int,
    std::vector<int>,
    double,
    std::vector<double>,
    std::string,
    std::vector<std::string>,
    StochasticDistribution>;

/// @brief the elementary type of a container, in a sense such as std::vector<std::pair<key, value>>
using ParameterElementLevel3 = std::pair<internal::ParameterKey, internal::ParameterValue>;
/// @brief set of parameter level 3 elements
using ParameterSetLevel3 = std::vector<internal::ParameterElementLevel3>;

/// @brief list of parameter sets level 3
using ParameterListLevel2 = std::vector<internal::ParameterSetLevel3>;

/// @brief level 2 parameter elements are key value pairs, where the value can either be a ParameterValue or a ParameterList level 2
using ParameterElementLevel2 = std::pair<internal::ParameterKey, std::variant<internal::ParameterValue, internal::ParameterListLevel2>>;

/// @brief set of parameter level 2 elements
using ParameterSetLevel2 = std::vector<ParameterElementLevel2>;

/// @brief list of parameter sets level 2
using ParameterListLevel1 = std::vector<internal::ParameterSetLevel2>;

}  // namespace internal

/// @brief use to collect ParameterValues or ParameterLists (only one layer of lists is allowed)
using ParameterSetLevel1 = std::vector<std::pair<internal::ParameterKey, std::variant<internal::ParameterValue, internal::ParameterListLevel2, internal::ParameterListLevel1>>>;

//! Query the internal parameter set of a parameter list for a specific token
//! @param param       the internal parameter set
//! @param searchToken the search token
//! @return value or std::nullopt
template <typename T>
static std::optional<T> Get(const internal::ParameterSetLevel3& param, const internal::ParameterKey& searchToken)
{
  for (const auto& [key, value] : param)
  {
    if (searchToken == key && std::holds_alternative<T>(value))
    {
      return std::get<T>(value);
    }
  }

  return std::nullopt;
}

//! Query the internal parameter set of a parameter list for a specific token
//! @param container        the internal parameter container
//! @param searchToken      the search token
//! @return value or std::nullopt
template <typename T>
static std::optional<T> Get(const internal::ParameterSetLevel2& container, const internal::ParameterKey& searchToken)
{
  for (const auto& [key, value] : container)
  {
    if (searchToken == key)
    {
      if constexpr (std::is_same_v<T, internal::ParameterListLevel2>)
      {
        if (std::holds_alternative<T>(value))
        {
          return std::get<T>(value);
        }
      }
      else if (std::holds_alternative<T>(std::get<internal::ParameterValue>(value)))
      {
        return std::get<T>(std::get<internal::ParameterValue>(value));
      }
    }
  }

  return std::nullopt;
}

//! Query a parameter container for a specific token
//! @param container       the parameter container
//! @param searchToken the search token
//! @return value or std::nullopt
template <typename T>
static std::optional<T> Get(const ParameterSetLevel1& container, const internal::ParameterKey& searchToken)
{
  for (const auto& [key, value] : container)
  {
    if (searchToken == key)
    {
      if constexpr (std::is_same_v<T, internal::ParameterListLevel1> || std::is_same_v<T, internal::ParameterListLevel2>)
      {
        if (std::holds_alternative<T>(value))
        {
          return std::get<T>(value);
        }
      }
      else if (std::holds_alternative<T>(std::get<internal::ParameterValue>(value)))
      {
        return std::get<T>(std::get<internal::ParameterValue>(value));
      }
    }
  }

  return std::nullopt;
}

}  // namespace scm::parameter
