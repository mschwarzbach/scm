/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ScmSampler.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include <iostream>
#include <map>
#include <unordered_map>

#include "LateralAction.h"
#include "common/StochasticDefinitions.h"

//! \brief returns a sample from the map according to a randomValue.
//!
//! This class returns a sample from the map according to a randomValue.
//! The map contains elements from template type T (keys) and according
//! probabilities (value) of their appearance. The sum of probabilities do not
//! have to sum up to one - normalisation is included in the function.
class ScmSampler
{
public:
  ScmSampler() = default;
  ScmSampler(const ScmSampler&) = delete;
  ScmSampler(ScmSampler&&) = delete;
  ScmSampler& operator=(const ScmSampler&) = delete;
  ScmSampler& operator=(ScmSampler&&) = delete;
  virtual ~ScmSampler() = default;

  //! \brief Sample from the map according to a randomValue.
  //! \param [in]   map           map with elements to be sampled type T and their sample probabilities
  //! \param [in]   defaultValue  returned, if sum of probabilities is zero (includes empty map)
  //! \param [in]   randomValue   random number between 0 and 1
  //! \return sampled_value
  template <typename T>
  static T Sample(const std::map<T, double>& map, T defaultValue, double randomValue)
  {
    // prepare normalisation
    double sumAllProb = 0.;
    for (auto iterator = map.begin(); iterator != map.end(); ++iterator)
    {
      sumAllProb += iterator->second;
    }

    // default values
    if (sumAllProb == 0.)
    {
      return defaultValue;
    }

    if (map.size() == 1 || randomValue == 0.)
    {
      return map.begin()->first;
    }

    // 'normalisation'
    randomValue *= sumAllProb;

    // sampling
    double sumProb = 0.;
    for (auto iterator = map.begin(); iterator != map.end(); ++iterator)
    {
      sumProb += iterator->second;
      if (sumProb >= randomValue)
      {
        return iterator->first;
      }
    }

    // can just be reached due to rounding errors -> get last element
    return map.rbegin()->first;
  }

  template <typename T>
  static T Sample(const std::unordered_map<T, double, LateralActionHash>& map, T defaultValue, double randomValue)
  {
    // prepare normalisation
    double sumAllProb = 0.;
    for (auto iterator = map.begin(); iterator != map.end(); ++iterator)
    {
      sumAllProb += iterator->second;
    }

    // default values
    if (sumAllProb == 0.)
    {
      return defaultValue;
    }

    if (map.size() == 1 || randomValue == 0.)
    {
      return map.begin()->first;
    }

    // 'normalisation'
    randomValue *= sumAllProb;

    // sampling
    double sumProb = 0.;
    for (auto iterator = map.begin(); iterator != map.end(); ++iterator)
    {
      sumProb += iterator->second;
      if (sumProb >= randomValue)
      {
        return iterator->first;
      }
    }

    // can just be reached due to rounding errors -> get last element
    return map.begin()->first;
  }

  /// Number of maximal retries
  static constexpr int MAX_RETRIES{1000000};

  /// @brief TODO
  /// @param chance
  /// @param stochastics Pointer to the stochastics interface
  /// @return
  static bool RollFor(double chance, StochasticsInterface* stochastics)
  {
    if (chance == 0.0)
    {
      return false;
    }

    double roll = stochastics->GetUniformDistributed(0, 1);
    return (roll <= chance);
  }

  /// @brief TODO
  /// @param distribution
  /// @param stochastics Pointer to the stochastics interface
  /// @return
  static double RollForStochasticAttribute(const scm::parameter::StochasticDistribution& distribution, StochasticsInterface* stochastics)
  {
    if (std::holds_alternative<scm::parameter::NormalDistribution>(distribution))
    {
      auto normalDistribution = std::get<scm::parameter::NormalDistribution>(distribution);

      if (scm::common::DoubleEquality(normalDistribution.GetMin(), normalDistribution.GetMax()))
      {
        return normalDistribution.GetMin();
      }

      int run = 0;
      double result = stochastics->GetNormalDistributed(normalDistribution.GetMean(), normalDistribution.GetStandardDeviation());

      while (result > normalDistribution.GetMax() || result < normalDistribution.GetMin())
      {
        run++;
        result = stochastics->GetNormalDistributed(normalDistribution.GetMean(), normalDistribution.GetStandardDeviation());
        if (run == MAX_RETRIES)
        {
          return normalDistribution.GetMean();
        }
      }
      return result;
    }
    else if (std::holds_alternative<scm::parameter::LogNormalDistribution>(distribution))
    {
      auto logNormalDistribution = std::get<scm::parameter::LogNormalDistribution>(distribution);

      if (scm::common::DoubleEquality(logNormalDistribution.GetMin(), logNormalDistribution.GetMax()))
      {
        return logNormalDistribution.GetMin();
      }

      int run = 0;
      double result = stochastics->GetLogNormalDistributedMuSigma(logNormalDistribution.GetMu(), logNormalDistribution.GetSigma());

      while (result > logNormalDistribution.GetMax() || result < logNormalDistribution.GetMin())
      {
        run++;
        result = stochastics->GetLogNormalDistributedMuSigma(logNormalDistribution.GetMu(), logNormalDistribution.GetSigma());
        if (run == MAX_RETRIES)
        {
          return 0.5 * (logNormalDistribution.GetMin() + logNormalDistribution.GetMax());
        }
      }
      return result;
    }
    else if (std::holds_alternative<scm::parameter::UniformDistribution>(distribution))
    {
      auto uniformDistribution = std::get<scm::parameter::UniformDistribution>(distribution);
      return stochastics->GetUniformDistributed(uniformDistribution.GetMin(), uniformDistribution.GetMax());
    }
    else if (std::holds_alternative<scm::parameter::ExponentialDistribution>(distribution))
    {
      int run = 0;
      auto exponentialDistribution = std::get<scm::parameter::ExponentialDistribution>(distribution);
      double result = stochastics->GetExponentialDistributed(exponentialDistribution.GetLambda());

      while (result > exponentialDistribution.GetMax() || result < exponentialDistribution.GetMin())
      {
        run++;
        result = stochastics->GetExponentialDistributed(exponentialDistribution.GetLambda());
        if (run == MAX_RETRIES)
        {
          return 1 / exponentialDistribution.GetLambda();
        }
      }
      return result;
    }
    else if (std::holds_alternative<scm::parameter::GammaDistribution>(distribution))
    {
      int run = 0;
      auto gammaDistribution = std::get<scm::parameter::GammaDistribution>(distribution);
      double result = stochastics->GetGammaDistributedShapeScale(gammaDistribution.GetShape(), gammaDistribution.GetScale());

      while (result > gammaDistribution.GetMax() || result < gammaDistribution.GetMin())
      {
        run++;

        if (run == MAX_RETRIES)
        {
          return gammaDistribution.GetMean();
        }

        result = stochastics->GetGammaDistributedShapeScale(gammaDistribution.GetShape(), gammaDistribution.GetScale());
      }
      return result;
    }
    else
    {
      throw std::runtime_error("Unsupported distribution type. Variant index: " + std::to_string(distribution.index()));
    }
  }

  /// @brief
  /// @tparam T               TODO
  /// @param weightedValues
  /// @param stochastics
  /// @return
  template <typename T>
  T Sample(std::vector<std::pair<T, double>> weightedValues, StochasticsInterface* stochastics)
  {
    double sumOfWeights{0.0};
    for (const auto& [value, weight] : weightedValues)
    {
      sumOfWeights += weight;
    }
    auto roll = stochastics->GetUniformDistributed(0, sumOfWeights);
    for (const auto& [value, weight] : weightedValues)
    {
      roll -= weight;
      if (roll <= 0)
      {
        return value;
      }
    }
    throw std::runtime_error("Invalid roll in Sampler");
  }
};
