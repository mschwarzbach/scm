/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file sensorInterface.h
#pragma once

#include "include/signal/sensorInput.h"
#include "include/signal/sensorOutput.h"

namespace scm::sensor
{
//! @brief Interface for sensor
class Interface
{
public:
  //! @brief Trigger the SensorOutput
  //! @param sensorInput
  //! @param time
  //! @param customCommands
  //! @return Sensoroutput signal
  virtual scm::signal::SensorOutput Trigger(scm::signal::SensorInput sensorInput, const std::vector<std::string>& customCommands) = 0;
  virtual ~Interface() = default;
};
}  // namespace scm::sensor