/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file parameterParserInterface.h
#pragma once

#include "include/signal/parameterParserOutput.h"

namespace scm::parameter_parser
{

//! @brief Interface for parameterparser
class Interface
{
public:
  //! @brief Trigger the ParameterParserOutput
  //! @return ParameterParserOutput signal
  virtual scm::signal::ParameterParserOutput Trigger(units::velocity::meters_per_second_t egoVelocity) = 0;
  virtual ~Interface() = default;
};
}  // namespace scm::parameter_parser