/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "module/driverInterface.h"
#include "module/dynamicsModelInterface.h"
#include "module/lateralControllerInterface.h"
#include "module/longitudinalControllerInterface.h"
#include "module/parameterParserInterface.h"
#include "module/sensorInterface.h"
