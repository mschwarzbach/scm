################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building OsiQueryLibrary with Conan
################################################################################

from conan import ConanFile
from conan.tools.scm import Git
from conan.tools.cmake import CMake, CMakeToolchain
import os, subprocess

required_conan_version = ">=1.53.0"

class OsiQueryLibraryConan(ConanFile):
    name = "osiquerylibrary"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "open_simulation_interface_version":["ANY"],
               "protobuf_version":["ANY"],
               "gtest_version":["ANY"],
               "boost_version":["ANY"],
               "OSIQL_TEST":["ON", "OFF"]
               }

    default_options = {"shared": False,
                       "fPIC": True,
                       "open_simulation_interface_version":"3.6.0",
                       "protobuf_version":"3.20.0",
                       "gtest_version":"1.14.0",
                       "boost_version":"1.85.0",
                       "OSIQL_TEST":"OFF"}

    no_copy_source = False
    short_paths = True

    def requirements(self):
        self.requires(f"protobuf/{self.options.protobuf_version}")
        self.requires(f"open-simulation-interface/{self.options.open_simulation_interface_version}@scm/testing")
        self.requires(f"gtest/{self.options.gtest_version}")
        self.requires(f"boost/{self.options.boost_version}")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        self.options["boost"].shared=True
        self.options["boost"].fPIC=True
        self.options["boost"].without_chrono=True
        self.options["boost"].without_cobalt=True
        self.options["boost"].without_container=True
        self.options["boost"].without_context=True
        self.options["boost"].without_contract=True
        self.options["boost"].without_coroutine=True
        self.options["boost"].without_date_time=True
        self.options["boost"].without_fiber=True
        self.options["boost"].without_iostreams=True
        self.options["boost"].without_json=True
        self.options["boost"].without_locale=True
        self.options["boost"].without_log=True
        self.options["boost"].without_nowide=True
        self.options["boost"].without_stacktrace=True
        self.options["boost"].without_test=True
        self.options["boost"].without_thread=True
        self.options["boost"].without_timer=True
        self.options["boost"].without_type_erasure=True
        self.options["boost"].without_url=True
        self.options["boost"].without_wave=True

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url_https = self.conan_data["sources"][self.version]["url_https"]
            url_ssh = self.conan_data["sources"][self.version]["url_ssh"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url_https = self.conan_data["sources"]["default"]["url_https"]
            url_ssh = self.conan_data["sources"]["default"]["url_ssh"]
            sha256 = self.version
        return url_https, url_ssh, sha256

    def generate(self):
        _cmake_prefix_paths = []
        for _, dependency in self.dependencies.items():
            _cmake_prefix_paths.append(dependency.package_folder)
        _cmake_prefix_paths = ';'.join(str(_cmake_prefix_path) for _cmake_prefix_path in _cmake_prefix_paths)
        tc = CMakeToolchain(self)
        tc.cache_variables["CMAKE_PREFIX_PATH"] = _cmake_prefix_paths
        tc.cache_variables["OSIQL_TEST"] = self.options.OSIQL_TEST
        tc.generate()

    def source(self):
        _, url_ssh, sha256 = self._get_url_sha()
        git = Git(self)
        url = f"https://gitlab.eclipse.org/eclipse/openpass/osi-query-library.git"
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()