################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building stochastics with Conan
################################################################################

from conan import ConanFile
from conan.tools.scm import Git
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
import os


required_conan_version = ">=2.0"

class StochasticsLibraryRecipe(ConanFile):
    name = "stochastics"
    version = "0.8.0"
    user = "scm"
    channel = "testing"

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _get_url(self):
        if self.version in self.conan_data["sources"]:
            url_https = self.conan_data["sources"][self.version]["url_https"]
            url_ssh = self.conan_data["sources"][self.version]["url_ssh"]
        else:
            url_https = self.conan_data["sources"]["default"]["url_https"]
            url_ssh = self.conan_data["sources"]["default"]["url_ssh"]
        return url_https, url_ssh

    def source(self):
        url_https, url_ssh = self._get_url()
        git = Git(self)
        url = url_https
        git.clone(url=url, target=self.name)
        git.folder=self.name

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["Stochastics"]