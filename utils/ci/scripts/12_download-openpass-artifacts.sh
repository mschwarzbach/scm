#!/bin/bash

################################################################################
# Copyright (c) 2021 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script downloads the openPASS artifacts
################################################################################


INSTALL_PATH="${WORKSPACE}/dist/"
echo "Install path is ${INSTALL_PATH}"

if [[ "${OSTYPE}" = "msys" ]]; then
    ARCHIVE_EXT="zip"
else
    ARCHIVE_EXT="tar.gz"
fi

echo "Downloading openpass artifact"
rm -r artifacts
mkdir artifacts && curl -L --output artifacts/openPASS_SIM_"${OP_RELEASE_VERSION}"."${ARCHIVE_EXT}" https://download.eclipse.org/openpass/releases/opSimulation/openPASS_SIM_"${OP_RELEASE_VERSION}"."${ARCHIVE_EXT}"


echo "Extracting openPASS_SIM artifact"
# extract downloaded simulator from artifactory
rm -rf "${INSTALL_PATH}" # remove any potential files already in this path
mkdir -p "${INSTALL_PATH}"

if [[ "${OSTYPE}" = "msys" ]]; then
unzip -q artifacts/openPASS_SIM_"${OP_RELEASE_VERSION}".zip -d "${INSTALL_PATH}" || exit 1


elif [[ ${OSTYPE} =~ $linux ]]; then
tar zxf artifacts/openPASS_SIM_"${OP_RELEASE_VERSION}".tar.gz -C "${INSTALL_PATH}" || exit 1

else
  echo "ERROR with OSTYPE identification:\n >>> ${OSTYPE} not supported"
  exit 1
fi