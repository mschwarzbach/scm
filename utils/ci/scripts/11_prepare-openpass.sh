#!/bin/bash

################################################################################
# Copyright (c) 2023 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares building
################################################################################

INSTALL_PATH="${WORKSPACE}/dist/"
echo "Install path is ${INSTALL_PATH}"

# checkout openpass metadata
git clone --filter=blob:none --sparse https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git openPASS
cd openPASS || exit 9
# only download pyOpenPASS
git sparse-checkout add sim/tests/endToEndTests/pyOpenPASS || exit 1

# checkout commit develop on 06.20.2024
git checkout 877c26b3979be3818b6794c6478531599108a52e


# copy pyOpenPass to install dir
cp -r sim/tests/endToEndTests/* "${INSTALL_PATH}"

cp "${WORKSPACE}/repo/e2eTests/Resources/test.json" "${INSTALL_PATH}/pyOpenPASS"

rm -rf artifacts

printenv

exit 0

