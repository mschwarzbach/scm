#!/bin/bash

################################################################################
# Copyright (c) 2023 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script packs the artifacts
################################################################################

cd "${WORKSPACE}" || exit 1

ARTIFACT_PATH=artifacts/E2E-Out
SOURCES=e2e-output/

# remove AlgorithmScm.fmu as they were copied for each config that ran by pyOpenPASS (e2e-test framework)
find "${SOURCES}" -type f -name 'AlgorithmScm.fmu' -exec rm {} +

if [[ "${OSTYPE}" = "msys" ]]; then
zip -q -r -9 ${ARTIFACT_PATH}.zip ${SOURCES}

else
tar -czf ${ARTIFACT_PATH}.tar.gz ${SOURCES}

fi

exit 0