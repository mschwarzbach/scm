#!/bin/bash

################################################################################
# Copyright (c) 2021 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

#####################################f###########################################
# This script executes pyOpenPAss (E2E-tests)
################################################################################

E2E_OUTPUT="${WORKSPACE}/e2e-output"
mkdir -p "${E2E_OUTPUT}"

# set up paths for pyOpenPASS
cd "${WORKSPACE}/dist/pyOpenPASS" || exit 2

RESOURCES_PATH="${WORKSPACE}/repo/e2eTests/Resources"
COMMON_CONFIGS="${RESOURCES_PATH}/Common"
SPECIALIZED_CONFIGS="${RESOURCES_PATH}/Configurations"
ALLOWED_WARNINGS_FILE="${RESOURCES_PATH}/allowed_warnings.txt"



REPORT="${E2E_OUTPUT}/testreport"
ARTIFACTS="${E2E_OUTPUT}/artifacts"

if [[ "${OSTYPE}" = "msys" ]]; then
  PYTHON_EXE="${PYTHON_WINDOWS_EXE}"
  EXE_SUFFIX=".exe"
else
  PYTHON_EXE="python3"
  EXE_SUFFIX=""
fi

# workaround as there is an error in the opSimulation v1.1.0
export LD_LIBRARY_PATH="${WORKSPACE}/dist/opSimulation/lib"

# run pyOpenPASS on e2e-tests
"${PYTHON_EXE}" -m pytest -n auto -vvv --simulation="${WORKSPACE}/dist/opSimulation/opSimulation${EXE_SUFFIX}" --mutual="${COMMON_CONFIGS}" --resources="${SPECIALIZED_CONFIGS}" --report-path="${REPORT}" --artifacts-path="${ARTIFACTS}" --allowed-warnings="${ALLOWED_WARNINGS_FILE}" test.json --junitxml="result_test.xml"
