#!/bin/bash

################################################################################
# Copyright (c) 2023 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script configures cmake
################################################################################

# joins arguments using the cmake list separator (;)
function join_paths()
{
  local IFS=\;
  echo "$*"
}

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

# dependencies built previously
DEPS=(
  "direct_deploy/gtest"
  "direct_deploy/open-simulation-interface"
  "direct_deploy/osiquerylibrary"
  "direct_deploy/protobuf"
  "direct_deploy/stochastics"
  "direct_deploy/libxml2"
  "direct_deploy/libiconv"
  "direct_deploy/units"
)

# preparations for building on Windows/MSYS
if [[ "${OSTYPE}" = "msys" ]]; then
  # set the correct CMake generator
  CMAKE_GENERATOR_ARG="-GMSYS Makefiles"

  # set python command
  if [[ -n "${PYTHON_WINDOWS_EXE}" ]]; then
    CMAKE_PYTHON_COMMAND_ARG="-DPython3_EXECUTABLE=${PYTHON_WINDOWS_EXE}"
  fi

  # prepare dependency paths
  # it seems cmake doesn't like MSYS paths starting with drive letters (e.g. /c/thirdParty/...)
  # cygpath is used here to format the paths in "mixed format" (e.g. c:/thirdparty/...)
  # also, add the mingw64 base path to allow resolving of runtime dependencies during install
  DEPS=(${DEPS[@]/#/$PWD/../repo/deps/conan/})
  OLD_DEPS=(${DEPS[@]})
  OLD_DEPS+=("/mingw64")
  DEPS=()
  for DEP in "${OLD_DEPS[@]}"; do
    DEPS+=("$(cygpath -a -m ${DEP})")
  done
elif [[ ${OSTYPE} =~ $linux ]]; then
  DEPS=(${DEPS[@]/#/$PWD/../repo/deps/conan/})
else
  echo "ERROR: Unable to determine ARCH for thirdParty library selection."
  exit 1
fi

cmake \
  "$CMAKE_GENERATOR_ARG" \
  "$(cat cmake_version_args.txt)" \
  -D CMAKE_PREFIX_PATH="$(join_paths "${DEPS[@]}")" \
  -D CMAKE_INSTALL_PREFIX="$PWD/../dist/scm" \
  -D CMAKE_BUILD_TYPE=Release \
  -D USE_CCACHE=ON \
  -D WITH_COVERAGE=OFF \
  -D WITH_DEBUG_POSTFIX=ON \
  -D WITH_DOC=OFF \
  ../repo
