/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <Stochastics/StochasticsInterface.h>

#include <memory>

#include "../include/module/longitudinalControllerInterface.h"
#include "ScmLongitudinalController.h"

namespace scm::longitudinal_controller::factory
{
inline std::unique_ptr<scm::longitudinal_controller::Interface> Create(StochasticsInterface* stochastics)
{
  return std::unique_ptr<scm::longitudinal_controller::Interface>(scm::longitudinal_controller::Create(stochastics));
}

}  // namespace scm::longitudinal_controller::factory