################################################################################
# Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(SCM_LONGITUDINAL_CONTROLLER_DIR ${CMAKE_CURRENT_LIST_DIR}/src)
add_scm_target(
  NAME ScmLongitudinalController TYPE library LINKAGE static COMPONENT ${COMPONENT}

  HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/ScmLongitudinalController.h
  ${CMAKE_CURRENT_LIST_DIR}/include/ScmLongitudinalControllerFactory.h
  ${CMAKE_CURRENT_LIST_DIR}/src/AlgorithmLongitudinalScmCalculations.h
  ${CMAKE_CURRENT_LIST_DIR}/src/LongitudinalController.h
  ${ROOT_DIR}/src/Logging/Logging.h
  ${ROOT_DIR}/include/common/VehicleProperties.h

  SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/src/ScmLongitudinalController.cpp
  ${SCM_LONGITUDINAL_CONTROLLER_DIR}/AlgorithmLongitudinalScmCalculations.cpp
  ${SCM_LONGITUDINAL_CONTROLLER_DIR}/LongitudinalController.cpp
  ${ROOT_DIR}/src/Logging/Logging.cpp

  LIBRARIES
  Stochastics::Stochastics

  INCDIRS
  ${ROOT_DIR}
  ${CMAKE_CURRENT_LIST_DIR}/include
)
