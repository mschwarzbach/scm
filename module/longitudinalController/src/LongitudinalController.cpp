/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LongitudinalController.h"
using namespace units::literals;
namespace scm
{
scm::signal::LongitudinalControllerOutput LongitudinalController::Trigger(scm::signal::LongitudinalControllerInput input, units::time::millisecond_t time)
{
  cycleTime = input.cycleTime;
  vehicleModelParameters = input.vehicleParameters;
  currentVelocity = units::velocity::meters_per_second_t(input.ownVehicleInformationSCM.absoluteVelocity);
  accelerationWish = input.acceleration + units::make_unit<units::acceleration::meters_per_second_squared_t>(CalculateAirDrag());
  pedalChangeTimeMin = input.driverParameters.pedalChangeTimeMinimum;
  pedalChangeTimeMean = input.driverParameters.pedalChangeTimeMean;
  pedalChangeTimeStdDev = input.driverParameters.pedalChangeTimeStandardDeviation;

  CalculatePedalPositionAndGear(time);

  if (isSpawn)
  {
    isSpawn = false;
  }

  scm::signal::LongitudinalControllerOutput signal;
  signal.accPedalPos = out_accPedalPos;
  signal.brakePedalPos = out_brakePedalPos;
  signal.gear = out_gear;
  return signal;
}

double LongitudinalController::CalculateAirDrag() const
{
  double constexpr roh = 1.2250;  // density
  const auto v = currentVelocity;
  const auto cw = GetAirDragParameter("AirDragCoefficient");
  const auto A = GetAirDragParameter("FrontSurface");
  const auto m = vehicleModelParameters.mass.value();
  if (m == 0)
    throw std::runtime_error("Math error - LongitudinalController::CalculateAirDrag() attempts to divide by zero");
  return 0.5 * roh / m * cw * A * pow(units::unit_cast<double>(v), 2);
}

double LongitudinalController::GetAirDragParameter(std::string paramName) const
{
  if (vehicleModelParameters.properties.count(paramName) > 0)
    return std::stod(vehicleModelParameters.properties.at(paramName));
  else
    throw std::runtime_error("LongitudinalController::GetAirDragParameter tries to access not present vehicle model parameter '" + paramName + "'");
}

void LongitudinalController::SetValuesAtStandstill()
{
  out_gear = 0;
  out_accPedalPos = 0.0;
  out_brakePedalPos = 0.0;
  lastPedalPositionOLC = 0.;
  lastVelocity = currentVelocity;
  lastAccelerationWish = accelerationWish;
  integralOfDeltaVelocity = 0.0_m;
}

void LongitudinalController::CalculatePedalPositionAndGear(units::time::millisecond_t timeStamp)
{
  if (currentVelocity == 0_mps && accelerationWish == 0_mps_sq)
  {
    SetValuesAtStandstill();
  }
  else
  {
    AlgorithmLongitudinalScmCalculations calculations{currentVelocity,
                                                      accelerationWish,
                                                      vehicleModelParameters,
                                                      footPosition,
                                                      lastPedalPositionOLC,
                                                      lastVelocity,
                                                      lastAccelerationWish,
                                                      integralOfDeltaVelocity,
                                                      isSpawn,
                                                      cycleTime};
    calculations.CalculateGearAndEngineSpeed();
    calculations.CalculatePedalPositions();
    calculations.ApplyFirstOrderTransientBehaviour();
    lastPedalPositionOLC = calculations.GetAcceleratorPedalPosition() - calculations.GetBrakePedalPosition();
    calculations.ApplyClosedLoopController();
    calculations.CheckForPedalChange();
    FootPosition newFootPosition = calculations.GetFootPosition();
    if (newFootPosition == FootPosition::ChangingToAcceleratorPedal || newFootPosition == FootPosition::ChangingToBrakePedal)
    {
      if ((newFootPosition == FootPosition::ChangingToAcceleratorPedal && footPosition != FootPosition::ChangingToAcceleratorPedal) || (newFootPosition == FootPosition::ChangingToBrakePedal && footPosition != FootPosition::ChangingToBrakePedal))
      {
        timeStempPedalChangeFinished = timeStamp + GetPedalChangeTime();
      }
      if (timeStamp < timeStempPedalChangeFinished)
      {
        calculations.PedalChangeOngoing();
      }
      else
      {
        calculations.PedalChangeFinished();
      }
    }

    out_gear = calculations.GetGear();
    out_accPedalPos = calculations.GetAcceleratorPedalPosition();
    out_brakePedalPos = calculations.GetBrakePedalPosition();
    footPosition = calculations.GetFootPosition();
    lastVelocity = currentVelocity;
    lastAccelerationWish = accelerationWish;
    integralOfDeltaVelocity = units::length::meter_t(calculations.GetIntegralOfDeltaVelocity());
  }
}

units::time::millisecond_t LongitudinalController::GetPedalChangeTime()
{
  return units::math::max(pedalChangeTimeMin, units::make_unit<units::time::millisecond_t>(stochastics->GetLogNormalDistributed(pedalChangeTimeMean.value(), pedalChangeTimeStdDev.value())));
}

}  // namespace scm