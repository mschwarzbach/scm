/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "../include/ScmLongitudinalController.h"

#include "LongitudinalController.h"

namespace scm::longitudinal_controller
{
Interface* Create(StochasticsInterface* stochastics)
{
  return new scm::LongitudinalController(stochastics);
}
}  // namespace scm::longitudinal_controller