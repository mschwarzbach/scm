/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  AlgorithmLongitudinalScmCalculations.h
#pragma once

#include <cmath>
#include <functional>
#include <vector>

#include "include/common/VehicleProperties.h"

//! @brief Position of the right feet
enum class FootPosition
{
  AcceleratorPedal,            // Right foot is on the accelerator pedal
  BrakePedal,                  // Right foot is on the brake pedal
  ChangingToAcceleratorPedal,  // Right foot is changing from brake pedal to accelerator pedal
  ChangingToBrakePedal,        // Right foot is changing from accelerator pedal to brake pedal
  NotYetActing                 // Right foot has not set itself on a pedal yet
};

//! @brief This class does all the calculations in the AlgorithmLongitudinalSCM module.
//! Based on the current velocity of the agent and a desired acceleration, this class calculates the
//! gear and the pedal positions required to achieve this acceleration. These pedal positions are
//! not changed instant. Instead the depend on the last positions via an closed loop controller.
//! In addition to that changing from the accelerator pedal to the brake pedal and vice versa requires
//! a certain amount of time.
//! The AlgorithmLongitudinal constructs an instance of this class with the parameters, then calls the
//! calculate functions and then acquires the results be calling the respective getter.

//! @brief This file contains the calculations
class AlgorithmLongitudinalScmCalculations
{
public:
  //! @brief Default constructor for AlgorithmLongitudinalScmCalculations
  //! @param velocity                 current velocity of the agent
  //! @param accelerationWish         desired acceleration (can be negative)
  //! @param vehicleModelParameters   parameters of the vehicle model
  //! @param footPosition             current position of the right foot
  //! @param lastPedalPosition        pedal position in the last timeStep
  //! @param lastVelocity             velocity in the last timeStep
  //! @param lastAccelerationWish     desired acceleration in the last timeStep
  //! @param integralOfDeltaVelocity  integral of wish to actual velocity
  //! @param isSpawn                  spawn flag
  //! @param cycleTime                cycleTime of the module
  AlgorithmLongitudinalScmCalculations(units::velocity::meters_per_second_t velocity,
                                       units::acceleration::meters_per_second_squared_t accelerationWish,
                                       const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters,
                                       FootPosition footPosition,
                                       double lastPedalPosition,
                                       units::velocity::meters_per_second_t lastVelocity,
                                       units::acceleration::meters_per_second_squared_t lastAccelerationWish,
                                       units::length::meter_t integralOfDeltaVelocity,
                                       bool isSpawn,
                                       units::time::millisecond_t cycleTime)
      : _velocity(velocity),
        _accelerationWish(accelerationWish),
        _vehicleModelParameters(vehicleModelParameters),
        _footPosition(footPosition),
        _lastPedalPositionOLC(lastPedalPosition),
        _lastVelocity(lastVelocity),
        _integralOfDeltaVelocity(integralOfDeltaVelocity),
        isSpawn(isSpawn),
        _lastAccelerationWish(lastAccelerationWish),
        _cycleTime(cycleTime)
  {
  }

  //! @brief Test constructor
  //! @param velocity                 current velocity of the agent
  //! @param accelerationWish         desired acceleration (can be negative)
  //! @param vehicleModelParameters   parameters of the vehicle model
  //! @param brakePedalPosition       current position of the right foot
  //! @param acceleratorPedalPosition pedal position in the last timeStep
  //! @param lastPedalPositionOLC     last pedal position
  //! @param lastVelocity             velocity in the last timeStep
  //! @param lastAccelerationWish     desired acceleration in the last timeStep
  //! @param integralOfDeltaVelocity  integral of wish to actual velocity
  //! @param isSpawn                  spawn flag
  //! @param cycleTime                cycleTime of the module
  AlgorithmLongitudinalScmCalculations(units::velocity::meters_per_second_t velocity,
                                       units::acceleration::meters_per_second_squared_t accelerationWish,
                                       const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters,
                                       double brakePedalPosition,
                                       double acceleratorPedalPosition,
                                       double lastPedalPositionOLC,
                                       units::velocity::meters_per_second_t lastVelocity,
                                       units::acceleration::meters_per_second_squared_t lastAccelerationWish,
                                       units::length::meter_t integralOfDeltaVelocity,
                                       bool isSpawn,
                                       int cycleTime)
      : _velocity(velocity),
        _accelerationWish(accelerationWish),
        _vehicleModelParameters(vehicleModelParameters),
        _lastPedalPositionOLC(lastPedalPositionOLC),
        _lastVelocity(lastVelocity),
        _lastAccelerationWish(lastAccelerationWish),
        _integralOfDeltaVelocity(integralOfDeltaVelocity),
        isSpawn(isSpawn),
        _cycleTime(units::time::millisecond_t(cycleTime))
  {
    this->brakePedalPosition = brakePedalPosition;
    this->acceleratorPedalPosition = acceleratorPedalPosition;
  }

  /******* <copy from previously inherited public AlgorithmLongitudinalCalculations> **************/

  //! @brief Calculates the necessary gear and engine to achieve the desired acceleration at the current velocity
  void CalculateGearAndEngineSpeed();

  //! @brief Calculates the necessary accelerator pedal and brake pedal position to achieve desired acceleration
  //! depending on the gear. Therefor CalculateGearAndEngineSpeed needs to be called first.
  void CalculatePedalPositions();

  //! @brief Determine position of brake pedal in percent
  //! @return Calculated position of brake pedal in percent
  double GetBrakePedalPosition() const;

  //! @brief Determine position of accelerator pedal in percent
  //! @return Calculated position of accelerator pedal in percent
  double GetAcceleratorPedalPosition() const;

  //! @brief Determine engine speed
  //! @return Calculated engine speed
  units::angular_velocity::revolutions_per_minute_t GetEngineSpeed() const;

  //! @brief Determine gear
  //! @return Calculated gear
  int GetGear() const;

  //! @brief Calculates the acceleration that will result from the engineTorque at the given gear
  //! @param engineTorque
  //! @param chosenGear
  //! @return acceleration from the engine torque at the given gear
  units::acceleration::meters_per_second_squared_t GetAccFromEngineTorque(const units::torque::newton_meter_t& engineTorque, int chosenGear);

  //! @brief Calculates the engine speed required to drive with the given velocity at the specified gear
  //! @param velocity
  //! @param gear
  //! @return engine speed required to drive with the given velocity at the specified gear
  units::angular_velocity::revolutions_per_minute_t GetEngineSpeedByVelocity(const units::velocity::meters_per_second_t& xVel, int gear);

  //! @brief Checks whether the engineSpeed and acceleration/torque can be achieved by the engine
  //! @param engineSpeed  velocity
  //! @param gear         gear number
  //! @param acceleration acceleration
  //! @return engine speed required to drive with the given velocity at the specified gear
  bool isWithinEngineLimits(int gear, const units::angular_velocity::revolutions_per_minute_t& engineSpeed, const units::acceleration::meters_per_second_squared_t& acceleration);

  //! @brief Checks if the engine speed is with in engine limits
  //! @param engineSpeed speed of the engine
  //! @return True, if engine speed is within engine limits
  inline bool isEngineSpeedWithinEngineLimits(const units::angular_velocity::revolutions_per_minute_t& engineSpeed);

  //! @brief Checks whether the engineSpeed and acceleration/torque can be achieved by the engine
  //! @param torque       Engine torque
  //! @param engineSpeed  Engine speed
  //! @return engine speed required to drive with the given velocity at the specified gear
  bool isTorqueWithinEngineLimits(const units::torque::newton_meter_t& torque, const units::angular_velocity::revolutions_per_minute_t& engineSpeed);

  //! @brief Get the maximum possible engineTorque for the given engineSpeed
  //! @param engineSpeed Engine speed
  //! @return the maximum possible engineTorque for the given engineSpeed
  units::torque::newton_meter_t GetEngineTorqueMax(const units::angular_velocity::revolutions_per_minute_t& engineSpeed);

  //! @brief Get the minimum engineTorque (i.e. maximum drag) for the given engineSpeed
  //! @param engineSpeed Engine speed
  //! @return Returns the minimum engineTorque (i.e. maximum drag) for the given engineSpeed
  units::torque::newton_meter_t GetEngineTorqueMin(const units::angular_velocity::revolutions_per_minute_t& engineSpeed);

  //! @brief Calculates the engine torque required to achieve the given acceleration at the specified gear
  //! @param gear          Gear Number
  //! @param acceleration  Acceleration
  //! @return the engine torque required to achieve the given acceleration at the specified gear
  units::torque::newton_meter_t GetEngineTorqueAtGear(int gear, const units::acceleration::meters_per_second_squared_t& acceleration);

private:
  //! @brief Function to return the property value
  //! @param propertyName Name of the property
  //! @return Value of the property
  double GetVehicleProperty(const std::string& propertyName);

  //! @brief velocity
  units::velocity::meters_per_second_t _velocity{0.0};
  //! @brief desired accelerations
  units::acceleration::meters_per_second_squared_t _accelerationWish{0.0};
  //! @brief vehicle properties
  const scm::common::vehicle::properties::EntityProperties _vehicleModelParameters;

  //! @brief gear number
  int gear{1};
  //! @brief engine speed
  units::angular_velocity::revolutions_per_minute_t _engineSpeed{0.0};
  //! @brief position of the break pedal
  double brakePedalPosition{0.0};
  //! @brief position of accelerator pedal
  double acceleratorPedalPosition{0.0};

  /******* </copy from previously inherited public AlgorithmLongitudinalCalculations> *************/

public:
  bool IsMajorBrake(double currentVal);
  void ApplyFirstOrderTransientBehaviour();
  void ApplyClosedLoopController();

  //! @brief Check for a pedal change in current situation
  void CheckForPedalChange();

  //! @brief Set the new foot position after the pedal change is finished
  void PedalChangeFinished();

  //! @brief Set pedal position to zero will foot is changing pedals
  void PedalChangeOngoing();

  //! @brief Get the current foot position
  //! @return the current foot position
  FootPosition GetFootPosition() const;

  //! @brief Determine the integral of delta between wish and actual velocity
  //! @return the integral of delta between wish and actual velocity
  units::length::meter_t GetIntegralOfDeltaVelocity() const;

private:
  //! @brief
  //! @param xVel
  //! @param lastXVel
  //! @param lastIn_aVehicle
  //! @param cycleTime
  //! @return
  double ClosedLoopController(units::velocity::meters_per_second_t xVel,
                              units::velocity::meters_per_second_t lastXVel,
                              units::acceleration::meters_per_second_squared_t lastIn_aVehicle,
                              units::time::millisecond_t cycleTime);
  //! @brief _integralOfDeltaVelocity
  units::length::meter_t _integralOfDeltaVelocity{0.0};
  //! @brief _isSpawn
  bool isSpawn{false};
  //! @brief _footPosition
  FootPosition _footPosition{FootPosition::NotYetActing};
  //! @brief _lastPedalPositionOLC
  double _lastPedalPositionOLC{0.0};
  //! @brief _lastVelocity
  units::velocity::meters_per_second_t _lastVelocity{0.0};
  //! @brief _lastAccelerationWish
  units::acceleration::meters_per_second_squared_t _lastAccelerationWish{0.0};
  //! @brief _cycleTime
  units::time::millisecond_t _cycleTime{0};
};
