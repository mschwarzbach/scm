/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include <memory>
#include <optional>
#include <string>

#include "LaneGeometryExtractorInterface.h"
#include "SensorTypes.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
class LaneGeometryExtractor : public LaneGeometryExtractorInterface
{
public:
  LaneGeometryExtractor(const scm::VehicleState& state, units::length::meter_t visibilityDistance)
      : _state{state},
        _visibilityDistance{visibilityDistance}
  {
  }
  const GeometryInformationSCM& Update(units::length::meter_t previewDistance, OwnVehicleInformationSCM* ownVehicleInformation, TrafficRuleInformationSCM* trafficRulesInformation, const scm::HighwayExitInformation& highwayExitInformation) override;

private:
  const scm::VehicleState& _state;
  units::length::meter_t _visibilityDistance{};
  GeometryInformationSCM _laneInformation;
  scm::DistancesToEndOfLane GetPathEndDistances(const osiql::RoadPoint& start) const;

  //! \brief Position to move a sign from back to front (at spawn)
  static constexpr units::length::meter_t DISTANCE_TO_VIRTUAL_SIGN{1._m};

  //! \brief Get traffic rules sensor data from the requested lane.
  //! \param [in] relativeLaneId      Lane id relative to ego lane (left lane --> 1, ego lane --> 0, right lane --> -1)
  //! \return Traffic rules lane information of the requested lane
  LaneInformationGeometrySCM GetGeometryLaneInformation(const int relativeLaneId, units::length::meter_t previewDistance, OwnVehicleInformationSCM* ownVehicleInformation);

};
}  // namespace scm