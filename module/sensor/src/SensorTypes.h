/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include <algorithm>

#include "OsiEnumConverter.h"
#include "include/common/SensorDriverScmDefinitions.h"

using namespace units::literals;
namespace scm
{
using Route = osiql::Route;
using Lane = osiql::Lane;

struct DistancesToEndOfLane
{
  units::length::meter_t normal;
  units::length::meter_t emergency;
};

//! \brief State of a vehicle. Holds the vehicle, its route, current position and world query (for route renewal)
struct VehicleState
{
  //! Creates a vehicle state without a route
  //!
  //! \param Query
  VehicleState(const osiql::Query&, osiql::Id vehicleId, units::length::meter_t visibilityDistance);

  //! Constructs a vehicle state of the query's host vehicle and the shortest route from it to the given destination
  //!
  //! \param Query
  //! \param destination
  VehicleState(const osiql::Query&, osiql::Id vehicleId, const osiql::Point<const osiql::Lane>& destination, units::length::meter_t visibilityDistance);

  //! Returns the position and angle of the front bumper of the vehicle
  //! relative to the road it is on.
  //!
  //! \return const osiql::Pose<osiql::Point<const Lane>>&
  const osiql::Pose<osiql::Point<const Lane>>& GetPose() const;

  //! Updates the local pose of the host vehicle's front bumper. Fails if this state has no route.
  //!
  void UpdatePose();

  OngoingTrafficObjectsSCM GetOngoingTraffic() const;

  //! Returns a vector of all objects of the given type paired with their
  //! longitudinal distance along the route from the state's vehicle's front bumper
  //! within the given distances
  //!
  //! \tparam T osiql::MovingObject or osiql::StaticObject
  //! \param startDistance
  //! \param endDistance
  //! \return std::vector<std::pair<const T*, double>>
  template <typename T>
  std::vector<std::pair<const T*, double>> GetAllSurrounding(units::length::meter_t startDistance, units::length::meter_t endDistance) const;

  template <typename T>
  void AddSurroundingTraffic(OngoingTrafficObjectsSCM&) const;

  template <typename T>
  ObjectInformationSCM GetObjectInformationSCM(const T&) const;

  template <typename T>
  units::length::meter_t GetDistanceFromFrontBumperToRearAxle(const T& object) const;

  template <typename T>
  units::length::meter_t GetLongitudinalDistanceForPerception(const T& object) const;

  std::vector<units::length::meter_t> ProjectPoint(units::length::meter_t longitude, units::length::meter_t latidude) const;

  template <typename T>
  ObstructionScm GetLateralObstruction(const T&, const osiql::Point<const Lane>&) const;

  template <typename T>
  ObstructionLongitudinal GetLongitudinalObstruction(const T&) const;

  osiql::Vector2d GetDriverPosition() const;

  template <typename T>
  OrientationRelativeToDriver GetOrientationRelativeToDriver(const T& target, const osiql::Vector2d& targetFrontXY) const;

  //! \brief World query stored to allow route renewal
  const osiql::Query& query;

  const osiql::Vehicle* vehicle;

  //! \brief Set of points outlining an intended path
  osiql::Route route;

  units::length::meter_t visibilityDistance;

private:
  template <typename T>
  void FillMovingObjectData(ObjectInformationSCM&, const T&, const osiql::Pose<osiql::Point<const osiql::Lane>>&) const;

  //! \brief Local front bumper pose of the vehicle on the route. TODO: Initialize
  //! on construction to avoid the need for a wrapper (and because RAII is idiomatic)
  std::optional<osiql::Pose<osiql::Point<const osiql::Lane>>> pose;

  static constexpr auto DRIVER_LONGITUDINAL_POSITION_RELATIVE_TO_VEHICLE{-1._m};
  static constexpr auto DRIVER_LATERAL_POSITION_RELATIVE_TO_VEHICLE{-0.75_m};
};
}  // namespace scm

namespace scm
{

template <typename T>
std::vector<std::pair<const T*, double>> VehicleState::GetAllSurrounding(units::length::meter_t startDistance, units::length::meter_t endDistance) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);
  const auto offset{units::make_unit<units::length::meter_t>(route.GetDistance(GetPose()))};
  auto objects{route.FindAll<T>(  // clang-format off
    [&, start = startDistance + offset, end = endDistance + offset](const auto& object, double distance) {
            return (object.GetId() != vehicle->GetId()) && (start.value() <= distance + osiql::Length(object)) && (distance < end.value());
    }
  )};  // clang-format on

  // filter out ego vehicle as it is not considered as 'surrounding'
  objects.erase(std::remove_if(objects.begin(), objects.end(), [&](const auto& surrounding)
                               { return vehicle->GetId() == surrounding.first->GetId(); }),
                objects.end());
  for (auto& [object, distance] : objects)
  {
    distance -= offset.value();
  }
  return objects;
}

template <typename T>
void VehicleState::AddSurroundingTraffic(OngoingTrafficObjectsSCM& traffic) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);

  const osiql::Side side{GetPose().GetLane().GetSideOfRoad()};
  const int ownIndex{static_cast<int>(GetPose().GetLane().GetIndex(side))};

  auto objects{GetAllSurrounding<T>(-visibilityDistance, visibilityDistance)};
  const std::array<units::length::meter_t, 4> ranges{-visibilityDistance, units::make_unit<units::length::meter_t>(-vehicle->GetLength()), 0_m, visibilityDistance};
  auto range{std::next(ranges.begin())};
  for (const auto& data : objects)
  {
    const auto& object{*data.first};
    const auto distance{units::make_unit<units::length::meter_t>(data.second)};
    const auto endDistance{units::make_unit<units::length::meter_t>(data.second + object.GetLength())};
    while (*range <= distance)
    {
      ++range;
    }
    size_t row{static_cast<size_t>(std::distance(ranges.begin(), range))};
    // std::cout << "Range: " << *range << ", Start: " << distance << ", End: " << endDistance << '\n';
    if (row == 1 && (*range < endDistance))
    {
      ++row;
    }

    // TODO: Overlapping lanes of different roads not handled
    const auto& overlaps{object.template GetOverlaps<osiql::Lane>()};
    const auto it{std::min_element(overlaps.begin(), overlaps.end(), [&side, ownIndex](const osiql::Overlap<Lane>& a, const auto& b) {  // clang-format off
      return std::abs(static_cast<int>(a.GetLane().GetIndex(side)) - ownIndex) //
           < std::abs(static_cast<int>(b.GetLane().GetIndex(side)) - ownIndex);
    })};  // clang-format on
    const int nearestIndex{static_cast<int>(it->GetLane().GetIndex(side))};
    const int offset{ownIndex - nearestIndex};
    if (offset >= -2 && offset <= 2)
    {
      const size_t column{OngoingTrafficObjectsSCM::Index::CLOSE + offset};
      ObjectInformationSCM& emplacement{traffic[row][column].emplace_back(GetObjectInformationSCM(object))};
      traffic.allObjects.insert(&emplacement);
    }
  }
}

template <typename T>
ObjectInformationSCM VehicleState::GetObjectInformationSCM(const T& object) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);

  const osiql::Pose<osiql::Vector2d> globalPose{object.GetXY(object.GetSize().Times(osiql::Anchor::FRONT)), object.GetYaw()};
  const osiql::Pose<osiql::Point<const osiql::Lane>> frontBumperPosition{route.Localize(globalPose)};

  ObjectInformationSCM result{
      .id = static_cast<int>(object.GetId()),
      .exist = true,
      .isStatic = std::is_same_v<T, osiql::StaticObject>,
      .absoluteVelocity = 0.0_mps,
      .acceleration = 0.0_mps_sq,
      .heading = frontBumperPosition.angle,
      .length = units::make_unit<units::length::meter_t>(object.GetLength()),
      .width = units::make_unit<units::length::meter_t>(object.GetWidth()),
      .height = units::make_unit<units::length::meter_t>(object.GetHeight()),
      .relativeLongitudinalDistance = units::make_unit<units::length::meter_t>(std::abs(vehicle->GetDistanceTo(object))),
      .obstruction = GetLateralObstruction(object, frontBumperPosition),
      .lateralPositionInLane = units::make_unit<units::length::meter_t>(frontBumperPosition.GetDistanceFromCenterline()),
      .longitudinalVelocity = 0.0_mps,
      .lateralVelocity = 0.0_mps,
      .orientationRelativeToDriver = GetOrientationRelativeToDriver(object, globalPose),
      .longitudinalObstruction = GetLongitudinalObstruction(object),
      .longitudinalDistanceFrontBumperToRearAxle = GetLongitudinalDistanceForPerception(object),
      .relativeX = 0.0_m,
      .relativeY = 0.0_m,
  };

  result.relativeLateralDistance = result.obstruction.mainLaneLocator;
  auto distanceXY = ProjectPoint(result.longitudinalDistanceFrontBumperToRearAxle, result.relativeLateralDistance);

  result.relativeX = distanceXY.at(0);
  result.relativeY = distanceXY.at(1);

  FillMovingObjectData(result, object, frontBumperPosition);

  return result;
}

template <typename T>
void VehicleState::FillMovingObjectData(ObjectInformationSCM& data, const T& object, const osiql::Pose<osiql::Point<const osiql::Lane>>& frontBumperPosition) const
{
  if constexpr (std::is_base_of_v<osiql::MovingObject, T>)
  {
    data.absoluteVelocity = units::make_unit<units::velocity::meters_per_second_t>(object.GetVelocity().Length());

    double laneAngle = 0;

    if (vehicle->GetDistanceTo(object) < 0)
    {
      laneAngle = frontBumperPosition.GetLane().GetAngle(GetPose().latitude + vehicle->GetDistanceTo(object) - vehicle->GetLength());
    }
    else
    {
      laneAngle = frontBumperPosition.GetLane().GetAngle(GetPose().latitude + vehicle->GetDistanceTo(object) + object.GetLength());
    }

    data.longitudinalVelocity = static_cast<units::velocity::meters_per_second_t>(static_cast<osiql::Vector2d>(object.GetVelocity()).ProjectionLength(laneAngle));
    data.lateralVelocity = static_cast<units::velocity::meters_per_second_t>(static_cast<osiql::Vector2d>(object.GetVelocity()).ProjectionLength(laneAngle + M_PI_2));

    // Todo: Consider laneAngle instead of egoYaw
    data.acceleration = units::make_unit<units::acceleration::meters_per_second_squared_t>(static_cast<osiql::Vector2d>(object.GetAcceleration()).ProjectionLength(vehicle->GetYaw()));

    {  // Distance to left & right boundary
      const auto [left, right]{object.GetApproximateBoundsDistancesToBoundaries(frontBumperPosition.GetLane())};
      data.distanceToLaneBoundaryLeft = units::make_unit<units::length::meter_t>(left);
      data.distanceToLaneBoundaryRight = units::make_unit<units::length::meter_t>(right);
    }

    // TODO: Avoid type comparison & cast overhead by overloading this method for vehicles
    if (object.GetType() == osiql::MovingObject::Type::Vehicle)
    {
      const osiql::Vehicle& otherVehicle{*static_cast<const osiql::Vehicle*>(&object)};
      data.vehicleClassification = TranslateVehicleType(otherVehicle.GetVehicleType());
      data.brakeLightsActive = TranslateBrakeLightState(otherVehicle.GetBrakeLightState());
      data.indicatorState = TranslateIndicatorState(otherVehicle.GetIndicatorState());
      data.collision = query.FindOverlapping<osiql::MovingObject>(object).size() > 1  //
                       || !query.FindOverlapping<osiql::StaticObject>(object).empty();
    }
    else
    {
      data.vehicleClassification = scm::common::VehicleClass::kOther;
    }
  }
}

template <typename T>
ObstructionScm VehicleState::GetLateralObstruction(const T& target, const osiql::Point<const osiql::Lane>& movingObjectFront) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);

  const auto laneChain{vehicle->GetLaneChain(route)};
  const auto [rightObstruction, leftObstruction]{vehicle->GetObstruction(target, laneChain)};
  const auto otherDeviation{movingObjectFront.GetDeviation(laneChain)};
  const auto selfDeviation{GetPose().GetDeviation(laneChain)};
  const auto mlpDistance{otherDeviation - selfDeviation};
  return {units::make_unit<units::length::meter_t>(leftObstruction), units::make_unit<units::length::meter_t>(-rightObstruction), units::make_unit<units::length::meter_t>(mlpDistance)};
}

template <typename T>
ObstructionLongitudinal VehicleState::GetLongitudinalObstruction(const T& target) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);

  osiql::Interval<double> ownDistance{route.GetDistance(*vehicle)};
  osiql::Interval<double> targetDistance{route.GetDistance(target)};
  return {
      units::make_unit<units::length::meter_t>(targetDistance.max - ownDistance.min),  // Overtaking distance (rear bumper to front bumper)
      units::make_unit<units::length::meter_t>(ownDistance.max - targetDistance.min),  // Retrieval Distance (Front Bumper to Rear Bumper)
      units::make_unit<units::length::meter_t>(targetDistance.max - ownDistance.max)   // General distance (front bumper to front bumper)
  };
}

template <typename T>
OrientationRelativeToDriver VehicleState::GetOrientationRelativeToDriver(const T& target, const osiql::Vector2d& targetFrontXY) const
{
  static_assert(std::is_base_of_v<osiql::MovingObject, T> || std::is_same_v<T, osiql::StaticObject>);

  const osiql::Shape& shape{target.shape};
  assert(shape.size() == 4);
  const osiql::Vector2d driverXY{GetDriverPosition()};

  const auto getAngleTo = [&](const osiql::Vector2d& driverXY, const osiql::Vector2d& targetXY)
  {
    return osiql::WrapAngle((targetXY - driverXY).Angle() - vehicle->GetYaw());
  };

  return OrientationRelativeToDriver{
      .rearRight = units::make_unit<units::angle::radian_t>(getAngleTo(driverXY, shape[0])),
      .rearLeft = units::make_unit<units::angle::radian_t>(getAngleTo(driverXY, shape[3])),
      .frontLeft = units::make_unit<units::angle::radian_t>(getAngleTo(driverXY, shape[2])),
      .frontRight = units::make_unit<units::angle::radian_t>(getAngleTo(driverXY, shape[1])),
      .frontCenter = units::make_unit<units::angle::radian_t>(getAngleTo(driverXY, targetFrontXY))  //
  };
}

template <typename T>
units::length::meter_t VehicleState::GetDistanceFromFrontBumperToRearAxle(const T& object) const
{
  auto& overlapObject = object.template GetOverlaps<osiql::Road>()[0];
  auto frontPoint = overlapObject.GetRoad().Localize(object.GetXY(osiql::Anchor::FRONT.Times(object.GetSize())));
  auto rearPoint = overlapObject.GetRoad().Localize(object.GetXY(osiql::Anchor::REAR.Times(object.GetSize())));

  if constexpr (std::is_same_v<T, osiql::MovingObject>)
  {
    if (object.GetType() == osiql::MovingObject::Type::Vehicle)
    {
      rearPoint = overlapObject.GetRoad().Localize(static_cast<const osiql::Vehicle&>(object).GetAxleXY<osiql::Axle::Rear>());
    }
  }

  return units::make_unit<units::length::meter_t>(rearPoint.GetDistanceTo(frontPoint));
}

template <typename T>
units::length::meter_t VehicleState::GetLongitudinalDistanceForPerception(const T& object) const
{
  auto longitudinaldistance = units::make_unit<units::length::meter_t>(vehicle->GetDistanceTo(object));

  if (scm::common::DoubleEquality(longitudinaldistance.value(), 0))
  {
    return -GetDistanceFromFrontBumperToRearAxle(object);
  }
  else if (longitudinaldistance > 0_m)
  {
    return longitudinaldistance + units::make_unit<units::length::meter_t>(object.GetLength()) - GetDistanceFromFrontBumperToRearAxle(object);
  }
  return longitudinaldistance - units::make_unit<units::length::meter_t>(vehicle->GetLength()) - GetDistanceFromFrontBumperToRearAxle(object);
}

inline std::vector<units::length::meter_t> VehicleState::ProjectPoint(units::length::meter_t longitude, units::length::meter_t latidude) const
{
  auto pose = GetPose();
  auto projection = route.ProjectPoint(pose, longitude.value());

  if (projection.has_value())
  {
    projection->longitude += latidude.value();
    auto distanceXY = projection->GetXY() - pose.GetXY();

    return {units::make_unit<units::length::meter_t>(distanceXY.x), units::make_unit<units::length::meter_t>(distanceXY.y)};
  }

  return {units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE), units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)};
}

}  // namespace scm
