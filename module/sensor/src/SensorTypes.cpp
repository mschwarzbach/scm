/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SensorTypes.h"

#include <memory>

namespace scm
{
VehicleState::VehicleState(const osiql::Query& query, osiql::Id vehicleId, units::length::meter_t visibilityDistance)
    : query{query}, vehicle{static_cast<const osiql::Vehicle*>(query.GetMovingObject(vehicleId))}, visibilityDistance{visibilityDistance.value()}
{
  const osiql::Pose<osiql::Vector2d> frontBumperXY{vehicle->GetXY(vehicle->GetSize().Times(osiql::Anchor::FRONT)), vehicle->GetYaw()};
  const auto localizations{query.Localize(frontBumperXY)};
  if (!localizations.empty())
  {
    pose = localizations.front();
  }
}

VehicleState::VehicleState(const osiql::Query& query, osiql::Id vehicleId, const osiql::Point<const Lane>& destination, units::length::meter_t visibilityDistance)
    : VehicleState(query, vehicleId, visibilityDistance)
{
  route = GetPose().GetRoute<osiql::Orientation::Forward>(destination);
}

void VehicleState::UpdatePose()
{
  const osiql::Pose<osiql::Vector2d> globalFrontBumper{
      vehicle->GetXY(vehicle->GetSize().Times(osiql::Anchor::FRONT)),
      vehicle->GetYaw()  //
  };
  pose = route.Localize(globalFrontBumper);
}

const osiql::Pose<osiql::Point<const Lane>>& VehicleState::GetPose() const
{
  assert(pose.has_value());
  return pose.value();
}

osiql::Vector2d VehicleState::GetDriverPosition() const
{
  const osiql::Vector2d centerToRearAxle{vehicle->GetAxleOffset<osiql::Axle::Rear>()};
  return vehicle->GetTransformationMatrix() * osiql::Vector2d{// clang-format off
      (vehicle->GetLength() * 0.5) + centerToRearAxle.x + DRIVER_LONGITUDINAL_POSITION_RELATIVE_TO_VEHICLE.value(),
      DRIVER_LATERAL_POSITION_RELATIVE_TO_VEHICLE.value()
  };  // clang-format on
}

OngoingTrafficObjectsSCM VehicleState::GetOngoingTraffic() const
{
  OngoingTrafficObjectsSCM result;
  for (auto& row : result)
  {
    for (auto& cell : row)
    {
      cell.reserve(4);  // FIXME: result.allObjects can be invalidated if surpassed
    }
  }
  AddSurroundingTraffic<osiql::MovingObject>(result);
  AddSurroundingTraffic<osiql::StaticObject>(result);
  return result;
}
}  // namespace scm
