/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include "SensorTypes.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
class RouteManagerInterface
{
public:
  virtual ~RouteManagerInterface() = default;
  virtual scm::HighwayExitInformation ExtractHighwayExitInformation(TrafficRuleInformationSCM* trafficRulesInformation) = 0;
};
}  // namespace scm
