/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SurroundingTrafficUpdater.h"

#include <OsiQueryLibrary/osiql.h>

#include <cmath>

#include "OsiEnumConverter.h"

namespace scm
{
const SurroundingObjectsSCM& SurroundingTrafficUpdater::Update(units::length::meter_t previewDistance)
{
  _surroundingObjects.ongoingTraffic = _state.GetOngoingTraffic();
  //  _surroundingObjects.virtualTraffic = _virtualTrafficUpdater->GetVirtualAgentInformation(previewDistance);
  return _surroundingObjects;
}
}  // namespace scm
