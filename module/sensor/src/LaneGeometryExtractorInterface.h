/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "SensorTypes.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
class LaneGeometryExtractorInterface
{
public:
  virtual ~LaneGeometryExtractorInterface() = default;
  virtual const GeometryInformationSCM& Update(units::length::meter_t previewDistance, OwnVehicleInformationSCM* ownVehicleInformation, TrafficRuleInformationSCM* trafficRulesInformation, const scm::HighwayExitInformation& highwayExitInformation) = 0;
};
}  // namespace scm
