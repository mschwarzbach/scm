/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>

#include "RouteManagerInterface.h"
#include "SensorTypes.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
//! \brief Holds current route, updates the route if required and calculates highway exit information
class RouteManager : public RouteManagerInterface
{
public:
  RouteManager(const scm::VehicleState& state)
      : _state{state}
  {
  }

  scm::HighwayExitInformation ExtractHighwayExitInformation(TrafficRuleInformationSCM* trafficRulesInformation) override;

private:
  bool DetectHighwayExitSign(TrafficRuleInformationSCM* trafficRulesInformation);
  HighwayExitInformation DetermineHighwayExitInformation();
  const scm::VehicleState& _state;

  bool _highwayExitSignDedected = false;
};
}  // namespace scm