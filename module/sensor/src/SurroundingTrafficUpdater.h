/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include <memory>

#include "OsiEnumConverter.h"
#include "SensorTypes.h"
#include "SurroundingTrafficUpdaterInterface.h"
#include "VirtualTrafficUpdater.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
class SurroundingTrafficUpdater : public SurroundingTrafficUpdaterInterface
{
public:
  explicit SurroundingTrafficUpdater(const scm::VehicleState& state)
      : _state{state}
  /* _virtualTrafficUpdater{std::make_shared<VirtualTrafficUpdater>(state)}*/
  {
  }
  const SurroundingObjectsSCM& Update(units::length::meter_t previewDistance) override;

private:
  const scm::VehicleState& _state;
  //    std::shared_ptr<VirtualTrafficUpdaterInterface> _virtualTrafficUpdater;
  SurroundingObjectsSCM _surroundingObjects;
};
}  // namespace scm