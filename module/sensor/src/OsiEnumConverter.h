/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include "include/common/CommonLaneTypes.h"
#include "include/common/CommonTrafficSigns.h"
#include "include/common/LightState.h"
#include "include/common/VehicleProperties.h"

static const std::map<osiql::Vehicle::VehicleType, scm::common::VehicleClass> VEHICLE_TYPE_MAPPING{
    {osiql::Vehicle::VehicleType::CompactCar, scm::common::VehicleClass::kCompact_car},
    {osiql::Vehicle::VehicleType::SmallCar, scm::common::VehicleClass::kSmall_car},
    {osiql::Vehicle::VehicleType::MediumCar, scm::common::VehicleClass::kMedium_car},
    {osiql::Vehicle::VehicleType::LuxuryCar, scm::common::VehicleClass::kLuxury_car},
    {osiql::Vehicle::VehicleType::MediumCar, scm::common::VehicleClass::kMedium_car},
    {osiql::Vehicle::VehicleType::DeliveryVan, scm::common::VehicleClass::kDelivery_van},
    {osiql::Vehicle::VehicleType::HeavyTruck, scm::common::VehicleClass::kHeavy_truck},
    {osiql::Vehicle::VehicleType::SemiTractor, scm::common::VehicleClass::kHeavy_truck},
    {osiql::Vehicle::VehicleType::Motorbike, scm::common::VehicleClass::kMotorbike},
    {osiql::Vehicle::VehicleType::Bicycle, scm::common::VehicleClass::kBicycle},
    {osiql::Vehicle::VehicleType::Bus, scm::common::VehicleClass::kBus},
    {osiql::Vehicle::VehicleType::Tram, scm::common::VehicleClass::kTram}};

static const std::map<osiql::Vehicle::IndicatorState, scm::LightState::Indicator> MAP_INDICATOR_STATE{
    {osiql::Vehicle::IndicatorState::Left, scm::LightState::Indicator::Left},
    {osiql::Vehicle::IndicatorState::Right, scm::LightState::Indicator::Right},
    {osiql::Vehicle::IndicatorState::Warning, scm::LightState::Indicator::Warn}};

static const std::map<osiql::Lane::Type, scm::LaneType> MAP_TYPE{
    {osiql::Lane::Type::Biking, scm::LaneType::Biking},
    {osiql::Lane::Type::Border, scm::LaneType::Border},
    {osiql::Lane::Type::ConnectingRamp, scm::LaneType::ConnectingRamp},
    {osiql::Lane::Type::Curb, scm::LaneType::Curb},
    {osiql::Lane::Type::Entry, scm::LaneType::Entry},
    {osiql::Lane::Type::Exit, scm::LaneType::Exit},
    {osiql::Lane::Type::Median, scm::LaneType::Median},
    {osiql::Lane::Type::Normal, scm::LaneType::Driving},
    {osiql::Lane::Type::OffRamp, scm::LaneType::OffRamp},
    {osiql::Lane::Type::OnRamp, scm::LaneType::OnRamp},
    {osiql::Lane::Type::Other, scm::LaneType::Undefined},
    {osiql::Lane::Type::Parking, scm::LaneType::Parking},
    {osiql::Lane::Type::Restricted, scm::LaneType::Restricted},
    {osiql::Lane::Type::Shoulder, scm::LaneType::Shoulder},
    {osiql::Lane::Type::Sidewalk, scm::LaneType::Sidewalk},
    {osiql::Lane::Type::Stop, scm::LaneType::Stop},
    {osiql::Lane::Type::Tram, scm::LaneType::Tram}};

static const std::map<osiql::TrafficSign::Type, scm::CommonTrafficSign::Type> MAP_SIGN{
    {osiql::TrafficSign::Type::Unknown, scm::CommonTrafficSign::Undefined},
    {osiql::TrafficSign::Type::GiveWay, scm::CommonTrafficSign::GiveWay},
    {osiql::TrafficSign::Type::Stop, scm::CommonTrafficSign::Stop},
    {osiql::TrafficSign::Type::DoNotEnter, scm::CommonTrafficSign::DoNotEnter},
    {osiql::TrafficSign::Type::EnvironmentalZoneBegin, scm::CommonTrafficSign::EnvironmentalZoneBegin},
    {osiql::TrafficSign::Type::EnvironmentalZoneEnd, scm::CommonTrafficSign::EnvironmentalZoneEnd},
    {osiql::TrafficSign::Type::SpeedLimitBegin, scm::CommonTrafficSign::MaximumSpeedLimit},
    {osiql::TrafficSign::Type::AdvisorySpeedLimitBegin, scm::CommonTrafficSign::MaximumSpeedLimit},
    {osiql::TrafficSign::Type::SpeedLimitZoneBegin, scm::CommonTrafficSign::SpeedLimitZoneBegin},
    {osiql::TrafficSign::Type::SpeedLimitZoneEnd, scm::CommonTrafficSign::SpeedLimitZoneEnd},
    {osiql::TrafficSign::Type::OvertakingBanBegin, scm::CommonTrafficSign::OvertakingBanBegin},
    {osiql::TrafficSign::Type::OvertakingBanForTrucksBegin, scm::CommonTrafficSign::OvertakingBanTrucksBegin},
    {osiql::TrafficSign::Type::SpeedLimitEnd, scm::CommonTrafficSign::EndOfMaximumSpeedLimit},
    {osiql::TrafficSign::Type::OvertakingBanEnd, scm::CommonTrafficSign::OvertakingBanEnd},
    {osiql::TrafficSign::Type::OvertakingBanForTrucksEnd, scm::CommonTrafficSign::OvertakingBanTrucksEnd},
    {osiql::TrafficSign::Type::OvertakingBanEnd, scm::CommonTrafficSign::EndOffAllSpeedLimitsAndOvertakingRestrictions},
    {osiql::TrafficSign::Type::PedestrianBridge, scm::CommonTrafficSign::PedestrianCrossing},
    {osiql::TrafficSign::Type::RightOfWayNextIntersection, scm::CommonTrafficSign::RightOfWayNextIntersection},
    {osiql::TrafficSign::Type::RightOfWayBegin, scm::CommonTrafficSign::RightOfWayBegin},
    {osiql::TrafficSign::Type::RightOfWayEnd, scm::CommonTrafficSign::RightOfWayEnd},
    {osiql::TrafficSign::Type::TownBegin, scm::CommonTrafficSign::TownBegin},
    {osiql::TrafficSign::Type::TownEnd, scm::CommonTrafficSign::TownEnd},
    {osiql::TrafficSign::Type::LivingStreetBegin, scm::CommonTrafficSign::TrafficCalmedDistrictBegin},
    {osiql::TrafficSign::Type::LivingStreetEnd, scm::CommonTrafficSign::TrafficCalmedDistrictEnd},
    {osiql::TrafficSign::Type::HighwayBegin, scm::CommonTrafficSign::HighWayBegin},
    {osiql::TrafficSign::Type::HighwayEnd, scm::CommonTrafficSign::HighWayEnd},
    {osiql::TrafficSign::Type::HighwayExit, scm::CommonTrafficSign::HighWayExit},
    {osiql::TrafficSign::Type::PoleExit, scm::CommonTrafficSign::HighwayExitPole},
    {osiql::TrafficSign::Type::NamedHighwayExit, scm::CommonTrafficSign::AnnounceHighwayExit},
    {osiql::TrafficSign::Type::AnnounceRightLaneEnd, scm::CommonTrafficSign::AnnounceRightLaneEnd},
    {osiql::TrafficSign::Type::AnnounceLeftLaneEnd, scm::CommonTrafficSign::AnnounceLeftLaneEnd},
    {osiql::TrafficSign::Type::HighwayAnnouncement, scm::CommonTrafficSign::AnnounceHighwayExit},
    //{osiql::TrafficSign::Type::HighwayPreannouncementDirections???, scm::CommonTrafficSign::PreannounceHighwayExitDirections}
    //{osiql::TrafficSign::Type::DistanceIndication, scm::CommonTrafficSign::DistanceIndication}
};

static const std::map<osiql::SupplementarySign::Type, scm::CommonTrafficSign::Type> MAP_SUPPLEMENTARY_SIGN{
    {osiql::SupplementarySign::Type::Space, scm::CommonTrafficSign::DistanceIndication}  //
};

inline bool TranslateBrakeLightState(osiql::Vehicle::BrakeLightState state)
{
  if (state == osiql::Vehicle::BrakeLightState::Normal || state == osiql::Vehicle::BrakeLightState::Strong)
  {
    return true;
  }

  return false;
}

inline scm::common::VehicleClass TranslateVehicleType(const osiql::Vehicle::VehicleType& type)
{
  auto it{VEHICLE_TYPE_MAPPING.find(type)};

  if (VEHICLE_TYPE_MAPPING.end() == it)
  {
    return scm::common::VehicleClass::kOther;
  }
  return it->second;
}

inline scm::LightState::Indicator TranslateIndicatorState(osiql::Vehicle::IndicatorState state)
{
  auto it{MAP_INDICATOR_STATE.find(state)};

  if (MAP_INDICATOR_STATE.end() == it)
  {
    return scm::LightState::Indicator::Off;
  }
  return it->second;
}

inline scm::LaneType TranslateLaneType(osiql::Lane::Type type)
{
  auto it{MAP_TYPE.find(type)};

  if (MAP_TYPE.end() == it)
  {
    return scm::LaneType::Undefined;
  }
  return it->second;
}

inline scm::CommonTrafficSign::Type TranslateTrafficSign(const osiql::TrafficSign::Type sign)
{
  auto it{MAP_SIGN.find(sign)};

  if (MAP_SIGN.end() == it)
  {
    return scm::CommonTrafficSign::Undefined;
  }
  return it->second;
}

inline scm::CommonTrafficSign::Type TranslateSupplementaryTrafficSign(const osiql::SupplementarySign::Type type)
{
  auto it{MAP_SUPPLEMENTARY_SIGN.find(type)};

  if (MAP_SUPPLEMENTARY_SIGN.end() == it)
  {
    return scm::CommonTrafficSign::Undefined;
  }
  return it->second;
}