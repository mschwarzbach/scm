/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Sensor.h"

#include <algorithm>
#include <memory>
#include <stdexcept>

#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
scm::signal::SensorOutput Sensor::Trigger(scm::signal::SensorInput input, const std::vector<std::string>& customCommands)
{
  _state.vehicle = &_state.query.GetHostVehicle();

  SetTriggerNewRouteRequest(input.triggerNewRouteRequest);
  if (input.followPathAction.has_value())
  {
    _state.route = _state.query.GetRoute(input.followPathAction.value());
    if (!_state.vehicle->IsOnRoute(_state.route))
    {
      std::vector<osiql::Vector<2>> points;
      const auto& action{input.followPathAction.value().path_point()};
      points.reserve(static_cast<size_t>(action.size() + 1));
      points.push_back(_state.vehicle->GetXY(_state.vehicle->GetSize().Times(osiql::Anchor::FRONT)));  // clang-format off
      std::transform(action.begin(), action.end(), std::back_inserter(points), [](const auto& point) { return osiql::Vector<2>{point}; });
      _state.route = _state.query.GetRoute(points.begin(), points.end());  // clang-format on
    }
  }
  else if (!_state.vehicle->IsOnRoute(_state.route) || triggerNewRouteRequest)
  {
    _state.route = _state.GetPose().GetRandomRoute([&]() {  // clang-format off
      return _stochastics->GetUniformDistributed(0, 1);
    }, 10000.0);  // clang-format on
  }
  _state.UpdatePose();

  ParseGazeFollowerCommands(customCommands);

  SetPreviewDistance(input.previewDistance);
  SetVehicleParameters(input.vehicleParameters);

  SetTriggerNewRouteRequest(input.triggerNewRouteRequest);

  const auto forcedLaneChangeAction{GetForcedCustomLaneChangeAction(ParseLaneChangeFromCustomCommand(customCommands))};
  ownVehicleInformation = _ownVehicleInfoUpdater->Update(input.vehicleParameters, forcedLaneChangeAction);
  SetGazeFollowerActive(gazeActivityState, gazeFileName);
  trafficRulesInformation = _trafficRuleExtractor->Update(input.previewDistance);
  const auto highwayExitInformation = _routeManager->ExtractHighwayExitInformation(&trafficRulesInformation);
  geometryInformation = _laneGeometryExtractor->Update(input.previewDistance, &ownVehicleInformation, &trafficRulesInformation, highwayExitInformation);
  surroundingObjects = _surroundingTrafficUpdater->Update(input.previewDistance);

  return {
      GetOwnVehicleInformation(),
      GetTrafficRulesInformation(),
      GetGeometryInformation(),
      GetSurroundingObjects(),
      GetOwnVehicleRoutePose()};
}

int Sensor::ParseLaneChangeFromCustomCommand(const std::vector<std::string>& customCommands)
{
  for (const auto& command : customCommands)
  {
    if (command.find("CustomLaneChange") != std::string::npos)
    {
      const auto& tokens = scm::common::TokenizeString(command, ' ');
      return std::stoi(tokens.at(1));
    }
  }
  return 0;
}

void Sensor::ParseGazeFollowerCommands(const std::vector<std::string>& customCommands)
{
  for (const auto& command : customCommands)
  {
    if (command.find("GazeFollower") != std::string::npos)
    {
      const auto& tokens = scm::common::TokenizeString(command, ' ');
      gazeActivityState = tokens.at(1);
      gazeFileName = tokens.at(2);
    }
  }
}

void Sensor::SetPreviewDistance(units::length::meter_t previewDistance)
{
  this->previewDistance = previewDistance;
}

void Sensor::SetVehicleParameters(const scm::common::vehicle::properties::EntityProperties& vehicleParameters)
{
  this->vehicleParameters = vehicleParameters;
}

LaneChangeAction Sensor::GetForcedCustomLaneChangeAction(int laneDelta)
{
  if (laneDelta > 0)
  {
    return LaneChangeAction::ForcePositive;
  }

  if (laneDelta < 0)
  {
    return LaneChangeAction::ForceNegative;
  }

  return LaneChangeAction::None;
}

static std::string Concat(const std::string& path, const std::string& file)
{
  return path + "/" + file;
}

void Sensor::SetGazeFollowerActive(const std::string& gazeActivityState, const std::string& gazeFileName)
{
  gazeFollowerActive = (gazeActivityState == "Active");

  if (gazeFollowerActive && !gazeFollowerActiveLastTick)
  {
    ownVehicleInformation.gazeFollowerInformation.gazeFilePath =
        Concat(_configurationDirectory, gazeFileName);

    ownVehicleInformation.gazeFollowerInformation.gazeFileName = gazeFileName;
  }
  else
  {
    ownVehicleInformation.gazeFollowerInformation.gazeFilePath = "";
    ownVehicleInformation.gazeFollowerInformation.gazeFileName = "";
  }

  gazeFollowerActiveLastTick = gazeFollowerActive;
}

void Sensor::SetTriggerNewRouteRequest(bool triggerNewRouteRequest)
{
  this->triggerNewRouteRequest = triggerNewRouteRequest;
}

OwnVehicleInformationSCM Sensor::GetOwnVehicleInformation() const
{
  return this->ownVehicleInformation;
}

TrafficRuleInformationSCM Sensor::GetTrafficRulesInformation() const
{
  return this->trafficRulesInformation;
}

GeometryInformationSCM Sensor::GetGeometryInformation() const
{
  return this->geometryInformation;
}

SurroundingObjectsSCM Sensor::GetSurroundingObjects() const
{
  return this->surroundingObjects;
}

OwnVehicleRoutePose Sensor::GetOwnVehicleRoutePose() const
{
  return {std::make_shared<osiql::Route>(_state.route), std::make_shared<osiql::Pose<osiql::Point<const osiql::Lane>>>(_state.GetPose())};
}

}  // namespace scm
