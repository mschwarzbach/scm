/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TrafficRuleExtractor.h"

#include <OsiQueryLibrary/osiql.h>
#include <osi3/osi_trafficsign.pb.h>

#include <algorithm>
#include <cmath>
#include <iterator>
#include <map>
#include <ostream>
#include <string>
#include <vector>

#include "OsiEnumConverter.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "src/Logging/Logging.h"

namespace scm
{
const TrafficRuleInformationSCM& TrafficRuleExtractor::Update(units::length::meter_t previewDistance)
{
  _trafficRulesInformation.FarLeft() = GetTrafficRulesLaneInformationOsi(previewDistance, osiql::Side::Left, 2);
  _trafficRulesInformation.Left() = GetTrafficRulesLaneInformationOsi(previewDistance, osiql::Side::Left, 1);
  _trafficRulesInformation.Close() = GetTrafficRulesLaneInformationOsi(previewDistance, osiql::Side::Left, 0);
  _trafficRulesInformation.Right() = GetTrafficRulesLaneInformationOsi(previewDistance, osiql::Side::Right, 1);
  _trafficRulesInformation.FarRight() = GetTrafficRulesLaneInformationOsi(previewDistance, osiql::Side::Right, 2);

  _trafficRulesInformation.Close().laneMarkingsRight = GetLaneMarkings(previewDistance, osiql::Side::Right, 0);
  _trafficRulesInformation.Right().laneMarkingsLeft = _trafficRulesInformation.Close().laneMarkingsRight;
  _trafficRulesInformation.Right().laneMarkingsRight = GetLaneMarkings(previewDistance, osiql::Side::Right, 1);
  _trafficRulesInformation.FarRight().laneMarkingsLeft = _trafficRulesInformation.Right().laneMarkingsRight;
  _trafficRulesInformation.FarRight().laneMarkingsRight = GetLaneMarkings(previewDistance, osiql::Side::Right, 2);

  _trafficRulesInformation.Close().laneMarkingsLeft = GetLaneMarkings(previewDistance, osiql::Side::Left, 0);
  _trafficRulesInformation.Left().laneMarkingsRight = _trafficRulesInformation.Close().laneMarkingsLeft;
  _trafficRulesInformation.Left().laneMarkingsLeft = GetLaneMarkings(previewDistance, osiql::Side::Left, 1);
  _trafficRulesInformation.FarLeft().laneMarkingsRight = _trafficRulesInformation.Left().laneMarkingsLeft;
  _trafficRulesInformation.FarLeft().laneMarkingsLeft = GetLaneMarkings(previewDistance, osiql::Side::Left, 2);

  _spawn = false;
  return _trafficRulesInformation;
}

std::vector<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::GetMaximumSpeedLimitForSpawn(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns,
                                                                                               const std::vector<scm::CommonTrafficSign::Entity>& trafficSignsBack) const
{
  std::vector<scm::CommonTrafficSign::Entity> resultingSigns;
  std::optional<scm::CommonTrafficSign::Entity> nearestSignBack{FindClosestSignBehindEgo(trafficSignsBack, scm::CommonTrafficSign::Type::MaximumSpeedLimit)};

  // Insert found sign and virtually move them 1m ahead of ego
  if (nearestSignBack)
  {
    nearestSignBack->relativeDistance = DISTANCE_TO_VIRTUAL_SIGN;
    resultingSigns.push_back(*nearestSignBack);
  }

  // Add relevant signs in front of ego
  std::copy_if(trafficSigns.begin(), trafficSigns.end(), std::back_inserter(resultingSigns), [](auto& sign)
               { return sign.type == CommonTrafficSign::Type::MaximumSpeedLimit; });

  return resultingSigns;
}

std::vector<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::GetLaneEndingForSpawn(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns,
                                                                                        const std::vector<scm::CommonTrafficSign::Entity>& trafficSignsBack,
                                                                                        scm::CommonTrafficSign::Type laneEndingSign) const
{
  if (laneEndingSign != scm::CommonTrafficSign::AnnounceLeftLaneEnd &&
      laneEndingSign != scm::CommonTrafficSign::AnnounceRightLaneEnd)
  {
    std::string msg = "TrafficRuleExtractor::GetLaneEndingForSpawn called with a sign that is not a lane ending sign.";
    Logging::Error(msg);
    throw std::runtime_error(msg);
  }

  std::vector<scm::CommonTrafficSign::Entity> resultingSigns{};
  std::copy_if(trafficSigns.begin(), trafficSigns.end(), std::back_inserter(resultingSigns), [laneEndingSign](auto& sign)
               { return sign.type == laneEndingSign; });

  // Find the closest relevant signs behind ego
  // Check only if nothing was found in front of ego
  if (resultingSigns.empty())
  {
    std::optional<scm::CommonTrafficSign::Entity> nearestLaneEndsBack{FindClosestSignBehindEgo(trafficSignsBack, laneEndingSign)};
    // Insert found signs and virtually move them 1m ahead of ego
    if (nearestLaneEndsBack)
    {
      const std::optional<scm::CommonTrafficSign::Entity> movedLaneEndsSign = MoveEndOfLaneSignToVirtualPosition(*nearestLaneEndsBack);
      if (movedLaneEndsSign)  // Lane has not ended yet
      {
        resultingSigns.insert(resultingSigns.begin(), *movedLaneEndsSign);
      }
    }
  }
  return resultingSigns;
}

std::optional<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::MoveEndOfLaneSignToVirtualPosition(const scm::CommonTrafficSign::Entity& rearEndOfLaneSign) const
{
  scm::CommonTrafficSign::Entity adjustedEndOfLaneSign = rearEndOfLaneSign;

  const auto movedDistance{DISTANCE_TO_VIRTUAL_SIGN - adjustedEndOfLaneSign.relativeDistance};
  adjustedEndOfLaneSign.relativeDistance = DISTANCE_TO_VIRTUAL_SIGN;

  // Adjust supplementary sign value
  for (auto& supplementarySign : adjustedEndOfLaneSign.supplementarySigns)
  {
    supplementarySign.relativeDistance = DISTANCE_TO_VIRTUAL_SIGN;
    if (supplementarySign.type == scm::CommonTrafficSign::Type::DistanceIndication)
    {
      supplementarySign.value -= movedDistance.value();
      if (supplementarySign.value < 0.)
      {
        return std::nullopt;  // End of lane already started -> End of lane sign irrelevant
      }
    }
  }

  return adjustedEndOfLaneSign;
}

std::vector<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::GetHighwayExitPolesForSpawn(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns,
                                                                                              const std::vector<scm::CommonTrafficSign::Entity>& trafficSignsBack) const
{
  std::vector<scm::CommonTrafficSign::Entity> resultingSigns{};

  std::copy_if(trafficSigns.begin(), trafficSigns.end(), std::back_inserter(resultingSigns), [](auto& sign)
               { return sign.type == scm::CommonTrafficSign::HighwayExitPole; });

  // Find the closest relevant signs behind ego
  // Check only if nothing was found in front of ego
  if (resultingSigns.empty())
  {
    std::optional<scm::CommonTrafficSign::Entity> nearestExitPole{FindClosestSignBehindEgo(trafficSignsBack, scm::CommonTrafficSign::Type::HighwayExitPole)};
    // Insert found sign and virtually move them 1m ahead of ego
    if (nearestExitPole)
    {
      const auto movedDistance{DISTANCE_TO_VIRTUAL_SIGN - nearestExitPole->relativeDistance};
      nearestExitPole->relativeDistance = DISTANCE_TO_VIRTUAL_SIGN;
      nearestExitPole->value -= movedDistance.value();  // Adjust exit pole value after moving it in front of ego
      if (nearestExitPole->value >= 0.)         // Only insert if highway exit has not already started
      {
        resultingSigns.insert(resultingSigns.begin(), *nearestExitPole);
      }
    }
  }
  return resultingSigns;
}

std::optional<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::FindClosestSignBehindEgo(const std::vector<scm::CommonTrafficSign::Entity>& trafficSignsBehindEgo, scm::CommonTrafficSign::Type relevantType) const
{
  scm::CommonTrafficSign::Entity nearestSign;
  nearestSign.relativeDistance = -ScmDefinitions::INF_DISTANCE;

  for (scm::CommonTrafficSign::Entity trafficSignBack : trafficSignsBehindEgo)
  {
    if (trafficSignBack.type == relevantType &&
        trafficSignBack.relativeDistance >= nearestSign.relativeDistance &&
        trafficSignBack.relativeDistance <= 0.0_m)
    {
      nearestSign = trafficSignBack;
    }
  }

  if (nearestSign.type != scm::CommonTrafficSign::Type::Undefined)
  {
    return nearestSign;
  }

  return std::nullopt;
}

LaneInformationTrafficRulesSCM TrafficRuleExtractor::GetTrafficRulesLaneInformationOsi(units::length::meter_t previewDistance, osiql::Side side, size_t laneOffset) const
{
  LaneInformationTrafficRulesSCM laneInformation{};
  const auto laneOfInterest{_state.GetPose().GetLane().GetAdjacentLane(side, laneOffset)};
  if (laneOfInterest != nullptr)
  {
    osiql::RoadPoint location{*laneOfInterest, _state.GetPose()};
    //   Perceive Objects in range +/- min(visibilityDistance, previewDistance)
    const auto visibilityDistance = units::math::min(previewDistance, _visibilityDistance);

    if (_spawn)
    {
      osiql::Stream behind(location, -visibilityDistance.value());
      auto passedTrafficSigns{behind.Find<osiql::TrafficSign>(osiql::LaneChangeBehavior::WithoutLaneChanges)};
      for (auto& sign : passedTrafficSigns)
      {
        sign.second.value = -sign.second.value;
      }
      // TODO: ExtractInformation should take iterators instead of a range to avoid having to reverse this range:
      std::reverse(passedTrafficSigns.begin(), passedTrafficSigns.end());

      std::vector<CommonTrafficSign::Entity> rearInformation{ExtractInformation(passedTrafficSigns)};
      std::vector<CommonTrafficSign::Entity> speedLimitSigns{GetMaximumSpeedLimitForSpawn({}, rearInformation)};
      std::vector<CommonTrafficSign::Entity> laneEndingSignsLeft{GetLaneEndingForSpawn({}, rearInformation, CommonTrafficSign::Type::AnnounceLeftLaneEnd)};
      std::vector<CommonTrafficSign::Entity> laneEndingSignsRight{GetLaneEndingForSpawn({}, rearInformation, CommonTrafficSign::Type::AnnounceRightLaneEnd)};
      std::vector<CommonTrafficSign::Entity> highwayExitPoles{GetHighwayExitPolesForSpawn({}, rearInformation)};

      // TODO: Wouldn't it be better if these were sorted by distance instead of type?
      std::vector<CommonTrafficSign::Entity>& result{laneInformation.trafficSigns};
      result.insert(result.end(), std::make_move_iterator(speedLimitSigns.begin()), std::make_move_iterator(speedLimitSigns.end()));
      result.insert(result.end(), std::make_move_iterator(laneEndingSignsLeft.begin()), std::make_move_iterator(laneEndingSignsLeft.end()));
      result.insert(result.end(), std::make_move_iterator(laneEndingSignsRight.begin()), std::make_move_iterator(laneEndingSignsRight.end()));
      result.insert(result.end(), std::make_move_iterator(highwayExitPoles.begin()), std::make_move_iterator(highwayExitPoles.end()));
    }
    std::set<osiql::RoadId> visitedRoads;
    auto distanceRouteToFrontBumper = _state.route.GetDistance(_state.GetPose());

    const std::vector<const Lane*> lanes{_state.route.GetLaneChain(*laneOfInterest)};  // clang-format off
    const units::length::meter_t minDistance{units::make_unit<units::length::meter_t>(_state.route.GetDistance(_state.GetPose()))};
    const auto maxDistance{minDistance + visibilityDistance};
    const auto signs{_state.route.FindAll<osiql::TrafficSign>([&lanes, &minDistance, &maxDistance](const osiql::TrafficSign& sign, double distance) {
      const auto& assignments{sign.GetLaneAssignments()};
      return (minDistance.value() <= distance) && (distance < maxDistance.value()) && (std::find_if(assignments.begin(), assignments.end(), [&lanes](const osi3::LogicalLaneAssignment& assignment) {
        return std::find_if(lanes.begin(), lanes.end(), [&assignment](const Lane* lane) {
          return lane->GetId() == assignment.assigned_lane_id().value();
        }) != lanes.end();
      }) != assignments.end());
    })};  // clang-format on
    if (laneInformation.trafficSigns.empty()) [[likely]]
    {
      laneInformation.trafficSigns = ExtractInformation(signs);
    }
    else
    {
      auto signInformation{ExtractInformation(signs)};
      laneInformation.trafficSigns.insert(laneInformation.trafficSigns.end(), std::make_move_iterator(signInformation.begin()), std::make_move_iterator(signInformation.end()));
    }
  }
  // Sort them by distance
  std::sort(laneInformation.trafficSigns.begin(), laneInformation.trafficSigns.end(), [](const auto& lhs, const auto& rhs)
            { return lhs.relativeDistance < rhs.relativeDistance; });
  return laneInformation;
}

std::vector<scm::CommonTrafficSign::Entity> TrafficRuleExtractor::GetTrafficSignsOsi(const std::vector<std::pair<const osiql::TrafficSign*, osiql::Stream::Distance>>& trafficSigns) const
{
  std::vector<scm::CommonTrafficSign::Entity> signs{};
  if (!trafficSigns.empty())
  {
    for (const auto& [trafficSign, distance] : trafficSigns)
    {
      osiql::Value signValue(trafficSign->GetValue());
      scm::CommonTrafficSign::Entity sign{};
      sign.relativeDistance = units::make_unit<units::length::meter_t>(distance.value);
      sign.value = trafficSign->GetValue();
      sign.unit = static_cast<scm::CommonTrafficSign::Unit>(signValue.unit);
      sign.distanceToStartOfRoad = units::make_unit<units::length::meter_t>(distance.value - distance.node->distance);
      sign.type = TranslateTrafficSign(trafficSign->GetType());

      signs.push_back(sign);
    }
  }

  return signs;
}

std::vector<CommonTrafficSign::Entity> TrafficRuleExtractor::ExtractInformation(const std::vector<std::pair<const osiql::TrafficSign*, double>>& trafficSigns) const
{
  std::vector<CommonTrafficSign::Entity> signs{};
  const osiql::Orientation o{_state.route.orientation};
  if (!trafficSigns.empty())
  {
    auto it{_state.route.begin()};
    const double ownDistance{_state.route.GetDistance(_state.GetPose())};
    for (const auto& [trafficSign, distance] : trafficSigns)
    {  // clang-format off
        while (std::find_if(trafficSign->positions.begin(), trafficSign->positions.end(), [&road = it->GetRoad()](const auto& position) {
          return osiql::get<osiql::Road>(position) == road;
        }) == trafficSign->positions.end()) {  // clang-format on
        ++it;
      }
      const osiql::Direction direction{it->GetLane().GetDirection(o)};
      osiql::Value signValue(trafficSign->GetValue());
      CommonTrafficSign::Entity sign{};
      sign.relativeDistance = units::make_unit<units::length::meter_t>(distance - ownDistance);
      sign.value = signValue;
      sign.unit = static_cast<CommonTrafficSign::Unit>(signValue.unit);
      if (direction == osiql::Direction::Upstream)
      {
        sign.distanceToStartOfRoad = units::make_unit<units::length::meter_t>(it->GetLane().GetDistanceFromStart(it->latitude, o) + distance - it->distance);
      }
      else
      {
        sign.distanceToStartOfRoad = units::make_unit<units::length::meter_t>(it->latitude + distance - it->distance);
      }
      sign.type = TranslateTrafficSign(trafficSign->GetType());
      // TODO: Fill information fo supplementary signs
      sign.supplementarySigns.reserve(trafficSign->GetHandle().supplementary_sign_size());
      const auto& supplementarySigns{trafficSign->GetHandle().supplementary_sign()};  // clang-format off
      std::transform(supplementarySigns.begin(), supplementarySigns.end(), std::back_inserter(sign.supplementarySigns), [&mainSign = sign](const osi3::TrafficSign_SupplementarySign& sign) {
        CommonTrafficSign::Entity result{};
        result.relativeDistance = mainSign.relativeDistance;
        result.distanceToStartOfRoad = mainSign.distanceToStartOfRoad;
        result.type = TranslateSupplementaryTrafficSign(osiql::SupplementarySign::Type{sign.classification().type()});
        if(result.type == CommonTrafficSign::Type::DistanceIndication) {
          result.unit = CommonTrafficSign::Unit::Meter;
          if (!sign.classification().value().empty())
          {
            result.value = sign.classification().value(0).value();
            if(sign.classification().value(0).has_text())
            {
              result.text = sign.classification().value(0).text();
            }
          }
        } else if (!sign.classification().value().empty())
        {
          result.value = sign.classification().value(0).value();
          result.unit = static_cast<CommonTrafficSign::Unit>(osiql::Value{sign.classification().value(0)}.unit);
        }
        else if(mainSign.unit != CommonTrafficSign::Unit::None) {
          result.unit = mainSign.unit;
        }
        return result;  //
      });  // clang-format on
      signs.push_back(sign);
    }
  }
  return signs;
}

std::vector<CommonTrafficSign::Entity> TrafficRuleExtractor::ExtractInformation(const std::vector<std::pair<const osiql::TrafficSign*, osiql::Stream::Distance>>& trafficSigns) const
{
  std::vector<CommonTrafficSign::Entity> signs{};
  if (!trafficSigns.empty())
  {
    for (const auto& [trafficSign, distance] : trafficSigns)
    {
      osiql::Value signValue(trafficSign->GetValue());
      CommonTrafficSign::Entity sign{};
      sign.relativeDistance = units::make_unit<units::length::meter_t>(distance.value);
      sign.value = trafficSign->GetValue();
      sign.unit = static_cast<CommonTrafficSign::Unit>(signValue.unit);
      sign.distanceToStartOfRoad = units::make_unit<units::length::meter_t>(distance.value + distance.node->distance + distance.node->lane.GetRoad().GetLength());
      sign.type = TranslateTrafficSign(trafficSign->GetType());

      signs.push_back(sign);
    }
  }
  return signs;
}

LaneMarking::Color GetColor(osiql::LaneMarking::Color color)
{
  const std::map<osiql::LaneMarking::Color, LaneMarking::Color> map{
      {osiql::LaneMarking::Color::White, LaneMarking::Color::White},
      {osiql::LaneMarking::Color::Yellow, LaneMarking::Color::Yellow},
      {osiql::LaneMarking::Color::Red, LaneMarking::Color::Red},
      {osiql::LaneMarking::Color::Blue, LaneMarking::Color::Blue},
      {osiql::LaneMarking::Color::Green, LaneMarking::Color::Green}  //
  };
  const auto it{map.find(color)};
  return it != map.end() ? it->second : LaneMarking::Color::Other;
};

LaneMarking::Type GetType(const std::vector<const osiql::LaneMarking*>& markings)
{
  if (markings.size() == 2)
  {
    if (markings.front()->GetType() == osiql::LaneMarking::Type::SolidLine)
    {
      if (markings.back()->GetType() == osiql::LaneMarking::Type::SolidLine)
      {
        return LaneMarking::Type::Solid_Solid;
      }
      else if (markings.back()->GetType() == osiql::LaneMarking::Type::DashedLine)
      {
        return LaneMarking::Type::Solid_Broken;
      }
    }
    else if (markings.front()->GetType() == osiql::LaneMarking::Type::DashedLine)
    {
      if (markings.back()->GetType() == osiql::LaneMarking::Type::SolidLine)
      {
        return LaneMarking::Type::Broken_Solid;
      }
      else if (markings.back()->GetType() == osiql::LaneMarking::Type::DashedLine)
      {
        return LaneMarking::Type::Broken_Broken;
      }
    }
  }
  const std::map<osiql::LaneMarking::Type, LaneMarking::Type> map{
      {osiql::LaneMarking::Type::SolidLine, LaneMarking::Type::Solid},
      {osiql::LaneMarking::Type::DashedLine, LaneMarking::Type::Broken},
      {osiql::LaneMarking::Type::GrassEdge, LaneMarking::Type::Grass},
      {osiql::LaneMarking::Type::BottsDots, LaneMarking::Type::Botts_Dots},
      {osiql::LaneMarking::Type::Curb, LaneMarking::Type::Curb}  //
  };
  const auto it{map.find(markings.front()->GetType())};
  return it != map.end() ? it->second : LaneMarking::Type::None;
}

std::vector<LaneMarking::Entity> TrafficRuleExtractor::GetLaneMarkings(units::length::meter_t range, osiql::Side side, size_t boundaryOffset) const
{
  std::vector<LaneMarking::Entity> result;
  result.reserve(_state.route.size());

  const auto laneOfInterest{_state.GetPose().GetLane().GetAdjacentLane(side, boundaryOffset)};
  if (!laneOfInterest)
  {
    return result;
  }
  const std::vector<const osiql::Lane*> lanes{_state.route.GetLaneChain(*laneOfInterest)};

  const auto minDistance{units::make_unit<units::length::meter_t>(_state.route.GetDistance(_state.GetPose()))};
  const auto maxDistance{minDistance + range};
  for (const osiql::Lane* lane : lanes)
  {
    const auto& boundary{lane->GetBoundary(side, _state.GetPose().latitude)};
    for (const auto* marking : boundary.markings)
    {
      const osiql::Point<const osiql::Lane> boundaryStart{*lane, boundary.Localize(marking->front())};
      const auto distanceA{units::make_unit<units::length::meter_t>(_state.route.GetDistance(boundaryStart))};
      const osiql::Point<const osiql::Lane> boundaryEnd{*lane, boundary.Localize(marking->back())};
      const auto distanceB{units::make_unit<units::length::meter_t>(_state.route.GetDistance(boundaryEnd))};
      const auto [startDistance, endDistance]{std::minmax(distanceA, distanceB)};
      if (startDistance < maxDistance && minDistance <= endDistance)
      {
        LaneMarking::Entity entity;
        entity.relativeStartDistance = startDistance - minDistance;
        entity.color = GetColor(boundary.markings.front()->GetColor());
        entity.type = GetType(boundary.markings);
        entity.width = units::make_unit<units::length::meter_t>(boundary.markings.front()->front().width());
        result.push_back(std::move(entity));
      }
    }
  }
  return result;
}

std::ostream& operator<<(std::ostream& os, LaneMarking::Color c)
{
  static const std::map<LaneMarking::Color, const std::string> map{
      {LaneMarking::Color::White, "White"},
      {LaneMarking::Color::Yellow, "Yellow"},
      {LaneMarking::Color::Red, "Red"},
      {LaneMarking::Color::Blue, "Blue"},
      {LaneMarking::Color::Green, "Green"},
      {LaneMarking::Color::Other, "Other"}  //
  };
  const auto it{map.find(c)};
  return os << (it == map.end() ? "Other" : it->second);
}

std::ostream& operator<<(std::ostream& os, LaneMarking::Type t)
{
  static const std::map<LaneMarking::Type, const std::string> map{
      {LaneMarking::Type::None, "None"},
      {LaneMarking::Type::Solid, "Solid"},
      {LaneMarking::Type::Broken, "Broken"},
      {LaneMarking::Type::Solid_Solid, "Solid+Solid"},
      {LaneMarking::Type::Solid_Broken, "Solid+Broken"},
      {LaneMarking::Type::Broken_Solid, "Broken+Solid"},
      {LaneMarking::Type::Broken_Broken, "Broken+Broken"},
      {LaneMarking::Type::Grass, "Grass"},
      {LaneMarking::Type::Botts_Dots, "Botts Dots"},
      {LaneMarking::Type::Curb, "Curb"}  //
  };
  const auto it{map.find(t)};
  return os << (it == map.end() ? "Other" : it->second);
}

std::ostream& operator<<(std::ostream& os, const LaneMarking::Entity& e)
{
  return os << "[Color: " << e.color << ", Type: " << e.type << ", Start Distance: " << e.relativeStartDistance
            << ", Width: " << e.width << ']';
}
}  // namespace scm
