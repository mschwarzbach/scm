/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "SensorTypes.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "include/common/VehicleProperties.h"

namespace scm
{
class OwnVehicleInformationUpdaterInterface
{
public:
  virtual ~OwnVehicleInformationUpdaterInterface() = default;
  virtual const OwnVehicleInformationSCM& Update(const common::vehicle::properties::EntityProperties& vehicleParameters, LaneChangeAction forcedLaneChangeAction) = 0;
};
}  // namespace scm
