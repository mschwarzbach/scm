/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "RouteManager.h"

#include <algorithm>

namespace scm
{

scm::HighwayExitInformation RouteManager::ExtractHighwayExitInformation(TrafficRuleInformationSCM* trafficRulesInformation)
{
  if (!_highwayExitSignDedected)
  {
    _highwayExitSignDedected = DetectHighwayExitSign(trafficRulesInformation);
  }

  const bool takeNextExit{std::any_of(_state.route.begin(), _state.route.end(), [](const osiql::Waypoint& point)
                                      { return point.GetLane().GetType() == osiql::Lane::Type::Exit || point.GetLane().GetType() == osiql::Lane::Type::OffRamp; })};
  if (!takeNextExit || !_highwayExitSignDedected)
  {
    _highwayExitSignDedected = false;
    return {units::length::meter_t(ScmDefinitions::DEFAULT_VALUE), units::length::meter_t(ScmDefinitions::DEFAULT_VALUE), -1};
  }

  return DetermineHighwayExitInformation();
}

bool RouteManager::DetectHighwayExitSign(TrafficRuleInformationSCM* trafficRulesInformation)
{
  bool sign = false;
  for (const auto& lane : *trafficRulesInformation)
  {
    if (lane.trafficSigns.size() != 0)
    {
      sign = {std::any_of(lane.trafficSigns.begin(), lane.trafficSigns.end(), [](const CommonTrafficSign::Entity& sign)
                          { return sign.type == CommonTrafficSign::HighwayExitPole || sign.type == CommonTrafficSign::AnnounceHighwayExit; })};

      if (sign) return true;
    }
  }
  return false;
}

HighwayExitInformation RouteManager::DetermineHighwayExitInformation()
{
  HighwayExitInformation exitInformations{};
  auto roads{_state.route.FindAll<osiql::Road>()};

  roads.erase(roads.begin(), std::unique(roads.rbegin(), roads.rend(), [](const std::pair<const osiql::Road*, double>& a, const std::pair<const osiql::Road*, double>& b)
                                         { return *a.first == *b.first; })
                                 .base());

  for (auto& road : roads)
  {
    const auto* laneOfInterest(road.first->lanes.front());
    for (auto* lane : road.first->lanes)
    {
      if (lane->GetType() == osiql::Lane::Type::Exit)
      {
        auto startDistance = units::make_unit<units::length::meter_t>(road.second - _state.route.GetDistance(_state.GetPose()));
        auto endDistance = startDistance + units::make_unit<units::length::meter_t>(osiql::Length(*lane));
        auto lanesToCross = _state.GetPose().GetLane().GetLaneChangesTo(*lane);

        int laneId = 0;
        laneId = lanesToCross.first == osiql::Side::Right ? -1 * lanesToCross.second : lanesToCross.second;

        exitInformations.distanceToEndOfExit = endDistance;
        exitInformations.distanceToStartOfExit = startDistance;
        exitInformations.relativeLaneIdForJunctionIngoing = laneId;

        break;
      }
    }
  }
  return exitInformations;
}
}  // namespace scm
