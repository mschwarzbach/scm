/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//
// #pragma once
//
// #include <memory>
//
// #include "SensorDriverScmCalculationsInterface.h"
// #include "SensorDriverScmDefinitions.h"
// #include "SensorTypes.h"
// #include "SurroundingTrafficUpdaterInterface.h"
// #include "module/sensor/src/VirtualTrafficUpdaterInterface.h"
// #include <OsiQueryLibrary/osiql.h>
//
// namespace scm
//{
// class VirtualTrafficUpdater : public VirtualTrafficUpdaterInterface
//{
// public:
//  VirtualTrafficUpdater(const scm::VehicleState& state)
//      : _state{state}
//  {
//  }
//  VirtualAgentsSCM GetVirtualAgentInformation(double previewDistance) override;
//
// private:
//  const scm::VehicleState& _state;
//  RouteElement _routeElementForGraph;
//  std::string _sharedRoadId = "";
//
//  std::vector<const WorldObjectInterface*> GetVirtualAgents(int targetLaneId, RouteElement road, std::unique_ptr<RoadStreamInterface>& roadStream, double startPoint, double endPoint);
//  RouteElement FindRouteElementForRoadGraph(double previewDistance);
//  bool isEgoOnLeftLane(std::string egoRoadId);
//  std::vector<std::vector<RouteElement>> CreateBackwardRoadGraph(RouteElement routeElementForGraph);
//  bool IsEgoOnSharedRoadPositionOverVisibilityDistance(double visibilityDistance, std::string egoRouteId, std::string sharedRoadId);
//  int GetTargetLaneId(std::unique_ptr<RoadStreamInterface>& roadStream);
//  ObjectInformationSCM DetermineVirtualAgentParameters(const AgentInterface* agent) const;
//  ObjectInformationSCM DetermineVirtualAgentParametersOsi(const osiql::MovingObject* movingObject, double distance) const;
//
//  void isVirtualAgentsOsiAndOldEqual(ObjectInformationSCM objold, ObjectInformationSCM objOsi);
//
//  LaneType GetLaneType(const WorldObjectInterface* object) const;
//  double GetDistanceBetweenVirtualAgentAndEgo(const WorldObjectInterface* object) const;
//  double GetDistanceToEndOfLaneEgo() const;
//  double GetDistanceToEndOfLaneVirtualAgent(const WorldObjectInterface* object) const;
//};
//}  // namespace scm
