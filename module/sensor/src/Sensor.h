/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>
#include <Stochastics/StochasticsInterface.h>

#include <optional>
#include <string>

#include "LaneGeometryExtractor.h"
#include "OwnVehicleInformationUpdater.h"
#include "RouteManager.h"
#include "SensorTypes.h"
#include "SurroundingTrafficUpdater.h"
#include "TrafficRuleExtractor.h"
#include "include/common/OsiQlData.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "include/common/VehicleProperties.h"
#include "include/module/sensorInterface.h"

namespace scm
{

//! @brief Main class of SCM driver to extract simulation data of the own vehicle, surrounding traffic, lane geometry, traffic rules and route.
class Sensor : public scm::sensor::Interface
{
public:
  //! @brief Constructor of Sensor
  //! @param configurationDirectory
  //! @param osiQl
  //! @param stochastics
  //! @param visibilityDistance
  //! @param agentId
  Sensor(const std::string& configurationDirectory, osiql::Query* osiQl, StochasticsInterface* stochastics, units::length::meter_t visibilityDistance, int agentId)
      : _configurationDirectory{configurationDirectory},
        _state(*osiQl, agentId, visibilityDistance),
        _osiQl(osiQl),
        _stochastics(stochastics),
        _routeManager(std::make_shared<RouteManager>(_state)),
        _ownVehicleInfoUpdater(std::make_shared<OwnVehicleInformationUpdater>(_state)),
        _surroundingTrafficUpdater(std::make_shared<SurroundingTrafficUpdater>(_state)),
        _trafficRuleExtractor(std::make_shared<TrafficRuleExtractor>(_state, visibilityDistance)),
        _laneGeometryExtractor(std::make_shared<LaneGeometryExtractor>(_state, visibilityDistance))
  {
  }

  //! @brief this method simply relays the Trigger method of openPASS
  //! @param input
  //! @param time
  //! @param customCommands
  //! @return Specified sensor output with all information necessary for the driver
  scm::signal::SensorOutput Trigger(scm::signal::SensorInput input, const std::vector<std::string>& customCommands) override;

  friend class TestSensorDriverScmImplementation;

private:
  scm::VehicleState _state;

  // Why shared pointers?
  std::string _configurationDirectory{};
  //! @brief OSIQL
  osiql::Query* _osiQl;
  //! @brief Interface to the stochastics
  StochasticsInterface* _stochastics;
  //! @brief Interface to the route manager
  std::shared_ptr<RouteManagerInterface> _routeManager;
  //! @brief Interface to the own vehicle updater
  std::shared_ptr<OwnVehicleInformationUpdaterInterface> _ownVehicleInfoUpdater;
  //! @brief Interface to the surrounding traffic updater
  std::shared_ptr<SurroundingTrafficUpdaterInterface> _surroundingTrafficUpdater;
  //! @brief Interface to the traffic rule extractor
  std::shared_ptr<TrafficRuleExtractorInterface> _trafficRuleExtractor;
  //! @brief Interface to the lane geometry extractor
  std::shared_ptr<LaneGeometryExtractor> _laneGeometryExtractor;

  static LaneChangeAction GetForcedCustomLaneChangeAction(int laneDelta);

  //! @brief The distance around the driver's vehicle, which the driver considers relevant [m].
  units::length::meter_t previewDistance{250._m};

  //! @brief Data struct for driver parameters
  scm::common::vehicle::properties::EntityProperties vehicleParameters;

  //! @brief Struct for all sensor data concerning the own vehicle
  OwnVehicleInformationSCM ownVehicleInformation;

  //! @brief Struct for all sensor data concerning traffic rules (eg. traffic signs, lane markings, ...)
  TrafficRuleInformationSCM trafficRulesInformation;

  //! @brief Struct for all sensor data concerning the geometry of the street.
  GeometryInformationSCM geometryInformation;

  //! @brief Struct for all sensor data concerning surrounding objects
  SurroundingObjectsSCM surroundingObjects;

  //! Gaze follower was triggered this time step.
  std::pair<std::string, std::string> gazeFollowerTriggered{"", ""};

  //! GazeFollower activation is triggered in current time step.
  bool gazeFollowerActive{false};

  //! GazeFollower activation was triggered in last time step.
  bool gazeFollowerActiveLastTick{false};

  //! @brief Get the own vehicle information
  //! @return
  OwnVehicleInformationSCM GetOwnVehicleInformation() const;

  //! @brief
  //! @return
  TrafficRuleInformationSCM GetTrafficRulesInformation() const;

  GeometryInformationSCM GetGeometryInformation() const;

  SurroundingObjectsSCM GetSurroundingObjects() const;

  OwnVehicleRoutePose GetOwnVehicleRoutePose() const;

  bool triggerNewRouteRequest{false};
  void SetPreviewDistance(units::length::meter_t previewDistance);
  void SetVehicleParameters(const scm::common::vehicle::properties::EntityProperties& vehicleParameters);
  void SetGazeFollowerActive(const std::string& gazeActivityState, const std::string& gazeFileName);
  void SetTriggerNewRouteRequest(bool triggerNewRouteRequest);

  int deltaLaneId{0};
  std::string gazeActivityState;
  std::string gazeFileName;
  void ReadCustomCommands(const std::vector<std::string>& customCommands);
  void SetForcedCustomLaneChangeTriggered(int deltaLaneId);
  static int ParseLaneChangeFromCustomCommand(const std::vector<std::string>& customCommands);
  void ParseGazeFollowerCommands(const std::vector<std::string>& customCommands);
};

}  // namespace scm