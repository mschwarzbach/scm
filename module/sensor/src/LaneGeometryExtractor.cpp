/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneGeometryExtractor.h"

#include <OsiQueryLibrary/osiql.h>

#include "SensorTypes.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "src/Logging/Logging.h"

namespace scm
{
const GeometryInformationSCM& LaneGeometryExtractor::Update(units::length::meter_t previewDistance, OwnVehicleInformationSCM* ownVehicleInformation, TrafficRuleInformationSCM* trafficRulesInformation, const scm::HighwayExitInformation& highwayExitInformation)
{
  _laneInformation.visibilityDistance = _visibilityDistance;
  _laneInformation.distanceToEndOfNextExit = highwayExitInformation.distanceToEndOfExit;
  _laneInformation.distanceToStartOfNextExit = highwayExitInformation.distanceToStartOfExit;
  _laneInformation.relativeLaneIdForJunctionIngoing = {highwayExitInformation.relativeLaneIdForJunctionIngoing};

  _laneInformation.Close() = GetGeometryLaneInformation(0, previewDistance, ownVehicleInformation);
  _laneInformation.Left() = GetGeometryLaneInformation(1, previewDistance, ownVehicleInformation);
  _laneInformation.Right() = GetGeometryLaneInformation(-1, previewDistance, ownVehicleInformation);
  _laneInformation.FarLeft() = GetGeometryLaneInformation(2, previewDistance, ownVehicleInformation);
  _laneInformation.FarRight() = GetGeometryLaneInformation(-2, previewDistance, ownVehicleInformation);
  _laneInformation.numberOfLanes = _state.GetPose().GetRoad().lanes.size();
  _laneInformation.currentRoadId = _state.GetPose().GetRoad().GetId();
  return _laneInformation;
}

LaneType TranslateOsiLaneType(osiql::Lane::Type osiLaneType)
{
  static const std::map<osiql::Lane::Type, LaneType> translationMap{
      {osiql::Lane::Type::Normal, LaneType::Driving},
      {osiql::Lane::Type::Biking, LaneType::Biking},
      {osiql::Lane::Type::Border, LaneType::Border},
      {osiql::Lane::Type::ConnectingRamp, LaneType::ConnectingRamp},
      {osiql::Lane::Type::Curb, LaneType::Curb},
      {osiql::Lane::Type::Entry, LaneType::Entry},
      {osiql::Lane::Type::Exit, LaneType::Exit},
      {osiql::Lane::Type::Median, LaneType::Median},
      {osiql::Lane::Type::OffRamp, LaneType::OffRamp},
      {osiql::Lane::Type::OnRamp, LaneType::OnRamp},
      {osiql::Lane::Type::Other, LaneType::Undefined},
      {osiql::Lane::Type::Undefined, LaneType::Undefined},
      {osiql::Lane::Type::Rail, LaneType::Undefined},
      {osiql::Lane::Type::Parking, LaneType::Parking},
      {osiql::Lane::Type::Restricted, LaneType::Restricted},
      {osiql::Lane::Type::Shoulder, LaneType::Shoulder},
      {osiql::Lane::Type::Sidewalk, LaneType::Sidewalk},
      {osiql::Lane::Type::Stop, LaneType::Stop},
      {osiql::Lane::Type::Tram, LaneType::Tram}};

  return translationMap.at(osiLaneType);
}

LaneInformationGeometrySCM LaneGeometryExtractor::GetGeometryLaneInformation(int relativeLaneId, units::length::meter_t previewDistance, OwnVehicleInformationSCM* ownVehicleInformation)
{
  // Lane existence
  const osiql::Side side{relativeLaneId < 0 ? osiql::Side::Right : osiql::Side::Left};
  const auto* laneOfInterest{_state.GetPose().GetLane().GetAdjacentLane(side, std::abs(relativeLaneId))};
  if (!laneOfInterest)
  {
    return {};
  }
  LaneInformationGeometrySCM laneInformation;
  const auto visibilityDistance = units::math::min(previewDistance, _visibilityDistance);

  laneInformation.exists = true;

  // Lane Width
  laneInformation.width = static_cast<units::length::meter_t>(laneOfInterest->GetWidth(_state.GetPose().latitude));

  // Lane Curvature
  laneInformation.curvature = static_cast<units::curvature::inverse_meter_t>(laneOfInterest->GetRoad().GetReferenceLine().GetCurvature(_state.GetPose().latitude));
  if (std::isnan(laneInformation.curvature.value()))
  {
    laneInformation.curvature = 0.0_i_m;
  }

  laneInformation.laneType = TranslateOsiLaneType(laneOfInterest->GetType());

  // Distance of closest slip road
  scm::DistancesToEndOfLane distances{GetPathEndDistances(osiql::RoadPoint{*laneOfInterest, _state.GetPose()})};
  laneInformation.distanceToEndOfLane = distances.normal;
  laneInformation.distanceToEndOfLaneDuringEmergency = distances.emergency;

  // Lane curvature ahead
  const double targetLatitude{_state.GetPose().latitude + previewDistance.value()};
  const auto& referenceLine{laneOfInterest->GetRoad().GetReferenceLine()};
  double curvatureAhead{referenceLine.GetCurvature(targetLatitude)};
  if (std::isnan(curvatureAhead))
  {
    curvatureAhead = 0.0;
  }
  laneInformation.curvatureInPreviewDistance = static_cast<units::curvature::inverse_meter_t>(curvatureAhead);

  return laneInformation;
}

scm::DistancesToEndOfLane LaneGeometryExtractor::GetPathEndDistances(const osiql::RoadPoint& start) const
{
  static const std::vector<osiql::Lane::Type> allowedTypes{
      osiql::Lane::Type::Normal,
      osiql::Lane::Type::Entry,
      osiql::Lane::Type::Exit,
      osiql::Lane::Type::OffRamp,
      osiql::Lane::Type::OnRamp  //
  };
  static const std::vector<osiql::Lane::Type> allowedEmergencyTypes{
      osiql::Lane::Type::Normal,
      osiql::Lane::Type::Entry,
      osiql::Lane::Type::Exit,
      osiql::Lane::Type::OffRamp,
      osiql::Lane::Type::OnRamp,
      osiql::Lane::Type::ConnectingRamp,
      osiql::Lane::Type::Biking,
      osiql::Lane::Type::Border,
      osiql::Lane::Type::Median,
      osiql::Lane::Type::Parking,
      osiql::Lane::Type::Curb,
      osiql::Lane::Type::Restricted,
      osiql::Lane::Type::Shoulder,
      osiql::Lane::Type::Sidewalk,
      osiql::Lane::Type::Stop  //
  };

  osiql::Stream stream{start, _laneInformation.visibilityDistance.value()};  // clang-format off
  const std::vector<const osiql::Node *> endNodes{stream.GetLastMatches([&](const osiql::Node& node) {
    return std::any_of(allowedTypes.begin(), allowedTypes.end(), [&](osiql::Lane::Type type) { return type == node.lane.GetType(); });
  })};
  const std::vector<const osiql::Node *> emergencyEndNodes{stream.GetLastMatches([&](const osiql::Node& node) {
    return std::any_of(allowedEmergencyTypes.begin(), allowedEmergencyTypes.end(), [&](osiql::Lane::Type type) { return type == node.lane.GetType(); });
  })};  // clang-format on
  auto distanceNormal = endNodes.empty() ? 0.0 : std::max(0.0, endNodes.front()->distance + osiql::Length(endNodes.front()->lane));
  auto distanceEmergency = emergencyEndNodes.empty() ? 0.0 : std::max(0.0, emergencyEndNodes.front()->distance + osiql::Length(emergencyEndNodes.front()->lane));
  return {distanceNormal > _laneInformation.visibilityDistance.value() ? ScmDefinitions::INF_DISTANCE : units::make_unit<units::length::meter_t>(distanceNormal),
          distanceEmergency > _laneInformation.visibilityDistance.value() ? ScmDefinitions::INF_DISTANCE : units::make_unit<units::length::meter_t>(distanceEmergency)};
}

}  // namespace scm
