/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <OsiQueryLibrary/osiql.h>

#include <memory>
#include <string>

#include "../../../include/module/sensorInterface.h"
#include "ScmSensor.h"

namespace scm::sensor::factory
{
inline std::unique_ptr<scm::sensor::Interface> Create(int cycleTime, std::string configurationDirectory, StochasticsInterface* staticStochastics, osiql::Query* osiQl, units::length::meter_t visibilityDistance, int agentId)
{
  return std::unique_ptr<scm::sensor::Interface>(scm::sensor::Create(
      cycleTime,
      configurationDirectory,
      staticStochastics,
      osiQl,
      visibilityDistance,
      agentId));
}

}  // namespace scm::sensor::factory
