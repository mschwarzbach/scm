/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SteeringController.h
#pragma once

#include "../include/common/ScmDefinitions.h"
#include "../include/signal/lateralControllerInput.h"
using namespace units::literals;
namespace scm
{
//! @brief models the steering controller of the driver
//! @details It calculates a steering wheel angle of the driver.
class SteeringController
{
public:
  //! @brief Calculates the steering angle for a given timestep.
  //! @param    time           Current scheduling time
  //! @return   steering angle
  units::angle::radian_t CalculateSteeringAngle(units::time::millisecond_t time);

  //! @brief Sets the lateral input, which contains the desired lateral position.
  //! @param[in]     lateralSignal           LateralSignal
  void SetLateralInput(const signal::LateralControllerInput& lateralSignal)
  {
    SetVehicleParameter(lateralSignal.steeringRatio, units::make_unit<units::angle::radian_t>(lateralSignal.frontAxleMaxSteering), lateralSignal.wheelBase);
    SetVelocityAndSteeringWheelAngle(lateralSignal.velocity, lateralSignal.steeringWheelAngle);
    lateralDeviation = lateralSignal.lateralDeviation;
    gainLateralDeviation = lateralSignal.gainLateralDeviation;
    headingError = lateralSignal.headingError;
    gainHeadingError = lateralSignal.gainHeadingError;
    kappaManoeuvre = lateralSignal.kappaManoeuvre;
    kappaRoad = units::curvature::inverse_meter_t(lateralSignal.kappaRoad);
    curvatureOfSegmentsToNearPoint = lateralSignal.curvatureOfSegmentsToNearPoint;
    curvatureOfSegmentsToFarPoint = lateralSignal.curvatureOfSegmentsToFarPoint;
  }

  //! @brief Sets the vehicle parameters. This should ideally only be called once.
  //! @param     steeringRatio                              steeringRatio
  //! @param     maximumSteeringWheelAngleAmplitude         maximumSteeringWheelAngleAmplitude
  //! @param     wheelbase                                  wheelbase
  void SetVehicleParameter(const double& steeringRatio,
                           const units::angle::radian_t& maximumSteeringWheelAngleAmplitude,
                           const units::length::meter_t& wheelbase)
  {
    in_steeringRatio = steeringRatio;
    in_steeringMax = maximumSteeringWheelAngleAmplitude;
    in_wheelBase = wheelbase;
  }

  //! @brief Sets the current velocity and the current steeringwheel angle of the vehicle.
  //! @param     velocity                              Current velocity
  //! @param     steeringWheelAngle         Current steeringwheel angle
  void SetVelocityAndSteeringWheelAngle(const units::velocity::meters_per_second_t& velocity,
                                        const units::angle::radian_t& steeringWheelAngle)
  {
    in_velocity = velocity;
    in_steeringWheelAngle = steeringWheelAngle;
  }

  protected:
    //! @brief laneWidth
    units::length::meter_t laneWidth{0.};
    //! @brief lateralDeviation
    units::length::meter_t lateralDeviation{0.};
    //! @brief gainLateralDeviation
    units::angular_acceleration::radians_per_second_squared_t gainLateralDeviation{20.0};
    //! @brief headingError
    units::angle::radian_t headingError{0.};
    //! @brief gainHeadingError
    units::frequency::hertz_t gainHeadingError{7.5};
    //! @brief kappaManoeuvre
    units::curvature::inverse_meter_t kappaManoeuvre{0.0_i_m};
    //! @brief kappaRoad
    units::curvature::inverse_meter_t kappaRoad{0.0_i_m};
    //! @brief curvatureOfSegmentsToNearPoint
    std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToNearPoint{0.0_i_m};
    //! @brief curvatureOfSegmentsToFarPoint
    std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToFarPoint{0.0_i_m};

    //! @brief current velocity
    units::velocity::meters_per_second_t in_velocity = 0.0_mps;
    //! @brief current angle of the steering wheel
    units::angle::radian_t in_steeringWheelAngle = 0.0_rad;

    //! @brief The steering ratio of the vehicle.
    double in_steeringRatio = 10.7;
    //! @brief The maximum steering wheel angle of the car in both directions in degree.
    units::angle::radian_t in_steeringMax = 180.0_rad;
    //! @brief The wheelbase of the car in m.
    units::length::meter_t in_wheelBase = 2.89_m;

    //! @brief Time to Average regulation over
    units::time::second_t tAverage{0.0_s};
    //! @brief Previous scheduling time (for calculation of cycle time lenght).
    units::time::millisecond_t timeLast{-100_ms};
    //! @brief running average of  mean curvature up to NearPoint
    units::curvature::inverse_meter_t meanCurvatureToNearPointSmoothLast{0.0_i_m};
    //! @brief running average of  mean curvature from NearPoint up to FarPoint
    units::curvature::inverse_meter_t meanCurvatureToFarPointSmoothLast{0.0_i_m};
    //! @brief running average of kappaRoad at referencepoint
    units::curvature::inverse_meter_t curvatureRoadSmoothLast{0.0_i_m};
};
}  // namespace scm