/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "../include/module/lateralControllerInterface.h"
#include "SteeringController.h"
namespace scm
{
/** \addtogroup Algorithm_Lateral
 * @{
 * \brief models the lateral controller of the driver
 *
 * This component models the lateral controller of the driver. It calculates a
 * steering wheel angle wish of the driver that tries to diminish the lateral
 * offset to the current or target lane in a lane change. This wish depends
 * also on the intensity of a lane change.
 *
 * \section SCM_Algorithm_Lateral_Inputs Inputs
 * Input variables:
 * name | meaning
 * -----|------
 * in_LatDisplacement        | The current control deviation of lateral displacment in m.
 * in_lateralDeviation       | The lateral deviation from the lane change or swerving trajectory [m].
 * in_headingError           | The heading error regarding the lane change or swerving trajectory [rad].
 * in_gainLateralDeviation   | Gain for lateral deviation control in AlgorithmLateralDriver [rad/s^2].
 * in_gainHeadingError       | Gain for heading error control in AlgorithmLateralDriver [1/s].
 * in_kappaSet               | Set value for trajectory curvature [1/m].
 * velocity                  | Current velocity
 * steeringWheelAngle        | Current steering wheel angle [rad]
 *
 */
class LateralController : public scm::lateral_controller::Interface
{
public:
  const std::string COMPONENTNAME = "AlgorithmLateral";

  LateralController()
  {
  }

  virtual ~LateralController() = default;

  scm::signal::LateralControllerOutput Trigger(const scm::signal::LateralControllerInput& input, units::time::millisecond_t time) override;

protected:
  SteeringController steeringController{};
  bool isActive{false};
};
/** @} */
}  // namespace scm