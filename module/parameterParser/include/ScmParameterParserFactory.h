/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <Stochastics/StochasticsInterface.h>

#include <memory>

#include "../../include/Logging/PublisherInterface.h"
#include "../../include/module/parameterParserInterface.h"
#include "ScmParameterParser.h"

namespace scm::parameter_parser::factory
{
inline std::unique_ptr<scm::parameter_parser::Interface> Create(StochasticsInterface* staticStochastics,
                                                                const scm::parameter::ParameterMapping* parameterMapping,
                                                                scm::publisher::PublisherInterface* const scmPublisher,
                                                                common::VehicleClass vehicleClass,
                                                                units::length::meter_t visibilityDistance)
{
  return std::unique_ptr<scm::parameter_parser::Interface>(scm::parameter_parser::Create(staticStochastics,
                                                                                         parameterMapping,
                                                                                         scmPublisher,
                                                                                         vehicleClass,
                                                                                         visibilityDistance));
}

}  // namespace scm::parameter_parser::factory