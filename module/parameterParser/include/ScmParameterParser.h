/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <Stochastics/StochasticsInterface.h>

#include <string>

#include "include/Logging/PublisherInterface.h"
#include "include/common/Parameters.h"
#include "include/common/VehicleProperties.h"

namespace scm::parameter_parser
{
class Interface;

//! @brief Creates an instance of a parameter_parser module
//! @note  The creator is responsible for destroying the instance
Interface* Create(StochasticsInterface* stochastics,
                  const scm::parameter::ParameterMapping* parameterMapping,
                  scm::publisher::PublisherInterface* const scmPublisher,
                  common::VehicleClass vehicleClass,
                  units::length::meter_t visibilityDistance);
}  // namespace scm::parameter_parser