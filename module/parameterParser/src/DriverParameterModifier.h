/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <algorithm>

#include "Signals/ParametersScmDefinitions.h"

class DriverParameterModifier
{
public:
  //! \brief Sets the scaled comfort acceleration/deceleration values depending on ego speed.
  //! Calculates the scaled Acceleration and Deceleration values by linear interpolation between comfort and maximumComfort values.
  //! Interpolation starts below accelerationAdjustmentThresholdVelocity.
  //! Maximum acceleration is reached at velocityForMaxAcceleration.
  static void ComfortAcceleration(units::velocity::meters_per_second_t velocity, const ComfortAccelerationParameterSet& set, DriverParameters& parameter);

  /*!
   * \brief Calculate the preview distance (in meters) of the driver from his preview time headway (in seconds) and environmental conditions
   */
  static units::length::meter_t CalculatePreviewDistance(units::length::meter_t previewDistanceMaximum, units::velocity::meters_per_second_t absoluteVelocity, units::time::second_t previewTimeHeadway, units::length::meter_t previewDistanceMinimum);
};
