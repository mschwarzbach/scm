/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "../include/ScmParameterParser.h"

#include "../../../include/Logging/PublisherInterface.h"
#include "ScmParameters.h"

namespace scm::parameter_parser
{
Interface* Create(StochasticsInterface* stochastics,
                  const scm::parameter::ParameterMapping* parameterMapping,
                  scm::publisher::PublisherInterface* const scmPublisher,
                  common::VehicleClass vehicleClass,
                  units::length::meter_t visibilityDistance)
{
  return new scm::ScmParameters(parameterMapping,
                                scmPublisher,
                                stochastics,
                                vehicleClass,
                                visibilityDistance);
}
}  // namespace scm::parameter_parser