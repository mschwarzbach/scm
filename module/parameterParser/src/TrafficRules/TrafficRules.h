/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * NOTE this file is generated.
 * Be careful with manual changes as they may be overridden in future without noticing.
 */

#pragma once

#include <units.h>

#include <stdexcept>
#include <string>

template <typename ValueType>
struct Rule
{
  bool applicable{false};  // in country
  ValueType value{};

  constexpr operator ValueType() const
  {
    return value;
  }
  constexpr bool operator==(ValueType const& other) const
  {
    return this->value == other;
  }
  constexpr bool operator!=(ValueType const& other) const
  {
    return this->value != other;
  }
};

struct TrafficRulesScm
{
  struct Common
  {
    Rule<bool> handsFreePhoneMandatory;
    Rule<bool> rightHandTraffic;

  } common;
  struct Urban
  {
    Rule<units::velocity::meters_per_second_t> openSpeedLimit;

  } urban;
  struct Rural
  {
    Rule<units::velocity::meters_per_second_t> openSpeedLimit;

  } rural;
  struct Motorway
  {
    Rule<units::velocity::meters_per_second_t> openSpeedLimit;
    Rule<bool> keepToOuterLanes;
    Rule<bool> DontOvertakeOnOuterLanes;
    Rule<bool> formRescueLane;
    Rule<bool> ZipperMerge;

  } motorway;
};

class GenerateTrafficRules
{
public:
  GenerateTrafficRules() = default;
  TrafficRulesScm Generate(const std::string& country);
};
