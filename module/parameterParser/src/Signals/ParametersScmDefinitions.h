/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  ParametersScmDefinitions.h
//! @brief This file contains several classes for sensor driver purposes
//-----------------------------------------------------------------------------

#pragma once
#include <units.h>

#include <optional>

#include "../../../../include/common/ScmDefinitions.h"
using namespace units::literals;
//! @brief This struct contains the stochastic activation model parameters
struct StochasticActivationModelParameters
{
  //! @brief earliestTimeToActivate
  units::time::millisecond_t earliestTimeToActivate = 0._ms;
  //! @brief latestTimeToActivate
  units::time::millisecond_t latestTimeToActivate = 2000._ms;
  //! @brief minimumProbabilityToActivate
  double minimumProbabilityToActivate = 15.;
  //! @brief maximumProbabilityToActivate
  double maximumProbabilityToActivate = 90.;
};

//! @brief This struct contains the behavioural patterns
struct BehaviouralPatterns
{
  //! @brief indicatorActivation
  StochasticActivationModelParameters indicatorActivation;
};

//! @brief This struct contains the national specifics
struct NationalSpecifics
{
  //! @brief behaviouralPatterns
  BehaviouralPatterns behaviouralPatterns;
};


//! @brief This struct is used to transport driver model parameters between and inside modules
struct DriverParameters
{
  //! @brief nationalSpecifics
  NationalSpecifics nationalSpecifics;

  // -----------------------------------------
  // perception related parameters
  // -----------------------------------------

  //! @brief The distance around the driver's vehicle, which the driver considers relevant in m
  units::length::meter_t previewDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The threshold above which a change in the tangent of the angle between the vehicle and another vehicle is regarded as a lane change of the second vehicle
  std::optional<double> thresholdLoomingFovea;
  //! @brief The threshold above which a change of the size of a retinal projection in the fovea centralis can be perceived by the human eye in rad/s
  units::angular_velocity::radians_per_second_t thresholdRetinalProjectionAngularSpeedFovea{ScmDefinitions::DEFAULT_VALUE};
  //! @brief A flag to activate a perfect perception by the driver model (for simulating a HAF-Dummy)
  bool idealPerception{false};

  // -----------------------------------------
  // reaction time related parameters
  // -----------------------------------------

  //! @brief The minimum value of the driver's reaction time regarding rule-based behaviour in get
  units::time::second_t reactionBaseTimeMinimum{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The maximum value of the driver's reaction time regarding rule-based behaviour in get
  units::time::second_t reactionBaseTimeMaximum{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mean value of the driver's reaction time regarding rule-based behaviour in s
  units::time::second_t reactionBaseTimeMean{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The standard deviation of the driver's reaction time regarding rule-based behaviour in s
  units::time::second_t reactionBaseTimeStandardDeviation{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The minimum value of the driver's reaction time regarding skill-based behaviour in s
  units::time::second_t adjustmentBaseTimeMinimum{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The maximum value of the driver's reaction time regarding skill-based behaviour in s
  units::time::second_t adjustmentBaseTimeMaximum{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mean value of the driver's reaction time regarding skill-based behaviour in s
  units::time::second_t adjustmentBaseTimeMean{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The standard deviation of the driver's reaction time regarding skill-based behaviour in s
  units::time::second_t adjustmentBaseTimeStandardDeviation{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The minimum value of the driver's pedal change time in s
  units::time::second_t pedalChangeTimeMinimum{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mean value of the driver's pedal change time in s
  units::time::second_t pedalChangeTimeMean{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The standard deviation of the driver's pedal change time in s
  units::time::second_t pedalChangeTimeStandardDeviation{ScmDefinitions::DEFAULT_VALUE};

  // -----------------------------------------
  // simplified cognition related parameters
  // -----------------------------------------

  //! @brief A flag to activate high cognitive mode
  bool highCognitiveMode{false};
  //! @brief The amount of drivers which are able to anticipate the actions of other agents
  std::optional<double> anticipationQuota;
  //! @brief A factor between 0 and 1 to model agents cooperative behavior
  double agentCooperationFactor{ScmDefinitions::DEFAULT_VALUE};
  //! @brief A factor between 0 and 1 to model agents behaviour when detecting slow or stillstanding vehicles on side lane
  double agentSuspiciousBehaviourEvasionFactor{ScmDefinitions::DEFAULT_VALUE};
  //! @brief A factor between 0 and 1 to apply the keep-right-rule (the higher the factor, the higher the obligation to the keep-right-rule)
  std::optional<double> outerKeepingIntensity;
  //! @brief A factor between 0 and 1 to favour the current lane against neighbouring lanes (the higher the factor, the higher the favour for the current lane)
  std::optional<double> egoLaneKeepingIntensity;
  //! @brief Percentage of drivers which ignore the prohibition to overtake on right side
  std::optional<double> rightOvertakingProhibitionIgnoringQuota;
  //! @brief Percentage of drivers which ignore the prohibition to change lanes due to lane markings
  std::optional<double> laneChangeProhibitionIgnoringQuota;
  //! @brief The percentage how early the driver start reacting to a new speed limitation or how late to an increase of the legal velocity
  double speedLimitAnticipationPercentage = 0.;
  //! @brief The lateral safety distance to an obstacle, which is considered in evading manoeuvres in m
  units::length::meter_t lateralSafetyDistanceForEvading = 0._m;
  //! @brief Percentage of drivers which drive off-center in the lane
  std::optional<double> lateralOffsetNeutralPositionQuota;
  //! @brief The lateral offset the driver will keep to the center of the lane
  units::length::meter_t lateralOffsetNeutralPosition{ScmDefinitions::DEFAULT_VALUE};
  //! @brief A flag if the offset is used or not.
  bool lateralOffsetUsed = true;
  //! @brief The lateral offset the driver will keep to build up a rescue lane
  units::length::meter_t lateralOffsetNeutralPositionRescueLane{ScmDefinitions::DEFAULT_VALUE};

  //! @brief outerKeepingQuota
  std::optional<double> outerKeepingQuota;
  //! @brief egoLaneKeepingQuota
  std::optional<double> egoLaneKeepingQuota;

  //! @brief useShoulderInTrafficJamQuota
  std::optional<double> useShoulderInTrafficJamQuota;
  //! @brief useShoulderInTrafficJamToExitQuota
  std::optional<double> useShoulderInTrafficJamToExitQuota;
  //! @brief useShoulderWhenPeerPressuredQuota
  std::optional<double> useShoulderWhenPeerPressuredQuota;
  //! @brief distractionPercentage
  std::optional<double> distractionPercentage;
  //! @brief distractionQuota
  std::optional<double> distractionQuota;

  // -----------------------------------------
  // speed control related parameters
  // -----------------------------------------

  //! @brief The velocity the driver wants to drive without any other influences in m/s
  units::velocity::meters_per_second_t desiredVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The amount of velocity which the driver is willing to violate his desired velocity in uncomfortable situations in m/s
  units::velocity::meters_per_second_t amountOfDesiredVelocityViolation{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The amount of velocity which the driver is willing to violate the speed limit in m/s
  units::velocity::meters_per_second_t amountOfSpeedLimitViolation{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mimium amount of time the driver wants to drive at his targeted speed in a specific lane in s
  units::time::second_t desiredTimeAtTargetSpeed{ScmDefinitions::DEFAULT_VALUE};

  // -----------------------------------------
  // car following related parameters
  // -----------------------------------------

  //! @brief The proportionality factor for the reduction of following distance at higher speeds (Steven's power law)
  double proportionalityFactorForFollowingDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The distance the driver tries to maintain to his leading vehicle at standstill in m
  units::length::meter_t carQueuingDistance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The distance to the leading vehicle at which the driver will start moving his vehicle again after standstill in a traffic jam in m
  units::length::meter_t carRestartDistance{ScmDefinitions::DEFAULT_VALUE};

  // -----------------------------------------
  // acceleration behaviour related parameters
  // -----------------------------------------

  //! @brief The maximum longitudinal acceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t maximumLongitudinalAcceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The maximum longitudinal deceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t maximumLongitudinalDeceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The maximum lateral acceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t maximumLateralAcceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mean value of the maximum lateral acceleration of all drivers in m/s2
  units::acceleration::meters_per_second_squared_t maximumLateralAccelerationMean{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The comfort longitudinal acceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t comfortLongitudinalAcceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The comfort longitudinal deceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t comfortLongitudinalDeceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The comfort lateral acceleration of the driver in m/s2
  units::acceleration::meters_per_second_squared_t comfortLateralAcceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The mean value of the comfort lateral acceleration of all drivers in m/s2
  units::acceleration::meters_per_second_squared_t comfortLateralAccelerationMean{ScmDefinitions::DEFAULT_VALUE};

  // -----------------------------------------
  // lane change related parameters
  // -----------------------------------------
  //! @brief The comfort angular steering wheel velocity in rad/s
  units::angular_velocity::radians_per_second_t comfortAngularVelocitySteeringWheel{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The maximum angular steering wheel velocity in rad/s
  units::angular_velocity::radians_per_second_t maximumAngularVelocitySteeringWheel{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The time for headaway in free lange change
  units::time::second_t timeHeadwayFreeLaneChange{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief This struct contains the driver parameter modifications
struct DriverParameterModifications
{
  //! @brief Factor to calculate the maximum comfort acceleration by multiplying with the comfort acceleration
  double maxComfortAccelerationFactor = 1.0;
  //! @brief Factor to calculate the maximum comfort deceleration by multiplying with the comfort acceleration
  double maxComfortDecelerationFactor = 1.0;
  //! @brief The velocity below which the comfort acceleration is increased until the maximum comfort value in m/s
  units::velocity::meters_per_second_t accelerationAdjustmentThresholdVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The velocity below which the maximum comfort acceleration is used in m/s
  units::velocity::meters_per_second_t velocityForMaxAcceleration{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief This struct contains the comfort acceleration and deceleration parameters
struct ComfortAccelerationParameterSet
{
  //! @brief comfortAcceleration
  units::acceleration::meters_per_second_squared_t comfortAcceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief comfortDeceleration
  units::acceleration::meters_per_second_squared_t comfortDeceleration{ScmDefinitions::DEFAULT_VALUE};
  //! @brief modifications
  DriverParameterModifications modifications;
};
