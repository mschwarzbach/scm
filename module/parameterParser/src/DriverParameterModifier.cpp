/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DriverParameterModifier.h"

#include <algorithm>

#include "include/common/ScmDefinitions.h"

void DriverParameterModifier::ComfortAcceleration(units::velocity::meters_per_second_t velocity, const ComfortAccelerationParameterSet& set, DriverParameters& parameter)
{
  if (velocity < set.modifications.accelerationAdjustmentThresholdVelocity)
  {
    const auto velocityThreshold{set.modifications.accelerationAdjustmentThresholdVelocity};
    const auto velocityForMaxAcceleration{set.modifications.velocityForMaxAcceleration};
    const auto maxComfortAcceleration{set.comfortAcceleration * set.modifications.maxComfortAccelerationFactor};
    const auto maxComfortDeceleration{set.comfortDeceleration * set.modifications.maxComfortDecelerationFactor};

    /* Linear interpolation from comfort to maximum comfort acceleration */
    /* Maximum acceleration is reached at velocityForMaxAcceleration */
    const auto relativeVelocityDifference = ((velocity - velocityForMaxAcceleration) / (velocityThreshold - velocityForMaxAcceleration));
    const double comfortFactor = std::clamp(1. - relativeVelocityDifference.value(), 0., 1.);

    parameter.comfortLongitudinalAcceleration = set.comfortAcceleration + (maxComfortAcceleration - set.comfortAcceleration) * comfortFactor;
    parameter.comfortLongitudinalDeceleration = set.comfortDeceleration + (maxComfortDeceleration - set.comfortDeceleration) * comfortFactor;
  }
  else
  {
    parameter.comfortLongitudinalAcceleration = set.comfortAcceleration;
    parameter.comfortLongitudinalDeceleration = set.comfortDeceleration;
  }
}

units::length::meter_t DriverParameterModifier::CalculatePreviewDistance(units::length::meter_t previewDistanceMaximum, units::velocity::meters_per_second_t absoluteVelocity, units::time::second_t previewTimeHeadway, units::length::meter_t previewDistanceMinimum)
{
  // derive previewDistance from previewTimeHeadway
  auto previewDistanceFromTimeHeadway = previewTimeHeadway * absoluteVelocity;
  return std::clamp(previewDistanceFromTimeHeadway, previewDistanceMinimum, previewDistanceMaximum);
}