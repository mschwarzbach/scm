/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file ScmParameters.h
#pragma once

#include "DriverBehavior/DriverBehavior.h"
#include "DriverCharacteristicsSampler.h"
#include "Signals/ParametersScmDefinitions.h"
#include "TrafficRules/TrafficRules.h"
#include "include/Logging/PublisherInterface.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/VehicleProperties.h"
#include "include/module/parameterParserInterface.h"

namespace scm
{

//! @brief defines constants or creates stochastic values that are used in more than one module
class ScmParameters : public scm::parameter_parser::Interface
{
public:
  //! @brief COMPONENTNAME
  const std::string COMPONENTNAME = "ParameterParser";

  //! @brief specialized constructor
  //! @param parameter const scm::parameter::ParameterMapping*
  //! @param scmPublisher scm::publisher::PublisherInterface*
  //! @param stochastics StochasticsInterface*
  //! @param vehicleClass common::VehicleClass
  //! @param visibilityDistance double
  ScmParameters(const scm::parameter::ParameterMapping* parameter,
                scm::publisher::PublisherInterface* const scmPublisher,
                StochasticsInterface* stochastics,
                common::VehicleClass vehicleClass,
                units::length::meter_t visibilityDistance);
  virtual ~ScmParameters() = default;

  scm::signal::ParameterParserOutput Trigger(units::velocity::meters_per_second_t egoVelocity) override;

  //! @brief Calculates a value with a stochastic distribution including lower and upper boundaries
  //! @param distribution This contains the distribution function and lower and upper boundary
  //! @return roll stochastic value
  virtual double CalculateStochastic(scm::parameter::StochasticDistribution distribution);

protected:
  //! @brief Starts the initialization sequence of the class instance - mostly parsing inputs virtual
  void Initialize();

  //! @brief Prepares all values that are necessary for the DriverParameterSignal
  virtual void PrepareDriverParameter();

  //! @brief Calculate the value used to determine when the driver will start reacting to a perceived speed limitation sign(0. = start braking at sign lowering speed limit; 1. = braking to new speed limit fully completed at sign)
  //! @param driverBehavior const GenerateDriverBehavior&
  virtual void CalculateSpeedLimitAnticipationPercentage(const GenerateDriverBehavior& driverBehavior);

  // --- Driver Characteristics

  //! @brief set ego agent to world traffic rules
  void EgoAgentAdapterToWorldTrafficRules();

  //! @brief Driver Characteristics Sampler
  std::unique_ptr<DriverCharacteristicsSampler> driverCharacteristicsSampler;
  //! @brief data struct for driver parameters
  DriverParameters out_driverParameters;
  //! @brief data struct for out_trafficRulesScm
  TrafficRulesScm out_trafficRulesScm{};

private:
  const scm::parameter::ParameterMapping* _parameterMapping;
  scm::publisher::PublisherInterface* const _scmPublisher;
  StochasticsInterface* _stochastics;
  common::VehicleClass _vehicleClass{};
  units::length::meter_t _visibilityDistance{};

  //! @brief Publishes all out_DriverParameters values
  void LogDriverParameters();

  //! @brief Checks if the DCS is required to get values for missing parameters
  //! @return true if reuired parameter is missing, false otherwise
  bool IsDriverCharacteristicsSamplerRequired();

  //! @brief Apply DCS to override certain driver parameters in the output struct
  void ApplyDriverCharacteristicsSampler();

  //! @brief Checks if a parameter is initialized with the default value
  //! @param value Value to check
  //! @return True if value equals - 999.0, false otherwise.
  bool IsUnset(double value);

  // --- Internal Variables

  //! @brief Flag that indicates the need to initialize - e.g parsing input informations
  bool _initialisation = false;
  //! @brief Preview time headway of the driver in s (internal stochastic variable)
  units::time::second_t previewTimeHeadway{ScmDefinitions::DEFAULT_VALUE};

  // --- Internal Parameters
  // driver model related
  //! @brief The threshold above which a change of the size of a retinal projection in the fovea centralis can be perceived by the human eye in rad/s
  const units::angular_velocity::radians_per_second_t thresholdRetinalProjectionAngularSpeedFovea = 0.003_rad_per_s;
  //! @brief The minimum value of the driver's reaction time regarding skill-based behaviour in s
  const units::time::second_t adjustmentBaseTimeMinimum = 0.200_s;
  //! @brief The maximum value of the driver's reaction time regarding skill-based behaviour in s
  const units::time::second_t adjustmentBaseTimeMaximum = 1.500_s;
  //! @brief The mean value of the driver's reaction time regarding skill-based behaviour in s
  const units::time::second_t adjustmentBaseTimeMean = 0.900_s;
  //! @brief The standard deviation of the driver's reaction time regarding skill-based behaviour in s
  const units::time::second_t adjustmentBaseTimeStandardDeviation = 0.613_s;
  //! @brief The minimum value of the driver's pedal change time in s
  const units::time::second_t pedalChangeTimeMinimum = 0.180_s;
  //! @brief The mean value of the the driver's pedal change time in s
  const units::time::second_t pedalChangeTimeMean = 0.373_s;
  //! @brief The standard deviation of the the driver's pedal change time in s
  const units::time::second_t pedalChangeTimeStandardDeviation = 0.138_s;
  //! @brief The mean value of the lateral safety distance to an obstacle, which is considered in evading manoeuvres in m
  const units::length::meter_t lateralSafetyDistanceForEvadingMean = 1.0_m;
  //! @brief The standard deviation of the lateral safety distance to an obstacle, which is considered in evading manoeuvres in m
  const units::length::meter_t lateralSafetyDistanceForEvadingStandardDeviation = 0.5_m;
  //! @brief The minimum value of the lateral safety distance to an obstacle, which is considered in evading manoeuvres in m
  const units::length::meter_t lateralSafetyDistanceForEvadingMin = 0.3_m;
  //! @brief The maximum value of the lateral safety distance to an obstacle, which is considered in evading manoeuvres in m
  const units::length::meter_t lateralSafetyDistanceForEvadingMax = 2.0_m;

  // --- External Parameters
  // driver model related (Initialised - not for Output)
  //! @brief The minimum value of the driver's preview distance in m
  units::length::meter_t previewDistanceMinimum;

  //! @brief Tolerance for detecting uninitialized/defaulted parameters
  static constexpr double DEFAULT_VALUE_TOLERANCE = 1e-6;

  ComfortAccelerationParameterSet _comfortAccelerationParameter;
};

}  // namespace scm
