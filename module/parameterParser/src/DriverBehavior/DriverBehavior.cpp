/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * NOTE this file is generated.
 * Be careful with manual changes as they may be overridden in future without noticing.
 */

#include "DriverBehavior.h"

#include "include/common/OverloadPattern.h"

void GenerateDriverBehavior::InitDefaultSampler()
{
  if (!defaultSampler)
  {
    defaultSampler = *driverCharacteristicsSampler.SampleDriverCharacteristics().get();
  }
}

void GenerateDriverBehavior::throw_out_of_range(const std::string& key)
{
  using namespace std::string_literals;
  throw std::out_of_range("No default value available for parameter "s + key);
}

template <typename T>
T GenerateDriverBehavior::get_default([[maybe_unused]] const std::string& key)
{
  throw std::runtime_error("No specialization for given type T");
}

template <>
double GenerateDriverBehavior::get_default(const std::string& key)
{
  InitDefaultSampler();

  if (key.compare("DesiredVelocity") == 0)
    return defaultSampler->vWish.value();
  if (key.compare("AmountOfSpeedLimitViolation") == 0)
    return defaultSampler->deltaVViolation.value();
  if (key.compare("AgentCooperationFactor") == 0)
    return defaultSampler->agentCooperationFactor;
  if (key.compare("AgentSuspiciousBehaviourEvasionFactor") == 0)
    return defaultSampler->agentSuspiciousBehaviourEvasionFactor;
  if (key.compare("ComfortLongitudinalAcceleration") == 0)
    return defaultSampler->comfortLongitudinalAcceleration.value();
  if (key.compare("ComfortLongitudinalDeceleration") == 0)
    return defaultSampler->comfortLongitudinalDeceleration.value();
  if (key.compare("OuterKeepingIntensity") == 0)
    return defaultSampler->outerKeepingIntensity;
  throw_out_of_range(key);
}

double SampleFromDistribution(const scm::parameter::StochasticDistribution& distribution, StochasticsInterface* stochastics)
{
  auto roller = [&](auto&& distribution)
  { return ScmSampler::RollForStochasticAttribute(distribution, stochastics); };
  return std::visit(roller, distribution);
}

void GenerateDriverBehavior::CheckForMandatoryDistributionTypes() const
{
  ExpectNormalDistribution("MaxLateralAcceleration");
  ExpectNormalDistribution("ComfortLateralAcceleration");

  ExpectLogNormalDistribution("ReactionBaseTime");
  ExpectLogNormalDistribution("DeltaVViolation");
}

void GenerateDriverBehavior::ExpectNormalDistribution(const std::string& key) const
{
  if (parameters.find(key) != parameters.end())
  {
    if (!std::holds_alternative<scm::parameter::NormalDistribution>(std::get<scm::parameter::StochasticDistribution>(parameters.at(key))))
    {
      throw std::out_of_range("GenerateDriverBehavior: Expected normal distribution for " + key);
    }
  }
}

void GenerateDriverBehavior::ExpectLogNormalDistribution(const std::string& key) const
{
  if (parameters.find(key) != parameters.end())
  {
    if (!std::holds_alternative<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(parameters.at(key))))
    {
      throw std::out_of_range("GenerateDriverBehavior: Expected lognormal distribution for " + key);
    }
  }
}

template <typename ParamType>
ParamType GenerateDriverBehavior::GetParameter(const std::string& key)
{
  if (parameters.find(key) != parameters.end())
    return std::get<ParamType>(std::visit(
        overloaded{
            [this](const scm::parameter::StochasticDistribution& dist) -> std::variant<bool, double>
            { return SampleFromDistribution(dist, this->stochastics); },
            [](ParamType value) -> std::variant<bool, double>
            { return value; }},
        parameters.at(key)));

  return get_default<ParamType>(key);
}

Behaviors::Driverbehavior GenerateDriverBehavior::InitBehavior()
{
  return {
      GetParameter<bool>("EnableHighCognitiveMode"),
      GetParameter<bool>("IdealPerception"),
      units::make_unit<units::velocity::meters_per_second_t>(GetParameter<double>("AccelerationAdjustmentThresholdVelocity")),
      GetParameter<double>("AgentCooperationFactor"),
      GetParameter<double>("AgentSuspiciousBehaviourEvasionFactor"),
      GetParameter<double>("AnticipationQuota"),
      units::make_unit<units::length::meter_t>(GetParameter<double>("CarQueuingDistance")),
      units::make_unit<units::angular_velocity::radians_per_second_t>(GetParameter<double>("ComfortAngularVelocitySteeringWheel")),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("ComfortLateralAcceleration")),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("ComfortLongitudinalAcceleration")),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("ComfortLongitudinalDeceleration")),
      units::make_unit<units::velocity::meters_per_second_t>(GetParameter<double>("DeltaVViolation")),
      units::make_unit<units::velocity::meters_per_second_t>(GetParameter<double>("DeltaVWish")),
      units::make_unit<units::time::second_t>(GetParameter<double>("DesiredTimeAtTargetSpeed")),
      GetParameter<double>("EgoLaneKeepingIntensity"),
      GetParameter<double>("EgoLaneKeepingQuota"),
      units::make_unit<units::length::meter_t>(GetParameter<double>("HysteresisForRestart")),
      GetParameter<double>("LaneChangeProhibitionIgnoringQuota"),
      units::make_unit<units::length::meter_t>(GetParameter<double>("LateralOffsetNeutralPosition")),
      GetParameter<double>("LateralOffsetNeutralPositionQuota"),
      units::make_unit<units::length::meter_t>(GetParameter<double>("LateralOffsetNeutralPositionRescueLane")),
      GetParameter<double>("MaxComfortAccelerationFactor"),
      GetParameter<double>("MaxComfortDecelerationFactor"),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("MaxLateralAcceleration")),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("MaxLongitudinalAcceleration")),
      units::make_unit<units::acceleration::meters_per_second_squared_t>(GetParameter<double>("MaxLongitudinalDeceleration")),
      units::make_unit<units::angular_velocity::radians_per_second_t>(GetParameter<double>("MaximumAngularVelocitySteeringWheel")),
      GetParameter<double>("OuterKeepingIntensity"),
      GetParameter<double>("OuterKeepingQuota"),
      units::make_unit<units::length::meter_t>(GetParameter<double>("PreviewDistanceMin")),
      units::make_unit<units::time::second_t>(GetParameter<double>("PreviewTimeHeadway")),
      GetParameter<double>("ProportionalityFactorForFollowingDistance"),
      units::make_unit<units::time::second_t>(GetParameter<double>("ReactionBaseTime")),
      GetParameter<double>("RightOvertakingProhibitionIgnoringQuota"),
      GetParameter<double>("ThresholdLooming"),
      units::make_unit<units::time::second_t>(GetParameter<double>("TimeHeadwayFreeLaneChange")),
      GetParameter<double>("UseShoulderInTrafficJamQuota"),
      GetParameter<double>("UseShoulderInTrafficJamToExitQuota"),
      GetParameter<double>("UseShoulderWhenPeerPressuredQuota"),
      units::make_unit<units::velocity::meters_per_second_t>(GetParameter<double>("VWish")),
      units::make_unit<units::velocity::meters_per_second_t>(GetParameter<double>("VelocityForMaxAcceleration")),
      GetTiming("indicatorActivation"),
      GetParameter<double>("DistractionPercentage"),
      GetParameter<double>("DistractionQuota")};
}

Timing GenerateDriverBehavior::GetTiming(const std::string& key) const
{
  if (key == "indicatorActivation")
    return indicatorActivation;
  throw std::out_of_range("GenerateDriverBehavior::GetTiming was called with invalid key " + key);
}
