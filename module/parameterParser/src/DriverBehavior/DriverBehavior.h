/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * NOTE this file is generated.
 * Be careful with manual changes as they may be overridden in future without noticing.
 */
//! @file  DriverBehavior.h
#pragma once

#include <Stochastics/StochasticsInterface.h>
#include <units.h>

#include <stdexcept>
#include <string>

#include "../DriverCharacteristics.h"
#include "../DriverCharacteristicsSampler.h"
#include "include/ScmSampler.h"
#include "include/common/ParameterInterface.h"
#include "include/common/Parameters.h"
#include "include/common/StochasticDefinitions.h"

//! @brief Struct definition for the description of a Timing
struct Timing
{
  //! @brief earliest
  double earliest;
  //! @brief latest
  double latest;
  //! @brief minProb
  double minProb;
  //! @brief maxProb
  double maxProb;
};

//! @brief Struct definition for the description of a Behaviors
struct Behaviors
{
  //! @brief Struct definition for the description of a Patterns
  struct Patterns
  {
    //! @brief indicatorActivation
    Timing indicatorActivation;
  };

  //! @brief patterns
  Patterns patterns;

  //! @brief Struct definition for the description of a Common
  struct Common
  {
    //! @brief PreviewDistanceMin
    units::length::meter_t PreviewDistanceMin;
    //! @brief IdealPerception
    bool IdealPerception;
    //! @brief EnableHighCognitiveMode
    bool EnableHighCognitiveMode;
    //! @brief AnticipationQuota
    double AnticipationQuota;
    //! @brief RightOvertakingProhibitionIgnoringQuota
    double RightOvertakingProhibitionIgnoringQuota;
    //! @brief LaneChangeProhibitionIgnoringQuota
    double LaneChangeProhibitionIgnoringQuota;
    //! @brief ProportionalityFactorForFollowingDistance
    double ProportionalityFactorForFollowingDistance;
    //! @brief MaxComfortAccelerationFactor
    double MaxComfortAccelerationFactor;
    //! @brief MaxComfortDecelerationFactor
    double MaxComfortDecelerationFactor;
    //! @brief LateralOffsetNeutralPositionQuota
    double LateralOffsetNeutralPositionQuota;
  };

  //! @brief common
  Common common;

  //! @brief Struct definition for the description of a Driverbehavior
  struct Driverbehavior
  {
    //! @brief EnableHighCognitiveMode
    bool EnableHighCognitiveMode;
    //! @brief IdealPerception
    bool IdealPerception;
    //! @brief AccelerationAdjustmentThresholdVelocity
    units::velocity::meters_per_second_t AccelerationAdjustmentThresholdVelocity;
    //! @brief AgentCooperationFactor
    double AgentCooperationFactor;
    //! @brief AgentSuspiciousBehaviourEvasionFactor
    double AgentSuspiciousBehaviourEvasionFactor;
    //! @brief AnticipationQuota
    double AnticipationQuota;
    //! @brief CarQueuingDistance
    units::length::meter_t CarQueuingDistance;
    //! @brief ComfortAngularVelocitySteeringWheel
    units::angular_velocity::radians_per_second_t ComfortAngularVelocitySteeringWheel;
    //! @brief ComfortLateralAcceleration
    units::acceleration::meters_per_second_squared_t ComfortLateralAcceleration;
    //! @brief ComfortLongitudinalAcceleration
    units::acceleration::meters_per_second_squared_t ComfortLongitudinalAcceleration;
    //! @brief ComfortLongitudinalDeceleration
    units::acceleration::meters_per_second_squared_t ComfortLongitudinalDeceleration;
    //! @brief DeltaVViolation
    units::velocity::meters_per_second_t DeltaVViolation;
    //! @brief DeltaVWish
    units::velocity::meters_per_second_t DeltaVWish;
    //! @brief DesiredTimeAtTargetSpeed
    units::time::second_t DesiredTimeAtTargetSpeed;
    //! @brief EgoLaneKeepingIntensity
    double EgoLaneKeepingIntensity;
    //! @brief EgoLaneKeepingQuota
    double EgoLaneKeepingQuota;
    //! @brief HysteresisForRestart
    units::length::meter_t HysteresisForRestart;
    //! @brief LaneChangeProhibitionIgnoringQuota
    double LaneChangeProhibitionIgnoringQuota;
    //! @brief LateralOffsetNeutralPosition
    units::length::meter_t LateralOffsetNeutralPosition;
    //! @brief LateralOffsetNeutralPositionQuota
    double LateralOffsetNeutralPositionQuota;
    //! @brief LateralOffsetNeutralPositionRescueLane
    units::length::meter_t LateralOffsetNeutralPositionRescueLane;
    //! @brief MaxComfortAccelerationFactor
    double MaxComfortAccelerationFactor;
    //! @brief MaxComfortDecelerationFactor
    double MaxComfortDecelerationFactor;
    //! @brief MaxLateralAcceleration
    units::acceleration::meters_per_second_squared_t MaxLateralAcceleration;
    //! @brief MaxLongitudinalAcceleration
    units::acceleration::meters_per_second_squared_t MaxLongitudinalAcceleration;
    //! @brief MaxLongitudinalDeceleration
    units::acceleration::meters_per_second_squared_t MaxLongitudinalDeceleration;
    //! @brief MaximumAngularVelocitySteeringWheel
    units::angular_velocity::radians_per_second_t MaximumAngularVelocitySteeringWheel;
    //! @brief OuterKeepingIntensity
    double OuterKeepingIntensity;
    //! @brief OuterKeepingQuota
    double OuterKeepingQuota;
    //! @brief PreviewDistanceMin
    units::length::meter_t PreviewDistanceMin;
    //! @brief PreviewTimeHeadway
    units::time::second_t PreviewTimeHeadway;
    //! @brief ProportionalityFactorForFollowingDistance
    double ProportionalityFactorForFollowingDistance;
    //! @brief ReactionBaseTime
    units::time::second_t ReactionBaseTime;
    //! @brief RightOvertakingProhibitionIgnoringQuota
    double RightOvertakingProhibitionIgnoringQuota;
    //! @brief ThresholdLooming
    double ThresholdLooming;
    //! @brief TimeHeadwayFreeLaneChange
    units::time::second_t TimeHeadwayFreeLaneChange;
    //! @brief UseShoulderInTrafficJamQuota
    double UseShoulderInTrafficJamQuota;
    //! @brief UseShoulderInTrafficJamToExitQuota
    double UseShoulderInTrafficJamToExitQuota;
    //! @brief UseShoulderWhenPeerPressuredQuota
    double UseShoulderWhenPeerPressuredQuota;
    //! @brief VWish
    units::velocity::meters_per_second_t VWish;
    //! @brief VelocityForMaxAcceleration
    units::velocity::meters_per_second_t VelocityForMaxAcceleration;
    //! @brief indicatorActivation
    Timing indicatorActivation;
    //! @brief DistractionPercentage
    double DistractionPercentage;
    //! @brief DistractionQuota
    double DistractionQuota;
  };

  //! @brief driverBehavior
  Driverbehavior driverBehavior;
};

//! @brief This class samples and provides the driver behavior specified in the profiles catalog.
class GenerateDriverBehavior
{
public:
  //! @brief specialized constuctor
  //! @param driverCharacteristicsSampler
  //! @param parameters
  //! @param stochastics
  GenerateDriverBehavior(DriverCharacteristicsSampler& driverCharacteristicsSampler, const scm::parameter::ParameterMapping& parameters, StochasticsInterface* stochastics)
      : driverCharacteristicsSampler(driverCharacteristicsSampler), stochastics(stochastics), parameters(parameters)
  {
  }

  //! @brief driverCharacteristicsSampler
  DriverCharacteristicsSampler& driverCharacteristicsSampler;
  //! @brief defaultSampler
  std::optional<DriverCharacteristics> defaultSampler;
  //! @brief stochastics
  StochasticsInterface* stochastics;
  //! @brief parameters
  const scm::parameter::ParameterMapping parameters;
  //! @brief indicatorActivation
  Timing indicatorActivation;

  //! @brief Init default sampler
  void InitDefaultSampler();

  //! @brief No default value available expect throw
  //! @param key
  [[noreturn]] static void throw_out_of_range(const std::string& key);

  //! @brief Get default
  //! @param key
  //! @return T
  template <typename T>
  T get_default([[maybe_unused]] const std::string& key);

  //! @brief Get timing
  //! @param key
  //! @return Timing
  Timing GetTiming(const std::string& key) const;

  //! @brief Get parameter
  //! @param key
  //! @return T
  template <typename T>
  T GetParameter(const std::string& key);

  //! @brief Check for mandatory distribution types
  void CheckForMandatoryDistributionTypes() const;

  //! @brief Expect normal distribution
  //! @param key
  void ExpectNormalDistribution(const std::string& key) const;

  //! @brief Expect log normal distribution
  //! @param key
  void ExpectLogNormalDistribution(const std::string& key) const;

  //! @brief return driver behavior
  //! @return Behaviors::Driverbehavior
  Behaviors::Driverbehavior InitBehavior();
};