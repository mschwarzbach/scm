/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  DriverCharacteristics.h

#pragma once

/** \addtogroup DriverCharacteristics
 * @{
 * \brief Stores all relevant driver characteristics sampled from DriverCharacteristicsSampler.
 *
 * \details The parameters are stored and sampled as floating point numbers with double precision.
 * Every parameter belongs to a variable class, described in the next 4 sections.
 *
 * \section Level_1: Measurable Variables
 * name  | meaning  | unit
 * ------|--------|-------------
 * age | Age of the driver in years | time [a]
 *
 * \section Level_2: Latent-Exogenous Variables
 * name  | meaning  | range
 * ------|--------|-------------
 * anxiety | Anxiety level of the agent on the State-Trait Anxiety Inventory (STAI) scale | 0 - 100
 * experienceSeeking | Experience seeking level of the agent on the Brief Sensation Seeking Scale (BSSS) | 1 - 5
 * thrillSeeking | Thrill seeking level of the agent on the Brief Sensation Seeking Scale (BSSS) | 1 - 5
 * boredomSusceptibility | Boredom susceptibility level of the agent on the Brief Sensation Seeking Scale (BSSS) | 1 - 5
 * drivingAnger | Driving anger level of the agent on the Driving Anger Scale (DAS) | 0 - 100
 *
 * \section Level_3: Latent-Endogenous Variables
 * name  | meaning  | range
 * ------|--------|-------------
 * drivingDynamics | How dynamic does the agent drive | 0 - 100 // TODO: determine with manu
 * safetyNeed | Need of the agent for safety and right-keeping| 0 - 100
 * driverPerformance | Driver performance of the agent | 0 - 100
 *
 * \section Level_4: Measurable Variables for SCM
 * name  | meaning  | unit
 * ------|--------|-------------
 * tgapego | The time headway that the driver wants to maintain | time [s]
 * comfortLongitudinalAcceleration | The comfort longitudinal acceleration of the driver | acceleration  [m/s^2]
 * comfortLongitudinalDeceleration | The comfort braking deceleration of the driver | acceleration  [m/s^2]
 * vWish | The favoured speed of the driver | speed [m/s]
 * gapLaneChange | The gap length to the driver in front when ego initiates a lane change | length [m] // TODO: check with manu
 * laneChangeProbability | The probability for a driver to change the lane | percent (no unit) // TODO: check with manu
 * deltaVViolation | The amount of speed the driver is willing to violate the speed limit | speed [m/s]
 * deltaDistanceViolation | The distance the driver is willing to violate the distance limit | length [m] // TODO: check with manu
 * rightKeepingFactor | TBD | percent (no unit) // TODO: check with manu
 * egoLaneKeepingIntensity | TBD | percent (no unit) // TODO: check with manu
 * rightOvertakingProhibitionIgnoringFactor | TBD | percent (no unit) // TODO: check with manu
 *
 */

#include <units.h>
struct DriverCharacteristics
{
  // Internal parameters in DriverCharacteristics model
  // TODO: check with Manuela Witt
  //! The gender of the driver. 0 and 1 for female and male.
  double gender;
  //! The age of the driver in years.
  double age;
  // TODO: check with Manuela Witt
  //! The driving routine of the driver from 1 to 6.
  double drivingRoutine;

  // TODO: check all variables with Manuela Witt
  //! The patience level of the agent.
  double patience;
  //! The risk tolerance level of the agent.
  double riskTolerance;
  //! The anxiety level of the agent on the State-Trait Anxiety Inventory (STAI) scale.
  double anxiety;
  //! The disinhibition level of the agent on the Brief Sensation Seeking Scale (BSSS).
  double disinhibition;
  //! The thrill seeking level of the agent on the Brief Sensation Seeking Scale (BSSS).
  double thrillSeeking;
  //! The boredom susceptibility level of the agent on the Brief Sensation Seeking Scale (BSSS).
  double boredomSusceptibility;

  // TODO: "Grouping Variables" rename: currently not used
  //! The driving dynamics value determines how dynamic the agent drives. It has values between 0 and 100.
  double drivingDynamics;
  //! The safety need value determines the need for safety and right keeping of the agent. It has values between 0 and 100.
  double safetyNeed;
  //! The driver performance value determines the driving performance of the agent. It has values between 0 and 100.
  double driverPerformance;

  // Final variables for SCM to parametrize Driver
  //! The favoured speed of the driver in m/s.
  units::velocity::meters_per_second_t vWish;
  //! The amount of speed the driver is willing to violate the speed limit in m/s.
  units::velocity::meters_per_second_t deltaVViolation;
  //! The willingness of an agent to cooperate.
  double agentCooperationFactor;
  //! The willingness of an agent to slow down or evade suspiciously behaving vehicles in side lanes.
  double agentSuspiciousBehaviourEvasionFactor;
  //! The time headway that the driver wants to maintain in s.
  units::time::second_t tgapego;
  //! TBD.
  // double rightOvertakingProhibitionIgnoringFactor;
  double outerKeepingIntensity;
  //! The comfort longitudinal acceleration of the driver in m/s^2.
  units::acceleration::meters_per_second_squared_t comfortLongitudinalAcceleration;
  //! The comfort braking deceleration of the driver in m/s^2.
  units::acceleration::meters_per_second_squared_t comfortLongitudinalDeceleration;
  //! The comfort lateral acceleration of the driver in m/s^2.
  units::acceleration::meters_per_second_squared_t comfortLateralAcceleration;
  // TODO: wird vorerst nicht an SCM weitergegeben.
  //! The quality how good the driver stays in the lane.
  double qualityLaneKeeping;

  //! The mean steering angle speed of the driver in deg/s later in rad/s.
  units::angular_velocity::radians_per_second_t stearingAngle;
  //! The time in s.
  units::time::second_t desiredTimeAtTargetSpeed;
  //! The total time in s a driver needs to react (steering or braking) when recognizing for example the end of a traffic jam.
  units::time::second_t reactionBaseTime;
};
/** @} */