/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmDynamicDriving.h"

#include <algorithm>

#include "include/common/CommonHelper.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/VehicleProperties.h"
using namespace units::literals;
namespace scm
{

void ScmDynamicDriving::SetInputs(const scm::signal::DynamicsModelInput& input)
{
  vehicleModelParameters = input.vehicleParameters;
  in_accPedalPos = input.longitudinalControllerOutput.accPedalPos;
  in_brakePedalPos = input.longitudinalControllerOutput.brakePedalPos;
  ApplyPedalPositionLimits();
  in_gear = input.longitudinalControllerOutput.gear;
  ApplyGearLimit();

  in_steeringWheelAngle = units::angle::radian_t(input.lateralControllerOutput.desiredSteeringWheelAngle);
  dynamicsInformation.roll = 0.0_rad;  // currently, no bike -> no roll
}

scm::signal::DynamicsModelOutput ScmDynamicDriving::Trigger(const scm::signal::DynamicsModelInput& input)
{
  SetInputs(input);

  // Lateral behavior
  _maxDeceleration = _oneG * GetFrictionCoefficient() * -1;

  const auto& ego = _query->GetHostVehicle();
  const auto yawAngle = units::angle::radian_t(ego.GetYaw());

  auto accVehicle = GetAccVehicle(in_accPedalPos, in_brakePedalPos, in_gear);

  auto v = units::velocity::meters_per_second_t(ego.GetVelocity().Length()) + accVehicle * _cycleTime;

  if (v < VLowerLimit)
  {
    accVehicle = 0.0_mps_sq;
    v = VLowerLimit;
  }

  // change of path coordinate since last time step [m]
  const units::length::meter_t ds = v * _cycleTime;
  // change of inertial x-position due to ds and yaw [m]
  const units::length::meter_t dx = ds * units::math::cos(yawAngle);
  // change of inertial y-position due to ds and yaw [m]
  const units::length::meter_t dy = ds * units::math::sin(yawAngle);
  // new inertial x-position [m]
  auto x = units::length::meter_t(ego.GetXY().x) + dx;
  // new inertial y-position [m]
  auto y = units::length::meter_t(ego.GetXY().y) + dy;

  dynamicsInformation.acceleration = accVehicle;
  dynamicsInformation.velocityX = v * units::math::cos(yawAngle);
  dynamicsInformation.velocityY = v * units::math::sin(yawAngle);
  dynamicsInformation.positionX = x;
  dynamicsInformation.positionY = y;
  dynamicsInformation.travelDistance = ds;

  // convert steering wheel angle to steering angle of front wheels [radian]
  const auto steering_angle = std::clamp(in_steeringWheelAngle / GetVehicleProperty(scm::common::vehicle::properties::SteeringRatio), -vehicleModelParameters.front_axle.max_steering, vehicleModelParameters.front_axle.max_steering);
  dynamicsInformation.steeringWheelAngle = steering_angle * GetVehicleProperty(scm::common::vehicle::properties::SteeringRatio);
  const auto wheelbase = units::length::meter_t(vehicleModelParameters.front_axle.bb_center_to_axle_center.x) - units::length::meter_t(vehicleModelParameters.rear_axle.bb_center_to_axle_center.x);
  // calculate curvature (Ackermann model; reference point of yawing = rear axle!) [radian]
  units::curvature::inverse_meter_t steeringCurvature = units::math::tan(steering_angle) / wheelbase;
  // change of yaw angle due to ds and curvature [radian]
  units::angle::radian_t dpsi{units::math::atan(steeringCurvature * ds)};

  dynamicsInformation.yawRate = dpsi / _cycleTime;
  dynamicsInformation.yawAcceleration = (dynamicsInformation.yawRate - yawRatePrevious) / _cycleTime;

  yawRatePrevious = units::angular_velocity::radians_per_second_t(dynamicsInformation.yawRate);
  dynamicsInformation.centripetalAcceleration = units::inverse_radian(1) * dynamicsInformation.yawRate * v;
  // new yaw angle in current time step [radian]
  const auto psi = units::angle::radian_t(ego.GetYaw()) + dpsi;
  dynamicsInformation.yaw = psi;
  return {dynamicsInformation};
}

void ScmDynamicDriving::ApplyGearLimit()
{
  in_gear = std::min(std::max(in_gear, 1), static_cast<int>(GetVehicleProperty(scm::common::vehicle::properties::NumberOfGears)));
}

void ScmDynamicDriving::ApplyPedalPositionLimits()
{
  in_accPedalPos = std::min(std::max(in_accPedalPos, 0.0), 1.0);
  in_brakePedalPos = std::min(std::max(in_brakePedalPos, 0.0), 1.0);
}

units::angular_velocity::revolutions_per_minute_t ScmDynamicDriving::GetEngineSpeedByVelocity(units::velocity::meters_per_second_t xVel, int gear)
{
  return 1_rad * (xVel * GetVehicleProperty(scm::common::vehicle::properties::AxleRatio) * GetVehicleProperty(scm::common::vehicle::properties::GearRatio + std::to_string(gear))) /
         (vehicleModelParameters.rear_axle.wheel_diameter * 0.5);  // an dynamic wheel radius rDyn must actually be used here
}

units::torque::newton_meter_t ScmDynamicDriving::GetEngineMomentMax(units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  const units::torque::newton_meter_t maximumEngineTorque{GetVehicleProperty(scm::common::vehicle::properties::MaximumEngineTorque)};
  const units::angular_velocity::revolutions_per_minute_t maximumEngineSpeed{GetVehicleProperty(scm::common::vehicle::properties::MaximumEngineSpeed)};
  const units::angular_velocity::revolutions_per_minute_t minimumEngineSpeed{GetVehicleProperty(scm::common::vehicle::properties::MinimumEngineSpeed)};

  auto torqueMax = maximumEngineTorque;  // initial value at max
  auto speed = engineSpeed;

  bool isLowerSection = engineSpeed < minimumEngineSpeed + 1000.0_rpm;
  bool isBeyondLowerSectionBorder = engineSpeed < minimumEngineSpeed;
  bool isUpperSection = engineSpeed > maximumEngineSpeed - 1000.0_rpm;
  bool isBeyondUpperSectionBorder = engineSpeed > maximumEngineSpeed;

  if (isLowerSection)
  {
    if (isBeyondLowerSectionBorder)  // not within limits
    {
      speed = minimumEngineSpeed;
    }
    torqueMax = units::inverse_radian(0.5 / M_PI) * (1000_rpm - (speed - minimumEngineSpeed)) * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.1) + maximumEngineTorque;
  }
  else if (isUpperSection)
  {
    if (isBeyondUpperSectionBorder)
    {
      speed = maximumEngineSpeed;
    }

    torqueMax = units::inverse_radian(0.5 / M_PI) * (speed - maximumEngineSpeed + 1000_rpm) * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.04) + maximumEngineTorque;
  }
  return torqueMax;
}

units::acceleration::meters_per_second_squared_t ScmDynamicDriving::GetAccelerationFromRollingResistance()
{
  double rollingResistanceCoeff = .0125;  // Dummy value, get via vehicle Parameters (vehicleModelParameters.rollingDragCoefficient)
  const units::acceleration::meters_per_second_squared_t accDueToRollingResistance = -rollingResistanceCoeff * _oneG;
  return accDueToRollingResistance;
}

units::acceleration::meters_per_second_squared_t ScmDynamicDriving::GetAccelerationFromAirResistance(units::velocity::meters_per_second_t velocity)
{
  units::force::newton_t forceAirResistance = -.5 * _rho * GetVehicleProperty(scm::common::vehicle::properties::AirDragCoefficient) *
                                              units::area::square_meter_t(GetVehicleProperty(scm::common::vehicle::properties::FrontSurface)) * velocity * velocity;
  const units::acceleration::meters_per_second_squared_t accDueToAirResistance = forceAirResistance / vehicleModelParameters.mass;
  return accDueToAirResistance;
}

units::torque::newton_meter_t ScmDynamicDriving::GetEngineMomentMin(units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  return GetEngineMomentMax(engineSpeed) * -.1;
}

double ScmDynamicDriving::GetFrictionCoefficient()
{
  const double friction = 1.0;  // TODO! was previously _dependency->GetFriction()
  return friction * GetVehicleProperty(scm::common::vehicle::properties::FrictionCoefficient);
}

double ScmDynamicDriving::GetVehicleProperty(const std::string& propertyName)
{
  const auto property = scm::common::map::query(vehicleModelParameters.properties, propertyName);
  //   THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::torque::newton_meter_t ScmDynamicDriving::GetEngineMoment(double gasPedalPos, int gear)
{
  const auto& ego = _query->GetHostVehicle();
  const auto xVel = units::velocity::meters_per_second_t(ego.GetVelocity().Length());
  const auto engineSpeedAtGear = GetEngineSpeedByVelocity(xVel, gear);
  const auto max = GetEngineMomentMax(engineSpeedAtGear);
  const auto min = GetEngineMomentMin(engineSpeedAtGear);

  return (units::math::fabs(min) + max) * gasPedalPos + min;
}

units::acceleration::meters_per_second_squared_t ScmDynamicDriving::GetAccFromEngineMoment(units::velocity::meters_per_second_t xVel, units::torque::newton_meter_t engineMoment, int chosenGear)
{
  units::torque::newton_meter_t wheelSetMoment = engineMoment * (GetVehicleProperty(scm::common::vehicle::properties::AxleRatio) * GetVehicleProperty(scm::common::vehicle::properties::GearRatio + std::to_string(chosenGear)));
  units::force::newton_t wheelSetForce = wheelSetMoment / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);

  const auto vehicleSetForce = wheelSetForce;
  const units::acceleration::meters_per_second_squared_t acc = vehicleSetForce / vehicleModelParameters.mass;

  return acc;
}

units::acceleration::meters_per_second_squared_t ScmDynamicDriving::GetAccVehicle(double accPedalPos, double brakePedalPos, int gear)
{
  units::acceleration::meters_per_second_squared_t resultAcc{0};

  const auto& ego = _query->GetHostVehicle();
  const auto xVel = units::velocity::meters_per_second_t(ego.GetVelocity().Length());

  if (brakePedalPos > 0.)  // Brake
  {
    units::acceleration::meters_per_second_squared_t accelerationDueToPedal{brakePedalPos * _oneG * -1.};
    units::angular_velocity::revolutions_per_minute_t engineSpeed{GetEngineSpeedByVelocity(xVel, gear)};
    auto engineDrag{GetEngineMomentMin(engineSpeed)};
    auto accelerationDueToDrag{GetAccFromEngineMoment(xVel, engineDrag, gear)};
    if (accelerationDueToPedal > 0.0_mps_sq || accelerationDueToDrag > 0.0_mps_sq)
    {
      throw std::runtime_error("ScmDynamicDriving - Wrong sign for acceleration!");
    }

    resultAcc = accelerationDueToPedal + accelerationDueToDrag;

    resultAcc = units::math::fmax(_maxDeceleration, resultAcc);
  }
  else  // Gas
  {
    const auto engineMoment = GetEngineMoment(accPedalPos, gear);
    resultAcc = GetAccFromEngineMoment(xVel, engineMoment, gear);
  }

  const auto accelerationDueToAirResistance = GetAccelerationFromAirResistance(xVel);
  const auto accelerationDueToRollingResistance = GetAccelerationFromRollingResistance();

  return resultAcc + accelerationDueToAirResistance + accelerationDueToRollingResistance;
}

}  // namespace scm