/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *               2016-2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <OsiQueryLibrary/osiql.h>
#include <units.h>

#include "include/common/DynamicsInformation.h"
#include "include/module/dynamicsModelInterface.h"
#include "include/signal/dynamicsModelInput.h"
#include "include/signal/dynamicsModelOutput.h"

using namespace units::literals;
namespace scm
{

/** \addtogroup Dynamics_RegularDriving
 * @{
 * \brief models the longitudinal and lateral dynamics of the vehicle coresponding to the pedal and steering inputs
 *
 * This component models the reaction of the vehicle based on the pedal and steering wheel operation of the driver.
 * It calculates velocity and position of the agent according to his acceleration and his collision state.
 * If no collision took place in the previous time step where the agent was
 * the rear opponent, the velocity and position of the agent is calculated
 * according to his acceleration wish.
 * If the agent was the rear opponent in a collision, its velocity is set to
 * the one of the front agent. The position is calculated according to this
 * velocity. Additionaly the maximum change of steering wheel angle according to the
 * desired steering wheel angle is calculated.
 */

class ScmDynamicDriving : public scm::dynamics_model::Interface
{
public:
  //! Name of the current component
  const std::string COMPONENTNAME = "DynamicRegularDriving";

  explicit ScmDynamicDriving(units::time::millisecond_t cycleTime, const osiql::Query* query)
      : _cycleTime{cycleTime}, _query{query}
  {
  }

  ~ScmDynamicDriving() override = default;

  scm::signal::DynamicsModelOutput Trigger(const scm::signal::DynamicsModelInput& input) override;

private:
  units::time::millisecond_t _cycleTime{};
  const osiql::Query* _query{nullptr};
  units::acceleration::meters_per_second_squared_t _maxDeceleration{};

  //! Applies limit to incoming geer
  void ApplyGearLimit();

  //! Applies limits to the incoming steering wheel angle and incoming pedal position
  void ApplyPedalPositionLimits();

  //! Get the acceleration of the vehicle from pedal position and gear.
  //! @param [in] accPedalPos         current Gas pedal position [%]
  //! @param [in] brakePedalPos       current Brake pedal position [%]
  //! @param [in] gear                current Gear
  //! @return current acceleration
  units::acceleration::meters_per_second_squared_t GetAccVehicle(double accPedalPos, double brakePedalPos, int gear);

  //! Get the acceleration (negative) caused by the air resistance of the vehicle.
  //! @param [in] velocity        absolute vehicle speed [m/s]
  //! @return acceleration due to rolling resistance [m/s^2]
  //-----------------------------------------------------------------------------
  units::acceleration::meters_per_second_squared_t GetAccelerationFromAirResistance(units::velocity::meters_per_second_t velocity);

  //! Get the acceleration (negative) caused by the rolling resistance of the wheels.
  //! @return acceleration due to rolling resistance [m/s^2]
  //-----------------------------------------------------------------------------
  units::acceleration::meters_per_second_squared_t GetAccelerationFromRollingResistance();

  //! Get the moment of the engine from pedal position and gear.
  //! @param [in] gasPedalPos         current Gas pedal position. [%]
  //! @param [in] lastGear            the last chosen gear.
  //! @param [in] carParameters       parameters of the car
  //! @return engine moment [Nm]
  //-----------------------------------------------------------------------------
  units::torque::newton_meter_t GetEngineMoment(double gasPedalPos, int lastGear);

  //! Get the drag moment of the engine corresponding to its speed
  //! @param [in] engineSpeed         the speed of the engine
  //! @param [in] carParameters       parameters of the car
  //! @return drag moment [Nm]
  units::torque::newton_meter_t GetEngineMomentMin(units::angular_velocity::revolutions_per_minute_t engineSpeed);

  //! Get the maximum possible torque of the engine in the current state
  //! @param [in] engineSpeed         the speed of the engine
  //! @param [in] carParameters       parameters of the car
  //! @return maximum moment in the current state [Nm]
  units::torque::newton_meter_t GetEngineMomentMax(units::angular_velocity::revolutions_per_minute_t engineSpeed);

  //! Calculate the engine speed coresponding to the current speed and gear of the car
  //! @param [in] xVel               longitudinal speed of the car
  //! @param [in] gear               current gear
  //! @param [in] carParameters      parameters of the car
  //! @return engine speed [1/min]
  units::angular_velocity::revolutions_per_minute_t GetEngineSpeedByVelocity(units::velocity::meters_per_second_t xVel, int gear);

  //! Calculate the resulting acceleration of the car with the delivered moment of the engine
  //! @param [in] xVel               longitudinal speed of the car (unused)
  //! @param [in] engineMoment       delivered moment of the engine
  //! @param [in] chosenGear         the curent gear
  //! @param [in] carParameters      parameters of the car
  //! @param [in] cycleTime          Cycle time of this components trigger task [ms] (unused)
  //! @return resulting acceleration [m/s²]
  units::acceleration::meters_per_second_squared_t GetAccFromEngineMoment(units::velocity::meters_per_second_t xVel, units::torque::newton_meter_t engineMoment, int chosenGear);

  //! Returns the friction coefficient according to enviroment and car conditions
  //! @return friction coefficient (currently ALWAYS 1)
  double GetFrictionCoefficient();

  //! Returns the property with given name in the VehicleModelParameters
  //! or throws an error if the property is missing
  double GetVehicleProperty(const std::string& propertyName);

  /** @name Private Variables
   * @{
   * */
  /** @name Internal Parameters
   * @{
   */

  //! The minimal velocity of the agent [m/s].
  const units::velocity::meters_per_second_t VLowerLimit = 0_mps;

  /**
   *  @} */
  // End of Internal Parameters
  /** @name External Parameters
   * @{
   */

  // --- Inputs

  //! Position of the accecaleration pedal position [%].
  double in_accPedalPos = 0;

  //! Position of the brake pedal position [%].
  double in_brakePedalPos = 0;

  //! Number of gears and position of gear.
  int in_gear = 0;

  //! The steering wheel angle [rad].
  units::angle::radian_t in_steeringWheelAngle{0};

  // --- Outputs

  //! Output dynamics containing (aLong,v,x,y,dpsi,psi)
  scm::DynamicsInformation dynamicsInformation;

  // --- Init Inputs

  //! Containing the vehicle parameters e.g. double carMass; double rDyn and more.
  //  mantle_api::VehicleProperties vehicleModelParameters;
  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;

  // Constants

  //! PI.
  units::angle::radian_t _twoPi = 2_rad * M_PI;

  //! value of earth gravity [m/s²].
  units::acceleration::meters_per_second_squared_t _oneG{9.81};

  //! air density.
  units::density::kilograms_per_cubic_meter_t _rho{1.23};

  units::angular_velocity::radians_per_second_t yawRatePrevious{0.0};
  /**
   *  @} */
  // End of External Parameters
  /**
   *  @} */
  // End of Private Variables
  void SetInputs(const signal::DynamicsModelInput& input);
};
/** @} */  // End of group DynamicsRegularDriving
}  // namespace scm
