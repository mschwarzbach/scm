/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <memory>

#include "../../../include/module/dynamicsModelInterface.h"
#include "ScmDynamicsModel.h"

namespace scm::dynamics_model::factory
{
inline std::unique_ptr<scm::dynamics_model::Interface> Create(units::time::millisecond_t cycleTime, const osiql::Query* query)
{
  return std::unique_ptr<scm::dynamics_model::Interface>(scm::dynamics_model::Create(cycleTime, query));
}

}  // namespace scm::dynamics_model::factory