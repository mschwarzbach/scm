/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <OsiQueryLibrary/osiql.h>
#include <units.h>
namespace scm::dynamics_model
{
class Interface;

/// @brief Creates an instance of a dynamics_model module
/// @note  The creator is responsible for destroying the instance
Interface* Create(units::time::millisecond_t cycleTime, const osiql::Query* query);
}  // namespace scm::dynamics_model