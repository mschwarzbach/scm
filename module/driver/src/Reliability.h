/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "ScmDefinitions.h"

namespace ReliabilityCalculations
{
//!@brief Calculates the reliability of observed vehicles based on the distante to the optical axis.
//! The farther from the optical axis the vehicle is perceived the worse the reliability of its data is.
//!@param angleToOpticalAxis Horizontal angle between the optical center of the driver and the perceived vehicle.
//!@return Calculated reliability
double CalculateReliability(units::angle::radian_t angleToOpticalAxis);

//!@brief Reduce reliability by a predefined amount depending on the parameter change rate.
//! Intended to be called after each extrapolation step.
//! The faster a parameter changes, the faster the reliability should decay.
//! TODO Note that currently only one DecayRate is implemented.
//!@param currentReliability Current reliability of the parameter class
//!@param changeRate         Parameter class for which the reliability should be updated
//!@param cycleTime          Cycle time of the simulation, used to calculate the decay rate
//!@return Reduced reliability
double DecayReliability(double currentReliability, ParameterChangeRate changeRate, units::time::millisecond_t cycleTime);
}  // namespace ReliabilityCalculations

namespace VisualReliabilityCalculations
{
std::map<VisualPerceptionSector, double> CalculateSectorUpdates(units::angle::radian_t angle);
}  // namespace VisualReliabilityCalculations