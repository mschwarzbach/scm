/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneChangeDimension.h"

#include "MentalModelInterface.h"

LaneChangeDimension::LaneChangeDimension(const MentalModelInterface& mentalModel)
    : _mentalModel(mentalModel)
{
}

units::length::meter_t LaneChangeDimension::EstimateLaneChangeLength() const
{
  const auto laneChangeWidth{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width};
  const auto lateralAcc{_mentalModel.GetDriverParameters().comfortLateralAcceleration};
  const auto longitudinalVelocity{_mentalModel.GetAbsoluteVelocityEgo(true)};
  return EstimateLaneChangeLength(laneChangeWidth, lateralAcc, longitudinalVelocity);
}

units::length::meter_t LaneChangeDimension::EstimateLaneChangeLength(units::length::meter_t laneChangeWidth, units::acceleration::meters_per_second_squared_t lateralAcc, units::velocity::meters_per_second_t longitudinalVelocity) const
{
  if (longitudinalVelocity != 0._mps)
  {
    // Add another 20% to the evading length as safety buffer
    double constexpr safetybuffer = 1.2;
    return longitudinalVelocity * units::math::sqrt(10. * units::math::fabs(laneChangeWidth) / std::sqrt(3.) / units::math::fabs(lateralAcc)) * safetybuffer;
  }
  else
  {
    return units::math::max(15._m,
                            units::math::min(_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane,
                                             _mentalModel.GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)));
  }
}
