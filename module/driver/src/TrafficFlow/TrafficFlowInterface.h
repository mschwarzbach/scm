/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrafficFlowInterface.h

#pragma once
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"

//! **************************************************************************************
//! @brief Interface for the current traffic flow, i.e. a possibility to track the current
//! mean velocities in surrounding lanes.
//! **************************************************************************************
class TrafficFlowInterface
{
public:
  virtual ~TrafficFlowInterface() = default;

  //! @brief Updates the mean velocities for surrounding lanes (left, right and ego).
  //! @param traffic vector of all surrounding vehicles
  virtual void UpdateMeanVelocities(const std::vector<const SurroundingVehicleInterface*>& traffic) = 0;

  //! @brief Returns the current mean velocity for a given surrounding lane.
  //! @param surroundingLane the lane for which the mean velocity is queried
  //! @return mean velocity
  virtual units::velocity::meters_per_second_t GetMeanVelocity(SurroundingLane surroundingLane) = 0;

  //! @brief Resets all three surrounding lanes to initial values.
  virtual void Reset() = 0;
};