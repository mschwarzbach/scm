/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneMeanVelocity.cpp

#include "LaneMeanVelocity.h"

using namespace units::literals;
LaneMeanVelocity::LaneMeanVelocity(units::time::millisecond_t cycleTime, units::velocity::meters_per_second_t desiredVelocity)
    : _desiredVelocity(desiredVelocity)
{
  const int RETROSPECT_TIME_AOI = 5;
  _maxSampleSize = 1000_ms * RETROSPECT_TIME_AOI / cycleTime;
}

void LaneMeanVelocity::IncorporateNewSample(units::velocity::meters_per_second_t sample)
{
  if (_mean == _desiredVelocity && sample != _desiredVelocity)
  {
    Reset();
  }
  if (_sampleSize < _maxSampleSize)
  {
    _sampleSize++;
  }
  _mean = _mean + (sample - _mean) / _sampleSize;
}

units::velocity::meters_per_second_t LaneMeanVelocity::GetMean()
{
  return _mean;
}

void LaneMeanVelocity::Reset()
{
  _sampleSize = 0;
  _mean = _desiredVelocity;
}
