/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrafficFlow.cpp

#include "TrafficFlow.h"

#include <numeric>

#include "LaneMeanVelocity.h"

TrafficFlow::TrafficFlow(units::time::millisecond_t cycleTime, units::velocity::meters_per_second_t desiredVelocity)
    : _desiredVelocity(desiredVelocity),
      _egoLane{std::make_unique<LaneMeanVelocity>(cycleTime, desiredVelocity)},
      _leftLane{std::make_unique<LaneMeanVelocity>(cycleTime, desiredVelocity)},
      _rightLane{std::make_unique<LaneMeanVelocity>(cycleTime, desiredVelocity)}
{
}

void TrafficFlow::UpdateMeanVelocities(const std::vector<const SurroundingVehicleInterface*>& traffic)
{
  _leftLane->IncorporateNewSample(CalculateCurrentMeanVelocity(traffic, SurroundingLane::LEFT));
  _egoLane->IncorporateNewSample(CalculateCurrentMeanVelocity(traffic, SurroundingLane::EGO));
  _rightLane->IncorporateNewSample(CalculateCurrentMeanVelocity(traffic, SurroundingLane::RIGHT));
}

units::velocity::meters_per_second_t TrafficFlow::CalculateCurrentMeanVelocity(const std::vector<const SurroundingVehicleInterface*>& traffic, SurroundingLane surroundingLane)
{
  std::vector<const SurroundingVehicleInterface*> vehiclesOnLane{};
  std::copy_if(traffic.cbegin(), traffic.cend(), std::back_inserter(vehiclesOnLane), [surroundingLane](const SurroundingVehicleInterface* vehicle)
               { return SurroundingLane2RelativeLane.at(surroundingLane) == vehicle->GetLaneOfPerception() && !vehicle->BehindEgo(); });
  if (vehiclesOnLane.empty())
  {
    return _desiredVelocity;
  }
  auto average = std::accumulate(vehiclesOnLane.cbegin(), vehiclesOnLane.cend(), 0.0_mps, [](units::velocity::meters_per_second_t sum, const SurroundingVehicleInterface* vehicle)
                                 { return sum += vehicle->GetAbsoluteVelocity().GetValue(); }) /
                 vehiclesOnLane.size();
  return average;
}

void TrafficFlow::Reset()
{
  _egoLane->Reset();
  _leftLane->Reset();
  _rightLane->Reset();
}

units::velocity::meters_per_second_t TrafficFlow::GetMeanVelocity(SurroundingLane surroundingLane)
{
  switch (surroundingLane)
  {
    case SurroundingLane::LEFT:
      return _leftLane->GetMean();
    case SurroundingLane::EGO:
      return _egoLane->GetMean();
    case SurroundingLane::RIGHT:
      return _rightLane->GetMean();
    default:
      return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }
}