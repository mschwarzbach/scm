/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneMeanVelocityInterface.h
#pragma once

//! **********************************************************
//! @brief Interface for the current mean velocity of a lane.
//! **********************************************************
#include <units.h>
class LaneMeanVelocityInterface
{
public:
  virtual ~LaneMeanVelocityInterface() = default;

  //! @brief Incorporates a new value into the current mean velocity.
  //! @param sample new sample value to incorporate
  virtual void IncorporateNewSample(units::velocity::meters_per_second_t sample) = 0;

  //! @brief Returns the current mean velocity.
  //! @return current mean
  virtual units::velocity::meters_per_second_t GetMean() = 0;

  //! @brief Resets internal data.
  //! @details Should be called when an agent transitions to a new lane or when a lane does no longer exist.
  virtual void Reset() = 0;
};