/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneMeanVelocity.h
#pragma once

#include "LaneMeanVelocityInterface.h"

//! *******************************************
//! @brief implements LaneMeanVelocityInterface
//! *******************************************
class LaneMeanVelocity : public LaneMeanVelocityInterface
{
public:
  //! @brief Sets _maxSampleSize (number of considered samples in mean) based on the given cycle time.
  //! @param cycleTime simulation cycle time or step size
  //! @param desiredVelocity chosen value to incorporate to the current 'mean velocity' if there is no vehicle present
  LaneMeanVelocity(units::time::millisecond_t cycleTime, units::velocity::meters_per_second_t desiredVelocity);

  //! @copydoc LaneMeanVelocityInterface::IncorporateNewSample
  //! @details based on https://stackoverflow.com/questions/12636613/how-to-calculate-moving-average-without-keeping-the-count-and-data-total
  void IncorporateNewSample(units::velocity::meters_per_second_t sample) override;

  //! @copydoc LaneMeanVelocityInterface::GetMean
  units::velocity::meters_per_second_t GetMean() override;

  //! @copydoc LaneMeanVelocityInterface::Reset
  //! @details resets sample size to 0 and mean to desired velocity
  void Reset() override;

private:
  int _maxSampleSize{};
  int _sampleSize{};
  units::velocity::meters_per_second_t _mean{};
  units::velocity::meters_per_second_t _desiredVelocity{};
};
