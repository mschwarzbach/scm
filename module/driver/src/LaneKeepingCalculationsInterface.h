/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "LateralAction.h"

class LaneKeepingCalculationsInterface
{
public:
  virtual ~LaneKeepingCalculationsInterface() = default;

  //! \brief Return true when Vehicle not in LaneChangeRight, not targetReached and no rightLane
  virtual bool ChangingRightAppropriate() const = 0;

  //! \brief Return true when Vehicle not in LaneChangeLeft, not targetReached and no leftLane
  virtual bool ChangingLeftAppropriate() const = 0;

  //! \brief Get state of fulfilled lateral movement, while still in lateral movement
  virtual bool IsAtEndOfLateralMovement() const = 0;

  //! \brief Check is Object on Left or Right Side
  virtual bool IsObjectInTargetSide() const = 0;

  //! \brief Check is velocity delta from Ego equal greater velocity delta side
  virtual bool EgoFasterThanDeltaSide() const = 0;
};