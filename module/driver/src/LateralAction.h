/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cstddef>

//! @brief A collection of implemented lateral action which consists of a 'State' and a 'Direction'.
//! \details Lateral actions do not coexist with each other:
//! they are triggered and persist until a new lateral action is triggered.
struct LateralAction
{
  enum class Direction
  {
    NONE,
    LEFT,
    RIGHT
  } direction{Direction::NONE};

  enum class State
  {
    LANE_KEEPING,
    INTENT_TO_CHANGE_LANES,
    LANE_CHANGING,
    PREPARING_TO_MERGE,
    URGENT_SWERVING,
    COMFORT_SWERVING,
    EXIT
  } state{State::LANE_KEEPING};

  bool operator==(const LateralAction& rhs) const
  {
    return direction == rhs.direction &&
           state == rhs.state;
  }
  bool operator!=(const LateralAction& rhs) const
  {
    return !(rhs == *this);
  }

  LateralAction() = default;
  explicit LateralAction(LateralAction::Direction direction) noexcept
      : direction(direction){};
  explicit LateralAction(LateralAction::State state) noexcept
      : state(state){};
  explicit LateralAction(LateralAction::State state, LateralAction::Direction direction) noexcept
      : direction(direction), state(state){};
};

//! @brief Hash used as third parameter in std::unordered_map<> of LateralAction's.
//! @details Note: To prevent ambiguity of different .state and .direction combinations, e.g. (2, 1) == (1, 2), the .state is shifted >> 4 before XOR'ing them to create a unique key.
struct LateralActionHash
{
  std::size_t operator()(LateralAction action) const noexcept
  {
    return (std::size_t(action.state) >> 4) ^ std::size_t(action.direction);
  }
};
