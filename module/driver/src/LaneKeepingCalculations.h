/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "LaneKeepingCalculationsInterface.h"
#include "LateralAction.h"
#include "LateralActionQuery.h"
#include "MentalModelInterface.h"

class LaneKeepingCalculations : public LaneKeepingCalculationsInterface
{
public:
  LaneKeepingCalculations(const MentalModelInterface& mentalModel, const LateralActionQuery lateralAction);

  bool ChangingRightAppropriate() const override;
  bool ChangingLeftAppropriate() const override;
  bool IsAtEndOfLateralMovement() const override;
  bool IsObjectInTargetSide() const override;
  bool EgoFasterThanDeltaSide() const override;

  private:
  std::pair<AreaOfInterest, AreaOfInterest> ChooseFrontAndRearSideAois() const;
  units::velocity::meters_per_second_t CalculateVelocityDelta(AreaOfInterest referenceAoiFront, AreaOfInterest referenceAoiRear) const;
  units::velocity::meters_per_second_t CalculateVelocityDeltaAtCollision(units::time::second_t ttc, AreaOfInterest aoi) const;

  const MentalModelInterface& _mentalModel;
  const LateralActionQuery _lateralAction;
};