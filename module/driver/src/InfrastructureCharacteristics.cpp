/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "InfrastructureCharacteristics.h"

#include <algorithm>
#include <stdexcept>
#include <string>

#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

InfrastructureCharacteristics::InfrastructureCharacteristics()
    : _trafficRuleInformation{std::make_unique<TrafficRuleInformationScmExtended>()}, _geometryInformation{std::make_unique<GeometryInformationSCM>()}
{
}

void InfrastructureCharacteristics::Initialize(units::time::millisecond_t cycleTime,
                                               TrafficRuleInformationScmExtended* trafficRuleInformation,
                                               GeometryInformationSCM* geometryInformation)
{
  if (!_isInit)
  {
    if (trafficRuleInformation)
    {
      *this->_trafficRuleInformation = *trafficRuleInformation;
    }

    if (geometryInformation)
    {
      *this->_geometryInformation = *geometryInformation;
    }

    _isInit = true;
  }

  this->_cycleTime = cycleTime;
}

void InfrastructureCharacteristics::SetLaneMarkingTypeLeftLast(scm::LaneMarking::Type laneTypeLeftLast)
{
  this->_laneTypeLeftLast = laneTypeLeftLast;
}

scm::LaneMarking::Type InfrastructureCharacteristics::GetLaneMarkingTypeLeftLast() const
{
  return _laneTypeLeftLast;
}

void InfrastructureCharacteristics::SetLaneMarkingTypeRightLast(scm::LaneMarking::Type laneTypeRightLast)
{
  this->_laneTypeRightLast = laneTypeRightLast;
}

scm::LaneMarking::Type InfrastructureCharacteristics::GetLaneMarkingTypeRightLast() const
{
  return _laneTypeRightLast;
}

void InfrastructureCharacteristics::SetTrafficRuleInformation(TrafficRuleInformationSCM* trafficRuleInformation)
{
  auto& leftMarkings{trafficRuleInformation->Close().laneMarkingsLeft};
  _laneTypeLeftLast = leftMarkings.empty() ? scm::LaneMarking::Type::None : leftMarkings.front().type;

  auto& rightMarkings{trafficRuleInformation->Close().laneMarkingsRight};
  _laneTypeRightLast = rightMarkings.empty() ? scm::LaneMarking::Type::None : rightMarkings.front().type;

  for (size_t i{0u}; i < 5u; ++i)
  {
    (*GetTrafficRuleInformation())[i].laneMarkingsLeft = (*trafficRuleInformation)[i].laneMarkingsLeft;
    (*GetTrafficRuleInformation())[i].laneMarkingsRight = (*trafficRuleInformation)[i].laneMarkingsRight;
    (*GetTrafficRuleInformation())[i].trafficSigns = (*trafficRuleInformation)[i].trafficSigns;
  }
}

void InfrastructureCharacteristics::SetGeometryInformation(GeometryInformationSCM* geometryInformation,
                                                           units::velocity::meters_per_second_t velocityEgoEstimated,
                                                           bool takeNextExit,
                                                           int laneIdEgo)
{
  // Alterations due to the agents perception happens here
  // _____________________________________________________

  if (takeNextExit)  // Are upcoming traffic signs for highway exit relevant?
  {
    // Distance to start and end of Next Exit and relative lane ID of exit lane
    UpdateDistanceToStartAndEndOfNextExit(geometryInformation, velocityEgoEstimated);
  }

  UpdateDistanceToEndOfLane(SurroundingLane::EGO, geometryInformation, velocityEgoEstimated, laneIdEgo);
  UpdateDistanceToEndOfLane(SurroundingLane::LEFT, geometryInformation, velocityEgoEstimated, laneIdEgo);
  UpdateDistanceToEndOfLane(SurroundingLane::RIGHT, geometryInformation, velocityEgoEstimated, laneIdEgo);

  // Apply manipulated ground truth data as mental data
  *this->_geometryInformation = *geometryInformation;
}

template <typename Method>
auto* InfrastructureCharacteristics::GetLaneInformation(Method method, RelativeLane relativeLane) const
{
  if (relativeLane == RelativeLane::RIGHTRIGHT)
  {
    return &std::invoke(method, this)->FarRight();
  }
  if (relativeLane == RelativeLane::RIGHT)
  {
    return &std::invoke(method, this)->Right();
  }
  if (relativeLane == RelativeLane::EGO)
  {
    return &std::invoke(method, this)->Close();
  }
  if (relativeLane == RelativeLane::LEFT)
  {
    return &std::invoke(method, this)->Left();
  }
  if (relativeLane == RelativeLane::LEFTLEFT)
  {
    return &std::invoke(method, this)->FarLeft();
  }
  throw std::runtime_error("GetLaneInformation: invalid relativeLane " + ScmCommons::to_string(relativeLane));
}

const LaneInformationGeometrySCM& InfrastructureCharacteristics::GetLaneInformationGeometry(RelativeLane relativeLane) const
{
  return *GetLaneInformation(&InfrastructureCharacteristics::GetGeometryInformation, relativeLane);
}

LaneInformationGeometrySCM* InfrastructureCharacteristics::UpdateLaneInformationGeometry(RelativeLane relativeLane)
{
  return GetLaneInformation(&InfrastructureCharacteristics::GetGeometryInformation, relativeLane);
}

const LaneInformationTrafficRulesScmExtended& InfrastructureCharacteristics::GetLaneInformationTrafficRules(RelativeLane relativeLane) const
{
  return *GetLaneInformation(&InfrastructureCharacteristics::GetTrafficRuleInformation, relativeLane);
}

LaneInformationTrafficRulesScmExtended* InfrastructureCharacteristics::UpdateLaneInformationTrafficRules(RelativeLane relativeLane)
{
  return GetLaneInformation(&InfrastructureCharacteristics::GetTrafficRuleInformation, relativeLane);
}

TrafficRuleInformationScmExtended* InfrastructureCharacteristics::GetTrafficRuleInformation() const
{
  return _trafficRuleInformation.get();
}

GeometryInformationSCM* InfrastructureCharacteristics::GetGeometryInformation() const
{
  return _geometryInformation.get();
}

TrafficRuleInformationScmExtended* InfrastructureCharacteristics::UpdateTrafficRuleInformation()
{
  return _trafficRuleInformation.get();
}

GeometryInformationSCM* InfrastructureCharacteristics::UpdateGeometryInformation()
{
  return _geometryInformation.get();
}

bool InfrastructureCharacteristics::TrafficSignRelevantToEndOfLane(const scm::CommonTrafficSign::Entity& trafficSign, int laneIdEgo, SurroundingLane lane, int numberOfLanes)
{
  if (lane == SurroundingLane::EGO &&
      // Agent is on ending right most lane, therefore lane ego ends
      ((trafficSign.type == scm::CommonTrafficSign::Type::AnnounceRightLaneEnd && numberOfLanes == -laneIdEgo) ||
       // or Agent is on ending left most lane, therefore lane ego ends
       (trafficSign.type == scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd && laneIdEgo == -1)))
  {
    return true;
  }

  if (lane == SurroundingLane::RIGHT &&
      // Agents neighbouring right lane can only end when agent is on second right most lane
      (trafficSign.type == scm::CommonTrafficSign::Type::AnnounceRightLaneEnd &&
       numberOfLanes - 1 == -laneIdEgo))
  {
    return true;
  }

  if (lane == SurroundingLane::LEFT &&
      // Agents neighbouring left lane can only end when agent is on second left most lane
      (trafficSign.type == scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd && laneIdEgo == -2))
  {
    return true;
  }

  return false;
}

units::length::meter_t InfrastructureCharacteristics::DeriveDistanceToEndOfLaneFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns, int laneIdEgo, SurroundingLane lane, int numberOfLanes)
{
  units::length::meter_t distanceToTrafficSignIndicatingEndOfLane{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t distanceToEndOfLaneDerivedFromTrafficSign{ScmDefinitions::DEFAULT_VALUE};
  bool isNewSign{false};

  for (auto currentSign : trafficSigns)
  {
    const bool signRelatesToLane{TrafficSignRelevantToEndOfLane(currentSign, laneIdEgo, lane, numberOfLanes)};

    // Get the farthest sign for lane end ahead
    if (signRelatesToLane &&
        currentSign.relativeDistance >= 0.0_m &&
        currentSign.relativeDistance > distanceToTrafficSignIndicatingEndOfLane)
    {
      distanceToTrafficSignIndicatingEndOfLane = currentSign.relativeDistance;  // Set distance to traffic sign from provided attribute relativeDistance

      // Check for additional sign providing information on distance to end of lane
      const auto distanceIndication = std::find_if(begin(currentSign.supplementarySigns), end(currentSign.supplementarySigns), [](const auto& suppSign)
                                                   { return suppSign.type == scm::CommonTrafficSign::Type::DistanceIndication; });

      if (distanceIndication != end(currentSign.supplementarySigns))
      {
        const auto valueSupplementarySign(distanceIndication->value);
        // Check if information is new for driver and reset subsequent extrapolation process if applicable
        if (valueSupplementarySign != _valuesLastTrafficSignEndOfLane[lane])
        {
          _valuesLastTrafficSignEndOfLane[lane] = valueSupplementarySign;
          distanceToEndOfLaneDerivedFromTrafficSign = distanceToTrafficSignIndicatingEndOfLane + static_cast<units::length::meter_t>(valueSupplementarySign);
          isNewSign = true;
        }
      }
    }
  }

  if (isNewSign)
  {
    return distanceToEndOfLaneDerivedFromTrafficSign;
  }

  return units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
}

void InfrastructureCharacteristics::UpdateDistanceToEndOfLane(SurroundingLane lane,
                                                              GeometryInformationSCM* geometryInformation,
                                                              units::velocity::meters_per_second_t velocityEgoEstimated,
                                                              int laneIdEgo)
{
  const auto distanceToEndOfLaneGroundTruth{geometryInformation->FromLane(lane).distanceToEndOfLane};
  if (!units::math::isinf(distanceToEndOfLaneGroundTruth))  // Neglect traffic signs in case the end of the lane can already be seen by the driver
  {
    _valuesLastTrafficSignEndOfLane[lane] = -999.0;  // Reset (possibly) remembered traffic sign for next situation
    return;
  }

  // Check all traffic signs for relation to end of lane
  const std::vector<scm::CommonTrafficSign::Entity> laneSigns{GetTrafficRuleInformation()->Close().trafficSigns};

  auto distanceToEndOfLaneDerivedFromTrafficSign{DeriveDistanceToEndOfLaneFromTrafficSigns(laneSigns, laneIdEgo, lane, geometryInformation->numberOfLanes)};

  // Apply knowledge
  const auto distanceToEndOfLaneLast{this->_geometryInformation->FromLane(lane).distanceToEndOfLane};
  if (distanceToEndOfLaneDerivedFromTrafficSign != units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
  {
    geometryInformation->FromLane(lane).distanceToEndOfLane = distanceToEndOfLaneDerivedFromTrafficSign;
  }
  // Extrapolate distance to end of lane from earlier traffic sign information
  else if (distanceToEndOfLaneLast > 0._m && !units::math::isinf(distanceToEndOfLaneLast))
  {
    const auto distanceToEndOfLaneExtrapolated{distanceToEndOfLaneLast - units::math::fabs(velocityEgoEstimated) * _cycleTime};

    if (distanceToEndOfLaneExtrapolated > 0.0_m)
    {
      geometryInformation->FromLane(lane).distanceToEndOfLane = static_cast<units::length::meter_t>(distanceToEndOfLaneExtrapolated);
    }
  }
}

void InfrastructureCharacteristics::UpdateDistanceToStartAndEndOfNextExit(GeometryInformationSCM* geometryInformation,
                                                                          units::velocity::meters_per_second_t velocityEgoEstimated)
{
  auto distanceToStartOfExitGroundTruth = geometryInformation->distanceToStartOfNextExit;
  auto distanceToEndOfExitGroundTruth = geometryInformation->distanceToEndOfNextExit;

  bool resetJunction{distanceToEndOfExitGroundTruth == units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) && _distanceToEndOfNextExistGroundTruthLast != units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)};

  auto distanceToStartOfExitLast = resetJunction ? units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) : this->_geometryInformation->distanceToStartOfNextExit;
  auto distanceToEndOfExitLast = resetJunction ? units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) : this->_geometryInformation->distanceToEndOfNextExit;

  units::length::meter_t lengthOfExitForEstimations{250._m};

  auto distanceToEndOfExitNew = CalculateDistanceToEndOfExitNew(distanceToEndOfExitGroundTruth, distanceToEndOfExitLast, velocityEgoEstimated);
  auto distanceToStartOfExitNew = CalculateDistanceToStartOfExitNew(distanceToStartOfExitGroundTruth, distanceToStartOfExitLast, velocityEgoEstimated);

  // Cross-check information regarding start and end of exit and apply estimation about exit length if necessary
  if (distanceToEndOfExitNew > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) && distanceToStartOfExitNew <= units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
  {
    distanceToStartOfExitNew = distanceToEndOfExitNew - lengthOfExitForEstimations;
  }
  else if (distanceToStartOfExitNew > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) && distanceToEndOfExitNew <= units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
  {
    distanceToEndOfExitNew = distanceToStartOfExitNew + lengthOfExitForEstimations;
  }

  geometryInformation->distanceToEndOfNextExit = distanceToEndOfExitNew;
  geometryInformation->distanceToStartOfNextExit = distanceToStartOfExitNew;

  // Estimate numbers of lanes to change to reach exit lane
  if (geometryInformation->relativeLaneIdForJunctionIngoing.empty() && (!IsSeenStartOfExit(distanceToStartOfExitGroundTruth) && !IsSeenEndOfExit(distanceToEndOfExitGroundTruth))  // only estimate if information is not yet visually available
      && (distanceToStartOfExitNew > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) || distanceToEndOfExitNew > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)))     // only estimate if exit was actually announced
  {
    geometryInformation->relativeLaneIdForJunctionIngoing = {EstimateRelativeLaneIdOfExitLane(geometryInformation)};
  }

  _distanceToEndOfNextExistGroundTruthLast = distanceToEndOfExitGroundTruth;
}

units::length::meter_t InfrastructureCharacteristics::CalculateDistanceToStartOfExitNew(units::length::meter_t distanceToStartOfExitGroundTruth, units::length::meter_t distanceToStartOfExitLast, units::velocity::meters_per_second_t velocityEgoEstimated)
{
  auto distanceToStartOfExitNew{units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)};

  // Neglect traffic signs regarding start of next exit, if it can already be seen by the driver
  if (IsSeenStartOfExit(distanceToStartOfExitGroundTruth))
  {
    distanceToStartOfExitNew = distanceToStartOfExitGroundTruth;
    _valueLastTrafficSignIndicatingStartOfExit = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);  // Reset (possibly) remembered traffic sign for next situation
  }
  // Evaluate traffic signs regarding distance to end of next exit or already existing information
  else
  {
    // Check all traffic signs for HighWayExitPoles
    std::vector<scm::CommonTrafficSign::Entity> laneSigns{GetTrafficRuleInformation()->Close().trafficSigns};
    auto distanceToStartOfExitDerivedFromTrafficSign = DeriveDistanceToStartOfExitFromTrafficSigns(laneSigns);
    const auto distanceToStartOfExitExtrapolated = ExtrapolateDistanceToExitFromEarlierTrafficSignInformation(distanceToStartOfExitLast, velocityEgoEstimated);

    // Apply knowledge
    if (distanceToStartOfExitDerivedFromTrafficSign != units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
    {
      distanceToStartOfExitNew = distanceToStartOfExitDerivedFromTrafficSign;
    }
    else if (distanceToStartOfExitLast > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))  // Extrapolate distance to start of exit from earlier traffic sign information
    {
      distanceToStartOfExitNew = distanceToStartOfExitExtrapolated;
    }
  }
  return distanceToStartOfExitNew;
}

units::length::meter_t InfrastructureCharacteristics::CalculateDistanceToEndOfExitNew(units::length::meter_t distanceToEndOfExitGroundTruth, units::length::meter_t distanceToEndOfExitLast, units::velocity::meters_per_second_t velocityEgoEstimated)
{
  auto distanceToEndOfExitNew{units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)};

  // Neglect traffic signs regarding end of next exit, if it can already be seen by the driver
  if (IsSeenEndOfExit(distanceToEndOfExitGroundTruth))
  {
    distanceToEndOfExitNew = distanceToEndOfExitGroundTruth;
    _valueLastTrafficSignIndicatingEndOfExit = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);  // Reset (possibly) remembered traffic sign for next situation
  }
  // Evaluate traffic signs regarding distance to end of next exit or already existing information
  else
  {
    // Check all traffic signs for HighWayExit
    std::vector<scm::CommonTrafficSign::Entity> laneSigns{GetTrafficRuleInformation()->Close().trafficSigns};
    auto distanceToEndOfExitDerivedFromTrafficSign = DeriveDistanceToEndOfExitFromTrafficSigns(laneSigns);
    const auto distanceToEndOfExitExtrapolated = ExtrapolateDistanceToExitFromEarlierTrafficSignInformation(distanceToEndOfExitLast, velocityEgoEstimated);

    // Apply knowledge
    if (distanceToEndOfExitDerivedFromTrafficSign != units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
    {
      distanceToEndOfExitNew = distanceToEndOfExitDerivedFromTrafficSign;
    }
    else if (distanceToEndOfExitLast > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE) && distanceToEndOfExitExtrapolated > 0._m)  // Extrapolate distance to end of exit from earlier traffic sign information
    {
      distanceToEndOfExitNew = distanceToEndOfExitExtrapolated;
    }
  }
  return distanceToEndOfExitNew;
}

units::length::meter_t InfrastructureCharacteristics::DeriveDistanceToEndOfExitFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns)
{
  units::length::meter_t distanceToTrafficSignIndicatingEndOfExit{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t valueTrafficSign{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t distanceToEndOfExitDerivedFromTrafficSign{ScmDefinitions::DEFAULT_VALUE};
  bool isNewSign{false};

  for (auto currentSign : trafficSigns)
  {
    if (currentSign.type == scm::CommonTrafficSign::Type::AnnounceHighwayExit && currentSign.relativeDistance >= 0.0_m)
    {
      distanceToTrafficSignIndicatingEndOfExit = currentSign.relativeDistance;  // Set distance to traffic sign from provided attribute relativeDistance
      valueTrafficSign = static_cast<units::length::meter_t>(currentSign.value);
    }
    else
    {
      continue;  // Skip sign if it doesn't fit
    }

    isNewSign = valueTrafficSign != _valueLastTrafficSignIndicatingEndOfExit;

    if (isNewSign)
    {
      _valueLastTrafficSignIndicatingEndOfExit = valueTrafficSign;
      distanceToEndOfExitDerivedFromTrafficSign = distanceToTrafficSignIndicatingEndOfExit + valueTrafficSign;
    }
  }
  return distanceToEndOfExitDerivedFromTrafficSign;
}

units::length::meter_t InfrastructureCharacteristics::DeriveDistanceToStartOfExitFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns)
{
  units::length::meter_t distanceToTrafficSignIndicatingStartOfExit{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t valueTrafficSign{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t distanceToStartOfExitDerivedFromTrafficSign{ScmDefinitions::DEFAULT_VALUE};
  bool isNewSign{false};

  for (auto currentSign : trafficSigns)
  {
    // Get the farthest sign for start of exit
    if (currentSign.type == scm::CommonTrafficSign::Type::HighwayExitPole && currentSign.relativeDistance >= 0.0_m &&
        currentSign.relativeDistance > distanceToTrafficSignIndicatingStartOfExit)
    {
      distanceToTrafficSignIndicatingStartOfExit = currentSign.relativeDistance;  // Set distance to traffic sign from provided attribute relativeDistance
      valueTrafficSign = static_cast<units::length::meter_t>(currentSign.value);
    }
    else
    {
      continue;  // Skip sign if it doesn't fit or a sign was already evaluated, which is farther away
    }

    isNewSign = valueTrafficSign != _valueLastTrafficSignIndicatingStartOfExit;

    if (isNewSign)
    {
      _valueLastTrafficSignIndicatingStartOfExit = valueTrafficSign;
      distanceToStartOfExitDerivedFromTrafficSign = distanceToTrafficSignIndicatingStartOfExit + valueTrafficSign;
    }
  }
  return distanceToStartOfExitDerivedFromTrafficSign;
}

bool InfrastructureCharacteristics::IsSeenEndOfExit(units::length::meter_t distanceToEndOfExitGroundTruth)
{
  return distanceToEndOfExitGroundTruth > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
}

bool InfrastructureCharacteristics::IsSeenStartOfExit(units::length::meter_t distanceToStartOfExitGroundTruth)
{
  return distanceToStartOfExitGroundTruth > units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
}

units::length::meter_t InfrastructureCharacteristics::ExtrapolateDistanceToExitFromEarlierTrafficSignInformation(units::length::meter_t distanceToExitLast, units::velocity::meters_per_second_t velocityEgoEstimated) const
{
  return distanceToExitLast - units::math::fabs(velocityEgoEstimated) * _cycleTime;
}

int InfrastructureCharacteristics::EstimateRelativeLaneIdOfExitLane(GeometryInformationSCM* geometryInformation)
{
  if (geometryInformation->Close().laneType == scm::LaneType::Exit || geometryInformation->Close().laneType == scm::LaneType::OffRamp)  // already on exit lane
  {
    return 0;
  }
  else if (geometryInformation->Right().laneType != scm::LaneType::Driving)  // right lane is no driving lane -> stop lane or exit lane
  {
    return -1;
  }
  else if (geometryInformation->FarRight().laneType != scm::LaneType::Driving)  // right-right lane is no driving lane -> stop lane or exit lane
  {
    return -2;
  }
  else  // cannot look farther than two lanes to the right -> exit lane is at least three lanes to the right
  {
    return -3;
  }
}

bool InfrastructureCharacteristics::GetLaneExistence(RelativeLane relativeLane, bool isEmergency) const
{
  const auto geometryInformation = GetLaneInformationGeometry(relativeLane);

  if (isEmergency)
  {
    return geometryInformation.exists;
  }

  return (geometryInformation.exists &&
          (geometryInformation.laneType == scm::LaneType::Driving ||
           geometryInformation.laneType == scm::LaneType::Entry ||
           geometryInformation.laneType == scm::LaneType::Exit ||
           geometryInformation.laneType == scm::LaneType::OffRamp ||
           geometryInformation.laneType == scm::LaneType::OnRamp));
}

bool InfrastructureCharacteristics::CheckLaneExistence(AreaOfInterest aoi, bool isEmergency) const
{
  if (!ScmCommons::IsSurroundingAreaOfInterest(aoi))
  {
    return false;
  }

  const auto relativeLane{AreaOfInterest2RelativeLane.at(aoi)};
  return GetLaneExistence(relativeLane, isEmergency);
}
