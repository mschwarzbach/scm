/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Extrapolation.h"

#include <OsiQueryLibrary/osiql.h>

#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "ScmCommons.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

using namespace units::literals;
Extrapolation::Extrapolation(MentalModelInterface& mentalModel, OwnVehicleRoutePose& ownVehicleRoutePose)
    : _mentalModel{mentalModel}, _ownVehicleRoutePose(ownVehicleRoutePose)
{
  _cycleTimeInS = _mentalModel.GetCycleTime();
}

void Extrapolation::Extrapolate(ObjectInformationScmExtended* objectInformation, AreaOfInterest aoi) const
{
  ExtrapolateSurroundingVehicleDataLongitudinal(objectInformation, aoi);
  ExtrapolateSurroundingVehicleDataLateral(objectInformation, aoi);
  ProjectSurroundingVehicle(objectInformation);
}

void Extrapolation::ExtrapolateLongitudinalAcceleration(ObjectInformationScmExtended* surroundingObject) const
{
  if (surroundingObject->accelerationChange == -999._mps_sq || surroundingObject->accelerationChange == 0._mps_sq ||
      surroundingObject->acceleration == -999._mps_sq)
  {
    return;  // No extrapolation possible or necessary
  }

  const auto comfortDeceleration{-_mentalModel.GetComfortLongitudinalDeceleration()};
  const auto comfortAcceleration{_mentalModel.GetComfortLongitudinalAcceleration()};
  const bool outsideComfort{surroundingObject->acceleration > comfortAcceleration + units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::EPSILON) ||
                            surroundingObject->acceleration < comfortDeceleration - units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::EPSILON)};  // Include epsilon to compensate for small changes in the comfort values

  const auto accelerationNew = surroundingObject->accelerationLastStep + surroundingObject->accelerationChange;
  const bool didSignChange{accelerationNew.value() * surroundingObject->acceleration < 0.0_mps_sq};

  if (didSignChange)
  {
    surroundingObject->acceleration = 0.0_mps_sq;
    surroundingObject->accelerationChange = 0._mps_sq;
    return;
  }

  surroundingObject->acceleration = accelerationNew;
  surroundingObject->accelerationChange *= .85;

  // Restrict extrapolated acceleration with ego comfort and max acceleration
  if (!outsideComfort)
  {
    surroundingObject->acceleration = units::math::max(surroundingObject->acceleration, comfortDeceleration);  // Lower cap at comfort deceleration of ego agent
    surroundingObject->acceleration = units::math::min(surroundingObject->acceleration, comfortAcceleration);  // Upper cap at comfort acceleration of ego agent
  }
  else
  {
    const auto maxDeceleration{-_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration};
    const auto maxAcceleration{_mentalModel.GetDriverParameters().maximumLongitudinalAcceleration};
    surroundingObject->acceleration = units::math::max(surroundingObject->acceleration, maxDeceleration);  // Lower cap at maximum deceleration of ego agent
    surroundingObject->acceleration = units::math::min(surroundingObject->acceleration, maxAcceleration);  // Upper cap at maximum acceleration of ego agent
  }
}

void Extrapolation::ExtrapolateSurroundingVehicleDataLongitudinal(ObjectInformationScmExtended* surroundingObject, AreaOfInterest aoi) const
{
  const int sign{
      LaneQueryHelper::IsRearArea(aoi)   ? -1
      : LaneQueryHelper::IsSideArea(aoi) ? 0
                                         : 1};

  ExtrapolateLongitudinalAcceleration(surroundingObject);
  auto longitudinalVelocityDelta{sign * (surroundingObject->longitudinalVelocity - _mentalModel.GetLongitudinalVelocityEgo())};
  
  surroundingObject->absoluteVelocity = units::math::max(surroundingObject->absoluteVelocity + _cycleTimeInS * surroundingObject->acceleration, 0._mps);
  surroundingObject->longitudinalVelocity = units::math::max(surroundingObject->longitudinalVelocity + _cycleTimeInS * surroundingObject->acceleration, 0._mps);
  surroundingObject->relativeLongitudinalDistance = surroundingObject->relativeLongitudinalDistance + longitudinalVelocityDelta * units::time::second_t(_cycleTimeInS);
  surroundingObject->longitudinalDistanceFrontBumperToRearAxle += sign * (longitudinalVelocityDelta * units::time::second_t(_cycleTimeInS));

  if (surroundingObject->longitudinalVelocity == 0.0_mps && surroundingObject->acceleration < 0._mps_sq)
  {
    surroundingObject->acceleration = 0._mps_sq;
    surroundingObject->accelerationChange = 0._mps_sq;
  }

  surroundingObject->longitudinalObstruction = ExtrapolateLongitudinalObstruction(aoi, surroundingObject->longitudinalObstruction, surroundingObject->longitudinalVelocity);

  surroundingObject->gap = std::fabs(sign) * ScmCommons::CalculateNetGap(_mentalModel.GetLongitudinalVelocityEgo(),
                                                                         surroundingObject->longitudinalVelocity,
                                                                         surroundingObject->relativeLongitudinalDistance,
                                                                         aoi);
  surroundingObject->gapDot = std::fabs(sign) * ScmCommons::CalculateNetGapDot(_mentalModel.GetLongitudinalVelocityEgo(),
                                                                               surroundingObject->longitudinalVelocity,
                                                                               _mentalModel.GetAcceleration(),
                                                                               surroundingObject->acceleration,
                                                                               surroundingObject->relativeLongitudinalDistance,
                                                                               longitudinalVelocityDelta,
                                                                               aoi);
  if (LaneQueryHelper::IsSideArea(aoi))
  {
    surroundingObject->ttc = ScmDefinitions::TTC_LIMIT;
    surroundingObject->tauDot = 0.;
  }
  else if (units::math::fabs(surroundingObject->ttc) >= ScmDefinitions::TTC_LIMIT)
  {
    surroundingObject->tauDot = 0.;
  }
  else
  {
    surroundingObject->ttc = ScmCommons::CalculateTTC(longitudinalVelocityDelta,
                                                      surroundingObject->relativeLongitudinalDistance,
                                                      aoi);

    const bool canPerceiveAcceleration{surroundingObject->relativeLongitudinalDistance < ACCELERATION_SENSING_DISTANCE};
    const auto egoAcceleration{_mentalModel.GetAcceleration()};
    const auto accelerationDelta{LaneQueryHelper::IsRearArea(aoi) ? egoAcceleration - surroundingObject->acceleration : surroundingObject->acceleration - egoAcceleration};
    surroundingObject->tauDot = ScmCommons::CalculateTauDot(longitudinalVelocityDelta,
                                                            surroundingObject->relativeLongitudinalDistance,
                                                            canPerceiveAcceleration ? accelerationDelta : 0_mps_sq,
                                                            aoi);
  }
}

void Extrapolation::ExtrapolateSurroundingVehicleDataLateral(ObjectInformationScmExtended* surroundingObject, AreaOfInterest aoi) const
{
  if (surroundingObject->exist)
  {
    double sign{0.};
    auto laneWidthEgo{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width};
    const auto laneWidthLeft{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Left().width};
    const auto laneWidthRight{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Right().width};
    const auto laneWidthLeftLeft{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->FarLeft().width};
    const auto laneWidthRightRight{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->FarRight().width};
    auto laneWidthSide{laneWidthEgo};
    if (LaneQueryHelper::IsRightLane(aoi))
    {
      sign = -1.;
      laneWidthSide = laneWidthRight;
    }
    else if (LaneQueryHelper::IsLeftLane(aoi))
    {
      sign = 1.;
      laneWidthSide = laneWidthLeft;
    }
    else if (LaneQueryHelper::IsLeftLeftLane(aoi))
    {
      sign = 1.;
      laneWidthSide = laneWidthLeftLeft;
      laneWidthEgo = laneWidthLeft;
    }
    else if (LaneQueryHelper::IsRightRightLane(aoi))
    {
      sign = -1.;
      laneWidthSide = laneWidthRightRight;
      laneWidthEgo = laneWidthRight;
    }

    // LateralVelocity not extrapolated at the moment
    // Todo Extrapolate once lateral acceleration is implemented or certain calculation is possible with absolute and longitudinal veocity
    surroundingObject->lateralVelocity = surroundingObject->lateralVelocity /* *dt */;

    bool extrapolatedEndOfLaneChange = false;
    if (ScmCommons::sgn(surroundingObject->lateralPositionInLane) !=
        ScmCommons::sgn(surroundingObject->lateralPositionInLane + surroundingObject->lateralVelocity * _cycleTimeInS))
    {
      extrapolatedEndOfLaneChange = true;
    }

    if (extrapolatedEndOfLaneChange)
    {
      const auto lateralPositionInLaneLast = surroundingObject->lateralPositionInLane;
      surroundingObject->lateralVelocity = 0._mps;
      surroundingObject->lateralPositionInLane = 0._m;
      surroundingObject->distanceToLaneBoundaryLeft += lateralPositionInLaneLast;
      surroundingObject->distanceToLaneBoundaryRight -= lateralPositionInLaneLast;
    }

    if (surroundingObject->lateralVelocity != 0._mps)
    {
      surroundingObject->lateralPositionInLane += surroundingObject->lateralVelocity * _cycleTimeInS;
      surroundingObject->distanceToLaneBoundaryLeft -= surroundingObject->lateralVelocity * _cycleTimeInS;
      surroundingObject->distanceToLaneBoundaryRight += surroundingObject->lateralVelocity * _cycleTimeInS;
    }

    const auto lateralVelocityDelta = surroundingObject->lateralVelocity - _mentalModel.GetLateralVelocity();
    surroundingObject->relativeLateralDistance += lateralVelocityDelta * _cycleTimeInS;
    surroundingObject->obstruction.left += lateralVelocityDelta * _cycleTimeInS;
    surroundingObject->obstruction.right -= lateralVelocityDelta * _cycleTimeInS;
    surroundingObject->obstruction = ObstructionScm(surroundingObject->obstruction.left,
                                                    surroundingObject->obstruction.right,
                                                    surroundingObject->obstruction.mainLaneLocator);  // Note: Extrapolation of main lane locator not yet implemented
  }
}

ObstructionLongitudinal Extrapolation::ExtrapolateLongitudinalObstruction(AreaOfInterest aoi, const ObstructionLongitudinal& oldObstruction, units::velocity::meters_per_second_t agentVelocity) const
{
  auto velocityDelta{_mentalModel.GetLongitudinalVelocityEgo() - agentVelocity};
  auto distanceDelta{velocityDelta * _cycleTimeInS};
  const auto obstructionFront = oldObstruction.front - distanceDelta;
  const auto obstructionRear = oldObstruction.rear + distanceDelta;
  const auto obstructionMainLocator = oldObstruction.mainLaneLocator - distanceDelta;

  return ObstructionLongitudinal(obstructionFront, obstructionRear, obstructionMainLocator);
}

void Extrapolation::ProjectSurroundingVehicle(ObjectInformationScmExtended* surroundingObject) const
{
  auto projection = _ownVehicleRoutePose.route->ProjectPoint(_ownVehicleRoutePose.pose, surroundingObject->longitudinalDistanceFrontBumperToRearAxle.value());

  if (projection.has_value())
  {
    projection->longitude += surroundingObject->relativeLateralDistance.value();
    auto distanceXY = projection->GetXY() - _ownVehicleRoutePose.pose->GetXY();
    surroundingObject->relativeX = units::make_unit<units::length::meter_t>(distanceXY.x);
    surroundingObject->relativeY = units::make_unit<units::length::meter_t>(distanceXY.y);
    return;
  }

  surroundingObject->relativeX = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);
  surroundingObject->relativeY = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);
}