/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <algorithm>

#include "LateralAction.h"

//! @brief LateralActionQuery is a utility class for a 'LateralAction'
//! \details LateralActionQuery provides a collection of convenience methods to check for the state, direction or both of a LateralAction.
class LateralActionQuery
{
private:
  LateralAction _lateralAction;

public:
  explicit LateralActionQuery(LateralAction lateralAction)
      : _lateralAction{std::move(lateralAction)}
  {
  }

  LateralAction operator=(const LateralActionQuery& me)
  {
    return me._lateralAction;
  }

  LateralActionQuery operator=(const LateralAction& lateralAction)
  {
    _lateralAction = lateralAction;
    return *this;
  }

  constexpr bool IsIntentActive() const noexcept
  {
    return _lateralAction.state == LateralAction::State::INTENT_TO_CHANGE_LANES;
  }

  constexpr bool IsPreparingToMerge() const noexcept
  {
    return _lateralAction.state == LateralAction::State::PREPARING_TO_MERGE;
  }

  constexpr bool IsPreparingToMergeLeft() const noexcept
  {
    return IsPreparingToMerge() && _lateralAction.direction == LateralAction::Direction::LEFT;
  }

  constexpr bool IsPreparingToMergeRight() const noexcept
  {
    return IsPreparingToMerge() && _lateralAction.direction == LateralAction::Direction::RIGHT;
  }

  constexpr bool IsSwerving() const noexcept
  {
    return _lateralAction.state == LateralAction::State::URGENT_SWERVING ||
           _lateralAction.state == LateralAction::State::COMFORT_SWERVING;
  }

  constexpr bool IsSwervingLeft() const noexcept
  {
    return IsSwerving() && _lateralAction.direction == LateralAction::Direction::LEFT;
  }

  constexpr bool IsSwervingRight() const noexcept
  {
    return IsSwerving() && _lateralAction.direction == LateralAction::Direction::RIGHT;
  }

  constexpr bool IsSwervingUrgent() const noexcept
  {
    return _lateralAction.state == LateralAction::State::URGENT_SWERVING;
  }

  constexpr bool IsSwervingComfort() const noexcept
  {
    return _lateralAction.state == LateralAction::State::COMFORT_SWERVING;
  }

  constexpr bool IsLaneChanging() const noexcept
  {
    return _lateralAction.state == LateralAction::State::LANE_CHANGING;
  }
  constexpr bool IsLaneChangingRight() const noexcept
  {
    return IsLaneChanging() && _lateralAction.direction == LateralAction::Direction::RIGHT;
  }

  constexpr bool IsLaneChangingLeft() const noexcept
  {
    return IsLaneChanging() && _lateralAction.direction == LateralAction::Direction::LEFT;
  }

  constexpr bool IsLaneChangingOrSwerving() const noexcept
  {
    return IsLaneChanging() || IsSwerving();
  }

  constexpr bool IsLaneKeeping() const noexcept
  {
    return _lateralAction.state == LateralAction::State::LANE_KEEPING;
  }
  constexpr bool IsLaneKeepingRightActions() const noexcept
  {
    return IsLaneKeeping() ||
           ((_lateralAction.state == LateralAction::State::INTENT_TO_CHANGE_LANES ||
             _lateralAction.state == LateralAction::State::PREPARING_TO_MERGE) &&
            _lateralAction.direction == LateralAction::Direction::RIGHT);
  }
  constexpr bool IsLaneKeepingLeftActions() const noexcept
  {
    return IsLaneKeeping() ||
           ((_lateralAction.state == LateralAction::State::INTENT_TO_CHANGE_LANES ||
             _lateralAction.state == LateralAction::State::PREPARING_TO_MERGE) &&
            _lateralAction.direction == LateralAction::Direction::LEFT);
  }
  constexpr bool IsRelevantForIndicator() const noexcept
  {
    return IsIntentActive() || IsPreparingToMerge() || IsLaneChanging();
  }
  constexpr bool IsRelevantForIndicatorLeft() const noexcept
  {
    return IsRelevantForIndicator() && _lateralAction.direction == LateralAction::Direction::LEFT;
  }
  constexpr bool IsRelevantForIndicatorRight() const noexcept
  {
    return IsRelevantForIndicator() && _lateralAction.direction == LateralAction::Direction::RIGHT;
  }
  constexpr bool IsLeavingEgoLane() const noexcept
  {
    return IsLaneChanging() || IsSwervingUrgent();
  }
  constexpr LateralAction::Direction GetDirection() const noexcept
  {
    return _lateralAction.direction;
  }
};
