/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  LaneChangeParameters.h

#pragma once

#include <vector>

struct LaneChangeParameters
{
  struct SteeringAction
  {
    double curvatureTransition1{-999.};                                                     // kappa_end (Rising Clothoid -> Curve Segment)
    double curvatureTransition2{-999.};                                                     // kappa_end (Curve Segment -> Falling Clothoid)
    double curvatureTransition3{-999.};                                                     // kappa_end (Falling Clothoid -> Straight Segement or Second steering action)
    double headingTransition1{-999.};                                                       // psi_end (Rising Clothoid -> Curve Segment)
    double headingTransition2{-999.};                                                       // psi_end (Curve Segment -> Falling Clothoid)
    double headingTransition3{-999.};                                                       // psi_end (Falling Clothoid -> Straight Segement or Second steering action)
    std::pair<std::vector<double>, std::vector<double>> trajectoryRisingClothoid{{}, {}};   // x_s , y_s
    std::pair<std::vector<double>, std::vector<double>> trajectoryCurveSection{{}, {}};     // x_s , y_s
    std::pair<std::vector<double>, std::vector<double>> trajectoryFallingClothoid{{}, {}};  // x_s , y_s
    std::vector<double> headingRisingClothoid{};
    std::vector<double> headingCurveSection{};
    std::vector<double> headingFallingClothoid{};
    std::vector<double> curvatureRisingClothoid{};
    std::vector<double> curvatureCurveSection{};
    std::vector<double> curvatureFallingClothoid{};
    std::vector<double> sPathRisingClothoid{};
    std::vector<double> sPathCurveSection{};
    std::vector<double> sPathFallingClothoid{};
  };

  // Vehicle parameters
  double curvatureStart{-999.};  // kappa_0
  double headingStart{-999.};    // psi_0
  double steeringRatio{-999.};
  double velocityEgoAbsolute{-999.};
  double wheelbase{-999.};  // l

  // Driver parameters
  double accelerationLateralComfort{-999.};
  double accelerationLateralMax{-999.};
  double angleVelocitySteeringWheelComfort{-999.};
  double angleVelocitySteeringWheelMax{-999.};
  double timeHeadwayFreeLaneChange{-999.};
  double headingEnd{-999.};  // psi_2

  // Obstacle parameters
  double relativeNetDistance{-999.};  // delta_x
  double timeHeadway{-999.};
  double timeToCollision{-999.};
  double velocityObject{-999.};

  // Planning parameters
  double headingDeltaStart{-999.};  // Delta_psi_start
  double headingDeltaEnd{-999.};    // Delta_psi_end
  double headingManeuver{-999.};    // psi_1
  bool isFreeLaneChange{false};
  bool isComfortLaneChange{false};
  double lengthLaneChange{-999.};
  double lengthSteeringManeuver{-999.};
  SteeringAction firstSteeringActions;
  SteeringAction secondSteeringActions;
  double widthLaneChange{-999.};
  double widthSteeringManeuver{-999.};
  bool isSideAoi{false};
};
