/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  MentalCalculations.h

#pragma once

#include <memory>

#include "AccelerationCalculations/AccelerationCalculationsQueryInterface.h"
#include "ActionManager.h"
#include "LaneChangeBehaviorInterface.h"
#include "LaneChangeDimension.h"
#include "LongitudinalCalculations/LongitudinalCalculations.h"
#include "LongitudinalCalculations/LongitudinalCalculationsInterface.h"
#include "LongitudinalCalculations/LongitudinalPartCalculationsInterface.h"
#include "MentalCalculationsInterface.h"
#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"

class MentalModelInterface;
class FeatureExtractorInterface;
class ActionManager;

//! @brief receives informations from module InternelData and estimates the current situation based on the high cognitive process
//! @details This class receives informations from MicroscopicCharacteristics and estimates the current situatiation
//! based on acquired informations during high cognitive process
class MentalCalculations : public MentalCalculationsInterface
{
  friend class MentalModel;

public:
  //! @brief Constructs MentalCalculations
  //! @param featureExtractor Access to FeatureExtractor
  //! @param formRescueLane traffic rule relevant for MentalCalculations
  //! @param openSpeedLimit traffic rule relevant for MentalCalculations
  //! @param rightHandTraffic traffic rule relevant for MentalCalculations
  //! @param keepToOuterLane traffic rule relevant for MentalCalculations
  MentalCalculations(const FeatureExtractorInterface& featureExtractor,
                     bool formRescueLane = true,
                     units::velocity::meters_per_second_t openSpeedLimit = ScmDefinitions::INF_VELOCITY,
                     bool rightHandTraffic = true,
                     bool keepToOuterLane = true);

  void Initialize(MentalModelInterface* mentalModel, SurroundingVehicleQueryFactoryInterface* queryFactory, const LongitudinalCalculationsInterface* longitudinalCalculation) override;
  units::time::second_t GetTimeHeadwayBetweenTwoAreasOfInterest(AreaOfInterest aoi_first, AreaOfInterest aoi_second) const override;
  double GetTauDotEq(AreaOfInterest aoi) const override;
  units::velocity::meters_per_second_t CalculateDeltaVelocityWish() const override;
  units::length::meter_t CalculateVLegalInfluencigDistance(units::velocity::meters_per_second_t vEgo, units::velocity::meters_per_second_t vTrafficSign) const override;
  std::vector<LaneInformationTrafficRulesScmExtended> CalculateVLegal(bool spawn) const override;
  void TransitionOfEgoVehicle(LaneChangeState laneChangeState, bool transitionTrigger) override;
  units::velocity::meters_per_second_t VReasonPassingSlowPlatoon() const override;
  double GetIntensityForNeedToChangeLane(AreaOfInterest aoi) const override;
  double GetIntensityForSlowerLeadingVehicle(AreaOfInterest aoi) const override;
  units::length::meter_t GetMinDistance(AreaOfInterest aoi,
                                        MinThwPerspective perspective = MinThwPerspective::NORM,
                                        AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
                                        units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const override;
  units::length::meter_t GetMinDistance(const SurroundingVehicleInterface* vehicle,
                                        MinThwPerspective perspective = MinThwPerspective::NORM,
                                        units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const override;
  units::length::meter_t GetEqDistance(AreaOfInterest aoi, AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests) const override;
  units::length::meter_t GetEqDistance(const SurroundingVehicleInterface* vehicle) const override;
  units::length::meter_t GetInfDistance(AreaOfInterest aoi, AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests) const override;
  units::length::meter_t GetInfluencingDistance(const SurroundingVehicleInterface* vehicle) const override;
  units::length::meter_t GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, units::length::meter_t distanceToEndOfLane) const override;
  units::length::meter_t GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, RelativeLane relativeLane) const override;
  double GetUrgencyFactorForLaneChange() const override;
  double GetUrgencyFactorForAcceleration() const override;
  scm::LaneMarking::Entity GetLaneMarkingAtAoi(AreaOfInterest aoi) const override;
  scm::LaneMarking::Entity GetLaneMarkingAtDistance(units::length::meter_t distance, Side side) const override;
  scm::LaneMarking::Entity InterpreteBrokenBoldLaneMarking(units::length::meter_t distance, Side side) const override;
  double DetermineUrgencyFactorForNextExit(bool enableNewRouteRequest, const LaneChangeDimensionInterface& laneChangeDimension) const override;
  units::length::meter_t CalculateReachedDistanceForUniformlyAcceleratedMotion(units::time::second_t time,
                                                                               units::acceleration::meters_per_second_squared_t acceleration,
                                                                               units::velocity::meters_per_second_t startingVelocity,
                                                                               units::length::meter_t startingDistance) const override;
  units::velocity::meters_per_second_t CalculateReachedVelocityForUniformlyAcceleratedMotion(units::time::second_t time,
                                                                                             units::acceleration::meters_per_second_squared_t acceleration,
                                                                                             units::velocity::meters_per_second_t startingVelocity) const override;
  units::time::second_t CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleration,
                                                                                                   units::velocity::meters_per_second_t startingVelocity,
                                                                                                   units::length::meter_t changeInDistance) const override;
  units::time::second_t CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleration,
                                                                                                   units::velocity::meters_per_second_t changeInVelocity) const override;

  units::length::meter_t CalculateLateralOffsetNeutralPositionScaled() const override;
  units::acceleration::meters_per_second_squared_t CalculateAccelerationToDefuse(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t accelerationLimit) const override;
  units::acceleration::meters_per_second_squared_t CalculateAccelerationToDefuseLateral(AreaOfInterest aoi) const override;
  bool IsApproaching(AreaOfInterest aoi, AreaOfInterest aoiFar) const override;
  LaneChangeBehaviorInterface& GetLaneChangeBehavior() const override;
  units::time::second_t EstimatedTimeToLeaveLane() const override;
  bool EgoBelowJamSpeed() const override;
  units::acceleration::meters_per_second_squared_t CalculateFollowingAcceleration(const SurroundingVehicleInterface& leadingVehicle,
                                                                                  units::acceleration::meters_per_second_squared_t maxDeceleration,
                                                                                  units::length::meter_t deltaDistanceForAdjustment = 0_m) const override;

#ifdef TESTING_ENABLED
  friend class TestMentalCalculations;
  friend class TestMentalCalculations3;
  friend class TestMentalCalculations4;
  friend class TestMentalCalculations6;
  friend class TestMentalCalculations7;
#endif

protected:
  //! @brief returns the equilibium relative net Distance.
  //! @param aoi          AreaOfInterest
  //! @return RelativeNetDistanceEq
  virtual units::length::meter_t GetRelativeNetDistanceEq(AreaOfInterest aoi) const;

  //! @brief Calculate velocity taking into consideration the object in AOI
  //! @param aoi AreaOfInterest
  //! @param ttc Time to collision
  //! @return Current Velocity + acceleration * ttc, but at least 0.0
  virtual units::velocity::meters_per_second_t CalculateVelocityAtCurrentParameters(AreaOfInterest aoi, units::time::second_t ttc) const;

  //! @brief Calculates the relative net distance for the given lane.
  //! @param targetLane Lane for which the relative net distance is returned.
  //! @return Zero if a vehicle is in the side AOI of the lane, the relative net distance to the vehicle if it is in front of the ego, Infinity otherwise.
  virtual units::length::meter_t GetRelativeNetDistance(SurroundingLane targetLane) const;

  //! @brief Evaluate if the velocity difference of the front and front_far agent is small enough to count as a platoon.
  //! @param side Side of the considered agents relative to ego
  //! @return True if the velocity difference is below 3 m/s
  virtual bool CheckVelocityDifferenceIsPlatoon(const Side side) const;

  //! @brief Evaluate if the relative distance of the front and front_far agent is small enough to count as a platoon.
  //! @param side Side of the considered agents relative to ego
  //! @return True if the distance between front and front_far agent + vehicle length is larger than the half speedo distance
  virtual bool CheckRelativeDistanceIsPlatoon(const Side side) const;

  //! @brief Check if the there is an anticipated lane change situation from the side that is not hight risk.
  //! @param side Side that is checked for lane changers
  //! @return True if there is an anticipated lane change situation going on.
  virtual bool AnticipatedLaneChangerPresent(const Side side) const;

  //! @brief Check if the platoon on the side is slow enough to be considered a jam.
  //! @param side Side that is checked for a jam platoon.
  //! @return True if the velocity of the front agent on the given side is lower than the jam velocity
  virtual bool PlatoonBelowJamSpeed(const Side side) const;

  //! @brief Check if there is a agent in front and front_far present on the given side.
  //! @param side Side that is checked for platoon cars
  //! @return True if there is acar present in front and front far on the given side
  virtual bool PossiblePlatoonCarsVisible(const Side side) const;

  //! @brief Adjust lateral offset in lane based on cooperation factor if ActionState is IntentToChangeLanes.
  //! @return Desired lateral position in lane.
  units::length::meter_t DetermineLateralOffsetDueToCooperation() const;

  //! @brief Evaluate AOIs and distances
  //! @param aoi
  //! @param aoiFar
  //! @param distanceToObstacle
  //! @param distanceToObstacleFar
  void EvaluateAOIsAndDistances(AreaOfInterest& aoi, AreaOfInterest& aoiFar, units::length::meter_t& distanceToObstacle, units::length::meter_t& distanceToObstacleFar) const;

  //! @brief Increases the given desired following distance if ego is cooperative and is below jam speed.
  //! @param desiredDistance previously calculated desired distance.
  //! @return Desired following distance adjusted based on willingness to cooperate
  units::length::meter_t AdjustDesiredFollowingDistanceDueToCooperation(units::length::meter_t desiredDistance) const;

  //! @brief Evaluate riskObstacle
  //! @param aoi
  //! @param aoiFar
  //! @param distanceToObstacle
  //! @param distanceToObstacleFar
  //! @param riskObstacle
  //! @return The risk quotient for the distance
  virtual double CalculateRiskObstacle(AreaOfInterest aoi,
                                       AreaOfInterest aoiFar,
                                       units::length::meter_t distanceToObstacle,
                                       units::length::meter_t distanceToObstacleFar,
                                       double riskObstacle) const;

  //! @brief Calculate risk obstacle
  //! @param distanceToObstacle
  //! @return The risk quotient for the distance
  virtual double CalculateRiskObstacle(units::length::meter_t distanceToObstacle) const;

  //! @brief Calculate urgencyFactor
  //! @param riskObstacle
  //! @param riskEndOfLane
  //! @return The urgency factor based on the risks
  virtual double CalculateUrgencyFactor(double riskObstacle, double& riskEndOfLane) const;

  //! @brief Determine agents persistent cooperative behavior (for rescue lane).
  bool _persistentCooperativeBehavior{false};

  //! @brief AccelerationCalculationsQuery module
  std::shared_ptr<AccelerationCalculationsQueryInterface> _accelerationCalculationQuery;

  //! @brief Calculate the lateral time to collision for side areas
  //! @param aoi                 AreaOfInterest
  //! @return sideTtc
  virtual units::time::second_t GetSideTtc(AreaOfInterest aoi) const;

  //! @brief Get the remaining distance to the point of no return for a successful coasting to the next speed limit sign in m
  //! @return Distance for regulating velocity
  virtual units::length::meter_t GetDistanceForRegulatingVelocity(units::velocity::meters_per_second_t targetVelocity) const;

  //! @brief Get the remaining distance to the point of no return for a successful braking to an end of exit in m
  //! @return Distance to point of no return for braking to end of exit
  virtual units::length::meter_t GetDistanceToPointOfNoReturnForBrakingToEndOfExit();
  
  //! @brief Calculates the maximum velocities for a platoon to the left and to the right based on the ego velocity.
  //! @param speedThreshold Speed above which the limits are changed for fast driving
  //! @return Maximum velocity of platoons to the left and to the right
  virtual std::tuple<units::velocity::meters_per_second_t, units::velocity::meters_per_second_t> CalculatePlatoonPassingVelocityLimits(units::velocity::meters_per_second_t speedThreshold) const;

  // ***************** HIGH COGNITIVE ***********************************

  //! @brief Transition surrounding object data in lateral direction.
  //! @param left    Transition to left (true) or right (false)
  void TransitionSurroundingData(bool left);

private:
  //! @brief Checks current situation/traffic rules for applicable speed limits
  //! @param spawn
  //! @param laneTrafficRules
  //! @return LaneInformationTrafficRulesScmExtended
  LaneInformationTrafficRulesScmExtended DetermineValidSpeedLimit(bool spawn, const LaneInformationTrafficRulesScmExtended& laneTrafficRules) const;

  //! @brief Filters speed limit traffic signs from all available traffic signs
  //! @param allSigns
  //! @return Set of speed limit signs
  std::vector<scm::CommonTrafficSign::Entity> FilterSpeedLimitSigns(const std::vector<scm::CommonTrafficSign::Entity>& allSigns) const;

  //! @brief returns equilibrium Tau.
  //! @param aoi          AreaOfInterest
  //! @return TauEq
  virtual units::time::second_t GetTauEq(AreaOfInterest aoi) const;

  //! @brief Getter function for the distance to the end of an exit.
  //! @return Distance to end of exit
  virtual units::length::meter_t GetDistanceToEndOfExit();

  //! @brief Calculate longitudinal displacement from desired time headway.
  //! @param relativeNetDistance double
  //! @param aoi                 AreaOfInterest
  //! @return displacement_from_desired_time_headway
  virtual units::length::meter_t GetDisplacementFromDesiredDistance(units::length::meter_t relativeNetDistance, AreaOfInterest aoi) const;

  //! @brief Calculate Urgency between min time headway and equilibrium time headway.
  //! @param tGap                 double
  //! @param aoi                  AreaOfInterest
  //! @return urgency factor based on tGap, min and eq time headway
  virtual double GetUrgencyBetweenMinAndEq(units::length::meter_t tGap, AreaOfInterest aoi) const;

  //! @brief Get deceleration from power trin drag
  virtual units::acceleration::meters_per_second_squared_t GetDecelerationFromPowertrainDrag() const;

  //! @brief Calculate the half speedometer distance for the agent in the passed area of interest.
  //! @param aoi AreaOfInterest of which the speed is used for calculation
  //! @return Half speedometer distance
  virtual units::length::meter_t CalculateHalfSpeedometerDistanceForAoi(const AreaOfInterest aoi) const;

  //! @brief Check if there is a platoon present on the given side that fulfills all necessary condition for passing it.
  //! @param side Side that is checked for a platoon that could be passed
  //! @return True if there is a slow platoon that can be passed
  virtual bool PassablePlatoonPresent(const Side side) const;

  //! @brief Calculates the applicable superelevation factors for a platoon to the left and to the right based on the ego velocity.
  //! @param speedThreshold Speed above which factors are changed to fast driving
  //! @return Superelevation factors for platoons to the left and to the right
  virtual std::tuple<double, double> CalculatePlatoonPassingSuperevelationFactors(units::velocity::meters_per_second_t speedThreshold) const;

  // ***************** HIGH COGNITIVE ***********************************

  //! @brief Lateral transition of known data.
  //! @param aoiMapping      Pairs of originating and target area of interest
  void TransitionKnownData(const std::vector<std::pair<RelativeLane, RelativeLane>>& laneMapping);

  //! @brief Lateral transition of lane specific data.
  //! @param previousAoi      Originating area of interest
  //! @param currentAoi       New area of interest
  void TransitionLaneSpecificData(RelativeLane previousLane, RelativeLane currentLane);

  //! @brief Lateral transition for unknown data (areas of interest that were not known before transitioning).
  //! @param aois     Vector of affected area of interests
  void TransitionForUnknownData(RelativeLane lane);

  //! @brief Fills default values for area of interest the ego agent transitioned from.
  //! @param aoi      Area of interest that the ego agent used to inhabit
  void TransitionForOwnPreviousAoi(AreaOfInterest aoi);

  //! @brief Evaluate mesosopic situation.
  double EvaluateUrgencyFactorMesoscopicSituation(units::length::meter_t& distanceToObstacle, double& riskObstacle, double& riskEndOfLane) const;

  //! @brief Get Time to collision (helper for CalculateUrgencyFactorSituationCollision)
  //! @param currentSituation
  virtual units::time::second_t GetTtcForUrgencyFactorSituationCollision(Situation currentSituation) const;

  //! @brief Calculate urgencyFactorSituation (collision)
  //! @param timeToCollision
  virtual double CalculateUrgencyFactorSituationCollision(units::time::second_t timeToCollision) const;

  //! @brief Calculate urgencyFactorSituation (nocollision)
  //! @param distanceToObstacle
  //! @param distanceToObstacleFar
  //! @param riskObstacle
  //! @param riskEndOfLane
  virtual double CalculateUrgencyFactorSituationNoCollision(units::length::meter_t distanceToObstacle,
                                                            units::length::meter_t distanceToObstacleFar,
                                                            double riskObstacle,
                                                            double riskEndOfLane) const;

  //! @brief Check if the passed situation is the current situation and it is not of high risk
  //! @param situation
  virtual bool IsCurrentSituationAndNotHighRisk(Situation situation) const;

  //! @brief Evaluate situation
  //! @param distanceToObstacle
  //! @param distanceToObstacleFar
  //! @param riskObstacle
  //! @param riskEndOfLane
  //! @return The urgency factor for the current situation
  double CalculateUrgencyFactorSituation(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const;

  //! @brief pointer to mental model
  MentalModelInterface* _mentalModel;

  //! @brief reference to feature extractor
  const FeatureExtractorInterface& _featureExtractor;

  //! @brief LaneChangeBehavior module
  std::shared_ptr<LaneChangeBehaviorInterface> _laneChangeBehavior;

  //! @brief SurroundingVehicleQueryFactory module
  SurroundingVehicleQueryFactoryInterface* _vehicleQueryFactory;

  //! @brief Estimated value for the full time of a normal lane change in seconds
  units::time::second_t _timeLaneChangeNorm{5.0};

  //! @brief Estimated value for the full time of an urgent lane change in seconds
  units::time::second_t _timeLaneChangeUrgent{1.5};

  //! @brief Estimated range of time to adjust between a normal and an urgent lane change in seconds
  units::time::second_t _timeRangeBetweenNormUrgent{_timeLaneChangeNorm - _timeLaneChangeUrgent};

  //! @brief High-Cognitive situation
  HighCognitiveSituation _situationHC = HighCognitiveSituation::UNDEFINED;

  //! @brief Determine if the MentalCalculations object has already been initialized with MentalModel and Stochastics objects.
  bool _isInit{false};

  //! @brief Factor used to reduce following distance
  static constexpr double FOLLOWING_DISTANCE_REDUCTION_FACTOR{1.5};

  //! @brief Anticipated brake time of the leading vehicle for car following model in s.
  static constexpr units::time::second_t ANTICIP_BRAKE_TIME{1.0};

  //! @brief delta for perceived velocities
  static constexpr units::velocity::meters_per_second_t EPSILON_PERCEPTION_DELTA_VELOCITY{1E-3};

  //! @brief Module for longitudinal calculations
  const LongitudinalCalculationsInterface* _longitudinalCalculations;

  //! @brief Little convenience collection of SCM behaviour regarding traffic rules
  struct Behavior
  {
    //! @brief if a rescue lane should be formed
    bool formRescueLane;
    //! @brief the open speed limit
    units::velocity::meters_per_second_t openSpeedLimit;
    //! @brief if there is right hand traffic
    bool rightHandTraffic;
    //! @brief if the agent should stick to the outer lane when possible
    bool keepToOuterLane;
  } _behavior;
};
