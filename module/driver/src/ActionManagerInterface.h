/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ActionManagerInterface.h

#pragma once

#include "LateralAction.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
class ActionManagerInterface
{
public:
  virtual ~ActionManagerInterface() = default;

  //! @brief Runs the leading vehicle selector, thus determining the leading vehicle for this time step
  virtual void RunLeadingVehicleSelector() = 0;

  //! @brief Determines the longitudinal action state
  virtual void DetermineLongitudinalActionState() = 0;

  //! @brief Discards the last action pattern intensities
  virtual void DiscardLastActionPatternIntensities() = 0;

  //! @brief Get the lateral action of the previous time step
  //! @return last lateral action
  virtual LateralAction GetLateralActionLastTick() = 0;

  //! @brief Get the ActionSubStateLastTick
  //! @return action substate last tick
  virtual LongitudinalActionState GetActionSubStateLastTick() const = 0;

  //! @brief Set action substate last tick
  virtual void SetActionSubStateLastTick(LongitudinalActionState actionSubStateLastTick) = 0;

  //! @brief Set LateralActionLastTick
  virtual void SetLateralActionLastTick(LateralAction lastLateralAction) = 0;

  //! @brief Generate any intensity vector and sample from it.
  virtual void GenerateIntensityAndSampleLateralAction() = 0;
};