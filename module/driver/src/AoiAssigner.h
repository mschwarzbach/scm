/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "AoiAssignerInterface.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "SurroundingVehicles/SurroundingVehicles.h"

namespace AoiAssignment
{
static const std::map<std::tuple<RelativeLane, RelativeLongitudinalPosition>, AreaOfInterest> AoiMap{
    {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::FRONT), AreaOfInterest::EGO_FRONT},
    {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::FRONT_FAR), AreaOfInterest::EGO_FRONT_FAR},
    {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::REAR), AreaOfInterest::EGO_REAR},
    {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::FRONT_FAR), AreaOfInterest::LEFT_FRONT_FAR},
    {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::FRONT), AreaOfInterest::LEFT_FRONT},
    {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::SIDE), AreaOfInterest::LEFT_SIDE},
    {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::REAR), AreaOfInterest::LEFT_REAR},
    {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::FRONT_FAR), AreaOfInterest::RIGHT_FRONT_FAR},
    {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::FRONT), AreaOfInterest::RIGHT_FRONT},
    {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::SIDE), AreaOfInterest::RIGHT_SIDE},
    {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::REAR), AreaOfInterest::RIGHT_REAR},
    {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::FRONT), AreaOfInterest::LEFTLEFT_FRONT},
    {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::FRONT_FAR), AreaOfInterest::LEFTLEFT_FRONT},  // There is no such AOI
    {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::REAR), AreaOfInterest::LEFTLEFT_REAR},
    {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::SIDE), AreaOfInterest::LEFTLEFT_SIDE},
    {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::FRONT), AreaOfInterest::RIGHTRIGHT_FRONT},
    {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::FRONT_FAR), AreaOfInterest::RIGHTRIGHT_FRONT},  // There is no such AOI
    {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::REAR), AreaOfInterest::RIGHTRIGHT_REAR},
    {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::SIDE), AreaOfInterest::RIGHTRIGHT_SIDE},
};
}  // namespace AoiAssignment

//! @brief assign Vehicles to aois
class AoiAssigner : public AoiAssignerInterface
{
public:
  //! @brief SectorMapping
  using SectorMapping = std::map<std::tuple<RelativeLane, RelativeLongitudinalPosition>, std::vector<SurroundingVehicleInterface*>>;

  //! @brief Constructor initializes and creates AoiAssigner.
  //! @param infrastructure
  //! @param egoInfo
  AoiAssigner(const InfrastructureCharacteristicsInterface& infrastructure, const OwnVehicleInformationSCM& egoInfo)
      : _infrastructure{infrastructure},
        _egoInfo{egoInfo} {};

  AoiVehicleMapping AssignToAois(const SurroundingVehicleVector& surroundingVehicles) const override;
  AoiVehicleMapping AssignToAoisVector(const LaneVehicleMapping& laneMapping) const override;
  LaneVehicleMapping AssignToLanes(const SurroundingVehicleVector& surroundingVehicles) const override;
  AoiVehicleMapping AssignToAois(const LaneVehicleMapping& laneMapping) const override;

private:
  //! @brief Assigns the vehicles to a sector around the ego agent based on relative longitudinal position and lane.
  //! Sectors are LeftFront, LeftSide, LeftRear, EgoFront, EgoRear, etc.
  //! @param laneMapping
  //! @return sector mapping
  SectorMapping AssignVehiclesToSectors(const LaneVehicleMapping& laneMapping) const;

  //! @brief Assigns the vehicles to a sector around the ego agent based on relative longitudinal position and lane.
  //! Sectors are LeftFront, LeftSide, LeftRear, EgoFront, EgoRear, etc.
  //! @param laneMapping
  //! @return sector mapping
  SectorMapping AssignVehiclesToSectorsVector(const LaneVehicleMapping& laneMapping) const;

  //! @brief Assigns vehicles to the side of ego to their corresponding AOI.
  //! @param sectorMap
  //! @return aoi vehicle mapping
  static AoiVehicleMapping AssignSideAoi(const SectorMapping& sectorMap);

  //! @brief Assigns vehicles in front of ego to their corresponding AOI.
  //! @param sectorMap
  //! @return aoi vehicle mapping
  static AoiVehicleMapping AssignFrontAoi(const SectorMapping& sectorMap);

  //! @brief Assigns vehicles behind ego to their corresponding AOI.
  //! @param sectorMap
  //! @return aoi vehicle mapping
  static AoiVehicleMapping AssignRearAoi(const SectorMapping& sectorMap);

  //! @brief Assigns vehicle to relative lane.
  //! @param vehicle
  //! @return relative lane
  RelativeLane AssignToLane(const SurroundingVehicleInterface& vehicle) const;

  //! @brief Creates sector map with empty vector for each sector to avoid access to non existing map keys.
  //! @return sector mapping
  static SectorMapping CreateEmptySectorMap();

  //! @brief Creates sector map with empty vector for each sector to avoid access to non existing map keys.
  //! @return sector mapping
  static SectorMapping CreateEmptySectorMapVector();

  //! @brief infrastructure
  const InfrastructureCharacteristicsInterface& _infrastructure;
  //! @brief egoInfo
  const OwnVehicleInformationSCM& _egoInfo;
};
