/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "MentalCalculations.h"

#include <cmath>
#include <memory>
#include <stdexcept>
#include <string>

#include "AccelerationCalculations/AccelerationAdjustmentParameters.h"
#include "AccelerationCalculations/AccelerationCalculationsQuery.h"
#include "AccelerationCalculations/AccelerationComponents.h"
#include "FeatureExtractorInterface.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "LaneChangeBehavior.h"
#include "LaneQueryHelper.h"
#include "LateralAction.h"
#include "LateralActionQuery.h"
#include "LongitudinalCalculations/LongitudinalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "include/common/ScmEnums.h"
#include "src/Logging/Logging.h"

using namespace units::literals;

MentalCalculations::MentalCalculations(const FeatureExtractorInterface& featureExtractor,
                                       bool formRescueLane,
                                       units::velocity::meters_per_second_t openSpeedLimit,
                                       bool rightHandTraffic,
                                       bool keepToOuterLane)
    : _featureExtractor{featureExtractor},
      _behavior{formRescueLane, openSpeedLimit, rightHandTraffic, keepToOuterLane}
{
}

void MentalCalculations::Initialize(MentalModelInterface* mentalModel, SurroundingVehicleQueryFactoryInterface* queryFactory, const LongitudinalCalculationsInterface* longitudinalCalculation)
{
  if (!_isInit)
  {
    _mentalModel = mentalModel;
    _mentalModel->CheckSituationCooperativeBehavior();
    _persistentCooperativeBehavior = _mentalModel->GetCooperativeBehavior();
    _laneChangeBehavior = CreateLaneChangeBehavior(*this, *_mentalModel, _featureExtractor);
    _vehicleQueryFactory = queryFactory;
    _longitudinalCalculations = longitudinalCalculation;
    _accelerationCalculationQuery = CreateAccelerationCalculationsQuery(*_mentalModel, *this);

    _isInit = true;
  }
}

units::length::meter_t MentalCalculations::GetRelativeNetDistanceEq(AreaOfInterest aoi) const
{
  auto extrapolatedRelativeNetDistance = 0._m;
  const int sideAoiIndex{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
  bool isMerge = _mentalModel->GetIsMergeRegulate(aoi, sideAoiIndex);

  // Check for speed reduction due to prohibition of overtaking on right side
  if (!isMerge && aoi == AreaOfInterest::LEFT_FRONT &&
      _mentalModel->DetermineVelocityReasonRightOvertakingProhibition() == _mentalModel->GetAbsoluteVelocityTargeted())
  {
    // Reduce following distance when leading vehicle is in LEFT_FRONT and ego adjusts vTarget accordingly
    extrapolatedRelativeNetDistance = FOLLOWING_DISTANCE_REDUCTION_FACTOR * _mentalModel->GetRelativeNetDistance(aoi);
  }
  else  // Regular case
  {
    extrapolatedRelativeNetDistance = _mentalModel->GetRelativeNetDistance(aoi);
  }

  return GetDisplacementFromDesiredDistance(extrapolatedRelativeNetDistance, aoi);
}

units::time::second_t MentalCalculations::GetTimeHeadwayBetweenTwoAreasOfInterest(AreaOfInterest aoi_first, AreaOfInterest aoi_second) const  // TODO CHECK is calculation of deltaS/ vRear in every case necessary
{
  AreaOfInterest aoi_rear = AreaOfInterest::EGO_REAR;
  AreaOfInterest aoi_front = AreaOfInterest::EGO_FRONT;
  auto vRear = 0._mps;
  auto deltaS = 0._m;

  // Check for agents in involved aois
  if (!_mentalModel->GetIsVehicleVisible(aoi_first) || !_mentalModel->GetIsVehicleVisible(aoi_second))
  {
    return _mentalModel->GetPreviewDistance() / units::math::max(1.0_mps, _mentalModel->GetLongitudinalVelocityEgo());  // Default if one aoi is empty
  }

  // Both aois are in FRONT areas
  if (LaneQueryHelper::IsFrontArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    if ((_mentalModel->GetRelativeNetDistance(aoi_first) + _mentalModel->GetVehicleLength(aoi_first)) <
        _mentalModel->GetRelativeNetDistance(aoi_second))
    {
      aoi_front = aoi_second;
      aoi_rear = aoi_first;
    }
    else if ((_mentalModel->GetRelativeNetDistance(aoi_second) + _mentalModel->GetVehicleLength(aoi_second)) <
             _mentalModel->GetRelativeNetDistance(aoi_first))
    {
      aoi_front = aoi_first;
      aoi_rear = aoi_second;
    }
    else
    {
      return 0._s;
    }

    deltaS = _mentalModel->GetRelativeNetDistance(aoi_front) - _mentalModel->GetRelativeNetDistance(aoi_rear) +
             _mentalModel->GetVehicleLength(aoi_rear);
    vRear = _mentalModel->GetLongitudinalVelocity(aoi_rear);
  }

  // One aoi is FRONT and one is SIDE
  if (LaneQueryHelper::IsSideArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    aoi_front = aoi_second;
    aoi_rear = aoi_first;
  }
  else if (LaneQueryHelper::IsSideArea(aoi_second) && LaneQueryHelper::IsFrontArea(aoi_first))
  {
    aoi_front = aoi_first;
    aoi_rear = aoi_second;
  }

  int sideAoiIndex{_mentalModel->DetermineIndexOfSideObject(aoi_rear, SideAoiCriteria::FRONT)};
  deltaS = _mentalModel->GetRelativeNetDistance(aoi_front);
  vRear = _mentalModel->GetLongitudinalVelocity(aoi_rear, sideAoiIndex);

  // One aoi is FRONT and one is REAR
  if (LaneQueryHelper::IsRearArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    aoi_front = aoi_second;
    aoi_rear = aoi_first;
  }
  else if (LaneQueryHelper::IsRearArea(aoi_second) && LaneQueryHelper::IsFrontArea(aoi_first))
  {
    aoi_front = aoi_first;
    aoi_rear = aoi_second;
  }

  deltaS = _mentalModel->GetRelativeNetDistance(aoi_front) + _mentalModel->GetRelativeNetDistance(aoi_rear) +
           _mentalModel->GetVehicleLength();
  vRear = _mentalModel->GetLongitudinalVelocity(aoi_rear);

  // one aoi is SIDE and one is REAR
  if (LaneQueryHelper::IsSideArea(aoi_first) && LaneQueryHelper::IsRearArea(aoi_second))
  {
    aoi_front = aoi_first;
  }
  else if (LaneQueryHelper::IsSideArea(aoi_second) && LaneQueryHelper::IsRearArea(aoi_first))
  {
    aoi_front = aoi_second;
  }

  sideAoiIndex = _mentalModel->DetermineIndexOfSideObject(aoi_rear, SideAoiCriteria::FRONT);
  deltaS = _mentalModel->GetRelativeNetDistance(aoi_rear);
  vRear = _mentalModel->GetLongitudinalVelocity(aoi_rear, sideAoiIndex);

  // Both aois are in REAR areas
  if (LaneQueryHelper::IsRearArea(aoi_first) && LaneQueryHelper::IsRearArea(aoi_second))
  {
    if ((_mentalModel->GetRelativeNetDistance(aoi_first) + _mentalModel->GetVehicleLength(aoi_first)) <
        _mentalModel->GetRelativeNetDistance(aoi_second))
    {
      aoi_front = aoi_first;
      aoi_rear = aoi_second;
    }
    else if ((_mentalModel->GetRelativeNetDistance(aoi_second) + _mentalModel->GetVehicleLength(aoi_second)) <
             _mentalModel->GetRelativeNetDistance(aoi_first))
    {
      aoi_front = aoi_second;
      aoi_rear = aoi_first;
    }
    else
    {
      return 0._s;
    }

    deltaS = _mentalModel->GetRelativeNetDistance(aoi_rear) - _mentalModel->GetRelativeNetDistance(aoi_front) +
             _mentalModel->GetVehicleLength(aoi_front);
    vRear = _mentalModel->GetLongitudinalVelocity(aoi_rear);
  }

  // Both in SIDE aoi is not defined
  if ((aoi_first == AreaOfInterest::LEFT_SIDE && aoi_second == AreaOfInterest::RIGHT_SIDE) ||
      (aoi_first == AreaOfInterest::RIGHT_SIDE && aoi_second == AreaOfInterest::LEFT_SIDE))
  {
    return 0._s;
  }

  return deltaS / vRear;
}

units::time::second_t MentalCalculations::GetTauEq(AreaOfInterest aoi) const
{
  // Only used in HighCognitive w/o side-AOI -> no specification necessary
  auto deltaVEq = _mentalModel->GetLongitudinalVelocityDelta(aoi);
  auto relativeNetDistanceEq = GetRelativeNetDistanceEq(aoi);

  auto tauEq = ScmCommons::CalculateTTC(deltaVEq, relativeNetDistanceEq, aoi);

  return tauEq;
}

double MentalCalculations::GetTauDotEq(AreaOfInterest aoi) const
{
  // Only used in HighCognitive w/o side-AOI -> no specification necessary
  double tauDotEq = 0.;
  auto deltaVEq = _mentalModel->GetLongitudinalVelocityDelta(aoi);
  auto relativeNetDistanceEq = GetRelativeNetDistanceEq(aoi);
  auto deltaAEq = _mentalModel->GetAccelerationDelta(aoi);
  auto tauEq = GetTauEq(aoi);

  if (units::math::fabs(tauEq) >= 99._s)
  {
    tauDotEq = 0.;
  }
  else
  {
    tauDotEq = ScmCommons::CalculateTauDot(deltaVEq, relativeNetDistanceEq, deltaAEq, aoi);
  }

  return tauDotEq;
}

units::acceleration::meters_per_second_squared_t MentalCalculations::CalculateFollowingAcceleration(const SurroundingVehicleInterface& leadingVehicle, units::acceleration::meters_per_second_squared_t maxDeceleration, units::length::meter_t deltaDistanceForAdjustment) const
{
  auto accelerationAdjustmentParameters{AccelerationParameters::CalculateAccelerationAdjustmentParameters(*_accelerationCalculationQuery.get(), leadingVehicle, maxDeceleration, _mentalModel->GetMergeRegulateId(), deltaDistanceForAdjustment)};
  if (leadingVehicle.GetAssignedAoi() == AreaOfInterest::EGO_FRONT_FAR)
  {
    accelerationAdjustmentParameters.desiredDistance += 2.0 * _accelerationCalculationQuery->GetCarQueuingDistance() + _mentalModel->GetVehicleLength(ScmCommons::ConvertFarAoiToCloseAoi(leadingVehicle.GetAssignedAoi()));
    accelerationAdjustmentParameters.regulationWindowSize += _accelerationCalculationQuery->GetCarQueuingDistance() + _mentalModel->GetVehicleLength(ScmCommons::ConvertFarAoiToCloseAoi(leadingVehicle.GetAssignedAoi()));
  }

  const AccelerationComponents accelerationComponents(*_accelerationCalculationQuery.get(), accelerationAdjustmentParameters, leadingVehicle);

  const auto accelerationFromDistance{accelerationComponents.CalculateAccelerationFromDistance()};
  const auto accelerationFromVelocity{accelerationComponents.CalculateAccelerationFromVelocity()};
  auto accelerationFromAcceleration{accelerationComponents.CalculateAccelerationFromAcceleration()};

  if (leadingVehicle.GetRelativeLongitudinalPosition().GetValue() <= _mentalModel->GetCarRestartDistance() &&
      _mentalModel->GetAbsoluteVelocityEgo(false) < 0.1_mps)
  {
    return -1_mps_sq;  // Braking while standing still
  }

  if (leadingVehicle.GetRelativeNetDistance().GetValue() < (accelerationAdjustmentParameters.desiredDistance / 2) && _featureExtractor.IsCollisionCourseDetected(&leadingVehicle) && leadingVehicle.GetTtc().GetValue() < 10_s)
  {
    const int signAccelerationFromAcceleration{ScmCommons::sgn<units::acceleration::meters_per_second_squared_t>(accelerationFromAcceleration)};
    const int signAccelerationFromDistance{ScmCommons::sgn<units::acceleration::meters_per_second_squared_t>(accelerationFromDistance)};

    if (signAccelerationFromAcceleration != signAccelerationFromDistance)
    {
      accelerationFromAcceleration = 0_mps_sq;
    }
  }

  return accelerationFromDistance + accelerationFromVelocity + accelerationFromAcceleration;
}

std::vector<LaneInformationTrafficRulesScmExtended> MentalCalculations::CalculateVLegal(bool spawn) const
{
  std::vector<LaneInformationTrafficRulesScmExtended> vLegal{};

  auto leftLane = _mentalModel->GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Left();
  auto egoLane = _mentalModel->GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Close();
  auto rightLane = _mentalModel->GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Right();

  vLegal.push_back(DetermineValidSpeedLimit(spawn, leftLane));
  vLegal.push_back(DetermineValidSpeedLimit(spawn, egoLane));
  vLegal.push_back(DetermineValidSpeedLimit(spawn, rightLane));

  return vLegal;
}

LaneInformationTrafficRulesScmExtended MentalCalculations::DetermineValidSpeedLimit(bool spawn,
                                                                                    const LaneInformationTrafficRulesScmExtended& laneTrafficRules) const
{
  LaneInformationTrafficRulesScmExtended laneInformation{};
  if (!laneTrafficRules.signDetected)
  {
    laneInformation.speedLimit = _behavior.openSpeedLimit;  // set open speed limit from traffic rules as default when no sign was detected
  }
  else
  {
    laneInformation = laneTrafficRules;  // otherwise, keep old lane information
  }

  std::vector<scm::CommonTrafficSign::Entity> speedLimitSigns = FilterSpeedLimitSigns(laneTrafficRules.trafficSigns);
  for (auto sign = speedLimitSigns.begin(); sign != speedLimitSigns.end(); ++sign)
  {
    scm::CommonTrafficSign::Entity currentSign = *sign;
    const auto relativeDistance = currentSign.relativeDistance;

    if ((spawn || relativeDistance > 0._m) &&
        relativeDistance < _mentalModel->GetDriverParameters().previewDistance)
    {
      // if the traffic sign shows a speed limit that isn't a positive number, throw an exception
      // this is likely caused by an error in the configuration
      if (currentSign.value <= 0)
      {
        std::string errorMessage{
            "Found a speed limit sign with an invalid speed limit value. Check the definition of any speed limit sign.\n"
            "Keep in mind that different styles of definition may be required for different countries.\n"
            "Tried to set speed limit of zero or less from a speed limit sign."};
        Logging::Error(errorMessage);
        throw new std::domain_error(errorMessage);
      }

      // take just the first found sign
      laneInformation.speedLimit = units::make_unit<units::velocity::meters_per_second_t>(currentSign.value);
      laneInformation.relativeDistanceToTrafficsign = relativeDistance;
      laneInformation.timeOfLastDetection = _mentalModel->GetTime();
      laneInformation.speedWhenDetected = _mentalModel->GetAbsoluteVelocityEgo(true);
      laneInformation.signDetected = true;
      break;
    }
  }
  return laneInformation;
}

std::vector<scm::CommonTrafficSign::Entity> MentalCalculations::FilterSpeedLimitSigns(const std::vector<scm::CommonTrafficSign::Entity>& allSigns) const
{
  std::vector<scm::CommonTrafficSign::Entity> speedLimitSigns{};
  std::copy_if(allSigns.begin(), allSigns.end(), std::back_inserter(speedLimitSigns), [](const scm::CommonTrafficSign::Entity& sign)
               { return sign.type == scm::CommonTrafficSign::Type::MaximumSpeedLimit; });
  return speedLimitSigns;
}

units::length::meter_t MentalCalculations::CalculateVLegalInfluencigDistance(units::velocity::meters_per_second_t vEgo, units::velocity::meters_per_second_t vTrafficSign) const
{
  double anticipationPercentage = _mentalModel->GetDriverParameters().speedLimitAnticipationPercentage;

  if (vTrafficSign > vEgo)
  {
    anticipationPercentage = 1. - anticipationPercentage;
  }

  const auto influencingDistanceMin = 0._m;  // 1s THW to allow recognition of signal even if trafficSign isn't constantly in ufov
  const auto influencingDistanceMax = units::math::min(_mentalModel->GetDriverParameters().previewDistance,
                                                       GetDistanceForRegulatingVelocity(
                                                           units::math::min(_mentalModel->GetDriverParameters().desiredVelocity, vTrafficSign + _mentalModel->GetDriverParameters().amountOfDesiredVelocityViolation)));

  const auto influencingDistance = anticipationPercentage * (influencingDistanceMax - influencingDistanceMin) +
                                   influencingDistanceMin;
  return influencingDistance;
}

units::velocity::meters_per_second_t MentalCalculations::CalculateDeltaVelocityWish() const
{
  // Check for adjustment of velocity wish due to accelerating merge action or pushing rear vehicle
  const auto mergeGap{_mentalModel->GetMergeGap()};
  if (mergeGap.IsValid() && mergeGap.GetMergeAction() == MergeAction::ACCELERATING)
  {
    return _mentalModel->GetDriverParameters().amountOfDesiredVelocityViolation;
  }

  // Check for adjustment of velocity wish due to cooperative behavior regarding anticipated lane changer
  if (_mentalModel->IsAbleToCooperate())
  {
    const double urgencyFactor = GetUrgencyFactorForAcceleration();
    return ((10._kph + 10._kph * urgencyFactor));
  }

  return 0._mps;
}

void MentalCalculations::TransitionOfEgoVehicle(LaneChangeState laneChangeState, bool transitionTrigger)
{
  if (laneChangeState == LaneChangeState::LaneChangeLeft || laneChangeState == LaneChangeState::LaneChangeRight)
  {
    const bool left = laneChangeState == LaneChangeState::LaneChangeLeft;

    if (transitionTrigger)
    {
      TransitionSurroundingData(left);
    }
  }
}

/*  ********************************************************************** */
//              HIGH COGNITIVE - Calculations
//  **********************************************************************

void MentalCalculations::TransitionSurroundingData(bool left)
{
  std::vector<std::pair<RelativeLane, RelativeLane>> laneMapping = {};  // Pairs of original and new AOI
  const RelativeLane unknownLane = left ? RelativeLane::LEFTLEFT : RelativeLane::RIGHTRIGHT;
  const AreaOfInterest previousOwnAoi = left ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE;

  if (left)  // Lane change to left
  {
    laneMapping = {{RelativeLane::RIGHT, RelativeLane::RIGHTRIGHT},
                   {RelativeLane::EGO, RelativeLane::RIGHT},
                   {RelativeLane::LEFT, RelativeLane::EGO},
                   {RelativeLane::LEFTLEFT, RelativeLane::LEFT}};
  }
  else  // Lane change to right
  {
    laneMapping = {{RelativeLane::LEFT, RelativeLane::LEFTLEFT},
                   {RelativeLane::EGO, RelativeLane::LEFT},
                   {RelativeLane::RIGHT, RelativeLane::EGO},
                   {RelativeLane::RIGHTRIGHT, RelativeLane::RIGHT}};
  }

  TransitionKnownData(laneMapping);
  TransitionForUnknownData(unknownLane);
  TransitionForOwnPreviousAoi(previousOwnAoi);
}

void MentalCalculations::TransitionKnownData(const std::vector<std::pair<RelativeLane, RelativeLane>>& laneMapping)
{
  for (const auto& [previousLane, currentLane] : laneMapping)
  {
    TransitionLaneSpecificData(previousLane, currentLane);
  }
}

void MentalCalculations::TransitionLaneSpecificData(RelativeLane previousLane, RelativeLane currentLane)
{
  // Geometry information
  const LaneInformationGeometrySCM previousGeometry{_mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(previousLane)};
  LaneInformationGeometrySCM* currentGeometry{_mentalModel->GetInfrastructureCharacteristics()->UpdateLaneInformationGeometry(currentLane)};
  *currentGeometry = previousGeometry;

  // Traffic rule information
  const LaneInformationTrafficRulesScmExtended previousTrafficRules{_mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationTrafficRules(previousLane)};
  LaneInformationTrafficRulesScmExtended* currentTrafficRules{_mentalModel->GetInfrastructureCharacteristics()->UpdateLaneInformationTrafficRules(currentLane)};
  *currentTrafficRules = previousTrafficRules;
  _mentalModel->GetInfrastructureCharacteristics()->SetLaneMarkingTypeLeftLast(scm::LaneMarking::Type::None);
  _mentalModel->GetInfrastructureCharacteristics()->SetLaneMarkingTypeRightLast(scm::LaneMarking::Type::None);
}

void MentalCalculations::TransitionForUnknownData(RelativeLane lane)
{
  const LaneInformationGeometrySCM noGeometryInformation{};
  const LaneInformationTrafficRulesScmExtended noTrafficRuleInformation{};

  *_mentalModel->GetInfrastructureCharacteristics()->UpdateLaneInformationGeometry(lane) = noGeometryInformation;
  *_mentalModel->GetInfrastructureCharacteristics()->UpdateLaneInformationTrafficRules(lane) = noTrafficRuleInformation;
}

void MentalCalculations::TransitionForOwnPreviousAoi(AreaOfInterest aoi)
{
  const ObjectInformationScmExtended noSurroundingObject{};
  std::vector<ObjectInformationScmExtended> noSurroundingObjects;
  noSurroundingObjects.push_back(noSurroundingObject);
  std::vector<ObjectInformationScmExtended>* surroundingObjects{_mentalModel->GetMicroscopicData()->UpdateSideObjectsInformation(aoi)};

  *surroundingObjects = noSurroundingObjects;
  surroundingObjects->front().exist = false;
  surroundingObjects->front().reliabilityMap.at(FieldOfViewAssignment::FOVEA) = _mentalModel->GetTime();
  surroundingObjects->front().reliabilityMap.at(FieldOfViewAssignment::UFOV) = _mentalModel->GetTime();
  surroundingObjects->front().reliabilityMap.at(FieldOfViewAssignment::PERIPHERY) = _mentalModel->GetTime();
}

units::velocity::meters_per_second_t MentalCalculations::VReasonPassingSlowPlatoon() const
{
  if (EgoBelowJamSpeed())
  {
    return ScmDefinitions::INF_VELOCITY;
  }

  const bool platoonPresentOnLeftLane = PassablePlatoonPresent(Side::Left);
  const bool platoonPresentOnRightLane = PassablePlatoonPresent(Side::Right);

  if (!platoonPresentOnLeftLane && !platoonPresentOnRightLane)
  {
    return ScmDefinitions::INF_VELOCITY;
  }

  // minimal velocity of one car in the platoon left and/or right
  const auto VMinPlatoonLeft = platoonPresentOnLeftLane ? units::math::min(_mentalModel->GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT),
                                                                           _mentalModel->GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT_FAR))
                                                        : ScmDefinitions::INF_VELOCITY;
  const auto VMinPlatoonRight = platoonPresentOnRightLane ? units::math::min(_mentalModel->GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT),
                                                                             _mentalModel->GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT_FAR))
                                                          : ScmDefinitions::INF_VELOCITY;

  // calculation of passing speed
  constexpr units::velocity::meters_per_second_t VFastPassingThreshold = 55.0_mps;
  const auto [VLimitPassingSlowPlatoonLeft, VLimitPassingSlowPlatoonRight] = CalculatePlatoonPassingVelocityLimits(VFastPassingThreshold);
  const auto [VSuperevelationFactorLeft, VSuperevelationFactorRight] = CalculatePlatoonPassingSuperevelationFactors(VFastPassingThreshold);

  const auto vReasonPassingSlowPlatoonLeft = VMinPlatoonLeft <= VLimitPassingSlowPlatoonLeft ? VSuperevelationFactorLeft * VMinPlatoonLeft : ScmDefinitions::INF_VELOCITY;

  const auto vReasonPassingSlowPlatoonRight = VMinPlatoonRight <= VLimitPassingSlowPlatoonRight ? VSuperevelationFactorRight * VMinPlatoonRight : ScmDefinitions::INF_VELOCITY;

  // pass at least with jam speed but not faster than the slower platoon
  return (units::math::max(units::math::min(vReasonPassingSlowPlatoonRight, vReasonPassingSlowPlatoonLeft), _mentalModel->GetTrafficJamVelocityThreshold()));
}

units::length::meter_t MentalCalculations::CalculateHalfSpeedometerDistanceForAoi(const AreaOfInterest aoi) const
{
  // low relative distances (equal "half speedometer")
  // When driving in a column, a distance of half the speedometer value is sufficient
  // convert (m/s * 3.6) / 2 -> meter
  return units::make_unit<units::length::meter_t>((_mentalModel->GetAbsoluteVelocity(aoi).value() * 3.6) / 2.0);
}

bool MentalCalculations::CheckVelocityDifferenceIsPlatoon(const Side side) const
{
  constexpr units::velocity::meters_per_second_t MAX_VELOCITY_DIFFERENCE = 3.0_mps;

  const auto aoiFront = (side == Side::Left) ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;
  const auto aoiFrontFar = (side == Side::Left) ? AreaOfInterest::LEFT_FRONT_FAR : AreaOfInterest::RIGHT_FRONT_FAR;

  // check for sufficiently low velocity differences
  const auto frontVelocity = _mentalModel->GetAbsoluteVelocity(aoiFront);
  const auto frontFarVelocity = _mentalModel->GetAbsoluteVelocity(aoiFrontFar);

  return units::math::fabs(frontVelocity - frontFarVelocity) <= MAX_VELOCITY_DIFFERENCE;
}

bool MentalCalculations::AnticipatedLaneChangerPresent(const Side side) const
{
  const auto laneChangeSituation = side == Side::Left ? Situation::LANE_CHANGER_FROM_LEFT : Situation::LANE_CHANGER_FROM_RIGHT;

  return (_mentalModel->GetIsAnticipating() &&
          _mentalModel->GetCurrentSituation() == laneChangeSituation &&
          _mentalModel->GetSituationRisk(laneChangeSituation) != Risk::HIGH &&
          _mentalModel->IsSituationAnticipated(laneChangeSituation));
}

bool MentalCalculations::CheckRelativeDistanceIsPlatoon(const Side side) const
{
  // check for sufficiently low relative distances (equal "half speedometer")
  const auto aoiFront = side == Side::Left ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;
  const auto aoiFrontFar = side == Side::Left ? AreaOfInterest::LEFT_FRONT_FAR : AreaOfInterest::RIGHT_FRONT_FAR;

  const auto nearFarDistance = _mentalModel->GetRelativeNetDistance(aoiFrontFar) -
                               _mentalModel->GetRelativeNetDistance(aoiFront);

  const auto vehicleLength = _mentalModel->GetVehicleLength(aoiFront);
  const auto maximumDistance = CalculateHalfSpeedometerDistanceForAoi(aoiFront);

  return (nearFarDistance + vehicleLength) < maximumDistance;
}

bool MentalCalculations::PlatoonBelowJamSpeed(const Side side) const
{
  const auto aoiFront = side == Side::Left ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;

  return _mentalModel->GetAbsoluteVelocity(aoiFront) < _mentalModel->GetTrafficJamVelocityThreshold();
}

bool MentalCalculations::EgoBelowJamSpeed() const
{
  return _mentalModel->GetAbsoluteVelocityEgo(true) < _mentalModel->GetTrafficJamVelocityThreshold();
}

bool MentalCalculations::PossiblePlatoonCarsVisible(const Side side) const
{
  const auto aoiFront = side == Side::Left ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;
  const auto aoiFrontFar = side == Side::Left ? AreaOfInterest::LEFT_FRONT_FAR : AreaOfInterest::RIGHT_FRONT_FAR;

  return _mentalModel->GetIsVehicleVisible(aoiFront) &&
         _mentalModel->GetIsVehicleVisible(aoiFrontFar);
}

bool MentalCalculations::PassablePlatoonPresent(const Side side) const
{
  return PossiblePlatoonCarsVisible(side) &&
         CheckVelocityDifferenceIsPlatoon(side) &&
         CheckRelativeDistanceIsPlatoon(side) &&
         !AnticipatedLaneChangerPresent(side) &&
         !PlatoonBelowJamSpeed(side);
}

std::tuple<units::velocity::meters_per_second_t, units::velocity::meters_per_second_t> MentalCalculations::CalculatePlatoonPassingVelocityLimits(units::velocity::meters_per_second_t speedThreshold) const
{
  const auto egoSpeed = _mentalModel->GetAbsoluteVelocityEgo(true);

  const units::velocity::meters_per_second_t VLimitOffset = egoSpeed > speedThreshold ? 9.75_mps : -4.0_mps;
  const auto LeftSpeedFactor = egoSpeed > speedThreshold ? 0.45 : 0.70;
  const auto RightSpeedFactor = egoSpeed > speedThreshold ? 0.4 : 0.65;

  const units::velocity::meters_per_second_t VLimitPassingSlowPlatoonLeft = egoSpeed * LeftSpeedFactor + VLimitOffset;
  const units::velocity::meters_per_second_t VLimitPassingSlowPlatoonRight = egoSpeed * RightSpeedFactor + VLimitOffset;

  return std::make_tuple(VLimitPassingSlowPlatoonLeft, VLimitPassingSlowPlatoonRight);
}

std::tuple<double, double> MentalCalculations::CalculatePlatoonPassingSuperevelationFactors(units::velocity::meters_per_second_t speedThreshold) const
{
  const auto egoFast = _mentalModel->GetAbsoluteVelocityEgo(true) > static_cast<units::velocity::meters_per_second_t>(speedThreshold);;

  const auto VSuperevelationFactorLeft = egoFast ? 2.23 : 1.43;
  const auto VSuperevelationFactorRight = egoFast ? 2.5 : 1.54;

  return std::make_tuple(VSuperevelationFactorLeft, VSuperevelationFactorRight);
}

units::length::meter_t MentalCalculations::GetDistanceToEndOfExit()
{
  return _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->distanceToEndOfNextExit;
}

double MentalCalculations::GetIntensityForNeedToChangeLane(AreaOfInterest aoi) const
{
  // intensity of an observed agent in aoi to change lane due to his ending lane
  // can only be evaluated when ego sees the end of the lane in aoi, hence steps in intensity from 0 to 0.X are normal
  const int sideAoiIndex{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};

  if (!_mentalModel->GetIsVehicleVisible(aoi, sideAoiIndex))
  {
    return 0.;
  }

  units::length::meter_t extrapolatedRelativeNetDistance = _mentalModel->GetRelativeNetDistance(aoi);
  units::length::meter_t extrapolatedDistToEndOfLane = 0._m;

  const LaneInformationGeometrySCM* info{_mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->at(aoi)};
  if (!info)
  {
    throw std::runtime_error("Unexpected Area Of Interest for call of MesoscopicInformation::GetIntensityForNeedToChangeLane");
  }
  extrapolatedDistToEndOfLane = info->distanceToEndOfLane;

  if (extrapolatedDistToEndOfLane < _mentalModel->GetInfluencingDistanceToEndOfLane() &&
      extrapolatedRelativeNetDistance < extrapolatedDistToEndOfLane)
  {
    return (_mentalModel->GetInfluencingDistanceToEndOfLane() -
            (extrapolatedDistToEndOfLane - extrapolatedRelativeNetDistance)) /
           _mentalModel->GetInfluencingDistanceToEndOfLane();
  }

  return 0.;
}

double MentalCalculations::GetIntensityForSlowerLeadingVehicle(AreaOfInterest aoi) const
{
  AreaOfInterest aoiFar = AreaOfInterest::NumberOfAreaOfInterests;

  if (aoi == AreaOfInterest::LEFT_FRONT)
  {
    aoiFar = AreaOfInterest::LEFT_FRONT_FAR;
  }
  else if (aoi == AreaOfInterest::LEFT_SIDE)
  {
    aoiFar = AreaOfInterest::LEFT_FRONT;
  }
  else if (aoi == AreaOfInterest::RIGHT_SIDE)
  {
    aoiFar = AreaOfInterest::RIGHT_FRONT;
  }
  else if (aoi == AreaOfInterest::RIGHT_FRONT)
  {
    aoiFar = AreaOfInterest::RIGHT_FRONT_FAR;
  }
  else
  {
    return 0.;
  }

  if (!_mentalModel->GetIsVehicleVisible(aoiFar) ||  // Not visible
      !IsApproaching(aoi, aoiFar))                   // No approaching
  {
    return 0.;
  }

  // TODO: Temporary solution until concept for new GetUrgencyBetweenMinAndEq Method is implemented (User-Story: 809)
  // Set Scaling factor for scaling result of GetUrgencyBetweenMinAndEq
  double factorVehicleTypes{1.};
  const int sideAoiIndexAsFront{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::REAR)};  // When the side object is the front object, then the last object is relevant
  const int sideAoiIndexAsRear{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};  // When the side object is the rear object, then the first object is relevant

  if (ScmCommons::IsCar(_mentalModel->GetVehicleClassification(aoi, sideAoiIndexAsRear)))
  {
    if (ScmCommons::IsCar(_mentalModel->GetVehicleClassification(aoiFar, sideAoiIndexAsFront)))
    {
      factorVehicleTypes = 0.75;
    }
    else if (ScmCommons::IsTruck(_mentalModel->GetVehicleClassification(aoiFar, sideAoiIndexAsFront)))
    {
      factorVehicleTypes = 1.0;
    }
  }
  else if (ScmCommons::IsTruck(_mentalModel->GetVehicleClassification(aoi, sideAoiIndexAsRear)))
  {
    if (ScmCommons::IsCar(_mentalModel->GetVehicleClassification(aoiFar, sideAoiIndexAsFront)))
    {
      factorVehicleTypes = 0.0;
    }
    else if (ScmCommons::IsTruck(_mentalModel->GetVehicleClassification(aoiFar, sideAoiIndexAsFront)))
    {
      factorVehicleTypes = 0.5;
    }
  }

  const auto relativeNetDistanceNetBetweenObservedVehicles{_mentalModel->GetRelativeNetDistance(aoiFar) -
                                                           _mentalModel->GetRelativeNetDistance(aoi) + _mentalModel->GetVehicleLength(aoi, sideAoiIndexAsRear)};

  // TODO Min and Eq of from the other aoi to other aoiFar -> not the own perspective
  return GetUrgencyBetweenMinAndEq(relativeNetDistanceNetBetweenObservedVehicles, aoiFar) * factorVehicleTypes;
}

units::length::meter_t MentalCalculations::GetDisplacementFromDesiredDistance(units::length::meter_t relativeNetDistance, AreaOfInterest aoi) const
{
  // No side index (formerly: SideAoiCriteria) set since only used in TauDotEq Calculation etc. in HighCognitive -> Standard Value is fine
  auto desiredDistance = GetEqDistance(aoi);

  if (_mentalModel->GetIsMergeRegulate(aoi))
  {
    desiredDistance = GetMinDistance(aoi, MinThwPerspective::EGO_ANTICIPATED_REAR);
  }

  return (relativeNetDistance - desiredDistance);
}

units::length::meter_t MentalCalculations::GetMinDistance(const SurroundingVehicleInterface* vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t vGap) const
{
  const auto* ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
  auto minDistance = _longitudinalCalculations->CalculateSecureNetDistance(*vehicle,
                                                                           *ego,
                                                                           -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                           0._mps,
                                                                           _mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean,
                                                                           -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                           _mentalModel->GetDriverParameters().carQueuingDistance,
                                                                           perspective,
                                                                           vGap);

  minDistance = units::math::max(minDistance, _mentalModel->GetDriverParameters().carQueuingDistance);

  return minDistance;
}

units::length::meter_t MentalCalculations::GetMinDistance(AreaOfInterest aoi, MinThwPerspective perspective, AreaOfInterest aoiReference, units::velocity::meters_per_second_t vGap) const
{
  const auto* vehicle{_mentalModel->GetVehicle(aoi)};
  if (vehicle == nullptr)
  {
    return ScmDefinitions::INF_DISTANCE;
  }

  units::length::meter_t minDistance{ScmDefinitions::INF_DISTANCE};
  if (aoiReference == AreaOfInterest::NumberOfAreaOfInterests)
  {
    const auto* ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
    minDistance = _longitudinalCalculations->CalculateSecureNetDistance(*vehicle,
                                                                        *ego,
                                                                        -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                        0._mps,
                                                                        _mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean,
                                                                        -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                        _mentalModel->GetDriverParameters().carQueuingDistance,
                                                                        perspective,
                                                                        vGap);
  }
  else
  {
    const auto* referenceVehicle{_mentalModel->GetVehicle(aoiReference)};
    if (referenceVehicle == nullptr)
    {
      return ScmDefinitions::INF_DISTANCE;
    }
    minDistance = _longitudinalCalculations->CalculateSecureNetDistance(*vehicle,
                                                                        *referenceVehicle,
                                                                        -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                        0._mps,
                                                                        _mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean,
                                                                        -_mentalModel->GetDriverParameters().maximumLongitudinalDeceleration,
                                                                        _mentalModel->GetDriverParameters().carQueuingDistance,
                                                                        vGap);
  }

  minDistance = units::math::max(minDistance, _mentalModel->GetDriverParameters().carQueuingDistance);

  return minDistance;
}

units::length::meter_t MentalCalculations::GetEqDistance(AreaOfInterest aoi, AreaOfInterest aoiReference) const
{
  // There is no time headway at all, when there is no vehicle in the observed area
  if (!_mentalModel->GetIsVehicleVisible(aoi))
  {
    return 0._m;
  }

  if (!_mentalModel->GetIsVehicleVisible(aoiReference))
  {
    return 0._m;
  }
  if (_mentalModel->IsObjectInSideArea(aoiReference, aoi))
  {
    return 0._m;
  }
  if (!_mentalModel->IsObjectInSurroundingArea(aoiReference, aoi, false))
  {
    return 0._m;
  }

  // Determine, which vehicle is the leading vehicle or change perspective for planing reasons
  auto currentVelFront = _mentalModel->GetLongitudinalVelocity(aoi);
  if (_mentalModel->IsObjectInRearArea(aoiReference, aoi))  // the vehicles that are behind cannot be handled as rear vehicles while anticipating front perspective
  {
    currentVelFront = _mentalModel->GetLongitudinalVelocity(aoiReference);
  }

  auto anticipVelEnd = 0._mps;

  if (currentVelFront != 0.0_mps)
  {
    anticipVelEnd = units::math::max(0._mps, -_mentalModel->GetComfortLongitudinalDeceleration() * ANTICIP_BRAKE_TIME + currentVelFront);
  }

  auto deltaDistEnd = GetMinDistance(aoi, MinThwPerspective::NORM, aoiReference);

  const auto* vehicle{_mentalModel->GetVehicle(aoi)};
  const auto* ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
  auto secDistanceForEq = _longitudinalCalculations->CalculateSecureNetDistance(*vehicle,
                                                                                *ego,
                                                                                -_mentalModel->GetComfortLongitudinalDeceleration(),
                                                                                anticipVelEnd,
                                                                                _mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean,
                                                                                -_mentalModel->GetComfortLongitudinalDeceleration(),
                                                                                deltaDistEnd,
                                                                                MinThwPerspective::NORM,
                                                                                units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));

  if (secDistanceForEq < deltaDistEnd)
  {
    secDistanceForEq = units::make_unit<units::length::meter_t>(deltaDistEnd.value() + 1. + 2. * units::math::fabs(_longitudinalCalculations->CalculateVelocityDelta(*vehicle, *ego)).value());
  }

  // If front vehicle is slow (traffic jam) and accelerating use restartDistance instead of carQueuingDistance
  if (aoiReference == AreaOfInterest::NumberOfAreaOfInterests &&
      _mentalModel->GetAbsoluteVelocityEgo(false) < 0.1_mps &&
      !_mentalModel->IsObjectInRearArea(aoiReference, aoi) &&
      !_mentalModel->IsObjectInSideArea(aoiReference, aoi))
  {
    secDistanceForEq = std::max(secDistanceForEq, _mentalModel->GetDriverParameters().carRestartDistance);
  }

  return secDistanceForEq;
}

units::length::meter_t MentalCalculations::GetEqDistance(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle->ToTheSideOfEgo())
  {
    return 0._m;
  }

  // Determine, which vehicle is the leading vehicle or change perspective for planing reasons
  auto currentVelFront = vehicle->GetLongitudinalVelocity().GetValue();
  if (vehicle->BehindEgo())  // the vehicles that are behind cannot be handled as rear vehicles while anticipating front perspective
  {
    currentVelFront = _mentalModel->GetLongitudinalVelocityEgo();
  }

  auto anticipVelEnd = 0._mps;

  if (currentVelFront != 0._mps)
  {
    anticipVelEnd = units::math::max(0._mps, -_mentalModel->GetComfortLongitudinalDeceleration() * ANTICIP_BRAKE_TIME + currentVelFront);
  }

  auto deltaDistEnd = GetMinDistance(vehicle, MinThwPerspective::NORM);
  const auto *ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
  auto secDistanceForEq = _longitudinalCalculations->CalculateSecureNetDistance(*vehicle,
                                                                                *ego,
                                                                                -_mentalModel->GetComfortLongitudinalDeceleration(),
                                                                                anticipVelEnd,
                                                                                _mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean,
                                                                                -_mentalModel->GetComfortLongitudinalDeceleration(),
                                                                                deltaDistEnd,
                                                                                MinThwPerspective::NORM);

  if (secDistanceForEq < deltaDistEnd)
  {
    auto query = _vehicleQueryFactory->GetQuery(*vehicle);
    secDistanceForEq = units::make_unit<units::length::meter_t>(deltaDistEnd.value() + 1. + 2. * std::fabs(query->GetLongitudinalVelocityDelta().value()));
  }

  // If front vehicle is slow (traffic jam) and accelerating use restartDistance instead of carQueuingDistance
  if (_mentalModel->GetAbsoluteVelocityEgo(false) < 0.1_mps &&
      !vehicle->BehindEgo() &&
      !vehicle->ToTheSideOfEgo())
  {
    secDistanceForEq = std::max(secDistanceForEq, _mentalModel->GetDriverParameters().carRestartDistance);
  }

  return secDistanceForEq;
}

units::acceleration::meters_per_second_squared_t MentalCalculations::GetDecelerationFromPowertrainDrag() const
{
  const auto decelerationFromPowertrainDrag = scm::common::map::query(_mentalModel->GetVehicleModelParameters().properties, scm::common::vehicle::properties::DecelerationFromPowertrainDrag);
  if (!decelerationFromPowertrainDrag.has_value())
  {
    throw std::runtime_error("DecelerationFromPowertrainDrag was not defined in VehicleCatalog");
  }
  return units::make_unit<units::acceleration::meters_per_second_squared_t>(std::stod(decelerationFromPowertrainDrag.value()));
}

units::length::meter_t MentalCalculations::GetInfDistance(AreaOfInterest aoi, AreaOfInterest aoiReference) const
{
  const int sideAoiIndex{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
  const int sideAoiReferenceIndex{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};  // TODO CHECK is aoi wrong aoiReference ?

  if (!_mentalModel->GetIsVehicleVisible(aoi, sideAoiIndex) ||
      !_mentalModel->GetIsVehicleVisible(aoi, sideAoiReferenceIndex) ||  // TODO CHECK is aoi wrong aoiReference ?
      _mentalModel->IsObjectInSideArea(aoiReference, aoi) ||
      _mentalModel->IsObjectInRearArea(aoiReference, aoi) ||
      !_mentalModel->IsObjectInSurroundingArea(aoiReference, aoi, false))
  {
    return 0._m;
  }

  const auto* ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
  const auto* vehicle{_mentalModel->GetVehicle(aoi)};

  const auto deltaVel = _longitudinalCalculations->CalculateVelocityDelta(*vehicle, *ego);
  const auto velReference = _mentalModel->GetLongitudinalVelocity(aoiReference);
  const auto comfDec = _mentalModel->GetComfortLongitudinalDeceleration();
  const auto aCoast = units::math::min(GetDecelerationFromPowertrainDrag(), comfDec);

  // Define borders
  const auto eqDist = GetEqDistance(aoi, aoiReference);
  const auto deltaVelCorr = std::clamp(deltaVel, (units::velocity::meters_per_second_t)-30._kph, 0._mps);

  const auto infDistDelta = (_mentalModel->GetDriverParameters().reactionBaseTimeMean +
                             _mentalModel->GetDriverParameters().pedalChangeTimeMean) *
                                velReference +
                            (deltaVelCorr * deltaVelCorr) / (2. * units::math::sqrt(aCoast * comfDec));
  const auto vehicleLengthReference{_mentalModel->GetVehicleLength(aoiReference)};

  const auto minInfDist = eqDist + (_mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean) * velReference + vehicleLengthReference;
  const auto maxInfDist = eqDist + 2.5_s * velReference + 3. * vehicleLengthReference;

  auto correctedInfDist = eqDist + infDistDelta;
  correctedInfDist = std::clamp(correctedInfDist, minInfDist, maxInfDist);

  return correctedInfDist;
}

units::length::meter_t MentalCalculations::GetInfluencingDistance(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle->ToTheSideOfEgo() || vehicle->BehindEgo())
  {
    return 0._m;
  }

  auto* ego{_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()};
  const auto deltaVelocity = _longitudinalCalculations->CalculateVelocityDelta(*vehicle, *ego);

  const auto velocityEgo = _mentalModel->GetLongitudinalVelocityEgo();
  const auto comfortDeceleration = _mentalModel->GetComfortLongitudinalDeceleration();
  const auto accelerationCoast = units::math::min(GetDecelerationFromPowertrainDrag(), comfortDeceleration);

  // Define borders
  const auto equilibriumDistance = GetEqDistance(vehicle);
  const auto deltaVelocityCorr = std::clamp(deltaVelocity, units::velocity::meters_per_second_t(-30._kph), 0._mps);

  const auto influencingDistanceDelta = (_mentalModel->GetDriverParameters().reactionBaseTimeMean +
                                         _mentalModel->GetDriverParameters().pedalChangeTimeMean) *
                                            velocityEgo +
                                        (deltaVelocityCorr * deltaVelocityCorr) / (2. * units::math::sqrt(accelerationCoast * comfortDeceleration));
  const auto egoVehicleLength{_mentalModel->GetVehicleLength()};

  const auto minInfluencingDistance = equilibriumDistance + (_mentalModel->GetDriverParameters().reactionBaseTimeMean + _mentalModel->GetDriverParameters().pedalChangeTimeMean) * velocityEgo +
                                      egoVehicleLength;
  const auto maxInfluencingDistance = equilibriumDistance + 2.5_s * velocityEgo + 3. * egoVehicleLength;  // magic Number
  auto correctedInfDist = equilibriumDistance + influencingDistanceDelta;
  correctedInfDist = std::clamp(correctedInfDist, minInfluencingDistance, maxInfluencingDistance);

  return correctedInfDist;
}

units::length::meter_t MentalCalculations::GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, units::length::meter_t distanceToEndOfLane) const
{
  auto dec = .75 * _mentalModel->GetDriverParameters().maximumLongitudinalDeceleration;
  auto brakeDist = velocity * velocity / 2. / dec;
  return distanceToEndOfLane - brakeDist;
}

units::length::meter_t MentalCalculations::GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, RelativeLane relativeLane) const
{
  auto dec = .75 * _mentalModel->GetDriverParameters().maximumLongitudinalDeceleration;
  auto brakeDist = velocity * velocity / 2. / dec;

  if (relativeLane == RelativeLane::LEFT)
  {
    return _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::LEFT).distanceToEndOfLaneDuringEmergency - brakeDist;
  }

  if (relativeLane == RelativeLane::RIGHT)
  {
    return _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::RIGHT).distanceToEndOfLaneDuringEmergency - brakeDist;
  }

  return _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLaneDuringEmergency - brakeDist;
}

units::length::meter_t MentalCalculations::GetDistanceToPointOfNoReturnForBrakingToEndOfExit()
{
  auto vEgo = _mentalModel->GetAbsoluteVelocityEgo(false);
  auto dec = units::math::min(1.5 * _mentalModel->GetComfortLongitudinalDeceleration(),
                              .75 * _mentalModel->GetDriverParameters().maximumLongitudinalDeceleration);
  units::length::meter_t brakeDist = vEgo * vEgo / 2. / dec;
  return GetDistanceToEndOfExit() - brakeDist;
}

units::length::meter_t MentalCalculations::GetDistanceForRegulatingVelocity(units::velocity::meters_per_second_t targetVelocity) const
{
  const auto vEgo = _mentalModel->GetAbsoluteVelocityEgo(true);
  const auto deltaV = vEgo - targetVelocity;
  auto acceleration{0.0_mps_sq};
  if (deltaV > 0._mps)
  {
    acceleration = units::math::min(_mentalModel->GetComfortLongitudinalDeceleration(), GetDecelerationFromPowertrainDrag());
  }
  else
  {
    acceleration = _mentalModel->GetComfortLongitudinalAcceleration();
  }

  // Distance assuming constant acceleration
  return (vEgo - .5 * deltaV) * (units::math::fabs(deltaV / acceleration) +
                                 _mentalModel->GetDriverParameters().pedalChangeTimeMean +
                                 _mentalModel->GetDriverParameters().reactionBaseTimeMean);
}

double MentalCalculations::GetUrgencyBetweenMinAndEq(units::length::meter_t relativeNetDistanceNetBetweenVehicles, AreaOfInterest aoi) const
{
  double urgency = 0.;
  auto vehicle = _mentalModel->GetVehicle(aoi);
  if (!_featureExtractor.IsNearEnoughForFollowing(*vehicle))
  {
    urgency = 0.;
  }
  else if (_featureExtractor.IsMinimumFollowingDistanceViolated(vehicle))
  {
    urgency = 1.;
  }
  else
  {
    urgency = (GetEqDistance(aoi) - relativeNetDistanceNetBetweenVehicles) / (GetEqDistance(aoi) - GetMinDistance(aoi));
  }

  return urgency;
}

double MentalCalculations::GetUrgencyFactorForLaneChange() const
{
  // The return value of the function is a value between 0 and 1 in which 1 stands for the maximum urgency and 0 for the minimal urgency.

  // Expansion: taking situations DETECTED and ANTICIPATED_LANE_CHANGER into account (refering thesis by Philipp Ring - BMW)
  // --> calculating an urgency using TGapMin and TGapEq, where urgency=0 for ExtrapolatedGap(aoi)> TGapEq and urgency=1 for ExtrapolatedGap(aoi)<TGapMin
  // --> urgency increases smoothly from 0 to 1 in between limits TGapEq and TGapMin

  // LaneChange when entering highway and agent is forced to continue on stopping lane due to blocking traffic needs to be revisited once GetDistanceToEndOfExit is updated to accomodate all lanetypes

  double riskObstacle = 0.;
  double riskEndOfLane = 0.;

  auto distanceToObstacle = _mentalModel->GetRelativeNetDistance(AreaOfInterest::EGO_FRONT);
  auto distanceToObstacleFar = _mentalModel->GetRelativeNetDistance(AreaOfInterest::EGO_FRONT);

  double urgencyFactorMesoscopicSituation = EvaluateUrgencyFactorMesoscopicSituation(distanceToObstacle, riskObstacle, riskEndOfLane);

  double urgencyFactorSituation = CalculateUrgencyFactorSituation(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane);

  double urgencyFactor = std::max(urgencyFactorSituation, urgencyFactorMesoscopicSituation);

  // cut to max value
  if (urgencyFactor > .9)
  {
    return .9;
  }

  return urgencyFactor;
}

double MentalCalculations::GetUrgencyFactorForAcceleration() const
{
  const auto leadingVehicle{_mentalModel->GetLeadingVehicle()};
  const auto leadingVehicleAoi{leadingVehicle != nullptr ? AreaOfInterest(leadingVehicle->GetAssignedAoi()) : AreaOfInterest::EGO_FRONT_FAR};

  double urgencyEndingLane = GetIntensityForNeedToChangeLane(leadingVehicleAoi);
  double urgencySlowerLeadingVehicle = 0.;

  if (leadingVehicleAoi == AreaOfInterest::RIGHT_FRONT)  // no overtaking on right side
  {
    urgencySlowerLeadingVehicle = GetIntensityForSlowerLeadingVehicle(leadingVehicleAoi);
  }

  return std::min((urgencyEndingLane + urgencySlowerLeadingVehicle), 1.);  // combined urgency
}

units::time::second_t MentalCalculations::GetSideTtc(AreaOfInterest aoi) const
{
  if ((aoi == AreaOfInterest::LEFT_SIDE || aoi == AreaOfInterest::RIGHT_SIDE) && _mentalModel->GetIsVehicleVisible(aoi))
  {
    units::time::second_t result = 99._s;

    for (size_t i = 0; i < _mentalModel->GetMicroscopicData()->GetSideObjectVector(aoi)->size(); i++)
    {
      ObstructionScm obstruction = _mentalModel->GetObstruction(aoi, i);
      auto relativeLatNetDistance = aoi == AreaOfInterest::LEFT_SIDE ? -obstruction.right : -obstruction.left;

      if (relativeLatNetDistance <= 0.0_m)
      {
        return 0.0_s;  // temporary fix until we know how to handle negative distances
      }

      auto vDeltaLat = _mentalModel->GetLateralVelocityDelta(aoi, i);

      return units::math::min(result, ScmCommons::CalculateTimeToCollision(vDeltaLat, relativeLatNetDistance));
    }

    return result;
  }
  else
  {
    return ScmDefinitions::TTC_LIMIT;  // standard value for cases that are not calculated here
  }
}

scm::LaneMarking::Entity MentalCalculations::GetLaneMarkingAtAoi(AreaOfInterest aoi) const
{
  if (!_mentalModel->GetVirtualAgents().empty())
  {
    std::vector<units::length::meter_t> distanceToVirtualAgents;
    auto vehicles = _mentalModel->GetVirtualAgents();
    for (auto& virtualVehicle : vehicles)
    {
      distanceToVirtualAgents.push_back(virtualVehicle.relativeLongitudinalDistance);
    }
    std::vector<units::length::meter_t>::iterator result = std::min_element(distanceToVirtualAgents.begin(), distanceToVirtualAgents.end());
    int minDistance = std::distance(distanceToVirtualAgents.begin(), result);
    return GetLaneMarkingAtDistance(vehicles[minDistance].relativeLongitudinalDistance, Side::Right);
  }
  if (!LaneQueryHelper::IsEgoLane(aoi) &&
      (_mentalModel->GetIsVehicleVisible(aoi) || aoi == AreaOfInterest::LEFT_SIDE || aoi == AreaOfInterest::RIGHT_SIDE))
  {
    const auto distanceToTarget{_mentalModel->GetRelativeNetDistance(aoi)};
    Side side;
    if (LaneQueryHelper::IsLeftLane(aoi))
    {
      side = Side::Left;
    }
    else if (LaneQueryHelper::IsRightLane(aoi))
    {
      side = Side::Right;
    }
    else  // Possible for no known lane markings
    {
      throw std::runtime_error("MentalCalculations - No valid area of interest was chosen!");
    }

    return GetLaneMarkingAtDistance(distanceToTarget, side);
  }
  else
  {
    throw std::runtime_error("FeatureExtractor - No object in AOI, can't determine distance");
  }
}

scm::LaneMarking::Entity MentalCalculations::GetLaneMarkingAtDistance(units::length::meter_t distance, Side side) const
{
  std::vector<scm::LaneMarking::Entity> laneMarkings{};
  scm::LaneMarking::Entity sectionLaneMarking;
  sectionLaneMarking.type = scm::LaneMarking::Type::None;
  sectionLaneMarking.relativeStartDistance = -999._m;
  if (Side::Left == side)
  {
    laneMarkings = _mentalModel->GetTrafficRuleInformation()->Close().laneMarkingsLeft;
  }
  else
  {
    laneMarkings = _mentalModel->GetTrafficRuleInformation()->Close().laneMarkingsRight;
  }

  auto sectionLaneMarkingNextStart{ScmDefinitions::INF_DISTANCE};  // Relative distance to start of next section of the observed aoi

  for (const auto& laneMarking : laneMarkings)
  {
    if (distance < sectionLaneMarkingNextStart && distance >= laneMarking.relativeStartDistance &&
        laneMarking.relativeStartDistance >= sectionLaneMarking.relativeStartDistance)  // New lane section found
    {
      sectionLaneMarking = laneMarking;
    }
    else if (distance < sectionLaneMarkingNextStart && distance < laneMarking.relativeStartDistance)  // New next section start found
    {
      sectionLaneMarkingNextStart = laneMarking.relativeStartDistance;
    }
  }

  return sectionLaneMarking;
}

scm::LaneMarking::Entity MentalCalculations::InterpreteBrokenBoldLaneMarking(units::length::meter_t distance, Side side) const
{
  auto laneMarking{GetLaneMarkingAtDistance(distance, side)};
  if (laneMarking.type != scm::LaneMarking::Type::Broken && !LaneQueryHelper::IsLaneMarkingBold(laneMarking) &&
      laneMarking.type != scm::LaneMarking::Type::None)
  {
    throw std::runtime_error("MentalCalculations - InterpreteBrokenBoldLaneMarking: Lane marking at AOI is not broken bold!");
  }

  const int relativeLaneIdForJunctionIngoing{_mentalModel->GetClosestRelativeLaneIdForJunctionIngoing()};
  if (side == Side::Right)
  {
    // Ego agent is on the highway
    laneMarking.width = .15_m;
    if (relativeLaneIdForJunctionIngoing != -1)
    {
      // Ego agent plans to stay on highway
      laneMarking.type = scm::LaneMarking::Type::Solid;
    }

    return laneMarking;
  }
  else
  {
    // Ego agent is on the acceleration / deceleration lane
    laneMarking.width = .15_m;
    if (relativeLaneIdForJunctionIngoing == 0)
    {
      // Ego agent plans to stay on deceleration lane
      laneMarking.type = scm::LaneMarking::Type::Solid;
    }

    return laneMarking;
  }
}

double MentalCalculations::DetermineUrgencyFactorForNextExit(bool enableNewRouteRequest, const LaneChangeDimensionInterface& laneChangeDimension) const
{
  double constexpr urgencyPerLaneToCross{0.2};
  const std::vector<int> laneIdForJunctionIngoing{_mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->relativeLaneIdForJunctionIngoing};
  if (std::find(laneIdForJunctionIngoing.begin(), laneIdForJunctionIngoing.end(), 0) != laneIdForJunctionIngoing.end())
  {
    return 0.;  // Already on the correct lane
  }

  const auto distanceToEndOfExit{_mentalModel->GetDistanceToEndOfNextExit()};
  const auto distanceToStartOfExit{_mentalModel->GetDistanceToStartOfNextExit()};

  if (distanceToEndOfExit < 0._m)  // Already past the exit or no exit known
  {
    return 0.;
  }

  const auto lengthOfExitPhase{distanceToEndOfExit - distanceToStartOfExit};

  auto visibilityDistance{_mentalModel->GetVisibilityDistance()};
  auto lengthOfRegulationPhase = 300._m;  // Starts at first HighwayExitPole sign

  const bool isCooperative{_mentalModel->GetCooperativeBehavior()};

  if (isCooperative)
  {
    lengthOfRegulationPhase += (visibilityDistance + units::make_unit<units::length::meter_t>(_mentalModel->GetVisibilityFactorForHighwayExit()));
  }
  else
  {
    lengthOfRegulationPhase += visibilityDistance;
  }

  const int closestRelativeExitLane{_mentalModel->GetClosestRelativeLaneIdForJunctionIngoing()};
  const int lanesToCross{std::abs(closestRelativeExitLane)};
  const auto lengthRequiredForLaneChange{lanesToCross * laneChangeDimension.EstimateLaneChangeLength()};

  if (distanceToEndOfExit > lengthOfExitPhase + lengthOfRegulationPhase)  // Announcement phase
  {
    return lanesToCross * urgencyPerLaneToCross;
  }
  else if (distanceToEndOfExit > lengthOfExitPhase)  // Regulation phase
  {
    return std::min((1. - (distanceToEndOfExit.value() - lengthOfExitPhase.value()) * (1.0 - urgencyPerLaneToCross) / lengthOfRegulationPhase.value()) + (lanesToCross - 1) * urgencyPerLaneToCross, 1.);
  }
  else if (distanceToEndOfExit > lengthRequiredForLaneChange)  // Exit phase
  {
    return 1.;
  }
  else  // Chance to start exit maneuver has been missed except if driver is already changing lanes
  {
    const LateralActionQuery action(_mentalModel->GetLateralAction());
    if (action.IsLaneChangingRight() && (closestRelativeExitLane == 0 || closestRelativeExitLane == -1))
    {
      return 1.;
    }
    else
    {
      if (enableNewRouteRequest)
      {
        _mentalModel->GetMicroscopicData()->UpdateOwnVehicleData()->triggerNewRouteRequest = true;  // Request new route
      }
      return 0.;
    }
  }
}

units::length::meter_t MentalCalculations::GetRelativeNetDistance(SurroundingLane targetLane) const
{
  if (targetLane == SurroundingLane::LEFT)
  {
    if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE))
    {
      return 0._m;
    }

    if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT))
    {
      return _mentalModel->GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  if (targetLane == SurroundingLane::RIGHT)
  {
    if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE))
    {
      return 0._m;
    }

    if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT))
    {
      return _mentalModel->GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  if (targetLane == SurroundingLane::EGO)
  {
    if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::EGO_FRONT))
    {
      return _mentalModel->GetRelativeNetDistance(AreaOfInterest::EGO_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  throw std::runtime_error("GetRelativeNetDistance called with invalid tagetLane");
}

units::length::meter_t MentalCalculations::CalculateReachedDistanceForUniformlyAcceleratedMotion(units::time::second_t time, units::acceleration::meters_per_second_squared_t acceleation, units::velocity::meters_per_second_t startingVelocity, units::length::meter_t startingDistance) const
{
  return acceleation / 2. * time * time + startingVelocity * time + startingDistance;
}

units::velocity::meters_per_second_t MentalCalculations::CalculateReachedVelocityForUniformlyAcceleratedMotion(units::time::second_t time, units::acceleration::meters_per_second_squared_t acceleation, units::velocity::meters_per_second_t startingVelocity) const
{
  return acceleation * time + startingVelocity;
}

units::time::second_t MentalCalculations::CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleration,
                                                                                                                     units::velocity::meters_per_second_t startingVelocity,
                                                                                                                     units::length::meter_t changeInDistance) const
{
  units::time::second_t time{ScmDefinitions::INF_TIME};
  if (acceleration == 0_mps_sq)  // case "linear motion"
  {
    if (startingVelocity != 0_mps)  // change in distance is only possible if there is a velocity
    {
      time = changeInDistance / startingVelocity;
    }
  }
  // case "accelerated motion" - change in distance is only possible for non-complex solutions of the equation of motion
  else if ((startingVelocity * startingVelocity / acceleration / acceleration + 2. * changeInDistance / acceleration).value() >= 0.)
  {
    if (ScmCommons::sgn(acceleration) == ScmCommons::sgn(changeInDistance))
    {
      time = -startingVelocity / acceleration + units::math::sqrt(startingVelocity * startingVelocity / acceleration /
                                                                      acceleration +
                                                                  2. * changeInDistance / acceleration);
    }
    else
    {
      time = -startingVelocity / acceleration - units::math::sqrt(startingVelocity * startingVelocity / acceleration /
                                                                      acceleration +
                                                                  2. * changeInDistance / acceleration);
    }
  }

  if (time < 0._s)  // negative times indicate movement in the wrong direction
  {
    time = ScmDefinitions::INF_TIME;
  }

  return time;
}

units::time::second_t MentalCalculations::CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleration,
                                                                                                                     units::velocity::meters_per_second_t changeInVelocity) const
{
  if (acceleration == 0._mps_sq)  // change in velocity not possible if there is no acceleration
  {
    return ScmDefinitions::INF_TIME;
  }

  auto time = changeInVelocity / acceleration;

  if (time < 0._s)  // negative times indicate an acceleration in the wrong direction
  {
    return ScmDefinitions::INF_TIME;
  }

  return time;
}

units::length::meter_t MentalCalculations::CalculateLateralOffsetNeutralPositionScaled() const
{
  const bool isCooperative{_mentalModel->GetCooperativeBehavior()};
  auto lateralActionState = _mentalModel->GetLateralAction();
  const LateralActionQuery action{lateralActionState};
  bool isVehicleIntentEnd = false;

  if ((lateralActionState.state != LateralAction::State::INTENT_TO_CHANGE_LANES) && (_mentalModel->GetLastLateralActionState().state == LateralAction::State::INTENT_TO_CHANGE_LANES))
  {
    isVehicleIntentEnd = true;
  }

  if (isCooperative && EgoBelowJamSpeed() && (action.IsIntentActive() || isVehicleIntentEnd))
  {
    return DetermineLateralOffsetDueToCooperation();
  }

  _mentalModel->SetLastLateralAction(lateralActionState);

  // Condition for calculating lateral offset
  if (_mentalModel->GetFormRescueLaneVelocityThreshold() * 2.0 > 2.0 / 3.0 * _mentalModel->GetTrafficJamVelocityThreshold())
  {
    throw std::runtime_error("MentalCalculations - CalculatingLateralOffset: RescueLaneVelocity and JamVelocity set incorrectly");
  }

  // Only lateralOffset Driving if Situation == QUEUED_TRAFFIC
  if (!_mentalModel->IsMesoscopicSituationActive(MesoscopicSituation(MesoscopicSituation::QUEUED_TRAFFIC)))
  {
    return 0.0_m;
  }

  bool driveToLeftSide{false};
  // Drive only to left side to create the rescue lane, if there is a right lane and no left lane next to you
  if (_mentalModel->GetLaneExistence(RelativeLane::RIGHT, false) && !_mentalModel->GetLaneExistence(RelativeLane::LEFT, false))
  {
    driveToLeftSide = true;
  }

  // Set lateralOffset
  const auto lateralOffset = _mentalModel->GetLateralOffsetNeutralPosition();
  // Set lateralOffsetRescueLane according to roadposition
  auto lateralOffsetRescueLane = _behavior.formRescueLane ? _mentalModel->GetLateralOffsetNeutralPositionRescueLane() : 0.0_m;

  const auto maxLateralOffset = units::math::max(units::math::abs(lateralOffset), lateralOffsetRescueLane);
  lateralOffsetRescueLane = driveToLeftSide ? maxLateralOffset : -maxLateralOffset;

  // Disable lateralOffsetRescueLane
  // For future integration of roadtype and country specifications
  if (!_persistentCooperativeBehavior)
  {
    lateralOffsetRescueLane = lateralOffset;
  }

  // Calculation of percentage lateralOffset
  auto vEgo = _mentalModel->GetAbsoluteVelocityEgo(true);
  double lateralOffsetPercent = 0.0;
  if (vEgo > _mentalModel->GetTrafficJamVelocityThreshold())
  {
    return 0.0_m;
  }
  else if (vEgo > _mentalModel->GetTrafficJamVelocityThreshold() * 2.0 / 3.0)
  {
    // Equation for scaled position f(x) = (mx+b) * lateraloffset
    lateralOffsetPercent = (-3.0 * vEgo / _mentalModel->GetTrafficJamVelocityThreshold() + 3.0) * lateralOffset.value();
  }
  else if (vEgo > _mentalModel->GetFormRescueLaneVelocityThreshold() * 2)
  {
    lateralOffsetPercent = lateralOffset.value();
  }
  else if (vEgo > _mentalModel->GetFormRescueLaneVelocityThreshold())
  {
    // Equation for scaled position for rescue lane f(x) = mx + b
    lateralOffsetPercent = (lateralOffset.value() - lateralOffsetRescueLane.value()) * vEgo / _mentalModel->GetFormRescueLaneVelocityThreshold() + (2 * lateralOffsetRescueLane.value() - lateralOffset.value());
  }
  else
  {
    lateralOffsetPercent = lateralOffsetRescueLane.value();
  }

  // Mind lane width and vehicle width
  units::length::meter_t laneWidthEgo = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width;
  const auto vehicleWidth = _mentalModel->GetVehicleWidth();
  return (laneWidthEgo / 2.0 - vehicleWidth / 2.0) * lateralOffsetPercent;
}

units::velocity::meters_per_second_t MentalCalculations::CalculateVelocityAtCurrentParameters(AreaOfInterest aoi, units::time::second_t ttc) const
{
  const auto velocity{_mentalModel->GetAbsoluteVelocity(aoi)};
  const auto acceleration{_mentalModel->GetAcceleration(aoi)};

  return units::math::max(velocity + acceleration * ttc, 0._mps);
}

units::acceleration::meters_per_second_squared_t MentalCalculations::CalculateAccelerationToDefuse(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t accelerationLimit) const
{
  const auto minimumFollowingDistance{GetMinDistance(aoi)};
  const auto ttc{_mentalModel->GetTtc(aoi)};
  const auto velocityEgo{_mentalModel->GetLongitudinalVelocityEgo()};
  const auto velocityObject{_mentalModel->GetLongitudinalVelocity(aoi)};
  const auto estimatedVelocityObject{CalculateVelocityAtCurrentParameters(aoi, ttc)};
  const auto relativeNetDistance{_mentalModel->GetRelativeNetDistance(aoi)};

  // below minimum following distance ego will accelerate as hard as possible when observing a rear vehicle
  if (LaneQueryHelper::IsRearArea(aoi) && relativeNetDistance < minimumFollowingDistance)
  {
    return _mentalModel->GetDriverParameters().maximumLongitudinalAcceleration;
  }

  // below minimum following distance ego will target standstill, above it will try to match the objects estimated velocity

  const auto targetedVelocityEgo{_mentalModel->GetRelativeNetDistance(aoi) < minimumFollowingDistance ? 0.0_mps : estimatedVelocityObject};

  // v = v_0 + a * t for both objects and an identical t -> a_1 = (v_1 - v_01) / (v_2 - v_02) * a_2
  const auto accelerationObject{_mentalModel->GetAcceleration(aoi)};
  if (accelerationObject == 0.0_mps_sq)
  {
    return (targetedVelocityEgo - velocityEgo) / ttc;
  }

  const auto perceptionDeltaVelocity = estimatedVelocityObject - velocityObject;
  // catch values close to zero to prevent prevent numeric issues at division
  if (units::math::abs(perceptionDeltaVelocity) < EPSILON_PERCEPTION_DELTA_VELOCITY)
    return (targetedVelocityEgo - velocityEgo) / ttc;

  const auto accelerationRequired{accelerationObject * (targetedVelocityEgo - velocityEgo) / perceptionDeltaVelocity};
  return units::math::max(accelerationRequired, accelerationLimit);
}

units::acceleration::meters_per_second_squared_t MentalCalculations::CalculateAccelerationToDefuseLateral(AreaOfInterest aoi) const
{
  const int sideIndex = _mentalModel->DetermineIndexOfSideObject(aoi,
                                                                 SideAoiCriteria::MIN,
                                                                 [](const SurroundingVehicleInterface* vehicle)
                                                                 { return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value()); });
  const ObstructionScm obstruction{_mentalModel->GetObstruction(aoi, sideIndex)};
  auto relativeNetDistance{-999.9_m};

  if (obstruction.right < 0.0_m && LaneQueryHelper::IsLeftLane(aoi))  // object completely to ego's left
  {
    relativeNetDistance = -obstruction.right;
  }
  else if (obstruction.left < 0.0_m && LaneQueryHelper::IsRightLane(aoi))  // object completely to ego's right
  {
    relativeNetDistance = -obstruction.left;
  }
  else  // Object has lateral obstruction with ego
  {
    relativeNetDistance = 0.0_m;
  }

  const auto velocityDeltaLateral{_mentalModel->GetLateralVelocityDelta(aoi, sideIndex)};
  auto ttc{-999.9_s};

  if (velocityDeltaLateral >= 0.0_mps)  // Objects receede or have same lateral velocity
  {
    return 0.0_mps_sq;
  }
  else
  {
    ttc = units::math::fabs(relativeNetDistance / velocityDeltaLateral);
  }

  // Necessarty acceleration through v = v_0 + a * t
  if (ttc != 0_s)
  {
    const auto accelerationRequired = units::math::fabs(velocityDeltaLateral / ttc);
    return accelerationRequired;
  }
  else
  {
    return 0.0_mps_sq;
  }
}

bool MentalCalculations::IsApproaching(AreaOfInterest aoi, AreaOfInterest aoiFar) const
{
  const int indexFront{_mentalModel->DetermineIndexOfSideObject(aoiFar, SideAoiCriteria::REAR)};
  const int indexRear{_mentalModel->DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
  const auto deltaV = _mentalModel->GetAbsoluteVelocity(aoiFar, indexFront) -  // When side object is the front object, then the last one counts
                      _mentalModel->GetAbsoluteVelocity(aoi, indexRear);       // When sie object is the rear objet, then the first one counts
  return (deltaV < 0._mps);
}

double MentalCalculations::EvaluateUrgencyFactorMesoscopicSituation(units::length::meter_t &distanceToObstacle, double &riskObstacle, double &riskEndOfLane) const
{
  if (_mentalModel->IsMesoscopicSituationActive(MesoscopicSituation(MesoscopicSituation::CURRENT_LANE_BLOCKED)))
  {
    if (_featureExtractor.IsObstacle(_mentalModel->GetVehicle(AreaOfInterest::EGO_FRONT)))
    {
      riskObstacle = CalculateRiskObstacle(distanceToObstacle);
    }

    return CalculateUrgencyFactor(riskObstacle, riskEndOfLane);
  }
  return 0;
}

void MentalCalculations::EvaluateAOIsAndDistances(AreaOfInterest &aoi, AreaOfInterest &aoiFar, units::length::meter_t &distanceToObstacle, units::length::meter_t &distanceToObstacleFar) const
{
  switch (_mentalModel->GetCurrentSituation())
  {
    case Situation::LANE_CHANGER_FROM_LEFT:
      aoi = AreaOfInterest::LEFT_FRONT;
      aoiFar = AreaOfInterest::LEFT_FRONT_FAR;
      distanceToObstacle = _mentalModel->GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT);
      distanceToObstacleFar = _mentalModel->GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT_FAR);
      return;
    case Situation::LANE_CHANGER_FROM_RIGHT:
      aoi = AreaOfInterest::RIGHT_FRONT;
      aoiFar = AreaOfInterest::RIGHT_FRONT_FAR;
      distanceToObstacle = _mentalModel->GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT);
      distanceToObstacleFar = _mentalModel->GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT_FAR);
      return;
    default:
      aoi = AreaOfInterest::EGO_FRONT;
      aoiFar = AreaOfInterest::EGO_FRONT_FAR;
  }
}

double MentalCalculations::CalculateRiskObstacle(AreaOfInterest aoi, AreaOfInterest aoiFar, units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle) const
{
  double result = riskObstacle;
  if (_featureExtractor.IsObstacle(_mentalModel->GetVehicle(aoi)))
  {
    if (aoi == AreaOfInterest::EGO_FRONT || _mentalModel->GetObstruction(aoi).isOverlapping)
    {
      result = CalculateRiskObstacle(distanceToObstacle);
    }

    if (aoiFar != AreaOfInterest::EGO_FRONT_FAR && _mentalModel->GetObstruction(aoi).isOverlapping)
    {
      auto riskObstacleFar = CalculateRiskObstacle(distanceToObstacleFar);
      result = std::max(result, riskObstacleFar);
    }
  }
  return result;
}

units::time::second_t MentalCalculations::GetTtcForUrgencyFactorSituationCollision(Situation currentSituation) const
{
  switch (currentSituation)
  {
    case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
      return GetSideTtc(AreaOfInterest::LEFT_SIDE);

    case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
      return GetSideTtc(AreaOfInterest::RIGHT_SIDE);

    default:
      return ScmDefinitions::TTC_LIMIT;
  }
}

double MentalCalculations::CalculateUrgencyFactorSituationCollision(units::time::second_t timeToCollision) const
{
  if (timeToCollision >= ScmDefinitions::TTC_LIMIT ||  // no collision course -> no urgency
      timeToCollision >= _timeLaneChangeNorm ||        // normal lane change is possible
      _timeRangeBetweenNormUrgent == 0._s)             // cannot decide by this information
  {
    return 0.;
  }
  else if (timeToCollision <= _timeLaneChangeUrgent)  // urgent anyways
  {
    return 1.;
  }
  else  // calculate urgency factor
  {
    return (_timeRangeBetweenNormUrgent - (timeToCollision - _timeLaneChangeUrgent)) / _timeRangeBetweenNormUrgent;
  }
}

double MentalCalculations::CalculateUrgencyFactorSituationNoCollision(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const
{
  if (IsCurrentSituationAndNotHighRisk(Situation::FOLLOWING_DRIVING))
  {
    return 0;
  }
  if (IsCurrentSituationAndNotHighRisk(Situation::LANE_CHANGER_FROM_LEFT))
  {
    return GetUrgencyBetweenMinAndEq(_mentalModel->GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT), AreaOfInterest::LEFT_FRONT);
  }
  if (IsCurrentSituationAndNotHighRisk(Situation::LANE_CHANGER_FROM_RIGHT))
  {
    return GetUrgencyBetweenMinAndEq(_mentalModel->GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT), AreaOfInterest::RIGHT_FRONT);
  }

  AreaOfInterest aoi;
  AreaOfInterest aoiFar;

  EvaluateAOIsAndDistances(aoi, aoiFar, distanceToObstacle, distanceToObstacleFar);

  riskObstacle = CalculateRiskObstacle(aoi, aoiFar, distanceToObstacle, distanceToObstacleFar, riskObstacle);

  return CalculateUrgencyFactor(riskObstacle, riskEndOfLane);
}

double MentalCalculations::CalculateRiskObstacle(units::length::meter_t distanceToObstacle) const
{
  const units::length::meter_t previewDistance = _mentalModel->GetDriverParameters().previewDistance;
  return (previewDistance - distanceToObstacle) / previewDistance;
}

double MentalCalculations::CalculateUrgencyFactor(double riskObstacle, double& riskEndOfLane) const
{
  const auto distanceToEndOfLane = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;
  const auto influencingDistanceEndOfLane = _mentalModel->GetInfluencingDistanceToEndOfLane();
  const auto vEgo = _mentalModel->GetAbsoluteVelocityEgo(false);
  const auto aComf = _mentalModel->GetComfortLongitudinalDeceleration();
  const auto safetyDistanceToPointOfNoReturn = distanceToEndOfLane - (vEgo * vEgo / 2. / aComf);

  if (distanceToEndOfLane <= influencingDistanceEndOfLane)
  {
    riskEndOfLane = (influencingDistanceEndOfLane - distanceToEndOfLane) /
                    (influencingDistanceEndOfLane - distanceToEndOfLane + safetyDistanceToPointOfNoReturn);
  }
  return std::max(riskObstacle, riskEndOfLane);
}

bool MentalCalculations::IsCurrentSituationAndNotHighRisk(Situation situation) const
{
  return _mentalModel->GetCurrentSituation() == situation && _mentalModel->GetSituationRisk(situation) != Risk::HIGH;
}

double MentalCalculations::CalculateUrgencyFactorSituation(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const
{
  const Situation currentSituation = _mentalModel->GetCurrentSituation();
  switch (currentSituation)
  {
    case Situation::FOLLOWING_DRIVING:
    case Situation::LANE_CHANGER_FROM_LEFT:
    case Situation::LANE_CHANGER_FROM_RIGHT:
    case Situation::OBSTACLE_ON_CURRENT_LANE:
      return CalculateUrgencyFactorSituationNoCollision(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane);

    case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
    case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
    {
      auto timeToCollision = GetTtcForUrgencyFactorSituationCollision(currentSituation);
      return CalculateUrgencyFactorSituationCollision(timeToCollision);
    }
    default:
      return 0.;
  }
}

units::length::meter_t MentalCalculations::AdjustDesiredFollowingDistanceDueToCooperation(units::length::meter_t desiredDistance) const
{
  const bool isCooperative{_mentalModel->GetCooperativeBehavior()};

  if (desiredDistance > 0._m && isCooperative && EgoBelowJamSpeed())
  {
    const Situation situation{_mentalModel->GetCurrentSituation()};
    const bool isLaneChangerDetected{situation == Situation::LANE_CHANGER_FROM_LEFT || situation == Situation::LANE_CHANGER_FROM_RIGHT};

    if (isLaneChangerDetected)
    {
      const auto regulateVehicle{_mentalModel->GetLeadingVehicle()};
      const bool laneChangerIsAoiRegulate{regulateVehicle != nullptr &&
                                          _mentalModel->GetCausingVehicleOfSituationSideCluster() == regulateVehicle->GetAssignedAoi()};

      if (laneChangerIsAoiRegulate)
      {
        const double cooperateAdjustmentFactor{1.2};
        desiredDistance *= cooperateAdjustmentFactor;
      }
    }
  }

  return desiredDistance;
}

units::length::meter_t MentalCalculations::DetermineLateralOffsetDueToCooperation() const
{
  if (!_mentalModel->GetCooperativeBehavior())
  {
    throw std::runtime_error("MentalCalculations::DetermineLateralOffsetDueToCooperation: Cooperative behaviour is required!");
  }

  const LateralAction action{_mentalModel->GetLateralAction()};
  const LateralActionQuery advancedAction{action};

  const bool isLeft{action.direction == LateralAction::Direction::LEFT};
  auto distanceToLaneBoundary = isLeft ? _mentalModel->GetDistanceToBoundaryLeft() : _mentalModel->GetDistanceToBoundaryRight();
  const auto lateralPositionInLane{_mentalModel->GetLateralPosition()};

  if (distanceToLaneBoundary < 0._m)
  {
    // Ego is already outside the ego lane at some point of the vehicle -> no further adjustment required, since already above max
    return lateralPositionInLane;
  }

  const double cooperationFactor{_mentalModel->GetAgentCooperationFactor()};
  return isLeft ? lateralPositionInLane + distanceToLaneBoundary * cooperationFactor : lateralPositionInLane - distanceToLaneBoundary * cooperationFactor;
}

LaneChangeBehaviorInterface& MentalCalculations::GetLaneChangeBehavior() const
{
  return *_laneChangeBehavior;
}

units::time::second_t MentalCalculations::EstimatedTimeToLeaveLane() const
{
  const auto lateralVelocity = _mentalModel->GetLateralVelocity();

  if (lateralVelocity == 0._mps)  // Not changing lanes
  {
    return ScmDefinitions::INF_TIME;
  }

  const auto distanceToLaneBoundary = lateralVelocity > 0_mps ? _mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryLeft : _mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryRight;
  const auto carWidth{_mentalModel->GetVehicleModelParameters().bounding_box.dimension.width};
  const auto distanceToLeaveEgoLane{distanceToLaneBoundary + carWidth};

  auto timeToLeaveEgoLane{distanceToLeaveEgoLane / lateralVelocity};

  return timeToLeaveEgoLane;
}