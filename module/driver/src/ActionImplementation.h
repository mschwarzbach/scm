/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ActionImplementation.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include <algorithm>  // std::find
#include <optional>

#include "ActionImplementationInterface.h"
#include "ActionManager.h"
#include "FeatureExtractorInterface.h"
#include "GazeControl/GazeControl.h"
#include "MentalCalculationsInterface.h"
#include "Merging/ZipMergingInterface.h"
#include "Swerving/SwervingInterface.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "TrajectoryCalculations/TrajectoryCalculations.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"
#include "include/ScmSampler.h"
#include "include/common/LightState.h"

class MentalModelInterface;

//! ************************************************************************************************
//! @brief This component deduces action implementations from the stochastic cognitive model (SCM).
//! @details Here, goals for target velocities and accelerations are calculated.
//! Additionally, braking and turning indicator decisions are made.
//! Implements the ActionImplementationInterface
//! ************************************************************************************************
class ActionImplementation : public ActionImplementationInterface
{
public:
  //! @brief Constructor.
  //! @param stochastics Provides access to the stochastics functionality of the framework.
  //! @param mentalModel Interface to the mental processes of the stochastic cognitive model.
  //! @param featureExtractor Interface to FeatureExtractor to avoid accessing it through MentalModel
  //! @param mentalCalculations Interface to MentalCalculations to avoid accessing it through MentalModel
  //! @param swerving Interface to Swerving
  //! @param trajectoryPlanner Interface to trajectory planner instance in ScmComponents
  //! @param zipMerging Interface to zip merging
  ActionImplementation(StochasticsInterface* stochastics,
                       MentalModelInterface& mentalModel,
                       const FeatureExtractorInterface& featureExtractor,
                       const MentalCalculationsInterface& mentalCalculations,
                       SwervingInterface& swerving,
                       const TrajectoryPlannerInterface& trajectoryPlanner,
                       ZipMergingInterface& zipMerging);

  ActionImplementation(const ActionImplementation&) = delete;
  ActionImplementation(ActionImplementation&&) = delete;
  ActionImplementation& operator=(const ActionImplementation&) = delete;
  ActionImplementation& operator=(ActionImplementation&&) = delete;
  virtual ~ActionImplementation() = default;

  void UpdateInformationFromActionManager(ActionManagerInterface* actionManagerInterface) override;
  void ImplementAction() override;
  bool GetLateralDisplacementReached() const override;
  units::length::meter_t GetDesiredLateralDisplacement() const override;
  scm::LightState::Indicator GetNextIndicatorState() const override;
  bool GetFlasherActiveNext() const override;
  units::acceleration::meters_per_second_squared_t GetLongitudinalAccelerationWish() const override;
  units::angular_acceleration::radians_per_second_squared_t GetLateralDeviationGain() const override;
  units::length::meter_t GetLateralDeviationNext() const override;
  units::frequency::hertz_t GetHeadingErrorGain() const override;
  units::angle::radian_t GetHeadingErrorNext() const override;
  units::curvature::inverse_meter_t GetKappaManoeuvre() const override;
  units::curvature::inverse_meter_t GetKappaRoad() const override;
  units::curvature::inverse_meter_t GetKapaSet() const override;
  units::acceleration::meters_per_second_squared_t GetDecelerationFromPowertrainDrag() const override;
  units::length::meter_t GetLateralDisplacementTraveled() const override;

protected:
  //! @brief Adjust desired deceleration if ego is set to cooperate, is below jam speed, already braking and a lane changer is present.
  //! @param accelerationWish Acceleration wish previously calculated
  //! @return adjusted accelerationWish
  units::acceleration::meters_per_second_squared_t AdjustAccelerationWishDueToCooperation(units::acceleration::meters_per_second_squared_t accelerationWish) const;

  //! @brief Adjust indicator parameters if ego is below jam speed and is cooperative.
  //! @param startTime
  //! @param endTime
  //! @param minProb
  //! @param maxProb
  //! @return Tuple of adjusted parameters
  std::tuple<units::time::millisecond_t, units::time::millisecond_t, double, double> AdjustIndicatorParametersDueToCooperation(units::time::millisecond_t startTime, units::time::millisecond_t endTime, double minProb, double maxProb) const;

  //! @brief Determines the desired lateral displacement.
  //! @details In general, the desired lateral displacement is 0 (vehicles drive exactly in the middle of the lane).
  //! However, there are several situations where this is not the case. The most concise is a lane change where the desired lateral displacement
  //! is based on the planned trajectory or on a planned swerving maneuver. Another reason to deviate from the middle of the road is, for example, in a traffic jam
  //! where vehicles tend to drive an offset to better see what is going on ahead of them. Especially at very low velocities, in some countries driver will
  //! open up a rescue lane for police, ambulance and service vehicles.
  virtual void DetermineLateralDisplacement();

  //! @brief Does maximal probability apply. If one of these cases (IntentToChangeLanes, when QueuedTraffic is detected, or preparing to merge) occurs, the maximum activation probability is not applied and should be set to 100%.
  //! @return true if maximal probability is applied
  bool DoesMaxProbabilityApply() const;

  //! @brief Implements the stochastic activation of indicators for lane change actions.
  //! @param indicatorState State of the indicator
  //! @param durationSinceInitiation Duration since Initiation
  //! @param probIndicatorActivation Activation of Indicator by probe
  scm::LightState::Indicator ImplementStochasticIndicatorActivation(scm::LightState::Indicator indicatorState, double probIndicatorActivation);

  //! @brief Determine lateral movement
  virtual void SetLateralOutputKeys();

  //! @brief Progresses a planned trajectory (e.g. for a lane change or swerving maneuver) and determines when such a maneuver is done.
  virtual void DetermineManeuverValues();

  //! @brief Consolidates output parameter (heading error, lateral deviation etc for the controller)
  virtual void DetermineOutputValues();

  //! @brief Sets and adjusts trajectory parameter based on the current and planned swerving state
  //! @param isSwerving Flag indicating if there is already a swerving ongoing
  //! @param dimensions Current trajectory state, can be altered depending on the swerving state
  void SetSwervingValues(bool isSwerving, TrajectoryCalculations::TrajectoryDimensions& dimensions);

  //! @brief Uses trajectory planner to plan a new lane change or swerving trajectory.
  //! Must be called with a target lane for the lane change. Swerving overrides width values internally if active.
  //! @param targetLane The relative lane for which a trajectory is planned
  //! @throws std::invalid_argument If trajectory planner receives invalid arguments.
  //! @returns A new trajectory state with the newly planned trajectory and current vehicle state.
  TrajectoryState PlanTrajectory(RelativeLane targetLane);

  //! @brief The agents trajectory state
  TrajectoryState _currentTrajectoryState{};

  //! @brief Adjusts the current acceleration wish based on a merging maneuver
  //! @return Potentially adjusted acceleration wish
  units::acceleration::meters_per_second_squared_t AccelerationWishWhileMerging() const;

  //! @brief Checks if the agent should start to realign and sets/resets the flag isStartOfRealign
  void CheckForChangeInOffset();

  //! @brief Calculate the acceleration needed for speed adjustment.
  //! @param [in] velocityTargetEgo       Velocity of Target Vehicle
  //! @return acceleration_goal
  units::acceleration::meters_per_second_squared_t GetAccelerationForSpeedAdjustment(units::velocity::meters_per_second_t velocityTargetEgo) const;

  //! @brief Flag indicating if the agent just started to realign its neutral position (due to lateral offset driving)
  bool _isStartOfRealign{false};

  //! Desired lateral displacement for ideal (calculated) lane change trajectory has been reached
  bool _lateralDisplacementReached{false};

  //! @brief Limits the currently planned acceleration if there is a suitable swerving maneuver pending.
  void LimitAccelerationDueToPlannedSwerving();

  //! @brief Limits the current acceleration wish if there is a suspicious object nearby
  //! @param currentAccelerationWish
  //! @return Potentially adjusted acceleration wish
  units::acceleration::meters_per_second_squared_t LimitAccelerationDueToSuspiciousVehicle(units::acceleration::meters_per_second_squared_t currentAccelerationWish) const;

  //! @brief Desired neutral position for the Agent (lateral; t-coordinate)
  units::length::meter_t _neutralPosition{0.0_m};

  //! @brief Desired neutral position for the Agent (lateral; t-coordinate) in last time step
  units::length::meter_t _neutralPositionLast{0.0_m};

  //! @brief Set Indicator State according to actionState
  void SetIndicator();

  //! @brief Set flasher according to current situation and duration
  void SetFlasher();

  //! @brief Determine longitudinal guidance by acting according to the current longitudinal action state
  void SwitchLongitudinalActionState();

  //! @brief Longitudinal acceleration wish.
  units::acceleration::meters_per_second_squared_t _accelerationWishLongitudinal = 0._mps_sq;

  //! @brief Lateral action from last time step.
  LateralAction _lateralActionLastTick;

  //! @brief Indicator state to set in the vehicle (left, right, warn, off).
  scm::LightState::Indicator _indicatorStateNext = scm::LightState::Indicator::Off;

private:
  //! @brief Safety buffer, added to calculated swerving time
  static constexpr units::time::millisecond_t SWERVING_END_TIME_SAFETY_BUFFER{1000_ms};

  //! @brief Implements the stochastic activation of the flasher in high risk lane changer situations.
  //! @param [in] probActivation                Probe Activation
  //! @param [in] durationCurrentSituation      Duration of Current Situation
  void ImplementStochasticHighRiskFlasherActivation(double probActivation, units::time::millisecond_t durationCurrentSituation);

  //! @brief Reset the flags ModifiedTrajectory and AbortedTrajectory to false.
  void ResetTrajectoryFlag();

  //! @brief Interface to the FeatureExtractor
  const FeatureExtractorInterface& _featureExtractor;
  //! Flasher state to set in the vehicle.
  bool _flasherActiveNext = false;
  //! Lateral displacement for lane change or swerving maneuver [m].
  units::length::meter_t _desiredLateralDisplacement = 0._m;
  //! @brief Current lateral deviation regarding trajectory [m].
  units::length::meter_t _lateralDeviationNext = 0._m;
  //! @brief Current heading error regarding trajectory [rad].
  units::angle::radian_t _headingErrorNext = 0._rad;
  //! @brief Gain of lateral deviation controller [-].
  units::angular_acceleration::radians_per_second_squared_t _gainLateralDeviation = 20._rad_per_s_sq;
  //! @brief Gain of the heading error controller [-].
  units::frequency::hertz_t _gainHeadingError = 7.5_Hz;
  //! @brief Set value for trajectory curvature [1/m].
  units::curvature::inverse_meter_t _kappaSet = 0._i_m;

  //! @brief Current trajectory velocity in road coordinate system [m/s].
  units::velocity::meters_per_second_t _sVelocityRoad = 0._mps;
  //! @brief Current lateral coordinate [m].
  units::length::meter_t _lateralPositionAct = 0._m;
  //! @brief Current lateral velocity [m/s].
  units::velocity::meters_per_second_t _wVelocityRoad = 0._mps;
  //! @brief Current absolute velocity [m/s].
  units::velocity::meters_per_second_t _absoluteVelocityAct = 0._mps;
  //! @brief Current heading angle [rad].
  units::angle::radian_t _headingAct = 0._rad;
  //! @brief Current curvature of road [1/m].
  units::curvature::inverse_meter_t _kappaRoad = 0._i_m;

  //! @brief Interface to the Stochastics
  StochasticsInterface* _stochastics = nullptr;

  //! @brief Interface to the MentalModel
  MentalModelInterface& _mentalModel;

  //! @brief Interface to the MentalCalculations
  const MentalCalculationsInterface& _mentalCalculations;

  //! @brief Interface to the TrajectoryPlanner
  const TrajectoryPlannerInterface& _trajectoryPlanner;

  //! @brief Interface  to the ZipMerging
  ZipMergingInterface& _zipMerging;

  //! @brief duration since orientation of ActionState regarding side lane focus has changed (always 0 for regarding ego lane) [ms].
  units::time::millisecond_t _durationCurrentActionStateSideLaneOrientation = 0_ms;
  //! @brief Calculated total curvature of the current agent in lane and curvature of the lane.
  units::curvature::inverse_meter_t kappaSet{0._i_m};
  //! @brief Flag indicating that the current lane change is a modified lane change.
  bool _modifyTrajectory{false};
  //! @brief Flag indicating that the current lane change is an aborted lane change.
  bool _abortTrajectory{false};

  //! @brief Cycle time [ms]
  units::time::millisecond_t _cycleTime{};

  //! @brief Calculated heading angle of the current agent.
  units::angle::radian_t _headingSet{0._rad};

  //! @brief Calculated lateral position of the current agent.
  units::length::meter_t _lateralPositionSet{0._m};

  //! @brief Current curvature adjustment due to manoeuvre e.g. lane changing [1/m].
  units::curvature::inverse_meter_t _kappaManoeuvre{0._i_m};

  SwervingInterface& _swerving;

  //! @brief Checks whether SCM has just started a lane change maneuver
  //! @return True if the Agent is lane changing, false otherwise
  virtual bool IsStartOfLaneChange() const;

  //! @brief Checks whether SCM has just started its swerving maneuver or is already swerving and returning back to its original position
  //! @return True if in the corresponding state, false otherwise
  virtual bool IsStartOfSwervingOrReturningToLaneCenter() const;

  //! @brief Checks whether the given Situation is suspicious
  //! @param situation
  //! @return True if there is a suspicious object in left or right lane, false otherwise
  bool IsSuspiciousObject(Situation situation) const;

  //! @brief Checks whether the given situation is suspicious or a lane changer
  //! @param situation
  //! @return True if there is a suspicious object or a lane changer, false otherwise
  bool IsSuspiciousObjectOrLaneChanger(Situation situation) const;

  //! @brief Checks whether the situation has a side collision risk
  //! @param situation
  //! @return True if there is a side collision risk, false otherwise
  bool IsSideCollisionRisk(Situation situation) const;

  //! @brief Calculate the trajectory of a normal lane change (inactive until analytical solution for lane change length has been implemented).

  void ProgressTrajectory();
};
