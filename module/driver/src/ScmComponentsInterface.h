/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  ScmComponentsInterface.h
#pragma once

#include <iostream>
#include <memory>

#include "ActionImplementation.h"
#include "ActionManager.h"
#include "AlgorithmSceneryCar.h"
#include "AuditoryPerception.h"
#include "FeatureExtractorInterface.h"
#include "GazeControl/GazeControl.h"
#include "GazeControl/GazeControlComponentsInterface.h"
#include "GazeControl/GazeFollower.h"
#include "InformationAcquisition.h"
#include "LongitudinalCalculations/LongitudinalCalculationsInterface.h"
#include "MentalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.h"
#include "ScmDependencies.h"
#include "SituationManager.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "Swerving/SwervingInterface.h"

//! @brief Interface for ScmComponents class to enable testing of dependent classes.
struct ScmComponentsInterface
{
  virtual ~ScmComponentsInterface() = default;

  //! @brief Get MentalModelInterface
  //! @return MentalModel
  virtual MentalModelInterface* GetMentalModel() = 0;

  //! @brief Get GazeContolInterface
  //! @return GazeControl
  virtual GazeControlInterface* GetGazeControl() = 0;

  //! @brief Get GazeFollowerInterface
  //! @return GazeFollower
  virtual GazeFollowerInterface* GetGazeFollower() = 0;

  //! @brief Get SituationManagerInterface
  //! @return SituationManager
  virtual SituationManagerInterface* GetSituationManager() = 0;

  //! @brief Get ActionManagerInterface
  //! @return ActionManager
  virtual ActionManagerInterface* GetActionManager() = 0;

  //! @brief Get ActionImplementationInterface
  //! @return ActionImplementation
  virtual ActionImplementationInterface* GetActionImplementation() = 0;

  //! @brief Get AlgorithmSceneryCarInterface
  //! @return AlgorithmSceneryCar
  virtual AlgorithmSceneryCarInterface* GetAlgorithmSceneryCar() = 0;

  //! @brief Get AuditoryPerceptionInterface
  //! @return AuditoryPerception
  virtual AuditoryPerceptionInterface* GetAuditoryPerception() = 0;

  //! @brief Get InformationAcquisitionInterface
  //! @return InformationAcquisition
  virtual InformationAcquisitionInterface* GetInformationAcquisition() = 0;

  //! @brief Get FeatureExtractorInterface
  //! @return FeatureExtractor
  virtual FeatureExtractorInterface* GetFeatureExtractor() = 0;

  //! @brief Get MentalCalculationsInterface
  //! @return MentalCalculations
  virtual MentalCalculationsInterface* GetMentalCalculations() = 0;

  //! @brief Get ScmDependenciesInterface
  //! @return ScmDependencies
  virtual ScmDependenciesInterface* GetScmDependencies() = 0;

  //! @brief Get IgnoringOuterLaneOvertakingProhibitionInterface
  //! @return IgnoringOuterLaneOvertakingProhibition
  virtual IgnoringOuterLaneOvertakingProhibitionInterface* GetIgnoringOuterLaneOvertakingProhibition() = 0;

  //! @brief Get SwervingInterface
  //! @return SwervingInterface
  virtual SwervingInterface* GetSwerving() = 0;

  //! @brief Get LongitudinalCalculationsInterface
  //! @return LongitudinalCalculations
  virtual LongitudinalCalculationsInterface* GetLongitudinalCalculations() = 0;

  //! @brief Get GazeControlComponentsInterface
  //! @return GazeControlComponents
  virtual GazeControlComponentsInterface* GetGazeControlComponents() = 0;

  //! @brief Get ZipMergingInterface
  //! @return ZipMerging
  virtual ZipMergingInterface* GetZipMerging() = 0;
};
