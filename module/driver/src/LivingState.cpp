/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "LivingState.h"

LivingState::LivingState(const std::string& componentName,
                         units::time::millisecond_t cycleTime,
                         PeriodicLoggerInterface* pLog,
                         ScmComponentsInterface& scmComponents,
                         ExternalControlState externalControlState)
    : ScmLifetimeState(componentName, cycleTime, pLog, scmComponents, externalControlState)
{
}

void LivingState::UpdateMicroscopicData()
{
  _scmComponents->GetInformationAcquisition()->UpdateEgoVehicleData(false);
  _scmComponents->GetInformationAcquisition()->UpdateSurroundingVehicleData(false, _scmComponents->GetGazeControl()->IsNewInformationAcquisitionRequested());
  _scmComponents->GetMentalModel()->UpdateVisualReliabilityMap();

  ScmLifetimeState::UpdateMicroscopicData();
}

void LivingState::UpdateAgentInformation(units::time::millisecond_t time)
{
  _isNewEgoLane = _scmComponents->GetMentalModel()->CheckForEgoLaneTransition();
  ScmLifetimeState::UpdateAgentInformation(time);
}

void LivingState::DetermineAgentStates()
{
  if (_externalControlState.lateralActive)
    ResetLateralMovement();
  else
    CheckForEndOfLateralMovement();

  // EVALUATE SITUATION
  // update duration of situation every cycle time
  auto currentDurationSituation = _scmComponents->GetSituationManager()->GetCurrentSituationDuration() + _cycleTime;
  _scmComponents->GetSituationManager()->SetCurrentSituationDuration(currentDurationSituation);
  auto* mentalModel = _scmComponents->GetMentalModel();
  mentalModel->SetDurationCurrentSituation(static_cast<units::time::millisecond_t>(currentDurationSituation));

  // run high cognitive sub module calculations every time step
  // TODO: what about the gaze control -> plan situation one time step ahead?
  // TODO: what todo with HighCognitive when externalControl is active?
  mentalModel->TriggerHighCognitive(_externalControlState.lateralActive && _externalControlState.longitudinalActive);

  if (mentalModel->GetReactionBaseTime() <= 0._s)
  {
    mentalModel->CheckIsLaneChangeProhibitionIgnored();

    _scmComponents->GetSituationManager()->GenerateIntensitiesAndSampleSituation(*_scmComponents->GetZipMerging());

    // Check whether an active lane change is still safe needs to be decided before choosing the new actions
    // e.g. to make riskIntervention possible
    mentalModel->ReevaluateCurrentLaneChange();

    // EVALUATE ACTION SELECTION
    if (!_externalControlState.lateralActive)
    {
      _scmComponents->GetActionManager()->GenerateIntensityAndSampleLateralAction();
    }

    // determine the longitudinal action state according to the current situation
    if (!_externalControlState.longitudinalActive)
    {
      LateralActionQuery lateralActionQuery(mentalModel->GetLateralAction());
      if (mentalModel->GetMergeGap().IsValid() && !lateralActionQuery.IsLaneChanging())
      {
        mentalModel->ReevaluateMergeGap();
      }
      _scmComponents->GetActionManager()->DetermineLongitudinalActionState();
    }

    if (ShouldResetBaseTimes())
    {
      ResetBaseTimes();

      if (ShouldResetMerges())
      {
        ResetMerges();
      }
    }
  }

  HandleExternalControl();
}

bool LivingState::ShouldResetBaseTimes() const
{
  const auto* mentalModel = _scmComponents->GetMentalModel();
  return mentalModel->GetLateralAction() != _scmComponents->GetActionManager()->GetLateralActionLastTick() ||
         mentalModel->GetLongitudinalActionState() != _scmComponents->GetActionManager()->GetActionSubStateLastTick() ||
         mentalModel->GetHasSituationChanged() ||
         mentalModel->GetHasMesoscopicSituationChanged() ||
         ((mentalModel->GetLongitudinalActionState() == LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE ||
           mentalModel->GetLongitudinalActionState() == LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE ||
           mentalModel->GetLongitudinalActionState() == LongitudinalActionState::APPROACHING) &&
          mentalModel->HasLeadingVehicleChanged());
}

bool LivingState::ShouldResetMerges() const
{
  LateralActionQuery action(_scmComponents->GetMentalModel()->GetLateralAction());
  return !(action.IsPreparingToMerge() || action.IsLaneChanging() || action.IsIntentActive());
}
