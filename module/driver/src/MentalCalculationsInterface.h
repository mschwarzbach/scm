/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  MentalCalculationsInterface.h

#pragma once

#include "InfrastructureCharacteristicsInterface.h"
#include "LaneChangeBehaviorInterface.h"
#include "LaneChangeDimension.h"
#include "LateralActionQuery.h"
#include "LongitudinalCalculations/LongitudinalCalculationsInterface.h"
#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"

class MentalModelInterface;

//! @brief receives information from module InternalData and estimates the current situation based on the high cognitive process
//! @details This class receives information from MicroscopicCharacteristics and estimates the current situation
//! based on acquired information during high cognitive process
class MentalCalculationsInterface
{
public:
  virtual ~MentalCalculationsInterface() = default;

  //! @brief Initialize MentalCalculations object with mentalModel
  //! @param mentalModel
  //! @param queryFactory
  //! @param longitudinalCalculation
  virtual void Initialize(MentalModelInterface* mentalModel, SurroundingVehicleQueryFactoryInterface* queryFactory, const LongitudinalCalculationsInterface* longitudinalCalculation) = 0;

  //! @brief returns the value of time headway between two aoi's.
  //! @param aoi_first
  //! @param aoi_second
  //! @return THW
  virtual units::time::second_t GetTimeHeadwayBetweenTwoAreasOfInterest(AreaOfInterest aoi_first, AreaOfInterest aoi_second) const = 0;

  //! @brief returns equilibrium TauDot.
  //! @param aoi
  //! @return TauDotEq
  virtual double GetTauDotEq(AreaOfInterest aoi) const = 0;

  //! @brief Calculate delta to favoured velocity
  //! @return DeltaVWish
  virtual units::velocity::meters_per_second_t CalculateDeltaVelocityWish() const = 0;

  //! @brief Calculate the distance at which the agent starts to accelerate/brake to follow new speed limit.
  //! @param vEgo
  //! @param vTrafficSign
  //! @return Velocity legal influencing distance
  virtual units::length::meter_t CalculateVLegalInfluencigDistance(units::velocity::meters_per_second_t vEgo, units::velocity::meters_per_second_t vTrafficSign) const = 0;

  //! @brief Calculate legal velocity of all lanes
  //! @param spawn
  //! @return legal velocities for left, ego and right lane
  virtual std::vector<LaneInformationTrafficRulesScmExtended> CalculateVLegal(bool spawn) const = 0;

  //! @brief Transit of information from aoi specific internal data to another aoi destination in the internal data
  //! @param laneChangeState
  //! @param transitionTrigger
  virtual void TransitionOfEgoVehicle(LaneChangeState laneChangeState, bool transitionTrigger) = 0;

  //! @brief Check, if the vehicle is passing a slow platoon on either left or right side and calculates the velocity the driver would choose for passing a slow platoon.
  //! @return Velocity reason passing slow platoon
  virtual units::velocity::meters_per_second_t VReasonPassingSlowPlatoon() const = 0;

  //! @brief Calculate intensity for agent in left or right front aoi to change his lane due to end of lane.
  //! @param aoi
  //! @return Intensity for need to change lane
  virtual double GetIntensityForNeedToChangeLane(AreaOfInterest aoi) const = 0;

  //! @brief Determine, if in front of an aoi vehicle is a slower vehicle and consequently the driver would cross into another lane.
  //! @param aoi
  //! @return intensity
  virtual double GetIntensityForSlowerLeadingVehicle(AreaOfInterest aoi) const = 0;

  //! @brief Determine a distance between two agents for a safe following driving maneuver.
  //! @param aoi
  //! @param perspective
  //! @param aoiReference
  //! @param vGap
  //! @return minimum secure net distance between aoi and aoi reference agents
  virtual units::length::meter_t GetMinDistance(AreaOfInterest aoi,
                                                MinThwPerspective perspective = MinThwPerspective::NORM,
                                                AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
                                                units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const = 0;
  
  //! @brief Determine a distance between two agents for a safe following driving maneuver.
  //! @param vehicle
  //! @param perspective
  //! @param vGap
  //! @return minimum secure net distance between aoi and aoi reference agents
  virtual units::length::meter_t GetMinDistance(const SurroundingVehicleInterface* vehicle,
                                                MinThwPerspective perspective = MinThwPerspective::NORM,
                                                units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const = 0;

  //! @brief Determine the equilibrium distance between two aois
  //! @param aoi
  //! @param aoiReference
  //! @return Equilibrium distance
  virtual units::length::meter_t GetEqDistance(AreaOfInterest aoi, AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests) const = 0;

  //! @brief Determine the equilibrium distance etween ego and another Agent
  //! @param vehicle
  //! @return Equilibrium distance
  virtual units::length::meter_t GetEqDistance(const SurroundingVehicleInterface *vehicle) const = 0;

  //! @brief Determine the influencing distance between two aois
  //! @param aoi
  //! @param aoiReference
  //! @return Influencing distance
  virtual units::length::meter_t GetInfDistance(AreaOfInterest aoi, AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests) const = 0;

  //! @brief Determine the influencing distance between ego and another Agent
  //! @param vehicle
  //! @return Influencing distance
  virtual units::length::meter_t GetInfluencingDistance(const SurroundingVehicleInterface* vehicle) const = 0;
  
  //! @brief Get the remaining distance to the point of no return for a successful braking to an end of lane in m
  //! @param velocity
  //! @param distanceToEndOfLane
  //! @return Distance to point of no return for braking to end of lane
  virtual units::length::meter_t GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, units::length::meter_t distanceToEndOfLane) const = 0;

  //! @brief Get the remaining distance to the point of no return for a successful braking to an end of lane in m
  //! @param velocity
  //! @param relativeLane
  //! @return Distance to point of no return for braking to end of lane
  virtual units::length::meter_t GetDistanceToPointOfNoReturnForBrakingToEndOfLane(units::velocity::meters_per_second_t velocity, RelativeLane relativeLane) const = 0;
  
  //! @brief Calculate Urgency factor of a possible lane change.
  //! @return urgency factor of a lane change
  virtual double GetUrgencyFactorForLaneChange() const = 0;

  //! @brief Calculate urgency factor for accelerating due to anticipated lane changer, based on the current regulate vehicle.
  //! @return urgency factor for accelerating
  virtual double GetUrgencyFactorForAcceleration() const = 0;

  //! @brief Determines the lane marking between the observed lane and the ego lane at a certain area of interest
  //! @param aoi
  //! @return Longitudinal lane marking
  virtual scm::LaneMarking::Entity GetLaneMarkingAtAoi(AreaOfInterest aoi) const = 0;

  //! @brief Determines the lane marking between the observed lane and the ego lane at a certain distance
  //! @param distance    Distance at which the lane marking is supposed to be analyzed
  //! @param side        Left or right hand side lane marking observed
  //! @return Longitudinal lane marking
  virtual scm::LaneMarking::Entity GetLaneMarkingAtDistance(units::length::meter_t distance, Side side) const = 0;

  //! @brief Personal interpretation of the broken bold lane marking for the current situation
  //! @param distance     Distance at which the lane marking is supposed to be analyzed
  //! @param side         Left or right hand side lane marking observed
  //! @return Longitudinal lane marking
  virtual scm::LaneMarking::Entity InterpreteBrokenBoldLaneMarking(units::length::meter_t distance, Side side) const = 0;

  //! @brief Getter for the urgency factor for next exit manipulations.
  //! @param enableNewRouteRequest
  //! @param laneChangeDimension
  //! @return UrgencyFactor
  virtual double DetermineUrgencyFactorForNextExit(bool enableNewRouteRequest, const LaneChangeDimensionInterface& laneChangeDimension) const = 0;

  //! @brief Calculate the reached distance for a uniformly accelereted motion.
  //! @param time                 Given time in s
  //! @param acceleation          Given acceleration in m/s2
  //! @param startingVelocity     Starting velocity in m/s
  //! @param startingDistance     Starting distance in m
  //! @return Reached distance in m
  virtual units::length::meter_t CalculateReachedDistanceForUniformlyAcceleratedMotion(units::time::second_t time,
                                                                                       units::acceleration::meters_per_second_squared_t acceleation,
                                                                                       units::velocity::meters_per_second_t startingVelocity,
                                                                                       units::length::meter_t startingDistance) const = 0;

  //! @brief Calculate the reached velocity for a uniformly accelereted motion.
  //! @param time                 Given time in s
  //! @param acceleation          Given acceleration in m/s2
  //! @param startingVelocity     Starting velocity in m/s
  //! @return Reached velocity in m/s
  virtual units::velocity::meters_per_second_t CalculateReachedVelocityForUniformlyAcceleratedMotion(units::time::second_t time,
                                                                                                     units::acceleration::meters_per_second_squared_t acceleation,
                                                                                                     units::velocity::meters_per_second_t startingVelocity) const = 0;

  //! @brief Calculate the necessary time to perform a certain change in distance in a uniformly accelereted motion.
  //! @param acceleation          Given acceleration in m/s2
  //! @param startingVelocity     Starting velocity in m/s
  //! @param changeInDistance     Change in distance (end distance minus starting distance) in m
  //! @return Necessary time in s (INFINITY if change in distance is not possible)
  virtual units::time::second_t CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleation,
                                                                                                           units::velocity::meters_per_second_t startingVelocity,
                                                                                                           units::length::meter_t changeInDistance) const = 0;

  //! @brief Calculate the necessary time to perform a certain change in velocity in a uniformly accelereted motion.
  //! @param acceleation          Given acceleration in m/s2
  //! @param changeInVelocity     Change in velocity (end velocity minus starting velocity) in m/s
  //! @return Necessary time in s (INFINITY if change in velocity is not possible)
  virtual units::time::second_t CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion(units::acceleration::meters_per_second_squared_t acceleation,
                                                                                                           units::velocity::meters_per_second_t changeInVelocity) const = 0;
  //! @brief Calculate the scaled lateral offset in relation to velocity [m].
  //! @return Lateral offset scaled (>40kmh:lateralOffset 40-60kmh:lateralOffsetScaled <60kmh:0 )
  virtual units::length::meter_t CalculateLateralOffsetNeutralPositionScaled() const = 0;

  //! @brief Calculate acceleration to defuse
  //! @param aoi
  //! @param accelerationLimit maximum acceleration, the method can return (e.g. maxAcceleration)
  //! @return Acceleration to defuse
  virtual units::acceleration::meters_per_second_squared_t CalculateAccelerationToDefuse(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t accelerationLimit) const = 0;

  //! @brief Calculate lateral acceleration to defuse
  //! @param aoi             Area of interest
  //! @return Acceleration to defuse lateral
  virtual units::acceleration::meters_per_second_squared_t CalculateAccelerationToDefuseLateral(AreaOfInterest aoi) const = 0;

  //! @brief Determine, if the near vehicle is approaching the far vehicle.
  //! @param aoi
  //! @param aoiFar
  //! @return True if the near vehicle approaches the far one, false otherwise
  virtual bool IsApproaching(AreaOfInterest aoi, AreaOfInterest aoiFar) const = 0;

  //! @brief Check if the ego is driving below jam velocity.
  //! @return True if the ego speed is below jam velocity
  virtual bool EgoBelowJamSpeed() const = 0;

  //! @brief Returns the lane change behavior created in the Initialize function.
  //! @return Returns the lane change behavior created in the Initialize function.
  virtual LaneChangeBehaviorInterface& GetLaneChangeBehavior() const = 0;

  //! @brief Estimation due to the two assumptions of small heading angles (projected width == ego width) and constant current lateral velocity.
  //! @return time in seconds or INFINITY if not changing lanes.
  virtual units::time::second_t EstimatedTimeToLeaveLane() const = 0;

  //! @brief Returns the required acceleration to reach the equilibirum distance for the given leading vehicle.
  //! @param leadingVehicle  Current leading vehicle
  //! @param maxDeceleration Maximum deceleration to use when breaking is most appropriate
  //! @param deltaDistanceForAdjustment
  //! @return Calculated acceleration.
  virtual units::acceleration::meters_per_second_squared_t CalculateFollowingAcceleration(const SurroundingVehicleInterface& leadingVehicle,
                                                                                          units::acceleration::meters_per_second_squared_t maxDeceleration,
                                                                                          units::length::meter_t deltaDistanceForAdjustment = 0_m) const = 0;
};
