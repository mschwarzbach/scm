/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  InfrastructureCharacteristics.h

#pragma once

#include <limits>
#include <memory>
#include <optional>

#include "InfrastructureCharacteristicsInterface.h"
#include "ScmDefinitions.h"

//! @brief Implements the InfrastructureCharacteristicsInterface.
class InfrastructureCharacteristics : public InfrastructureCharacteristicsInterface
{
public:
  InfrastructureCharacteristics();
  InfrastructureCharacteristics(const InfrastructureCharacteristics&) = delete;
  InfrastructureCharacteristics(InfrastructureCharacteristics&&) = delete;
  InfrastructureCharacteristics& operator=(const InfrastructureCharacteristics&) = delete;
  InfrastructureCharacteristics& operator=(InfrastructureCharacteristics&&) = delete;
  virtual ~InfrastructureCharacteristics() = default;

  void Initialize(units::time::millisecond_t cycleTime, TrafficRuleInformationScmExtended* trafficRuleInformation = nullptr, GeometryInformationSCM* geometryInformation = nullptr) override;
  void SetLaneMarkingTypeLeftLast(scm::LaneMarking::Type laneTypeLeftLast) override;
  scm::LaneMarking::Type GetLaneMarkingTypeLeftLast() const override;
  void SetLaneMarkingTypeRightLast(scm::LaneMarking::Type laneTypeRightLast) override;
  scm::LaneMarking::Type GetLaneMarkingTypeRightLast() const override;
  void SetTrafficRuleInformation(TrafficRuleInformationSCM* trafficRuleInformation) override;
  void SetGeometryInformation(GeometryInformationSCM* geometryInformation, units::velocity::meters_per_second_t velocityEgoEstimated, bool takeNextExit, int laneIdEgo) override;
  const LaneInformationTrafficRulesScmExtended& GetLaneInformationTrafficRules(RelativeLane relativeLane) const override;
  LaneInformationTrafficRulesScmExtended* UpdateLaneInformationTrafficRules(RelativeLane relativeLane) override;
  TrafficRuleInformationScmExtended* GetTrafficRuleInformation() const override;
  GeometryInformationSCM* GetGeometryInformation() const override;
  LaneInformationGeometrySCM* UpdateLaneInformationGeometry(RelativeLane relativeLane) override;
  const LaneInformationGeometrySCM& GetLaneInformationGeometry(RelativeLane relativeLane) const override;
  TrafficRuleInformationScmExtended* UpdateTrafficRuleInformation() override;
  GeometryInformationSCM* UpdateGeometryInformation() override;
  bool GetLaneExistence(RelativeLane relativeLane, bool isEmergency = false) const override;
  bool CheckLaneExistence(AreaOfInterest aoi, bool isEmergency = true) const override;

protected:
  //! @brief Checks if the given traffic sign is a lane end announcement and the ego agent is on a relevant lane for that announcement.
  //! @param trafficSign Traffic sign to check
  //! @param laneIdEgo Lane id on which ego is located
  //! @param lane Surrounding lane that is checked
  //! @param numberOfLanes Number of lanes on the street
  //! @return true if the traffic sign is a relevant announcement
  static bool TrafficSignRelevantToEndOfLane(const scm::CommonTrafficSign::Entity& trafficSign, int laneIdEgo, SurroundingLane lane, int numberOfLanes);

  //! @brief Checks a vector of traffic signs for relevant lane ending signs and updates the _valuesLastTrafficSignEndOfLane if it finds a new distance for the given lane.
  //! @param trafficSigns Vector of traffic signs of a lane
  //! @param laneIdEgo Lane id on which ego is located
  //! @param lane Surrounding lane that is checked and updated
  //! @param numberOfLanes Number of lanes on the street
  //! @return std::optional<double> The new derived distance value in case a new sign was found, std::nullopt otherwise
  units::length::meter_t DeriveDistanceToEndOfLaneFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns, int laneIdEgo, SurroundingLane lane, int numberOfLanes);

  //! @brief Estimate numbers of lanes to change to reach exit lane.
  //! @param  geometryInformation Data struct containing road geometry information
  //! @return laneID
  virtual int EstimateRelativeLaneIdOfExitLane(GeometryInformationSCM* geometryInformation);

  //! @brief Evaluate traffic signs regarding distance to start of next exit or already existing information.
  //! @param trafficSigns     Traffic sign to check
  //! @return The new derived distance value in case a new sign was found
  units::length::meter_t DeriveDistanceToStartOfExitFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns);

  //! @brief Evaluate traffic signs regarding distance to end of next exit or already existing information.
  //! @param trafficSigns     Traffic sign to check
  //! @return The new derived distance value in case a new sign was found
  units::length::meter_t DeriveDistanceToEndOfExitFromTrafficSigns(const std::vector<scm::CommonTrafficSign::Entity>& trafficSigns);

  //! @brief Check traffic signs for additional information regarding end of a lane and handle mental data.
  //! @param lane                    Surrounding lane to evaluate
  //! @param geometryInformation     Data struct containing road geometry information
  //! @param velocityEgoEstimated    Estimated velocity of the ego agent
  //! @param laneIdEgo               Absolute Lane ID of the ego lane
  void UpdateDistanceToEndOfLane(SurroundingLane lane, GeometryInformationSCM* geometryInformation, units::velocity::meters_per_second_t velocityEgoEstimated, int laneIdEgo);

  //! @brief Check traffic signs for additional information regarding start and end of the next highway exit and handle mental data.
  //! @param geometryInformation     Data struct containing road geometry information
  //! @param velocityEgoEstimated    Estimated velocity of the ego agent
  void UpdateDistanceToStartAndEndOfNextExit(GeometryInformationSCM* geometryInformation, units::velocity::meters_per_second_t velocityEgoEstimated);

  //! @brief Calculate distance to start of exit
  //! @param distanceToStartOfExitGroundTruth
  //! @param distanceToStartOfExitLast
  //! @param velocityEgoEstimated    Estimated velocity of the ego agent
  //! @return distanceToStartOfExitNew
  virtual units::length::meter_t CalculateDistanceToStartOfExitNew(units::length::meter_t distanceToStartOfExitGroundTruth, units::length::meter_t distanceToStartOfExitLast, units::velocity::meters_per_second_t velocityEgoEstimated);

  units::length::meter_t _valueLastTrafficSignIndicatingStartOfExit{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t _valueLastTrafficSignIndicatingEndOfExit{ScmDefinitions::DEFAULT_VALUE};

  std::map<SurroundingLane, double> _valuesLastTrafficSignEndOfLane{{SurroundingLane::EGO, ScmDefinitions::DEFAULT_VALUE},
                                                                    {SurroundingLane::LEFT, ScmDefinitions::DEFAULT_VALUE},
                                                                    {SurroundingLane::RIGHT, ScmDefinitions::DEFAULT_VALUE}};
  units::time::millisecond_t _cycleTime{};

private:
  //! @brief Calculate distance to end of exit
  //! @param distanceToEndOfExitGroundTruth
  //! @param distanceToEndOfExitLast
  //! @param velocityEgoEstimated    Estimated velocity of the ego agent
  //! @return distanceToEndOfExitNew
  virtual units::length::meter_t CalculateDistanceToEndOfExitNew(units::length::meter_t distanceToEndOfExitGroundTruth, units::length::meter_t distanceToEndOfExitLast, units::velocity::meters_per_second_t velocityEgoEstimated);

  //! @brief Check if start of next exit can already be seen by the driver.
  //! @param distanceToEndOfExitGroundTruth
  //! @return Return true if start of next exit can already be seen by the driver
  virtual bool IsSeenEndOfExit(units::length::meter_t distanceToEndOfExitGroundTruth);

  //! @brief Check if end of next exit can already be seen by the driver.
  //! @param distanceToStartOfExitGroundTruth
  //! @return Return true if end of next exit can already be seen by the driver
  virtual bool IsSeenStartOfExit(units::length::meter_t distanceToStartOfExitGroundTruth);

  //! @brief Extrapolate distance to start or end of lane from earlier traffic sign information.
  //! @param distanceToExitLast      Distance to start or end of exit last
  //! @param velocityEgoEstimated    Estimated velocity of the ego agent
  //! @return Distance to start or end of exit extrapolated
  virtual units::length::meter_t ExtrapolateDistanceToExitFromEarlierTrafficSignInformation(units::length::meter_t distanceToExitLast, units::velocity::meters_per_second_t velocityEgoEstimated) const;

  std::unique_ptr<TrafficRuleInformationScmExtended> _trafficRuleInformation;
  std::unique_ptr<GeometryInformationSCM> _geometryInformation;

  bool _isInit{false};

  units::length::meter_t _distanceToEndOfNextExistGroundTruthLast{ScmDefinitions::DEFAULT_VALUE};

  scm::LaneMarking::Type _laneTypeLeftLast{scm::LaneMarking::Type::None};
  scm::LaneMarking::Type _laneTypeRightLast{scm::LaneMarking::Type::None};

  template <typename Method>
  auto* GetLaneInformation(Method method, RelativeLane relativeLane) const;
};
