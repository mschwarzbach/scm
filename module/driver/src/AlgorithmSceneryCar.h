/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  AlgorithmSceneryCar.h

#pragma once

#include "ActionImplementation.h"
#include "ActionImplementationInterface.h"
#include "ActionManager.h"
#include "ActionManagerInterface.h"
#include "AlgorithmSceneryCarInterface.h"

/*! \addtogroup AlgorithmSceneryCar
 * @{
 * \brief Calculates initial output variables of the AlgorithmScmMonolithischImplementation for a forced lane change.
 *
 * When the Manipulator sets a forced lane change this module will calculate the output variables for the first time
 * step. Safety distances and existence of target lane will not be considered for this approach. The following steps
 * after the initialisation of the lane change maneuver will be done by the regular algorithm.
 *
 * \section SceneryCar_Inputs Inputs
 * name | meaning
 * mentalModel          | Represents the mental processes of the stochastic cognitive model
 * actionManager        | Holds all functionality regarding action patterns
 * actionImplementation | Deduces action implementations from the stochastic cognitive model
 *
 * \section SceneryCar_Outputs
 * name | meaning
 * _resetReactionBaseTime   | Indicator to reset the reaction base time after a forced lane change has been issued
 *
 *
 */

class MentalModelInterface;
//! \ingroup AlgorithmSceneryCar
class AlgorithmSceneryCar : public AlgorithmSceneryCarInterface
{
public:
  //! \brief Constructor
  AlgorithmSceneryCar(MentalModelInterface& mentalModel);
  AlgorithmSceneryCar(const AlgorithmSceneryCar&) = delete;
  AlgorithmSceneryCar(AlgorithmSceneryCar&&) = delete;
  AlgorithmSceneryCar& operator=(const AlgorithmSceneryCar&) = delete;
  AlgorithmSceneryCar& operator=(AlgorithmSceneryCar&&) = delete;

  void ForcedLaneChange(ActionManagerInterface* actionManager,
                        ActionImplementationInterface* actionImplementation,
                        LaneChangeAction laneChangeAction) override;

  bool ResetReactionBaseTime() override;

protected:
  //! \brief
  //! \details
  //! \param [in] actionManager           Holds all functionality regarding action patterns
  //! \param [in] actionImplementation    Deduces action implementations from the stochastic cognitive model
  virtual void CorrectLaneChange(ActionManagerInterface* actionManager, ActionImplementationInterface* actionImplementation);

private:
  MentalModelInterface& _mentalModel;

  //! Previous lane change action of the agent.
  LaneChangeAction _laneChangeActionLastTick = LaneChangeAction::None;

  //! After a successful foced lane change initiation the reaction base time needs to be reset to zero. This variable
  //! indicates that this action is necessary.
  bool _resetReactionBaseTime = false;
};
/** @} */  // End of class