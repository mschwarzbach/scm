/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  HighCognitive.h

#pragma once

#include <deque>
#include <memory>
#include <random>
#include <stdexcept>
#include <string>
#include <vector>

#include "AnticipatedLaneChangerModel.h"
#include "ApproachFollowClassifier.h"
#include "ApproachSaliencyClassifier.h"
#include "FollowSaliencyClassifier.h"
#include "HighCognitiveInterface.h"
#include "LaneChangeTrajectories.h"
#include "MicroscopicCharacteristics.h"
#include "include/common/ScmEnums.h"

class MicroscopicCharacteristics;
class AnticipatedLaneChangerModel;
class MentalCalculationsInterface;

//! ***************************************************************************************************************
//! @brief This component holds functionality to recognize traffic situations,
//! determine gaze metrics to situation and to determine situation intensity.
//! @details Situations are only recognized inside AreaOfInterest::RIGHT_FRONT and
//! AreaOfInterest::RIGHT_FRONT_FAR. This class implements models for anticipation and prediction of lane changes.
//! ***************************************************************************************************************
class HighCognitive : public HighCognitiveInterface
{
public:
  //! @brief Constructor.
  //! @param MicroscopicCharacteristicsInterface* _microscopicData
  HighCognitive(MicroscopicCharacteristicsInterface* _microscopicData,
                MentalCalculationsInterface& mentalCalculations,
                const MentalModelInterface& mentalModel,
                bool isActive);

  HighCognitive(const HighCognitive&) = delete;
  HighCognitive(HighCognitive&&) = delete;
  HighCognitive& operator=(const HighCognitive&) = delete;
  HighCognitive& operator=(HighCognitive&&) = delete;

  bool RightFrontAndRightFrontFarAvailable(void) override;
  void AdvanceHighCognitive(int time, bool externalControlActive) override;
  double AnticipateLaneChangeProbability(int time) override;
  HighCognitiveSituation GetCurrentSituation(int time) const override;

private:
  //! @brief size of the moving average filter window, 10 Cycle
  static const int movingAverageMaxWindowSize = 10;  // Note: Adjust for cycle times other than 100 ms!
  //! @brief deque of observed situation pattern for moving average filtering
  std::deque<double> observedSituationPattern = {};
  //! @brief prevent HighCognitive from becoming advanced multiple times per time step and ensure that the requested data match the current time step.
  int advanceTime = 0;
  double anticipatedLaneChangeProbability{0.};
  //! @brief save the situation of the current time step
  HighCognitiveSituation currentHighCognitiveSituation{HighCognitiveSituation::UNDEFINED};

  //! @brief Interface to the MentalModel
  const MentalModelInterface& _mentalModel;

  MicroscopicCharacteristicsInterface* microscopicData = nullptr;
  MentalCalculationsInterface& mentalCalculations;
  std::unique_ptr<AnticipatedLaneChangerModel> anticipatedLaneChangerModel = nullptr;

  //! @brief Returns label of current situation pattern formed by two vehicle on the right lane
  //! @return int 0=normal approach, 1=salient approach, 2=normal follow, 3=salient follow, -1=no traffic situation
  virtual int DetermineCurrentSituationPattern(void);

  //! @brief Interface to insert a situationPatternLaben into the deque container
  //! @param situationLabel Label of the recognized situation (0=Approach, 1=Salient Approach, 2=Follow, 3=Salient Follow, 4=No TSP)
  void InsertPatternToDeque(int situationPatternLabel);

  //! @brief isHighCognitiveActive
  bool _isHighCognitiveActive;
};