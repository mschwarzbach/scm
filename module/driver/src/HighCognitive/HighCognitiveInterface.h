/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "include/common/ScmEnums.h"
class HighCognitiveInterface
{
public:
  virtual ~HighCognitiveInterface() = default;
  //! @brief Checks whether RightFront and RightFrontFar vehicles are present or not
  //! @return true if they're present
  virtual bool RightFrontAndRightFrontFarAvailable(void) = 0;

  //! @brief Determine the current HighCognitiveSituationPattern, set a current advanceTime and reset currentHighCognitiveSituation to NaN
  //! @param time
  //! @param externalControlActive
  virtual void AdvanceHighCognitive(int time, bool externalControlActive) = 0;

  //! @brief Returns probability of a lane change by the vehicle on the right lane next to the agent
  //! @param time
  //! @return double
  virtual double AnticipateLaneChangeProbability(int time) = 0;

  //! @brief Return the situation of the current time step. If this has not yet been determined, UNDEFINED is used.
  //! @param [in] time
  //! @return HighCognitiveSituation
  virtual HighCognitiveSituation GetCurrentSituation(int time) const = 0;
};