/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
 * MACHINE GENERATED FILE, MODIFY CAREFULLY
 ***********************************************/
#include <cmath>
#include <cstdio>
#include <cstdlib>

bool PredictFollowSaliency(float features[2])
{
  int classes[2];

  if (features[0] <= 0.06621350347995758)
  {
    if (features[1] <= 0.9952750205993652)
    {
      if (features[1] <= 0.9659199714660645)
      {
        if (features[0] <= 0.023019500076770782)
        {
          if (features[1] <= 0.8673499822616577)
          {
            if (features[1] <= 0.7671849727630615)
            {
              classes[0] = 0;
              classes[1] = 156;
            }
            else
            {
              if (features[0] <= -0.028549499809741974)
              {
                classes[0] = 1;
                classes[1] = 0;
              }
              else
              {
                if (features[1] <= 0.7698450088500977)
                {
                  classes[0] = 1;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 51;
                }
              }
            }
          }
          else
          {
            if (features[0] <= 0.009702799841761589)
            {
              classes[0] = 0;
              classes[1] = 33;
            }
            else
            {
              if (features[1] <= 0.948170006275177)
              {
                if (features[0] <= 0.02147100120782852)
                {
                  classes[0] = 4;
                  classes[1] = 0;
                }
                else
                {
                  if (features[1] <= 0.9226700067520142)
                  {
                    classes[0] = 0;
                    classes[1] = 1;
                  }
                  else
                  {
                    classes[0] = 1;
                    classes[1] = 0;
                  }
                }
              }
              else
              {
                classes[0] = 0;
                classes[1] = 1;
              }
            }
          }
        }
        else
        {
          classes[0] = 0;
          classes[1] = 116;
        }
      }
      else
      {
        if (features[1] <= 0.9822199940681458)
        {
          if (features[1] <= 0.9695349931716919)
          {
            if (features[1] <= 0.9691849946975708)
            {
              classes[0] = 1;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 1;
            }
          }
          else
          {
            classes[0] = 2;
            classes[1] = 0;
          }
        }
        else
        {
          classes[0] = 0;
          classes[1] = 6;
        }
      }
    }
    else
    {
      if (features[0] <= 0.007770049851387739)
      {
        if (features[1] <= 1.3075499534606934)
        {
          if (features[1] <= 1.3005499839782715)
          {
            if (features[0] <= -0.08635799586772919)
            {
              classes[0] = 0;
              classes[1] = 2;
            }
            else
            {
              if (features[1] <= 1.0975000858306885)
              {
                if (features[1] <= 1.0392500162124634)
                {
                  if (features[0] <= -0.02374649979174137)
                  {
                    classes[0] = 0;
                    classes[1] = 1;
                  }
                  else
                  {
                    classes[0] = 12;
                    classes[1] = 0;
                  }
                }
                else
                {
                  if (features[1] <= 1.0791000127792358)
                  {
                    if (features[1] <= 1.0460999011993408)
                    {
                      if (features[1] <= 1.0425000190734863)
                      {
                        classes[0] = 0;
                        classes[1] = 1;
                      }
                      else
                      {
                        classes[0] = 1;
                        classes[1] = 0;
                      }
                    }
                    else
                    {
                      if (features[0] <= -0.03403150290250778)
                      {
                        classes[0] = 1;
                        classes[1] = 0;
                      }
                      else
                      {
                        classes[0] = 0;
                        classes[1] = 8;
                      }
                    }
                  }
                  else
                  {
                    if (features[1] <= 1.0909500122070312)
                    {
                      classes[0] = 2;
                      classes[1] = 0;
                    }
                    else
                    {
                      classes[0] = 0;
                      classes[1] = 1;
                    }
                  }
                }
              }
              else
              {
                if (features[1] <= 1.2924000024795532)
                {
                  if (features[1] <= 1.1862499713897705)
                  {
                    if (features[1] <= 1.1858000755310059)
                    {
                      if (features[1] <= 1.1317999362945557)
                      {
                        classes[0] = 9;
                        classes[1] = 0;
                      }
                      else
                      {
                        if (features[1] <= 1.1333500146865845)
                        {
                          classes[0] = 0;
                          classes[1] = 2;
                        }
                        else
                        {
                          if (features[1] <= 1.1450999975204468)
                          {
                            classes[0] = 8;
                            classes[1] = 0;
                          }
                          else
                          {
                            if (features[1] <= 1.1468499898910522)
                            {
                              classes[0] = 0;
                              classes[1] = 2;
                            }
                            else
                            {
                              if (features[0] <= 0.0031484500505030155)
                              {
                                classes[0] = 12;
                                classes[1] = 0;
                              }
                              else
                              {
                                if (features[0] <= 0.005558100063353777)
                                {
                                  classes[0] = 0;
                                  classes[1] = 1;
                                }
                                else
                                {
                                  classes[0] = 1;
                                  classes[1] = 0;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                    else
                    {
                      classes[0] = 0;
                      classes[1] = 2;
                    }
                  }
                  else
                  {
                    if (features[0] <= -0.014465000480413437)
                    {
                      if (features[0] <= -0.018653500825166702)
                      {
                        if (features[0] <= -0.031127501279115677)
                        {
                          classes[0] = 13;
                          classes[1] = 0;
                        }
                        else
                        {
                          if (features[1] <= 1.2683500051498413)
                          {
                            classes[0] = 8;
                            classes[1] = 0;
                          }
                          else
                          {
                            if (features[0] <= -0.024770500138401985)
                            {
                              classes[0] = 0;
                              classes[1] = 1;
                            }
                            else
                            {
                              classes[0] = 2;
                              classes[1] = 0;
                            }
                          }
                        }
                      }
                      else
                      {
                        classes[0] = 0;
                        classes[1] = 1;
                      }
                    }
                    else
                    {
                      classes[0] = 15;
                      classes[1] = 0;
                    }
                  }
                }
                else
                {
                  if (features[1] <= 1.293299913406372)
                  {
                    classes[0] = 0;
                    classes[1] = 1;
                  }
                  else
                  {
                    if (features[0] <= -0.026153000071644783)
                    {
                      classes[0] = 0;
                      classes[1] = 1;
                    }
                    else
                    {
                      classes[0] = 3;
                      classes[1] = 0;
                    }
                  }
                }
              }
            }
          }
          else
          {
            classes[0] = 0;
            classes[1] = 3;
          }
        }
        else
        {
          if (features[0] <= -0.07025499641895294)
          {
            if (features[0] <= -0.0775275006890297)
            {
              classes[0] = 1;
              classes[1] = 0;
            }
            else
            {
              if (features[0] <= -0.07468800246715546)
              {
                classes[0] = 0;
                classes[1] = 1;
              }
              else
              {
                if (features[0] <= -0.0719740018248558)
                {
                  classes[0] = 1;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 1;
                }
              }
            }
          }
          else
          {
            if (features[0] <= -0.03950100019574165)
            {
              if (features[1] <= 2.1187000274658203)
              {
                if (features[0] <= -0.04525099694728851)
                {
                  if (features[1] <= 2.0590500831604004)
                  {
                    classes[0] = 27;
                    classes[1] = 0;
                  }
                  else
                  {
                    classes[0] = 0;
                    classes[1] = 2;
                  }
                }
                else
                {
                  if (features[1] <= 1.8909000158309937)
                  {
                    classes[0] = 7;
                    classes[1] = 0;
                  }
                  else
                  {
                    classes[0] = 0;
                    classes[1] = 6;
                  }
                }
              }
              else
              {
                classes[0] = 36;
                classes[1] = 0;
              }
            }
            else
            {
              if (features[1] <= 4.87030029296875)
              {
                if (features[1] <= 2.151750087738037)
                {
                  if (features[1] <= 2.143350124359131)
                  {
                    classes[0] = 229;
                    classes[1] = 0;
                  }
                  else
                  {
                    classes[0] = 0;
                    classes[1] = 1;
                  }
                }
                else
                {
                  classes[0] = 276;
                  classes[1] = 0;
                }
              }
              else
              {
                if (features[1] <= 5.094399929046631)
                {
                  classes[0] = 0;
                  classes[1] = 4;
                }
                else
                {
                  classes[0] = 10;
                  classes[1] = 0;
                }
              }
            }
          }
        }
      }
      else
      {
        if (features[1] <= 4.670949935913086)
        {
          if (features[1] <= 1.2644500732421875)
          {
            if (features[1] <= 1.1401500701904297)
            {
              if (features[1] <= 1.0819000005722046)
              {
                if (features[0] <= 0.019085999578237534)
                {
                  classes[0] = 0;
                  classes[1] = 9;
                }
                else
                {
                  if (features[0] <= 0.03623500093817711)
                  {
                    classes[0] = 11;
                    classes[1] = 0;
                  }
                  else
                  {
                    classes[0] = 0;
                    classes[1] = 7;
                  }
                }
              }
              else
              {
                classes[0] = 19;
                classes[1] = 0;
              }
            }
            else
            {
              if (features[1] <= 1.1719999313354492)
              {
                classes[0] = 0;
                classes[1] = 8;
              }
              else
              {
                if (features[0] <= 0.030496999621391296)
                {
                  if (features[0] <= 0.01783899962902069)
                  {
                    if (features[1] <= 1.1842999458312988)
                    {
                      classes[0] = 0;
                      classes[1] = 2;
                    }
                    else
                    {
                      if (features[0] <= 0.015372000634670258)
                      {
                        classes[0] = 1;
                        classes[1] = 0;
                      }
                      else
                      {
                        classes[0] = 0;
                        classes[1] = 2;
                      }
                    }
                  }
                  else
                  {
                    classes[0] = 7;
                    classes[1] = 0;
                  }
                }
                else
                {
                  if (features[0] <= 0.04159900173544884)
                  {
                    classes[0] = 0;
                    classes[1] = 10;
                  }
                  else
                  {
                    if (features[1] <= 1.2202999591827393)
                    {
                      classes[0] = 0;
                      classes[1] = 1;
                    }
                    else
                    {
                      classes[0] = 2;
                      classes[1] = 0;
                    }
                  }
                }
              }
            }
          }
          else
          {
            if (features[0] <= 0.048618000000715256)
            {
              if (features[1] <= 1.557300090789795)
              {
                if (features[1] <= 1.4805500507354736)
                {
                  classes[0] = 55;
                  classes[1] = 0;
                }
                else
                {
                  if (features[1] <= 1.5460000038146973)
                  {
                    if (features[0] <= 0.01813950017094612)
                    {
                      classes[0] = 0;
                      classes[1] = 6;
                    }
                    else
                    {
                      classes[0] = 16;
                      classes[1] = 0;
                    }
                  }
                  else
                  {
                    if (features[0] <= 0.018998999148607254)
                    {
                      classes[0] = 0;
                      classes[1] = 4;
                    }
                    else
                    {
                      classes[0] = 1;
                      classes[1] = 0;
                    }
                  }
                }
              }
              else
              {
                if (features[1] <= 2.6210999488830566)
                {
                  if (features[0] <= 0.03700850158929825)
                  {
                    if (features[0] <= 0.027371499687433243)
                    {
                      if (features[1] <= 2.4629998207092285)
                      {
                        if (features[0] <= 0.01566999964416027)
                        {
                          if (features[0] <= 0.015598500147461891)
                          {
                            classes[0] = 37;
                            classes[1] = 0;
                          }
                          else
                          {
                            classes[0] = 0;
                            classes[1] = 1;
                          }
                        }
                        else
                        {
                          classes[0] = 65;
                          classes[1] = 0;
                        }
                      }
                      else
                      {
                        if (features[1] <= 2.4707999229431152)
                        {
                          classes[0] = 0;
                          classes[1] = 4;
                        }
                        else
                        {
                          classes[0] = 16;
                          classes[1] = 0;
                        }
                      }
                    }
                    else
                    {
                      classes[0] = 54;
                      classes[1] = 0;
                    }
                  }
                  else
                  {
                    if (features[1] <= 2.6110000610351562)
                    {
                      if (features[0] <= 0.037225499749183655)
                      {
                        classes[0] = 0;
                        classes[1] = 2;
                      }
                      else
                      {
                        if (features[1] <= 1.7390499114990234)
                        {
                          if (features[0] <= 0.04034300148487091)
                          {
                            if (features[1] <= 1.6868499517440796)
                            {
                              classes[0] = 1;
                              classes[1] = 0;
                            }
                            else
                            {
                              classes[0] = 0;
                              classes[1] = 2;
                            }
                          }
                          else
                          {
                            classes[0] = 14;
                            classes[1] = 0;
                          }
                        }
                        else
                        {
                          classes[0] = 30;
                          classes[1] = 0;
                        }
                      }
                    }
                    else
                    {
                      classes[0] = 0;
                      classes[1] = 2;
                    }
                  }
                }
                else
                {
                  classes[0] = 87;
                  classes[1] = 0;
                }
              }
            }
            else
            {
              if (features[1] <= 1.7449500560760498)
              {
                if (features[0] <= 0.05298849940299988)
                {
                  if (features[0] <= 0.049461498856544495)
                  {
                    classes[0] = 0;
                    classes[1] = 2;
                  }
                  else
                  {
                    if (features[0] <= 0.04970249906182289)
                    {
                      classes[0] = 1;
                      classes[1] = 0;
                    }
                    else
                    {
                      if (features[1] <= 1.6196999549865723)
                      {
                        classes[0] = 0;
                        classes[1] = 6;
                      }
                      else
                      {
                        classes[0] = 1;
                        classes[1] = 0;
                      }
                    }
                  }
                }
                else
                {
                  if (features[1] <= 1.6337499618530273)
                  {
                    classes[0] = 12;
                    classes[1] = 0;
                  }
                  else
                  {
                    classes[0] = 0;
                    classes[1] = 4;
                  }
                }
              }
              else
              {
                if (features[0] <= 0.04935599863529205)
                {
                  classes[0] = 0;
                  classes[1] = 4;
                }
                else
                {
                  if (features[1] <= 2.3285000324249268)
                  {
                    classes[0] = 30;
                    classes[1] = 0;
                  }
                  else
                  {
                    if (features[0] <= 0.0547109991312027)
                    {
                      if (features[1] <= 2.676800012588501)
                      {
                        classes[0] = 0;
                        classes[1] = 4;
                      }
                      else
                      {
                        classes[0] = 2;
                        classes[1] = 0;
                      }
                    }
                    else
                    {
                      if (features[0] <= 0.06268049776554108)
                      {
                        if (features[0] <= 0.061363499611616135)
                        {
                          classes[0] = 5;
                          classes[1] = 0;
                        }
                        else
                        {
                          classes[0] = 0;
                          classes[1] = 2;
                        }
                      }
                      else
                      {
                        classes[0] = 6;
                        classes[1] = 0;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        else
        {
          classes[0] = 0;
          classes[1] = 45;
        }
      }
    }
  }
  else
  {
    if (features[0] <= 0.08775900304317474)
    {
      if (features[0] <= 0.08424399793148041)
      {
        if (features[0] <= 0.0792279988527298)
        {
          if (features[0] <= 0.07860149443149567)
          {
            if (features[1] <= 1.260949969291687)
            {
              classes[0] = 0;
              classes[1] = 25;
            }
            else
            {
              if (features[1] <= 1.7240999937057495)
              {
                classes[0] = 5;
                classes[1] = 0;
              }
              else
              {
                if (features[0] <= 0.0704599991440773)
                {
                  if (features[1] <= 2.2146501541137695)
                  {
                    classes[0] = 0;
                    classes[1] = 6;
                  }
                  else
                  {
                    if (features[1] <= 2.26924991607666)
                    {
                      classes[0] = 1;
                      classes[1] = 0;
                    }
                    else
                    {
                      if (features[0] <= 0.06759600341320038)
                      {
                        classes[0] = 0;
                        classes[1] = 4;
                      }
                      else
                      {
                        classes[0] = 1;
                        classes[1] = 0;
                      }
                    }
                  }
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 14;
                }
              }
            }
          }
          else
          {
            classes[0] = 2;
            classes[1] = 0;
          }
        }
        else
        {
          classes[0] = 0;
          classes[1] = 18;
        }
      }
      else
      {
        if (features[0] <= 0.08476150035858154)
        {
          classes[0] = 2;
          classes[1] = 0;
        }
        else
        {
          if (features[1] <= 1.5022300481796265)
          {
            classes[0] = 0;
            classes[1] = 4;
          }
          else
          {
            if (features[1] <= 2.749849796295166)
            {
              classes[0] = 3;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 2;
            }
          }
        }
      }
    }
    else
    {
      classes[0] = 0;
      classes[1] = 564;
    }
  }

  int index = 0;
  for (int i = 0; i < 2; i++)
  {
    index = classes[i] > classes[index] ? i : index;
  }
  if (index == 0)
  {
    return false;
  }
  else
  {
    return true;
  }
}
