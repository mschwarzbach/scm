/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
 * MACHINE GENERATED FILE, MODIFY CAREFULLY
 ***********************************************/
#include <cmath>
#include <cstdio>
#include <cstdlib>

bool PredictApproachSaliency(float features[2])
{
  int classes[2];

  if (features[1] <= 0.741569995880127)
  {
    if (features[0] <= 0.05365400016307831)
    {
      if (features[0] <= 0.053604498505592346)
      {
        if (features[0] <= 0.01522199995815754)
        {
          if (features[1] <= 0.6320300102233887)
          {
            classes[0] = 0;
            classes[1] = 22;
          }
          else
          {
            classes[0] = 7;
            classes[1] = 0;
          }
        }
        else
        {
          if (features[0] <= 0.030583500862121582)
          {
            if (features[0] <= 0.022789999842643738)
            {
              if (features[0] <= 0.02254049852490425)
              {
                if (features[1] <= 0.6833349466323853)
                {
                  classes[0] = 0;
                  classes[1] = 38;
                }
                else
                {
                  classes[0] = 2;
                  classes[1] = 0;
                }
              }
              else
              {
                classes[0] = 1;
                classes[1] = 0;
              }
            }
            else
            {
              if (features[0] <= 0.02831150032579899)
              {
                classes[0] = 0;
                classes[1] = 63;
              }
              else
              {
                if (features[0] <= 0.028432998806238174)
                {
                  classes[0] = 1;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 42;
                }
              }
            }
          }
          else
          {
            if (features[1] <= 0.7037950158119202)
            {
              classes[0] = 0;
              classes[1] = 440;
            }
            else
            {
              if (features[0] <= 0.04276300221681595)
              {
                classes[0] = 21;
                classes[1] = 0;
              }
              else
              {
                if (features[0] <= 0.04335299879312515)
                {
                  classes[0] = 0;
                  classes[1] = 1;
                }
                else
                {
                  classes[0] = 20;
                  classes[1] = 0;
                }
              }
            }
          }
        }
      }
      else
      {
        classes[0] = 1;
        classes[1] = 0;
      }
    }
    else
    {
      if (features[0] <= 0.07276149839162827)
      {
        if (features[1] <= 0.726984977722168)
        {
          classes[0] = 0;
          classes[1] = 323;
        }
        else
        {
          classes[0] = 12;
          classes[1] = 0;
        }
      }
      else
      {
        if (features[1] <= 0.7275699973106384)
        {
          classes[0] = 0;
          classes[1] = 3660;
        }
        else
        {
          if (features[0] <= 0.08053499460220337)
          {
            if (features[0] <= 0.078404501080513)
            {
              classes[0] = 0;
              classes[1] = 1;
            }
            else
            {
              classes[0] = 1;
              classes[1] = 0;
            }
          }
          else
          {
            classes[0] = 0;
            classes[1] = 182;
          }
        }
      }
    }
  }
  else
  {
    if (features[1] <= 2.048150062561035)
    {
      if (features[1] <= 0.9918750524520874)
      {
        if (features[0] <= 0.09502850472927094)
        {
          if (features[0] <= 0.09387250244617462)
          {
            if (features[0] <= 0.004680600017309189)
            {
              if (features[0] <= 0.0045310501009225845)
              {
                if (features[1] <= 0.7775449752807617)
                {
                  classes[0] = 9;
                  classes[1] = 1;
                }
                else
                {
                  classes[0] = 34;
                  classes[1] = 0;
                }
              }
              else
              {
                classes[0] = 0;
                classes[1] = 1;
              }
            }
            else
            {
              if (features[1] <= 0.7426750063896179)
              {
                if (features[0] <= 0.07715100049972534)
                {
                  classes[0] = 9;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 1;
                }
              }
              else
              {
                if (features[1] <= 0.7971149682998657)
                {
                  classes[0] = 485;
                  classes[1] = 3;
                }
                else
                {
                  classes[0] = 2464;
                  classes[1] = 0;
                }
              }
            }
          }
          else
          {
            if (features[1] <= 0.7828199863433838)
            {
              classes[0] = 0;
              classes[1] = 2;
            }
            else
            {
              classes[0] = 8;
              classes[1] = 0;
            }
          }
        }
        else
        {
          if (features[1] <= 0.9403649568557739)
          {
            if (features[1] <= 0.9086300134658813)
            {
              classes[0] = 0;
              classes[1] = 1906;
            }
            else
            {
              if (features[1] <= 0.9087499976158142)
              {
                classes[0] = 1;
                classes[1] = 0;
              }
              else
              {
                classes[0] = 0;
                classes[1] = 361;
              }
            }
          }
          else
          {
            if (features[0] <= 0.11416500061750412)
            {
              classes[0] = 33;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 547;
            }
          }
        }
      }
      else
      {
        if (features[0] <= 0.11882999539375305)
        {
          if (features[1] <= 1.0639500617980957)
          {
            if (features[0] <= 0.1121399998664856)
            {
              classes[0] = 1382;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 2;
            }
          }
          else
          {
            classes[0] = 9918;
            classes[1] = 0;
          }
        }
        else
        {
          if (features[1] <= 1.2300500869750977)
          {
            if (features[1] <= 1.1974499225616455)
            {
              if (features[0] <= 0.12435999512672424)
              {
                if (features[0] <= 0.12421000003814697)
                {
                  classes[0] = 0;
                  classes[1] = 20;
                }
                else
                {
                  classes[0] = 1;
                  classes[1] = 1;
                }
              }
              else
              {
                classes[0] = 0;
                classes[1] = 1770;
              }
            }
            else
            {
              if (features[1] <= 1.19760000705719)
              {
                if (features[0] <= 0.14540499448776245)
                {
                  classes[0] = 1;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 0;
                  classes[1] = 2;
                }
              }
              else
              {
                if (features[1] <= 1.201550006866455)
                {
                  classes[0] = 1;
                  classes[1] = 35;
                }
                else
                {
                  classes[0] = 1;
                  classes[1] = 262;
                }
              }
            }
          }
          else
          {
            if (features[0] <= 0.14140000939369202)
            {
              if (features[1] <= 1.5748000144958496)
              {
                if (features[1] <= 1.2411500215530396)
                {
                  classes[0] = 3;
                  classes[1] = 11;
                }
                else
                {
                  classes[0] = 64;
                  classes[1] = 18;
                }
              }
              else
              {
                if (features[1] <= 1.6173499822616577)
                {
                  classes[0] = 0;
                  classes[1] = 9;
                }
                else
                {
                  classes[0] = 11;
                  classes[1] = 23;
                }
              }
            }
            else
            {
              classes[0] = 0;
              classes[1] = 3905;
            }
          }
        }
      }
    }
    else
    {
      if (features[1] <= 2.6262500286102295)
      {
        if (features[0] <= 0.11977000534534454)
        {
          if (features[1] <= 2.060149908065796)
          {
            if (features[1] <= 2.05964994430542)
            {
              classes[0] = 24;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 1;
            }
          }
          else
          {
            if (features[0] <= 0.11408999562263489)
            {
              classes[0] = 603;
              classes[1] = 0;
            }
            else
            {
              if (features[0] <= 0.11608999967575073)
              {
                classes[0] = 0;
                classes[1] = 1;
              }
              else
              {
                classes[0] = 5;
                classes[1] = 0;
              }
            }
          }
        }
        else
        {
          if (features[0] <= 0.1412149965763092)
          {
            if (features[0] <= 0.14072000980377197)
            {
              if (features[0] <= 0.13290999829769135)
              {
                if (features[1] <= 2.1406497955322266)
                {
                  classes[0] = 1;
                  classes[1] = 0;
                }
                else
                {
                  classes[0] = 1;
                  classes[1] = 9;
                }
              }
              else
              {
                classes[0] = 0;
                classes[1] = 29;
              }
            }
            else
            {
              classes[0] = 2;
              classes[1] = 0;
            }
          }
          else
          {
            classes[0] = 0;
            classes[1] = 855;
          }
        }
      }
      else
      {
        if (features[1] <= 3.381999969482422)
        {
          if (features[1] <= 2.9126501083374023)
          {
            if (features[0] <= 0.1072700023651123)
            {
              classes[0] = 30;
              classes[1] = 0;
            }
            else
            {
              classes[0] = 0;
              classes[1] = 311;
            }
          }
          else
          {
            if (features[0] <= 0.10786999762058258)
            {
              if (features[1] <= 3.031450033187866)
              {
                classes[0] = 5;
                classes[1] = 0;
              }
              else
              {
                if (features[1] <= 3.107300043106079)
                {
                  classes[0] = 0;
                  classes[1] = 1;
                }
                else
                {
                  classes[0] = 3;
                  classes[1] = 0;
                }
              }
            }
            else
            {
              classes[0] = 0;
              classes[1] = 269;
            }
          }
        }
        else
        {
          if (features[0] <= 0.1050879955291748)
          {
            classes[0] = 14;
            classes[1] = 0;
          }
          else
          {
            if (features[0] <= 0.1257449984550476)
            {
              if (features[0] <= 0.12110000103712082)
              {
                classes[0] = 0;
                classes[1] = 3;
              }
              else
              {
                classes[0] = 1;
                classes[1] = 0;
              }
            }
            else
            {
              classes[0] = 0;
              classes[1] = 46;
            }
          }
        }
      }
    }
  }

  int index = 0;
  for (int i = 0; i < 2; i++)
  {
    index = classes[i] > classes[index] ? i : index;
  }

  if (index == 0)
  {
    return false;
  }
  else
  {
    return true;
  }
}
