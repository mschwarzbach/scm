/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneChangeTrajectories.h
#pragma once

#include <array>
#include <vector>

//! @brief An enum to access specific vectors from LaneChangeTrajectories.
//!
//! E.g. LaneChangeTrajectories[200][1][TrajectoryMetrics::s]
//!                                                           ↑   ↑           ↑
//!                                                           |   |            |
//!                       No. of Trajectory ---┘   |            └--- Specific Metric
//!                                        Time Step ---┘
enum TrajectoryMetrics
{
  s = 0,  // relative x-position in respect to EGO
  y = 1   // relative y-position in respect to EGO
};

//! @brief LaneChangeTrajectories[No of Trajectory][Time Step][Specific Element]
extern std::vector<std::vector<std::vector<double>>> LaneChangeTrajectories;
