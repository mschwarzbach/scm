/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "AnticipatedLaneChangerModel.h"

#include "MentalCalculationsInterface.h"

AnticipatedLaneChangerModel::AnticipatedLaneChangerModel(MicroscopicCharacteristicsInterface* microscopicData,
                                                         MentalCalculationsInterface& mentalCalculations,
                                                         const MentalModelInterface& mentalModel)
    : _mentalCalculations{mentalCalculations}, _mentalModel(mentalModel)
{
  _microscopicData = microscopicData;
}

double AnticipatedLaneChangerModel::GetLaneChangeProbability()
{
  double laneChangeLikelihood;
  double generalEvidenceProbability;

  if (!CheckMetricsAvailable())
  {
    return 0.0;
  }

  try
  {
    if (_microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT)->exist ||
        _microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT_FAR)->exist)
    {
      laneChangeLikelihood = GetLaneChangeEvidenceLikelihoodWithEgoFrontVehicle();
      generalEvidenceProbability = GetGeneralEvidenceProbabilityWithEgoFrontVehicle();
    }
    else
    {
      laneChangeLikelihood = GetLaneChangeEvidenceLikelihoodWithoutEgoFrontVehicle();
      generalEvidenceProbability = GetGeneralEvidenceProbabilityWithoutEgoFrontVehicle();
    }

    if (generalEvidenceProbability > 0.0)
    {
      const double bayesianPosterior{1.0 - laneChangeLikelihood / generalEvidenceProbability};
      return GetLogisticRegressionWeight(bayesianPosterior) * bayesianPosterior;
    }
    else
    {
      return 0.0;
    }
  }
  catch (std::out_of_range& oor)
  {
    std::cerr << "Out of Range error: " << oor.what() << '\n';
    return 0.0;
  }
}

double AnticipatedLaneChangerModel::GetLaneChangeEvidenceLikelihoodWithEgoFrontVehicle()
{
  double evidenceLikelihood = 1.0;
  double likelihoodFromDistribution = 0.0;
  UpdateMetricsArray();
  int key = 0;

  for (int i = 0; i < 7; i++)
  {
    key = GetMapKeyFromValue(metricsArray[i]);
    likelihoodFromDistribution = EvidenceObservationLikelihoodDistributionLaneChangeWithEgoFrontVehicle.at(key)[i];
    evidenceLikelihood *= likelihoodFromDistribution * PRIOR_LANE_CHANGE_WITH_EGO_FRONT_VEHICLE;
  }

  return evidenceLikelihood;
}

double AnticipatedLaneChangerModel::GetGeneralEvidenceProbabilityWithEgoFrontVehicle()
{
  double evidenceProbability = 1.0;
  double likelihoodFromDistribution = 0.0;
  UpdateMetricsArray();
  int key = 0;

  for (int i = 0; i < 7; i++)
  {
    key = GetMapKeyFromValue(metricsArray[i]);
    likelihoodFromDistribution = EvidenceObservationProbabilityDistributionGeneralWithEgoFrontVehicle.at(key)[i];
    evidenceProbability *= likelihoodFromDistribution;
  }

  return evidenceProbability;
}

double AnticipatedLaneChangerModel::GetLaneChangeEvidenceLikelihoodWithoutEgoFrontVehicle()
{
  double evidenceLikelihood = 1.0;
  double likelihoodFromDistribution = 0.0;
  UpdateMetricsArray();
  int key = 0;

  for (int i = 0; i < 5; i++)
  {
    key = GetMapKeyFromValue(metricsArray[i]);
    likelihoodFromDistribution = EvidenceObservationLikelihoodDistributionLaneChangeWithoutEgoFrontVehicle.at(key)[i];
    evidenceLikelihood *= likelihoodFromDistribution * PRIOR_LANE_CHANGE_WITHOUT_EGO_FRONT_VEHICLE;
  }

  return evidenceLikelihood;
}

double AnticipatedLaneChangerModel::GetGeneralEvidenceProbabilityWithoutEgoFrontVehicle()
{
  double evidenceProbability = 1.0;
  double likelihoodFromDistribution = 0.0;
  UpdateMetricsArray();
  int key = 0;

  for (int i = 0; i < 5; i++)
  {
    key = GetMapKeyFromValue(metricsArray[i]);
    likelihoodFromDistribution = EvidenceObservationProbabilityDistributionGeneralWithoutEgoFrontVehicle.at(key)[i];
    evidenceProbability *= likelihoodFromDistribution;
  }

  return evidenceProbability;
}

double AnticipatedLaneChangerModel::GetLogisticRegressionWeight(double bayesianPosterior)
{
  double regressionFactor = exp(-BETA_0 - bayesianPosterior * BETA_1);
  return regressionFactor / (1.0 + regressionFactor);
}

bool AnticipatedLaneChangerModel::CheckMetricsAvailable()
{
  UpdateMetricsArray();
  for (int index = 0; index < 7; index++)
  {
    if (std::isnan(metricsArray[index]))
      return false;
  }
  return true;
}

int AnticipatedLaneChangerModel::GetMapKeyFromValue(double value)
{
  // If value is not valid return 1.0
  if (std::isnan(value) || std::isinf(value))
    return 1;

  // applying offset to map values between -1 and 3
  // so -1 is mapped to the index 0 and 3 to index 4001
  // because of language constraints in c++
  value = value + 1.0;

  // check index violation
  if (value <= 0.0)
  {
    return MIN_LOOKUP_INDEX;
  }
  else if (value >= 4.0)
  {
    return MAX_LOOKUP_INDEX;
  }
  else
  {
    return static_cast<int>(value / TABLE_RESOLUTION) + 1;
  }
  return 1;
}

double AnticipatedLaneChangerModel::GetTtcInverseRightFrontToRightFrontFar()
{
  const double ttcInverseRightFrontToRightFrontFar{GetTtcInverseBetweenTwoAreasOfInterest(AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR)};

  if (ttcInverseRightFrontToRightFrontFar == INFINITY)
  {
    return std::nan("");
  }

  return ttcInverseRightFrontToRightFrontFar;
}

void AnticipatedLaneChangerModel::UpdateMetricsArray()
{
  metricsArray[0] = GetThwRightFront();
  metricsArray[1] = GetThwDotRightFront();
  metricsArray[2] = GetTtcInverseRightFrontToRightFrontFar();
  metricsArray[3] = GetTtcInverseRightFront();
  metricsArray[4] = GetTtcInverseDotRightFront();
  metricsArray[5] = GetTtcInverseEgoFront();
  metricsArray[6] = GetTtcInverseDotEgoFront();
}

double AnticipatedLaneChangerModel::GetThwRightFront()
{
  const double thwRightFront{_microscopicData->GetObjectInformation(AreaOfInterest::RIGHT_FRONT)->gap};

  if (thwRightFront < 0.0 || thwRightFront > 999.0)
  {
    return std::nan("");
  }

  return thwRightFront;
}

double AnticipatedLaneChangerModel::GetTtcInverseRightFront()
{
  const double ttcRightFront{_microscopicData->GetObjectInformation(AreaOfInterest::RIGHT_FRONT)->ttc};

  if (ttcRightFront == 0.0)
  {
    return std::nan("");
  }

  return 1.0 / ttcRightFront;
}

double AnticipatedLaneChangerModel::GetThwDotRightFront()
{
  const double thwDotRightFront{_microscopicData->GetObjectInformation(AreaOfInterest::RIGHT_FRONT)->gapDot};

  return std::abs(thwDotRightFront) > THW_OUT_OF_REACH ? std::nan("") : thwDotRightFront;
}

double AnticipatedLaneChangerModel::GetTtcInverseDotRightFront()
{
  const double ttcDotInvRightFront{_mentalCalculations.GetTauDotEq(AreaOfInterest::RIGHT_FRONT)};

  return ttcDotInvRightFront == INFINITY ? std::nan("") : ttcDotInvRightFront;  // Anmerkung für Implementierung: Evtl. Anpassung hier noch notwendig
}

double AnticipatedLaneChangerModel::GetTtcInverseEgoFront()
{
  if (_microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT)->exist ||
      _microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT_FAR)->exist)
  {
    const AreaOfInterest aoi = _microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT)->exist ? AreaOfInterest::EGO_FRONT : AreaOfInterest::EGO_FRONT_FAR;
    const auto ttcEgoFront = _microscopicData->GetObjectInformation(aoi)->ttc;

    if (ttcEgoFront == 0.0_s)
    {
      return std::nan("");
    }

    return 1.0 / ttcEgoFront.value();
  }

  return std::nan("");
}

double AnticipatedLaneChangerModel::GetTtcInverseDotEgoFront()
{
  if (_microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT)->exist ||
      _microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT_FAR)->exist)
  {
    const AreaOfInterest aoi = _microscopicData->GetObjectInformation(AreaOfInterest::EGO_FRONT)->exist ? AreaOfInterest::EGO_FRONT : AreaOfInterest::EGO_FRONT_FAR;
    const double ttcDotInvEgoFront = _mentalCalculations.GetTauDotEq(aoi);

    if (ttcDotInvEgoFront == INFINITY)
    {
      return std::nan("");
    }

    return ttcDotInvEgoFront;  // Anmerkung für Implementierung: Evtl. Anpassung hier noch notwendig
  }

  return std::nan("");
}

double AnticipatedLaneChangerModel::GetTtcInverseBetweenTwoAreasOfInterest(AreaOfInterest aoi_first, AreaOfInterest aoi_second) const
{
  AreaOfInterest aoi_rear = AreaOfInterest::EGO_REAR;
  AreaOfInterest aoi_front = AreaOfInterest::EGO_FRONT;
  units::length::meter_t deltaS = 0._m;
  auto deltaV = 0._mps;

  // check for agents in involved aois
  if (!_mentalModel.GetIsVehicleVisible(aoi_first) || !_mentalModel.GetIsVehicleVisible(aoi_second))
  {
    return 1. / ScmDefinitions::TTC_LIMIT.value();  // default if one aoi is empty
  }

  // both aois are in FRONT areas
  if (LaneQueryHelper::IsFrontArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    if ((_mentalModel.GetRelativeNetDistance(aoi_first) + _mentalModel.GetVehicleLength(aoi_first)) < _mentalModel.GetRelativeNetDistance(aoi_second))
    {
      aoi_front = aoi_second;
      aoi_rear = aoi_first;
    }
    else if ((_mentalModel.GetRelativeNetDistance(aoi_second) + _mentalModel.GetVehicleLength(aoi_second)) < _mentalModel.GetRelativeNetDistance(aoi_first))
    {
      aoi_front = aoi_first;
      aoi_rear = aoi_second;
    }
    else
    {
      return INFINITY;
    }

    deltaS = _mentalModel.GetRelativeNetDistance(aoi_front) - _mentalModel.GetRelativeNetDistance(aoi_rear) +
             _mentalModel.GetVehicleLength(aoi_rear);
    deltaV = _mentalModel.GetLongitudinalVelocity(aoi_front) - _mentalModel.GetLongitudinalVelocity(aoi_rear);
  }

  // one is FRONT and one is SIDE
  if (LaneQueryHelper::IsSideArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    aoi_front = aoi_second;
    aoi_rear = aoi_first;
  }
  else if (LaneQueryHelper::IsSideArea(aoi_second) && LaneQueryHelper::IsFrontArea(aoi_first))
  {
    aoi_front = aoi_first;
    aoi_rear = aoi_second;
  }

  deltaS = _mentalModel.GetRelativeNetDistance(aoi_front);
  deltaV = _mentalModel.GetLongitudinalVelocity(aoi_front) - _mentalModel.GetLongitudinalVelocity(aoi_rear);

  // one aoi is FRONT and one is REAR
  if (LaneQueryHelper::IsRearArea(aoi_first) && LaneQueryHelper::IsFrontArea(aoi_second))
  {
    aoi_front = aoi_second;
    aoi_rear = aoi_first;
  }
  else if (LaneQueryHelper::IsRearArea(aoi_second) && LaneQueryHelper::IsFrontArea(aoi_first))
  {
    aoi_front = aoi_first;
    aoi_rear = aoi_second;
  }

  deltaS = _mentalModel.GetRelativeNetDistance(aoi_front) + _mentalModel.GetRelativeNetDistance(aoi_rear) +
           _mentalModel.GetVehicleLength();
  deltaV = _mentalModel.GetLongitudinalVelocity(aoi_front) - _mentalModel.GetLongitudinalVelocity(aoi_rear);

  // one aoi is SIDE and one is REAR
  if (LaneQueryHelper::IsSideArea(aoi_first) && LaneQueryHelper::IsRearArea(aoi_second))
  {
    aoi_front = aoi_first;
  }
  else if (LaneQueryHelper::IsSideArea(aoi_second) && LaneQueryHelper::IsRearArea(aoi_first))
  {
    aoi_front = aoi_second;
  }

  deltaS = _mentalModel.GetRelativeNetDistance(aoi_rear);
  deltaV = _mentalModel.GetLongitudinalVelocity(aoi_front) - _mentalModel.GetLongitudinalVelocity(aoi_rear);

  // both aois are in REAR areas
  if (LaneQueryHelper::IsRearArea(aoi_first) && LaneQueryHelper::IsRearArea(aoi_second))
  {
    if ((_mentalModel.GetRelativeNetDistance(aoi_first) + _mentalModel.GetVehicleLength(aoi_first)) < _mentalModel.GetRelativeNetDistance(aoi_second))
    {
      aoi_front = aoi_first;
      aoi_rear = aoi_second;
    }
    else if ((_mentalModel.GetRelativeNetDistance(aoi_second) + _mentalModel.GetVehicleLength(aoi_second)) < _mentalModel.GetRelativeNetDistance(aoi_first))
    {
      aoi_front = aoi_second;
      aoi_rear = aoi_first;
    }
    else
    {
      return INFINITY;
    }

    deltaS = _mentalModel.GetRelativeNetDistance(aoi_rear) - _mentalModel.GetRelativeNetDistance(aoi_front) +
             _mentalModel.GetVehicleLength(aoi_front);
    deltaV = _mentalModel.GetLongitudinalVelocity(aoi_front) - _mentalModel.GetLongitudinalVelocity(aoi_rear);
  }

  // both in SIDE aoi is not defined
  if ((aoi_first == AreaOfInterest::LEFT_SIDE && aoi_second == AreaOfInterest::RIGHT_SIDE) ||
      (aoi_first == AreaOfInterest::RIGHT_SIDE && aoi_second == AreaOfInterest::LEFT_SIDE))
  {
    return 0.;
  }

  return -1. / (deltaS.value() / deltaV.value());
}