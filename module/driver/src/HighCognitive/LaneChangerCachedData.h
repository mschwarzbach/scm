/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
 * MACHINE GENERATED FILE, MODIFY CAREFULLY
 ***********************************************/

#pragma once

#include <array>
#include <map>

//! @file  LaneChangerCachedData.h

// TODO, CHANGE MODEL FOR... ON HOLD

//! @brief The probability distributions of observing a feature in general, with a leading vehicle in front of
//! EGO present.
//! Access E.g.:
//! EvidenceObservationProbabilityDistributionGeneralWithEgoFrontVehicle.at(320)[EvidenceMetrics::THW_EGO_E1]
//!                                                                             ↑              ↑
//!                                                                             |              |
//!                              Translated Double Value into LookUp-Integer ---┘              └--- Specific Metric
//!
extern std::map<int, std::array<double, 7>> EvidenceObservationProbabilityDistributionGeneralWithEgoFrontVehicle;

//! @brief The probability distributions of observing a feature during before a lane change occurs,
//! with a leading vehicle in front of EGO present.
extern std::map<int, std::array<double, 7>> EvidenceObservationLikelihoodDistributionLaneChangeWithEgoFrontVehicle;

//! @brief The probability distributions of observing a feature in general, without a leading vehicle in front of EGO
//! present.
extern std::map<int, std::array<double, 5>> EvidenceObservationProbabilityDistributionGeneralWithoutEgoFrontVehicle;

//! @brief The probability distributions of observing a feature during before a lane change occurs,
//! without a leading vehicle in front of EGO present.
extern std::map<int, std::array<double, 7>> EvidenceObservationLikelihoodDistributionLaneChangeWithoutEgoFrontVehicle;
