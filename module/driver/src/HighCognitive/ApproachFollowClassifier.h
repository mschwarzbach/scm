/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ApproachFollowClassifier.h

#pragma once

//! @brief Decision Tree Classifier for determining the kind of
//! observed traffic situation. Traffic Situations are distinguished
//! between approach (0), follow (1) or No Traffic Situation (-1)
//! Relevant features for this classifier are TTC_EGO_E1_inv (first element
//! of the features array) and THW_EGO_E1 (second element)
//!
//! @param features
//! @return label
int PredictTrafficSituation(float features[2]);
