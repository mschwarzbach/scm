/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AoiAssigner.h"

#include <vector>

#include "LaneQueryHelper.h"
#include "SurroundingVehicles/SurroundingVehicleDefinitions.h"
#include "include/common/ScmDefinitions.h"

LaneVehicleMapping AoiAssigner::AssignToLanes(const SurroundingVehicleVector& surroundingVehicles) const
{
  LaneVehicleMapping laneMapping;
  for (const auto& vehicle : surroundingVehicles)
  {
    RelativeLane relativeLane{AssignToLane(*vehicle)};

    if (laneMapping.count(relativeLane))
    {
      laneMapping.at(relativeLane).push_back(vehicle);
    }
    else
    {
      laneMapping[relativeLane] = {vehicle};
    }
  }

  return laneMapping;
}

AoiAssigner::SectorMapping AoiAssigner::AssignVehiclesToSectors(const LaneVehicleMapping& laneMapping) const
{
  AoiAssigner::SectorMapping sectorMap{CreateEmptySectorMap()};
  for (const auto& [relativeLane, vehicleVector] : laneMapping)
  {
    for (const auto& vehicle : vehicleVector)
    {
      RelativeLongitudinalPosition relativeLongitudinalPosition;
      if (vehicle->ToTheSideOfEgo())
      {
        relativeLongitudinalPosition = RelativeLongitudinalPosition::SIDE;
      }
      else
      {
        relativeLongitudinalPosition = vehicle->GetRelativeLongitudinalPosition().GetValue() > 0._m ? RelativeLongitudinalPosition::FRONT : RelativeLongitudinalPosition::REAR;
      }
      sectorMap[std::make_tuple(relativeLane, relativeLongitudinalPosition)].push_back(vehicle);
    }
  }
  return sectorMap;
}

AoiAssigner::SectorMapping AoiAssigner::AssignVehiclesToSectorsVector(const LaneVehicleMapping& laneMapping) const
{
  AoiAssigner::SectorMapping sectorMap{CreateEmptySectorMapVector()};
  for (const auto& [relativeLane, vehicleVector] : laneMapping)
  {
    for (const auto& vehicle : vehicleVector)
    {
      RelativeLongitudinalPosition relativeLongitudinalPosition;
      if (vehicle->ToTheSideOfEgo())
      {
        relativeLongitudinalPosition = RelativeLongitudinalPosition::SIDE;
      }
      else
      {
        relativeLongitudinalPosition = vehicle->GetRelativeLongitudinalPosition().GetValue() > 0._m ? RelativeLongitudinalPosition::FRONT : RelativeLongitudinalPosition::REAR;
      }
      sectorMap[std::make_tuple(relativeLane, relativeLongitudinalPosition)].push_back(vehicle);
    }
  }
  return sectorMap;
}

AoiAssigner::SectorMapping AoiAssigner::CreateEmptySectorMap()
{
  return AoiAssigner::SectorMapping{
      {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::FRONT_FAR), {}},
      {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::FRONT_FAR), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::FRONT_FAR), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::REAR), {}}};
}

AoiAssigner::SectorMapping AoiAssigner::CreateEmptySectorMapVector()
{
  return AoiAssigner::SectorMapping{
      {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::EGO, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::LEFT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::RIGHT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::LEFTLEFT, RelativeLongitudinalPosition::REAR), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::FRONT), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::SIDE), {}},
      {std::make_tuple(RelativeLane::RIGHTRIGHT, RelativeLongitudinalPosition::REAR), {}}};
}

AoiVehicleMapping AoiAssigner::AssignSideAoi(const AoiAssigner::SectorMapping& sectorMap)
{
  AoiVehicleMapping sideAoiMap;
  for (const auto lane : ALL_RELATIVE_LANES)
  {
    if (lane == RelativeLane::EGO)
    {
      continue;
    }
    const auto sectorTuple{std::make_tuple(lane, RelativeLongitudinalPosition::SIDE)};
    auto vehicleVector = sectorMap.at(sectorTuple);

    if (!vehicleVector.empty())
    {
      const auto targetAoi{AoiAssignment::AoiMap.at(sectorTuple)};
      std::sort(vehicleVector.begin(), vehicleVector.end(), [](auto& a, auto& b)
                { return a->FrontDistance().GetValue() > b->FrontDistance().GetValue(); });
      sideAoiMap.insert({targetAoi, vehicleVector});
    }
  }

  return sideAoiMap;
}

AoiVehicleMapping AoiAssigner::AssignFrontAoi(const AoiAssigner::SectorMapping& sectorMap)
{
  AoiVehicleMapping frontAoiMap;
  for (const auto lane : ALL_RELATIVE_LANES)
  {
    const auto sectorTupleFront{std::make_tuple(lane, RelativeLongitudinalPosition::FRONT)};
    const auto sectorTupleFrontFar{std::make_tuple(lane, RelativeLongitudinalPosition::FRONT_FAR)};

    const auto targetAoiFront{AoiAssignment::AoiMap.at(sectorTupleFront)};
    const auto targetAoiFrontFar{AoiAssignment::AoiMap.at(sectorTupleFrontFar)};

    auto vehicleVector = sectorMap.at(sectorTupleFront);
    std::sort(vehicleVector.begin(), vehicleVector.end(), [](auto& a, auto& b)
              { return a->RearDistance().GetValue() < b->RearDistance().GetValue(); });

    if ((lane == RelativeLane::LEFTLEFT || lane == RelativeLane::RIGHTRIGHT) && !vehicleVector.empty() ||
        vehicleVector.size() == 1)
    {
      frontAoiMap[targetAoiFront] = {vehicleVector.at(0)};
    }
    else if (vehicleVector.size() > 1)
    {
      frontAoiMap[targetAoiFront] = {vehicleVector.at(0)};
      frontAoiMap[targetAoiFrontFar] = {vehicleVector.at(1)};
    }
  }

  return frontAoiMap;
}

AoiVehicleMapping AoiAssigner::AssignRearAoi(const AoiAssigner::SectorMapping& sectorMap)
{
  AoiVehicleMapping rearAoiMap;
  for (const auto lane : ALL_RELATIVE_LANES)
  {
    const auto sectorTupleRear{std::make_tuple(lane, RelativeLongitudinalPosition::REAR)};
    const auto targetAoiRear{AoiAssignment::AoiMap.at(sectorTupleRear)};

    auto vehicleVector = sectorMap.at(sectorTupleRear);
    std::sort(vehicleVector.begin(), vehicleVector.end(), [](auto& a, auto& b)
              { return a->FrontDistance().GetValue() > b->FrontDistance().GetValue(); });

    if (!vehicleVector.empty())
    {
      rearAoiMap[targetAoiRear] = {vehicleVector.at(0)};
    }
  }

  return rearAoiMap;
}

// TODO Remove this function after adapting tests to separation of lane and AOI assignment
AoiVehicleMapping AoiAssigner::AssignToAois(const SurroundingVehicleVector& vehicles) const
{
  const auto laneMapping = AssignToLanes(vehicles);
  return AssignToAois(laneMapping);
}

AoiVehicleMapping AoiAssigner::AssignToAois(const LaneVehicleMapping& laneMapping) const
{
  const auto sectorMap = AssignVehiclesToSectors(laneMapping);

  AoiVehicleMapping aoiMap;

  const auto frontAoiMap{AssignSideAoi(sectorMap)};
  const auto sideAoiMap{AssignFrontAoi(sectorMap)};
  const auto rearAoiMap{AssignRearAoi(sectorMap)};

  aoiMap.insert(std::begin(frontAoiMap), std::end(frontAoiMap));
  aoiMap.insert(std::begin(sideAoiMap), std::end(sideAoiMap));
  aoiMap.insert(std::begin(rearAoiMap), std::end(rearAoiMap));

  return aoiMap;
}

AoiVehicleMapping AoiAssigner::AssignToAoisVector(const LaneVehicleMapping& laneMapping) const
{
  const auto sectorMap = AssignVehiclesToSectorsVector(laneMapping);

  std::map<AreaOfInterest, std::vector<SurroundingVehicleInterface*>> aoiMap{};
  for (const auto& [key, vector] : sectorMap)
  {
    const auto it{AoiAssignment::AoiMap.find(key)};
    if (it == AoiAssignment::AoiMap.end())
    {
      // FIXME: Use error logger instead of cerr
      std::cerr << "AoiAssigner::AssignToAoisVector: Invalid key\n";
    }
    else
    {
      aoiMap[it->second] = vector;
    }
  }
  return aoiMap;
}

RelativeLane AoiAssigner::AssignToLane(const SurroundingVehicleInterface& vehicle) const
{
  const auto& rightRightLane = _infrastructure.GetLaneInformationGeometry(RelativeLane::RIGHTRIGHT);
  const auto& rightLane = _infrastructure.GetLaneInformationGeometry(RelativeLane::RIGHT);
  const auto& egoLane = _infrastructure.GetLaneInformationGeometry(RelativeLane::EGO);
  const auto& leftLane = _infrastructure.GetLaneInformationGeometry(RelativeLane::LEFT);
  const auto& leftLeftLane = _infrastructure.GetLaneInformationGeometry(RelativeLane::LEFTLEFT);

  // OwnVehicleInformation lateralPosition : Lateral Position in lane. Distanz aus der Spurmitte
  const units::length::meter_t egoReferencePointToLaneBoundaryLeft{egoLane.width / 2 - _egoInfo.lateralPosition};
  const units::length::meter_t egoReferencePointToLaneBoundaryRight{egoLane.width / 2 + _egoInfo.lateralPosition};

  /* Other vehicle to left of ego */
  if (vehicle.GetRelativeLateralPosition().GetValue() >= 0_m)
  {
    if (vehicle.GetRelativeLateralPosition().GetValue() <= egoReferencePointToLaneBoundaryLeft)
    {
      return RelativeLane::EGO;
    }

    if (vehicle.GetRelativeLateralPosition().GetValue() > egoReferencePointToLaneBoundaryLeft + leftLane.width)
    {
      return RelativeLane::LEFTLEFT;
    }

    return RelativeLane::LEFT;
  }
  else
  {
    if (units::math::abs(vehicle.GetRelativeLateralPosition().GetValue()) <= egoReferencePointToLaneBoundaryRight)
    {
      return RelativeLane::EGO;
    }

    if (units::math::abs(vehicle.GetRelativeLateralPosition().GetValue()) > egoReferencePointToLaneBoundaryRight + rightLane.width)
    {
      return RelativeLane::RIGHTRIGHT;
    }

    return RelativeLane::RIGHT;
  }

  throw std::runtime_error("AoiAssigner::AssignToLane: Could not assign lane.");
}
