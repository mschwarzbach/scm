/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  LaneChangeBehavior.h

#pragma once
#include "LaneChangeBehaviorQueryInterface.h"

class LaneChangeBehaviorQuery : public LaneChangeBehaviorQueryInterface
{
public:
  LaneChangeBehaviorQuery(const MentalCalculationsInterface& mentalCalculations, const MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor);

  LaneChangeIntensities AdjustLaneConvenienceDueToHighwayExit(const LaneChangeIntensities& inIntensities, bool isRightHandTraffic) const override;
  LaneChangeIntensities AdjustLaneChangeIntensitiesForJam(const LaneChangeIntensities& inIntensities) const override;
  double CalculateLaneChangeIntensity(SurroundingLane targetLane, ComponentsForLaneChangeIntensity) const override;
  bool HasObstacleOnLane(SurroundingLane targetLane) const override;
  bool ShouldChangeLaneInJam(SurroundingLane targetLane) const override;
  units::length::meter_t GetRelativeNetDistance(SurroundingLane targetLane) const override;
  bool IsLaneChangeSave(SurroundingLane targetLane) const override;
  SurroundingLane DetermineLaneChangeWishFromIntensities(const LaneChangeIntensities& intensities) const override;
  bool EgoFitsInLane(SurroundingLane targetLane) const override;
  bool EgoBelowJamSpeed() const override;
  bool IsOuterLaneOvertakingProhibited(bool rightHandTraffic) const override;
  units::time::second_t GetTimeAtTargetSpeedInLane(SurroundingLane lane) const override;
  bool CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface *vehicle) const override;
  bool GetIsInfluencingDistanceViolated(const SurroundingVehicleInterface *vehicle) const override;
  bool GetIsLaneChangePermittedDueToLaneMarkings(Side side) const override;
  LaneChangeIntensities NormalizeLaneChangeIntensities(const LaneChangeIntensities& intensities) const override;

protected:
  SurroundingLane LaneWishAllIntensitiesAreEqual() const;
  std::optional<SurroundingLane> LaneWishTwoIntensitiesAreEqual(units::velocity::meters_per_second_t meanLaneVelocity) const;
  double CalculateIntensityHighwayExitLeft(const double urgencyFactor, double leftLaneIntensity) const;

private:
  double CalculateIntensityForQueuedTraffic(double laneIntensities, SurroundingLane targetLane) const;
  const MentalModelInterface& _mentalModel;
  const FeatureExtractorInterface& _featureExtractor;
  const MentalCalculationsInterface& _mentalCalculations;
};