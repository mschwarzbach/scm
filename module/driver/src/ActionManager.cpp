/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ActionManager.h"

#include <iterator>
#include <memory>
#include <optional>
#include <stdexcept>

#include "FeatureExtractorInterface.h"
#include "LaneKeepingCalculations.h"
#include "LaneKeepingCalculationsInterface.h"
#include "LateralAction.h"
#include "LateralActionQuery.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "SideLaneSafety/SideLaneSafety.h"
#include "SurroundingVehicles/LeadingVehicleQuery.h"
#include "SurroundingVehicles/LeadingVehicleSelector.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactory.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"

using namespace units::literals;

ActionManager::ActionManager(StochasticsInterface* stochastics,
                             MentalModelInterface& mentalModel,
                             const FeatureExtractorInterface& featureExtractor,
                             const MentalCalculationsInterface& mentalCalculations,
                             SwervingInterface& swerving,
                             const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory,
                             const TrafficRulesScm& trafficRulesScm,
                             const TrajectoryPlannerInterface& trajectoryPlanner,
                             ZipMergingInterface& zipMerging)
    : _stochastics{stochastics},
      _mentalModel{mentalModel},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations},
      _swerving{swerving},
      _surroundingVehicleQueryFactory{surroundingVehicleQueryFactory},
      _trafficRulesScm{trafficRulesScm},
      _trajectoryPlanner{trajectoryPlanner},
      _zipMerging{zipMerging}
{
  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  _mentalModel = mentalModel;
  _cycleTime = _mentalModel.GetCycleTime();

  _sideLaneSafety = std::make_shared<SideLaneSafety>(std::make_shared<SideLaneSafetyCalculations>(),
                                                     std::make_shared<SideLaneSafetyQuery>(mentalModel, mentalCalculations, surroundingVehicleQueryFactory),
                                                     surroundingVehicleQueryFactory);
}

LateralAction ActionManager::GetLateralActionLastTick()
{
  return _lateralActionLastTick;
}

void SetPossibleLeadingVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles, MentalModelInterface* mentalModel, const LeadingVehicleSelector* selector)
{
  const auto possibleRegulates{selector->DeterminePossibleRegulates(vehicles)};
  std::vector<int> possibleRegulateIds{};
  if (!possibleRegulates.empty())
  {
    std::transform(begin(possibleRegulates), end(possibleRegulates), std::back_inserter(possibleRegulateIds), [](const auto* vehicle)
                   { return vehicle->GetId(); });
  }

  mentalModel->SetPossibleRegulateIds(possibleRegulateIds);
}

void ActionManager::RunLeadingVehicleSelector()
{
  const auto egoInfo{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()};
  const auto vehicleQueryFactory{SurroundingVehicleQueryFactory(*egoInfo)};

  const auto leadingVehicleQuery{LeadingVehicleQuery(_mentalModel, _mentalCalculations, _featureExtractor, _swerving)};
  const auto leadingVehicleFilter{LeadingVehicleFilter(leadingVehicleQuery, vehicleQueryFactory)};
  const auto leadingVehicleSelector{LeadingVehicleSelector(vehicleQueryFactory, leadingVehicleFilter, leadingVehicleQuery)};

  auto vehicles{_mentalModel.GetSurroundingVehicles()};
  // Filter avoids problems with vehicles that have no AOI assigned to them
  // TODO remove when Aoi of leading vehicle is not used anymore
  vehicles = leadingVehicleFilter.FilterVehiclesWithUnassignedAoi(vehicles);
  const auto leadingVehicle{leadingVehicleSelector.FindLeadingVehicle(vehicles)};
  _mentalModel.SetLeadingVehicleId(leadingVehicle.has_value() ? leadingVehicle.value()->GetId() : -1);

  SetPossibleLeadingVehicles(vehicles, &_mentalModel, &leadingVehicleSelector);
}

void ActionManager::SetLateralMove(LateralAction action)
{
  LateralActionQuery lateralAction(action);
  if (lateralAction.IsLaneChangingLeft() || lateralAction.IsSwervingLeft())
  {
    _mentalModel.SetDirection(1);
  }
  else if (lateralAction.IsLaneChangingRight() || lateralAction.IsSwervingRight())
  {
    _mentalModel.SetDirection(-1);
  }
  else
  {
    _mentalModel.SetDirection(0);
  }
}

bool HasSwitchedToLaneKeeping(LateralAction currentAction, LateralAction newAction)
{
  return currentAction != LateralAction(LateralAction::State::LANE_KEEPING) && newAction == LateralAction(LateralAction::State::LANE_KEEPING);
}

bool IsOppositeLaneChangeDirection(LateralActionQuery currentAction, LateralActionQuery newAction)
{
  return (newAction.IsLaneChangingLeft() && currentAction.IsLaneChangingRight()) ||
         (newAction.IsLaneChangingRight() && currentAction.IsLaneChangingLeft());
}

void ActionManager::GenerateIntensityAndSampleLateralAction()
{
  const auto currentLateralAction{_mentalModel.GetLateralAction()};
  LateralActionQuery currentLateralActionQuery{currentLateralAction};

  GenerateLateralActionPatternIntensities();

  bool remainInLateralMovement = _mentalModel.GetRemainInLaneChangeState() || _mentalModel.GetRemainInSwervingState();
  bool unsafeLateralMovement = false;

  if (!_mentalModel.GetIsLateralActionForced() && !_mentalModel.GetRemainInSwervingState() &&
      _mentalModel.GetLongitudinalActionState() != LongitudinalActionState::ACTIVE_ZIP_MERGING)  // Currently no reevaluation of swerving state considered
  {
    unsafeLateralMovement = !_mentalModel.GetIsLateralMovementStillSafe();
  }

  bool hasSituationChanged{_actionPatternIntensities != _lastActionPatternIntensities};
  bool hasLaneChangeBlockChanged{_mentalModel.GetLaneChangePrevention() != lastPreventLaneChangeAtLateralActionDetermination};

  if (unsafeLateralMovement && currentLateralActionQuery.IsLaneChanging() && _actionPatternIntensities.count(currentLateralAction))
  {
    _actionPatternIntensities.at(currentLateralAction) = 0;
  }

  if ((hasSituationChanged && (!remainInLateralMovement || unsafeLateralMovement)) ||                                                                     // Situation has changed
      ((!remainInLateralMovement || unsafeLateralMovement) && (currentLateralActionQuery.IsSwerving() || currentLateralActionQuery.IsLaneChanging())) ||  // Lane change has ended but situation unchanged (e.g. due to no traffic)
      currentLateralActionQuery.IsIntentActive() || hasLaneChangeBlockChanged)
  {
    SetIntensitiesWhileLaneChangeBlocked();
    if (!_mentalModel.GetIsLateralActionForced())
    {
      LateralAction newLateralAction = ScmSampler::Sample(_actionPatternIntensities, LateralAction(LateralAction::State::LANE_KEEPING), _stochastics->GetUniformDistributed(0, 1));

      if (IsOppositeLaneChangeDirection(currentLateralActionQuery, LateralActionQuery(newLateralAction)))
      {
        newLateralAction = LateralAction(LateralAction::State::LANE_KEEPING);
      }

      _mentalModel.SetLateralAction(newLateralAction);
      if (HasSwitchedToLaneKeeping(currentLateralAction, newLateralAction))
      {
        _mentalModel.ResetPlannedLaneChangeDimensions();
      }
      SetLateralMove(newLateralAction);

      DetermineIsLaneChangeModifiedOrAborted(unsafeLateralMovement, newLateralAction);
    }

    // save actionPatternIntensities
    _lastActionPatternIntensities = _actionPatternIntensities;
  }

  // Save lane change prevention flag
  lastPreventLaneChangeAtLateralActionDetermination = _mentalModel.GetLaneChangePrevention();
}

void ActionManager::SetIntensitiesWhileLaneChangeBlocked()
{
  if (_mentalModel.GetLaneChangePrevention())
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)] =
        std::max({_actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)],
                  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)],
                  _actionPatternIntensities[LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)]});

    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)] = 0.;
    _actionPatternIntensities[LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)] = 0.;

    _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)] =
        std::max({_actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)],
                  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)],
                  _actionPatternIntensities[LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)]});

    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)] = 0.;
    _actionPatternIntensities[LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)] = 0.;
  }
}

void ActionManager::DetermineIsLaneChangeModifiedOrAborted(bool unsafeLateralMovement, LateralAction lateralAction)
{
  _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->modifyLaneChange = false;

  LateralActionQuery action(lateralAction);
  if (unsafeLateralMovement && (action.IsLaneChanging() || action.IsLaneKeeping()))
  {
    const bool isPastTransition{_mentalModel.GetIsLaneChangePastTransition()};
    const bool isSameState{_mentalModel.GetLateralAction() == _lateralActionLastTick};
    const bool isValidCase{(!isPastTransition && isSameState) || SwitchedFromLaneChangingToLaneKeeping() || (isPastTransition && IsOpposingState())};

    if (isValidCase)
    {
      _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->modifyLaneChange = true;
      _mentalModel.ResetTransitionState();
    }
  }
}

bool ActionManager::SwitchedFromLaneChangingToLaneKeeping() const
{
  LateralActionQuery action(_mentalModel.GetLateralAction());
  LateralActionQuery lastAction(_lateralActionLastTick);
  return action.IsLaneKeeping() && lastAction.IsLaneChanging();
}

bool ActionManager::IsOpposingState() const
{
  LateralActionQuery action(_mentalModel.GetLateralAction());
  LateralActionQuery lastAction(_lateralActionLastTick);
  return (action.IsLaneChangingLeft() && lastAction.IsLaneChangingRight()) ||
         (action.IsLaneChangingRight() && lastAction.IsLaneChangingLeft());
}

ActionManager::ActionPatternIntensities ActionManager::GetActionPatternIntensities() const
{
  return _actionPatternIntensities;
}

void ActionManager::ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement()
{
  const LateralAction lateralAction{_mentalModel.GetLateralAction()};

  LateralActionQuery action(lateralAction);
  if (!action.IsLaneChanging())
    throw std::runtime_error("ActionManager::ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement: This function is currently only valid while changing lanes!");

  if (LaneKeepingCalculations laneKeepingCalculations(_mentalModel, action);
      IsLaneKeepingMostAppropriate(laneKeepingCalculations))
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else  // go on with current lateral state ...
  {
    _actionPatternIntensities[lateralAction] = 1.;
  }
}

bool ActionManager::IsLaneKeepingMostAppropriate(const LaneKeepingCalculationsInterface& laneKeepingCalculations) const
{
  return laneKeepingCalculations.ChangingRightAppropriate() ||
         laneKeepingCalculations.ChangingLeftAppropriate() ||
         laneKeepingCalculations.IsAtEndOfLateralMovement() ||
         laneKeepingCalculations.IsObjectInTargetSide() ||
         laneKeepingCalculations.EgoFasterThanDeltaSide();
}

bool ActionManager::IsObjectInTargetSide(LateralAction lateralAction, bool isInTargetLane) const
{
  LateralActionQuery action(lateralAction);
  return (action.IsLaneChangingLeft() && !isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)) ||
         (action.IsLaneChangingRight() && !isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)) ||
         (action.IsLaneChangingLeft() && isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)) ||
         (action.IsLaneChangingRight() && isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0));
}

void ActionManager::ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest, bool urgent)
{
  // CONFLICT CASE - e.g. collision course + the conflict was found on the current ego lane
  const LateralAction lateralAction{_mentalModel.GetLateralAction()};

  // what does and where is the driver ?
  bool isVehicleOnTargetLaneDueToLateralMovement = _mentalModel.GetIsLaneChangePastTransition();

  // check intensities
  if (isChangingLaneLeft)
  {
    bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT, true);
    if (isVehicleOnTargetLaneDueToLateralMovement)  // reached target lane
    {
      // there will be no intent, just an action

      // can the driver turn around ?
      // in this moment the driver does not care about the data being degraded
      if (auto laneChangeDimensions = _trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::RIGHT);
          hasRightLane && _mentalCalculations.GetLaneChangeBehavior().GetLaneChangeWish() == SurroundingLane::RIGHT &&
          _featureExtractor.IsSideLaneSafe(Side::Right) &&
          _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::RIGHT))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)] = 1.;
        _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      }
      else
      {
        // choose minimal risk
        ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();
      }
    }
    else if (!_featureExtractor.IsSideLaneSafe(Side::Left))  // not on target lane by now
    {
      ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();
    }
  }
  else if (isChangingLaneRight)
  {
    bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT, true);

    if (isVehicleOnTargetLaneDueToLateralMovement)
    {
      // there will be no intent, just an action

      // can the driver turn around ?
      // in this moment the driver does not care about the data being degraded

      if (auto laneChangeDimensions = _trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::LEFT);
          hasLeftLane && _mentalCalculations.GetLaneChangeBehavior().GetLaneChangeWish() == SurroundingLane::LEFT &&
          _featureExtractor.IsSideLaneSafe(Side::Left) &&
          _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::LEFT))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)] = 1.;
        _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      }
      else
      {
        if (_mentalModel.GetIsEndOfLateralMovement())
        {
          _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
        }
        else
        {
          _actionPatternIntensities[lateralAction] = 1.;
        }
      }
    }
    else if (!_featureExtractor.IsSideLaneSafe(Side::Right))
    {
      if (_mentalModel.GetIsEndOfLateralMovement())
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
        _mentalModel.ResetPlannedLaneChangeDimensions();
      }
      else
      {
        _actionPatternIntensities[lateralAction] = 1.;
      }
    }
  }
  else  // e.g. swerving or exiting right
  {
    // no action pattern is set here
    // => go on...
    if (_mentalModel.GetIsEndOfLateralMovement())
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
      _mentalModel.ResetPlannedLaneChangeDimensions();
    }
    else
    {
      _actionPatternIntensities[lateralAction] = 1.;
    }
  }
}

std::array<bool, 5> ActionManager::GetAvailableLanesForCollisionCase(AreaOfInterest aoiCollision)
{
  const bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT, true);
  bool checkLeftLane = hasLeftLane;
  const bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT, true);
  bool checkRightLane = hasRightLane;

  if (aoiCollision == AreaOfInterest::LEFT_FRONT)
  {
    // Do not check although lane exists (because collision)
    checkLeftLane = false;
  }
  else if (aoiCollision == AreaOfInterest::RIGHT_FRONT)
  {
    // Do not check although lane exists (because collision)
    checkRightLane = false;
  }
  else if (aoiCollision != AreaOfInterest::EGO_FRONT)
  {
    // do not check (because irrelevant aoi)
    checkRightLane = false;
    checkLeftLane = false;
  }

  bool noAlternativeLane = (!checkLeftLane && !checkRightLane);

  return {hasLeftLane, checkLeftLane, hasRightLane, checkRightLane, noAlternativeLane};
}

ActionManager::CollisionBrakingEvadingIntensities ActionManager::GetBrakingEvadingIntensititesForCollisionCase(const std::map<CriticalActionState, double>& mapFront, CriticalActionState criticalActionState)
{
  const double intensityCollisionUnavoidable = mapFront.at(CriticalActionState::COLLISION_UNAVOIDABLE);
  const double intensityBraking = mapFront.at(CriticalActionState::BRAKING);
  const double intensityEvading = mapFront.at(criticalActionState);
  const double intensityBrakingOrEvading = mapFront.at(CriticalActionState::BRAKING_OR_EVADING);
  const double intensityEmergencyBraking = intensityCollisionUnavoidable + intensityBraking;

  const bool collisionUnavoidable = ((intensityCollisionUnavoidable > intensityBraking) &&
                                     (intensityCollisionUnavoidable > intensityEvading) &&
                                     (intensityCollisionUnavoidable > intensityBrakingOrEvading));

  const bool evading = ((intensityEvading > intensityCollisionUnavoidable) ||
                        (intensityEvading > intensityBraking) ||
                        (intensityEvading > intensityBrakingOrEvading));

  // Allow evading even inside the spawn zone, where lane changes are usually prohibited
  CheckLaneChangePreventionWithCollisionRisk(evading, intensityBraking);

  return {collisionUnavoidable, evading, intensityEvading, intensityBrakingOrEvading, intensityEmergencyBraking};
}

void ActionManager::SetActionPatternIntensitiesForCollisionCase(AreaOfInterest aoiCollision, bool swervingPermitted, std::array<bool, 5> lanes, CollisionBrakingEvadingIntensities frontLeft, CollisionBrakingEvadingIntensities frontRight)
{
  // Unpack all lane information to variables
  const auto [hasLeftLane, checkLeftLane, hasRightLane, checkRightLane, noAlternativeLane] = lanes;

  if (((checkLeftLane && checkRightLane) && frontLeft.collisionUnavoidable && frontRight.collisionUnavoidable) ||  // Collision unavoidable even when considering evading to eather side
      ((checkLeftLane && !checkRightLane) && frontLeft.collisionUnavoidable) ||                                    // Intention to evade to the left side but collision unavoidable
      ((!checkLeftLane && checkRightLane) && frontRight.collisionUnavoidable))                                     // Intention to evade to the right side but collision unavoidable
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else if (noAlternativeLane)
  {
    if (swervingPermitted)  // Swerving in lane possible
    {
      _swerving.DetermineSwervingIntensities(aoiCollision, true, _actionPatternIntensities);
    }
    else
    {
      if (checkLeftLane)
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = std::max(frontLeft.intensityBrakingOrEvading, frontLeft.intensityEmergencyBraking);
      }
      else if (checkRightLane)
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = std::max(frontRight.intensityBrakingOrEvading, frontRight.intensityEmergencyBraking);
      }
      else  // No right and left lane exist
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
      }
    }
  }

  else
  {
    if (checkLeftLane && hasLeftLane && _mentalModel.GetVehicleWidth() < _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::LEFT).width &&
        _featureExtractor.CheckLaneChange(Side::Left, true))  // Intention to evade to the left side
    {
      SetLaneChangeForCollisionCase(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
                                    frontLeft.intensityEmergencyBraking,
                                    frontLeft.intensityBrakingOrEvading + frontLeft.intensityEvading);
    }

    else if (checkRightLane && hasRightLane && _mentalModel.GetVehicleWidth() < _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::RIGHT).width &&
             _featureExtractor.CheckLaneChange(Side::Right, true))  // Intention to evade to the right side
    {
      SetLaneChangeForCollisionCase(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
                                    frontRight.intensityEmergencyBraking,
                                    frontRight.intensityBrakingOrEvading + frontRight.intensityEvading);
    }

    else if (checkLeftLane && hasLeftLane &&
             !_featureExtractor.CheckLaneChange(Side::Left, true))  // Intention to evade to the left side
    {
      SetIntentToChangeLanesForCollisionCase(frontLeft.evading, swervingPermitted, aoiCollision, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT), frontLeft.intensityEmergencyBraking, frontLeft.intensityBrakingOrEvading, frontLeft.intensityEvading, true);
    }

    else if (checkRightLane && hasRightLane &&
             !_featureExtractor.CheckLaneChange(Side::Right, true))  // Intention to evade to the right side
    {
      SetIntentToChangeLanesForCollisionCase(frontRight.evading, swervingPermitted, aoiCollision, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT), frontRight.intensityEmergencyBraking, frontRight.intensityBrakingOrEvading, frontRight.intensityEvading, true);
    }

    else if (checkLeftLane && hasLeftLane)
    {
      SetIntentToChangeLanesForCollisionCase(frontLeft.evading, swervingPermitted, aoiCollision, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT), frontLeft.intensityEmergencyBraking, frontLeft.intensityBrakingOrEvading, frontLeft.intensityEvading, false);
    }

    else
    {
      SetIntentToChangeLanesForCollisionCase(frontRight.evading, swervingPermitted, aoiCollision, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT), frontRight.intensityEmergencyBraking, frontRight.intensityBrakingOrEvading, frontRight.intensityEvading, false);
    }
  }
}

void ActionManager::SetLaneChangeForCollisionCase(LateralAction lateralAction, double intensityEmergencyBraking, double intensityBrakingEvading)
{
  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = intensityEmergencyBraking;
  _actionPatternIntensities[lateralAction] = intensityBrakingEvading;
}

void ActionManager::SetIntentToChangeLanesForCollisionCase(bool evadingFront, bool swervingPermitted, AreaOfInterest aoiCollision, LateralAction intentLaneChange, double intensityEmergencyBraking, double intensityBrakingOrEvading, double intensityEvading, bool useMax)
{
  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = intensityEmergencyBraking;

  if (evadingFront)
  {
    if (swervingPermitted)
    {
      _swerving.DetermineSwervingIntensities(aoiCollision, true, _actionPatternIntensities);
    }
    _actionPatternIntensities[intentLaneChange] = intensityBrakingOrEvading;
  }

  else if (useMax)
  {
    _actionPatternIntensities[intentLaneChange] = std::max(0., intensityEvading) + intensityBrakingOrEvading;
  }

  else
  {
    _actionPatternIntensities[intentLaneChange] = intensityEvading + intensityBrakingOrEvading;
  }
}

void ActionManager::ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest aoiCollision, bool swervingPermitted)
{
  // creating maps for the GeneratedCriticalActionStateEgoFront intensities

  if (!(aoiCollision == AreaOfInterest::EGO_FRONT || aoiCollision == AreaOfInterest::LEFT_FRONT ||
        aoiCollision == AreaOfInterest::RIGHT_FRONT))
  {
    return;
  }

  auto lanes = GetAvailableLanesForCollisionCase(aoiCollision);

  std::map<CriticalActionState, double> mapFrontLeft = GenerateCriticalActionIntensityVector(aoiCollision, true);
  std::map<CriticalActionState, double> mapFrontRight = GenerateCriticalActionIntensityVector(aoiCollision, false);

  auto intensitiesLeft = GetBrakingEvadingIntensititesForCollisionCase(mapFrontLeft, CriticalActionState::EVADING_LEFT);
  auto intensitiesRight = GetBrakingEvadingIntensititesForCollisionCase(mapFrontRight, CriticalActionState::EVADING_RIGHT);

  SetActionPatternIntensitiesForCollisionCase(aoiCollision, swervingPermitted, lanes, intensitiesLeft, intensitiesRight);
}

void ActionManager::CheckLaneChangePreventionWithCollisionRisk(bool evadingFront, double IntensityBraking)
{
  if (evadingFront && IntensityBraking == 0. && _mentalModel.GetLaneChangePrevention())
  {
    _mentalModel.SetLaneChangePreventionAtSpawn(false);
  }
}

bool ActionManager::CheckIntensitiesForChangingLeft()
{
  bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT);

  if (hasLeftLane && _mentalCalculations.GetLaneChangeBehavior().GetLaneChangeWish() == SurroundingLane::LEFT &&
      _featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Left))
  {
    const auto laneChangeDimensions{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::LEFT)};
    if (_featureExtractor.CheckLaneChange(Side::Left) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::LEFT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)] = 1.;
    }
    else
    {
      if (_mentalModel.IsSideLaneSafeWithMergePreparation(AreaOfInterest::LEFT_SIDE))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)] = 1.;
      }
      else
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)] = 1.;
      }
    }

    return true;
  }
  else
  {
    return false;
  }
}

bool ActionManager::CheckIntensitiesForChangingRight()
{
  bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT);

  if (hasRightLane && _mentalCalculations.GetLaneChangeBehavior().GetLaneChangeWish() == SurroundingLane::RIGHT &&
      _featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Right))
  {
    if (const auto laneChangeDimensions{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::RIGHT)};
        _featureExtractor.CheckLaneChange(Side::Right) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::RIGHT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)] = 1.;
    }
    else
    {
      if (_mentalModel.IsSideLaneSafeWithMergePreparation(AreaOfInterest::RIGHT_SIDE))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT)] = 1.;
      }
      else
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)] = 1.;
      }
    }

    return true;
  }
  else
  {
    return false;
  }
}

double ActionManager::GetUrgency()
{
  return _mentalCalculations.GetUrgencyFactorForLaneChange();
}

void ActionManager::GenerateLaneChangeIntensitiesForZipMerging()
{
  if (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING))
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.0;
    return;
  }
  // active zip merger
  const auto mergeParameters{_zipMerging.GetZipMergeParameters()};
  auto relativeLane = mergeParameters.relativeLane;
  auto mergePointDistance = _zipMerging.EstimateMergePoint(mergeParameters);
  const bool currentLaneBlocked{!_featureExtractor.IsLaneDriveablePerceived()};

  const auto lateralDirection = relativeLane == RelativeLane::LEFT ? LateralAction::Direction::LEFT : LateralAction::Direction::RIGHT;
  const auto aoi = relativeLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE;

  if (mergePointDistance <= 0_m && _mentalModel.GetVehicle(aoi) == nullptr)
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, lateralDirection)] = 1.0;
  }
  else if (currentLaneBlocked)
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, lateralDirection)] = 1.0;
  }
  else if (!_mentalModel.GetRemainInLaneChangeState())
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
}

void ActionManager::GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations()
{
  if (_trafficRulesScm.motorway.ZipperMerge && (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING) || _mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING)))
  {
    GenerateLaneChangeIntensitiesForZipMerging();
    _onlyMesoscopicSituation = true;
    return;
  }

  if (!_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED))
  {
    // initialize variables for lateral movement case
    pastPointOfLaneChangeIntensityMax = false;  // reset
    return;
  }

  bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT);
  bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT);
  if (!hasLeftLane && !hasRightLane)  // only one lane
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    return;
  }

  auto distanceToEndOfLaneEgo = _mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;
  auto distanceToEndOfLaneToStartZipMerge = units::math::max(30._m, 50._m * (_mentalModel.GetAbsoluteVelocityEgo(true) / _mentalModel.GetTrafficJamVelocityThreshold()));

  double LaneChangeIntensity = 0.;
  double LaneKeepingIntensity = 1.;

  if (!pastPointOfLaneChangeIntensityMax)
  {
    std::pair<double, double> intensityValuesForEndingLane = CalculateLateralActionIntensityValuesForEndingLane(LaneChangeIntensity, LaneKeepingIntensity, distanceToEndOfLaneEgo, distanceToEndOfLaneToStartZipMerge);
    LaneChangeIntensity = intensityValuesForEndingLane.first;
    LaneKeepingIntensity = intensityValuesForEndingLane.second;
  }
  else  // past point where lane change intensity reaches its maximum
  {
    LaneChangeIntensity = 1.;
    LaneKeepingIntensity = 0.;
  }

  // lateral guidance
  _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = LaneKeepingIntensity;

  const LateralActionQuery action(_mentalModel.GetLateralAction());
  if (action.IsLaneKeepingLeftActions() || action.IsLaneKeepingRightActions())
  {
    if (const auto laneChangeDimensionsRight{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::RIGHT)};
        hasRightLane && _featureExtractor.CheckLaneChange(Side::Right) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensionsRight.length, RelativeLane::RIGHT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensionsRight);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)] = LaneChangeIntensity;
    }

    else if (const auto laneChangeDimensionsLeft{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::LEFT)};
             hasLeftLane && _featureExtractor.CheckLaneChange(Side::Left) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensionsLeft.length, RelativeLane::LEFT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensionsLeft);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)] = LaneChangeIntensity;
    }
    else if (hasRightLane && !_featureExtractor.IsSideLaneSafe(Side::Right) &&
             _featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Right))
    {
      // prepare to merge
      if (_mentalModel.IsSideLaneSafeWithMergePreparation(AreaOfInterest::RIGHT_SIDE))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT)] = LaneChangeIntensity;
      }
      else
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)] = LaneChangeIntensity;
      }
    }
    else if (hasLeftLane && !_featureExtractor.IsSideLaneSafe(Side::Left) &&
             _featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Left))
    {
      // prepare to merge
      if (_mentalModel.IsSideLaneSafeWithMergePreparation(AreaOfInterest::LEFT_SIDE))
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)] = LaneChangeIntensity;
      }
      else
      {
        _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)] = LaneChangeIntensity;
      }
    }
    else
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
  }
  else  // lane changing
  {
    urgentLateralMovement = false;  // no abrupt movement here ?
    AreaOfInterest vehicleInTheWay = AreaOfInterest::EGO_FRONT;
    ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(vehicleInTheWay, urgentLateralMovement);
  }

  _onlyMesoscopicSituation = true;
}

std::pair<double, double> ActionManager::CalculateLateralActionIntensityValuesForEndingLane(double laneChangeIntensity,
                                                                                            double laneKeepingIntensity,
                                                                                            units::length::meter_t distanceToEndOfLaneEgo,
                                                                                            units::length::meter_t distanceToEndOfLaneToStartZipMerge)
{
  std::pair<double, double> intensityValuesForEndingLane;
  intensityValuesForEndingLane.first = laneChangeIntensity;
  intensityValuesForEndingLane.second = laneKeepingIntensity;

  // expand PONR by safety distance to reach maximum lane change intensity before PONR
  const auto vEgo = _mentalModel.GetAbsoluteVelocityEgo(false);
  const auto aComf = _mentalModel.GetComfortLongitudinalDeceleration() * 0.75;
  const auto safetyDistanceToPointOfNoReturn = distanceToEndOfLaneEgo - (vEgo * vEgo / 2. / aComf);
  const auto influencingDistance = _mentalModel.GetInfluencingDistanceToEndOfLane();

  if (safetyDistanceToPointOfNoReturn <= 0._m || distanceToEndOfLaneEgo < distanceToEndOfLaneToStartZipMerge)
  {
    pastPointOfLaneChangeIntensityMax = true;
    intensityValuesForEndingLane.first = 1.;
    intensityValuesForEndingLane.second = 0.;
  }
  else
  {
    if (distanceToEndOfLaneEgo <= influencingDistance)
    {
      intensityValuesForEndingLane.first = (influencingDistance - distanceToEndOfLaneEgo) /
                                           (influencingDistance - distanceToEndOfLaneEgo + safetyDistanceToPointOfNoReturn);
      intensityValuesForEndingLane.second = (intensityValuesForEndingLane.first > 0.) ? (1. - intensityValuesForEndingLane.first) : 1.;
    }
    else  // not influenced by end of lane
    {
      intensityValuesForEndingLane.first = 0.;
      intensityValuesForEndingLane.second = 1.;
    }
  }

  return intensityValuesForEndingLane;
}

void ActionManager::GenerateLateralActionPatternIntensitiesForFreeDriving()
{
  const bool laneChangeWishLeft = CheckIntensitiesForChangingLeft();
  const bool laneChangeWishRight = CheckIntensitiesForChangingRight();

  if (_featureExtractor.IsOnEntryOrExitLane())
  {
    if (laneChangeWishLeft && laneChangeWishRight)
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
  }
  else if (!laneChangeWishLeft && !laneChangeWishRight)
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForFollowingDriving()
{
  if (!CheckIntensitiesForChangingLeft() && !CheckIntensitiesForChangingRight())
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForTooClose()
{
  const auto causingAoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
  const auto* causingVehicle{_mentalModel.GetVehicle(causingAoi)};
  if (causingVehicle == nullptr)
  {
    SetLateralIntensitiesForNoCollisionThreatSituation();
    return;
  }

  if (!causingVehicle->IsReliable(DataQuality::HIGH, ParameterChangeRate::FAST))
  {
    _mentalModel.AddInformationRequest(causingAoi, FieldOfViewAssignment::FOVEA, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  const auto query{_surroundingVehicleQueryFactory.GetQuery(*causingVehicle)};
  const auto minDistance{_mentalCalculations.GetMinDistance(causingVehicle->GetAssignedAoi())};
  if (!query->HasCollisionCourse(minDistance))
  {
    SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else if (laneKeeping)  // collision course and lane keeping case
  {
    ChooseLateralActionIntensitiesForCollisionCase(causingAoi, true);
  }
  else if (!laneKeeping)  // collision course and lane changing ...
  {
    ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(causingAoi, urgentLateralMovement);
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane()
{
  const auto causingAoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
  const auto* causingVehicle{_mentalModel.GetVehicle(causingAoi)};
  if (causingVehicle == nullptr)
  {
    if (!CheckIntensitiesForChangingLeft() && !CheckIntensitiesForChangingRight())
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
    return;
  }

  if (!causingVehicle->IsReliable(DataQuality::HIGH, ParameterChangeRate::FAST))
  {
    _mentalModel.AddInformationRequest(causingAoi, FieldOfViewAssignment::FOVEA, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  const auto query{_surroundingVehicleQueryFactory.GetQuery(*causingVehicle)};
  const auto minDistance{_mentalCalculations.GetMinDistance(causingVehicle->GetAssignedAoi())};
  if (!query->HasCollisionCourse(minDistance))
  {
    if (!CheckIntensitiesForChangingLeft() && !CheckIntensitiesForChangingRight())
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
  }
  else  // collision course
  {
    ChooseLateralActionIntensitiesForCollisionCase(causingAoi, false);  // No swerving permitted since all obstacles are assumed to be in the center of the lane (see SensorDriver)
  }
}

LateralActionPatternSet ActionManager::GenerateLateralActionPatternSetForDetectedLaneChanger(Side side) const
{
  LateralActionPatternSet result;
  result.hasSideLane = side == Side::Right ? _mentalModel.GetLaneExistence(RelativeLane::LEFT) : _mentalModel.GetLaneExistence(RelativeLane::RIGHT);
  result.frontSideAoi = side == Side::Right ? AreaOfInterest::RIGHT_FRONT : AreaOfInterest::LEFT_FRONT;
  result.sideAoi = side == Side::Right ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE;
  result.laneChange = side == Side::Right ? LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT) : LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT);
  result.intentLaneChange = side == Side::Right ? LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT) : LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT);
  result.side = side;
  return result;
}

bool ActionManager::CantAvoidDetectedLaneChanger(LateralActionPatternSet laps) const
{
  return !laps.hasSideLane || !_featureExtractor.IsUnfavorable(_mentalModel.GetVehicle(laps.frontSideAoi)) ||
         !_featureExtractor.IsLaneChangePermittedDueToLaneMarkings(laps.side);
}

void ActionManager::AvoidDetectedLaneChanger(LateralActionPatternSet laps)
{
  if (_featureExtractor.CheckLaneChange(laps.side))
  {
    _actionPatternIntensities[laps.laneChange] = 1.;
  }
  else  // lane change is not safe
  {
    _actionPatternIntensities[laps.intentLaneChange] = 1.;
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side side)
{
  LateralActionPatternSet laps = GenerateLateralActionPatternSetForDetectedLaneChanger(side);

  if (CantAvoidDetectedLaneChanger(laps))
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else
  {
    AvoidDetectedLaneChanger(laps);
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft()
{
  // TODO is there a different behavior when changing the lane?
  bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT);

  if (!hasRightLane || !_mentalModel.GetCooperativeBehavior() ||
      !_featureExtractor.IsUnfavorable(_mentalModel.GetVehicle(AreaOfInterest::LEFT_FRONT)) ||
      !_featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Right) ||
      _mentalModel.GetVehicleClassification() == scm::common::VehicleClass::kHeavy_truck)
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else
  {
    const auto laneChangeDimensions{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::RIGHT)};
    if (_featureExtractor.CheckLaneChange(Side::Right) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::RIGHT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)] = 1.;
    }
    else  // data is old or lane change unsafe
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)] = 1.;
    }
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane()
{
  const bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT);

  if (!hasRightLane || !_featureExtractor.IsAvoidingLaneNextToSuspiciousSideVehicles() ||
      !_featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Right))
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else
  {
    CheckIntensitiesForChangingRight();  // Check wether laneChange is safe is included in function
  }
}

void ActionManager::ValidateSideAoi(AreaOfInterest sideAoi) const
{
  if (sideAoi != AreaOfInterest::LEFT_SIDE && sideAoi != AreaOfInterest::RIGHT_SIDE)
  {
    std::string msg;
    msg.append("ActionManager::ValidateSideAoi was called with ")
        .append(ScmCommons::AreaOfInterestToString(sideAoi))
        .append(" which is not a sideAoi");
    throw std::runtime_error(msg);
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest sideAoi)
{
  ValidateSideAoi(sideAoi);
  const int sideAoiIndex = _mentalModel.DetermineIndexOfSideObject(sideAoi,
                                                                   SideAoiCriteria::MIN,
                                                                   [](const SurroundingVehicleInterface* vehicle)
                                                                   { return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value()); });
  const auto* causingVehicle{_mentalModel.GetVehicle(sideAoi, sideAoiIndex)};
  if (causingVehicle == nullptr)
  {
    SetLateralIntensitiesForNoCollisionThreatSituation();
    return;
  }

  if (!causingVehicle->IsReliable(DataQuality::HIGH, ParameterChangeRate::FAST))
  {
    _mentalModel.AddInformationRequest(sideAoi, FieldOfViewAssignment::FOVEA, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  if (!_featureExtractor.HasActualLaneCrossingConflict(*causingVehicle))
  {
    SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else  // conflict is detected
  {
    if (laneKeeping)
    {
      const auto relativeLane{AreaOfInterest2RelativeLane.at(sideAoi)};
      const auto surroundingLane{RelativeLane2SurroundingLane.at(relativeLane)};
      const auto laneBoundaryCheckSide{surroundingLane == SurroundingLane::LEFT ? Side::Right : Side::Left};
      const auto direction{surroundingLane == SurroundingLane::LEFT ? LateralAction::Direction::LEFT : LateralAction::Direction::RIGHT};
      const bool isAgentOverLaneBoundary{causingVehicle->GetDistanceToLaneBoundary(laneBoundaryCheckSide).GetValue() < 0_m};

      if (!_featureExtractor.HasJamVelocityEgo() || isAgentOverLaneBoundary)
      {
        if (IsLaneChangeViable(relativeLane, sideAoi) && IsLaneChangeWishActive(surroundingLane))
        {
          _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, direction)] = 1.;
        }
      }
    }
    else  // lane changing
    {
      ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(sideAoi, urgentLateralMovement);
    }
  }
}

bool ActionManager::IsLaneChangeViable(RelativeLane lane, AreaOfInterest aoi) const
{
  Side side{};
  if (lane == RelativeLane::LEFT || lane == RelativeLane::LEFTLEFT)
  {
    side = Side::Left;
  }
  else if (lane == RelativeLane::RIGHT || lane == RelativeLane::RIGHTRIGHT)
  {
    side = Side::Right;
  }
  else
  {
    throw std::runtime_error("ActionManager::IsLaneChangeViable: egoLane not supported");
  }

  return _mentalModel.GetLaneExistence(lane, true) && _featureExtractor.IsSideLaneSafe(side);
}

bool ActionManager::IsLaneChangeWishActive(SurroundingLane surroundingLane) const
{
  return _mentalCalculations.GetLaneChangeBehavior().GetLaneChangeWish() == surroundingLane;
}

void ActionManager::ValidateFrontSideAoi(AreaOfInterest frontSideAoi) const
{
  if (frontSideAoi != AreaOfInterest::LEFT_FRONT && frontSideAoi != AreaOfInterest::RIGHT_FRONT &&
      frontSideAoi != AreaOfInterest::LEFT_FRONT_FAR && frontSideAoi != AreaOfInterest::RIGHT_FRONT_FAR)
  {
    std::string msg;
    msg.append("ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger was called with ")
        .append(ScmCommons::AreaOfInterestToString(frontSideAoi))
        .append(" which is not a frontSideAoi");
    throw std::runtime_error(msg);
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger()
{
  AreaOfInterest frontSideAoi = _mentalModel.GetCausingVehicleOfSituationSideCluster();

  ValidateFrontSideAoi(frontSideAoi);
  const auto* frontSideVehicle{_mentalModel.GetVehicle(frontSideAoi)};
  if (frontSideVehicle != nullptr && !frontSideVehicle->IsReliable(DataQuality::HIGH))
  {
    _mentalModel.AddInformationRequest(frontSideAoi, FieldOfViewAssignment::FOVEA, 1., InformationRequestTrigger::LATERAL_ACTION);
  }

  if (frontSideVehicle != nullptr && !_featureExtractor.IsCollisionCourseDetected(frontSideVehicle) &&
      !_featureExtractor.HasActualLaneCrossingConflict(*frontSideVehicle))
  {
    SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else
  {
    if (laneKeeping)
    {
      ChooseLateralActionIntensitiesForCollisionCase(frontSideAoi, false);
    }
    else  // lane changing
    {
      urgentLateralMovement = true;  // set the urgency ??
      ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(frontSideAoi, urgentLateralMovement);
    }
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight()
{
  // TODO is there a different behavior when changing the lane?
  bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT);

  if (!hasLeftLane || !_mentalModel.GetCooperativeBehavior() ||
      !_featureExtractor.IsUnfavorable(_mentalModel.GetVehicle(AreaOfInterest::RIGHT_FRONT)) ||
      !_featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Left) ||
      _mentalModel.GetVehicleClassification() == scm::common::VehicleClass::kHeavy_truck)
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else
  {
    const auto laneChangeDimensions{_trajectoryPlanner.CalculateLaneChangeDimensions(RelativeLane::LEFT)};
    if (_featureExtractor.CheckLaneChange(Side::Left) && _sideLaneSafety->CanFinishLaneChangeSafely(laneChangeDimensions.length, RelativeLane::LEFT))
    {
      _mentalModel.SetPlannedLaneChangeDimensions(laneChangeDimensions);
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)] = 1.;
    }
    else
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)] = 1.;
    }
  }
}

void ActionManager::GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane()
{
  // TODO is there a different behavior when changing the lane?
  const bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT);
  if (!hasLeftLane || !_featureExtractor.IsAvoidingLaneNextToSuspiciousSideVehicles() ||
      !_featureExtractor.IsLaneChangePermittedDueToLaneMarkings(Side::Left))
  {
    _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
  else
  {
    CheckIntensitiesForChangingLeft();
  }
}

void ActionManager::GenerateLateralActionPatternIntensities()
{
  // initialize common variables
  Situation currentSituation = _mentalModel.GetCurrentSituation();

  const LateralAction& lateralAction = _mentalModel.GetLateralAction();
  const LateralActionQuery action{lateralAction};

  _mentalCalculations.GetLaneChangeBehavior().CalculateLaneIntensities(_trafficRulesScm);
  bool isKeepingLane = action.IsLaneKeepingLeftActions() || action.IsLaneKeepingRightActions();
  // lane Keeping can also be interpreted, when the end of the lateral movement is reached for a single moment
  // before the next lateral action is set
  laneKeeping = isKeepingLane || (!isKeepingLane && _mentalModel.GetIsEndOfLateralMovement());

  // initialize variables for lateral movement case
  isChangingLaneLeft = false;
  isChangingLaneRight = false;
  urgentLateralMovement = false;

  if (!laneKeeping)  // lane changing case
  {
    urgentLateralMovement = GetUrgency() == 0. ? false : true;

    isChangingLaneLeft = action.IsLaneChangingLeft();
    isChangingLaneRight = action.IsLaneChangingRight();
  }

  // reset intensities and start calculation
  _actionPatternIntensities.clear();
  _onlyMesoscopicSituation = false;

  if (_swerving.IsSwervingPlanned() && action.IsLaneKeeping() && _mentalModel.IsFrontSituation())
  {
    const auto evadingLength = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->distanceForPlannedSwerving;

    const AreaOfInterest causingAoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
    const auto relativeNetDistance{_mentalModel.GetRelativeNetDistance(causingAoi)};

    if (evadingLength * 1.2 >= relativeNetDistance)
    {
      _swerving.DetermineSwervingIntensities(causingAoi, false, _actionPatternIntensities);
      return;  // Swerving maneuver initiated -> no need to check remaining logic
    }
  }
  else if (_swerving.IsSwervingPlanned())  // Planned swerving already started
  {
    _actionPatternIntensities = _lastActionPatternIntensities;
    return;  // Continue with the current swerving maneuver
  }

  GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations();

  if (!_onlyMesoscopicSituation)
  {
    switch (currentSituation)
    {
      case Situation::FREE_DRIVING:  // standard situation if nothing else applies
        GenerateLateralActionPatternIntensitiesForFreeDriving();
        break;

      case Situation::FOLLOWING_DRIVING:
      {
        const bool isHighRisk{_mentalModel.GetSituationRisk(currentSituation) == Risk::HIGH};
        if (isHighRisk)
        {
          GenerateLateralActionPatternIntensitiesForTooClose();
        }
        else
        {
          GenerateLateralActionPatternIntensitiesForFollowingDriving();
        }
      }
      break;

      case Situation::OBSTACLE_ON_CURRENT_LANE:
        GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
        break;

      case Situation::LANE_CHANGER_FROM_RIGHT:
      {
        const bool isHighRisk{_mentalModel.GetSituationRisk(currentSituation) == Risk::HIGH};
        bool isAnticipated{_mentalModel.IsSituationAnticipated(currentSituation)};

        if (isHighRisk)  // high risk lane changer and anticipated high risk lane changer
        {
          GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();
        }
        else if (!isHighRisk && !isAnticipated)  // detected lane changer
        {
          GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side::Right);
        }
        else  // anticipated lane changer (isAnticipated && !isHighRisk)
        {
          GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight();
        }
      }
      break;
      case Situation::LANE_CHANGER_FROM_LEFT:
      {
        bool isAnticipated{_mentalModel.IsSituationAnticipated(currentSituation)};
        const bool isHighRisk{_mentalModel.GetSituationRisk(currentSituation) == Risk::HIGH};
        if (isHighRisk)  // high risk lane changer and anticipated high risk lane changer
        {
          GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();
        }
        else if (!isHighRisk && !isAnticipated)  // detected lane changer
        {
          GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side::Left);
        }
        else  // anticipated lane changer (isAnticipated && !isHighRisk)
        {
          GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft();
        }
      }
      break;

      case Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE:
        GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane();
        break;

      case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
        GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest::LEFT_SIDE);
        break;

      case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
        GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest::RIGHT_SIDE);
        break;

      case Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE:
        GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane();
        break;

      case Situation::COLLISION:
        // simplifying assumption that a change of actionState is not possible in collision state
        _actionPatternIntensities[lateralAction] = 1.;
        break;

      default:
        throw std::runtime_error("ActionManager::GenerateLateralActionPatternIntensities: Driver does not know what to do in a chosen situation!");
    }
  }
}

void ActionManager::DetermineLongitudinalActionState()
{
  Situation situationCurrent = _mentalModel.GetCurrentSituation();

  RunLeadingVehicleSelector();
  const auto leadingVehicle{_mentalModel.GetLeadingVehicle()};

  /* CURRENT_LANE_BLOCKED is a special situation and an overwriting case for action substate finding
       - it will be handled before all other possibilities
       - it filters when to brake on the end of lane and lets other
       - case pass to be evaluated by standard handling below */

  if (situationCurrent == Situation::COLLISION)
  {
    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::STOPPED);
    return;
  }

  if (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING))
  {
    if (_actionSubStateLastTick != LongitudinalActionState::ACTIVE_ZIP_MERGING)
    {
      const auto mergeParameters{_zipMerging.DetermineZipMergeParameters()};
      const bool activationDistanceReached{_zipMerging.ActivationDistanceReached(mergeParameters)};
      if (activationDistanceReached)
      {
        _mentalModel.SetLongitudinalActionState(LongitudinalActionState::ACTIVE_ZIP_MERGING);
        _zipMerging.SetZipMergeParameters(mergeParameters);
      }
    }
    return;
  }

  if (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING))
  {
    if (_actionSubStateLastTick != LongitudinalActionState::PASSIVE_ZIP_MERGING)
    {
      const auto mergeParameters{_zipMerging.DetermineZipMergeParameters()};
      const bool activationDistanceReached{_zipMerging.ActivationDistanceReached(mergeParameters)};
      if (activationDistanceReached)
      {
        _mentalModel.SetLongitudinalActionState(LongitudinalActionState::PASSIVE_ZIP_MERGING);
        _zipMerging.SetZipMergeParameters(mergeParameters);
      }
    }
    return;
  }

  if (_mentalModel.GetMergeGap().IsValid())
  {
    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::PREPARING_TO_MERGE);
    return;
  }

  if (!_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED))
  {
    pastPointOfNoReturnForBrakingToEndOfLane = false;
  }
  auto vEgo = _mentalModel.GetAbsoluteVelocityEgo(false);
  auto distanceEndOfLane = _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::EGO).distanceToEndOfLaneDuringEmergency;
  if (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED) &&
      _mentalCalculations.GetDistanceToPointOfNoReturnForBrakingToEndOfLane(vEgo, distanceEndOfLane) <= 0._m &&
      !pastPointOfNoReturnForBrakingToEndOfLane)
  {
    pastPointOfNoReturnForBrakingToEndOfLane = true;
  }

  const LateralActionQuery action(_mentalModel.GetLateralAction());
  if ((_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED) &&
       !_featureExtractor.IsEvadingEndOfLane() &&
       !action.IsLaneChanging() &&  // When evading end of lane, longitudinal control shall not be in respect to end of lane
       !(_featureExtractor.IsEvadingEndOfLane() ||
         (action.IsLaneChanging() &&
          _mentalModel.GetAbsoluteVelocityEgo(false) <= 1._mps)) &&
       pastPointOfNoReturnForBrakingToEndOfLane))  // Don't start braking before reaching the point of no return (acceleration reserve is considered in its calculation)
  {
    const auto* frontVehicle{_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)};
    if (frontVehicle != nullptr && !frontVehicle->IsReliable(DataQuality::HIGH))
    {
      _mentalModel.AddInformationRequest(AreaOfInterest::EGO_FRONT, FieldOfViewAssignment::FOVEA, 1., InformationRequestTrigger::LONGITUDINAL_ACTION);
    }

    if (!_mentalModel.GetIsLaneChangePastTransition() && !_featureExtractor.HasDrivableSuccessor(true, RelativeLane::EGO))
    {
      _mentalModel.SetLongitudinalActionState(LongitudinalActionState::BRAKING_TO_END_OF_LANE);
      return;
    }

    if (frontVehicle == nullptr &&
        !(_featureExtractor.IsOnEntryOrExitLane() &&                 // When on entry or exit ignore LaneType 'None'
          !_mentalModel.GetLaneExistence(RelativeLane::EGO, true)))  // If already on forbidden lane keep driving to permit getting away
    {
      _mentalModel.SetLongitudinalActionState(LongitudinalActionState::BRAKING_TO_END_OF_LANE);
      return;
    }

    if (frontVehicle != nullptr &&
        !_featureExtractor.IsCollisionCourseDetected(frontVehicle) &&
        !(_featureExtractor.IsOnEntryOrExitLane() &&
          !_mentalModel.GetLaneExistence(RelativeLane::EGO, true)))  // If collision course to EGO_FRONT is detected, following mechanism must apply
    {
      _mentalModel.SetLongitudinalActionState(LongitudinalActionState::BRAKING_TO_END_OF_LANE);
      return;
    }
  }

  // generalisation
  if (leadingVehicle == nullptr)
  {
    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);
    return;
  }

  const auto leadingVehicleQuery{_surroundingVehicleQueryFactory.GetQuery(*leadingVehicle)};
  const auto leadingAoi{leadingVehicle->GetAssignedAoi()};

  if (!leadingVehicle->IsReliable(DataQuality::HIGH) && leadingAoi != AreaOfInterest::NumberOfAreaOfInterests)
  {
    _mentalModel.AddInformationRequest(leadingAoi, FieldOfViewAssignment::FOVEA, 1., InformationRequestTrigger::LONGITUDINAL_ACTION);
  }
  if (_featureExtractor.IsMinimumFollowingDistanceViolated(leadingVehicle) && _mentalModel.GetMergeRegulate() != leadingAoi)
  {
    if (leadingVehicleQuery->GetLongitudinalVelocityDelta() > 0._mps &&
        leadingVehicle->GetTtc().GetValue() < ScmDefinitions::TTC_LIMIT)  // vehicles are approaching eachother and the motion is perceivable
    {
      if (_mentalModel.IsAbleToCooperateByAccelerating())
      {
        _mentalModel.SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);  // accelerate to cooperate with anticipated lane changer
        return;
      }

      _mentalModel.SetLongitudinalActionState(LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE);  // can not cooperate by accelerating
      return;
    }

    if (leadingVehicleQuery->GetLongitudinalVelocityDelta() < -2._mps)  // significantly moves away
    {
      _mentalModel.SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);
      return;
    }

    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE);
    return;
  }
  if (_featureExtractor.IsNearEnoughForFollowing(*leadingVehicle))
  {
    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE);
    return;
  }
  if (_featureExtractor.IsInfluencingDistanceViolated(leadingVehicle) &&
      leadingVehicleQuery->GetLongitudinalVelocityDelta() > 0._mps &&
      !_mentalModel.IsMergePreparationActive())
  {
    _mentalModel.SetLongitudinalActionState(LongitudinalActionState::APPROACHING);
    return;
  }

  _mentalModel.SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);
}

void ActionManager::SetLateralIntensitiesForNoCollisionThreatSituation()
{
  // Vehicle is in a lane change at the moment
  if (_mentalModel.GetRemainInLaneChangeState() || _mentalModel.GetRemainInSwervingState())
  {
    if (_mentalModel.GetIsEndOfLateralMovement())
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
    else
    {
      _actionPatternIntensities[_mentalModel.GetLateralAction()] = 1.;
    }
  }
  else
  {
    // Without a desire to change lanes the lateral action defaults to lane keeping
    if (!CheckIntensitiesForChangingLeft() && !CheckIntensitiesForChangingRight())
    {
      _actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
    }
  }
}

void ActionManager::DiscardLastActionPatternIntensities()
{
  ActionPatternIntensities emptyMap;
  _lastActionPatternIntensities = emptyMap;
}

std::map<CriticalActionState, double> ActionManager::GenerateCriticalActionIntensityVector(AreaOfInterest aoi,
                                                                                           bool evadeTargetOnItsLeft)
{
  std::map<CriticalActionState, double> criticalActionIntensities;
  criticalActionIntensities.insert(std::make_pair(CriticalActionState::BRAKING, 0.));
  criticalActionIntensities.insert(std::make_pair(CriticalActionState::BRAKING_OR_EVADING, 0.));
  criticalActionIntensities.insert(std::make_pair(CriticalActionState::COLLISION_UNAVOIDABLE, 0.));
  criticalActionIntensities.insert(std::make_pair(CriticalActionState::EVADING_LEFT, 0.));
  criticalActionIntensities.insert(std::make_pair(CriticalActionState::EVADING_RIGHT, 0.));

  // Get parameters and support functions
  CalculateCriticalActionIntensitiesParameters(aoi);

  // Check necessity for an action
  if (IsActionNecessary())
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING) = 1.;  // Continue current action, since there will be no collision
    return criticalActionIntensities;
  }

  // Get ttc and calculate ttc actionlimits
  CalculateTTCActionLimits(aoi, evadeTargetOnItsLeft);

  // Evaluate action intensities
  EvaluateActionIntensities(criticalActionIntensities, evadeTargetOnItsLeft);

  return criticalActionIntensities;
}

double ActionManager::GetDividedCriticalActionIntensity(units::time::second_t ttcCurrent, units::time::second_t ttcUpperLimit, units::time::second_t ttcLowerLimit)
{
  double actionIntensity{0.};

  if (ttcCurrent >= ttcUpperLimit)
  {
    actionIntensity = 1.;
  }
  else if (ttcCurrent > ttcLowerLimit)
  {
    actionIntensity = (ttcCurrent - ttcLowerLimit) / (ttcUpperLimit - ttcLowerLimit);
  }

  return actionIntensity;
}

void ActionManager::CalculateCriticalActionIntensitiesParameters(AreaOfInterest aoi)
{
  _criticalActionIntensitiesParameters.lateralVelocityEgo = _mentalModel.GetLateralVelocity();
  _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper = (1. - MIS_JUDGEMENT_FACTOR) * _mentalModel.GetDriverParameters().maximumLateralAcceleration;
  _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedLower = (1. + MIS_JUDGEMENT_FACTOR) * _mentalModel.GetDriverParameters().maximumLateralAcceleration;
  _criticalActionIntensitiesParameters.lateralPositionEgo = _mentalModel.GetLateralPosition();
  const int sideAoiIndex = _mentalModel.DetermineIndexOfSideObject(aoi,
                                                                   SideAoiCriteria::MAX,
                                                                   [](const SurroundingVehicleInterface* vehicle)
                                                                   { return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value()); });
  _criticalActionIntensitiesParameters.sideAoiIndex = sideAoiIndex;
  _criticalActionIntensitiesParameters.lateralPositionToPassLeft = _criticalActionIntensitiesParameters.lateralPositionEgo + _swerving.DetermineLateralDistanceToEvade(aoi, Side::Left, true, _criticalActionIntensitiesParameters.sideAoiIndex);
  _criticalActionIntensitiesParameters.lateralPositionToPassRight = _criticalActionIntensitiesParameters.lateralPositionEgo - _swerving.DetermineLateralDistanceToEvade(aoi, Side::Right, true, _criticalActionIntensitiesParameters.sideAoiIndex);
  _criticalActionIntensitiesParameters.currentVelocityWillResolveRightUpperLimit = _criticalActionIntensitiesParameters.lateralVelocityEgo * _criticalActionIntensitiesParameters.lateralVelocityEgo > units::math::fabs(2. * (_criticalActionIntensitiesParameters.lateralPositionEgo - _criticalActionIntensitiesParameters.lateralPositionToPassRight) * _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper);
  _criticalActionIntensitiesParameters.currentVelocityWillResolveLeftUpperLimit = _criticalActionIntensitiesParameters.lateralVelocityEgo * _criticalActionIntensitiesParameters.lateralVelocityEgo > units::math::fabs(2. * (_criticalActionIntensitiesParameters.lateralPositionEgo - _criticalActionIntensitiesParameters.lateralPositionToPassLeft) * _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper);
}

bool ActionManager::IsActionNecessary() const
{
  return ((_criticalActionIntensitiesParameters.lateralVelocityEgo <= 0._mps && _criticalActionIntensitiesParameters.lateralPositionToPassRight < 0._m &&  // Pass right because...
           (_criticalActionIntensitiesParameters.lateralPositionEgo <= _criticalActionIntensitiesParameters.lateralPositionToPassRight ||                  //   ...already beyond required lateral position
            _criticalActionIntensitiesParameters.currentVelocityWillResolveRightUpperLimit)) ||                                                            //   ...current trajectory will be enough
          (_criticalActionIntensitiesParameters.lateralVelocityEgo >= 0._mps && _criticalActionIntensitiesParameters.lateralPositionToPassLeft > 0._m &&   // Pass left because...
           (_criticalActionIntensitiesParameters.lateralPositionEgo >= _criticalActionIntensitiesParameters.lateralPositionToPassLeft ||                   //   ...already beyond required lateral position
            _criticalActionIntensitiesParameters.currentVelocityWillResolveLeftUpperLimit)));                                                              //   ...current trajectory will be enough
}

void ActionManager::CalculateTTCSteerLimits(bool evadeTargetOnItsLeft)
{
  auto lateralPositionToPass = evadeTargetOnItsLeft ? _criticalActionIntensitiesParameters.lateralPositionToPassLeft : _criticalActionIntensitiesParameters.lateralPositionToPassRight;
  double sign = evadeTargetOnItsLeft ? -1. : 1.;
  _ttcActionLimits.steerUpperLimit = (sign * _criticalActionIntensitiesParameters.lateralVelocityEgo) / _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper +
                                     units::math::sqrt(_criticalActionIntensitiesParameters.lateralVelocityEgo * _criticalActionIntensitiesParameters.lateralVelocityEgo /
                                                           (_criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper * _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper) +
                                                       sign *
                                                           2. * (_criticalActionIntensitiesParameters.lateralPositionEgo - lateralPositionToPass) / _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedUpper);

  _ttcActionLimits.steerLowerLimit = (sign * _criticalActionIntensitiesParameters.lateralVelocityEgo) / _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedLower +
                                     units::math::sqrt(_criticalActionIntensitiesParameters.lateralVelocityEgo * _criticalActionIntensitiesParameters.lateralVelocityEgo /
                                                           (_criticalActionIntensitiesParameters.lateralAccelerationAnticipatedLower * _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedLower) +
                                                       sign *
                                                           2. * (_criticalActionIntensitiesParameters.lateralPositionEgo - lateralPositionToPass) / _criticalActionIntensitiesParameters.lateralAccelerationAnticipatedLower);
}

void ActionManager::CalculateTTCActionLimits(AreaOfInterest aoi, bool evadeTargetOnItsLeft)
{
  _ttc = _mentalModel.GetTtc(aoi);

  _ttcActionLimits.brakeUpperLimit = -_mentalModel.GetLongitudinalVelocityDelta(aoi) / (2. * (1. - MIS_JUDGEMENT_FACTOR) * _mentalModel.GetDriverParameters().maximumLongitudinalDeceleration);
  _ttcActionLimits.brakeLowerLimit = -_mentalModel.GetLongitudinalVelocityDelta(aoi) / (2. * (1. + MIS_JUDGEMENT_FACTOR) * _mentalModel.GetDriverParameters().maximumLongitudinalDeceleration);

  CalculateTTCSteerLimits(evadeTargetOnItsLeft);
}

void ActionManager::EvaluateActionIntensities(std::map<CriticalActionState, double>& criticalActionIntensities, bool evadeTargetOnItsLeft)
{
  double intensityEvading{0.0};

  if (IsCollisionUnavoidable())  // case 1: collision unavoidable
  {
    criticalActionIntensities.at(CriticalActionState::COLLISION_UNAVOIDABLE) = 1.;
  }
  else if (IsBraking())  // case 2: braking
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING) = 1.;
  }
  else if (IsEvading())  // case 3: evading
  {
    intensityEvading = 1.;
  }
  else if (IsNormalDriving())  // case 4: normal driving
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING) = 1.;
  }
  else if (IsTTCWithinBrakeLimits() && _ttcActionLimits.steerLowerLimit > _ttc)  // between case 1 and 2 (collision unavoidable or braking)
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING) = GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit);
    criticalActionIntensities.at(CriticalActionState::COLLISION_UNAVOIDABLE) = 1. - criticalActionIntensities.at(CriticalActionState::BRAKING);
  }
  else if (IsTTCWithinSteerLimits() && _ttcActionLimits.brakeLowerLimit > _ttc)  // between case 1 and 3 (collision unavoidable or evading)
  {
    intensityEvading = GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit);
    criticalActionIntensities.at(CriticalActionState::COLLISION_UNAVOIDABLE) = 1. - intensityEvading;
  }
  else if (IsTTCWithinSteerLimits() && _ttcActionLimits.brakeUpperLimit < _ttc)  // between case 2 and 4 (braking or normal driving)
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING) = GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit);
    criticalActionIntensities.at(CriticalActionState::BRAKING) = 1. - criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING);
  }
  else if (IsTTCWithinBrakeLimits() && _ttcActionLimits.steerUpperLimit < _ttc)  // between case 3 and 4 (evading or normal driving)
  {
    criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING) = GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit);
    intensityEvading = 1. - criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING);
  }
  else if (IsTTCWithinBrakeLimits() && IsTTCWithinSteerLimits())  // between case 1,2,3 and 4
  {
    criticalActionIntensities.at(CriticalActionState::COLLISION_UNAVOIDABLE) =
        (1. - GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit)) *
        (1. - GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit));
    criticalActionIntensities.at(CriticalActionState::BRAKING) =
        (1. - GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit)) *
        GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit);
    intensityEvading =
        GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit) *
        (1. - GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit));
    criticalActionIntensities.at(CriticalActionState::BRAKING_OR_EVADING) =
        GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.steerUpperLimit, _ttcActionLimits.steerLowerLimit) *
        GetDividedCriticalActionIntensity(_ttc, _ttcActionLimits.brakeUpperLimit, _ttcActionLimits.brakeLowerLimit);
  }

  if (evadeTargetOnItsLeft)
  {
    criticalActionIntensities.at(CriticalActionState::EVADING_LEFT) = intensityEvading;
  }
  else
  {
    criticalActionIntensities.at(CriticalActionState::EVADING_RIGHT) = intensityEvading;
  }
}

bool ActionManager::IsCollisionUnavoidable() const
{
  return _ttcActionLimits.brakeLowerLimit > _ttc && _ttcActionLimits.steerLowerLimit > _ttc;
}

bool ActionManager::IsBraking() const
{
  return _ttcActionLimits.brakeUpperLimit < _ttc && _ttcActionLimits.steerLowerLimit > _ttc;
}

bool ActionManager::IsEvading() const
{
  return _ttcActionLimits.brakeLowerLimit > _ttc && _ttcActionLimits.steerUpperLimit < _ttc;
}

bool ActionManager::IsNormalDriving() const
{
  return _ttcActionLimits.brakeUpperLimit < _ttc && _ttcActionLimits.steerUpperLimit < _ttc;
}

bool ActionManager::IsTTCWithinBrakeLimits() const
{
  return _ttcActionLimits.brakeUpperLimit >= _ttc && _ttcActionLimits.brakeLowerLimit <= _ttc;
}

bool ActionManager::IsTTCWithinSteerLimits() const
{
  return _ttcActionLimits.steerUpperLimit >= _ttc && _ttcActionLimits.steerLowerLimit <= _ttc;
}

LongitudinalActionState ActionManager::GetActionSubStateLastTick() const
{
  return _actionSubStateLastTick;
}

void ActionManager::SetActionSubStateLastTick(LongitudinalActionState actionSubStateLastTick)
{
  _actionSubStateLastTick = actionSubStateLastTick;
}

void ActionManager::SetLateralActionLastTick(LateralAction lastLateralAction)
{
  _lateralActionLastTick = lastLateralAction;
}