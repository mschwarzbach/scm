/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <TrajectoryCalculations/SteeringManeuver.h>
#include <TrajectoryCalculations/TrajectoryCalculations.h>

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <vector>

#include "TrajectoryCalculations/SteeringManeuverTypes.h"

namespace TrajectoryCalculations
{

TrajectoryHeadings CalculateHeadings(const TrajectoryDimensions& p)
{
  auto headingManeuver = units::math::atan2(p.width, p.length);

  auto clampedHeadingManeuver = std::clamp(headingManeuver, units::angle::radian_t(-M_PI / 2.), units::angle::radian_t(M_PI / 2.));  // Hotfix

  if (units::math::abs(clampedHeadingManeuver) > units::angle::radian_t(M_PI / 2.))
  {
    throw std::runtime_error("TrajectoryParameters::CalculateHeadings: Trajectory heading angles should not be above +- 90°!");
  }
  return {
      .headingManeuver = clampedHeadingManeuver,
      .deltaHeadingStart = clampedHeadingManeuver - p.headingStart,
      .deltaHeadingEnd = p.headingEnd - clampedHeadingManeuver,
  };
}

void ValidateTrajectoryParameters(const TrajectoryParameters& parameters)
{
  static constexpr units::angle::radian_t MINIMUM_MANEUVER_HEADING{0.001_rad};
  static constexpr units::length::meter_t MINIMUM_MANEUVER_WIDTH{0.01_m};
  static constexpr units::length::meter_t MAXIMUM_MANEUVER_LENGTH{10000.0_m};
  static constexpr auto MINIMUM_PLANNING_INTERVAL{0.005_m};

  if (units::math::abs(parameters.geometryParameters.width) < MINIMUM_MANEUVER_WIDTH)
  {
    std::stringstream error_message;
    error_message << "TrajectoryCalculations: Trajectory width invalid\n";
    error_message << parameters;
    throw std::invalid_argument(error_message.str());
  }

  if (units::math::abs(parameters.headings.headingManeuver) < MINIMUM_MANEUVER_HEADING)
  {
    std::stringstream error_message;
    error_message << "TrajectoryCalculations: Trajectory heading invalid\n";
    error_message << parameters;
    throw std::invalid_argument(error_message.str());
  }

  if (parameters.geometryParameters.length <= 0.0_m ||
      parameters.geometryParameters.length > MAXIMUM_MANEUVER_LENGTH)
  {
    std::stringstream error_message;
    error_message << "TrajectoryCalculations: Trajectory length invalid\n";
    error_message << parameters;
    throw std::invalid_argument(error_message.str());
  }

  if (parameters.planningInterval <= MINIMUM_PLANNING_INTERVAL ||
      parameters.planningInterval > parameters.geometryParameters.length)
  {
    std::stringstream error_message;
    error_message << "TrajectoryCalculations: Planning interval invalid\n";
    error_message << parameters;
    throw std::invalid_argument(error_message.str());
  }
}

Trajectory CalculateTrajectory(const TrajectoryDimensions& dimensions, const DriverParameters& driverParameters, const VehicleParameters& vehicleParameters, units::length::meter_t planningInterval)
{
  const TrajectoryParameters geometryParameters{dimensions, planningInterval};

  ValidateTrajectoryParameters(geometryParameters);  // Throws on invalid parameter

  // First try calculation with comfort values
  const DriverParametersInternal comfortDriverParameters{
      .maxSteeringWheelSpeed = driverParameters.comfortAngularSteeringWheelVelocity,
      .maxLateralAcceleration = driverParameters.comfortLateralAcceleration};
  auto steeringTrajectories{CalculateSteeringTrajectories(geometryParameters, comfortDriverParameters, vehicleParameters)};

  // Check if comfort lane change is possible to reach desired dimensions
  if (steeringTrajectories.CombinedLength() > dimensions.length ||
      units::math::abs(steeringTrajectories.CombinedWidth()) > units::math::abs(dimensions.width))
  {
    // If dimensions do not fit calculate again with maximum values
    const DriverParametersInternal maxDriverParameters{
        .maxSteeringWheelSpeed = driverParameters.maxAngularSteeringWheelVelocity,
        .maxLateralAcceleration = driverParameters.maxLateralAcceleration};
    steeringTrajectories = CalculateSteeringTrajectories(geometryParameters, maxDriverParameters, vehicleParameters);
  }

  const auto straightSectionWidth{dimensions.width - steeringTrajectories.CombinedWidth()};
  const SteeringManeuver::StraightSectionParameters straightSectionParams{
      planningInterval,
      steeringTrajectories.firstSteeringActions.back(),
      straightSectionWidth};
  const auto straightSectionTrajectory{SteeringManeuver::CalculateStraightSection(straightSectionParams)};

  return CombineTrajectories(steeringTrajectories, straightSectionTrajectory);
}

SteeringTrajectories CalculateSteeringTrajectories(const TrajectoryParameters& trajectoryParameters, const DriverParametersInternal& driverParameters, const VehicleParameters& vehicleParameters)
{
  // Select and calculate driver related parameters
  const double curvatureDot = {(driverParameters.maxSteeringWheelSpeed.value() / vehicleParameters.steeringRatio) / vehicleParameters.wheelbase.value()};
  const double curvatureMax = {driverParameters.maxLateralAcceleration.value() / std::pow(vehicleParameters.absoluteVelocity.value(), 2.0)};

  const SteeringManeuver::SteeringActionParameters firstSteeringParameters{
      .planningInterval = trajectoryParameters.planningInterval,
      .headingDelta = trajectoryParameters.headings.deltaHeadingStart,
      .velocityEgo = vehicleParameters.absoluteVelocity,
      .curvatureDot = curvatureDot,
      .curvatureMax = curvatureMax,
  };

  const SteeringManeuver::SteeringSegmentPoint firstSteeringTrajectoriesStartPoint{
      .sReferenceLine = 0_m,
      .t = vehicleParameters.tPosition,
      .heading = trajectoryParameters.geometryParameters.headingStart,
      .curvature = trajectoryParameters.geometryParameters.curvatureStart,
      .sTrajectory = 0_m,
  };
  const auto firstSteeringActions{SteeringManeuver::CalculateSteeringActions(firstSteeringParameters, firstSteeringTrajectoriesStartPoint)};

  const SteeringManeuver::SteeringActionParameters secondSteeringParameters{
      .planningInterval = trajectoryParameters.planningInterval,
      .headingDelta = trajectoryParameters.headings.deltaHeadingEnd,
      .velocityEgo = vehicleParameters.absoluteVelocity,
      .curvatureDot = curvatureDot,
      .curvatureMax = curvatureMax,
  };
  const SteeringManeuver::SteeringSegmentPoint secondSteeringTrajectoriesStartPoint{
      .sReferenceLine = 0_m,
      .t = 0_m,
      .heading = trajectoryParameters.headings.headingManeuver,
      .curvature = trajectoryParameters.geometryParameters.curvatureEnd,
      .sTrajectory = 0_m,
  };

  const auto secondSteeringActions{SteeringManeuver::CalculateSteeringActions(secondSteeringParameters, secondSteeringTrajectoriesStartPoint)};

  return {firstSteeringActions, secondSteeringActions};
}

Trajectory CombineTrajectories(const SteeringTrajectories& steeringTrajectories, const Trajectory& straightSectionTrajectory)
{
  Trajectory combinedTrajectory{steeringTrajectories.firstSteeringActions};
  // straight section was already planned with first steering actions as offset - only add T coordinate offset
  combinedTrajectory.insert(end(combinedTrajectory), begin(straightSectionTrajectory), end(straightSectionTrajectory));

  const auto& lastTrajectoryPoint{combinedTrajectory.back()};
  const auto addPreviousTrajectory = [lastTrajectoryPoint](const SteeringManeuver::SteeringSegmentPoint& trajectoryPoint)
  {
    return SteeringManeuver::SteeringSegmentPoint{
        .sReferenceLine = trajectoryPoint.sReferenceLine + lastTrajectoryPoint.sReferenceLine,
        .t = trajectoryPoint.t + lastTrajectoryPoint.t,
        .heading = trajectoryPoint.heading,
        .curvature = trajectoryPoint.curvature,
        .sTrajectory = trajectoryPoint.sTrajectory + lastTrajectoryPoint.sTrajectory};
  };

  // values of second steering actions need to be increased to sit on top of the rest of the trajectory
  std::transform(begin(steeringTrajectories.secondSteeringActions) + 1, end(steeringTrajectories.secondSteeringActions), std::back_inserter(combinedTrajectory), addPreviousTrajectory);

  return combinedTrajectory;
}
}  // namespace TrajectoryCalculations
