/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <numeric>
#include <ostream>
#include <vector>

#include "TrajectoryCalculations/SteeringManeuverTypes.h"

namespace TrajectoryCalculations
{
using Trajectory = std::vector<SteeringManeuver::SteeringSegmentPoint>;

//! @brief Struct used to store useful data for trajectory calculations
struct TrajectoryDimensions
{
  //! @brief Width of the trajectory
  units::length::meter_t width{};
  //! @brief Length of the trajectory
  units::length::meter_t length{};
  //! @brief Heading at the start of the trajectory
  units::angle::radian_t headingStart{};
  //! @brief Heading at the end of the trajectory
  units::angle::radian_t headingEnd{0.0};  // At the moment every Trajectory ends in driving direction
  //! @brief Curvature at the start of the trajectory
  units::curvature::inverse_meter_t curvatureStart{0.0};
  //! @brief Curvature at the end of the trajectory
  units::curvature::inverse_meter_t curvatureEnd{0.0};

  //! @brief Operator<< overload to simplify the output of the the TrajectoryDimensions
  //! @param os
  //! @param data
  //! @return Consolidated data of the struct
  friend std::ostream& operator<<(std::ostream& os, const TrajectoryDimensions& data)
  {
    os << "width: " << data.width
       << " length: " << data.length
       << " headingStart: " << data.headingStart
       << " headingEnd: " << data.headingEnd
       << " curvatureStart: " << data.curvatureStart
       << " curvatureEnd: " << data.curvatureEnd;
    return os;
  }
};

//! @brief Struct used to store useful data for trajectory calculations
struct TrajectoryHeadings
{
  //! @brief The heading for the trajectory point
  units::angle::radian_t headingManeuver{};
  //! @brief The delta heading at the start
  units::angle::radian_t deltaHeadingStart{};
  //! @brief The delta heading at the end
  units::angle::radian_t deltaHeadingEnd{};

  //! @brief Operator<< overload to simplify the output of the the TrajectoryHeadings
  //! @param os
  //! @param data
  //! @return Consolidated data of the struct
  friend std::ostream& operator<<(std::ostream& os, const TrajectoryHeadings& data)
  {
    os << "headingManeuver: " << data.headingManeuver
       << " deltaHeadingStart: " << data.deltaHeadingStart
       << " deltaHeadingEnd: " << data.deltaHeadingEnd;
    return os;
  }
};

//! @brief Struct used to store useful data for trajectory calculations
struct DriverParameters
{
  //! @brief Angular steering wheel velocity considered as comfortable
  units::angular_velocity::radians_per_second_t comfortAngularSteeringWheelVelocity{};
  //! @brief Maximum angular steering wheel velocity
  units::angular_velocity::radians_per_second_t maxAngularSteeringWheelVelocity{};
  //! @brief Lateral acceleration considered as comfortable
  units::acceleration::meters_per_second_squared_t comfortLateralAcceleration{};
  //! @brief Maximum lateral acceleration
  units::acceleration::meters_per_second_squared_t maxLateralAcceleration{};
};

//! @brief Struct used to store useful data for trajectory calculations
struct VehicleParameters
{
  //! @brief The steering ratio
  double steeringRatio{};
  //! @brief The wheel base
  units::length::meter_t wheelbase{};
  //! @brief The absolute velocity
  units::velocity::meters_per_second_t absoluteVelocity{};
  //! @brief The T-Coordinate
  units::length::meter_t tPosition{};
};

//! @brief Calculates headings for a given TrajectoryDimension p
//! @param p
//! @return TrajectoryHeadings
TrajectoryHeadings CalculateHeadings(const TrajectoryDimensions& p);
struct TrajectoryParameters
{
  //! @brief Struct used to store useful data for trajectory calculations
  const TrajectoryDimensions geometryParameters{};
  //! @brief Struct used to store useful data for trajectory calculations
  const TrajectoryHeadings headings{};
  //! @brief The planning Interval
  const units::length::meter_t planningInterval{};

  //! @brief Constructor initializes and creates TrajectoryParameters.
  //! @param p
  //! @param planningInterval
  TrajectoryParameters(const TrajectoryDimensions& p, units::length::meter_t planningInterval)
      : geometryParameters{p},
        headings{CalculateHeadings(p)},
        planningInterval{planningInterval}
  {
  }

  //! @brief Operator<< overload to simplify the output of the the TrajectoryParameters
  //! @param os
  //! @param data
  //! @return Consolidated data of the struct
  friend std::ostream& operator<<(std::ostream& os, const TrajectoryParameters& data)
  {
    os << "geometryDimensions: " << data.geometryParameters
       << "\nheadings: " << data.headings
       << "\nplanningInterval: " << data.planningInterval;
    return os;
  }
};

struct SteeringTrajectories
{
  //! @brief first Steering Actions
  Trajectory firstSteeringActions{};
  //! @brief second Steering Actions
  Trajectory secondSteeringActions{};

  //! @brief Calculate the Width from first and second Steering Action
  //! @return total of firstActionWidth + secondActionWidth
  units::length::meter_t CombinedWidth() const
  {
    const units::length::meter_t firstActionsWidth{firstSteeringActions.back().t};
    const units::length::meter_t secondActionsWidth{secondSteeringActions.back().t};

    return firstActionsWidth + secondActionsWidth;
  }

  //! @brief Calculate the Length from first and second Steering Action
  //! @return total of firstActionLength + secondActionLength
  units::length::meter_t CombinedLength() const
  {
    const units::length::meter_t firstActionsLength{firstSteeringActions.back().sReferenceLine};
    const units::length::meter_t secondActionsLength{secondSteeringActions.back().sReferenceLine};

    return firstActionsLength + secondActionsLength;
  }
};
}  // namespace TrajectoryCalculations