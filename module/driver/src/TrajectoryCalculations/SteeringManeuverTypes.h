/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file SteeringManeuverTypes.h
#pragma once

#include "ScmDefinitions.h"

//! @brief Namespaces for SteeringManeuver
namespace SteeringManeuver
{
//! @brief Struct definition for the description of a SteeringSegmentPoint
struct SteeringSegmentPoint
{
  //! @brief s coordinate of the Referenceline
  units::length::meter_t sReferenceLine{ScmDefinitions::DEFAULT_VALUE};
  //! @brief t coordinate
  units::length::meter_t t{ScmDefinitions::DEFAULT_VALUE};
  //! @brief heading at the start of the sterring segment point
  units::angle::radian_t heading{ScmDefinitions::DEFAULT_VALUE};
  //! @brief curvature at the start of the sterring segment point
  units::curvature::inverse_meter_t curvature{ScmDefinitions::DEFAULT_VALUE};
  //! @brief sTrajectory of the sterring segment point
  units::length::meter_t sTrajectory{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief Struct definition for the description of a SteeringActionParameters
struct SteeringActionParameters
{
  //! @brief The planning Interval
  units::length::meter_t planningInterval;
  //! @brief The heading delta
  units::angle::radian_t headingDelta;
  //! @brief The velocity ego
  units::velocity::meters_per_second_t velocityEgo;
  //! @brief The curvature Dot
  double curvatureDot;
  //! @brief The curvature Max
  double curvatureMax;
};

//! @brief Struct definition for the description of a SteeringSegmentLengths
struct SteeringSegmentLengths
{
  //! @brief The length clothoid segment
  units::length::meter_t lengthClothoidSegment{0.0};
  //! @brief The length curve segment
  units::length::meter_t lengthCurveSegment{0.0};
};

//! @brief Struct definition for the description of a BaseSegmentParameters
struct BaseSegmentParameters
{
  //! @brief The planning Interval
  units::length::meter_t planningInterval;
  //! @brief The steering segment startpoint
  const SteeringSegmentPoint& startPoint;
};

//! @brief Struct definition for the description of a ClothoidParameters
struct ClothoidParameters : BaseSegmentParameters
{
  //! @brief The segment length
  units::length::meter_t segmentLength;
  //! @brief The curvature dot
  double curvatureDot;
  //! @brief The velocity ego
  units::velocity::meters_per_second_t velocityEgo;
};

//! @brief Struct definition for the description of a CurveSectionParameters
struct CurveSectionParameters : BaseSegmentParameters
{
  //! @brief The segment length
  units::length::meter_t segmentLength;
};

//! @brief Struct definition for the description of a StraightSectionParameters
struct StraightSectionParameters : BaseSegmentParameters
{
  //! @brief The segment width
  units::length::meter_t segmentWidth;
};
}  // namespace SteeringManeuver