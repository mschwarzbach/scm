/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SteeringManeuver.h"

#include <cmath>
#include <stdexcept>
#include <vector>

#include "TrajectoryCalculations/SteeringManeuverTypes.h"

namespace SteeringManeuver
{
std::vector<SteeringSegmentPoint> CalculateClothoid(const ClothoidParameters& param)
{
  const auto& startPoint{param.startPoint};
  std::vector<SteeringSegmentPoint> sectionTrajectory{};
  auto sReferenceLine{startPoint.sReferenceLine};
  auto t{startPoint.t};
  auto heading{startPoint.heading};
  auto curvature{startPoint.curvature};
  for (auto longitudinalDistance = param.planningInterval; longitudinalDistance <= param.segmentLength; longitudinalDistance += param.planningInterval)
  {
    curvature += units::make_unit<units::curvature::inverse_meter_t>(param.curvatureDot / param.velocityEgo.value() * param.planningInterval.value());  // todo: divide by 0 if vehicle not moving??
    const auto dHeading{units::make_unit<units::angle::radian_t>(curvature.value() * param.planningInterval.value())};
    heading += dHeading;
    const auto ds{units::math::cos(heading) * param.planningInterval};
    const auto dt{units::math::sin(heading) * param.planningInterval};
    sReferenceLine += ds;
    t += dt;

    sectionTrajectory.push_back(
        SteeringSegmentPoint{
            .sReferenceLine = sReferenceLine,
            .t = t,
            .heading = heading,
            .curvature = curvature,
            .sTrajectory = startPoint.sTrajectory + longitudinalDistance});
  }

  return sectionTrajectory;
}

std::vector<SteeringSegmentPoint> CalculateCurveSection(const CurveSectionParameters& param)
{
  const auto& startPoint{param.startPoint};
  std::vector<SteeringSegmentPoint> sectionTrajectory{};
  auto sReferenceLine{startPoint.sReferenceLine};
  auto t{startPoint.t};
  auto heading{startPoint.heading};
  const auto curvature{startPoint.curvature};
  const auto dHeading{units::math::atan(curvature * param.planningInterval)};
  for (auto longitudinalDistance = param.planningInterval; longitudinalDistance <= param.segmentLength; longitudinalDistance += param.planningInterval)
  {
    heading += dHeading;
    const auto ds{units::math::cos(heading) * param.planningInterval};
    const auto dt{units::math::sin(heading) * param.planningInterval};
    sReferenceLine += ds;
    t += dt;

    sectionTrajectory.push_back(
        SteeringSegmentPoint{
            .sReferenceLine = sReferenceLine,
            .t = t,
            .heading = heading,
            .curvature = curvature,
            .sTrajectory = startPoint.sTrajectory + longitudinalDistance});
  }

  return sectionTrajectory;
}

std::vector<SteeringSegmentPoint> CalculateStraightSection(const StraightSectionParameters& param)
{
  const auto& startPoint{param.startPoint};
  auto heading{startPoint.heading};
  if (units::math::fabs(heading) < 0.01_rad)
  {
    heading = param.segmentWidth > 0_m ? 0.01_rad : -0.01_rad;
  }  // HOTFIX BUG SCM-1175

  std::vector<SteeringSegmentPoint> sectionTrajectory{};
  auto sReferenceLine{startPoint.sReferenceLine};
  auto t{startPoint.t};
  const auto curvature{0.0_i_m};
  const auto ds{units::math::cos(heading) * param.planningInterval};
  const auto dt{units::math::sin(heading) * param.planningInterval};
  auto distanceAlongTrajectory{startPoint.sTrajectory};

  while (units::math::abs(t) <= units::math::abs(param.segmentWidth + startPoint.t))
  {
    t += dt;
    sReferenceLine += ds;
    distanceAlongTrajectory += param.planningInterval;

    sectionTrajectory.push_back(
        SteeringSegmentPoint{
            .sReferenceLine = sReferenceLine,
            .t = t,
            .heading = startPoint.heading,
            .curvature = curvature,
            .sTrajectory = distanceAlongTrajectory});
  }

  return sectionTrajectory;
}

SteeringSegmentLengths CalculateSteeringSegmentDimensions(const SteeringActionParameters& p)
{
  // Calculate length of steering segments and necessity of curve section
  const double curvatureTotal{p.curvatureDot / p.velocityEgo *
                              std::sqrt(std::abs(p.headingDelta.value() * p.velocityEgo.value() / p.curvatureDot))};

  if (curvatureTotal > p.curvatureMax)  // Steering action requires curve section
  {
    return {
        .lengthClothoidSegment = units::make_unit<units::length::meter_t>(p.velocityEgo.value() * p.curvatureMax / p.curvatureDot),
        .lengthCurveSegment = units::make_unit<units::length::meter_t>(std::abs(p.headingDelta.value()) / p.curvatureMax - p.velocityEgo.value() * p.curvatureMax / p.curvatureDot)};
  }

  return {
      .lengthClothoidSegment = units::make_unit<units::length::meter_t>(std::sqrt(std::fabs(p.headingDelta.value() * p.velocityEgo.value() / p.curvatureDot))),
      .lengthCurveSegment = 0.0_m};
}

std::vector<SteeringSegmentPoint> CalculateSteeringActions(const SteeringActionParameters& p, const SteeringSegmentPoint& startPoint)
{
  // Calculate length of steering segments and necessity of curve section
  const auto segmentLengths{CalculateSteeringSegmentDimensions(p)};
  std::vector<SteeringSegmentPoint> steeringActions{startPoint};

  // First clothoid segment
  const int signRising{p.headingDelta.value() >= 0 ? 1 : -1};
  const ClothoidParameters risingClothoidParams{
      p.planningInterval,
      steeringActions.back(),
      segmentLengths.lengthClothoidSegment,
      signRising * p.curvatureDot,
      p.velocityEgo};
  const auto risingClothoid{CalculateClothoid(risingClothoidParams)};
  steeringActions.insert(end(steeringActions), begin(risingClothoid), end(risingClothoid));

  // Calculation of curve section
  if (segmentLengths.lengthCurveSegment > 0._m)
  {
    const CurveSectionParameters curveParams{
        p.planningInterval,
        steeringActions.back(),
        segmentLengths.lengthCurveSegment};
    const auto curveSection{CalculateCurveSection(curveParams)};
    steeringActions.insert(end(steeringActions), begin(curveSection), end(curveSection));
  }

  // Second clothoid segment
  const int signFalling{p.headingDelta.value() >= 0 ? -1 : 1};
  const ClothoidParameters fallingClothoidParams{
      p.planningInterval,
      steeringActions.back(),
      segmentLengths.lengthClothoidSegment,
      signFalling * p.curvatureDot,
      p.velocityEgo};
  const auto fallingClothoid{CalculateClothoid(fallingClothoidParams)};
  steeringActions.insert(end(steeringActions), begin(fallingClothoid), end(fallingClothoid));

  return steeringActions;
}
}  // namespace SteeringManeuver