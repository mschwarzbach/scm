/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <vector>

#include "TrajectoryCalculations/SteeringManeuverTypes.h"

//! @brief Namespaces for SteeringManeuver
namespace SteeringManeuver
{
//! @brief Returns a trajectory to reach the desired heading delta given the start point and initial conditions in the parameters.
//! @param p const SteeringActionParameters&
//! @param startPoint const SteeringSegmentPoint&
//! @return vector with steering segment points of steering actions
std::vector<SteeringSegmentPoint> CalculateSteeringActions(const SteeringActionParameters& p, const SteeringSegmentPoint& startPoint);

//! @brief Calculates the length of the individual steering trajectory segments determined from the given parameters.
//! @param p SteeringActionParameters
//! @return struct with steering segment lengths of segment dimensions
SteeringSegmentLengths CalculateSteeringSegmentDimensions(const SteeringActionParameters& p);

//! @brief Calculates a clothoid trajectory originating at startPoint, rising/falling is given by sign of curvatureDot.
//! @param param const ClothoidParameters&
//! @return vector with steering segment points of clothoid
std::vector<SteeringSegmentPoint> CalculateClothoid(const ClothoidParameters& param);

//! @brief Calculates steering section with constant curvature originating at startPoint.
//! @param param const CurveSectionParameters&
//! @return vector with steering segment points of curve section
std::vector<SteeringSegmentPoint> CalculateCurveSection(const CurveSectionParameters& param);

//! @brief Calculates a straight trajectory in s and t with the given width originating at startPoint.
//! @param param const StraightSectionParameters&
//! @return vector with steering segment points of straight sections
std::vector<SteeringSegmentPoint> CalculateStraightSection(const StraightSectionParameters& param);
}  // namespace SteeringManeuver