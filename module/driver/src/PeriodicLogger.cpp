/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "PeriodicLogger.h"

PeriodicLogger::PeriodicLogger(scm::publisher::PublisherInterface* publisher)
    : _publisher(publisher) {}

scm::publisher::PublisherInterface* PeriodicLogger::GetLogger()
{
  return _publisher;
}
