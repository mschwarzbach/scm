/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


#pragma once
#include <Stochastics/StochasticsInterface.h>

#include <iostream>

#include "ScmDependenciesInterface.h"

//! @brief brief ScmDependencies is a collection of all data SCM needs from other modules (sensors, driver- and vehicle parameter,..)
class ScmDependencies : public ScmDependenciesInterface
{
public:
  //! @brief Constructor initializes and creates ScmDependencies.
  //! @param driverParameters
  //! @param trafficRulesScm
  //! @param vehicleModelParameters
  //! @param sensorOutputSignal
  //! @param stochastics
  //! @param spawnVelocity
  //! @param cycleTime
  ScmDependencies(DriverParameters const& driverParameters,
                  TrafficRulesScm const& trafficRulesScm,
                  scm::common::vehicle::properties::EntityProperties const& vehicleModelParameters,
                  const scm::signal::SensorOutput& sensorOutputSignal,
                  StochasticsInterface* stochastics,
                  units::velocity::meters_per_second_t spawnVelocity,
                  units::time::millisecond_t cycleTime);
  virtual ~ScmDependencies()
  {
  }

  void UpdateScmSensorData(const scm::signal::DriverInput& driverInput) override;
  void UpdateParametersScmSignal(const scm::signal::DriverInput& driverInput) override;
  void UpdateParametersVehicleSignal(const scm::signal::DriverInput& driverInput) override;

  DriverParameters GetDriverParameters() override;
  TrafficRulesScm* GetTrafficRulesScm() override;
  scm::common::vehicle::properties::EntityProperties GetVehicleModelParameters() override;
  OwnVehicleInformationSCM* GetOwnVehicleInformationScm() override;
  SurroundingObjectsSCM* GetSurroundingObjectsScm() override;
  TrafficRuleInformationSCM* GetTrafficRuleInformationScm() override;
  GeometryInformationSCM* GetGeometryInformationScm() override;
  StochasticsInterface* GetStochastics() override;
  units::velocity::meters_per_second_t GetSpawnVelocity() override;
  units::time::millisecond_t GetSpawnTime() override;
  void SetSpawnTime(units::time::millisecond_t time) override;
  units::time::millisecond_t GetCycleTime() override;
  OwnVehicleRoutePose* GetOwnVehicleRoutePose() override;

private:
  //! @brief information about the driverParameters
  DriverParameters _driverParameters{};
  //! @brief information about the traffic rules scm
  TrafficRulesScm _trafficRulesScm{};
  //! @brief information about the entity properties
  scm::common::vehicle::properties::EntityProperties _vehicleModelParameters{};
  //! @brief interface to stochastics
  StochasticsInterface* _stochastics;
  //! @brief spawning velocity
  units::velocity::meters_per_second_t _spawnVelocity{};
  //! @brief spawning time
  units::time::millisecond_t _spawnTime{};
  //! @brief cycle time
  units::time::millisecond_t _cycleTime{};
  //! @brief information about the own vehicle
  OwnVehicleInformationSCM _ownVehicleInformationScm{};
  //! @brief information about the surrounding objects
  SurroundingObjectsSCM _surroundingObjectsScm{};
  //! @brief information about the traffic rule informations
  TrafficRuleInformationSCM _trafficRuleInformationScm{};
  //! @brief information about the geometry informations
  GeometryInformationSCM _geometryInformationScm{};
  //! @brief information about the route and pose of ego agent
  OwnVehicleRoutePose _ownVehicleRoutePose;

  //! @brief set default angles to orientations relative to driver
  void SetDefaultOrientationsRelativeToDriver();
};
