/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  MentalModelInterface.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "ActionImplementationInterface.h"
#include "InfrastructureCharacteristics.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "MentalModelDefinitions.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "ScmDependencies.h"
#include "SurroundingVehicles/SurroundingVehicleDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "TrafficFlow/TrafficFlow.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

class ActionImplementation;
class SurroundingVehicles;

//! @brief Interface for MentalModel class to enable testing of dependent classes.
class MentalModelInterface
{
public:
  virtual ~MentalModelInterface() = default;

  //! @brief Get the id of the merge regulate
  //! @return merge regulate id
  virtual int GetMergeRegulateId() const = 0;  // TODO check if this method and all connected logic can be removed

  //! @brief Set the id of the merge regulate
  //! @param id
  virtual void SetMergeRegulateId(int id) = 0;  // TODO check if this method and all connected logic can be removed

  //! @brief Separate initialize method to provide the mental model with data which is first generated after the mental model was created
  //! @param driverParameters A set of driver parameter
  //! @param vehicleParameters A set of vehicle parameter
  //! @param initVEgo The initial velocity of the agent
  //! @param stochastics Interface to the stochastics module
  virtual void Initialize(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleParameters, units::velocity::meters_per_second_t initVEgo, StochasticsInterface* stochastics) = 0;

  //! @brief Checks if ego is about to be on a new lane (e.g. when the agent is considered to cross a lane)
  //! @return true if ego relates now to another lane, false otherwise
  virtual bool CheckForEgoLaneTransition() = 0;

  //! @brief Triggers the transition of ego information from aoi specific internal data to another aoi destination in the internal data
  //! @copydoc MentalCalculations::TransitionOfEgoVehicle
  virtual void Transition() = 0;

  //! @brief Resets the reaction base of the agent.
  //! @details The new reaction base time is drawn stochastically within human limits for the average reaction time.
  virtual void ResetReactionBaseTime() = 0;

  //! @brief Sets the reaction base time to a certain value (contrary to MentalModel::ResetReactionBaseTime where the time is drawn)
  //! @param time the time until the next reaction can occur
  virtual void SetReactionBaseTime(units::time::second_t time) = 0;

  //! @brief Advances the current reaction base time (based on the cycle time of the simulation)
  virtual void AdvanceReactionBaseTime() = 0;

  //! @brief Sets the adjustment base time (drawn stochastically within human limits for the average adjustment time, i.e. for changes from one foot pedal (brake) to the other (gas))
  //! @param actionImplementation Pointer to the actionImplementation to determine if the acceleration wish is opposite to the wish in the last time step.
  virtual void CalculateAdjustmentBaseTime(ActionImplementationInterface* actionImplementation) = 0;

  //! @brief Checks whether the adjustment base time is expired
  //! @return true if adjustmentBaseTime is <= 0, false otherwise
  virtual bool IsAdjustmentBaseTimeExpired() const = 0;

  //! @brief Advances the current adjustment base time (based on the cycle time of the simulation)
  virtual void AdvanceAdjustmentBaseTime() = 0;

  //! @brief Checks whether the last update to egos front area is late/too old (exceeding a certain threshold)
  //! @return true if time since last update of EGO_FRONT is greater than CRITICAL_UPDATE_TIME_EGO_FRONT
  virtual bool IsLateWithFrontUpdate() const = 0;

  //! @brief Checks whether the last update to a specific gaze state area is urgently late (exceeding the _urgentUpdateThreshold)
  //! @param gs the GazeState
  //! @return true if time since last update of the gaze state is greater than _urgentUpdateThreshold
  virtual bool IsUrgentlyLateWithUpdate(GazeState gs) const = 0;

  //! @brief Checks whether the last update to a specific area of interest is urgently late (exceeding the _urgentUpdateThreshold)
  //! @param aoi the AreaOfInterest
  //! @return true if time since last update of the area of interest is greater than _urgentUpdateThreshold
  virtual bool IsUrgentlyLateWithUpdate(AreaOfInterest aoi) const = 0;

  //! @brief Checks whether the aoi is considered as inconsistent
  //! @param aoi the areaOfInterest
  //! @return true if the aoi is in the list of inconsistent area of interests
  virtual bool IsInconsistent(AreaOfInterest aoi) const = 0;

  //! @brief Sets and adds the specific aoi to the list of inconsistencies
  //! @param aoi the areaOfInterest
  virtual void MarkInconsistent(AreaOfInterest aoi) = 0;

  //! @brief Removes the given aoi from the set of inconsistent AOIs
  //! @param aoi AreaOfInterest to remove
  virtual void ClearInconsistency(AreaOfInterest aoi) = 0;

  //! @brief Get all as inconsistent marked area of interests
  //! @return vector of all inconsistent aois
  virtual const std::vector<AreaOfInterest>& GetInconsistentAois() const = 0;

  //! @brief Checks whether the last update to a specific gaze state is not so long ago (not exceeding the RECENT_UPDATE_THRESHOLD)
  //! @param gs the AreaOfInterest
  //! @return true if time since last update of the gaze state is lesser than RECENT_UPDATE_THRESHOLD
  virtual bool IsEarlyWithUpdate(GazeState gs) const = 0;

  //! @brief Checks whether the last update to a specific area of interest is not so long ago (not exceeding the RECENT_UPDATE_THRESHOLD)
  //! @param aoi the AreaOfInterest
  //! @return true if time since last update of the area of interest is lesser than RECENT_UPDATE_THRESHOLD
  virtual bool IsEarlyWithUpdate(AreaOfInterest aoi) const = 0;

  //! @brief Checks whether the current leading vehicle is predicated to perform a lane change into the ego lane
  //! @return true if there is a predicated lane changer set as leading vehicle, false otherwise
  virtual bool IsLeadingVehicleAnticipatedLaneChanger() const = 0;

  //! @brief Checks whether the ego agent is able to cooperate with another lane changer into the ego lane (e.g. braking to make more space)
  //! @return true if ego agent is cooperative and is able to predict a lane changer
  virtual bool IsAbleToCooperate() const = 0;

  //! @brief Checks whether the ego agent is able to cooperate with another lane changer into the ego lane by accelerating
  //! @return true if ego agent is cooperative and is able to accelerate to make more space, false otherwise
  virtual bool IsAbleToCooperateByAccelerating() const = 0;

  //! @brief Resets MergeRegulateID and MergeGap to default/null data
  virtual void ResetMergeRegulate() = 0;

  //! @brief Checks whether the agent is currently preparing or performing a merging action
  //! @return true if there is a valid merge gap available, false otherwise
  virtual bool IsMergePreparationActive() const = 0;

  //! @brief Calculates the current legal velocity (based on applicable traffic rules) for the ego and direct neighboring lanes
  //! @param spawn flag indicating if legal velocity is calculated at spawn time
  //! @details Wrapper for MentalCalculations::CalculateVLegal
  virtual void CalculateVLegal(bool spawn) = 0;

  //! @brief Check whether it is end of lane change.
  //! @param LaneChangeWidth                 Width of lane change.
  //! @param LateralVelocity                 Lateral velocity of the ego vehicle.
  //! @param TrajectoryPlanningCompleted     Flag wether trajectory planning to reach lateral_displacement was completed
  virtual void CheckForEndOfLaneChange(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, bool TrajectoryPlanningCompleted) = 0;

  //! @brief Check whether Agent will show cooperative behavior in current situation
  virtual void CheckSituationCooperativeBehavior() = 0;

  //! @brief Set the current state of lateral action.
  //! @param startTime                 Start time.
  //! @param endTime                   End time.
  //! @param durationSinceInitiation   Duration since initiation.
  //! @param hasStateChanged           Check whether the state has changed.
  //! @param probActivation            Probability of activation.
  //! @param maxProb                   Maximum probability.
  //! @param minProb                   Minimum probability.
  //! @param activationState           State of activation.
  //! @return True if activated, false otherwise
  virtual bool ImplementStochasticActivation(units::time::millisecond_t startTime, units::time::millisecond_t endTime, units::time::millisecond_t durationSinceInitiation, bool hasStateChanged, double probActivation, double maxProb, double minProb, bool activationState) const = 0;

  //! @brief Check whether observed AOI is behind the reference vehicle.
  //! @param aoiReference AreaOfInterest of reference agent
  //! @param aoiObserved AreaOfInterest of observed agent
  //! @return True if the object is in the rear area, false otherwise
  virtual bool IsObjectInRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const = 0;

  //! @brief Check whether observed AOI is next to the reference vehicle.
  //! @param aoiReference AreaOfInterest of reference agent
  //! @param aoiObserved AreaOfInterest of observed agent
  //! @return True if the object is in the side area, false otherwise
  virtual bool IsObjectInSideArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const = 0;

  //! @brief Check whether observed AOI is a surrounding aoi (not infotainment etc).
  //! @param aoiReference AreaOfInterest of reference agent
  //! @param aoiObserved AreaOfInterest of observed agent
  //! @param includeSideSideLanes flag if side side lanes are included
  //! @return True if the observed object is in the vicinity of the reference object
  virtual bool IsObjectInSurroundingArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool includeSideSideLanes) const = 0;

  //! @brief Check whether observed AOI is next to or behind the reference vehicle.
  //! @param aoiReference AreaOfInterest of reference agent
  //! @param aoiObserved AreaOfInterest of observed agent
  //! @param isSide call from IsSideArea or IsRearArea
  //! @return True if the object is in the side or rear area, false otherwise
  virtual bool IsObjectInSideOrRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool isSide) const = 0;

  //! @brief Check whether AOI is inside the vehicle (infotainment etc).
  //! @param aoi AreaOfInterest
  //! @return True if the aoi is inside of the vehicle, false otherwise
  virtual bool IsInsideVehicleArea(AreaOfInterest aoi) const = 0;

  //! Create and transfer an information request to the mental model
  //! @param aoi                AreaOfInterest
  //! @param sufficiencyLevel   Sufficiency of the current quality.
  //! @param priority           Priority of the possible information request (default = 1).
  //! @param requester          Requesting module (default = undefined).
  //! @return A collection of double values {requester, sufficiencyLevel, priority}
  virtual std::vector<double> AddInformationRequest(AreaOfInterest aoi, FieldOfViewAssignment sufficiencyLevel, double priority, InformationRequestTrigger requester) const = 0;

  //! @brief Reset information requests for all currently refreshed areas of interest.
  virtual void ResetInformationRequests() = 0;

  //! @brief Delete the information requests which are solved by the current gaze / observation of area of interest.
  //! @param aoi AreaOfInterest
  //! @param sufficientViewAssignment Sufficiency of the current view assignment.
  virtual void ResetInformationRequestLevel(AreaOfInterest aoi, FieldOfViewAssignment sufficientViewAssignment) = 0;

  //! @brief Scales a map of top-down requests to a maximum single intensity equal 1.0.
  //! @param map Unscaled intensity map
  //! @return Scaled intensity map
  virtual std::map<AreaOfInterest, double> ScaleTopDownRequestMap(std::map<AreaOfInterest, double> map) const = 0;

  //! @brief Helper function to compare information request with multiple conditions.
  //! @param a first information request
  //! @param b second information request
  //! @return something.
  virtual bool CompareInformationRequest(const InformationRequest& a, const InformationRequest& b) const = 0;

  //! @brief Gather top-down requests and score them by importance.
  //! @return Intensity map for related AreasOfInterest
  virtual std::map<AreaOfInterest, double> GenerateTopDownAoiScoring(bool isHafSignalProcessed) const = 0;

  //! @brief Check, if the information for a lane change is too old or not reliable and add information requests if reliability is not given.
  //! @param side Side to check
  //! @return yes/no
  virtual bool IsDataForLaneChangeReliable(Side side) const = 0;

  //! @brief Check, if there is a vehicle in the sidesidelane that is potentially changing into target side lane
  //! @param relativeLane        Lane change to the left or to the right
  //! @param merge       Is lane change a merge
  //! @return yes/no
  virtual bool IsSideSideLaneObjectChangingIntoSide(RelativeLane relativeLane, bool merge) const = 0;

  //! @brief Determine whether the previous evaluation of the lane change is still good.
  //! @param isTransitionTrigger   Was a lane transition triggered since the previous time step
  virtual void ReevaluateCurrentLaneChange() = 0;

  //! @brief Checks whether the merge gap should be reevaluated to prevent frequent changes in the selection process.
  virtual void ReevaluateMergeGap() = 0;

  //! @brief Set the transition trigger to false.
  virtual void ResetTransitionState() = 0;

  //! @brief Query the velocity the driver would choose for reasons (e.g. misty roads)
  //! @return VReason
  virtual units::velocity::meters_per_second_t DetermineVelocityReason() const = 0;

  //! @brief Query the velocity the driver would choose when a queued traffic is detected
  //! @return VReason for traffic jams
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonForDetectedTrafficJam() const = 0;

  //! @brief Query the velocity the driver would choose due to poor sight distance.
  //! @return VReasonSight
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonSight() const = 0;

  //! @brief Query the velocity the driver would choose due to lane width.
  //! @return VReasonLaneWidth
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonLaneWidth() const = 0;

  //! @brief Query the velocity the driver would choose due to curvature.
  //! @return VReasonCurvature
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonCurvature() const = 0;

  //! @brief Query the velocity the driver would choose due to the Prohibition of overtaking.
  //! @return VReasonRightOvertakingProhibition
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonRightOvertakingProhibition() const = 0;

  //! @brief Query the velocity the driver would choose for passing a slow platoon.
  //! @return VReasonPassingSlowPlatoon
  virtual units::velocity::meters_per_second_t DetermineVelocityReasonPassingSlowPlatoon() const = 0;

  //! @brief Update the reliability map with the current time for the area of interest containing the fovea, ufov and periphery (curently no periphery)
  virtual void UpdateReliabilityMap() = 0;

  //! @brief Get helper class for mental model
  //! @return _stochastics
  virtual StochasticsInterface* GetStochastics() const = 0;

  //! @brief Check if the queried area of interest is the current fovea area
  //! @param aoi AreaOfInterest
  //! @return True if the aoi corresponds to the current fovea, false otherwise
  virtual bool IsFovea(AreaOfInterest aoi) const = 0;

  //! @brief Getter for the current FOVEA
  //! @return Area of interest
  virtual AreaOfInterest GetFovea() const = 0;

  //! @brief Getter for all current useful field of view areas
  //! @return Areas of interest
  virtual std::vector<AreaOfInterest> GetUfov() const = 0;

  //! @brief Getter for all current periphery of view areas
  //! @return Areas of interest
  virtual std::vector<AreaOfInterest> GetPeriphery() const = 0;

  //! @brief Check if trigger is for new ego lane
  //! @return yes/no
  virtual bool GetIsNewEgoLane() const = 0;

  //! @brief Determine, if the driver is involved in a collision case
  //! @return yes/no
  virtual bool GetCollisionState() const = 0;

  //! @brief Check for a vehicle in front.
  //! @param aoi             AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return yes/no
  virtual bool GetIsVehicleVisible(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Check for object in front that is designated to be an obstacle.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return yes/no
  virtual bool GetIsStaticObject(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated time-to-collision.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return TTC
  virtual units::time::second_t GetTtc(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated gap.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return Gap
  virtual units::time::second_t GetGap(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated acceleration.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return deltaA
  virtual units::acceleration::meters_per_second_squared_t GetAccelerationDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated distance to a vehicle in the corresponding AOI.
  //! @param aoi AreaOfInterest
  //! @return relativeNetDistance
  virtual units::length::meter_t GetRelativeNetDistance(AreaOfInterest aoi) const = 0;

  //! @brief Query extrapolated longitudinal velocity difference to a vehicle in the corresponding AOI.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return deltaV
  virtual units::velocity::meters_per_second_t GetLongitudinalVelocityDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated lateral velocity difference to a vehicle in the corresponding AOI.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return deltaLatV
  virtual units::velocity::meters_per_second_t GetLateralVelocityDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query target absolute velocity of the vehicle from internal data.
  //! @return Target absolute velocity
  virtual units::velocity::meters_per_second_t GetAbsoluteVelocityTargeted() const = 0;

  //! @brief Query extrapolated longitudinal acceleration of a vehicle in the corresponding AOI.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return longitudinal acceleration
  virtual units::acceleration::meters_per_second_squared_t GetAcceleration(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated lateral speed of a vehicle in the corresponding AOI [m/s].
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return lateral speed
  virtual units::velocity::meters_per_second_t GetLateralVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated minimum lateral distance of a vehicle in the corresponding AOI to its left lane boundary.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return minimum lateral distance to left lane boundary
  virtual units::length::meter_t GetDistanceToBoundaryLeft(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query extrapolated minimum lateral distance of a vehicle in the corresponding AOI to its right lane boundary.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return minimum lateral distance to right lane boundary
  virtual units::length::meter_t GetDistanceToBoundaryRight(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Get the distance of the own vehicle to left lane boundary
  //! @return minimum lateral distance to left lane boundary
  virtual units::length::meter_t GetDistanceToBoundaryLeft() const = 0;

  //! @brief Get the distance of the own vehicle to right lane boundary
  //! @return minimum lateral distance to right lane boundary
  virtual units::length::meter_t GetDistanceToBoundaryRight() const = 0;

  //! @brief Query length of ego vehicle [m].
  //! @return ego car length
  virtual units::length::meter_t GetVehicleLength() const = 0;

  //! @brief Query width of ego vehicle [m].
  //! @return ego car width
  virtual units::length::meter_t GetVehicleWidth() const = 0;

  //! @brief Query type of ego vehicle
  //! @return ego car type
  virtual scm::common::VehicleClass GetVehicleClassification() const = 0;

  //! @brief Query length of a vehicle in the corresponding AOI [m].
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return length
  virtual units::length::meter_t GetVehicleLength(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query width of a vehicle in the corresponding AOI [m].
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return width
  virtual units::length::meter_t GetVehicleWidth(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query height of a vehicle in the corresponding AOI [m].
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return height
  virtual units::length::meter_t GetVehicleHeight(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query type of a vehicle in the corresponding AOI
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return type
  virtual scm::common::VehicleClass GetVehicleClassification(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Set ID of the ego vehicle.
  //! @param id      Agent id of ego agent
  virtual void SetAgentId(int id) = 0;

  //! @brief Query ID of the ego vehicle.
  //! @return ID
  virtual int GetAgentId() const = 0;

  //! @brief Query ID of a vehicle in the corresponding AOI.
  //! @param aoi the area of interest
  //! @param sideAoiIndex the side index
  //! @return the agents id
  virtual int GetAgentId(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query reaction base Time
  //! @return the reaction base time
  virtual units::time::second_t GetReactionBaseTime() const = 0;

  //! @brief Query adjustment base Time
  //! @return the adjustment base time
  virtual units::time::second_t GetAdjustmentBaseTime() const = 0;

  //! @brief Query car restart distance
  //! @return car restart distance [m]
  virtual units::length::meter_t GetCarRestartDistance() const = 0;

  //! @brief Obtain lateral offset the driver will keep to the center of the lane.
  //! @return lateral offset neutral position
  virtual units::length::meter_t GetLateralOffsetNeutralPosition() const = 0;

  //! @brief Obtain lateral offset to the center lane for rescue lane
  //! @return lateral offset neutral position
  virtual units::length::meter_t GetLateralOffsetNeutralPositionRescueLane() const = 0;

  //! @brief Obtain lateral velocity of a vehicle in the AoI towards the lane of the ego vehicle.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return lateral_velocity
  virtual units::velocity::meters_per_second_t GetLateralVelocityTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Obtain lateral distance of a vehicle in the AoI towards the lane of the ego vehicle.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return lateral_velocity
  virtual units::length::meter_t GetDistanceTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex = -1) const = 0;

  //! @brief Getter for the current indicator state of a observed agent
  //! @param aoi  Area of interest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return IndicatorState
  virtual scm::LightState::Indicator GetIndicatorState(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Getter for the time since the last information update of an aoi
  //! @param aoi     Area of interest
  //! @return timeSinceLastUpdate
  virtual units::time::millisecond_t GetTimeSinceLastUpdate(AreaOfInterest aoi) const = 0;

  //! @brief Getter for the urgent update threshold
  //! @return urgentUpdateThreshold
  virtual units::time::millisecond_t GetUrgentUpdateThreshold() const = 0;

  //! @brief Setter for the urgent update threshold
  //! @param threshold   Urgent update threshold
  virtual void SetUrgentUpdateThreshold(units::time::millisecond_t threshold) = 0;

  //! @brief Return the priority of gazing in the right direction, based on the time since last update.
  //! @param gs GazeState
  //! @return double
  virtual double GetPrioritySinceLastUpdate(GazeState gs) const = 0;

  //! @brief Returns the current vehicle model parameters, as stored in internal data
  //! @return Struct VehicleModelParameters of the current agent
  virtual scm::common::vehicle::properties::EntityProperties GetVehicleModelParameters() const = 0;

  //! @brief Getter for the InfrastructureCharacteristics object (read-only)
  //! @return infrastructureCollection
  virtual InfrastructureCharacteristicsInterface* GetInfrastructureCharacteristics() const = 0;

  //! @brief Returns the current driver parameters, as stored in internal data
  //! @return Struct DriverParameters of the current agent
  virtual DriverParameters GetDriverParameters() const = 0;

  //! @brief Returns if a given relativeLane is useable by the driver. In case of an emergency, the mere existence of a lane is sufficient, otherwise it must also have a valid LaneType.
  //! @param relativeLane    RelativeLane
  //! @param isEmergency     virtual bool
  //! @return True if the lane exists, false otherwise
  virtual bool GetLaneExistence(RelativeLane relativeLane, bool isEmergency = false) const = 0;

  //! @brief Check, if there is a head up display in the interior of the vehicle.
  //! @return yes/no
  virtual bool GetInteriorHudExistence() const = 0;

  //! @brief Check, if there is an infotainment system in the interior of the vehicle.
  //! @return yes/no
  virtual bool GetInteriorInfotainmentExistence() const = 0;

  //! @brief Get the mean velocity of agents in aois and mesoscopic mean velocities in left lane for actual and recent time steps in m/s.
  //! @return MeanVelocityLaneLeft
  virtual units::velocity::meters_per_second_t GetMeanVelocityLaneLeft() const = 0;

  //! @brief Get the mean velocity of agents in aois and mesoscopic mean velocitiesin in ego lane for actual and recent time steps in m/s.
  //! @return MeanVelocityLaneEgo
  virtual units::velocity::meters_per_second_t GetMeanVelocityLaneEgo() const = 0;

  //! @brief Get the mean velocity of agents in aois and mesoscopic mean velocities in right lane for actual and recent time steps in m/s.
  //! @return MeanVelocityLaneRight
  virtual units::velocity::meters_per_second_t GetMeanVelocityLaneRight() const = 0;

  //! @brief Query lateral position of the vehicle at main locate position.
  //! @return LateralPositionEgo
  virtual units::length::meter_t GetLateralPosition() const = 0;

  //! @brief Query lateral position of the vehicle at front axle.
  //! @return LateralPositionEgo
  virtual units::length::meter_t GetLateralPositionFrontAxle() const = 0;

  //! @brief Query lateral velocity of the vehicle at main locate position.
  //! @return VEgoLateral
  virtual units::velocity::meters_per_second_t GetLateralVelocity() const = 0;

  //! @brief Query lateral velocity of the vehicle at front axle.
  //! @return VEgoLateral
  virtual units::velocity::meters_per_second_t GetLateralVelocityFrontAxle() const = 0;

  //! @brief Query longitudinal velocity of the driver's own vehicle.
  //! @return VEgoLongitudinal
  virtual units::velocity::meters_per_second_t GetLongitudinalVelocityEgo() const = 0;

  //! @brief Query longitudinal velocity of a surrounding vehicle in the given aoi
  //! @param aoi
  //! @param sideAoiIndex Index of the object in the side aoi vector
  //! @return Longitudinal velocity of the velocity in the aoi
  virtual units::velocity::meters_per_second_t GetLongitudinalVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Query heading angle of the vehicle.
  //! @return HeadingAngleEgo
  virtual units::angle::radian_t GetHeading() const = 0;

  //! @brief Query curvature on current position in ego lane.
  //! @return CurvatureOnCurrentPosition
  virtual units::curvature::inverse_meter_t GetCurvatureOnCurrentPosition() const = 0;

  //! @brief Query curvature within preview distance.
  //! @return CurvatureInDistance
  virtual units::curvature::inverse_meter_t GetCurvatureInPreviewDistance() const = 0;

  //! @brief Query width of ego lane.
  //! @return LaneWidthEgo
  virtual units::length::meter_t GetEgoLaneWidth() const = 0;

  //! @brief Query legal velocity for left lane.
  //! @return VLegalLeft
  virtual units::velocity::meters_per_second_t GetVelocityLegalLeft() const = 0;

  //! @brief Query legal velocity for ego lane.
  //! @return VLegalEgo
  virtual units::velocity::meters_per_second_t GetVelocityLegalEgo() const = 0;

  //! @brief Query legal velocity for right lane.
  //! @return VLegalRight
  virtual units::velocity::meters_per_second_t GetVelocityLegalRight() const = 0;

  //! @brief Query the favoured velocity of the driver.
  //! @return VWish
  virtual units::velocity::meters_per_second_t GetDesiredVelocity() const = 0;

  //! @brief Query the acceleration of the ego vehicle
  //! @return AEgo
  virtual units::acceleration::meters_per_second_squared_t GetAcceleration() const = 0;

  //! @brief Query the delta the driver would violate a legal speed limit with
  //! @return DeltaVViolation
  virtual units::velocity::meters_per_second_t GetVelocityViolationDelta() const = 0;

  //! @brief Query the sight distance
  //! @return current preview distance
  virtual units::length::meter_t GetPreviewDistance() const = 0;

  //! @brief Set the new aoi merge regulate and discard old information
  //! @param mergeRegulate   AreaOfInterest to set as new merge regulate
  virtual void SetMergeRegulate(AreaOfInterest mergeRegulate) = 0;  // TODO check if this method and all connected logic can be removed

  //! @brief Gets the area of interest for the merge regulate
  //! @return the current MergeRegulate (NumberOfAreaOfInterests if no MergeRegulate exists)
  virtual AreaOfInterest GetMergeRegulate() const = 0;  // TODO check if this method and all connected logic can be removed

  //! @brief Query the possibility to merge instead of directly changing lanes
  //! @param aoi AreaOfInterest
  //! @return True if the side of the aoi is safe when merging is a possibility, false otherwise
  virtual bool IsSideLaneSafeWithMergePreparation(AreaOfInterest aoi) = 0;

  //! @brief Get information of current situation.
  //! @return the current situation
  virtual Situation GetCurrentSituation() const = 0;

  //! @brief Set the current situation.
  //! @param currentSituation             The current microsopic traffic situation.
  virtual void SetCurrentSituation(Situation currentSituation) = 0;

  //! @brief Query agent cooperation factor.
  //! @return the agent cooperation factor
  virtual double GetAgentCooperationFactor() const = 0;

  //! @brief Query agent factor for defensively reacting to suspicious vehicles on side lanes.
  //! @return the agent cooperation factor for suspicious behavior
  virtual double GetAgentCooperationFactorForSuspiciousBehaviourEvasion() const = 0;

  //! @brief Get the main lane id of the ego vehicle
  //! @return Id of the ego lane
  virtual int GetLaneId() const = 0;

  //! @brief Getter for the reliability map in an observed aoi
  //! @details Currently SideAoiIndex set to FRONT assuming that reltaiveLongitudinalDistance to al side Objects is 0 all have the same reliability
  //! @param aoi      Area of interest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return Times for fovea, ufov and periphery
  virtual std::map<FieldOfViewAssignment, units::time::millisecond_t> GetReliabilityMap(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Getter for tau dot for the observed aoi
  //! @param aoi      Area of interest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return Tau dot
  virtual double GetTauDot(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Get the current lateral action.
  //! @return the current lateral action
  virtual LateralAction GetLateralAction() const = 0;

  //! @brief Set the current lateral action.
  //! @param currentLateralAction The current lateral action.
  virtual void SetLateralAction(LateralAction currentLateralAction) = 0;

  //! @brief Set the current direction of vehicle movement.
  //! @param direction                    Direction of vehicle movement.
  virtual void SetDirection(int direction) = 0;

  //! @brief Get the current direction of vehicle movement.
  //! @return the surrounding lane for the direction
  virtual SurroundingLane GetCurrentDirection() const = 0;

  //! @brief Get the duration of the current situation.
  //! @return duration of the current situaiton
  virtual units::time::millisecond_t GetDurationCurrentSituation() const = 0;

  //! @brief Set the duration of the current situation.
  //! @param durationCurrentSituation   Duration of the current situation [ms].
  virtual void SetDurationCurrentSituation(units::time::millisecond_t durationCurrentSituation) = 0;

  //! @brief Get traffic signs for all known lanes
  //! @return Set of traffic sign entities for known lanes
  virtual std::vector<std::vector<scm::CommonTrafficSign::Entity>> GetTrafficSigns() const = 0;

  //! @brief Set the current time stamp of SCM
  //! @param time   Current time step of the simulation [ms].
  virtual void SetTime(units::time::millisecond_t time) = 0;

  //! @brief Queries the current time stamp
  //! @return int  Current time stamp
  virtual units::time::millisecond_t GetTime() const = 0;

  //! @brief Get velocity below which traffic is considered to be a jam
  //! @return jam velocity
  virtual units::velocity::meters_per_second_t GetTrafficJamVelocityThreshold() const = 0;

  //! @brief Get velocity for building up a rescue lane
  //! @return rescue lane velocity
  virtual units::velocity::meters_per_second_t GetFormRescueLaneVelocityThreshold() const = 0;

  //! @brief Get the indicator activation probability
  //! @return probability how likely it is that the indicater is set
  virtual double GetIndicatorActiviationProbability() const = 0;

  //! @brief Get the flasher activation probability
  //! @return probability how likely it is that the flasher is set
  virtual double GetFlasherActiviationProbability() const = 0;

  //! @brief Set the adjustment base time.
  //! @param adjustmentBaseTime   The adjustment base time.
  virtual void SetAdjustmentBaseTime(units::time::second_t adjustmentBaseTime) = 0;

  //! @brief Determine if the situation of the driver has changed.
  //! @return True if the situation has changed, false otherwise
  virtual bool GetHasSituationChanged() const = 0;

  //! @brief Determine if the mesoscopic situation of the driver has changed.
  //! @return True if the mesoscopic situation has changed, false otherwise
  virtual bool GetHasMesoscopicSituationChanged() const = 0;

  //! @brief Determine if the leading vehicle of the driver has changed.
  //! @return True if the leading vehicle has changed, false otherwise
  virtual bool HasLeadingVehicleChanged() const = 0;

  //! @brief Determine if the lateral action is forced by manipulator
  //! @return True if the lateral action is forced
  virtual bool GetIsLateralActionForced() const = 0;

  //! @brief Set the lateral action as forced by manipulator
  //! @param isForced   is forced and direction
  virtual void SetLateralActionAsForced(LaneChangeAction isForced) = 0;

  //! @brief Determine if the lange change or the sverwing action is still safe
  //! @return True if the current lateral movement is still considered as safe, false otherwise
  virtual bool GetIsLateralMovementStillSafe() const = 0;

  //! @brief Determine while ongoing lane change if the next lane has already been reached.
  //! @return True if the agent crossed the lane, false otherwise
  virtual bool GetIsLaneChangePastTransition() const = 0;

  //! @brief Get state of fulfilled lateral movement, while still in lateral movement
  //! @return True if lane change/lateral movement is completed, false otherwise
  virtual bool GetIsEndOfLateralMovement() const = 0;

  //! @brief Set state that the lateral movement has currently ended to keep apart start and end of lateral movement
  //! @param end   End of lateral movement.
  virtual void SetIsEndOfLateralMovement(bool end) = 0;

  //! @brief Getter of the isMergeRegulate flag
  //! @param aoi     Area of interest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return True if vehicle in the aoi is marked as merge regulate, false otherwise
  virtual bool GetIsMergeRegulate(AreaOfInterest aoi, int sideAoiIndex = -1) const = 0;

  //! @brief Setter for variable isAnticipating.
  //! @param anticipating    Is driver anticipating
  virtual void SetIsAnticipating(bool anticipating) = 0;

  //! @brief Getter for variable isAnticipating.
  //! @return _isAnticipating    Is driver anticipating
  virtual bool GetIsAnticipating() const = 0;

  //! @brief Refresh microscopic characteristics with calculated values, e.g. VTarget
  //! @param updateLaneMeanVelocities    Trigger update of lane mean velocity calculation or not
  virtual void CalculateMicroscopicData(bool updateLaneMeanVelocities = false) = 0;

  //! @brief Getter for the cycle time.
  //! @return Cycle time
  virtual units::time::millisecond_t GetCycleTime() const = 0;

  //! @brief Getter for the distance to the end of the next exit
  //! @return Distance to the end of the next exit
  virtual units::length::meter_t GetDistanceToEndOfNextExit() const = 0;

  //! @brief Getter for the distance to the start of the next exit
  //! @return Distance to the start of the next exit
  virtual units::length::meter_t GetDistanceToStartOfNextExit() const = 0;

  //! @brief Getter for the longitudinal action state of ego.
  //! @return longitudinalActionState
  virtual LongitudinalActionState GetLongitudinalActionState() const = 0;

  //! @brief Setter for the longitudinal action state of ego.
  //! @param state
  virtual void SetLongitudinalActionState(LongitudinalActionState state) = 0;

  //! @brief Trigger for high cognitive.
  //! @param externalControlActive flag indicating if external control is active
  virtual void TriggerHighCognitive(bool externalControlActive) = 0;

  //! @brief Get the situation of high cognitive.
  //! @return a high cognitive situation
  virtual HighCognitiveSituation GetHighCognitiveSituation() const = 0;

  //! @brief Getter for lane change prevention at spawn.
  //! @return Yes/No
  virtual bool GetLaneChangePrevention() const = 0;

  //! @brief Getter for lane change prevention at spawn.
  //! @return True if still in the lane change prevention area after spawn
  virtual bool GetLaneChangePreventionAtSpawn() const = 0;

  //! @brief Setter for lane change prevention at spawn.
  //! @param laneChangePrevention    Lane change prevention at spawn
  virtual void SetLaneChangePreventionAtSpawn(bool laneChangePrevention) = 0;

  //! @brief Getter for lane change prevention from external lateral control.
  //! @return True if external control prevents a lane change, false otherwise
  virtual bool GetLaneChangePreventionExternalControl() const = 0;

  //! @brief Setter for lane change prevention from external lateral control.
  //! @param laneChangePrevention    Lane change prevention from external lateral control
  virtual void SetLaneChangePreventionExternalControl(bool laneChangePrevention) = 0;

  //! @brief Getter for remainInLaneChangeState.
  //! @return remainInLaneChangeState
  virtual bool GetRemainInLaneChangeState() const = 0;

  //! @brief Setter for remainInLaneChangeState
  //! @param remain   Value of remainInLaneChangeState
  virtual void SetRemainInLaneChangeState(bool remain) = 0;

  //! @brief Checks whether the agent is staying in the realign state
  //! @return True if agent is still realigning, false otherwise
  virtual bool GetRemainInRealignState() const = 0;

  //! @brief Setter for the realign state
  //! @param remain
  virtual void SetRemainInRealignState(bool remain) = 0;

  //! @brief Getter for remainInSwervingState.
  //! @return remainInSwervingState
  virtual bool GetRemainInSwervingState() const = 0;

  //! @brief Setter for remainInSwervingState.
  //! @param remain   Value of remainInSwervingState
  virtual void SetRemainInSwervingState(bool remain) = 0;

  //! @brief Reset laneChangeWidthTraveled to a value of 0.
  virtual void ResetLaneChangeWidthTraveled() = 0;

  //! @brief Set laneChangeWidthTraveled
  //! @param width
  virtual void SetLaneChangeWidthTraveled(units::length::meter_t width) = 0;

  //! @brief GetLaneChangeWidthTraveled
  //! @return Returns the currently driven width of the lane change maneuver
  virtual units::length::meter_t GetLaneChangeWidthTraveled() const = 0;

  //! @brief Getter for _cooperativeBehavior.
  //! @return _cooperativeBehavior
  virtual bool GetCooperativeBehavior() const = 0;

  //! @brief Getter for the obstruction.
  //! @param aoi     Area of interest
  //! @param sideAoiIndex side aoi index
  //! @return ObstructionScm
  virtual ObstructionScm GetObstruction(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Getter for causingVehicleOfSituationFrontCluster.
  //! @param situation     Situation
  //! @return AreaOfInterest
  virtual AreaOfInterest GetCausingVehicleOfSituationFrontCluster() const = 0;

  //! @brief Setter for causingVehicleOfSituationFrontCluster, set given 'aoi' at 'situation' in causingVehicleOfSituationFrontCluster.
  //! @param situation Situation
  //! @param aoi  AreaOfInterest
  virtual void SetCausingVehicleOfSituationFrontCluster(Situation situation, AreaOfInterest aoi) = 0;

  //! @brief Reset the causing vehicle of the front cluster situations to the default aoi (NumberOfAreaOfInterests)
  virtual void ResetCausingVehicleOfFrontCluster() = 0;

  //! @brief Getter for causingVehicleOfSituationSideCluster.
  //! @param situation     Situation
  //! @return AreaOfInterest
  virtual AreaOfInterest GetCausingVehicleOfSituationSideCluster() const = 0;

  //! @brief Setter for causingVehicleOfSituationSideCluster, set given 'aoi' at 'situation' in causingVehicleOfSituationSideCluster.
  //! @param situation Situation
  //! @param aoi  AreaOfInterest
  virtual void SetCausingVehicleOfSituationSideCluster(Situation situation, AreaOfInterest aoi) = 0;

  //! @brief Reset the causing vehicle of the side clusters situations to the default aoi (NumberOfAreaOfInterests)
  virtual void ResetCausingVehicleOfSideCluster() = 0;

  //! @brief Check if 'situation' situation is a front situation.
  //! @return true if param situation belongs to category of front situations
  virtual bool IsFrontSituation() const = 0;

  //! @brief Determine the current index of a side object
  //! @param aoi the area of intereste
  //! @param criteria side aoi criteria
  //! @param parameter parameter as sorting criteria
  //! @return current valid side index
  virtual int DetermineIndexOfSideObject(AreaOfInterest aoi, SideAoiCriteria criteria, std::function<double(const SurroundingVehicleInterface*)> parameter = nullptr) const = 0;

  //! @brief Getter if Situation is anticipated
  //! @param situation
  //! @return True if the situation is anticipated, false otherwise
  virtual bool IsSituationAnticipated(Situation situation) const = 0;

  //! @brief Sets probability at position 'situation' in a map
  //! @param situation
  //! @param probability
  virtual void SetSituationAnticipated(Situation situation, bool probability) = 0;

  //! @brief Getter of Risk for a Situation
  //! @param situation
  //! @return Risk for the given situaiton
  virtual Risk GetSituationRisk(Situation situation) const = 0;

  //! @brief Sets risk at position 'situation' in a map
  //! @param situation situation
  //! @param risk risk
  virtual void SetSituationRisk(Situation situation, Risk risk) = 0;

  //! @brief Get current comfort acceleration depending on ego speed.
  //! @return Current comfort acceleration
  virtual units::acceleration::meters_per_second_squared_t GetComfortLongitudinalAcceleration() const = 0;

  //! @brief Get current comfort deceleration depending on ego speed.
  //! @return Current comfort deceleration
  virtual units::acceleration::meters_per_second_squared_t GetComfortLongitudinalDeceleration() const = 0;

  //! @brief Check whether a new solid lane marking exists and if so, than rolls the dice to see whether the agent adheres to it or not.
  virtual void CheckIsLaneChangeProhibitionIgnored() const = 0;

  //! @brief Draw and set if a lane change is prohibited due to solid lane markings based on laneChangeProhibitionIgnoringQuota.
  virtual void DrawLaneChangeProhibitionIgnored() const = 0;

  //! @brief Get the deceleration limit for merging maneuver
  //! @return Adjusted deceleration for merging
  virtual units::acceleration::meters_per_second_squared_t GetDecelerationLimitForMerging() const = 0;

  //! @brief Get the acceleration limit for merging maneuver
  //! @return Adjusted acceleration for merging
  virtual units::acceleration::meters_per_second_squared_t GetAccelerationLimitForMerging() const = 0;

  //! @brief Get the projected vehicle width
  //! @return projected vehicle width SensorDriverScmDefinitions::widthProjected
  virtual units::length::meter_t GetProjectedVehicleWidth() const = 0;

  //! @brief Get the projected vehicle width to the left
  //! @return projected vehicle width left SensorDriverScmDefinitions::widthProjectedLeft
  virtual units::length::meter_t GetProjectedVehicleWidthLeft() const = 0;

  //! @brief Get the projected vehicle width to the right
  //! @return projected vehicle width right SensorDriverScmDefinitions::widthProjectedRight
  virtual units::length::meter_t GetProjectedVehicleWidthRight() const = 0;

  //! @brief Get the projected spacial dimensions for a vehicle in a specific aoi
  //! @param aoi
  //! @param sideAoiIndex
  //! @return projected spacial dimension for the aoi
  virtual ProjectedSpacialDimensions GetProjectedDimensions(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Checks whether the current situation is considered a side situation
  //! @return true if the situation is a side situation, false otherwise
  virtual bool IsSideSituation() const = 0;

  //! @brief Checks if there is an object that should be avoided
  //! @return Object to evade from the front cluster if one exists, AreaOfInterest::EGO_FRONT otherwise
  virtual AreaOfInterest GetObjectToEvade() const = 0;

  //! @brief Get the mean velocity of agents in aois and mesoscopic mean velocities in the surrounding lane for actual and recent time steps in m/s.
  //! @param lane Surrounding lane
  //! @return MeanVelocityLane
  virtual units::velocity::meters_per_second_t GetMeanVelocity(SurroundingLane lane) const = 0;

  //! @brief Query legal velocity for given lane.
  //! @param lane
  //! @return Legal velocity of the given lane
  virtual units::velocity::meters_per_second_t GetLegalVelocity(SurroundingLane lane) const = 0;

  //! @brief Returns probability of a lane change of an E1-vehicle anticipated by Ego
  //! @return Anticipated Probability of a E1 Lane Change
  virtual double GetHighCognitiveLaneChangeProbability(void) const = 0;

  //! @brief Query absolute velocity of the driver's own vehicle (estimated or actual value).
  //! @param isEstimatedValue   estimated (true) or actual (false) value?
  //! @return Absolute velocity of the vehicle (estimated or actual value)
  virtual units::velocity::meters_per_second_t GetAbsoluteVelocityEgo(bool isEstimatedValue) const = 0;

  //! @brief Query absolute velocity of a vehicle in the corresponding AOI.
  //! @param aoi AreaOfInterest
  //! @param sideAoiIndex    Index of the object in the side aoi vector
  //! @return absolute velocity
  virtual units::velocity::meters_per_second_t GetAbsoluteVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Returns the closest laneId of the JunctionIngoing lane of an exit and the number of lanes to cross.
  //! @return Lane id and number of lanes to cross
  virtual int GetClosestRelativeLaneIdForJunctionIngoing() const = 0;

  //! @brief Get all of the internal data
  //! @return Interface to the microscopic characteristics
  virtual MicroscopicCharacteristicsInterface* GetMicroscopicData() const = 0;

  //! @brief Get the distance to the end of a lane
  //! @param relativeLaneId  relative ID of the lane to evaluate (0 = ego, -1 = right, 1 = left)
  //! @param isEmergency     flag to indicate, if lane sections should be considered, which are usually not allowed to drive on
  //! @return distance to end of required lane in m
  virtual units::length::meter_t GetDistanceToEndOfLane(int relativeLaneId, bool isEmergency = false) const = 0;

  //! @brief Get the distance to the end of a lane
  //! @param relativeLane relative lane to evaluate
  //! @param isEmergency flag to indicate, if lane sections should be considered, which are usually not allowed to drive on
  //! @return distance to end of required lane in [m] for LEFT, RIGHT, and EGO, otherwise INFINITY
  virtual units::length::meter_t GetDistanceToEndOfLane(RelativeLane relativeLane, bool isEmergency = false) const = 0;

  //! @brief Query car queueing distance
  //! @return car queueing distance
  virtual units::length::meter_t GetCarQueuingDistance() const = 0;

  //! @brief Getter function for the distance where the agent starts to be influenced in his action.
  //! @return Influencing distance
  virtual units::length::meter_t GetInfluencingDistanceToEndOfLane() const = 0;

  //! @brief Gets the longitudinal obstruction of an agent in the specified aoi
  //! @param aoi
  //! @param sideAoiIndex
  //! @return Return the LongitudinalObstruction of the vehicle in the given aoi if one exists, ObstructionLongitudinal::NoOpponent() otherwise
  virtual ObstructionLongitudinal GetLongitudinalObstruction(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Getter for the TrafficRuleInformation struct.
  //! @return trafficRuleInformation
  virtual TrafficRuleInformationScmExtended* GetTrafficRuleInformation() const = 0;

  //! @brief Checks if at least one corner of the given bounding box is in the current cone of sight
  //! @param boundingBox          BoundingBox to check for visibility
  //! @param gazeConeOpeningAngle Opening angle of the gaze cone to be used to check for visibility
  //! @return True if the bounding box contains at least one visible corner point
  virtual bool IsVisible(const Scm::BoundingBox& boundingBox, units::angle::radian_t gazeConeOpeningAngle) const = 0;

  //! @brief Checks whether the given point is visible
  //! @param point
  //! @return true if the point is inside the current gaze cone and within the visibility distance, false otherwise
  virtual bool IsPointVisible(const Scm::Point& point) const = 0;

  //! @brief Setter for the horizontal gaze angle
  //! @param gazeAngle
  virtual void SetHorizontalGazeAngle(units::angle::radian_t gazeAngle) = 0;

  //! @brief Returns the current gaze angle of the FOVEA perception or - Pi if FOVEA is in Rear, since gaze angle is invalid then
  //! @return horizontal gaze angle
  virtual units::angle::radian_t GetHorizontalGazeAngle() const = 0;

  //! @brief Setter for the list of possible collision course vehicle (i.e. vehicles to regulate on)
  //! @param collisionCourseAgentIds
  virtual void SetPossibleRegulateIds(const std::vector<int>& collisionCourseAgentIds) = 0;

  //! @brief Getter for the possible regulate ids
  //! @return list of agent ids to regulate on
  virtual std::vector<int> GetPossibleRegulateIds() const = 0;

  //! @brief Get the id of the leading vehicle
  //! @return leading vehicle id
  virtual int GetLeadingVehicleId() const = 0;

  //! @brief Set the id of the leading vehicle
  //! @param id
  virtual void SetLeadingVehicleId(int id) = 0;

  //! @brief Returns SurroundingVehicle object identified by the given Id or nullptr if object does not exist
  //! @param id
  //! @return the surrounding vehicle of the given id
  virtual const SurroundingVehicleInterface* GetVehicle(int id) const = 0;

  //! @brief Returns SurroundingVehicle object in the given Aoi or nullptr if object does not exist
  //! @param aoi
  //! @param sideAoiIndex
  //! @return the surrounding vehicle of the given aoi
  virtual const SurroundingVehicleInterface* GetVehicle(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Returns the current leading vehicle as a SurroundingVehicle object or nullptr if no leading vehicle is set
  //! @return current leading vehicle
  virtual const SurroundingVehicleInterface* GetLeadingVehicle() const = 0;

  //! @brief Set the given mesoscopic situation as active
  //! @param situation to activate
  virtual void ActivateMesoscopicSituation(MesoscopicSituation situation) = 0;

  //! @brief Set the given mesoscopic situation as inactive
  //! @param situation to deactivate
  virtual void DeactivateMesoscopicSituation(MesoscopicSituation situation) = 0;

  //! @brief Checks whether a mesoscopic situation is currently active
  //! @param situation
  //! @return Returns true if the given mesoscopic situation is currently active
  virtual bool IsMesoscopicSituationActive(MesoscopicSituation situation) const = 0;

  //! @brief Gets the vector of activa mesoscopic situations
  //! @return Returns a vector of all currently active mesoscopic situations
  virtual std::vector<MesoscopicSituation> GetActiveMesoscopicSituations() const = 0;

  //! @brief Setter for boolean Flag, indicating if a zip merge maneuver is currently active
  //! @param zipMerge
  virtual void SetZipMerge(bool zipMerge) = 0;

  //! @brief Checks whether there is currently a zip merge maneuver active
  //! @return true if active, false otherwise
  virtual bool GetZipMerge() const = 0;

  //! @brief Sets the action as buffer state (allows checking in future if the lateral action has changed)
  //! @param action
  virtual void SetLastLateralAction(const LateralAction& action) = 0;

  //! @brief Gets the last lateral action
  //! @return previously set lastLateralAction
  virtual LateralAction GetLastLateralActionState() const = 0;

  //! @brief Getter for current virtual vehicles
  //! @return a list of vehicles considered as 'virtual'
  virtual VirtualAgentsSCM GetVirtualAgents() const = 0;

  //! @brief Get the surrounding vehicles
  //! @return list of vehicles in the surroundings of ego
  virtual std::vector<const SurroundingVehicleInterface*> GetSurroundingVehicles() const = 0;

  //! @brief Get the surrounding vehicles for a specific relative lange
  //! @param relativeLane the relative lane for this query
  //! @return list of vehicles in the surroundings of ego on the relative lane
  virtual std::vector<const SurroundingVehicleInterface*> GetSurroundingVehicles(RelativeLane relativeLane) const = 0;

  //! @brief Get the surrounding vehicles of ego so that they can be modified
  //! @return list of vehicles in the surroundings of ego
  virtual SurroundingVehicles* UpdateSurroundingVehicles() = 0;

  //! @brief Setter for the Aoi to vehicle map
  //! @param mapping
  virtual void SetAoiMapping(const AoiVehicleMapping& mapping) = 0;

  //! @brief Gets the threshold looming for a specific aoi
  //! @param aoi
  //! @param sideAoiIndex
  //! @return the threshold looming of a surrounding vehicle in the given aoi
  virtual double GetThresholdLooming(AreaOfInterest aoi, int sideAoiIndex = 0) const = 0;

  //! @brief Get the VisibilityDistance from InfrastructureCharacteristics
  //! @return the visibility distance
  virtual units::length::meter_t GetVisibilityDistance() const = 0;

  //! @brief Todo: not sure what this is meant for!
  //! @return the visibility factor for a highway exit
  virtual double GetVisibilityFactorForHighwayExit() const = 0;

  //! @brief Updates the visual reliability map of agents, i.e. progressed the reliability of all surrounding vehicles in the mental model
  virtual void UpdateVisualReliabilityMap() = 0;

  //! @brief Gets the visual reliability map for a given visual perception sector
  //! @param sector
  //! @return current reliability for a sector
  virtual double GetVisualReliability(VisualPerceptionSector sector) const = 0;

  //! @brief Checks whether a given sector suffices a required quality
  //! @param sector
  //! @param requiredQuality
  //! @return True if the required quality is less than the reliability in the given sector, false otherwise
  virtual bool IsVisualSectorReliable(VisualPerceptionSector sector, double requiredQuality) const = 0;

  //! @brief Gets the merging ego information
  //! @return Merge::EgoInfo
  virtual Merge::EgoInfo GetEgoInfo() const = 0;

  //! @brief Sets a merge gap
  //! @param mergeGap
  virtual void SetMergeGap(Merge::Gap mergeGap) = 0;

  //! @brief Gets the merge gap
  //! @return merge gap
  virtual Merge::Gap GetMergeGap() const = 0;

  //! @brief Setter for the outer keeping quota is fulfilled
  //! @param outerKeeping
  virtual void SetOuterKeepingQuotaFulfilled(bool outerKeeping) = 0;

  //! @brief Checks whether the outer keeping quota is fulfilled
  //! @return True if quota is fulfilled, false otherwise
  virtual bool GetOuterKeepingQuotaFulfilled() const = 0;

  //! @brief Setter for the ego lane keeping quota
  //! @param egoLaneKeeping
  virtual void SetEgoLaneKeepingQuotaFulfilled(bool egoLaneKeeping) = 0;

  //! @brief Checks whether the ego lane keeping quota is fulfilled
  //! @return True if quota is fulfiled, false otherwise
  virtual bool GetEgoLaneKeepingQuotaFulfilled() const = 0;

  //! @brief Checks whether right hand traffic is applicable
  //! @return True if the current road has right hand traffic, false otherwise
  virtual bool IsRightHandTraffic() const = 0;

  //! @brief Gets the Acoustic and Optical ADAS signal
  //! @return the signal
  virtual AdasHmiSignal GetCombinedAcousticOpticSignal() const = 0;

  //! @brief Sets the Acoustic and Optical ADAS signal
  //! @param acousticAndHapticAdasSignals
  virtual void SetCombinedAcousticOpticSignal(AdasHmiSignal acousticAndHapticAdasSignals) = 0;

  //! @brief Stochastically draws if the agent should use the shoulder lane in certain sitautions
  virtual void DrawShoulderLaneUsage() const = 0;

  //! @brief Checks if a given relative lane will be considered for usage even though it's a shoulder lane.
  //! @param relativeLane
  //! @return True if the use of the shoulder lane is legit, false otherwise
  virtual bool IsShoulderLaneUsageLegit(RelativeLane relativeLane) const = 0;

  //! @brief Getter for planned lane change dimensions
  //! @return dimension for a planned lane change if one exists, std::nullopt otherwise
  virtual const std::optional<TrajectoryPlanning::TrajectoryDimensions>& GetPlannedLaneChangeDimensions() const = 0;

  //! @brief Setter for a planned lane change dimension
  //! @param dimensions
  virtual void SetPlannedLaneChangeDimensions(const TrajectoryPlanning::TrajectoryDimensions& dimensions) = 0;

  //! @brief Resets the planned lane change dimension to std::nullopt
  virtual void ResetPlannedLaneChangeDimensions() = 0;

  //! @brief Returns the inverse urgency factor used to calculate reduced distances acceptable under urgency.
  //! @return the reduction factor
  virtual double GetUrgencyReductionFactor() const = 0;

  //! @brief Gets a list of surrounding vehicles mapped to the given aoi
  //! @param aoi
  //! @return list of surrounding vehicles for the aoi
  virtual std::vector<const SurroundingVehicleInterface*> GetVehicleVector(AreaOfInterest aoi) const = 0;

  //! @brief determine if an agent is distracted
  virtual void DrawDistractionUsage() const = 0;

  //! @brief Returns a map debug values; for developer usage
  //! @return map of logged debug values
  virtual std::map<std::string, std::string> GetDebugValues() const = 0;

  //! @brief Adds a debug value for the output; for developer usage
  //! @param description the description/tag/key to log
  //! @param value the corresponding value
  virtual void AddDebugValue(const std::string& description, const std::string& value) = 0;

  //! @brief Gets the inverse of the urgency factor for lane change
  //! @return 1 - MentalCalculations::GetUrgencyFactorForLaneChange
  virtual double GetReductionFactorUnderUrgency() const = 0;

  //! @brief Calculate lateral distance left
  //! @param laneChangeWidth
  //! @return lateral distance left
  virtual units::length::meter_t DetermineDistanceTraveledLeft(units::length::meter_t laneChangeWidth) = 0;

  //! @brief Calculate lateral distance right
  //! @param laneChangeWidth
  //! @return lateral distance right
  virtual units::length::meter_t DetermineDistanceTraveledRight(units::length::meter_t laneChangeWidth) = 0;

  //! @brief Has ego lateral distance reached
  //! @param desiredLateralDisplacement
  //! @param lateralPositionSet
  //! @return true ego has lateral distance reached otherwise false
  virtual bool HasEgoLateralDistanceReached(units::length::meter_t desiredLateralDisplacement, units::length::meter_t lateralPositionSet) const = 0;
};
