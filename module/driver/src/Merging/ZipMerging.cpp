/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ZipMerging.h"

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <vector>

#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/ScmMathUtils.h"
#include "include/common/SensorDriverScmDefinitions.h"

ZipMerging::ZipMerging(MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, std::shared_ptr<const LaneChangeDimensionInterface> laneChangeDimension)
    : _mentalModel{mentalModel},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations},
      _laneChangeDimension{laneChangeDimension}
{
}

units::length::meter_t ZipMerging::EstimateMergePoint(const ZipMergeParameters& params) const
{
  const auto surroundingLane{RelativeLane2SurroundingLane.at(params.relativeLane)};
  const auto meanLaneVelocity{params.active ? _mentalModel.GetMeanVelocity(surroundingLane) : _mentalModel.GetMeanVelocityLaneEgo()};
  const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(params.active ? RelativeLane::EGO : params.relativeLane)};
  const auto laneChangeWidth{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width};
  const auto lateralAcc{_mentalModel.GetDriverParameters().comfortLateralAcceleration};
  const auto laneChangeLength = _laneChangeDimension->EstimateLaneChangeLength(laneChangeWidth, lateralAcc, meanLaneVelocity);

  constexpr units::time::second_t SAFETY_TIME{1.0_s};
  constexpr auto MIN_SAFETY_DISTANCE{10.0_m};
  const units::length::meter_t safetyBuffer{units::math::max(MIN_SAFETY_DISTANCE, meanLaneVelocity * SAFETY_TIME)};
  return distanceToEndOfLane - laneChangeLength - safetyBuffer;
}

units::length::meter_t ZipMerging::CalculateActivationDistance() const
{
  constexpr auto MIN_ACTIVATION_DISTANCE{10.0_m};
  constexpr auto MAX_ACTIVATION_DISTANCE{100.0_m};
  constexpr auto MIN_DISTANCE_VELOCITY = units::velocity::meters_per_second_t(10.0_kph);

  const auto trafficJamThreshold{_mentalModel.GetTrafficJamVelocityThreshold()};
  const auto egoVelocity{_mentalModel.GetAbsoluteVelocityEgo(false)};
  const auto activationDistanceFromMergePoint{ScmMathUtils::LinearInterpolation(egoVelocity, MIN_DISTANCE_VELOCITY, trafficJamThreshold, MIN_ACTIVATION_DISTANCE, MAX_ACTIVATION_DISTANCE)};

  return std::clamp(activationDistanceFromMergePoint, MIN_ACTIVATION_DISTANCE, MAX_ACTIVATION_DISTANCE);
}

bool ZipMerging::ActivationDistanceReached(const ZipMergeParameters& params) const
{
  const auto distanceToMergePoint{EstimateMergePoint(params)};
  const auto activationDistanceFromMergePoint{CalculateActivationDistance()};
  return distanceToMergePoint < activationDistanceFromMergePoint;
}

void ZipMerging::UpdateMesoscopicSituation()
{
  auto currentLaneBlocked = _mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED);
  auto leftLaneBlocked = _mentalModel.GetLaneExistence(RelativeLane::LEFT) && !_featureExtractor.IsLaneDriveablePerceived(RelativeLane::LEFT);
  auto rightLaneBlocked = _mentalModel.GetLaneExistence(RelativeLane::RIGHT) && !_featureExtractor.IsLaneDriveablePerceived(RelativeLane::RIGHT);
  auto neighbourLaneBlocked = leftLaneBlocked || rightLaneBlocked;

  bool egoLaneBelowJamSpeed{_mentalModel.GetMeanVelocity(SurroundingLane::EGO) <= _mentalModel.GetTrafficJamVelocityThreshold()};
  bool leftLaneBelowJamSpeed{_mentalModel.GetMeanVelocity(SurroundingLane::LEFT) <= _mentalModel.GetTrafficJamVelocityThreshold()};
  bool rightLaneBelowJamSpeed{_mentalModel.GetMeanVelocity(SurroundingLane::RIGHT) <= _mentalModel.GetTrafficJamVelocityThreshold()};

  bool anyLaneBelowJamSpeed{egoLaneBelowJamSpeed || leftLaneBelowJamSpeed || rightLaneBelowJamSpeed};

  if (!anyLaneBelowJamSpeed)
  {
    Reset();
    return;
  }

  if (currentLaneBlocked)
  {
    _mentalModel.ActivateMesoscopicSituation(MesoscopicSituation::ACTIVE_ZIP_MERGING);
  }
  else if (_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING) && !neighbourLaneBlocked)
  {
    _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::ACTIVE_ZIP_MERGING);
  }

  if (neighbourLaneBlocked && !currentLaneBlocked && !_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING))
  {
    _mentalModel.ActivateMesoscopicSituation(MesoscopicSituation::PASSIVE_ZIP_MERGING);
  }
  else
  {
    _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::PASSIVE_ZIP_MERGING);
  }
}

RelativeLane ZipMerging::DetermineRelativeLane(bool activeMerger) const
{
  if (activeMerger)
  {
    auto rightLaneBlocked = _mentalModel.GetLaneExistence(RelativeLane::RIGHT) && !_featureExtractor.IsLaneDriveablePerceived(RelativeLane::RIGHT);
    return !_mentalModel.GetLaneExistence(RelativeLane::RIGHT) || rightLaneBlocked ? RelativeLane::LEFT : RelativeLane::RIGHT;
  }

  const auto leftLaneBlocked = _mentalModel.GetLaneExistence(RelativeLane::LEFT) && !_featureExtractor.IsLaneDriveablePerceived(RelativeLane::LEFT);
  return leftLaneBlocked ? RelativeLane::LEFT : RelativeLane::RIGHT;
}

double ZipMerging::DetermineFollowingDistanceFactor(const ZipMergeParameters& params) const
{
  if (params.active)
  {
    return 0.0;
  }

  auto egoFront = _mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT);
  const auto mergePoint{EstimateMergePoint(params)};
  if (egoFront != nullptr)
  {
    return units::math::max(0.0_m,
                            (2 * _mentalCalculations.GetMinDistance(AreaOfInterest::EGO_FRONT) + _mentalModel.GetVehicleLength() - egoFront->GetRelativeNetDistance().GetValue())) /
           (_mentalModel.GetDistanceToEndOfLane(params.relativeLane) - mergePoint);
  }

  return 0.0;
}

ZipMergeParameters ZipMerging::DetermineZipMergeParameters() const
{
  const bool currentLaneBlocked{_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED)};
  ZipMergeParameters zmp;
  zmp.active = currentLaneBlocked;
  zmp.relativeLane = DetermineRelativeLane(zmp.active);
  zmp.distanceFactor = DetermineFollowingDistanceFactor(zmp);

  constexpr auto LOOKBACK_DISTANCE{100.0_m};
  const auto relativeLaneForMergingVehicles{zmp.active ? RelativeLane::EGO : zmp.relativeLane};
  const auto allVehicles = _mentalModel.GetSurroundingVehicles(relativeLaneForMergingVehicles);
  std::vector<const SurroundingVehicleInterface*> vehicles{};
  std::copy_if(allVehicles.cbegin(), allVehicles.cend(), std::back_inserter(vehicles), [&](const SurroundingVehicleInterface* vehicle)
               {
                const auto distanceVehicleToEndOfLane {_mentalModel.GetDistanceToEndOfLane(relativeLaneForMergingVehicles) - vehicle->GetRelativeLongitudinalPosition().GetValue()};
                return distanceVehicleToEndOfLane < LOOKBACK_DISTANCE; });
  for (const auto* vehicle : vehicles)
  {
    zmp.mergingAgentIds.push_back(vehicle->GetId());
  }
  return zmp;
}

ZipMergeParameters ZipMerging::GetZipMergeParameters() const
{
  return _zipMergeParameters;
}

void ZipMerging::SetZipMergeParameters(const ZipMergeParameters& ZipMergeParameters)
{
  _zipMergeParameters = ZipMergeParameters;
}

void ZipMerging::Reset()
{
  _zipMergeParameters = ZipMergeParameters{};
  _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::ACTIVE_ZIP_MERGING);
  _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::PASSIVE_ZIP_MERGING);
}

units::acceleration::meters_per_second_squared_t ZipMerging::CalculateFollowingAcceleration(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const
{
  if (_zipMergeParameters.active)
  {
    return CalculateFollowingAccelerationActive(maxAcceleration, maxDeceleration);
  }

  return CalculateFollowingAccelerationPassive(maxAcceleration, maxDeceleration);
}

const SurroundingVehicleInterface* ZipMerging::GetLeadingVehicleActive() const
{
  // if already on target lane, use ego front as leading vehicle
  if (units::math::isinf(_mentalModel.GetDistanceToEndOfLane(RelativeLane::EGO)))
  {
    return nullptr;
  }

  const auto relativeLane{_zipMergeParameters.relativeLane};
  const auto frontAoi{relativeLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT};
  const auto sideAoi{relativeLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE};
  const auto sideFrontVehicle{_mentalModel.GetVehicle(frontAoi)};
  const auto sideVehicle{_mentalModel.GetVehicle(sideAoi)};

  if (sideFrontVehicle != nullptr)
  {
    const auto sideFrontVehicleWasMerger{std::any_of(begin(_zipMergeParameters.mergingAgentIds), end(_zipMergeParameters.mergingAgentIds), [sideFrontVehicle](const int id)
                                                     { return sideFrontVehicle->GetId() == id; })};

    const auto* egoFrontVehicle{_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)};
    bool egoFrontMergesFirst{false};
    if (egoFrontVehicle != nullptr)
    {
      if (egoFrontVehicle->FrontDistance().GetValue() < sideFrontVehicle->RearDistance().GetValue() &&
          egoFrontVehicle->GetLongitudinalVelocity().GetValue() <= sideFrontVehicle->GetLongitudinalVelocity().GetValue())
      {
        egoFrontMergesFirst = true;
      }
    }

    if (!sideFrontVehicleWasMerger && !egoFrontMergesFirst)
    {
      return sideFrontVehicle;
    }
  }

  return sideVehicle;
}

units::acceleration::meters_per_second_squared_t ZipMerging::GetFollowingAccelerationFromVehicleActive(const SurroundingVehicleInterface* vehicle, units::acceleration::meters_per_second_squared_t maxDeceleration) const
{
  if (vehicle == nullptr)
  {
    throw std::out_of_range("ZipMerging::GetFollowingAccelerationFromVehicleActive called with nullptr");
  }

  const auto minDistance{_mentalCalculations.GetMinDistance(vehicle)};
  const auto followingOffset{units::math::min(0.0_m, _mentalModel.GetCarQueuingDistance() - minDistance / 2)};
  return _mentalCalculations.CalculateFollowingAcceleration(*vehicle, maxDeceleration, followingOffset);
}

units::acceleration::meters_per_second_squared_t ZipMerging::CalculateFollowingAccelerationActive(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const
{
  units::acceleration::meters_per_second_squared_t accelerationWishMergingVehicle{ScmDefinitions::INF_ACCELELERATION};
  units::acceleration::meters_per_second_squared_t accelerationWishEgoFrontVehicle{ScmDefinitions::INF_ACCELELERATION};

  const auto* leadingVehicle{GetLeadingVehicleActive()};
  if (leadingVehicle != nullptr)
  {
    accelerationWishMergingVehicle = std::clamp(GetFollowingAccelerationFromVehicleActive(leadingVehicle, maxDeceleration), -maxDeceleration, maxAcceleration);
  }

  const auto egoFrontVehicle{_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)};
  if (egoFrontVehicle != nullptr)
  {
    accelerationWishEgoFrontVehicle = std::clamp(GetFollowingAccelerationFromVehicleActive(egoFrontVehicle, maxDeceleration), -maxDeceleration, maxAcceleration);
  }

  return std::min(accelerationWishEgoFrontVehicle, accelerationWishMergingVehicle);
}

bool ZipMerging::EgoFrontVehicleWasMerger(const SurroundingVehicleInterface& egoFrontVehicle) const
{
  auto zmpIds = _zipMergeParameters.mergingAgentIds;
  bool egoFrontWasMerger = std::any_of(zmpIds.cbegin(), zmpIds.cend(), [&](int id)
                                       { return id == egoFrontVehicle.GetId(); });

  return egoFrontWasMerger;
}

units::length::meter_t ZipMerging::CalculateFollowingOffsetForMergerInFront(const SurroundingVehicleInterface& egoFrontVehicle) const
{
  const auto minDistance{_mentalCalculations.GetMinDistance(&egoFrontVehicle)};
  const auto followingOffset{std::min(0.0_m, _mentalModel.GetCarQueuingDistance() - minDistance)};
  return followingOffset;
}

units::length::meter_t ZipMerging::CalculateFollowingOffsetForPassiveVehicleInFront(const SurroundingVehicleInterface& egoFrontVehicle) const
{
  const auto mergePoint = EstimateMergePoint(_zipMergeParameters);
  const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(_zipMergeParameters.relativeLane)};
  std::vector<const SurroundingVehicleInterface*> relevantVehicles{};
  auto surroundingVehicles = _mentalModel.GetSurroundingVehicles(_zipMergeParameters.relativeLane);
  std::copy_if(surroundingVehicles.cbegin(), surroundingVehicles.cend(), std::back_inserter(relevantVehicles), [mergePoint, distanceToEndOfLane](const SurroundingVehicleInterface* vehicle)
               {
                  static constexpr auto MAX_DISTANCE_TO_MERGE_POINT{50_m};
                  static constexpr auto LOOKBACK_DISTANCE{-1_m}; // only consider vehicles with front slightly behind ego front
                  const auto relativeDistance{vehicle->GetRelativeLongitudinalPosition().GetValue()};
                  const auto distanceToMergePoint{mergePoint - relativeDistance};
                  return (distanceToMergePoint < MAX_DISTANCE_TO_MERGE_POINT) &&
                         relativeDistance > LOOKBACK_DISTANCE; });

  if (relevantVehicles.empty())
  {
    return 0.0_m;
  }

  const auto requiredFallbackDistanceNextMerger{_mentalCalculations.GetMinDistance(relevantVehicles.at(0)) - relevantVehicles.at(0)->GetRelativeLongitudinalPosition().GetValue()};
  const auto offsetToMinDistanceFront{egoFrontVehicle.RearDistance().GetValue() - _mentalCalculations.GetMinDistance(&egoFrontVehicle)};
  return units::math::max((distanceToEndOfLane - mergePoint) * _zipMergeParameters.distanceFactor, offsetToMinDistanceFront + requiredFallbackDistanceNextMerger);
}

units::length::meter_t ZipMerging::DetermineFollowingOffsetPassive(const SurroundingVehicleInterface* egoFrontVehicle) const
{
  if (egoFrontVehicle == nullptr)
  {
    return 0_m;
  }

  if (EgoFrontVehicleWasMerger(*egoFrontVehicle))
  {
    return CalculateFollowingOffsetForMergerInFront(*egoFrontVehicle);
  }

  return CalculateFollowingOffsetForPassiveVehicleInFront(*egoFrontVehicle);
}

units::acceleration::meters_per_second_squared_t ZipMerging::CalculateFollowingAccelerationPassive(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const
{
  const auto* egoFrontVehicle{_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)};
  const auto* detectedLaneChanger{GetDetectedLaneChanger(_zipMergeParameters.relativeLane)};
  if (egoFrontVehicle == nullptr && detectedLaneChanger == nullptr)
  {
    return ScmDefinitions::INF_ACCELELERATION;
  }
  const auto followingOffset{DetermineFollowingOffsetPassive(detectedLaneChanger)};
  const auto followingAccelerationLaneChanger{detectedLaneChanger != nullptr ? _mentalCalculations.CalculateFollowingAcceleration(*detectedLaneChanger, maxDeceleration, followingOffset) : ScmDefinitions::INF_ACCELELERATION};

  const auto followingOffsetEgoFront{DetermineFollowingOffsetPassive(egoFrontVehicle)};
  const auto followingAccelerationFrontVehicle{egoFrontVehicle != nullptr ? _mentalCalculations.CalculateFollowingAcceleration(*egoFrontVehicle, maxDeceleration, followingOffsetEgoFront) : ScmDefinitions::INF_ACCELELERATION};

  const auto followingAcceleration{units::math::min(followingAccelerationLaneChanger, followingAccelerationFrontVehicle)};
  return std::clamp(followingAcceleration, -maxDeceleration, maxAcceleration);
}

const SurroundingVehicleInterface* ZipMerging::GetDetectedLaneChanger(RelativeLane lane) const
{
  const auto situation{_mentalModel.GetCurrentSituation()};
  if ((lane == RelativeLane::LEFT && situation == Situation::LANE_CHANGER_FROM_LEFT) ||
      (lane == RelativeLane::RIGHT && situation == Situation::LANE_CHANGER_FROM_RIGHT))
  {
    const auto* causingVehicle{_mentalModel.GetVehicle(_mentalModel.GetCausingVehicleOfSituationSideCluster())};
    return causingVehicle;
  }
  return nullptr;
}