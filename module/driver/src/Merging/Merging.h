/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file Merging.h

#pragma once

#include <tuple>
#include <vector>

#include "FeatureExtractorInterface.h"
#include "LongitudinalCalculations/LongitudinalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "Merging/MergingInterface.h"

//! @brief Class responsible for planning merging maneuver on a highway.
//! @details A merging maneuver is useful when an agent wants to do a lane change but cannot do it cause it would not be safe.
//! Then, a merging maneuver can be planned, identifying possible gaps between other agents where the Ego could be accelerate
//! or decelerate to reach a certain point in space and time so that a lane change would be possible.
class Merging : public MergingInterface
{
public:
  Merging(const MentalModelInterface& mentalModel, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory, const FeatureExtractorInterface& featureExtractor);
  virtual ~Merging() = default;

  void DrawNewAccelerationLimits(StochasticsInterface* stochastics) override;
  units::acceleration::meters_per_second_squared_t GetDecelerationLimitForMerging() const override;
  units::acceleration::meters_per_second_squared_t GetAccelerationLimitForMerging() const override;
  Merge::Gap RetrieveMergeGap(RelativeLane relativeLane, const std::vector<const SurroundingVehicleInterface*>& surroundingVehicles) override;

private:
  const MentalModelInterface& _mentalModel;
  const SurroundingVehicleQueryFactoryInterface& _surroundingVehicleQueryFactory;
  const FeatureExtractorInterface& _featureExtractor;

  units::acceleration::meters_per_second_squared_t _mergingAcceleration;
  units::acceleration::meters_per_second_squared_t _mergingDeceleration;
  std::unique_ptr<LongitudinalCalculationsInterface> _longitudinalCalculations;

protected:
  //! @brief Determine velocity limits (min and max)
  //! @param vEgo
  //! @returns vMin and vMax
  Merge::VelocityLimits DetermineVelocityLimits(units::velocity::meters_per_second_t vEgo) const;

  //! @brief Calls the calculations for the velocity, safety clearances to leader and follower, the regulating vehicle
  //! and the distance for a gap.
  void CalculateGapVelocitiesAndDistances(Merge::Gap& gap) const;

  //! @brief Determines the velocity of the gap (i.e. the point behind the leader/ahead of the follower ego aims to
  //! reach).
  void DetermineGapVelocity(Merge::Gap& gap) const;

  //! @brief Calculates the safe distances to the leader and follower of the gap ego should keep after having changed
  //! into the lane of the gap.
  void CalculateGapClearances(Merge::Gap& gap) const;

  //! @brief Select the regulating vehicle for the gap and calculate the relative distance to the point ego wnats to
  //! change lanes at.
  void CalculateGapDistance(Merge::Gap& gap) const;

  //! @brief Determines the safe distance to keep to a surrounding vehicle that in in front or behind of a merge gap.
  //! @param vehicle the relevant vehicle for the calculation
  //! @param perspective the position relative to the vehicle (ahead/behind) ego is aiming for
  //! @param gapVelocity the velocity of the gap, which ego aims to reach at the end of the merging manoeuvre
  units::length::meter_t CalculateSafetyClearanceForMerging(const SurroundingVehicleInterface& vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t gapVelocity) const;

  //! @brief Determines all necessary values for a differential merging manoeuvre.
  void PlanDifferentialMerge(Merge::Gap& gap) const;

  //! @brief Calculates the velocity difference ego could overcome in the space of given gap.
  void CalculateDifferentialMergingVelocity(Merge::Gap& gap) const;

  //! @brief Adjusts the differential merging velocity to a reachable value considering the distance to the gap.
  void LimitDifferentialMergingVelocity(Merge::Gap& gap) const;

  //! @brief Calculates the manoeuvre details for a differential merge.
  void CalculateDifferentialMergingPhases(Merge::Gap& gap) const;

  //! @brief Determines reachabilities for the given gap (can be reachable by accelerating, decelerating or constant driving)
  //! @param gap
  void DetermineReachabilities(Merge::Gap& gap);

  //! @brief Adds constant driving phases to the merge gap
  //! @param gap
  void ReachabilityConstantDriving(Merge::Gap& mergeGap);

  //! @brief Adds regular merge phases by accelerating to the merge gap
  //! @param gap
  void ReachabilityAccelerating(Merge::Gap& mergeGap);

  //! @brief Adds regular merge phases by decelerating to the merge gap
  //! @param gap
  void ReachabilityDecelerating(Merge::Gap& mergeGap);

  //! @brief Determine the most appropriate set of merging phases and set `selectedMergingPhases` accordingly.
  void SelectMergingPhases(Merge::Gap& gap);

  //! @brief Determines the maximum velocity for the merging maneuver
  //! @param vEgo
  //! @returns vMax
  units::velocity::meters_per_second_t DetermineVMax(units::velocity::meters_per_second_t vEgo) const;

  //! @brief Remove merge gaps when they're too close to an end of lane
  //! @param allGaps
  //! @returns filtered vector of merge gaps
  std::vector<Merge::Gap> RemoveGapsCloseToEndOfLane(std::vector<Merge::Gap> allGaps) const;

  //! @brief Gathers and sets all needed information for the given merge gaps
  //! @param mergeGaps vector of possible merge gaps
  void DetermineGapInformation(std::vector<Merge::Gap>& mergeGaps);
};
