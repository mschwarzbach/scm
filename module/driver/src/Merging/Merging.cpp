/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Merging.h"

#include <Stochastics/StochasticsInterface.h>

#include <numeric>

#include "LaneQueryHelper.h"
#include "LongitudinalCalculations/LongitudinalCalculations.h"

Merging::Merging(const MentalModelInterface& mentalModel, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory, const FeatureExtractorInterface& featureExtractor)
    : _mentalModel(mentalModel),
      _surroundingVehicleQueryFactory(surroundingVehicleQueryFactory),
      _featureExtractor(featureExtractor),
      _mergingAcceleration{ScmDefinitions::DEFAULT_VALUE},
      _mergingDeceleration{ScmDefinitions::DEFAULT_VALUE}
{
  _longitudinalCalculations = std::make_unique<LongitudinalCalculations>(_mentalModel);
}

void Merging::DrawNewAccelerationLimits(StochasticsInterface* stochastics)
{
  const auto scaling = stochastics->GetUniformDistributed(3, 4);
  _mergingAcceleration = scaling * _mentalModel.GetDriverParameters().comfortLongitudinalAcceleration;
  _mergingDeceleration = 1.5 * _mentalModel.GetDriverParameters().comfortLongitudinalDeceleration;
}

units::acceleration::meters_per_second_squared_t Merging::GetDecelerationLimitForMerging() const
{
  return _mergingDeceleration;
}
units::acceleration::meters_per_second_squared_t Merging::GetAccelerationLimitForMerging() const
{
  return _mergingAcceleration;
}

//! @brief Remove merge gaps where the available merge space is less than needed for a safe merging maneuver
//! @param allGaps
//! @returns filtered vector of merge gaps
std::vector<Merge::Gap> RemoveTooNarrowGaps(const std::vector<Merge::Gap>& allGaps, units::length::meter_t egoLength)
{
  std::vector<Merge::Gap> toKeep{};
  std::copy_if(allGaps.cbegin(), allGaps.cend(), std::back_inserter(toKeep), [&](const Merge::Gap& gap)
               {
                 const auto spaceNeeded{gap.safetyClearanceToLeader + egoLength + gap.safetyClearanceToFollower};
                 return spaceNeeded < gap.fullMergeSpace && !gap.selectedMergePhases.empty();  // TODO extract check for empty phases
               });
  return toKeep;
}

Merge::Gap ChooseFastestReachableMergeGap(const std::vector<Merge::Gap>& mergeGaps)
{
  Merge::Gap bestGap{nullptr, nullptr};
  units::time::second_t rating = ScmDefinitions::INF_TIME;
  for (const auto& gap : mergeGaps)
  {
    auto timeNeeded = std::accumulate(gap.selectedMergePhases.cbegin(), gap.selectedMergePhases.cend(), 0.0_s, [](units::time::second_t i, const Merge::Phase& phase)
                                      { return i + phase.time; });
    if (timeNeeded < rating)
    {
      rating = timeNeeded;
      bestGap = gap;
    }
  }
  return bestGap;
}

Merge::Gap ChooseClosestMergeGap(const std::vector<Merge::Gap>& mergeGaps)
{
  Merge::Gap bestGap{nullptr, nullptr};
  auto rating = ScmDefinitions::INF_DISTANCE;
  for (const auto& gap : mergeGaps)
  {
    if (units::math::abs(gap.distance) < rating)
    {
      rating = units::math::abs(gap.distance);
      bestGap = gap;
    }
  }
  return bestGap;
}

void Merging::CalculateGapVelocitiesAndDistances(Merge::Gap& gap) const
{
  DetermineGapVelocity(gap);
  CalculateGapClearances(gap);
  CalculateGapDistance(gap);
}

void Merging::DetermineGapVelocity(Merge::Gap& gap) const
{
  if (gap.leader == nullptr)
  {
    gap.velocity = gap.follower->GetLongitudinalVelocity().GetValue();
    return;
  }
  if (gap.follower == nullptr)
  {
    gap.velocity = gap.leader->GetLongitudinalVelocity().GetValue();
    return;
  }

  // If the follower is faster than the leader, the gap is closing. Ego and the follower will have to adjust to the
  // leader's velocity.
  // If the leader is faster than the follower and we are approching from behind, we just need to slot in ahead of the
  // follower.
  // If the leader is faster than the follower and we are slowing down from further ahead, we can also aim for the
  // position ahead of the follower. TODO We may want to target the position (and velocity) behind the leader, but we
  // don't have the gap's position available here. Also check with the original concept.
  const auto vLeader = gap.leader->GetLongitudinalVelocity().GetValue();
  const auto vFollower = gap.follower->GetLongitudinalVelocity().GetValue();
  gap.velocity = units::math::min(vLeader, vFollower);
}

void Merging::CalculateGapClearances(Merge::Gap& gap) const
{
  // Get the safety clearances for leader and follower. If one of them doesn't exist ("open gap"), leave the safety distance at 0.
  if (gap.leader != nullptr)
  {
    gap.safetyClearanceToLeader = CalculateSafetyClearanceForMerging(
        *gap.leader, MinThwPerspective::EGO_ANTICIPATED_REAR, gap.velocity);
  }
  if (gap.follower != nullptr)
  {
    gap.safetyClearanceToFollower = CalculateSafetyClearanceForMerging(
        *gap.follower, MinThwPerspective::EGO_ANTICIPATED_FRONT, gap.velocity);
  }

  // Based on the current urgency, reduce the needed clearances, but only by a maximum of 90% for now. The urgencyFactor
  // seems too low for merging situations on an ending lane (e.g. highway entry), so it'll be scaled with an arbitrary
  // number.
  static double constexpr MAXIMUM_REDUCTION_FACTOR{0.9};
  static double constexpr URGENCY_SCALING_FACTOR{3.0};
  const double reductionFactorForUrgency{_mentalModel.GetEgoInfo().urgencyFactorForLaneChange * URGENCY_SCALING_FACTOR};
  gap.safetyClearanceToLeader *= 1 - std::min(MAXIMUM_REDUCTION_FACTOR, reductionFactorForUrgency);
  gap.safetyClearanceToFollower *= 1 - std::min(MAXIMUM_REDUCTION_FACTOR, reductionFactorForUrgency);
}

void Merging::CalculateGapDistance(Merge::Gap& gap) const
{
  //! The longitudinal distance to the point at which ego is completely behind the leader.
  //! (front of ego at the rear of leader)
  const auto distanceToLeader{
      gap.leader == nullptr
          ? ScmDefinitions::INF_DISTANCE
          : -ObstructionLongitudinal(gap.leader->GetLongitudinalObstruction()).rear};
  //! The longitudinal distance to the point at which ego is completely ahead of the follower.
  //! (rear of ego at the front of follower)
  const auto distanceToFollower{
      gap.follower == nullptr
          ? ScmDefinitions::INF_DISTANCE
          : ObstructionLongitudinal(gap.follower->GetLongitudinalObstruction()).front};

  // Determine regulating vehicle simply based on absolute distance.
  if (units::math::abs(distanceToLeader) < units::math::abs(distanceToFollower))
  {
    gap.regulatingVehicle = Merge::RegulatingVehicle::leader;
    // Calculate the distance to slot in safely behind the leader.
    gap.distance = distanceToLeader - gap.safetyClearanceToLeader;
  }
  else
  {
    gap.regulatingVehicle = Merge::RegulatingVehicle::follower;
    // Calculate the distance to slot in safely in front of the follower.

    if (!gap.leader)
    {
      gap.distance = units::math::abs(distanceToFollower) + gap.safetyClearanceToFollower;
    }
    else
    {
      gap.distance = distanceToFollower + gap.safetyClearanceToFollower;
    }
  }

  gap.position = gap.distance > 0_m ? Merge::GapPosition::inFront : Merge::GapPosition::behind;
}

units::length::meter_t Merging::CalculateSafetyClearanceForMerging(const SurroundingVehicleInterface& vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t gapVelocity) const
{
  const auto ego{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()};
  const auto driverParams{_mentalModel.GetDriverParameters()};
  const auto reactionBaseTimeMean{driverParams.reactionBaseTimeMean};
  const auto pedalChangeTimeMean{driverParams.pedalChangeTimeMean};

  return _longitudinalCalculations->CalculateSecureNetDistance(
      vehicle,
      *ego,
      -driverParams.maximumLongitudinalDeceleration,
      0._mps,
      reactionBaseTimeMean + pedalChangeTimeMean,
      -driverParams.maximumLongitudinalDeceleration,
      driverParams.carQueuingDistance,
      perspective,
      gapVelocity);
}

void Merging::PlanDifferentialMerge(Merge::Gap& gap) const
{
  CalculateDifferentialMergingVelocity(gap);
  LimitDifferentialMergingVelocity(gap);
  CalculateDifferentialMergingPhases(gap);
}

void Merging::CalculateDifferentialMergingVelocity(Merge::Gap& gap) const
{
  const auto maximumVelocity{DetermineVelocityLimits(_mentalModel.GetEgoInfo().velocity).vMax};  // TODO maybe limit to desiredVelocity

  // if (gap.leader == nullptr)
  // {
  //   // If there is no leader, the follower's velocity will be targeted to slot in safely ahead of them. If ego's desired
  //   // velocity is greater than that, choose it instead to avoid unnecessary slowness. Since the road ahead is clear, we
  //   // will target it after the merge anyway.
  //   gap.differentialMergingVelocity = std::max(desiredVelocity, gap.velocity);
  //   return;
  // }

  if (gap.position == Merge::GapPosition::inFront && gap.velocity < maximumVelocity)
  {
    // Gap is ahead and slower than we want to drive - we can enter it faster than vGap as long as we can slow down in
    // the available space.
    const auto possibleVelocityDifference{units::math::sqrt(2. * _mergingDeceleration * gap.fullMergeSpace)};
    gap.differentialMergingVelocity = units::math::min(gap.velocity + possibleVelocityDifference, maximumVelocity);
    return;
  }

  const auto minimumVelocity{DetermineVelocityLimits(_mentalModel.GetEgoInfo().velocity).vMin};
  if (gap.position == Merge::GapPosition::behind && gap.velocity > minimumVelocity)
  {
    // Gap is behind - we can enter it slower than vGap as long as we can speed up in the available space. We should not
    // drive slower than the minimum velocity (usually jam velocity).
    const auto possibleVelocityDifference{units::math::sqrt(2. * _mergingAcceleration * gap.fullMergeSpace)};
    gap.differentialMergingVelocity = units::math::max(gap.velocity - possibleVelocityDifference, minimumVelocity);
    return;
  }

  // If none of the special cases are met, aim exactly for vGap.
  gap.differentialMergingVelocity = gap.velocity;
}

void Merging::LimitDifferentialMergingVelocity(Merge::Gap& gap) const
{
  const auto currentVelocity{_mentalModel.GetEgoInfo().velocity};
  const auto velocityDifference{currentVelocity - gap.velocity};

  // We may need to move backwards relative to the gap. The calculation is more straightforward when using negative
  // distance and acceleration values to represent that.
  // Depending on the targeted merging velocity, ego may try to accelerate or decelerate to reach the gap.
  const auto acceleration{currentVelocity < gap.differentialMergingVelocity ? _mergingAcceleration : -_mergingDeceleration};

  // In order to find the time to reach the gap under continuous acceleration/deceleration, a quadratic equation needs
  // to be solved. Therefore, two solutions exists.
  std::vector<units::time::second_t> possibleSolutions{
      (-velocityDifference + units::math::sqrt(units::math::pow<2>(velocityDifference) + 2. * acceleration * gap.distance)) / acceleration,
      (-velocityDifference - units::math::sqrt(units::math::pow<2>(velocityDifference) + 2. * acceleration * gap.distance)) / acceleration};

  // There may be negative solutions to the equation. They are invalid for our use case and need to be filtered out.
  possibleSolutions.erase(
      std::remove_if(
          possibleSolutions.begin(), possibleSolutions.end(), [](units::time::second_t possibleSolution)
          { return possibleSolution < 0_s; }),
      possibleSolutions.end());

  // If there are only negative solutions, the gap is not reachable at all and we can simply quit as the gap will be
  // sorted out as invalid elsewhere.
  if (possibleSolutions.empty()) return;

  // Choose the smallest non-negative number as the solution.
  //! The time needed to cover the distance to the gap while accelerating/decelerating continuously.
  const auto accelerationTimeToReachGap{*std::min_element(possibleSolutions.begin(), possibleSolutions.end())};

  const auto achievableVelocityDifference{accelerationTimeToReachGap * acceleration};

  if (units::math::abs(currentVelocity - gap.differentialMergingVelocity) > units::math::abs(achievableVelocityDifference))
  {
    // If the difference to the "ideal" merging velocity is so large that it cannot be achieved in the available space
    // even with permanent acceleration/deceleration, target the best achievable velocity instead.
    gap.differentialMergingVelocity = currentVelocity + achievableVelocityDifference;
  }
}

void Merging::CalculateDifferentialMergingPhases(Merge::Gap& gap) const
{
  const auto currentVelocity{_mentalModel.GetEgoInfo().velocity};
  // Depending on the targeted merging velocity, ego may try to accelerate or decelerate to reach the gap.
  const auto acceleration{currentVelocity < gap.differentialMergingVelocity ? _mergingAcceleration : -_mergingDeceleration};

  //! The time needed to accelerate/decelerate to merging velocity.
  const auto accelerationTime{(gap.differentialMergingVelocity - currentVelocity) / acceleration};
  //! The distance covered relative to the gap while accelerating/decelerating to merge velocity.
  const auto relativeAccelerationDistance{
      0.5 * acceleration * units::math::pow<2>(accelerationTime) + (currentVelocity - gap.velocity) * accelerationTime};
  //! The absolute distance remaining to reach the gap after accelerating/decelerating to merging velocity.
  const auto remainingDistance{units::math::abs(gap.distance) - units::math::abs(relativeAccelerationDistance)};

  if (remainingDistance < units::make_unit<units::length::meter_t>(-ScmDefinitions::EPSILON))
  {
    // If the remaining distance after accelerating/decelerating were significantly less than zero, this would indicate
    // a merging velocity too high/low to be reached in the available space (gap.distance). This case should be prevented
    // by LimitDifferentialMergingVelocity().
    return;  // TODO temporarily return and leave an invalid empty list of phases
    // throw new std::logic_error("Cannot reach differential merging velocity in the available distance.");
  }

  // All necessary information for the acceleration part of the manoeuvre is known now. To get the distance travelled
  // along the road, the distance covered by the gap is added to the relative distance.
  gap.differentialMergePhases.push_back({
      .mergeAction = acceleration > 0_mps_sq ? MergeAction::ACCELERATING : MergeAction::DECELERATING,
      .time = accelerationTime,
      .distance = relativeAccelerationDistance + gap.velocity * accelerationTime,
  });

  if (remainingDistance <= units::make_unit<units::length::meter_t>(ScmDefinitions::EPSILON))
  {
    // No additional phase is necessary if the remaining distance is zero (approximately, since the many calculation
    // steps may introduce inaccuracies).
    return;
  }
  // If there is still distance to the gap left after accelerating, we will try to reach it with constant driving.
  // (The selected merging velocity should be the maximum/minimum velocity in this case anyway, so further acceleration
  // or deceleration would be disallowed or lead to a longer manoeuvre.)
  const auto finalVelocityDifference{units::math::abs(gap.differentialMergingVelocity - gap.velocity)};
  const auto timeToCoverRemainingDistance{remainingDistance / finalVelocityDifference};
  gap.differentialMergePhases.push_back({
      .mergeAction = MergeAction::CONSTANT_DRIVING,
      .time = timeToCoverRemainingDistance,
      .distance = gap.differentialMergingVelocity * timeToCoverRemainingDistance,
  });
}

//! @brief Determines relevant surrounding vehicles for the relative lane the merging maneuver should take place
//! @param relativeLane
//! @returns vector of surrounding vehicles for the relative lane sorted by relative distance
std::vector<const SurroundingVehicleInterface*> DetermineSurroundingVehicles(RelativeLane relativeLane, const std::vector<const SurroundingVehicleInterface*>& allSurroundingVehicles, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory)
{
  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  std::copy_if(allSurroundingVehicles.begin(), allSurroundingVehicles.end(), std::back_inserter(surroundingVehicles), [relativeLane](const auto* vehicle)
               { return vehicle->GetLaneOfPerception() == relativeLane; });

  if (surroundingVehicles.empty())
    return {};

  auto neighbouringLane = RightAndLeftRelativeLanes.at(relativeLane);

  std::copy_if(allSurroundingVehicles.begin(), allSurroundingVehicles.end(), std::back_inserter(surroundingVehicles), [&](const auto* vehicle)
               { auto vehicleQuery = surroundingVehicleQueryFactory.GetQuery(*vehicle);
                 return vehicle->GetLaneOfPerception() == neighbouringLane && vehicleQuery->IsSideSideVehicleChangingIntoSide(); });

  const auto SortByDistance = [](const SurroundingVehicleInterface* lhs, const SurroundingVehicleInterface* rhs)
  { return lhs->GetRelativeLongitudinalPosition().GetValue() < rhs->GetRelativeLongitudinalPosition().GetValue(); };

  std::sort(surroundingVehicles.begin(), surroundingVehicles.end(), SortByDistance);
  return surroundingVehicles;
}

//! @brief Determines all possible merge gaps - there is one between each surrounding vehicle, one in front and one behind
//! @param surroundingVehicles all surrounding vehicles which will be considered for merging
//! @returns vector of all possible merge gaps
std::vector<Merge::Gap> DetermineMergeGaps(const std::vector<const SurroundingVehicleInterface*>& surroundingVehicles)
{
  if (surroundingVehicles.empty())
  {
    return {};
  }

  std::vector<Merge::Gap> allGaps;

  Merge::Gap firstGap(surroundingVehicles.front(), nullptr);
  allGaps.push_back(firstGap);

  for (int i = 0; i < surroundingVehicles.size() - 1; ++i)
  {
    Merge::Gap gap(surroundingVehicles.at(i + 1), surroundingVehicles.at(i));
    allGaps.push_back(gap);
  }

  Merge::Gap lastGap(nullptr, surroundingVehicles.back());
  allGaps.push_back(lastGap);
  return allGaps;
}

void Merging::DetermineGapInformation(std::vector<Merge::Gap>& mergeGaps)
{
  for (auto& gap : mergeGaps)
  {
    CalculateGapVelocitiesAndDistances(gap);
    PlanDifferentialMerge(gap);
    DetermineReachabilities(gap);
    SelectMergingPhases(gap);
  }
}

std::vector<Merge::Gap> Merging::RemoveGapsCloseToEndOfLane(std::vector<Merge::Gap> allGaps) const
{
  allGaps.erase(std::remove_if(allGaps.begin(), allGaps.end(), [&](const Merge::Gap& gap)
                               {
                                const auto timeToReachGap {gap.distance / (gap.approachingVelocity - gap.velocity)};
                                const auto timeToReachEndOfLane {_mentalModel.GetDistanceToEndOfLane(0) / gap.approachingVelocity};
                                return timeToReachGap > timeToReachEndOfLane; }),
                allGaps.end());
  return allGaps;
}

void Merging::DetermineReachabilities(Merge::Gap& gap)
{
  if (gap.position == Merge::GapPosition::behind)
  {
    ReachabilityDecelerating(gap);
  }
  if (gap.position == Merge::GapPosition::inFront)
  {
    ReachabilityAccelerating(gap);
  }
  ReachabilityConstantDriving(gap);
}

units::velocity::meters_per_second_t Merging::DetermineVMax(units::velocity::meters_per_second_t vEgo) const
{
  const auto driverParams = _mentalModel.GetDriverParameters();
  const auto vLegal = _mentalModel.GetVelocityLegalEgo();  // is there still a difference in other lanes?
  const auto vWish = driverParams.desiredVelocity;
  const auto deltaVViolation = driverParams.amountOfSpeedLimitViolation;
  const auto deltaVWish = driverParams.amountOfDesiredVelocityViolation;
  const auto vReason = _mentalModel.DetermineVelocityReason();

  const auto vMax{std::min({vReason,
                            vWish + deltaVWish,
                            vLegal + deltaVViolation + deltaVWish})};

  return units::math::max(vMax, vEgo);
}

Merge::VelocityLimits Merging::DetermineVelocityLimits(units::velocity::meters_per_second_t vEgo) const
{
  const auto isOnEntryLane = _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::EGO).laneType == scm::LaneType::Entry;
  const auto velocityThreshold = isOnEntryLane ? _mentalModel.GetMeanVelocityLaneLeft() * 0.8 : _mentalModel.GetTrafficJamVelocityThreshold();
  units::velocity::meters_per_second_t vMax = DetermineVMax(vEgo);
  units::velocity::meters_per_second_t vMin = velocityThreshold > vMax ? vMax : velocityThreshold;  // ensure vMin <= vMax
  if (vEgo < vMin)
  {
    vMin = vEgo;
  }

  return {vMin, vMax};
}

void Merging::ReachabilityConstantDriving(Merge::Gap& mergeGap)
{
  // t: time, s: distance, v: velocity, d_ prefix means 'delta'
  const auto vEgo = _mentalModel.GetEgoInfo().velocity;
  const auto d_s0 = mergeGap.distance;
  const auto d_v0 = mergeGap.velocity - vEgo;
  const auto a_ego = mergeGap.position == Merge::GapPosition::inFront ? _mergingAcceleration : -_mergingDeceleration;
  const auto d_t_final = d_v0 / a_ego;
  const auto d_s_final = -d_v0 * d_v0 / (2.0 * a_ego);
  const auto d_s1 = d_s0 - d_s_final;
  const auto d_t1 = d_s1 / d_v0;

  mergeGap.AddConstantDrivingPhase({MergeAction::CONSTANT_DRIVING, units::time::second_t(d_t1), units::length::meter_t(d_s1)});

  MergeAction mergeAction = mergeGap.position == Merge::GapPosition::inFront ? MergeAction::DECELERATING : MergeAction::ACCELERATING;
  mergeGap.AddConstantDrivingPhase({mergeAction, d_t_final, 0.0_m});
}

void Merging::ReachabilityAccelerating(Merge::Gap& mergeGap)
{
  const auto a_com = _mergingAcceleration;
  const auto d_com = _mergingDeceleration;

  // t: time, s: distance, v: velocity, d_ prefix means 'delta'
  const auto vEgo = _mentalModel.GetEgoInfo().velocity;
  const auto vGap = mergeGap.velocity;
  const auto vMax = DetermineVelocityLimits(vEgo).vMax;

  const auto d_v0 = vGap - vEgo;
  const auto d_s0 = mergeGap.distance;
  const auto d_t_vEgo2vMax = (vMax - vEgo) / a_com;
  const auto d_t_vMax2vGap = (vMax - vGap) / d_com;
  const auto d_s_vEgo2vMax = (a_com / 2.0) * d_t_vEgo2vMax * d_t_vEgo2vMax - d_v0 * d_t_vEgo2vMax;
  const auto d_s_vMax2vGap = -(d_com / 2.0) * d_t_vMax2vGap * d_t_vMax2vGap - (vGap - vMax) * d_t_vMax2vGap;

  auto distanceAcceleratingPhase{0.0_m};
  auto distanceDeceleratingPhase{0.0_m};
  auto distanceConstantPhase{0.0_m};

  bool constantPhaseNecessary = d_s_vEgo2vMax + d_s_vMax2vGap < d_s0;
  if (constantPhaseNecessary)
  {
    const auto d_s_at_vMax = d_s0 - d_s_vEgo2vMax - d_s_vMax2vGap;
    const auto d_t_at_vMax = -d_s_at_vMax / (vGap - vMax);

    mergeGap.AddPhase({MergeAction::ACCELERATING, d_t_vEgo2vMax, d_s_vEgo2vMax});
    mergeGap.AddPhase({MergeAction::CONSTANT_DRIVING, d_t_at_vMax, d_s_at_vMax});
    mergeGap.AddPhase({MergeAction::DECELERATING, d_t_vMax2vGap, d_s_vMax2vGap});
    mergeGap.approachingVelocity = vMax;

    distanceAcceleratingPhase = a_com / 2 * units::math::pow<2>(d_t_vEgo2vMax) + vEgo * d_t_vEgo2vMax;
    distanceConstantPhase = vMax * d_t_at_vMax;
    distanceDeceleratingPhase = -d_com / 2 * units::math::pow<2>(d_t_vMax2vGap) + vMax * d_t_vMax2vGap;
  }
  else
  {
    const auto d_s1 = (d_s0 + (d_v0 * d_v0 / (2.0 * a_com))) / (1 + (d_com / a_com));

    const auto d_t1 = (units::math::sqrt(2.0 * d_s1 * d_com) + d_v0) / a_com;
    const auto d_v1 = -units::math::sqrt(2.0 * d_s1 * d_com);
    const auto d_t2 = -d_v1 / d_com;

    mergeGap.AddPhase({MergeAction::ACCELERATING, d_t1, d_s1});
    mergeGap.AddPhase({MergeAction::DECELERATING, d_t2, 0.0_m});
    mergeGap.approachingVelocity = vEgo - d_v1;

    distanceAcceleratingPhase = a_com / 2 * units::math::pow<2>(d_t1) + vEgo * d_t1;
    distanceDeceleratingPhase = -d_com / 2 * units::math::pow<2>(d_t2) + (vEgo - d_v1) * d_t2;
  }

  auto distanceEOL = _mentalModel.GetDistanceToEndOfLane(0, _featureExtractor.IsOnEntryLane());
  auto distanceNeeded = distanceAcceleratingPhase + distanceDeceleratingPhase + distanceConstantPhase;
  // Invalidate the merge gap when the distance needed is greater than the available space
  if (distanceNeeded > distanceEOL)
  {
    mergeGap.regularMergePhases.clear();
  }
}

void Merging::ReachabilityDecelerating(Merge::Gap& mergeGap)
{
  const auto a_com = _mergingAcceleration;
  const auto d_com = _mergingDeceleration;

  // t: time, s: distance, v:velocity, d_ prefix means 'delta'
  const auto vEgo = _mentalModel.GetEgoInfo().velocity;
  const auto vMin = DetermineVelocityLimits(vEgo).vMin;
  const auto d_s0 = mergeGap.distance;

  const auto vGap = mergeGap.velocity;
  const auto d_v0 = vGap - vEgo;

  const auto d_t_vEgo2vMin = (vEgo - vMin) / d_com;
  const auto d_s_vEgo2vMin = -d_com / 2.0 * (d_t_vEgo2vMin * d_t_vEgo2vMin) - d_v0 * d_t_vEgo2vMin;

  const auto d_t_vMin2vGap = (vGap - vMin) / a_com;
  const auto d_s_vMin2vGap = a_com / 2.0 * (d_t_vMin2vGap * d_t_vMin2vGap) - (vGap - vMin) * d_t_vMin2vGap;

  auto distanceAcceleratingPhase{0.0_m};
  auto distanceDeceleratingPhase{0.0_m};
  auto distanceConstantPhase{0.0_m};

  const bool constantPhaseNecessary = d_s_vEgo2vMin + d_s_vMin2vGap > d_s0;
  if (constantPhaseNecessary)
  {
    const auto d_s_at_vMin = d_s0 - d_s_vEgo2vMin - d_s_vMin2vGap;
    const auto d_t_at_vMin = -d_s_at_vMin / (vGap - vMin);

    mergeGap.AddPhase({MergeAction::DECELERATING, d_t_vEgo2vMin, d_s_vEgo2vMin});
    mergeGap.AddPhase({MergeAction::CONSTANT_DRIVING, d_t_at_vMin, d_s_at_vMin});
    mergeGap.AddPhase({MergeAction::ACCELERATING, d_t_vMin2vGap, d_s_vMin2vGap});
    mergeGap.approachingVelocity = vMin;

    distanceDeceleratingPhase = -d_com / 2 * units::math::pow<2>(d_t_vEgo2vMin) + vEgo * d_t_vEgo2vMin;
    distanceConstantPhase = vMin * d_t_at_vMin;
    distanceAcceleratingPhase = a_com / 2 * units::math::pow<2>(d_t_vMin2vGap) + vMin * d_t_vMin2vGap;
  }

  else
  {
    const auto d_s1 = (d_s0 - (d_v0 * d_v0 / (2 * d_com))) / (1 + (a_com / d_com));
    const auto d_t1 = (units::math::sqrt(-2 * d_s1 * a_com) - d_v0) / d_com;
    const auto d_v1 = units::math::sqrt(-2 * d_s1 * a_com);
    mergeGap.approachingVelocity = units::math::max(vEgo - d_v1, vMin);
    const auto d_t2 = d_v1 / a_com;

    mergeGap.AddPhase({MergeAction::DECELERATING, d_t1, d_s1});
    mergeGap.AddPhase({MergeAction::ACCELERATING, d_t2, 0.0_m});
    mergeGap.approachingVelocity = units::math::max(vEgo - d_v1, vMin);

    distanceDeceleratingPhase = -d_com / 2 * units::math::pow<2>(d_t1) + vEgo * d_t1;
    distanceAcceleratingPhase = a_com / 2 * units::math::pow<2>(d_t2) + (vEgo - d_v1) * d_t2;
  }

  auto distanceEOL = _mentalModel.GetDistanceToEndOfLane(0, _featureExtractor.IsOnEntryLane());
  auto distanceNeeded = distanceAcceleratingPhase + distanceDeceleratingPhase + distanceConstantPhase;
  // Invalidate the merge gap when the distance needed is greater than the available space
  if (distanceNeeded > distanceEOL)
  {
    mergeGap.regularMergePhases.clear();
  }
}

void Merging::SelectMergingPhases(Merge::Gap& gap)
{
  const auto sumUpTimes{[](std::vector<Merge::Phase> phases)
                        {
                          return std::accumulate(phases.begin(), phases.end(), 0._s, [](units::time::second_t acc, auto phase)
                                                 { return acc + phase.time; });
                        }};
  const bool differentialMergeFaster{sumUpTimes(gap.differentialMergePhases) < sumUpTimes(gap.regularMergePhases)};

  // For now, use a simple decision logic: Gaps that don't have a leader and a follower and a faster differential merge
  // time use a differential merge, in all other cases the regular merging manoeuvre is used.
  if ((gap.leader == nullptr || gap.follower == nullptr) && differentialMergeFaster)
  {
    gap.selectedMergePhases = gap.differentialMergePhases;
    gap.approachingVelocity = gap.differentialMergingVelocity;
    return;
  }
  gap.selectedMergePhases = gap.regularMergePhases;
}

std::vector<Merge::Gap> RemoveInvalidGaps(std::vector<Merge::Gap> allGaps)
{
  allGaps.erase(std::remove_if(begin(allGaps), end(allGaps), [](const Merge::Gap& gap)
                               { const auto phases {gap.selectedMergePhases};
    return std::any_of(begin(phases), end(phases), [](const Merge::Phase& phase)
                       { return phase.time <= 0.0_s || !std::isfinite(phase.time.value()); }); }),
                end(allGaps));
  return allGaps;
}

Merge::Gap Merging::RetrieveMergeGap(RelativeLane relativeLane, const std::vector<const SurroundingVehicleInterface*>& surroundingVehicles)
{
  std::vector<const SurroundingVehicleInterface*> surroundingVehiclesOnLane = DetermineSurroundingVehicles(relativeLane, surroundingVehicles, _surroundingVehicleQueryFactory);

  std::vector<Merge::Gap> allGaps = DetermineMergeGaps(surroundingVehiclesOnLane);

  DetermineGapInformation(allGaps);

  auto reachableGaps = RemoveInvalidGaps(allGaps);
  reachableGaps = RemoveTooNarrowGaps(reachableGaps, _mentalModel.GetEgoInfo().length);
  if (!_featureExtractor.HasDrivableSuccessor(true, RelativeLane::EGO) && _featureExtractor.IsOnEntryLane())
  {
    reachableGaps = RemoveGapsCloseToEndOfLane(reachableGaps);
  }

  auto bestGap = ChooseFastestReachableMergeGap(reachableGaps);
  bestGap.timestep = _mentalModel.GetTime();
  bestGap.relativeLane = relativeLane;

  return bestGap;
}
