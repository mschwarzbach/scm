/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <SurroundingVehicles/SurroundingVehicleInterface.h>

#include <vector>

#include "ZipMergeParameters.h"
class ZipMergingInterface
{
public:
  virtual ~ZipMergingInterface() = default;

  //! @brief Activates and deactives Mesoscopic ZipMerging-Situations, based on lane velocities and distances to end of lane.
  virtual void UpdateMesoscopicSituation() = 0;

  //! @brief Returns the distance from the end of the merging lane, a zip merge maneuver shall be executed.
  //! Estimated based on estimated lane change length and a safety buffer.
  //!
  //! @param params Zip merge parameters for the current maneuver.
  //! @return The estimated merge point in meters.
  virtual units::length::meter_t EstimateMergePoint(const ZipMergeParameters& params) const = 0;

  //! @brief Checks if the longitudinal action state for ZipMerging shall be activated.
  //! Calculated based on the MergePoint and an additional, velocity dependent safety buffer.
  //!
  //! @param params Zip merge parameters for the current maneuver.
  //! @return True if longitudinal control for zip merging shall be activated.
  virtual bool ActivationDistanceReached(const ZipMergeParameters& params) const = 0;

  //! @brief Determine parameters for the current ZipMerge maneuver.
  //! @return Calculated parameters.
  virtual ZipMergeParameters DetermineZipMergeParameters() const = 0;

  //! @brief Getter for currently set ZipMergeParameters.
  virtual ZipMergeParameters GetZipMergeParameters() const = 0;

  //! @brief Setter for ZipMergeParameters.
  virtual void SetZipMergeParameters(const ZipMergeParameters& ZipMergeParameters) = 0;

  //! @brief Resets MesoscopicSituations and ZipMergeParameters.
  virtual void Reset() = 0;

  //! @brief Returns the desired acceleration when in ZipMerging.
  //! @param maxAcceleration Maximum acceleration that can be applied.
  //! @param maxDeceleration Maximum deceleration that can be applied.
  //! @return The desired acceleration for the zip merging maneuver.
  virtual units::acceleration::meters_per_second_squared_t CalculateFollowingAcceleration(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const = 0;
};