/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <vector>

#include "ScmDefinitions.h"

//! @brief Parameter struct for zip merging execution
struct ZipMergeParameters
{
  //! @brief Target lane for active merger, closing lane for passive merger.
  RelativeLane relativeLane{RelativeLane::EGO};

  //! @brief Ids of agents in front of ego that shall merge from starting to target lane.
  std::vector<int> mergingAgentIds{};

  //! @brief Factor by which a active merger shall increase its following distance when approaching the merge point.
  double distanceFactor{};

  //! @brief Active merger flag.
  bool active{};
};