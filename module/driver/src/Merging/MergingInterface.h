/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "MentalModelDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"

class MergingInterface
{
public:
  virtual ~MergingInterface() = default;

  //! @brief Increases allowed acceleration and deceleration for merging maneuver compared to the comfort values.
  //! @param stochastics enables slightly different values for each agent
  virtual void DrawNewAccelerationLimits(StochasticsInterface* stochastics) = 0;

  //! @brief The partially randomly drawn deceleration limit for the merging maneuver.
  //! @return comfort deceleration for merging maneuver
  virtual units::acceleration::meters_per_second_squared_t GetDecelerationLimitForMerging() const = 0;

  //! @brief The partially randomly drawn acceleration limit for the merging maneuver.
  //! @return comfort acceleration for merging maneuver
  virtual units::acceleration::meters_per_second_squared_t GetAccelerationLimitForMerging() const = 0;

  //! @brief Determines all possibles merge gaps between surrounding vehicles given a relative lane and returns the most suitable.
  //! @param relativeLane relevant relative lane for merging
  //! @param surroundingVehicles all surrounding vehicles to consider
  //! @return best possible Merge::Gap for the current situation
  virtual Merge::Gap RetrieveMergeGap(RelativeLane relativeLane, const std::vector<const SurroundingVehicleInterface*>& surroundingVehicles) = 0;
};