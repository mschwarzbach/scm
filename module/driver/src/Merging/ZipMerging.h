/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <vector>

#include "FeatureExtractorInterface.h"
#include "LaneChangeDimension.h"
#include "MentalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "ZipMergingInterface.h"

class ZipMerging : public ZipMergingInterface
{
public:
  ZipMerging(MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, std::shared_ptr<const LaneChangeDimensionInterface> laneChangeDimension);

  //! @copydoc ZipMergingInterface::UpdateMesoscopicSituation
  void UpdateMesoscopicSituation() override;
  //! @copydoc ZipMergingInterface::EstimateMergePoint
  units::length::meter_t EstimateMergePoint(const ZipMergeParameters& params) const override;
  //! @copydoc ZipMergingInterface::DetermineZipMergeParameters
  ZipMergeParameters DetermineZipMergeParameters() const override;
  //! @copydoc ZipMergingInterface::GetZipMergeParameters
  ZipMergeParameters GetZipMergeParameters() const override;
  //! @copydoc ZipMergingInterface::SetZipMergeParameters
  void SetZipMergeParameters(const ZipMergeParameters& ZipMergeParameters) override;
  //! @copydoc ZipMergingInterface::Reset
  void Reset() override;
  //! @copydoc ZipMergingInterface::ActivationDistanceReached
  bool ActivationDistanceReached(const ZipMergeParameters& params) const override;
  //! @copydoc ZipMergingInterface::CalculateFollowingAcceleration
  units::acceleration::meters_per_second_squared_t CalculateFollowingAcceleration(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const override;

protected:
  //! @brief Calculates a factor by which the following distance should be increased when approaching the merge point.
  //! @param params Zip merge parameters for the current maneuver.
  //! @return Distance factor to be multiplied by the distance to the merge point.
  double DetermineFollowingDistanceFactor(const ZipMergeParameters& params) const;

  //! @brief Returns relevant relative lane for the currently planned maneuver.
  //! Merging target lane for active merger, ending lane to the side for passive merger.
  //!
  //! @param activeMerger True if the calculation should be done for an active merger.
  //! @return The relevant relative lane.
  RelativeLane DetermineRelativeLane(bool activeMerger) const;

  //! @brief Calculates the desired following offset for a passive zip merging vehicle.
  //! @param egoFrontVehicle EgoFront vehicle of the passive merger.
  //! @return The positive offset to the normal following distance, to create space for an active merger.
  units::length::meter_t DetermineFollowingOffsetPassive(const SurroundingVehicleInterface* egoFrontVehicle) const;

  //! @brief Calculate the distance from the merge point at which the longitudinal action state shall be activated.
  //! @return Distance from the merge point in meter.
  units::length::meter_t CalculateActivationDistance() const;

  //! @brief Calculates the desired following acceleration for an active merger.
  //! Different to the normal offset, because following offset distances are applied.
  //!
  //! @param vehicle Vehicle that shall be followed.
  //! @param maxDeceleration Maximum deceleration that shall be applied during acceleration calculation.
  //! @return The desired acceleration or deceleration.
  units::acceleration::meters_per_second_squared_t GetFollowingAccelerationFromVehicleActive(const SurroundingVehicleInterface* vehicle, units::acceleration::meters_per_second_squared_t maxDeceleration) const;

  //! @brief Determines the leading vehicle, that an active merger should follow.
  const SurroundingVehicleInterface* GetLeadingVehicleActive() const;

private:
  MentalModelInterface& _mentalModel;
  const FeatureExtractorInterface& _featureExtractor;
  const MentalCalculationsInterface& _mentalCalculations;
  std::shared_ptr<const LaneChangeDimensionInterface> _laneChangeDimension;
  ZipMergeParameters _zipMergeParameters;

  //! @brief Calculate the desired following acceleration in case of an active merger.
  //! @param maxAcceleration Maximum acceleration that can be applied.
  //! @param maxDeceleration Maximum decceleration that can be applied.
  //! @return Desired acceleration.
  units::acceleration::meters_per_second_squared_t CalculateFollowingAccelerationActive(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const;

  //! @brief Calculate the desired following acceleration in case of a passive merger.
  //! @param maxAcceleration Maximum acceleration that can be applied.
  //! @param maxDeceleration Maximum decceleration that can be applied.
  //! @return Desired acceleration.
  units::acceleration::meters_per_second_squared_t CalculateFollowingAccelerationPassive(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration) const;

  //! @brief Returns the detected lane changer vehicle for a given lane, if the Situation detected lane changer is active.
  //! @param lane Lane to check.
  //! @return A pointer to the causing vehicle of the DetectedLaneChanger situation if one is found or nullptr otherwise.
  const SurroundingVehicleInterface* GetDetectedLaneChanger(RelativeLane lane) const;

  //! @brief Checks if the vehicle in front of ego is in the list of merging vehicles.
  //! @param egoFrontVehicle Vehicle in front of ego.
  //! @return True if the ego front vehicle was a merger.
  bool EgoFrontVehicleWasMerger(const SurroundingVehicleInterface& egoFrontVehicle) const;

  //! @brief Calculates an offset to the normal following distance that shall be applied if the ego front vheicle was a merger.
  //! @param egoFrontVehicle Vehicle in front of ego.
  //! @return Following offset in meters.
  units::length::meter_t CalculateFollowingOffsetForMergerInFront(const SurroundingVehicleInterface& egoFrontVehicle) const;

  //! @brief Calculates an offset to the normal following distance that shall be applied if the ego front vheicle is a passive merging vehicle.
  //! @param egoFrontVehicle Vehicle in front of ego.
  //! @return Following offset in meters.
  units::length::meter_t CalculateFollowingOffsetForPassiveVehicleInFront(const SurroundingVehicleInterface& egoFrontVehicle) const;
};
