/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "include/common/OsiQlData.h"
#include "include/signal/driverInput.h"
#include "include/signal/sensorOutput.h"
#include "module/parameterParser/src/Signals/ParametersScmSignal.h"

//! @brief Interface for ScmDependencies class to enable testing of dependent classes.
struct ScmDependenciesInterface
{
  virtual ~ScmDependenciesInterface() = default;

  //! @brief update scm sensor data
  //! @param driverInput
  virtual void UpdateScmSensorData(const scm::signal::DriverInput& driverInput) = 0;

  //! @brief update parameters scm signal
  //! @param driverInput
  virtual void UpdateParametersScmSignal(const scm::signal::DriverInput& driverInput) = 0;

  //! @brief update parameters vehicle signal
  //! @param driverInput
  virtual void UpdateParametersVehicleSignal(const scm::signal::DriverInput& driverInput) = 0;

  //! @brief Get driver parameters
  //! @return _driverParameters
  virtual DriverParameters GetDriverParameters() = 0;

  //! @brief Get traffic rules scm
  //! @return _trafficRulesScm
  virtual TrafficRulesScm* GetTrafficRulesScm() = 0;

  //! @brief Get vehicle model Parameters
  //! @return _vehicleModelParameters
  virtual scm::common::vehicle::properties::EntityProperties GetVehicleModelParameters() = 0;

  //! @brief Get Stochastics
  //! @return _stochastics
  virtual StochasticsInterface* GetStochastics() = 0;

  //! @brief Get spawning velocity
  //! @return spawing velocity
  virtual units::velocity::meters_per_second_t GetSpawnVelocity() = 0;
  
  //! @brief Get spawning time
  //! @return spawning time
  virtual units::time::millisecond_t GetSpawnTime() = 0;
  
  //! @brief Set spawning time
  //! @param time
  virtual void SetSpawnTime(units::time::millisecond_t time) = 0;
  
  //! @brief Get cycle time
  //! @return cycle time
  virtual units::time::millisecond_t GetCycleTime() = 0;

  //! @brief Get own vehicle information scm
  //! @return _ownVehicleInformationScm
  virtual OwnVehicleInformationSCM* GetOwnVehicleInformationScm() = 0;

  //! @brief Get surroundingobjects scm
  //! @return _surroundingObjectsScm
  virtual SurroundingObjectsSCM* GetSurroundingObjectsScm() = 0;

  //! @brief Get traffic rule information scm
  //! @return _trafficRuleInformationScm
  virtual TrafficRuleInformationSCM* GetTrafficRuleInformationScm() = 0;

  //! @brief Get geometry information scm
  //! @return _geometryInformationScm
  virtual GeometryInformationSCM* GetGeometryInformationScm() = 0;

  //! @brief Get ownVehicleRoutePose
  virtual OwnVehicleRoutePose* GetOwnVehicleRoutePose() = 0;
};