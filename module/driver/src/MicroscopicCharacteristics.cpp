/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "MicroscopicCharacteristics.h"

#include <utility>

#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"

MicroscopicCharacteristics::MicroscopicCharacteristics()
{
}

void MicroscopicCharacteristics::Initialize(units::velocity::meters_per_second_t initVEgo)
{
  if (!isInit)
  {
    ownVehicleInformation->absoluteVelocity = initVEgo;
    ownVehicleInformation->longitudinalPosition = -ScmDefinitions::INF_DISTANCE;  // Do not use!

    isInit = true;
  }
}

OwnVehicleInformationScmExtended* MicroscopicCharacteristics::UpdateOwnVehicleData()
{
  return ownVehicleInformation.get();
}

ObjectInformationScmExtended* MicroscopicCharacteristics::UpdateObjectInformation(AreaOfInterest aoi, int sideIndex)
{
  return GetObjectFromAoi(aoi, sideIndex);
}

std::vector<ObjectInformationScmExtended>* MicroscopicCharacteristics::UpdateSideObjectsInformation(AreaOfInterest aoi)
{
  return GetObjectsFromSideAoi(aoi);
}

const OwnVehicleInformationScmExtended* MicroscopicCharacteristics::GetOwnVehicleInformation() const
{
  return ownVehicleInformation.get();
}

const SurroundingObjectsScmExtended* MicroscopicCharacteristics::GetSurroundingVehicleInformation() const
{
  return _surroundingObjectInformation.get();
}

const ObjectInformationScmExtended* MicroscopicCharacteristics::GetObjectInformation(AreaOfInterest aoi, int sideIndex) const
{
  return GetObjectFromAoi(aoi, sideIndex);
}

std::vector<ObjectInformationScmExtended>* MicroscopicCharacteristics::GetSideObjectVector(AreaOfInterest aoi) const
{
  if (aoi == AreaOfInterest::LEFT_SIDE)
  {
    return &_surroundingObjectInformation->Close().Left();
  }
  else if (aoi == AreaOfInterest::RIGHT_SIDE)
  {
    return &_surroundingObjectInformation->Close().Right();
  }
  else if (aoi == AreaOfInterest::RIGHTRIGHT_SIDE)
  {
    return &_surroundingObjectInformation->Close().FarRight();
  }
  else if (aoi == AreaOfInterest::LEFTLEFT_SIDE)
  {
    return &_surroundingObjectInformation->Close().FarLeft();
  }
  else
  {
    throw std::runtime_error("MicroscopicCharacteristics - GetSideObjectVector: Not a side AOI!");
  }
}

std::vector<ObjectInformationScmExtended>* MicroscopicCharacteristics::GetObjectVector(AreaOfInterest aoi) const
{
  auto* result{_surroundingObjectInformation->FromAreaOfInterest(aoi)};
  if (!result)
  {
    throw std::runtime_error("MicroscopicCharacteristics - GetObjectVector: Not a AOI!");
  }
  return result;
}

const std::vector<ObjectInformationScmExtended>* MicroscopicCharacteristics::GetSideObjectsInformation(AreaOfInterest aoi) const
{
  return GetObjectsFromSideAoi(aoi);
}

void MicroscopicCharacteristics::UpdateSideAoiAoiRegulateIndex(int counter)
{
  aoiRegulateIndex = counter;
}

int MicroscopicCharacteristics::GetSideAoiAoiRegulateIndex() const
{
  return aoiRegulateIndex;
}

void MicroscopicCharacteristics::UpdateSideAoiMergeRegulateIndex(int index)
{
  mergeRegulateIndex = index;
}

int MicroscopicCharacteristics::GetSideAoiMergeRegulateIndex() const
{
  return mergeRegulateIndex;
}

ObjectInformationScmExtended* MicroscopicCharacteristics::GetObjectFromAoi(AreaOfInterest aoi, int sideIndex) const
{
  switch (aoi)
  {
    case AreaOfInterest::HUD:
      return &_surroundingObjectInformation->objectHUD;
    case AreaOfInterest::INFOTAINMENT:
      return &_surroundingObjectInformation->objectInfotainment;
    case AreaOfInterest::INSTRUMENT_CLUSTER:
      return &_surroundingObjectInformation->objectInstrumentCluster;
    case AreaOfInterest::DISTRACTION:
      return &_surroundingObjectInformation->objectDistraction;
    default:
      const size_t i{_surroundingObjectInformation->GetIndexFrom(aoi)};
      if (i == _surroundingObjectInformation->size())
      {
        throw std::runtime_error("MicroscopicCharacteristics - GetObjectFromAoi: Invalid AOI!");
      }
      if (i == SurroundingObjectsScmExtended::Index::CLOSE)
      {
        auto& objects{*(*_surroundingObjectInformation)[i].at(aoi)};
        if (sideIndex < 0 || sideIndex >= static_cast<int>(objects.size()))
        {
          return nullptr;
        }
        return &objects[static_cast<size_t>(sideIndex)];
      }
      return (*_surroundingObjectInformation)[i].at(aoi)->data();
  }
}

std::vector<ObjectInformationScmExtended>* MicroscopicCharacteristics::GetObjectsFromSideAoi(AreaOfInterest aoi) const  // refactor? ist identisch mit GetSideObjectVector
{
  switch (aoi)
  {
    case AreaOfInterest::LEFT_SIDE:
      return &_surroundingObjectInformation->Close().Left();
    case AreaOfInterest::RIGHT_SIDE:
      return &_surroundingObjectInformation->Close().Right();
    case AreaOfInterest::LEFTLEFT_SIDE:
      return &_surroundingObjectInformation->Close().FarLeft();
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return &_surroundingObjectInformation->Close().FarRight();
    default:
      throw std::runtime_error("MicroscopicCharacteristics - GetObjectsFromSideAoi: Invalid AOI!");
  }
}
