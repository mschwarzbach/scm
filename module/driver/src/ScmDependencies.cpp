/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmDependencies.h"

#include <Stochastics/StochasticsInterface.h>

ScmDependencies::ScmDependencies(DriverParameters const& driverParameters,
                                 TrafficRulesScm const& trafficRulesScm,
                                 scm::common::vehicle::properties::EntityProperties const& vehicleModelParameters,
                                 const scm::signal::SensorOutput& sensorOutputSignal,
                                 StochasticsInterface* stochastics,
                                 units::velocity::meters_per_second_t spawnVelocity,
                                 units::time::millisecond_t cycleTime)
    : _driverParameters(driverParameters),
      _trafficRulesScm(trafficRulesScm),
      _vehicleModelParameters(vehicleModelParameters),
      _stochastics(stochastics),
      _spawnVelocity(spawnVelocity),
      _spawnTime{-999_ms},
      _cycleTime(cycleTime),
      _ownVehicleInformationScm(sensorOutputSignal.ownVehicleInformationSCM),
      _surroundingObjectsScm(sensorOutputSignal.surroundingObjectsSCM),
      _trafficRuleInformationScm(sensorOutputSignal.trafficRuleInformationSCM),
      _geometryInformationScm(sensorOutputSignal.geometryInformationSCM),
      _ownVehicleRoutePose(sensorOutputSignal.ownVehicleRoutePose)
{
  SetDefaultOrientationsRelativeToDriver();
}

void ScmDependencies::UpdateScmSensorData(const scm::signal::DriverInput& driverInput)
{
  _ownVehicleInformationScm = driverInput.sensorOutput.ownVehicleInformationSCM;
  SetDefaultOrientationsRelativeToDriver();
  _trafficRuleInformationScm = driverInput.sensorOutput.trafficRuleInformationSCM;
  _geometryInformationScm = driverInput.sensorOutput.geometryInformationSCM;
  _surroundingObjectsScm = driverInput.sensorOutput.surroundingObjectsSCM;
  _ownVehicleRoutePose = driverInput.sensorOutput.ownVehicleRoutePose;
}

void ScmDependencies::UpdateParametersScmSignal(const scm::signal::DriverInput& driverInput)
{
  _driverParameters = driverInput.driverParameters;
  _trafficRulesScm = driverInput.trafficRulesScm;
}

void ScmDependencies::UpdateParametersVehicleSignal(const scm::signal::DriverInput& driverInput)
{
  _vehicleModelParameters = driverInput.vehicleParameters;
}

DriverParameters ScmDependencies::GetDriverParameters()
{
  return _driverParameters;
}

TrafficRulesScm* ScmDependencies::GetTrafficRulesScm()
{
  return &_trafficRulesScm;
}

scm::common::vehicle::properties::EntityProperties ScmDependencies::GetVehicleModelParameters()
{
  return _vehicleModelParameters;
}

OwnVehicleInformationSCM* ScmDependencies::GetOwnVehicleInformationScm()
{
  return &_ownVehicleInformationScm;
}

SurroundingObjectsSCM* ScmDependencies::GetSurroundingObjectsScm()
{
  return &_surroundingObjectsScm;
}

TrafficRuleInformationSCM* ScmDependencies::GetTrafficRuleInformationScm()
{
  return &_trafficRuleInformationScm;
}

GeometryInformationSCM* ScmDependencies::GetGeometryInformationScm()
{
  return &_geometryInformationScm;
}

StochasticsInterface* ScmDependencies::GetStochastics()
{
  return _stochastics;
}

units::velocity::meters_per_second_t ScmDependencies::GetSpawnVelocity()
{
  return _spawnVelocity;
}

units::time::millisecond_t ScmDependencies::GetSpawnTime()
{
  return _spawnTime;
}

void ScmDependencies::SetSpawnTime(units::time::millisecond_t time)
{
  _spawnTime = time;
}

units::time::millisecond_t ScmDependencies::GetCycleTime()
{
  return _cycleTime;
}

void ScmDependencies::SetDefaultOrientationsRelativeToDriver()
{
  _ownVehicleInformationScm.InteriorMirrorOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(-20. / 180. * M_PI);
  _ownVehicleInformationScm.LeftMirrorOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(20. / 180. * M_PI);
  _ownVehicleInformationScm.RightMirrorOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(-45. / 180. * M_PI);
  _ownVehicleInformationScm.HeadUpDisplayOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(0. / 180. * M_PI);
  _ownVehicleInformationScm.InstrumentClusterOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(0. / 180. * M_PI);
  _ownVehicleInformationScm.InfotainmentOrientationRelativeToDriver = units::make_unit<units::angle::radian_t>(-20. / 180. * M_PI);
}

OwnVehicleRoutePose* ScmDependencies::GetOwnVehicleRoutePose()
{
  return &_ownVehicleRoutePose;
}
