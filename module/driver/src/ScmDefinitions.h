/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ScmDefinitions.h

//-----------------------------------------------------------------------------
//! @brief This file contains several classes for component purposes
//-----------------------------------------------------------------------------

#pragma once

#include "../../parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "LateralAction.h"
#include "ScmCommons.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief SCM internal declaration for an ADAS HMI signal
struct AdasHmiSignal
{
  //! @brief Flag indicating if the signal is active
  bool activity = false;
  std::string spotOfPresentation;
  //! @brief Representation of the system sending this signal
  std::string sendingSystem;
};

namespace Scm
{
//! @brief Point representation in lane coordinates s and t
struct Point
{
  //! @brief s coordinate in longitudinal direction of the lane (forward/backward)
  units::length::meter_t s = 0_m;
  //! @brief t coordinate in lateral direction of the lane (sideways)
  units::length::meter_t t = 0_m;

  //! @brief Custom equality operator if two points are the same
  //! @param other
  //! @return true if both coordinate are equal, false otherwise
  bool operator==(const Point& other) const
  {
    return scm::common::DoubleEquality(s.value(), other.s.value()) &&
           scm::common::DoubleEquality(t.value(), other.t.value());
  }
};

//! @brief A bounding box consists of the four corner points (s, t) of the agent/vehicle.
struct BoundingBox
{
  //! @brief leftFront point of the agent/vehicle
  Point leftFront;
  //! @brief rightFront point of the agent/vehicle
  Point rightFront;
  //! @brief leftRear point of the agent/vehicle
  Point leftRear;
  //! @brief rightRear point of the agent/vehicle
  Point rightRear;

  //! @brief Puts all four corner points into an array (probably not in an intuitive order!)
  //! @details 0=leftFront, 1=rightFront, 2=leftRear, 3=rightRear
  //! @return An array consists of the four corner points
  std::array<Point, 4> ToArray() const
  {
    return {leftFront, rightFront, leftRear, rightRear};
  }

  //! @brief Custom equality operator for this struct
  //! @param other
  //! @return True if all four points are equal, false otherwise
  bool operator==(const BoundingBox& other) const
  {
    return leftFront == other.leftFront &&
           rightFront == other.rightFront &&
           leftRear == other.leftRear &&
           rightRear == other.rightRear;
  }
};
}  // namespace Scm

struct InformationRequest
{
  //! Timestamp at request time [ms]
  units::time::millisecond_t Timestamp{0.};
  //! The priority of the requester from the viewpoint of mental model data [0-1]
  double RequesterPriority{0.};
  //! The priority of the request from the viewpoint of mental model data [0-1]
  double Priority{0.};
  //! Minimum requirement for the next view assigment by request [FOVEA/UFOV]
  FieldOfViewAssignment Request{FieldOfViewAssignment::UFOV};
};

//! @brief Consolidated flags if and under which traffic circumstances an agent will consider to use a shoulder lane
struct UseShoulderLane
{
  //! @brief Flag indicating if the agent will consider the shoulder lane when in a traffic jam
  bool inTrafficJam{false};
  //! @brief Flag indicating if the agent will consider the shoulder lane when in a traffic jam and other agents already use it
  bool inTrafficJamWhenPeerPressured{false};
  //! @brief Flag indicating if the agent will consider the shoulder lane when in a traffic jam and close to an exit the agent wants to take
  bool inTrafficJamCloseToExit{false};
  //! @brief Flag indicating if the agent will consider the shoulder lane when in a traffic jam and close to an exit the agent wants to take and other agents already use it
  bool inTrafficJamCloseToExitWhenPeerPressured{false};
  //! @brief Flag indicating if once started to use a shoulder lane it should remain even if the traffic situation may change (e.g. no peer pressuring other agent anymore)
  bool stayOnceStarted{false};
};

struct OwnVehicleInformationScmExtended : public OwnVehicleInformationSCM
{
  //! Current absolute velocity (ground truth data) [m/s]
  units::velocity::meters_per_second_t absoluteVelocityReal{ScmDefinitions::DEFAULT_VALUE};
  //! Targeted absolute velocity [m/s]
  units::velocity::meters_per_second_t absoluteVelocityTargeted{units::velocity::meters_per_second_t(130_kph)};
  //! Tempory change to the favoured velocity of the driver [m/s]
  units::velocity::meters_per_second_t deltaVelocityWish{0.};
  //! Current area of interest to regulate on
  AreaOfInterest aoiRegulate{AreaOfInterest::EGO_FRONT};
  //! The current situation of the ego agent
  Situation situation{Situation::FREE_DRIVING};
  //! Flag for a change in situation.
  bool hasSituationChanged{false};
  //! Flag for a change in situation.
  bool hasMesoscopicSituationChanged{false};
  //! Flag for a change in aoi regulate while following.
  bool hasAoiRegulateChanged{false};
  //! The lateral action of ego
  LateralAction lateralAction{LateralAction(LateralAction::State::LANE_KEEPING)};
  //! The longitudinal action state of ego
  LongitudinalActionState longitudinalActionState{LongitudinalActionState::SPEED_ADJUSTMENT};
  //! Direction relative to current lane (0: straight, 1: left, -1: right)
  int direction{0};
  //! Fvoveal area of view focused by the agent
  AreaOfInterest fovea{AreaOfInterest::EGO_FRONT};
  //! Useful field of view around focus
  std::vector<AreaOfInterest> ufov{AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::HUD};
  //! Areas of interest assigned to peripheral vision
  std::vector<AreaOfInterest> periphery{AreaOfInterest::EGO_REAR, AreaOfInterest::LEFT_REAR, AreaOfInterest::LEFT_SIDE, AreaOfInterest::RIGHT_REAR, AreaOfInterest::RIGHT_SIDE};
  //! Determine if current agent is an anticipating driver
  bool isAnticipating{false};

  //! Flag indicating that the vehicle is still too close to the spawn point to change lanes.
  bool preventLaneChangeDueToSpawn{false};

  //! Flag indicating that laneChanging is prevented since lateral manoevering is controlled by an external component.
  bool preventLaneChangeDueExternalLateralControl{false};

  //! Flag, whether to instruct the sensor driver to roll a new route.
  bool triggerNewRouteRequest{false};

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationFrontCluster{{Situation::FOLLOWING_DRIVING, AreaOfInterest::NumberOfAreaOfInterests},
                                                                            {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::NumberOfAreaOfInterests}};

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationSideCluster{{Situation::LANE_CHANGER_FROM_RIGHT, AreaOfInterest::NumberOfAreaOfInterests},
                                                                           {Situation::LANE_CHANGER_FROM_LEFT, AreaOfInterest::NumberOfAreaOfInterests},
                                                                           {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, AreaOfInterest::NumberOfAreaOfInterests},
                                                                           {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, AreaOfInterest::NumberOfAreaOfInterests},
                                                                           {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, AreaOfInterest::NumberOfAreaOfInterests},
                                                                           {Situation::SIDE_COLLISION_RISK_FROM_LEFT, AreaOfInterest::NumberOfAreaOfInterests}};

  //! LateralOffset of neutral Position (scaled due to velocity)
  units::length::meter_t lateralOffsetNeutralPositionScaled{0_m};

  //! Map holding information if a situation is anticipated
  std::map<Situation, bool> situationAnticipated{{Situation::FOLLOWING_DRIVING, false},
                                                 {Situation::OBSTACLE_ON_CURRENT_LANE, false},
                                                 {Situation::LANE_CHANGER_FROM_RIGHT, false},
                                                 {Situation::LANE_CHANGER_FROM_LEFT, false},
                                                 {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, false},
                                                 {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, false},
                                                 {Situation::SIDE_COLLISION_RISK_FROM_LEFT, false},
                                                 {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, false}};

  //! Mapping an actual risk to a situation
  std::map<Situation, Risk> situationRisk{{Situation::FOLLOWING_DRIVING, Risk::LOW},
                                          {Situation::OBSTACLE_ON_CURRENT_LANE, Risk::LOW},
                                          {Situation::LANE_CHANGER_FROM_RIGHT, Risk::LOW},
                                          {Situation::LANE_CHANGER_FROM_LEFT, Risk::LOW},
                                          {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, Risk::LOW},
                                          {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, Risk::LOW},
                                          {Situation::SIDE_COLLISION_RISK_FROM_LEFT, Risk::LOW},
                                          {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, Risk::LOW}};

  units::length::meter_t distanceForPlannedSwerving{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t lateralEvadeDistanceForPlannedSwerving{ScmDefinitions::DEFAULT_VALUE};
  bool isLaneChangeProhibitionIgnored{false};
  bool modifyLaneChange = false;
  bool isOuterKeeping = false;
  bool isEgoLaneKeeping = false;

  UseShoulderLane useShoulderLane{};

  bool isDistracted = false;
};

struct ObjectInformationScmExtended : public ObjectInformationSCM
{
  //! Change in acceleration [m/s²]
  units::acceleration::meters_per_second_squared_t accelerationChange{ScmDefinitions::DEFAULT_VALUE};

  units::acceleration::meters_per_second_squared_t accelerationLastStep{ScmDefinitions::DEFAULT_VALUE};

  //! Time gap  [s]
  units::time::second_t gap{99._s};
  //! Change in the time gap [1/s²]
  double gapDot{0.};
  //! Change in tau
  double tauDot{0.};
  //! Time to collision [s]
  units::time::second_t ttc{ScmDefinitions::TTC_LIMIT};
  //! Time stamp at which the agent has last updated its information for the qualities fove, ufov and periphery [ms]
  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap{{FieldOfViewAssignment::FOVEA, -9999_ms},
                                                                             {FieldOfViewAssignment::UFOV, -9999_ms},
                                                                             {FieldOfViewAssignment::PERIPHERY, -9999_ms}};
  //! The information of all optical requests
  std::vector<InformationRequest> opticalRequest{};

  units::time::second_t ttcthreshold{ScmDefinitions::DEFAULT_VALUE};
  void ResetToDefault()
  {
    *this = ObjectInformationScmExtended{};
  }
};

struct SurroundingObjectsScmExtended : scm::Grid<std::vector<ObjectInformationScmExtended>, 5>
{
  ObjectInformationScmExtended objectInfotainment;
  ObjectInformationScmExtended objectHUD;
  ObjectInformationScmExtended objectInstrumentCluster;
  ObjectInformationScmExtended objectDistraction;
};

struct LaneChangeIntensities
{
  double EgoLaneIntensity;
  double LeftLaneIntensity;
  double RightLaneIntensity;
};

inline bool operator==(LaneChangeIntensities lhs, const LaneChangeIntensities& rhs)
{
  return (lhs.EgoLaneIntensity == rhs.EgoLaneIntensity) &&
         (lhs.LeftLaneIntensity == rhs.LeftLaneIntensity) &&
         (lhs.RightLaneIntensity == rhs.RightLaneIntensity);
}

namespace DataQuality
{
static constexpr double HIGH = 400.0 / 7.0;
static constexpr double MEDIUM = 200.0 / 7.0;
static constexpr double LOW = 0;
}  // namespace DataQuality

enum class ParameterChangeRate
{
  FAST,
  MEDIUM,
  SLOW,
  CONSTANT
};

enum class VisualPerceptionSector
{
  FRONT = 0,
  LEFT,
  RIGHT,
  REAR
};

//! @brief Class representing parameter of perceived vehicles, including the data point itself and a related reliability (or "trustworthiness") of it.
//! @tparam T
template <typename T>
class VehicleParameter
{
public:
  //! @brief Ctor for a new vehicle parameter
  //! @param x
  //! @param r
  VehicleParameter(T x, double r = 0)
      : _data{x}, _reliability{r}
  {
  }
  //! @brief Default dtor
  ~VehicleParameter() = default;

  //! @brief Custom operator() for implicit conversion of the data point
  operator T() const noexcept
  {
    return _data;
  }

  T GetValue() const
  {
    return _data;
  }

  //! @brief Checks whether the data point is still considered reliable
  //! @param requiredQuality
  //! @return True if the reliability suffices the requiredQuality, false otherwise
  bool IsReliable(double requiredQuality) const noexcept
  {
    return _reliability >= requiredQuality;
  }

  //! @brief Gets the reliability related to the data point of this parameter
  //! @return The reliability
  double GetReliability() const noexcept
  {
    return _reliability;
  }

private:
  //! @brief data point for this parameter
  T _data;
  //! @brief Related reliability for this parameter
  double _reliability;
};