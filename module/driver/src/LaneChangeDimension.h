/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file LaneChangeDimension.h

#pragma once

#include <units.h>
class LaneChangeDimensionInterface
{
public:
  //! \brief Calculates the estimated length of a lane change maneuver [m].
  //! \return length
  virtual units::length::meter_t EstimateLaneChangeLength() const = 0;

  //! \brief Calculates the estimated length of a lane change maneuver [m].
  //! \return length
  //! \param laneChangeWidth      Width of lane change
  //! \param lateralAcc           Possible maximum lateral acceleration
  virtual units::length::meter_t EstimateLaneChangeLength(units::length::meter_t laneChangeWidth, units::acceleration::meters_per_second_squared_t lateralAcc, units::velocity::meters_per_second_t longitudinalVelocity) const = 0;
};

/**
 * \brief LaneChangeDimension
 *
 */
class MentalModelInterface;
class LaneChangeDimension : public LaneChangeDimensionInterface
{
public:
  LaneChangeDimension(const MentalModelInterface& mentalModel);

  units::length::meter_t EstimateLaneChangeLength() const override;
  units::length::meter_t EstimateLaneChangeLength(units::length::meter_t laneChangeWidth, units::acceleration::meters_per_second_squared_t lateralAcc, units::velocity::meters_per_second_t longitudinalVelocity) const override;

private:
  const MentalModelInterface& _mentalModel;
};
