/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  SituationManager.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "Merging/ZipMergingInterface.h"
#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h"
#include "SituationManagerInterface.h"

class MentalModelInterface;
class FeatureExtractorInterface;
class MentalCalculationsInterface;
class SwervingInterface;
/*! \addtogroup SituationManager
 * @{
 * \brief Keeps track of situations.
 *
 * It provides functionality to generate a probability distribution over situations given a real world
 * state and then to sample from this distribution.
 *
 *
 * \section SM_Inputs Inputs
 * name                      | meaning
 * ----------                | ----------
 * actionState               | Actual action state, in which the driver is in.
 * cycleTime                 | Cycle time of this components trigger task [ms].
 * durationCurrentSituation  | Duration of the current situation [ms].
 * mentalModel               | The mental processes of the stochastic cognitive model.
 * observationDebug          | This file contains the interface of the observationDebug module to interact with the framework.
 * spawn                     | Query, whether the agent spawned this cycle.
 * stochastics               | Provides access to the stochastics functionality of the framework.
 * time                      | Current time step of the simulation [ms].
 *
 * \section SM_Outputs Outputs
 * * name                    | meaning
 * ----------                | ----------
 * _currentSituation         | The situation that the current agent is in
 * _durationCurrentSituation | Duration of the current situation [ms]
 * _mentalModel              | The mental processes of the stochastic cognitive model
 * s
 */

//! \ingroup SituationManager
class SituationManager : public SituationManagerInterface
{
public:
  //! \brief Constructor.
  //! \param [in] cycleTime           Cycle time of this components trigger task [ms].
  //! \param [in] stochastics         Provides access to the stochastics functionality of the framework.
  //! \param [in] mentalModel         The mental processes of the stochastic cognitive model.
  //! \param [in] featureExtractor    Interface to FeatureExtractor to avoid accessing it through MentalModel
  //! \param [in] mentalCalculations  Interface to MentalCalculations to avoid accessing it through MentalModel
  //! \param [in] swerving            Interface to Swerving
  SituationManager(units::time::millisecond_t cycleTime, StochasticsInterface* stochastics, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving);
  SituationManager(const SituationManager&) = delete;
  SituationManager(SituationManager&&) = delete;
  SituationManager& operator=(const SituationManager&) = delete;
  SituationManager& operator=(SituationManager&&) = delete;
  virtual ~SituationManager() = default;

  IgnoringOuterLaneOvertakingProhibitionInterface* _outerLaneOvertakingProhibitionQuota;

  void SetIgnoringOuterLaneOvertakingProhibition(IgnoringOuterLaneOvertakingProhibitionInterface* ignoringOuterLaneOvertakingProhibition) override;
  virtual void GenerateIntensitiesAndSampleSituation(ZipMergingInterface& zipMerging) override;
  virtual units::time::millisecond_t GetCurrentSituationDuration() override;
  virtual void SetCurrentSituationDuration(units::time::millisecond_t durationCurrentSituation) override;
  void SetSituationLastTick(Situation situation) override;

protected:
  //! \brief Returns the intensity value of the case side collision risk from left.
  //! \param leftC_hasActualLaneCrossingConflict          Determine, if an aoi vehicle actual crosses into ego lane
  //! \param leftC_hasIntentionalLaneCrossingConflict     Determine, if an aoi vehicle intent to cross into ego lane
  //! \return GetIntensityFromQueryVector(queryVector)    Intensity of the case side collision risk from left
  double GetIntensity_LeftSideCollisionRisk(bool leftC_hasActualLaneCrossingConflict,
                                            bool leftC_hasIntentionalLaneCrossingConflict);

  //! \brief Returns the intensity value of the case side collision risk from right.
  //! \param rightC_hasActualLaneCrossingConflict          Determine, if an aoi vehicle actual crosses into ego lane
  //! \param rightC_hasIntentionalLaneCrossingConflict     Determine, if an aoi vehicle intent to cross into ego lane
  //! \return GetIntensityFromQueryVector(queryVector)    Intensity of the case side collision risk from left
  double GetIntensity_RightSideCollisionRisk(bool rightC_hasActualLaneCrossingConflict,
                                             bool rightC_hasIntentionalLaneCrossingConflict);

  //! \brief Determine the enablers for cluster vehicles (ego lane case)
  std::vector<AreaOfInterest> GetClusterVehicles(std::vector<AreaOfInterest> areas);

  //! \brief Determine the enablers for cluster vehicles (side lane case)
  std::map<AreaOfInterest, bool> GetClusterVehiclesSideLane(std::vector<AreaOfInterest> areas);

  //! \brief Calls DetermineIntensityFromAcceleration in longitudinal direction
  //! \param acceleration
  //! \param isFrontObject
  double DetermineIntensityFromAccelerationLongitudinal(units::acceleration::meters_per_second_squared_t acceleration, bool isFrontObject) const;

  //! \brief filter out near or far vehicle from vehicles
  void FilterVehicle(std::vector<AreaOfInterest>& vehicles, AreaOfInterest near, AreaOfInterest far);

  bool HaveIntensitiesChangedSignificantly(std::map<Situation, double> intensities, std::map<Situation, double> intensitiesLast) const;

  void SetLastIgnoreRiskState(Situation newSituation, std::map<Situation, double> intensities);

  void SetSideClusterIntensities(Situation situation, bool isAnticipated, double situationIntensity, double intensityFromAcceleration, std::map<Situation, double>& situationIntensitiesSideCluster);

  //! \brief Query the need to refresh information to know situation for sure.
  bool IsSituationAmbigious(double situationIntensity, bool reliableFovea, bool reliableUfov, bool reliablePeriphery);

  //! \brief Get the intensity for a high risk lane changer.
  //! \param [in] hasIntentionalLaneCrossingConflict      Determine, if an aoi vehicle intent to cross into ego lane
  //! \param [in] hasActualLaneCrossingConflict           Determine, if an aoi vehicle actual crosses into ego lane
  //! \param [in] isMinimumFollowingDistanceViolated      Determine, if the minimal following distance is violated
  //! \return GetIntensityFromQueryVector(queryVector)    Value that represents the overall intensitiy of a collection of different intensities
  double GetIntensity_HighRiskLaneChanger(bool hasIntentionalLaneCrossingConflict, bool hasActualLaneCrossingConflict, bool isMinimumFollowingDistanceViolated);

  //! \brief Get the intensity for an anticipated high risk lane changer.
  //! \param [in] fromLeft                                Determine, if the detected lane change is to the left of the agent
  //! \param [in] isMinimumFollowingDistanceViolated      Determine, if the minimal following distance is violated
  //! \param [in] isInfluencingDistanceViolated           Determine, if the influencing following distance is violated
  //! \param [in] hasIntentionalLaneCrossingConflict      Determine, if an aoi vehicle intent to cross into ego lane
  //! \param [in] isLaneDriveablePerceived                Check, if the end of the aoi lane is visible
  //! \param [in] intensityForNeedToChangeLane            Calculate intensity for agent in left or right front aoi to change his lane due to end of lane
  //! \param [in] intensityForSlowerLeadingVehicle        Determine, if in front of an aoi vehicle is a slower vehicle and consequently the driver would cross into another lane
  //! \return GetIntensity_AnticipatedLaneChanger         Numeric representation of the lane change intensity
  double GetIntensity_AnticipatedHighRiskLaneChanger(bool fromLeft, bool isMinimumFollowingDistanceViolated, bool isInfluencingDistanceViolated, bool hasAnticipatedLaneCrossingConflict, bool hasIntentionalLaneCrossingConflict, bool isLaneDriveablePerceived, bool isLaneChangePermittedDueToLaneMarkingsForAoi, double intensityForNeedToChangeLane, double intensityForSlowerLeadingVehicle);

  //! \brief Adds intensities for side cluster
  void AddRelevantSideClusterIntensities(double intensityFromAccelerationLaneChange, double probabilityLaneChanger, bool isAnticipatedLaneChanger, double intensityFromAccelerationSuspiciousObject, double probabilitySuspiciousObject, double intensityFromAccelerationSideCollision, double probabilitySideCollision, std::map<Situation, double>& situationIntensitiesSideCluster, Side side, AreaOfInterest aoi);

  //! Last known risk class violation (driver has ignored a higher situation risk)
  bool _lastIgnoreRiskState = false;

private:
  struct MesoscopicSituationFeatures
  {
    bool frontClusterJam = false;  // Determine, if the front cluster vehicle is slower than the traffic jam speed
    bool leftClusterJam = false;   // Determine, if the left side cluster is slower than the traffic jam speed
    bool rightClusterJam = false;  // Determine, if the right side cluster is slower than the traffic jam speed
  };

  //! \brief Get the intensity for an anticipated lane changer.
  //! \param [in] fromLeft                            Determine, if the detected lane change is to the left of the agent
  //! \param [in] isInfluencingDistanceViolated       Determine, if the influencing following distance is violated
  //! \param [in] hasIntentionalLaneCrossingConflict  Determine, if an aoi vehicle intent to cross into ego lane
  //! \param [in] isLaneDriveablePerceived            Check, if the end of the aoi lane is visible
  //! \param [in] intensityForNeedToChangeLane        Calculate intensity for agent in left or right front aoi to change his lane due to end of lane
  //! \param [in] intensityForSlowerLeadingVehicle    Determine, if in front of an aoi vehicle is a slower vehicle and consequently the driver would cross into another lane
  //! \return double-value                            Numeric representation of the lane change intensity
  double GetIntensity_AnticipatedLaneChanger(bool fromLeft, bool isNearEnoughForFollowing, bool hasAnticipatedLaneCrossingConflict, bool hasIntentionalLaneCrossingConflict, bool isLaneDriveablePerceived, bool isLaneChangePermittedDueToLaneMarkingsForAoi, double intensityForNeedToChangeLane, double intensityForSlowerLeadingVehicle);

  //! \brief Decide if there will be an information request and in cases of high risk update the suffiencyLevel to FOVEA.
  bool IsInformationRequestRequired(FieldOfViewAssignment* sufficiencyLevel, double situationIntensity, bool foveaReliable, bool ufovReliable, bool peripheryReliable);

  //! \brief Add most important situations of the front cluster to the main intensity vector.
  void GetIntensitiesFrontCluster(bool* frontC_hasJamVelocity);

  //!  \brief determine new intensitie values for front cluster aoi
  void DetermineIntensitiesForFrontClusterAoi(AreaOfInterest frontVehicleArea, bool* frontC_hasJamVelocity);

  //! \brief Adding new intensities for front cluster (at frontVehicleArea) if they are higher than before
  //! \param frontVehicleArea
  //! \param intensityFromAccelerationAtFollowingDriving
  //! \param probabilityFollowingDriving
  //! \param intensityFromAccelerationAtObstacleOnCurrentLane
  //! \param probabilityObstacleOnCurrentLane
  void AddRelevantFrontClusterIntensities(AreaOfInterest frontVehicleArea,
                                          double intensityFromAccelerationAtFollowingDriving,
                                          double probabilityFollowingDriving,
                                          double intensityFromAccelerationAtObstacleOnCurrentLane,
                                          double probabilityObstacleOnCurrentLane);

  //! \brief Resetting all front cluster intensities to default values
  void ResetIntensitiesFrontCluster();

  //! \brief calls FilterVehicle with corresponding AreaOfInterests
  std::vector<AreaOfInterest> FilterFrontClusterVehicle(std::vector<AreaOfInterest> vehicles);

  //! \brief filter out near or far front cluster side vehicle from vehicles
  void FilterFrontClusterSideVehicle(std::vector<AreaOfInterest>& vehicles);

  //! \brief get AreaOfInterest to erase
  AreaOfInterest GetAreaToErase(AreaOfInterest near, AreaOfInterest far);

  //! \brief Add most important situations of the left cluster to the main intensity vector.
  void GetIntensitiesLeftCluster(bool* leftC_hasJamVelocity);

  //! \brief Add most important situations of the right cluster to the main intensity vector.
  void GetIntensitiesRightCluster(bool* rightC_hasJamVelocity);

  //! \brief
  void GetIntensitiesSideCluster(bool* hasJamVelocity, std::map<AreaOfInterest, bool> clusterVehicles, std::map<Situation, double> situationIntensitiesCluster, Side side);

  //! \brief Add most important situations of the mesoscopic cluster to the mesoscopic intensity vector.
  void DetectMesoscopicSituations(const MesoscopicSituationFeatures& features, ZipMergingInterface& zipMerging);

  //! \brief Sets intensities for the current situation and determines mesoscopic Situation features
  MesoscopicSituationFeatures GenerateSituationIntensityVector();

  //! Shorten map to max values of highest intensity.
  //! \param [in] lhs         Numeric representation of the Intensity of every applicable situation
  //! \param [in] rhs           Number of Situations to be returned
  //! \return situationIntensitiesShortened       Shortened map to max values of highest intensity
  std::map<Situation, double> MostIntensiveSituations(std::map<Situation, double> lhs, int rhs);

  //! \brief Value that represents the overall intensitiy of a collection of different intensities.
  //! \param queryVector  A collection of different intensities
  //! \return intensity   Overall intensitiy of the vector
  double GetIntensityFromQueryVector(std::vector<bool>& queryVector);

  //! \brief Returns the intensity value of the case free driving.
  //! \return 1.      Intensity of the case free driving
  double GetIntensity_FreeDriving();

  //! \brief Returns the intensity value of the case of collision.
  //! \return 1.      Intensity of the case collision
  double GetIntensity_CollisionCase();

  //! \brief Returns the intensity value of the case obstacle on current lane.
  //! \param frontC_isObstacle    Determine, if an object in the aoi is an obstacle
  //! \return 0. / 1.     Intensity of the case obstacle on current lane
  double GetIntensity_ObstacleOnCurrentLane(bool frontC_isObstacle, bool frontC_isMinimumFollowingDistanceViolated);

  //! \brief Returns the intensity value of the case queued traffic.
  //! \param features Detected features to determine Queued traffic situation
  //! \return True if queued traffic was detected
  bool CheckMesoscopicSituationQueuedTraffic(const MesoscopicSituationFeatures& features);

  //! \brief Return the risk class of an intensity.
  //! \param intensityFactoredWithRisk    Intensity for the risk determination
  //! \return 1 / 2 / 3       Number, representing the risk class of an intensity (1 = Low / Default, 2 = Medium, 3 = High)
  int GetRiskClass(double intensityFactoredWithRisk);

  //! \brief Return the associated value of risk
  int GetRiskValue(Risk risk) const;

  //! \brief Check whether there is a risk class in the observed intensities, which is exceedes the one of a situation.
  //! \param currentSituation     The current situation
  //! \param observedIntensities  Observed intensities of the current situation
  //! \return yes/no
  bool HigherRiskClassIgnored(Situation currentSituation, std::map<Situation, double> observedIntensities);

  //! \brief Calls DetermineIntensityFromAcceleration in lateral direction
  //! \param acceleration
  double DetermineIntensityFromAccelerationLateral(units::acceleration::meters_per_second_squared_t acceleration) const;

  //! \brief Calculate the risk/situation intensity based on the acceleration necessary to avoid a collision.
  //! The result will be 0 if no acceleration is necessary and the intesity of low risk if the necessary acceleration
  //! is below comfortAcceleration. For baseAccelerations above comfortAcceleration, a value between the
  //! intensities of low risk and high risk scaled in relation to comfortAcceleration,
  //! ScmDefinitions::COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER and maxAcceleration is returned.
  //! \param baseAcceleration absolute value of the acceleration necessary to avoid a collision
  //! \param comfortAcceleration highest comfortable acceleration value for ego
  //! \param maxAcceleration maximum acceleration ego is capable of
  //! \return calculated intensity value (between 0 and intensity of high risk)
  double DetermineIntensityFromAcceleration(units::acceleration::meters_per_second_squared_t baseAcceleration, units::acceleration::meters_per_second_squared_t comfortAcceleration, units::acceleration::meters_per_second_squared_t maxAcceleration) const;

  Situation ObtainNewSituation(std::map<Situation, double> intensities);

  bool IsSignificantIntensityChange(double intensity, double intensityLast) const;

  /** @name Private Variables
   *   @{
   */
  /** @name Internal Parameters
   *    @{
   */

  //! Name of the current component
  const std::string COMPONENTNAME = "SituationManager";

  //! Define the areas, that belong to the rear cluster
  std::vector<AreaOfInterest> _frontClusterAreas = {AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR};

  //! Define the areas, that belong to the rear cluster
  std::vector<AreaOfInterest> _rearClusterAreas = {AreaOfInterest::EGO_REAR};

  //! Define the areas, that belong to the left cluster
  std::vector<AreaOfInterest> _leftClusterAreas = {AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR};

  //! Define the areas, that belong to the right cluster
  std::vector<AreaOfInterest> _rightClusterAreas = {AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR};

  //! Situation intensities within area of interest cluster rear
  std::map<Situation, double> _situationIntensities_rearCluster;

  //! Actual situation intensities.
  std::map<Situation, double> _situationIntensities;

  //! Last known situation intensities.
  std::map<Situation, double> _lastSituationIntensities{{Situation::UNDEFINED, ScmDefinitions::DEFAULT_VALUE}};

  //! Situation in last time step.
  Situation _situationLastTick{Situation::UNDEFINED};

  /**
   *  @} */
  // End of Internal Parameters
  /** @name External Parameters
   *  @{
   */

  //! Duration of the current situation [ms].
  units::time::millisecond_t _durationCurrentSituation = 0_ms;

  //! Cycle time of this components trigger task [ms].
  units::time::millisecond_t _cycleTime{};
  //! Provides access to the stochastics functionality of the framework.
  StochasticsInterface* _stochastics = nullptr;

  MentalModelInterface& _mentalModel;

  const FeatureExtractorInterface& _featureExtractor;
  const MentalCalculationsInterface& _mentalCalculations;
  SwervingInterface& _swerving;

  /**
   *  @} */
  // End of External Parameters
  /**
   *  @} */
  // End of Private Variables
};
/** @} */  // End of group SituationManager
