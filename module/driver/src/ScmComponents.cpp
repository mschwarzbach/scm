/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmComponents.h"

#include <memory>

#include "FeatureExtractor.h"
#include "GazeControl/GazeControlComponents.h"
#include "GazeControl/GazeControlInterface.h"
#include "LaneChangeDimension.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h"
#include "MentalCalculations.h"
#include "MentalModel.h"
#include "Merging/ZipMerging.h"
#include "SideLaneSafety/SideLaneSafety.h"
#include "SideLaneSafety/SideLaneSafetyInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactory.h"
#include "Swerving/Swerving.h"
#include "TrajectoryPlanner/TrajectoryPlanner.h"
#include "TrajectoryPlanner/TrajectoryPlanningQuery.h"

ScmComponents::ScmComponents(ScmDependenciesInterface& scmDependencies)
    : _scmDependencies(&scmDependencies)
{
  const auto trafficRules = scmDependencies.GetTrafficRulesScm();
  _featureExtractor = std::make_unique<FeatureExtractor>();
  _mentalCalculations = std::make_unique<MentalCalculations>(*_featureExtractor.get(), trafficRules->motorway.formRescueLane, units::velocity::meters_per_second_t(trafficRules->motorway.openSpeedLimit));
  _mentalModel = std::make_unique<MentalModel>(scmDependencies.GetCycleTime(), *_featureExtractor.get(), *_mentalCalculations.get(), scmDependencies);
  _mentalModel->Initialize(scmDependencies.GetDriverParameters(), scmDependencies.GetVehicleModelParameters(), scmDependencies.GetSpawnVelocity(), scmDependencies.GetStochastics());
  _ignoringOuterLaneOvertakingProhibition = std::make_unique<IgnoringOuterLaneOvertakingProhibition>(*_mentalModel.get());
  _longitudinalCalculations = std::make_unique<LongitudinalCalculations>(*_mentalModel.get());

  _vehicleQueryFactory = std::make_unique<SurroundingVehicleQueryFactory>(*_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation());
  _mentalCalculations->Initialize(_mentalModel.get(), _vehicleQueryFactory.get(), _longitudinalCalculations.get());

  _gazeComponents = std::make_unique<GazeControlComponents>(*_mentalModel.get(), scmDependencies.GetStochastics(), *_featureExtractor.get());

  _gazeControl = std::make_unique<GazeControl>(*_mentalModel.get(), scmDependencies.GetStochastics(), *_featureExtractor.get(), *scmDependencies.GetSurroundingObjectsScm(), *scmDependencies.GetOwnVehicleInformationScm(), scmDependencies.GetDriverParameters().idealPerception, *_gazeComponents.get());
  _gazeFollower = std::make_unique<GazeFollower>(*scmDependencies.GetOwnVehicleInformationScm());
 
  _auditoryPerception = std::make_unique<AuditoryPerception>(static_cast<units::time::millisecond_t>(scmDependencies.GetCycleTime()), scmDependencies.GetStochastics());
  _informationAcquisition = std::make_unique<InformationAcquisition>(*_mentalModel.get(), *_featureExtractor.get(), *_mentalCalculations.get(), *scmDependencies.GetOwnVehicleInformationScm(), *scmDependencies.GetSurroundingObjectsScm(), scmDependencies.GetDriverParameters().idealPerception, *scmDependencies.GetOwnVehicleRoutePose());

  auto laneChangeLengthCalculations{std::make_shared<LaneChangeLengthCalculations>()};
  auto laneChangeTrajectoryCalculations{std::make_shared<LaneChangeTrajectoryCalculations>(laneChangeLengthCalculations)};
  _trajectoryPlanner = std::make_unique<TrajectoryPlanner>(std::make_shared<TrajectoryPlanningQuery>(*_mentalModel), laneChangeTrajectoryCalculations, *_vehicleQueryFactory);
  _swerving = std::make_unique<Swerving>(*_mentalModel.get());
  _zipMerging = std::make_unique<ZipMerging>(*_mentalModel.get(), *_featureExtractor, *_mentalCalculations, std::make_shared<const LaneChangeDimension>(*_mentalModel.get()));
  _situationManager = std::make_unique<SituationManager>(scmDependencies.GetCycleTime(), scmDependencies.GetStochastics(), *_mentalModel.get(), *_featureExtractor.get(), *_mentalCalculations.get(), *_swerving.get());
  _situationManager->SetIgnoringOuterLaneOvertakingProhibition(_ignoringOuterLaneOvertakingProhibition.get());
  _actionManager = std::make_unique<ActionManager>(scmDependencies.GetStochastics(), *_mentalModel.get(), *_featureExtractor.get(), *_mentalCalculations.get(), *_swerving.get(), *_vehicleQueryFactory, *trafficRules, *_trajectoryPlanner, *_zipMerging);
  _actionImplementation = std::make_unique<ActionImplementation>(scmDependencies.GetStochastics(), *_mentalModel.get(), *_featureExtractor.get(), *_mentalCalculations.get(), *_swerving.get(), *_trajectoryPlanner, *_zipMerging);

  _algorithmSceneryCar = std::make_unique<AlgorithmSceneryCar>(*_mentalModel.get());
  static_cast<FeatureExtractor*>(_featureExtractor.get())->SetMentalModel(*_mentalModel.get());
  static_cast<FeatureExtractor*>(_featureExtractor.get())->SetMentalCalculations(*_mentalCalculations.get());
  static_cast<FeatureExtractor*>(_featureExtractor.get())->SetOuterLaneOvertakingProhibitionQuota(_ignoringOuterLaneOvertakingProhibition.get());
  static_cast<FeatureExtractor*>(_featureExtractor.get())->SetSurroundingVehicleQueryFactory(_vehicleQueryFactory.get());
}

MentalModelInterface* ScmComponents::GetMentalModel()
{
  return _mentalModel.get();
}
GazeControlInterface* ScmComponents::GetGazeControl()
{
  return _gazeControl.get();
}
GazeFollowerInterface* ScmComponents::GetGazeFollower()
{
  return _gazeFollower.get();
}
SituationManagerInterface* ScmComponents::GetSituationManager()
{
  return _situationManager.get();
}
ActionManagerInterface* ScmComponents::GetActionManager()
{
  return _actionManager.get();
}
ActionImplementationInterface* ScmComponents::GetActionImplementation()
{
  return _actionImplementation.get();
}
AlgorithmSceneryCarInterface* ScmComponents::GetAlgorithmSceneryCar()
{
  return _algorithmSceneryCar.get();
}
AuditoryPerceptionInterface* ScmComponents::GetAuditoryPerception()
{
  return _auditoryPerception.get();
}
InformationAcquisitionInterface* ScmComponents::GetInformationAcquisition()
{
  return _informationAcquisition.get();
}
FeatureExtractorInterface* ScmComponents::GetFeatureExtractor()
{
  return _featureExtractor.get();
}
MentalCalculationsInterface* ScmComponents::GetMentalCalculations()
{
  return _mentalCalculations.get();
}
ScmDependenciesInterface* ScmComponents::GetScmDependencies()
{
  return _scmDependencies;
}
IgnoringOuterLaneOvertakingProhibitionInterface* ScmComponents::GetIgnoringOuterLaneOvertakingProhibition()
{
  return _ignoringOuterLaneOvertakingProhibition.get();
}
SwervingInterface* ScmComponents::GetSwerving()
{
  return _swerving.get();
}

LongitudinalCalculationsInterface* ScmComponents::GetLongitudinalCalculations()
{
  return _longitudinalCalculations.get();
}

ZipMergingInterface* ScmComponents::GetZipMerging()
{
  return _zipMerging.get();
}

GazeControlComponentsInterface* ScmComponents::GetGazeControlComponents()
{
  return _gazeComponents.get();
}
