/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Merging/ZipMergingInterface.h"
#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h"

class SituationManagerInterface
{
public:
  virtual ~SituationManagerInterface() = default;

  //! @brief Setter for IgnoringOuterLaneOvertakingProhibition
  //! @param ignoringOuterLaneOvertakingProhibition
  virtual void SetIgnoringOuterLaneOvertakingProhibition(IgnoringOuterLaneOvertakingProhibitionInterface* ignoringOuterLaneOvertakingProhibition) = 0;

  //! @brief Generate any intensity vector and sample from it.
  virtual void GenerateIntensitiesAndSampleSituation(ZipMergingInterface& zipMerging) = 0;

  //! @brief Returns the duration of the agents current situation.
  //! @return _durationCurrentSituation       The duration of the agents current situation
  virtual units::time::millisecond_t GetCurrentSituationDuration() = 0;

  //! @brief Set the duration of the agents current situation.
  //! @param [in] durationCurrentSituation        Duration of the current situation [ms]
  virtual void SetCurrentSituationDuration(units::time::millisecond_t durationCurrentSituation) = 0;

  //! @brief Setter for situation of last time step.
  //! @param _situationLastTick
  virtual void SetSituationLastTick(Situation situation) = 0;
};
