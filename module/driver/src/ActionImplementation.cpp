/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ActionImplementation.h"

#include <algorithm>
#include <cmath>
#include <stdexcept>

#include "ActionManager.h"
#include "LaneQueryHelper.h"
#include "LateralActionQuery.h"
#include "MentalModelInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/CommonHelper.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "include/common/VehicleProperties.h"

ActionImplementation::ActionImplementation(StochasticsInterface* stochastics, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner, ZipMergingInterface& zipMerging)
    : _featureExtractor{featureExtractor},
      _stochastics{stochastics},
      _mentalModel{mentalModel},
      _mentalCalculations{mentalCalculations},
      _swerving{swerving},
      _trajectoryPlanner{trajectoryPlanner},
      _zipMerging{zipMerging},
      _cycleTime{static_cast<units::time::millisecond_t>(_mentalModel.GetCycleTime())}
{
}

void ActionImplementation::UpdateInformationFromActionManager(ActionManagerInterface* actionManagerInterface)
{
  _lateralActionLastTick = actionManagerInterface->GetLateralActionLastTick();
}

void ActionImplementation::ImplementAction()
{
  // set longitudinal guidance
  if (_mentalModel.IsAdjustmentBaseTimeExpired())
  {
    SwitchLongitudinalActionState();
  }

  // set lateral guidance
  SetLateralOutputKeys();

  // set indicator state (indicator lights)
  SetIndicator();

  // set flasher state (high beam lights)
  SetFlasher();
}

void ActionImplementation::LimitAccelerationDueToPlannedSwerving()
{
  const Situation currentSituation{_mentalModel.GetCurrentSituation()};
  const bool isRelevantSituation{currentSituation == Situation::FOLLOWING_DRIVING || currentSituation == Situation::OBSTACLE_ON_CURRENT_LANE};
  const bool hasJamVelocity{_featureExtractor.HasJamVelocityEgo()};

  if (_swerving.IsSwervingPlanned() && isRelevantSituation && !hasJamVelocity)
  {
    _accelerationWishLongitudinal = units::math::min(_accelerationWishLongitudinal, 0._mps_sq);
  }
}

void ActionImplementation::SetIndicator()
{
  LateralActionQuery currentAction(_mentalModel.GetLateralAction());
  LateralActionQuery lastAction(_lateralActionLastTick);

  // check for change of side lane orientation
  if ((currentAction.IsRelevantForIndicatorLeft() && lastAction.IsRelevantForIndicatorLeft()) ||
      currentAction.IsRelevantForIndicatorRight() && lastAction.IsRelevantForIndicatorRight())
  {
    _durationCurrentActionStateSideLaneOrientation += _cycleTime;
  }
  else
  {
    _durationCurrentActionStateSideLaneOrientation = 0_ms;
  }

  double probIndicatorActivation = _mentalModel.GetIndicatorActiviationProbability();

  // implement indicator activation
  if (currentAction.IsRelevantForIndicatorLeft())
  {
    _indicatorStateNext = ImplementStochasticIndicatorActivation(scm::LightState::Indicator::Left,
                                                                 probIndicatorActivation);
  }
  else if (currentAction.IsRelevantForIndicatorRight())
  {
    _indicatorStateNext = ImplementStochasticIndicatorActivation(scm::LightState::Indicator::Right,
                                                                 probIndicatorActivation);
  }
  else
  {
    _indicatorStateNext = scm::LightState::Indicator::Off;
  }
}

void ActionImplementation::SetFlasher()
{
  _flasherActiveNext = false;

  Situation currentSituation = _mentalModel.GetCurrentSituation();
  auto durationCurrentSituation = _mentalModel.GetDurationCurrentSituation();
  double probHighRiskFlasherActivation = _mentalModel.GetFlasherActiviationProbability();

  if ((currentSituation == Situation::LANE_CHANGER_FROM_RIGHT && _mentalModel.GetSituationRisk(Situation::LANE_CHANGER_FROM_RIGHT) == Risk::HIGH) ||
      (currentSituation == Situation::LANE_CHANGER_FROM_LEFT && _mentalModel.GetSituationRisk(Situation::LANE_CHANGER_FROM_LEFT) == Risk::HIGH))
    ImplementStochasticHighRiskFlasherActivation(probHighRiskFlasherActivation, durationCurrentSituation);
}

void ActionImplementation::SwitchLongitudinalActionState()
{
  const auto VTargetEgo = _mentalModel.GetAbsoluteVelocityTargeted();
  const auto leadingVehicle{_mentalModel.GetLeadingVehicle()};
  const auto currentTtc = leadingVehicle != nullptr ? leadingVehicle->GetTtc().GetValue() : ScmDefinitions::TTC_LIMIT;
  units::time::second_t limitedTtc = currentTtc < 0._s ? ScmDefinitions::TTC_LIMIT : currentTtc;
  const units::time::second_t lowerLimitTtc = 3._s;
  const units::time::second_t upperLimitTtc = 9._s;
  limitedTtc = std::clamp(limitedTtc, lowerLimitTtc, upperLimitTtc);
  double proportionTtc = (limitedTtc - lowerLimitTtc) / (upperLimitTtc - lowerLimitTtc);
  auto maxDecelerationUnderTtc =
      units::math::max(_mentalModel.GetComfortLongitudinalDeceleration(),
                       _mentalModel.GetDriverParameters().maximumLongitudinalDeceleration -
                           (_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration -
                            GetDecelerationFromPowertrainDrag()) *
                               proportionTtc);

  switch (_mentalModel.GetLongitudinalActionState())
  {
    case LongitudinalActionState::STOPPED:
      _accelerationWishLongitudinal = 0._mps_sq;
      break;
    case LongitudinalActionState::ACTIVE_ZIP_MERGING:
    case LongitudinalActionState::PASSIVE_ZIP_MERGING:
    {
      const auto maxAcceleration{_mentalModel.GetDriverParameters().maximumLongitudinalAcceleration};
      const auto comfortAcceleration{_mentalModel.GetDriverParameters().comfortLongitudinalAcceleration};
      const auto acceleration{_mentalModel.GetLongitudinalActionState() == LongitudinalActionState::ACTIVE_ZIP_MERGING ? (maxAcceleration + comfortAcceleration) / 2 : comfortAcceleration};
      const auto accelerationWishFromMerging{_zipMerging.CalculateFollowingAcceleration(acceleration, maxDecelerationUnderTtc)};

      if (units::math::isinf(accelerationWishFromMerging))
      {
        _accelerationWishLongitudinal = GetAccelerationForSpeedAdjustment(VTargetEgo);
      }
      else
      {
        _accelerationWishLongitudinal = accelerationWishFromMerging;
      }
      break;
    }
    case LongitudinalActionState::PREPARING_TO_MERGE:
      _accelerationWishLongitudinal = AccelerationWishWhileMerging();
      break;

    case LongitudinalActionState::SPEED_ADJUSTMENT:
      _accelerationWishLongitudinal = GetAccelerationForSpeedAdjustment(VTargetEgo);
      break;

    case LongitudinalActionState::BRAKING_TO_END_OF_LANE:
    {
      const auto brakedist = _mentalModel.GetDistanceToEndOfLane(0, true);
      if (brakedist >= 4._m)
      {
        const auto velocityEgo{_mentalModel.GetAbsoluteVelocityEgo(false)};
        // Always slow down with at least comfort deceleration
        const auto decelerationLongitudinalComfort = -_mentalModel.GetComfortLongitudinalDeceleration();
        // Acceleration to approach end of lane
        const auto decelerationLongitudinalEndOfLane = units::math::max(-_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration,
                                                                        -velocityEgo * velocityEgo / 2. / units::math::max(brakedist, 1._m));
        _accelerationWishLongitudinal = units::math::min(decelerationLongitudinalComfort, decelerationLongitudinalEndOfLane);
      }
      else
      {
        // Keep standstill
        _accelerationWishLongitudinal = -_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration;
      }
      break;
    }

    case LongitudinalActionState::APPROACHING:
      if (leadingVehicle != nullptr)
      {
        _accelerationWishLongitudinal = std::clamp(
            _mentalCalculations.CalculateFollowingAcceleration(*leadingVehicle, maxDecelerationUnderTtc, 0_m),
            -maxDecelerationUnderTtc,
            _mentalModel.GetComfortLongitudinalAcceleration());
        _accelerationWishLongitudinal = units::math::min(_accelerationWishLongitudinal, GetAccelerationForSpeedAdjustment(VTargetEgo));
      }
      break;

    case LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE:
    {
      if (leadingVehicle != nullptr)
      {
        auto maxAcceleration = _mentalModel.GetDriverParameters().comfortLongitudinalAcceleration;
        if (_mentalModel.IsMergePreparationActive())
        {
          maxAcceleration = _mentalModel.GetAccelerationLimitForMerging();
          maxDecelerationUnderTtc = _mentalModel.GetDriverParameters().comfortLongitudinalDeceleration;
        }

        const auto accelerationWish{
            std::clamp(
                _mentalCalculations.CalculateFollowingAcceleration(*leadingVehicle, maxDecelerationUnderTtc, 0_m),
                -maxDecelerationUnderTtc,
                maxAcceleration)};
        _accelerationWishLongitudinal = units::math::min(accelerationWish, GetAccelerationForSpeedAdjustment(VTargetEgo));
      }
      break;
    }

    case LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE:
    {
      if (leadingVehicle != nullptr)
      {
        const auto accelerationWish{
            std::clamp(
                _mentalCalculations.CalculateFollowingAcceleration(*leadingVehicle, maxDecelerationUnderTtc, 0_m),
                -_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration,
                -GetDecelerationFromPowertrainDrag())};
        _accelerationWishLongitudinal = units::math::min(accelerationWish, GetAccelerationForSpeedAdjustment(VTargetEgo));
      }
      break;
    }

    default:
      throw std::runtime_error("ActionImplementation - SwitchLongitudinalActionState: Unknown lateral action!");
      break;
  }

  if (!_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING) && !_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING))
  {
    _accelerationWishLongitudinal = AdjustAccelerationWishDueToCooperation(_accelerationWishLongitudinal);
  }
  LimitAccelerationDueToPlannedSwerving();
  _accelerationWishLongitudinal = LimitAccelerationDueToSuspiciousVehicle(_accelerationWishLongitudinal);
}

units::acceleration::meters_per_second_squared_t ActionImplementation::AccelerationWishWhileMerging() const
{
  const auto gap = _mentalModel.GetMergeGap();
  if (!gap.IsValid())
  {
    return _accelerationWishLongitudinal;  // return current accelerationWish if the gap should be invalid at this time
  }

  const auto* leader = _mentalModel.GetVehicle(gap.leaderId);

  if (LateralActionQuery{_mentalModel.GetLateralAction()}.IsLaneChanging())
  {
    // When a lane change is already in progress, the targeted velocity profile for merging is ignored in favor of the
    // regular control for lane changes.
    const auto velocityTargetEgo{
        (leader == nullptr) ? _mentalModel.GetAbsoluteVelocityTargeted() : leader->GetAbsoluteVelocity().GetValue()};
    return GetAccelerationForSpeedAdjustment(velocityTargetEgo);
  }

  // The current merge phase is determined solely based on time passed (ignoring whether certain target velocities were
  // reached, because this may be influenced by e.g. a vehicle ahead of ego.)
  const auto mergePhases = gap.selectedMergePhases;
  const auto timePassedCurrentMerge{(_mentalModel.GetTime() - gap.timestep) / 1000.0};
  auto currentMergePhase = std::find_if(mergePhases.begin(), mergePhases.end(), [&](auto phase)
                                        { return timePassedCurrentMerge < phase.time; });

  auto accelerationWish{0_mps_sq};
  // A merge gap *should* be replaced long before reaching the end of its last phase. Should we find ourself beyond it
  // anyway, just hold the current velocity by leaving acceleration wish at zero.
  if (currentMergePhase != mergePhases.end())
  {
    // In the first phase, we try to accelerate/decelerate to approaching velocity (or drive at constant speed).
    // Afterwards, we always will always aim for the velocity of the targeted gap (if accelerating/decelerating).
    const auto targetVelocity{*currentMergePhase == mergePhases.front() ? gap.approachingVelocity : gap.velocity};
    const auto currentVelocity{_mentalModel.GetLongitudinalVelocityEgo()};

    // Accelerate or decelerate if we are in the appropriate phase and haven't reached the target velocity yet.
    // Doing this solely based on time may overshoot because previously planned acceleration could have been limited
    // by e.g. a vehicle ahead of ego.
    // (Leave acceleration wish at zero for constant driving phases.)
    if (currentMergePhase->mergeAction == MergeAction::ACCELERATING && currentVelocity < targetVelocity)
    {
      accelerationWish = _mentalModel.GetAccelerationLimitForMerging();
    }
    if (currentMergePhase->mergeAction == MergeAction::DECELERATING && currentVelocity > targetVelocity)
    {
      accelerationWish = -_mentalModel.GetDecelerationLimitForMerging();
    }
  }

  const auto egoFrontVehicle{_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)};
  if (egoFrontVehicle == nullptr)
  {
    // If there is no vehicle ahead to worry about, the value can be used as is.
    return accelerationWish;
  }

  // If there is a vehicle ahead in the ego lane, limit acceleration in order to keep a safe distance to it.
  const auto maxDeceleration{_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration};
  auto followingAcceleration{_mentalCalculations.CalculateFollowingAcceleration(*egoFrontVehicle, maxDeceleration, 0._m)};
  // The required braking for following may be greater than what's possible. The value is limited to maxDeceleration.
  followingAcceleration = std::max(followingAcceleration, -maxDeceleration);

  return units::math::min(accelerationWish, followingAcceleration);
}

units::acceleration::meters_per_second_squared_t ActionImplementation::LimitAccelerationDueToSuspiciousVehicle(units::acceleration::meters_per_second_squared_t currentAccelerationWish) const
{
  const auto currentSituation = _mentalModel.GetCurrentSituation();
  if (IsSuspiciousObject(currentSituation) &&
      _featureExtractor.IsDeceleratingDueToSuspiciousSideVehicles() &&
      !_featureExtractor.HasJamVelocityEgo())
  {
    currentAccelerationWish = units::math::min(0.0_mps_sq, -units::math::abs(currentAccelerationWish));
  }

  return currentAccelerationWish;
}

bool ActionImplementation::IsSuspiciousObject(Situation situation) const
{
  return situation == Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE;
}

bool ActionImplementation::IsSuspiciousObjectOrLaneChanger(Situation situation) const
{
  return IsSuspiciousObject(situation) ||
         situation == Situation::LANE_CHANGER_FROM_LEFT ||
         situation == Situation::LANE_CHANGER_FROM_RIGHT;
}

bool ActionImplementation::IsSideCollisionRisk(Situation situation) const
{
  return situation == Situation::SIDE_COLLISION_RISK_FROM_LEFT ||
         situation == Situation::SIDE_COLLISION_RISK_FROM_RIGHT;
}

bool ActionImplementation::GetLateralDisplacementReached() const
{
  return _lateralDisplacementReached;
}

units::length::meter_t ActionImplementation::GetDesiredLateralDisplacement() const
{
  return _desiredLateralDisplacement;
}
units::length::meter_t ActionImplementation::GetLateralDisplacementTraveled() const
{
  return _currentTrajectoryState.traveledWidth;
}

scm::LightState::Indicator ActionImplementation::GetNextIndicatorState() const
{
  return _indicatorStateNext;
}

bool ActionImplementation::GetFlasherActiveNext() const
{
  return _flasherActiveNext;
}

units::acceleration::meters_per_second_squared_t ActionImplementation::GetLongitudinalAccelerationWish() const
{
  return _accelerationWishLongitudinal;
}

units::angular_acceleration::radians_per_second_squared_t ActionImplementation::GetLateralDeviationGain() const
{
  return _gainLateralDeviation;
}

units::length::meter_t ActionImplementation::GetLateralDeviationNext() const
{
  return _lateralDeviationNext;
}

units::frequency::hertz_t ActionImplementation::GetHeadingErrorGain() const
{
  return _gainHeadingError;
}

units::angle::radian_t ActionImplementation::GetHeadingErrorNext() const
{
  return _headingErrorNext;
}

units::curvature::inverse_meter_t ActionImplementation::GetKappaManoeuvre() const
{
  return _kappaManoeuvre;
}

units::curvature::inverse_meter_t ActionImplementation::GetKappaRoad() const
{
  return _kappaRoad;
}

units::curvature::inverse_meter_t ActionImplementation::GetKapaSet() const
{
  return _kappaSet;
}

units::acceleration::meters_per_second_squared_t ActionImplementation::GetDecelerationFromPowertrainDrag() const
{
  const auto decelerationFromPowertrainDrag = scm::common::map::query(_mentalModel.GetVehicleModelParameters().properties, scm::common::vehicle::properties::DecelerationFromPowertrainDrag);
  if (!decelerationFromPowertrainDrag.has_value())
  {
    throw std::runtime_error("DecelerationFromPowertrainDrag was not defined in VehicleCatalog");
  }

  return units::make_unit<units::acceleration::meters_per_second_squared_t>(std::stod(decelerationFromPowertrainDrag.value()));
}

void ActionImplementation::DetermineLateralDisplacement()
{
  auto lateralDisplacement{0._m};
  const LateralActionQuery actionState{_mentalModel.GetLateralAction()};
  const bool isModifiedLaneChange{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->modifyLaneChange};
  bool isModifyIntoLaneKeeping = isModifiedLaneChange && actionState.IsLaneKeeping();

  if (!_isStartOfRealign)
  {
    if (actionState.IsLaneChanging())
    {
      lateralDisplacement = _currentTrajectoryState.desiredDisplacement;
    }
    else if (isModifyIntoLaneKeeping)
    {
      lateralDisplacement = _mentalCalculations.CalculateLateralOffsetNeutralPositionScaled() -
                            _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->lateralPosition;

      if (units::math::abs(lateralDisplacement) < .5_m)
      {
        isModifyIntoLaneKeeping = false;                                                      // Small lateral displacements will be handled by the lateral controller without a trajectory
        _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->modifyLaneChange = false;  // Prevent creation of a new trajectory afterwards
      }
    }
    else if (actionState.IsSwerving() && _mentalModel.IsFrontSituation())  // For Swerve (left right comfort or urgent)
    {
      AreaOfInterest objectToEvade{_mentalModel.GetObjectToEvade()};

      // Set Side
      Side side = actionState.IsSwervingRight() ? Side::Right : Side::Left;

      // Get lateralDistanceTo evade
      auto distanceToEvade = _swerving.DetermineLateralDistanceToEvade(objectToEvade, side, actionState.IsSwervingUrgent());  // Note: This function should never be called for side aoi since use case here is lane change and swerving. Expand when functionis used for other.

      lateralDisplacement = side == Side::Left ? distanceToEvade : -distanceToEvade;
    }

    // Lateral displacement will only be reset to lane width when no lane change, swerving or zip merge is in progress.
    const bool isNotInLateralManeuver = !_mentalModel.GetRemainInLaneChangeState() &&
                                        !_mentalModel.GetRemainInSwervingState();
    const bool isAbortIntoLaneChange =
        isModifiedLaneChange && actionState.IsLaneChanging() && lateralDisplacement * _desiredLateralDisplacement.value() < 0._m;
    if ((isNotInLateralManeuver || isAbortIntoLaneChange || isModifyIntoLaneKeeping) && lateralDisplacement != 0._m)
    {
      _desiredLateralDisplacement = lateralDisplacement;
    }
  }
  else
  {
    _desiredLateralDisplacement = _neutralPosition - _neutralPositionLast;
  }
}

void ActionImplementation::SetSwervingValues(const bool isSwerving, TrajectoryCalculations::TrajectoryDimensions& dimensions)
{
  if (isSwerving && _swerving.GetSwervingState() == SwervingState::NONE)
  {
    _swerving.SetSwervingState(SwervingState::EVADING);
    const auto objectToEvade = _mentalModel.GetObjectToEvade();
    const auto timeToSwerve = _swerving.DetermineTimeToSwervePast(objectToEvade);
    _swerving.SetSwervingEndTime(_mentalModel.GetTime() + timeToSwerve + SWERVING_END_TIME_SAFETY_BUFFER);
  }
  else if (isSwerving && _swerving.GetSwervingState() == SwervingState::RETURNING)
  {
    dimensions.width = _swerving.DetermineSwervingReturnDistance(_featureExtractor, _mentalCalculations);
    _desiredLateralDisplacement = dimensions.width;
  }
  else if (!isSwerving && _swerving.IsSwervingPlanned())
  {
    _swerving.SetAoiToSwerve(AreaOfInterest::NumberOfAreaOfInterests);  // Planned swerving canceled due to lane change action
  }
}

void ActionImplementation::DetermineManeuverValues()
{
  const LateralActionQuery actionState{_mentalModel.GetLateralAction()};
  const bool isChangingLanes = actionState.IsLaneChanging();
  const bool isSwerving = actionState.IsSwerving();
  const units::time::second_t dt = _cycleTime;

  // Calculations of set values during lane keeping
  _headingSet = 0._rad;
  _lateralPositionSet = 0._m;

  // Open loop variables
  _kappaManoeuvre = 0._i_m;
  kappaSet = _kappaManoeuvre + _kappaRoad;  // set curvature

  // Calculations of set values while changing lanes

  if (isChangingLanes || isSwerving)
  {
    if (actionState.IsLaneChangingLeft() || actionState.IsSwervingLeft())
    {
      _currentTrajectoryState.traveledWidth = _mentalModel.DetermineDistanceTraveledLeft(GetDesiredLateralDisplacement());
    }

    if (actionState.IsLaneChangingRight() || actionState.IsSwervingRight())
    {
      _currentTrajectoryState.traveledWidth = _mentalModel.DetermineDistanceTraveledRight(GetDesiredLateralDisplacement());
    }

    _currentTrajectoryState.traveledDistanceAlongTrajectory += _mentalModel.GetAbsoluteVelocityEgo(false) * dt;

    if (_lateralDisplacementReached ||
        (isSwerving && _swerving.GetSwervingState() == SwervingState::DEFUSING))
    {
      _headingSet = 0._rad;
      _lateralPositionSet = _desiredLateralDisplacement;
      _kappaManoeuvre = 0._i_m;
      _mentalModel.ResetPlannedLaneChangeDimensions();
    }
    else if (isChangingLanes ||  // Normal lane change or swerving
             (isSwerving && (_swerving.GetSwervingState() == SwervingState::EVADING ||
                             _swerving.GetSwervingState() == SwervingState::RETURNING)))
    {
      ProgressTrajectory();
    }
    else
    {
      throw std::runtime_error("ActionImplementation - DetermineManeuverValues: Unknown lane change state!");  // Note: Temporary, until modify and abort are implemented again
    }

    // Desired lateral displacement is reached. Do not calculate any further trajectory in next time steps (undefined behaviour)
    if (_mentalModel.HasEgoLateralDistanceReached(_desiredLateralDisplacement, _lateralPositionSet))
    {
      _lateralDisplacementReached = true;
    }

    _lateralPositionAct = _currentTrajectoryState.traveledWidth;

    kappaSet = _kappaManoeuvre + _kappaRoad;
  }
  else
  {
    _lateralPositionSet += _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->lateralOffsetNeutralPositionScaled;
  }
}

void ActionImplementation::DetermineOutputValues()
{
  // calculation of heading error using transient behavior first order
  const auto headingErrorWish = _headingSet - _headingAct;

  // calculation of lateral deviation error
  const auto lateralDeviation = _lateralPositionSet - _lateralPositionAct;
  // writing outputs for AlgorithmLateralDriver
  _gainLateralDeviation = 20._rad_per_s_sq;  // Gain for lateral deviation controller in AlgorithmLateralDriver
  _lateralDeviationNext = lateralDeviation;
  _gainHeadingError = 7.5_Hz;  // Gain for heading error controller in AlgorithmLateralDriver
  _headingErrorNext = headingErrorWish;
}

bool ActionImplementation::IsStartOfLaneChange() const
{
  const LateralActionQuery action(_mentalModel.GetLateralAction());
  return action.IsLaneChanging() && !_mentalModel.GetRemainInLaneChangeState();
}

bool ActionImplementation::IsStartOfSwervingOrReturningToLaneCenter() const
{
  const LateralActionQuery action{_mentalModel.GetLateralAction()};
  return action.IsSwerving() && (!_mentalModel.GetRemainInSwervingState() || _swerving.HasSwervingStateSwitchedToReturning());
}

void ActionImplementation::CheckForChangeInOffset()
{
  if (!_mentalModel.GetRemainInRealignState())
  {
    _neutralPosition = _mentalCalculations.CalculateLateralOffsetNeutralPositionScaled();
    auto constexpr Threshold = 0.5_m;
    const LateralActionQuery actionState{_mentalModel.GetLateralAction()};
    const bool isSteeredLateralAction = actionState.IsLaneChanging() || actionState.IsSwerving();
    if (units::math::abs(_neutralPosition - _neutralPositionLast) > Threshold && !isSteeredLateralAction)
    {
      _isStartOfRealign = true;
    }
    else
    {
      _isStartOfRealign = false;
    }
    _neutralPositionLast = _neutralPosition;
  }
  else
  {
    _isStartOfRealign = false;
  }
}

TrajectoryState ActionImplementation::PlanTrajectory(RelativeLane targetLane)
{
  const LateralActionQuery action(_mentalModel.GetLateralAction());
  bool isSwerving = action.IsSwerving();
  auto trajectoryDimensions{_mentalModel.GetPlannedLaneChangeDimensions().value_or(_trajectoryPlanner.CalculateLaneChangeDimensions(targetLane))};
  SetSwervingValues(isSwerving, trajectoryDimensions);

  if (!isSwerving)
  {
    _desiredLateralDisplacement = trajectoryDimensions.width;
  }

  return TrajectoryState{
      .trajectory = _trajectoryPlanner.PlanTrajectory(trajectoryDimensions),
      .initialLateralOffset = _mentalModel.GetLateralPositionFrontAxle(),
      .desiredDisplacement = _desiredLateralDisplacement};
}

void ActionImplementation::SetLateralOutputKeys()
{
  CheckForChangeInOffset();
  DetermineLateralDisplacement();

  _lateralPositionAct = _mentalModel.GetLateralPositionFrontAxle();  // Current lateral coordinate
  _headingAct = _mentalModel.GetHeading();                           // Current heading regarding trajectory including road curvature
  _kappaRoad = _mentalModel.GetCurvatureOnCurrentPosition();         // Current curvature of road
  const bool isModifiedLaneChange{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->modifyLaneChange};

  if (IsStartOfLaneChange() || IsStartOfSwervingOrReturningToLaneCenter() || isModifiedLaneChange || _isStartOfRealign)
  {
    _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->modifyLaneChange = false;

    const LateralActionQuery action(_mentalModel.GetLateralAction());
    const auto& infrastructure = _mentalModel.GetInfrastructureCharacteristics();

    auto neighbourLane = action.GetDirection() == LateralAction::Direction::LEFT ? RelativeLane::LEFT : RelativeLane::RIGHT;

    try
    {
      _currentTrajectoryState = PlanTrajectory(neighbourLane);

      // HOTFIX SCM-1819
      // TODO: can be removed together with state for lane change progress (LaneChangeWidthTraveled) from MentalModel
      _mentalModel.SetLaneChangeWidthTraveled(_currentTrajectoryState.traveledWidth);

      _lateralDisplacementReached = false;
    }
    catch (const std::invalid_argument& e)
    {
      _currentTrajectoryState = TrajectoryState();
      _lateralDisplacementReached = true;
      std::cout << "Time: " << std::to_string(_mentalModel.GetTime().value())
                << " SCM Agent: " << std::to_string(_mentalModel.GetAgentId())
                << " Trajectory calculation failed: " << e.what()
                << '\n';  // TODO Integrate into logger when available
    }
  }
  DetermineManeuverValues();
  DetermineOutputValues();

  if (IsStartOfLaneChange() || IsStartOfSwervingOrReturningToLaneCenter())
  {
    // Swerving Flag
    if (IsStartOfSwervingOrReturningToLaneCenter())
    {
      _mentalModel.SetRemainInSwervingState(true);
    }
    // Lane Change
    const LateralActionQuery actionState{_mentalModel.GetLateralAction()};
    if (actionState.IsLaneChanging())
    {
      _mentalModel.SetRemainInLaneChangeState(true);
      _mentalModel.SetIsEndOfLateralMovement(false);
    }
  }
}

void ActionImplementation::ProgressTrajectory()
{
  for (int i = _currentTrajectoryState.lastUsedIndex; i < _currentTrajectoryState.trajectory.size(); ++i)
  {
    if (_currentTrajectoryState.traveledDistanceAlongTrajectory <= _currentTrajectoryState.trajectory.at(i).sTrajectory)  // Current position along the s coordinate of the trajectory
    {
      _lateralPositionSet = units::length::meter_t(_currentTrajectoryState.trajectory.at(i).t);
      _headingSet = _currentTrajectoryState.trajectory.at(i).heading;
      _kappaManoeuvre = _currentTrajectoryState.trajectory.at(i).curvature;

      _currentTrajectoryState.lastUsedIndex = i;
      return;
    }
  }
}

units::acceleration::meters_per_second_squared_t ActionImplementation::GetAccelerationForSpeedAdjustment(units::velocity::meters_per_second_t velocityTargetEgo) const
{
  const auto velocityEgoEstimated{_mentalModel.GetAbsoluteVelocityEgo(true)};
  auto deltaVelocityToSpeedlimit = _mentalModel.GetVelocityLegalEgo() - velocityEgoEstimated;
  auto deltaVelocityToAdjust = velocityTargetEgo - velocityEgoEstimated;
  auto comfortAcceleration = _mentalModel.GetComfortLongitudinalAcceleration();
  auto comfortDeceleration = _mentalModel.GetComfortLongitudinalDeceleration();

  if (_featureExtractor.IsOnEntryLane())
  {
    comfortAcceleration = units::math::min(units::math::max(3._mps_sq, 2. * comfortAcceleration),
                                           _mentalModel.GetDriverParameters().maximumLongitudinalAcceleration);  // Allow very high acceleration on entry lanes

    comfortDeceleration = units::math::min(2. * comfortDeceleration,
                                           _mentalModel.GetDriverParameters().maximumLongitudinalDeceleration);
  }

  double urgencyFactor = _mentalCalculations.GetUrgencyFactorForAcceleration();  // combined urgency

  // reduce deceleration for smaller speedlimit gaps
  if (!std::isinf(deltaVelocityToSpeedlimit.value()) &&                                                     // there even is a speedlimit
      deltaVelocityToSpeedlimit >= -2. * _mentalModel.GetDriverParameters().amountOfSpeedLimitViolation &&  // negative gap is not too big
      deltaVelocityToSpeedlimit <= deltaVelocityToAdjust)                                                   // deceleration to speedlimit will not hinder other influences on target velocity
  {
    comfortDeceleration = GetDecelerationFromPowertrainDrag();
  }

  // accelerate to cooperate with anticipated lane changer
  if (_mentalModel.IsAbleToCooperateByAccelerating())
  {
    double accExtensionFactor = .5 + urgencyFactor;
    comfortAcceleration = comfortAcceleration * accExtensionFactor;
    // note: VTargetEgo is already adjusted in Mental Calculations using CalculateDeltaVelocityWish
  }

  // return units::math::max(units::math::min(comfortAcceleration, deltaVelocityToAdjust / 2.), -comfortDeceleration);
  return static_cast<units::acceleration::meters_per_second_squared_t>(std::max(std::min(comfortAcceleration.value(), deltaVelocityToAdjust.value() / 2.), -comfortDeceleration.value()));
}

scm::LightState::Indicator ActionImplementation::ImplementStochasticIndicatorActivation(scm::LightState::Indicator indicatorState,
                                                                                        double probIndicatorActivation)
{
  const StochasticActivationModelParameters indicatorParameters{_mentalModel.GetDriverParameters().nationalSpecifics.behaviouralPatterns.indicatorActivation};
  const auto timeToStartIndicator{indicatorParameters.earliestTimeToActivate};
  const auto timeToEndIndicator{indicatorParameters.latestTimeToActivate};
  const bool maxProbApplies{DoesMaxProbabilityApply()};
  double maxProb{maxProbApplies ? indicatorParameters.maximumProbabilityToActivate : 100.};
  const double minProb{indicatorParameters.minimumProbabilityToActivate};
  const bool hasStateChanged{_indicatorStateNext != indicatorState};

  const auto [adjusted_timeToStartIndicator, adjusted_timeToEndIndicator, adjusted_minProb, adjusted_maxProb] = AdjustIndicatorParametersDueToCooperation(timeToStartIndicator, timeToEndIndicator, minProb, maxProb);

  bool activate = false;  // Default value

  activate = _mentalModel.ImplementStochasticActivation(adjusted_timeToStartIndicator, adjusted_timeToEndIndicator, _durationCurrentActionStateSideLaneOrientation, hasStateChanged, probIndicatorActivation, adjusted_maxProb, adjusted_minProb, activate);
  if (activate)
  {
    _indicatorStateNext = indicatorState;
  }

  return _indicatorStateNext;
}

bool ActionImplementation::DoesMaxProbabilityApply() const
{
  const LateralActionQuery action(_mentalModel.GetLateralAction());

  if ((action.IsIntentActive() && _mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)) || action.IsPreparingToMerge())
  {
    return false;
  }
  return true;
}

void ActionImplementation::ImplementStochasticHighRiskFlasherActivation(double probActivation, units::time::millisecond_t durationCurrentSituation)
{
  const auto timeToStartFlasher{500._ms};
  const auto timeToEndFlasher{1500._ms};

  const auto maxProb{70.};
  const auto minProb{10.};

  _flasherActiveNext = _mentalModel.ImplementStochasticActivation(timeToStartFlasher, timeToEndFlasher, durationCurrentSituation,
                                                                  true,  // state change will be ignored in this logic
                                                                  probActivation,
                                                                  maxProb,
                                                                  minProb,
                                                                  _flasherActiveNext);
}

units::acceleration::meters_per_second_squared_t ActionImplementation::AdjustAccelerationWishDueToCooperation(units::acceleration::meters_per_second_squared_t accelerationWish) const
{
  const bool isCooperative{_mentalModel.GetCooperativeBehavior()};
  if (accelerationWish < 0._mps_sq && isCooperative && _mentalCalculations.EgoBelowJamSpeed())
  {
    const Situation situation{_mentalModel.GetCurrentSituation()};
    const auto leadingVehicle{_mentalModel.GetLeadingVehicle()};

    const bool isLaneChangerDetected{situation == Situation::LANE_CHANGER_FROM_LEFT || situation == Situation::LANE_CHANGER_FROM_RIGHT};
    const bool laneChangerIsAoiRegulate{leadingVehicle != nullptr &&
                                        isLaneChangerDetected &&
                                        _mentalModel.GetCausingVehicleOfSituationSideCluster() == leadingVehicle->GetAssignedAoi()};
    if (laneChangerIsAoiRegulate)
    {
      const auto maximumDeceleration{-_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration};
      const double cooperateAdjustmentFactor{1.2};
      accelerationWish *= cooperateAdjustmentFactor;
      accelerationWish = units::math::max(accelerationWish, maximumDeceleration);
    }
  }

  return accelerationWish;
}

std::tuple<units::time::millisecond_t, units::time::millisecond_t, double, double> ActionImplementation::AdjustIndicatorParametersDueToCooperation(units::time::millisecond_t startTime, units::time::millisecond_t endTime, double minProb, double maxProb) const
{
  const double cooperationFactor{_mentalModel.GetAgentCooperationFactor()};
  if (_mentalModel.GetCooperativeBehavior() &&
      _mentalCalculations.EgoBelowJamSpeed() &&
      cooperationFactor != 0.)
  {
    startTime -= startTime * cooperationFactor;       // More cooperation means sooner activation (0s at max)
    endTime += endTime * cooperationFactor;           // More cooperation means longer patience (double at max)
    minProb += (100. - minProb) * cooperationFactor;  // More cooperation means higher activation probability
    maxProb += (100. - maxProb) * cooperationFactor;  // More cooperation means lesser chance of not activating
  }

  return std::make_tuple(startTime, endTime, minProb, maxProb);
}
