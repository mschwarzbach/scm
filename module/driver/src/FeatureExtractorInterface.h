/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  FeatureExtractorInterface.h

#pragma once

#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

class MentalCalculationsInterface;

//! @brief This class extracts features in the vicinity of the driver.
//! @details This class holds static functionality to extract salient features of the
//! situation that the driver in question is surrounded by. Most methods
//! extract these features from the mental model of the driver.
class FeatureExtractorInterface
{
public:
  virtual ~FeatureExtractorInterface() = default;
  //! @brief Determine, if the vehicle is in the side lane of the ego vehicle.
  //! @param vehicle             SurroundingVehicleInterface*
  //! @return True if vehicle is in a side lane, false otherwise
  virtual bool IsVehicleInSideLane(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if the minimal following distance is violated.
  //! @param vehicle SurroundingVehicleInterface*
  //! @param reductionFactorUrgency double
  //! @return True if distance is violated, false otherwise
  virtual bool IsMinimumFollowingDistanceViolated(const SurroundingVehicleInterface* vehicle, double reductionFactorUrgency = 1.0) const = 0;

  //! @brief Determine, if the influencing following distance is violated.
  //! @param vehicle         SurroundingVehicleInterface*
  //! @return True if vehicle is within the influencing distance, false otherwise
  virtual bool IsInfluencingDistanceViolated(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if on the lane of the aoi the desired speed is not feasible for a sufficient duration.
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if ego cannot drive long enough on the lane of the vehicle without being disturbed, false otherwise
  virtual bool IsUnfavorable(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if the aoi vehicle is slower than the boundary speed for traffic jam.
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if vehicle is slower than the traffic jam velocity threshold, false otherwise
  virtual bool HasJamVelocity(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if  the driver drives slower than the boundary speed for traffic jam.
  //! @return True if ego is slower than the traffic jam velocity threshold, false otherwise
  virtual bool HasJamVelocityEgo() const = 0;

  //! @brief Determine, whether the driver will slow down when detecting a suspicious vehicle in his side lanes
  //! @return True if ego is sufficiently cooperative, false otherwise
  virtual bool IsDeceleratingDueToSuspiciousSideVehicles() const = 0;

  //! @brief Determine, whether the driver will evaluate changing lanes when detecting a suspicious vehicle in his side lanes
  //! @return True if ego is sufficiently cooperative, false otherwise
  virtual bool IsAvoidingLaneNextToSuspiciousSideVehicles() const = 0;

  //! @brief Determine, if the aoi vehicle is near enough for following in case of urgency.
  //! @param vehicle             SurroundingVehicleInterface*
  //! @param reductionFactor     double
  //! @return True if ego is close enough, false otherwise
  virtual bool IsNearEnoughForFollowing(const SurroundingVehicleInterface& vehicle, double reductionFactor = 1.0) const = 0;

  //! @brief Determine, if the aoi vehicle is exceptionally slow (not in a general traffic jam scenario) e.g. due to a vehicle malfunction
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if a vehicle is considered as too slow for a regular driving situation, false otherwise
  virtual bool IsExceptionallySlow(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if there is an exceptionally slow vehicle, an obstacle or an accident on sideLane
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if vehicle is considered as an obstacle, exceptionally slow or collided
  virtual bool HasSuspiciousBehaviour(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if an aoi vehicle intent to cross into ego lane.
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if the vehicle has an indicator set towards egos lane, false otherwise
  virtual bool HasIntentionalLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if an aoi vehicle crosses into ego lane by anticipation.
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if the vehicle is anticipated to change its lane towards the ego lane, false otherwise
  virtual bool HasAnticipatedLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if the time gap or the time to collision (depending on the speed range) is below a minimum treshold
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return True if egos and vehicles course can collide, false otherwise
  virtual bool IsCollisionCourseDetected(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Determine, if it is safe change lane from a normal situation.
  //! @param side              Side
  //! @return True if side lane is considered as safe, false otherwise
  virtual bool IsSideLaneSafe(Side side) const = 0;

  //! @brief Check whether the current lane change is still save to perform.
  //! @param isTransitionTrigger   Flag, whether the vehicle has already transitioned into the new lane
  //! @return True if a lane change is considered as safe, false otherwise
  virtual bool IsLaneChangeSafe(bool isTransitionTrigger) const = 0;

  //! @brief Determine, if ego can pass a vehicle without collision at a secure distance.
  //! @param vehicle              const SurroundingVehicleInterface*
  //! @param deltaVelocity
  //! @return True if ego has enough time to pass a vehicle, false otherwise
  virtual bool AbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const = 0;

  //! @brief Determine, if ego can pass a vehicle by anticipation without collision at a secure distance.
  //! @param vehicle              const SurroundingVehicleInterface*
  //! @param deltaVelocity
  //! @return True if ego anticipates to have enough time to pass a vehicle, false otherwise
  virtual bool AnticipatedAbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const = 0;

  //! @brief Is the current Lane an exit or entry lane (including off-, on-ramp)
  //! @return True if ego is on a lane with type ∈{entry, on-ramp, exit, off-ramp}
  virtual bool IsOnEntryOrExitLane() const = 0;

  //! @brief Is the current Lane an entry lane (including on-ramp)
  //! @return True if ego is on a lane with type ∈{entry, on-ramp}
  virtual bool IsOnEntryLane() const = 0;

  //! @brief Determine, if ego can change into neighboring lane to evade end of ego lane.
  //! @return True if ego can leave its own lane before it ends, false otherwise
  virtual bool IsEvadingEndOfLane() const = 0;

  //! @brief Check, if the end of the relative lane is visible.
  //! @param relativeLane    RelativeLane
  //! @return True if the end of lane is not yet visible, false otherwise
  virtual bool IsLaneDriveablePerceived(RelativeLane relativeLane) const = 0;

  //! @brief Check, if the end of the aoi lane is visible.
  //! @return True if the end of lane is not yet visible, false otherwise
  virtual bool IsLaneDriveablePerceived() const = 0;

  //! @brief Check, if the lane type ist drivable (under normal circumstances).
  //! @param type    Lane type to check
  //! @return True if the lane type is considered as driveable, false otherwise
  virtual bool IsLaneTypeDriveable(scm::LaneType type) const = 0;

  //! @brief Determine, if the lateral motion towards ego lane is recognizable (except side areas)
  //! @param vehicle              const SurroundingVehicleInterface*
  //! @return True if the vehicle is noticeable driving towards the ego lane, false otherwise
  virtual bool IsExceedingLateralMotionThresholdTowardsEgoLane(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Check for traffic rules that might prohibit a lane change due to lane markings.
  //! @param side
  //! @return True if there are no conflicting lane markings for a lane change, false otherwise
  virtual bool IsLaneChangePermittedDueToLaneMarkings(Side side) const = 0;

  //! @brief Check for traffic rules that might prohibit a lane change due to lane markings.
  //! @param vehicle              const SurroundingVehicleInterface*
  //! @return True if there are no conflicting lane markings for a lane change, false otherwise
  virtual bool IsLaneChangePermittedDueToLaneMarkingsForAoi(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Check an aoi for lane change prohibitions and lane safety.
  //! @param side
  //! @param isEmergecy In emergency situations traffic rules (signs, lane markings, ...) will not be considered
  //! @return True if SideLane is safe in case of an emergency, otherwise the lane change must also be permitted due to lane markings. False otherwise
  virtual bool CheckLaneChange(Side side, bool isEmergecy = false) const = 0;

  //! @brief Is the agent supposed to cross a broken bold lane marking.
  //! @param laneMarking    Observed lane marking
  //! @param side           Left or right hand side lane marking observed
  //! @return True if laneMarking should prevent a lane change, false otherwise
  virtual bool DoesLaneMarkingBrokenBoldPreventLaneChange(const scm::LaneMarking::Entity& laneMarking, Side side) const = 0;

  //! @brief Does the right overtaking prohibition apply for the agent in its current state
  //! @param aoiToEvaluate   AreaOfInterest towards the right overtaking prohibition is evaluated (LEFT_FRONT or EGO_FRONT)
  //! @param rightHandTraffic
  //! @return True if is prohibited to overtake on the outer lane, false otherwise
  virtual bool DoesOuterLaneOvertakingProhibitionApply(AreaOfInterest aoiToEvaluate, bool rightHandTraffic) const = 0;

  //! @brief The time that a vehicle could travel at its target speed before theoretically having to slow down.
  //! @param lane
  //! @return The estimated time ego can drive on the target lane
  virtual units::time::second_t GetTimeAtTargetSpeedInLane(SurroundingLane lane) const = 0;

  //! @brief Checks if there is enough time to leave to avoid a collision.
  //! @param vehicle
  //! @return true if possible
  virtual bool WillEgoLeaveLaneBeforeCollision(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Checks if a given side as any collision courses
  //! @param side
  //! @return true if none
  virtual bool HasSideLaneNoCollisionCourses(Side side) const = 0;

  //! @brief Checks if the right lane is free to drive and has a higher convenience than ego lane
  //! @return True if right lane is empty and is more convenient
  virtual bool IsRightLaneEmptyLaneConvenienceAndHigherThanEgo() const = 0;

  //! @brief Todo: Not sure right now what this is meant for
  //! @param aoiToEvaluate
  //! @return
  virtual bool DoesVehicleSurroundingFit(AreaOfInterest aoiToEvaluate) const = 0;

  //! @brief Checks whether the vehicle can cause a collision course with ego based on TTC and tauDot
  //! @param vehicle
  //! @return True if TTC and TauDot are within its critical limits, false otherwise
  virtual bool CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Check if the given vehicle is crossing into the same lane as ego and there could be a potential collision.
  //! @param vehicle
  //! @return True if there is a conflict which cannot be resolved easily by ego, false otherwise
  virtual bool HasActualLaneCrossingConflict(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Checks whether a vehicle is considered as an obstacle
  //! @param vehicle
  //! @return True if the given vehicle is a static object or exceptionally slow.
  virtual bool IsObstacle(const SurroundingVehicleInterface* vehicle) const = 0;

  //! @brief Checks whether a relative lane has a drivable successor lane
  //! @param isEmergency When it is an emergency situation, other lane types are considered as driveable
  //! @param relativeLane
  //! @return True if there is another lane following the relative lane which is considered to be driveable, false otherwise
  virtual bool HasDrivableSuccessor(bool isEmergency, RelativeLane relativeLane) const = 0;
};
