/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "PeriodicLogger.h"
#include "ScmComponentsInterface.h"
#include "ScmSignalCollector.h"
#include "include/signal/driverOutput.h"

/**
 * \brief Base class for stochastic cognitive model (SCM) algorithm. It holds common functionality through all different states of its life time.
 * \details This class implements the stochastic cognitive model (SCM).
 * It models the observations by the driver, evaluates discrete situations and generates actions and acceleration wish.
 * Common 'UpdateOutput(..)' method for all lifetime states.
 */

class ScmLifetimeState
{
public:
  static constexpr char COMPONENTNAME[]{"ScmLifetimeState"};

  ScmLifetimeState(const std::string& componentName,
                   units::time::millisecond_t cycleTime,
                   PeriodicLoggerInterface* pLog,
                   ScmComponentsInterface& scmComponents,
                   ExternalControlState externalControlState);

  virtual ~ScmLifetimeState() = default;
  virtual void Trigger(units::time::millisecond_t time);
  virtual scm::signal::DriverOutput GetOutput() const;

protected:
  units::time::millisecond_t _cycleTime;
  units::time::millisecond_t LANE_CHANGE_PREVENTION_THRESHOLD{3100_ms};
  ScmComponentsInterface* _scmComponents;
  bool _isNewEgoLane{false};
  ExternalControlState _externalControlState;

  virtual void SetInitialParameters(units::time::millisecond_t time);
  virtual void UpdateMicroscopicData();
  virtual void DetermineAgentStates() = 0;
  virtual void UpdateAgentInformation(units::time::millisecond_t time);
  void SendDriverSimulationOutput(scm::publisher::PublisherInterface* simLog);
  void SendScmPerceptionSimulationOutputInformation(scm::publisher::PublisherInterface* simLog);
  void ImplementAgentStates();
  void SetFinalParameters();
  void TriggerSimulationOutputLog();
  void ResetLateralMovement();
  void CheckForEndOfLateralMovement();
  void ResetBaseTimes();
  void ResetMerges();
  void HandleExternalControl();

private:
  std::string _componentName;
  PeriodicLoggerInterface* _pLog;
  scm::LightState::Indicator out_indicatorState = scm::LightState::Indicator::Off;
  units::acceleration::meters_per_second_squared_t out_longitudinal_acc{0.0_mps_sq};
  units::angular_acceleration::radians_per_second_squared_t out_gainLateralDeviation{20.0_rad_per_s_sq};
  units::length::meter_t out_lateralDeviation{0.0_m};
  units::frequency::hertz_t out_gainHeadingError{7.5_Hz};
  units::angle::radian_t out_headingError{0.0_rad};
  units::curvature::inverse_meter_t out_kappaManoeuvre{0.0_i_m};
  units::curvature::inverse_meter_t out_kappaRoad{0.0_i_m};
  units::length::meter_t out_laneWidth{3.75_m};
  bool out_hornSwitch = false;
  bool out_headLight = false;
  bool out_highBeamLight = false;
  bool out_flasher = false;
  bool out_triggerNewRouteRequest{false};
  std::vector<AdasHmiSignal> _opticAdasSignals;
  std::vector<AdasHmiSignal> _acousticAdasSignals;

  void LaneChangePrevention();
  void UpdateInfrastructureData();
  void UpdateMesoscopicData();
  void UpdateStimulusData();

  void LogSurroundingVehicles(scm::publisher::PublisherInterface* simLog, std::vector<ObjectInformationScmExtended> vehicles, const std::string& direction);
  void LogSurroundingVehicles(scm::publisher::PublisherInterface* simLog, ObjectInformationScmExtended vehicle, const std::string& direction);
  void LogVehicles(scm::publisher::PublisherInterface* simLog, const std::vector<const SurroundingVehicleInterface*>& vehicles);
};

/**
 * \brief CollidedState represents a state where SCM don't really have to do anything as there is (currently) no post-crash behaviour intended.
 *  Therefore, most SCM functionaly is disabled except 'UpdateOutput(..)' as the simulation framework expects valid values for every simulation object in each time step.
 */
class CollidedState : public ScmLifetimeState
{
public:
  static constexpr char COMPONENTNAME_DERIVED[]{"CollidedState"};

  CollidedState(std::string componentName,
                units::time::millisecond_t cycleTime,
                PeriodicLoggerInterface* pLog,
                ScmComponentsInterface& scmComponents,
                ExternalControlState externalControlState)
      : ScmLifetimeState(componentName, cycleTime, pLog, scmComponents, externalControlState)
  {
  }

  void DetermineAgentStates() override {}
  void Trigger(units::time::millisecond_t) override {}
};
