/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TrajectoryPlanner/TrajectoryPlanner.h"

#include "TrajectoryCalculations/TrajectoryCalculations.h"
#include "TrajectoryPlanner/TrajectoryPlanningQueryInterface.h"
#include "include/common/ScmDefinitions.h"

TrajectoryPlanner::TrajectoryPlanner(std::shared_ptr<TrajectoryPlanningQueryInterface> planningQuery,
                                     std::shared_ptr<LaneChangeTrajectoryCalculationsInterface> laneChangeTrajectoryCalculations,
                                     const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory)
    : _planningQuery{planningQuery},
      _laneChangeTrajectoryCalc{laneChangeTrajectoryCalculations},
      _surroundingVehicleQueryFactory{surroundingVehicleQueryFactory}
{
}

TrajectoryPlanning::Trajectory TrajectoryPlanner::PlanTrajectory(const TrajectoryPlanning::TrajectoryDimensions& dimensions) const
{
  const auto driverParametersInput{_planningQuery->CreateDriverParameters()};
  const auto vehicleParametersInput{_planningQuery->CreateVehicleParameters()};
  return TrajectoryCalculations::CalculateTrajectory(dimensions, driverParametersInput, vehicleParametersInput);
}

TrajectoryPlanning::TrajectoryDimensions TrajectoryPlanner::CalculateLaneChangeDimensions(RelativeLane targetLane) const
{
  if (!(targetLane == RelativeLane::RIGHT || targetLane == RelativeLane::LEFT))
  {
    throw std::out_of_range("TrajectoryPlanner::CalculateLaneChangeDimensions called with invalid target lane! Double lane changes are not possible yet");
  }
  const auto laneChangeWidth{DetermineLaneChangeWidth(targetLane)};
  const auto laneChangeLength{DetermineLaneChangeLength(targetLane)};
  return {
      laneChangeWidth,
      laneChangeLength,
      _planningQuery->GetHeading()};
}

units::length::meter_t TrajectoryPlanner::DetermineLaneChangeWidth(RelativeLane targetLane) const
{
  const auto laneChangeWidthParameters{_planningQuery->CreateLaneChangeWidthInput(targetLane)};
  return _laneChangeTrajectoryCalc->DetermineLaneChangeWidth(laneChangeWidthParameters);
}

units::length::meter_t TrajectoryPlanner::DetermineLaneChangeLength(RelativeLane targetLane) const
{
  const auto relevantVehicles{_planningQuery->GetRelevantVehiclesForLaneChangeLength()};
  const auto situation{_planningQuery->GetSituation()};
  const auto* laneChangeObstacle = _laneChangeTrajectoryCalc->DetermineReferenceObjectForLaneChange(situation, relevantVehicles);

  const TrajectoryPlanning::LaneChangeLengthParameters laneChangeLengthParameters = _planningQuery->CreateLaneChangeLengthInput(targetLane, laneChangeObstacle);
  return _laneChangeTrajectoryCalc->DetermineLaneChangeLength(laneChangeLengthParameters, _surroundingVehicleQueryFactory);
}