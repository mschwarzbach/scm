/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrajectoryPlanningQuery.h

#pragma once

#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "TrajectoryPlanner/TrajectoryPlanningQueryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Implements the TrajectoryPlanningQueryInterface.
class TrajectoryPlanningQuery : public TrajectoryPlanningQueryInterface
{
public:
  //! @brief Constructor that sets mentalModel member.
  //! @param mentalModel
  TrajectoryPlanningQuery(const MentalModelInterface& mentalModel);

  TrajectoryPlanning::LaneChangeWidthParameters CreateLaneChangeWidthInput(RelativeLane targetLane) const override;

  TrajectoryCalculations::DriverParameters CreateDriverParameters() const override;

  TrajectoryCalculations::VehicleParameters CreateVehicleParameters() const override;

  TrajectoryPlanning::LaneChangeLengthParameters CreateLaneChangeLengthInput(RelativeLane targetLane, const SurroundingVehicleInterface* laneChangeObstacle) const override;

  TrajectoryPlanning::RelevantVehiclesForLaneChangeLength GetRelevantVehiclesForLaneChangeLength() const override;

  units::angle::radian_t GetHeading() const override;

  Situation GetSituation() const override;

protected:
  //! @brief Returns relevent information about the next highway exit if ego is set to take it, nullopt otherwise.
  //! @param targetLane Lane to which the lane change should be planned
  //! @return Information about the current highway exit or nullopt if either the lane change is away from the exit or no exit should be taken.
  std::optional<TrajectoryPlanning::HighwayExitInformation> GetHighwayExitInformation(RelativeLane targetLane) const;

private:
  //! @brief Interface to the mental model.
  const MentalModelInterface& _mentalModel;
};