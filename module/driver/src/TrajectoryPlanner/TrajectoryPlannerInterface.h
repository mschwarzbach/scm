/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrajectoryPlannerInterface.h
#pragma once

#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Interface for class that implements methods to calculate lane change dimensions and lane change trajectory.
class TrajectoryPlannerInterface
{
public:
  virtual ~TrajectoryPlannerInterface() = default;

  //! @brief Plans a s-curve trajectory with the given dimensions and the drivers parameters.
  //! @param dimensions Desired lane change dimensions
  //! @return The planned trajectory as a vector of steering points
  virtual TrajectoryPlanning::Trajectory PlanTrajectory(const TrajectoryPlanning::TrajectoryDimensions& dimensions) const = 0;

  //! @brief Calculates the desired dimensions of a lane change to the target lane.
  //! @details Can only handle RelativeLane::LEFT and RelativeLane::RIGHT at the moment.
  //! @throws std::out_of_range if given an invalid target lane
  //! @param targetLane Lane to which the lane change should be planned
  //! @return Planned dimensions of the lane change
  virtual TrajectoryPlanning::TrajectoryDimensions CalculateLaneChangeDimensions(RelativeLane targetLane) const = 0;
};