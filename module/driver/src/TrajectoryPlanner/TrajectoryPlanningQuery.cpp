/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TrajectoryPlanner/TrajectoryPlanningQuery.h"

#include <optional>

#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/ScmDefinitions.h"

TrajectoryPlanningQuery::TrajectoryPlanningQuery(const MentalModelInterface& mentalModel)
    : _mentalModel{mentalModel}
{
}

TrajectoryPlanning::LaneChangeWidthParameters TrajectoryPlanningQuery::CreateLaneChangeWidthInput(RelativeLane targetLane) const
{
  const auto* infrastructure = _mentalModel.GetInfrastructureCharacteristics();
  const auto egoLaneWidth{infrastructure->GetLaneInformationGeometry(RelativeLane::EGO).width};
  const auto targetLaneWidth{infrastructure->GetLaneInformationGeometry(targetLane).width};
  const auto egoVehicleWidth{_mentalModel.GetVehicleModelParameters().bounding_box.dimension.width};
  const bool egoFitsInTargetLane{targetLaneWidth >= egoVehicleWidth};

  return {
      egoLaneWidth,
      egoFitsInTargetLane ? targetLaneWidth : egoLaneWidth,  // handle edge case of non constant width lanes
      _mentalModel.GetLateralPositionFrontAxle(),
      0.0_m,
      targetLane};
}

TrajectoryCalculations::DriverParameters TrajectoryPlanningQuery::CreateDriverParameters() const
{
  const auto& driverParameters{_mentalModel.GetDriverParameters()};

  return {
      driverParameters.comfortAngularVelocitySteeringWheel,
      driverParameters.maximumAngularVelocitySteeringWheel,
      driverParameters.comfortLateralAcceleration,
      driverParameters.maximumLateralAcceleration};
}

TrajectoryCalculations::VehicleParameters TrajectoryPlanningQuery::CreateVehicleParameters() const
{
  const auto modelParameters = _mentalModel.GetVehicleModelParameters();
  const auto wheelBase = modelParameters.front_axle.bb_center_to_axle_center.x - modelParameters.rear_axle.bb_center_to_axle_center.x;

  return {
      std::stod(modelParameters.properties.at("SteeringRatio")),
      units::make_unit<units::length::meter_t>(wheelBase),
      _mentalModel.GetAbsoluteVelocityEgo(false),
      _mentalModel.GetLateralPositionFrontAxle()};
}

TrajectoryPlanning::LaneChangeLengthParameters TrajectoryPlanningQuery::CreateLaneChangeLengthInput(RelativeLane targetLane, const SurroundingVehicleInterface* laneChangeObstacle) const
{
  const auto& driverParameters{_mentalModel.GetDriverParameters()};
  const auto exitInformation{GetHighwayExitInformation(targetLane)};
  return {
      driverParameters.timeHeadwayFreeLaneChange,
      _mentalModel.GetLongitudinalVelocityEgo(),
      _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::EGO).distanceToEndOfLane,
      laneChangeObstacle,
      exitInformation};
}

TrajectoryPlanning::RelevantVehiclesForLaneChangeLength TrajectoryPlanningQuery::GetRelevantVehiclesForLaneChangeLength() const
{
  return {
      _mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT),
      _mentalModel.GetVehicle(_mentalModel.GetCausingVehicleOfSituationFrontCluster()),
      _mentalModel.GetVehicle(_mentalModel.GetCausingVehicleOfSituationSideCluster())};
}

units::angle::radian_t TrajectoryPlanningQuery::GetHeading() const
{
  return _mentalModel.GetHeading();
}

Situation TrajectoryPlanningQuery::GetSituation() const
{
  return _mentalModel.GetCurrentSituation();
}

bool ChangingAwayFromExit(bool rightHandTraffic, RelativeLane targetLane)
{
  return rightHandTraffic ? targetLane == RelativeLane::LEFT : targetLane == RelativeLane::RIGHT;
}

std::optional<TrajectoryPlanning::HighwayExitInformation> TrajectoryPlanningQuery::GetHighwayExitInformation(RelativeLane targetLane) const
{
  if (!_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT) ||
      ChangingAwayFromExit(_mentalModel.IsRightHandTraffic(), targetLane))
  {
    return std::nullopt;
  }

  const int numberOfLaneChanges{_mentalModel.GetClosestRelativeLaneIdForJunctionIngoing()};
  if (numberOfLaneChanges == 0)
  {
    return std::nullopt;
  }

  auto distanceToEndOfExit{_mentalModel.GetDistanceToEndOfNextExit()};
  if (distanceToEndOfExit == units::length::meter_t(ScmDefinitions::DEFAULT_VALUE))
  {
    // approximate distanceToEndOfExit
    static constexpr auto APPROXIMATE_EXIT_LENGTH{250.0_m};
    distanceToEndOfExit = _mentalModel.GetDistanceToStartOfNextExit() + APPROXIMATE_EXIT_LENGTH;
  }

  return TrajectoryPlanning::HighwayExitInformation{
      distanceToEndOfExit,
      std::abs(numberOfLaneChanges)};
}