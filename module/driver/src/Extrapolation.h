/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  Extrapolation.h

#pragma once

#include "ExtrapolationInterface.h"
#include "include/common/OsiQlData.h"
class MentalModelInterface;

class Extrapolation : public ExtrapolationInterface
{
public:
  Extrapolation(MentalModelInterface& mentalModel, OwnVehicleRoutePose& ownVehicleRoutePose);

  void Extrapolate(ObjectInformationScmExtended* objectInformation, AreaOfInterest aoi) const override;

protected:
  //! @brief Extrapolate the lateral velocity and position in lane of a surrounding object.
  //! @param surroundingObject Object to extrapolate
  //! @param aoi               AreaOfInterest in which the object was perceived
  void ExtrapolateSurroundingVehicleDataLateral(ObjectInformationScmExtended* surroundingObject, AreaOfInterest aoi) const;

  //! @brief Extrapolate the longitudinal acceleration of a surrounding object based on the perceived acceleration change,
  //! limited by comfort and maximum possible acceleration of ego.
  //! @param surroundingObject    Currently observed surrounding object
  void ExtrapolateLongitudinalAcceleration(ObjectInformationScmExtended* surroundingObject) const;

  //! @brief Extrapolate longitudinal velocity, gap to ego, TTC and tau dot of a surrounding object.
  //! @param surroundingObject Object to extrapolate
  //! @param aoi               AreaOfInterest in which the object was perceived
  void ExtrapolateSurroundingVehicleDataLongitudinal(ObjectInformationScmExtended* surroundingObject, AreaOfInterest aoi) const;

  //! @brief projects surrounding vehicle onto the route of the ego vehicle
  //! @param surroundingObject
  void ProjectSurroundingVehicle(ObjectInformationScmExtended* surroundingObject) const;

private:
  //! @brief Extrapolate longitudinal obstruction information for a surrounding object.
  //! @param aoi                  Area of interest
  //! @param surroundingObject    Currently observed surrounding object
  ObstructionLongitudinal ExtrapolateLongitudinalObstruction(AreaOfInterest aoi, const ObstructionLongitudinal& oldObstruction, units::velocity::meters_per_second_t agentVelocity) const;

  //! @brief Interface to the MentalModel
  const MentalModelInterface& _mentalModel;

  //! @brief Struct ownVehicleRoutePose
  const OwnVehicleRoutePose& _ownVehicleRoutePose;

  //! @brief Cycletime of the simulation in seconds
  units::time::second_t _cycleTimeInS{0_s};

  //! @brief Distance below which an agent can recognise the acceleration of an object [m]
  constexpr static units::length::meter_t ACCELERATION_SENSING_DISTANCE{100._m};
};
