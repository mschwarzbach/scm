/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  AlgorithmScmGlobal.h

//-----------------------------------------------------------------------------
/** @brief This file contains DLL export declarations
*
* This class contains all functionality of the module. */
//-----------------------------------------------------------------------------

#pragma once

#include "dllExport/scmExport.h"

#if defined(ALGORITHM_SCM_LIBRARY)
#define ALGORITHM_SCM_SHARED_EXPORT SCMEXPORT
#else
#define ALGORITHM_SCM_SHARED_EXPORT SCMIMPORT
#endif
