/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LongitudinalCalculationsInterface.h

#pragma once
#include "LaneQueryHelper.h"
#include "ScmDefinitions.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"

class MentalModelInterface;
class SurroundingVehicleInterface;

//! *************************************************************************************************************
//! @brief Interface class for secure net distance calculations between two agents (surrounding vehicles or ego).
//! *************************************************************************************************************
class LongitudinalCalculationsInterface
{
public:
  virtual ~LongitudinalCalculationsInterface() = default;
  //! @brief Calculate the secure net distance between two surrounding vehicles.
  //! @param observedVehicle Surrounding vehicle for which the time headway threshold is going to be evaluated
  //! @param referenceVehicle Surrounding vehicle for the reference vehicle
  //! @param anticipAccFront Anticipated acceleration of the leading vehicle in [m/s²]
  //! @param anticipVelEnd Anticipated velocity goal of both vehicles in [m/s]
  //! @param reactionTime Reaction time of the following vehicle in [s]
  //! @param anticipAccRear Anticipated acceleration of the following vehicle in [m/s²]
  //! @param deltaDistEnd Desired relative net-distance after anticipated manoeuvre in [m]
  //! @param perspective Anticipates being the front/rear/current position vehicle in a future perspective
  //! @return Time headway threshold to maintain DeltaDistEnd after the anticipated manoeuvre in [s]
  virtual units::length::meter_t CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                            const SurroundingVehicleInterface& referenceVehicle,
                                                            units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                            units::velocity::meters_per_second_t anticipVelEnd,
                                                            units::time::second_t reactionTime,
                                                            units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                            units::length::meter_t deltaDistEnd,
                                                            units::velocity::meters_per_second_t vGap) const = 0;

  //! @brief Calculate the secure net distance between the ego and a surrounding vehicle.
  //! @param observedVehicle Surrounding vehicle for which the time headway threshold is going to be evaluated
  //! @param egoVehicle Ego vehicle as reference
  //! @param anticipAccFront Anticipated acceleration of the leading vehicle in [m/s²]
  //! @param anticipVelEnd Anticipated velocity goal of both vehicles in [m/s]
  //! @param reactionTime Reaction time of the following vehicle in [s]
  //! @param anticipAccRear Anticipated acceleration of the following vehicle in [m/s²]
  //! @param DeltaDistEnd Desired relative net-distance after anticipated manoeuvre in [m]
  //! @param perspective Anticipates being the front/rear/current position vehicle in a future perspective
  //! @param vGap Velocity of potential merge gap
  //! @return Time headway threshold to maintain DeltaDistEnd after the anticipated manoeuvre in [s]
  virtual units::length::meter_t CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                            const OwnVehicleInformationScmExtended& egoVehicle,
                                                            units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                            units::velocity::meters_per_second_t anticipVelEnd,
                                                            units::time::second_t reactionTime,
                                                            units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                            units::length::meter_t deltaDistEnd,
                                                            MinThwPerspective perspective,
                                                            units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const = 0;

  //! @brief Determine the longitudinal velocity delta [m/s].
  //! @param observedVehicle Observed vehicle to get the data from
  //! @param egoVehicle Ego vehicle as reference
  //! @throws std::runtime_error when one vehicle is in the side of the other (currently not implemented)
  //! @return corresponding velocity delta
  virtual units::velocity::meters_per_second_t CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                                      const OwnVehicleInformationScmExtended& egoVehicle) const = 0;
};