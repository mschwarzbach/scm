/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LongitudinalPartCalculations.h

#pragma once

#include "LongitudinalPartCalculationsInterface.h"

//! @brief Implements the LongitudinalPartCalculationsInterface
class LongitudinalPartCalculations : public LongitudinalPartCalculationsInterface
{
public:
  explicit LongitudinalPartCalculations(const MentalModelInterface& mentalModel);

  SecureDistanceData DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                 const SurroundingVehicleInterface& referenceVehicle,
                                                 units::velocity::meters_per_second_t vGap) override;
  SecureDistanceData DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                 const OwnVehicleInformationScmExtended& egoVehicle,
                                                 MinThwPerspective perspective,
                                                 units::velocity::meters_per_second_t vGap) override;
  units::length::meter_t DistanceToBrakeLeadingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd,
                                                       units::acceleration::meters_per_second_squared_t anticipatedAcceleration,
                                                       units::velocity::meters_per_second_t velocity) override;
  units::length::meter_t DistanceToBrakeFollowingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd,
                                                         units::acceleration::meters_per_second_squared_t anticipatedAcceleration,
                                                         units::acceleration::meters_per_second_squared_t currentAccRear,
                                                         units::velocity::meters_per_second_t velocity,
                                                         units::time::second_t reactionTime) override;
  units::length::meter_t CalculateInsecurityDistance(const SurroundingVehicleInterface& observedVehicle,
                                                     const SurroundingVehicleInterface& referenceVehicle) override;
  units::length::meter_t CalculateInsecurityDistance(const SurroundingVehicleInterface& vehicle) override;
  double DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& observedVehicle,
                                                    const SurroundingVehicleInterface& referenceVehicle) override;
  double DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& vehicle) override;
  double DetermineTtcBasedScalingFactorForFollowingDistance(units::time::second_t ttc) const override;
  units::length::meter_t ExecuteStevensPowLaw(units::length::meter_t deltaDistance,
                                              double scalingFactorFollowingDistance,
                                              double proportionalityFactorForFollowingDistance) override;
  units::velocity::meters_per_second_t CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                              const OwnVehicleInformationScmExtended& egoVehicle) const override;

protected:
  //! \brief Determine the longitudinal net distance between objects [m].
  //! \param [in] observedVehicle       Observed vehicle to get the data from
  //! \param [in] referenceVehicle      Reference vehicle as
  //! \returns net distance between observed and reference vehicle
  units::length::meter_t CalculateNetDistanceBetweenObjects(const SurroundingVehicleInterface& observedVehicle,
                                                            const SurroundingVehicleInterface& referenceVehicle);

  //! \brief Determine the longitudinal velocity delta [m/s].
  //! \param [in] observedVehicle       Observed vehicle to get the data from
  //! \param [in] referenceVehicle      Reference vehicle as
  //! \throws std::runtime_error      when one vehicle is in the side of the other (currently not implemented)
  //! \return corresponding velocity delta
  units::velocity::meters_per_second_t CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                              const SurroundingVehicleInterface& referenceVehicle) const;

private:
  const MentalModelInterface& _mentalModel;
};