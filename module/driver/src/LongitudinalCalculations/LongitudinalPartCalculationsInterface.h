/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LongitudinalPartCalculationsInterface.h

#pragma once

#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"

//! @brief Collection of few data points used for secure distance calculations

struct SecureDistanceData
{
  units::velocity::meters_per_second_t velocityFront{0.0_mps};
  units::velocity::meters_per_second_t velocityRear{0.0_mps};
  units::acceleration::meters_per_second_squared_t accelerationRear{0.0_mps_sq};
};

//! ************************************************************************************************************************************************************
//! @brief This interface class is meant to provide data for the actual longitudinal calculations class (calculate a secure net distance between two agents).
//! @details To do so it provides methods to determine used velocities based on the perspective, distances when to brake, scaling factor for following and more.
//! ************************************************************************************************************************************************************
class LongitudinalPartCalculationsInterface
{
public:
  virtual ~LongitudinalPartCalculationsInterface() = default;
  //! @brief Determines needed velocitiy and acceleration data for the following calculations based on the MinThwPerspective and a reasonable vGap
  //! @param observedVehicle Observed vehicle to get the data from
  //! @param egoVehicle Ego vehicle as reference
  //! @param perspective Anticipates being the front/rear/current position vehicle in a future perspective
  //! @param vGap Velocity of potential merge gap
  //! @return SecureDistanceData
  virtual SecureDistanceData DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                         const OwnVehicleInformationScmExtended& egoVehicle,
                                                         MinThwPerspective perspective,
                                                         units::velocity::meters_per_second_t vGap) = 0;

  //! @brief Determines needed velocitiy and acceleration data for the following calculations based for two surrounding vehicles and their order
  //! @param observedVehicle Observed vehicle to get the data from
  //! @param referenceVehicle Reference vehicle as
  //! @param vGap Velocity of potential merge gap
  //! @return SecureDistanceData
  virtual SecureDistanceData DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                         const SurroundingVehicleInterface& referenceVehicle,
                                                         units::velocity::meters_per_second_t vGap) = 0;

  //! @brief Calculate Breakingdistance to Agent in Front
  //! @param anticipatedVelocityEnd
  //! @param anticipatedAcceleration
  //! @param velocity
  //! @return leading vehicle - distance to break
  virtual units::length::meter_t DistanceToBrakeLeadingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::velocity::meters_per_second_t velocity) = 0;

  //! @brief Calculate Breakingdistance to FollowingVehicle
  //! @param anticipatedVelocityEnd
  //! @param anticipatedAcceleration
  //! @param currentAccRear
  //! @param velocity
  //! @param reactionTime
  //! @return following vehicle - distance to break
  virtual units::length::meter_t DistanceToBrakeFollowingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::acceleration::meters_per_second_squared_t currentAccRear, units::velocity::meters_per_second_t velocity, units::time::second_t reactionTime) = 0;

  //! @brief Calculate insecurity distance between two surrounding vehicles (basically a small amount of an extra safety buffer)
  //! @param observedVehicle the observed vehicle
  //! @param referenceVehicle the reference vehicle
  //! @return insecurity distance
  virtual units::length::meter_t CalculateInsecurityDistance(const SurroundingVehicleInterface& observedVehicle,
                                                             const SurroundingVehicleInterface& referenceVehicle) = 0;

  //! @brief Calculate insecurity distance between ego and another vehicle
  //! @param vehicle the other vehicle
  //! @return insecurity distance
  virtual units::length::meter_t CalculateInsecurityDistance(const SurroundingVehicleInterface& vehicle) = 0;

  //! @brief Determine scaling factor for following distance between two surrounding vehicles
  //! @param observedVehicle the observed vehicle
  //! @param referenceVehicle the reference vehicle
  //! @return Scaling factor
  virtual double DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& observedVehicle,
                                                            const SurroundingVehicleInterface& referenceVehicle) = 0;

  //! @brief Determine Scaling Factor for Following Distance with vehicle
  //! @param vehicle
  //! @return Scaling Factor
  virtual double DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& vehicle) = 0;

  //! @brief Determines a scaling factor for the following calculations based on a given ttc within some specific ttc boundaries.
  //! @param ttc Currently estimated time to collision
  //! @returns scalingFactor
  virtual double DetermineTtcBasedScalingFactorForFollowingDistance(units::time::second_t ttc) const = 0;

  //! @brief Relationship between human sensitivity and stimulus strength.
  //! @param deltaDistance
  //! @param scalingFactorFollowingDistance
  //! @param proportionalityFactorForFollowingDistance
  //! @return misjudged secure distance
  virtual units::length::meter_t ExecuteStevensPowLaw(units::length::meter_t deltaDistance, double scalingFactorFollowingDistance, double proportionalityFactorForFollowingDistance) = 0;

  //! @brief Determine the longitudinal velocity delta [m/s].
  //! @param observedVehicle Observed vehicle to get the data from
  //! @param egoVehicle Ego vehicle as reference
  //! @throws std::runtime_error when one vehicle is in the side of the other (currently not implemented)
  //! @return corresponding velocity delta
  virtual units::velocity::meters_per_second_t CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                                      const OwnVehicleInformationScmExtended& egoVehicle) const = 0;
};