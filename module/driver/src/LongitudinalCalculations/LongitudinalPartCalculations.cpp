/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "LongitudinalPartCalculations.h"

LongitudinalPartCalculations::LongitudinalPartCalculations(const MentalModelInterface& mentalModel)
    : _mentalModel(mentalModel)
{
}

units::length::meter_t LongitudinalPartCalculations::DistanceToBrakeLeadingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::velocity::meters_per_second_t velocity)
{
  auto timeFront = (anticipatedVelocityEnd - velocity) / anticipatedAcceleration;
  auto distFront = velocity * timeFront + anticipatedAcceleration / 2. * timeFront * timeFront;
  return distFront;
}

units::length::meter_t LongitudinalPartCalculations::DistanceToBrakeFollowingVehicle(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::acceleration::meters_per_second_squared_t currentAccRear, units::velocity::meters_per_second_t velocity, units::time::second_t reactionTime)
{
  // time span to react as part 1
  auto timeRear_Part1 = reactionTime;
  auto velRear_Part1 = velocity + reactionTime * currentAccRear;
  auto distRear_Part1 = velocity * timeRear_Part1 + currentAccRear / 2. * timeRear_Part1 * timeRear_Part1;

  // time span from reaction to end velocity as part 2
  auto timeRear_Part2 = (anticipatedVelocityEnd - velRear_Part1) / anticipatedAcceleration;
  auto distRear_Part2 = velRear_Part1 * timeRear_Part2 + anticipatedAcceleration / 2. * timeRear_Part2 * timeRear_Part2;
  return distRear_Part1 + distRear_Part2;
}

units::length::meter_t LongitudinalPartCalculations::CalculateInsecurityDistance(const SurroundingVehicleInterface& observedVehicle,
                                                                                 const SurroundingVehicleInterface& referenceVehicle)
{
  auto urgentUpdateTime = _mentalModel.GetUrgentUpdateThreshold();
  auto timeSinceLastUpdateReferenceVehicle = _mentalModel.GetTime() - referenceVehicle.GetTimeOfLastPerception();
  auto timeSinceLastUpdateObservedVehicle = _mentalModel.GetTime() - observedVehicle.GetTimeOfLastPerception();

  const double insecurityFactorOfReference = units::math::min(urgentUpdateTime, timeSinceLastUpdateReferenceVehicle) / urgentUpdateTime;
  const double insecurityFactorOfObserved = units::math::min(urgentUpdateTime, timeSinceLastUpdateObservedVehicle) / urgentUpdateTime;
  double insecurityFactor = std::max(insecurityFactorOfReference, insecurityFactorOfObserved);
  constexpr auto MAX_DISTANCE_FOR_INSECURITY{10.0_m};
  auto insecurityDistance = insecurityFactor * MAX_DISTANCE_FOR_INSECURITY;
  return insecurityDistance;
}

double LongitudinalPartCalculations::DetermineTtcBasedScalingFactorForFollowingDistance(units::time::second_t ttc) const
{
  auto limitedTtc = ttc < 0._s ? ScmDefinitions::TTC_LIMIT : ttc;
  const auto lowerLimitTtc = 6._s;
  const auto upperLimitTtc = 20._s;

  limitedTtc = std::clamp(limitedTtc, lowerLimitTtc, upperLimitTtc);
  const double scalingFactor = (limitedTtc - lowerLimitTtc) / (upperLimitTtc - lowerLimitTtc);
  return scalingFactor;
}

double LongitudinalPartCalculations::DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& observedVehicle,
                                                                                const SurroundingVehicleInterface& referenceVehicle)
{
  const auto relativeDistance{CalculateNetDistanceBetweenObjects(observedVehicle, referenceVehicle)};
  const auto velocityDelta{CalculateVelocityDelta(observedVehicle, referenceVehicle)};
  const auto ttc = relativeDistance / velocityDelta;

  return DetermineTtcBasedScalingFactorForFollowingDistance(ttc);
}

double LongitudinalPartCalculations::DetermineScalingFactorForFollowingDistance(const SurroundingVehicleInterface& vehicle)
{
  auto curTtc = vehicle.GetTtc().GetValue();
  auto limitedTtc = curTtc < 0._s ? ScmDefinitions::TTC_LIMIT : curTtc;
  auto lowerLimitTtc = 6._s;
  auto upperLimitTtc = 20._s;

  limitedTtc = std::clamp(limitedTtc, lowerLimitTtc, upperLimitTtc);
  double scalingFactor = (limitedTtc - lowerLimitTtc) / (upperLimitTtc - lowerLimitTtc);
  return scalingFactor;
}

units::length::meter_t LongitudinalPartCalculations::ExecuteStevensPowLaw(units::length::meter_t deltaDistance, double scalingFactorFollowingDistance, double proportionalityFactorForFollowingDistance)
{
  static constexpr auto exponentForStevensPowLaw{0.25};
  const double factorK = 1.0 + (proportionalityFactorForFollowingDistance - 1.) * scalingFactorFollowingDistance * scalingFactorFollowingDistance;
  const double powExponent = 1.0 - (1.0 - exponentForStevensPowLaw) * scalingFactorFollowingDistance;

  auto misjudgedSecureDistance = units::make_unit<units::length::meter_t>(factorK * std::pow(deltaDistance.value(), powExponent));  // using Steven's power law to misjudge secure distance
  const auto lowerBoundary = units::make_unit<units::length::meter_t>(proportionalityFactorForFollowingDistance * std::pow(deltaDistance.value(), exponentForStevensPowLaw));
  misjudgedSecureDistance = units::math::max(misjudgedSecureDistance, lowerBoundary);

  return units::math::min(misjudgedSecureDistance, deltaDistance);
}

units::length::meter_t LongitudinalPartCalculations::CalculateNetDistanceBetweenObjects(const SurroundingVehicleInterface& observedVehicle,
                                                                                        const SurroundingVehicleInterface& referenceVehicle)
{
  const ObstructionLongitudinal obstructionLongitudinalReference{referenceVehicle.GetLongitudinalObstruction()};
  const ObstructionLongitudinal obstructionLongitudinalObserved{observedVehicle.GetLongitudinalObstruction()};
  const auto distanceToFrontOfReference{obstructionLongitudinalReference.mainLaneLocator};
  const auto lengthOfReference{referenceVehicle.GetLength().GetValue()};
  const auto distanceToFrontOfObserved{obstructionLongitudinalObserved.mainLaneLocator};
  const auto lengthOfObserved{observedVehicle.GetLength().GetValue()};

  units::length::meter_t netDistance;
  if (_mentalModel.IsObjectInRearArea(referenceVehicle.GetAssignedAoi(), observedVehicle.GetAssignedAoi()))
  {
    netDistance = distanceToFrontOfReference - distanceToFrontOfObserved - lengthOfReference;
  }
  else
  {
    netDistance = distanceToFrontOfObserved - distanceToFrontOfReference - lengthOfObserved;
  }
  return units::math::fabs(netDistance);
}

units::velocity::meters_per_second_t LongitudinalPartCalculations::CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                                                          const SurroundingVehicleInterface& referenceVehicle) const
{
  const auto velocityReference{referenceVehicle.GetLongitudinalVelocity().GetValue()};
  const auto velocityObserved{observedVehicle.GetLongitudinalVelocity().GetValue()};

  if (_mentalModel.IsObjectInRearArea(referenceVehicle.GetAssignedAoi(), observedVehicle.GetAssignedAoi()))
  {
    return velocityReference - velocityObserved;
  }
  else if (_mentalModel.IsObjectInSideArea(referenceVehicle.GetAssignedAoi(), observedVehicle.GetAssignedAoi()))
  {
    throw std::runtime_error("LongitudinalCalculations – CalculateVelocityDelta: Functionality for side aoi not included yet. Expand when required!");
  }
  else  // Front area
  {
    return velocityObserved - velocityReference;
  }
}

units::velocity::meters_per_second_t LongitudinalPartCalculations::CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                                                          const OwnVehicleInformationScmExtended& egoVehicle) const
{
  const auto velocityReference{egoVehicle.longitudinalVelocity};
  const auto velocityObserved{observedVehicle.GetLongitudinalVelocity()};

  if (observedVehicle.BehindEgo())
  {
    return velocityReference - velocityObserved.GetValue();
  }
  else if (observedVehicle.ToTheSideOfEgo())
  {
    throw std::runtime_error("LongitudinalCalculations – CalculateVelocityDelta: Functionality for side aoi not included yet. Expand when required!");
  }
  else  // Front area
  {
    return velocityObserved.GetValue() - velocityReference;
  }
}

SecureDistanceData LongitudinalPartCalculations::DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                                             const SurroundingVehicleInterface& referenceVehicle,
                                                                             units::velocity::meters_per_second_t vGap)
{
  const bool isRearArea = _mentalModel.IsObjectInRearArea(referenceVehicle.GetAssignedAoi(), observedVehicle.GetAssignedAoi());
  SecureDistanceData sdd;
  sdd.velocityFront = isRearArea ? referenceVehicle.GetLongitudinalVelocity().GetValue() : observedVehicle.GetLongitudinalVelocity().GetValue();
  sdd.velocityRear = isRearArea ? observedVehicle.GetLongitudinalVelocity().GetValue() : referenceVehicle.GetLongitudinalVelocity().GetValue();
  sdd.accelerationRear = isRearArea ? observedVehicle.GetLongitudinalAcceleration().GetValue() : referenceVehicle.GetLongitudinalAcceleration().GetValue();

  if (vGap != units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE))
  {
    if (!isRearArea)
      sdd.velocityRear = vGap;
    else
      sdd.velocityFront = vGap;
  }

  return sdd;
}

SecureDistanceData LongitudinalPartCalculations::DetermineSecureDistanceData(const SurroundingVehicleInterface& observedVehicle,
                                                                             const OwnVehicleInformationScmExtended& egoVehicle,
                                                                             MinThwPerspective perspective,
                                                                             units::velocity::meters_per_second_t vGap)
{
  const bool notAnticipated = (perspective == MinThwPerspective::NORM);
  // the observed vehicle will be in rear position
  const bool anticipatedEgoInFront = (perspective == MinThwPerspective::EGO_ANTICIPATED_FRONT);
  // the observed vehicle will be in front
  const bool anticipatedEgoInRear = (perspective == MinThwPerspective::EGO_ANTICIPATED_REAR);

  SecureDistanceData sdd;
  sdd.velocityFront = observedVehicle.GetLongitudinalVelocity().GetValue();
  sdd.velocityRear = egoVehicle.longitudinalVelocity;
  sdd.accelerationRear = egoVehicle.acceleration;

  if (vGap != units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE))
  {
    sdd.velocityRear = vGap;
  }
  // looking backwards? Ego is in Front!
  // => Then change perspective of leading vehicle to observed area of interest
  // + for anticipating any vehicle as a rear vehicle in case of flag set to true
  if (!anticipatedEgoInRear)  // NORM or EGO_ANTICIPATED_FRONT
  {
    if (anticipatedEgoInFront ||                                                            // the vehicles that are ahead/besides are handled while anticipating rear perspective
        (LaneQueryHelper::IsRearArea(observedVehicle.GetAssignedAoi()) && notAnticipated))  // the vehicles that are behind cannot be handled as rear vehicles while anticipating front perspective
    {
      sdd.velocityFront = sdd.velocityRear;
      sdd.velocityRear = observedVehicle.GetLongitudinalVelocity().GetValue();
      sdd.accelerationRear = observedVehicle.GetLongitudinalAcceleration().GetValue();
    }
  }

  return sdd;
}

units::length::meter_t LongitudinalPartCalculations::CalculateInsecurityDistance(const SurroundingVehicleInterface& observedVehicle)
{
  auto timeSinceLastUpdateObservedVehicle = _mentalModel.GetTime() - observedVehicle.GetTimeOfLastPerception();
  auto urgentUpdateTime = _mentalModel.GetUrgentUpdateThreshold();

  double insecurityFactor = units::math::min(urgentUpdateTime, timeSinceLastUpdateObservedVehicle) / urgentUpdateTime;  // Can't be a side aoi, so no index is necessary
  constexpr auto MAX_DISTANCE_FOR_INSECURITY{10.0_m};
  auto insecurityDistance = insecurityFactor * MAX_DISTANCE_FOR_INSECURITY;
  return insecurityDistance;
}
