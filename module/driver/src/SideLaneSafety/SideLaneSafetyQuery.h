/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyQuery.h

#pragma once

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h"
#include "MentalModelInterface.h"
#include "SideLaneSafety/SideLaneSafetyQueryInterface.h"
#include "SideLaneSafety/SideLaneSafetyTypes.h"

//! @brief Implements the SideLaneSafetyQueryInterface
class SideLaneSafetyQuery : public SideLaneSafetyQueryInterface
{
public:
  //! @brief Constructor initializes and creates SideLaneSafetyQuery.
  //! @param mentalModel
  //! @param mentalCalculations
  //! @param surroundingVehicleQueryFactory
  SideLaneSafetyQuery(MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory)
      : _mentalModel(mentalModel),
        _mentalCalculations(mentalCalculations),
        _surroundingVehicleQueryFactory(surroundingVehicleQueryFactory)
  {
  }

  SideLaneSafetyTypes::LaneSafetyParameters GetLaneChangeSafetyParameters(units::length::meter_t laneChangeLength, RelativeLane targetLane) const override;

private:
  //! @brief mentalModel
  MentalModelInterface& _mentalModel;
  //! @brief mentalCalculations
  const MentalCalculationsInterface& _mentalCalculations;
  //! @brief surroundingVehicleQueryFactory
  const SurroundingVehicleQueryFactoryInterface& _surroundingVehicleQueryFactory;
};