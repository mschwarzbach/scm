/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyCalculations.h

#pragma once

#include "SideLaneSafety/SideLaneSafetyCalculationsInterface.h"
#include "SideLaneSafety/SideLaneSafetyTypes.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"

//! **************************************************************************************************************
//! @brief Interface for side lane safety calculations, i.e. if an agent has enough space to finish a lane change
//! without violating the minimum distance to other agents.
//! **************************************************************************************************************
class SideLaneSafetyCalculations : public SideLaneSafetyCalculationsInterface
{
public:
  bool CanFinishLaneChangeWithoutEnteringMinDistance(units::time::second_t estimatedLaneChangeDuration, const SideLaneSafetyTypes::TargetLaneVehicleInformation& vehicleInfo, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const override;
};
