/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SideLaneSafetyQuery.h"

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "SideLaneSafety/SideLaneSafetyTypes.h"

SideLaneSafetyTypes::LaneSafetyParameters SideLaneSafetyQuery::GetLaneChangeSafetyParameters(units::length::meter_t laneChangeLength, RelativeLane targetLane) const
{
  if (!(targetLane == RelativeLane::RIGHT || targetLane == RelativeLane::LEFT))
  {
    throw std::out_of_range("CanFinishLaneChangeSafely called with invalid target lane");
  }

  const auto rearAoi{targetLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_REAR : AreaOfInterest::RIGHT_REAR};
  const auto frontAoi{targetLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT};

  const auto* frontVehicle = _mentalModel.GetVehicle(frontAoi);
  const auto* rearVehicle = _mentalModel.GetVehicle(rearAoi);

  const double reductionFactorUrgency = _mentalModel.GetUrgencyReductionFactor();
  const auto minDistanceFront{frontVehicle == nullptr ? 0.0_m
                                                      : _mentalCalculations.GetMinDistance(frontVehicle, MinThwPerspective::EGO_ANTICIPATED_REAR)};

  const auto minDistanceRear{rearVehicle == nullptr ? 0.0_m
                                                    : _mentalCalculations.GetMinDistance(rearVehicle, MinThwPerspective::EGO_ANTICIPATED_FRONT)};
  return {
      .estimatedLaneChangeDuration = laneChangeLength / _mentalModel.GetLongitudinalVelocityEgo(),
      .front = {minDistanceFront * reductionFactorUrgency, frontVehicle},
      .rear = {minDistanceRear * reductionFactorUrgency, rearVehicle},
  };
}