/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafety.h
#pragma once

#include <memory>
#include <optional>

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "SideLaneSafety/SideLaneSafetyCalculations.h"
#include "SideLaneSafety/SideLaneSafetyCalculationsInterface.h"
#include "SideLaneSafety/SideLaneSafetyQuery.h"
#include "SideLaneSafety/SideLaneSafetyQueryInterface.h"
#include "SideLaneSafetyInterface.h"

//! @brief Interface for side lane safety, i.e. if a lane change can be done safely without violation minimum distances.
class SideLaneSafety : public SideLaneSafetyInterface
{
public:
  //! @brief Constructor initializes and creates SideLaneSafety.
  //! @param sideLaneSafetyCalculations
  //! @param sideLaneSafetyQuery
  //! @param surroundingVehicleQueryFactory
  SideLaneSafety(std::shared_ptr<SideLaneSafetyCalculationsInterface> sideLaneSafetyCalculations, std::shared_ptr<SideLaneSafetyQueryInterface> sideLaneSafetyQuery, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory)
      : _sideLaneSafetyCalculations(sideLaneSafetyCalculations),
        _sideLaneSafetyQuery(sideLaneSafetyQuery),
        _surroundingVehicleQueryFactory(surroundingVehicleQueryFactory)
  {
  }

  bool CanFinishLaneChangeSafely(units::length::meter_t laneChangeLength, RelativeLane targetLane) const override;

private:
  //! @brief sideLaneSafetyCalculations
  std::shared_ptr<SideLaneSafetyCalculationsInterface> _sideLaneSafetyCalculations;
  //! @brief sideLaneSafetyQuery
  std::shared_ptr<SideLaneSafetyQueryInterface> _sideLaneSafetyQuery;
  //! @brief surroundingVehicleQueryFactory
  const SurroundingVehicleQueryFactoryInterface& _surroundingVehicleQueryFactory;
};