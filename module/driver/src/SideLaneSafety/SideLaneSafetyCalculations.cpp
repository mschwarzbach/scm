/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SideLaneSafety/SideLaneSafetyCalculations.h"

#include "SideLaneSafety/SideLaneSafetyTypes.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"

bool SideLaneSafetyCalculations::CanFinishLaneChangeWithoutEnteringMinDistance(units::time::second_t estimatedLaneChangeDuration, const SideLaneSafetyTypes::TargetLaneVehicleInformation& vehicleInfo, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const
{
  if (vehicleInfo.vehicle == nullptr)
  {
    return true;
  }

  const auto* opponent{vehicleInfo.vehicle};
  auto aOpponent{0.0_mps_sq};
  auto deltaV{0.0_mps};
  if (opponent->BehindEgo())
  {
    aOpponent = -opponent->GetLongitudinalAcceleration().GetValue();
    deltaV = surroundingVehicleQueryFactory.GetQuery(*opponent)->GetLongitudinalVelocityDelta();
  }
  else
  {
    aOpponent = opponent->GetLongitudinalAcceleration().GetValue();
    deltaV = -surroundingVehicleQueryFactory.GetQuery(*opponent)->GetLongitudinalVelocityDelta();  // Negate sign, because distance gets smaller if ego is faster
  }

  const auto ds{(aOpponent / 2.0) * units::math::pow<2>(estimatedLaneChangeDuration) + deltaV * estimatedLaneChangeDuration};
  const auto currentDistance{opponent->GetRelativeNetDistance().GetValue()};

  const auto estimatedEndDistance{currentDistance + ds};
  return estimatedEndDistance >= vehicleInfo.minDistance;
}
