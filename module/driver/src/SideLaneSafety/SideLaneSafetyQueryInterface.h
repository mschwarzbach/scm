/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyQueryInterface.h

#pragma once

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "SideLaneSafety/SideLaneSafetyTypes.h"

//! ********************************************************************
//! @brief Interface to query safety related parameters for a side lane.
//! ********************************************************************
class SideLaneSafetyQueryInterface
{
public:
  virtual ~SideLaneSafetyQueryInterface() = default;

  //! @brief Returns parameterized struct to determine lane change safety, filled with data from MentalModel and calculated parameters from MentalCalculations.
  //! @param laneChangeLength Desired lane change length
  //! @param targetLane Target lane of the lane change
  //! @return Parameters used for lane change safety calculations
  virtual SideLaneSafetyTypes::LaneSafetyParameters GetLaneChangeSafetyParameters(units::length::meter_t laneChangeLength, RelativeLane targetLane) const = 0;
};