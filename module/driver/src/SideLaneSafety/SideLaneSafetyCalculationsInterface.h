/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyCalculationsInterface.h

#pragma once

#include "SideLaneSafety/SideLaneSafetyTypes.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"

//! @brief Implements the SideLaneSafetyCalculationsInterface
class SideLaneSafetyCalculationsInterface
{
public:
  virtual ~SideLaneSafetyCalculationsInterface() = default;

  //! @brief Checks if the estimated covered distance delta during the lane change leads to violation of min distance.
  //! @param estimatedLaneChangeDuration      Estimated duration of the lane change in seconds
  //! @param vehicleInfo                      Information of the vehicle on the target lane, vehicle and minDistance calculated with urgency
  //! @param surroundingVehicleQueryFactory   QueryFactoryModule held by ScmComponents
  //! @return True if the lane change can be finished without entering MinDistance
  virtual bool CanFinishLaneChangeWithoutEnteringMinDistance(units::time::second_t estimatedLaneChangeDuration, const SideLaneSafetyTypes::TargetLaneVehicleInformation& vehicleInfo, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const = 0;
};
