/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyTypes.h
//! @brief Contains structs for lane change and safety parameter

#pragma once

#include "SurroundingVehicles/SurroundingVehicleInterface.h"

//! @brief Namespaces for SideLaneSafetyTypes
namespace SideLaneSafetyTypes
{
//! @brief Struct definition for the description of a TargetLaneVehicleInformation
struct TargetLaneVehicleInformation
{
  //! @brief The min distance
  units::length::meter_t minDistance;
  //! @brief The surrounding vehicle
  const SurroundingVehicleInterface* vehicle;

  //! @brief Operator== overload to simplify is mindistance equal to other vehicle and vehicle == other vehicle
  //! @param other
  //! @return true when mindistance == other.vehicle && other.vehicle
  bool operator==(const TargetLaneVehicleInformation& other) const
  {
    return minDistance == other.minDistance && vehicle == other.vehicle;
  }
};

//! @brief Struct definition for the description of a LaneSafetyParameters
struct LaneSafetyParameters
{
  //! @brief The estimated lane change duration
  units::time::second_t estimatedLaneChangeDuration;
  //! @brief Target lane vehicle information about front area
  TargetLaneVehicleInformation front;
  //! @brief Target lane vehicle information about rear area
  TargetLaneVehicleInformation rear;
};
}  // namespace SideLaneSafetyTypes