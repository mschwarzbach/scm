/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SideLaneSafetyInterface.h

#pragma once

#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

//! @brief Implements the SideLaneSafetyInterface
class SideLaneSafetyInterface
{
public:
  virtual ~SideLaneSafetyInterface() = default;

  //! @brief Checks if ego can finish the lane change without entering the min distance of the rear and front vehicle on the target lane.
  //! @param laneChangeLength Calculated length of the desired lane change
  //! @param targetLane Target lane of the desired lane change
  //! @return True if the lane change can be finished without entering the min distance of vehicles on the target lane.
  virtual bool CanFinishLaneChangeSafely(units::length::meter_t laneChangeLength, RelativeLane targetLane) const = 0;
};