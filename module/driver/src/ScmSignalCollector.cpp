/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmSignalCollector.h"

ScmSignalCollector::ScmSignalCollector(StochasticsInterface* stochastics, units::time::millisecond_t cycleTime)
    : _stochastics(stochastics), _spawnVelocity(0), _cycleTime(cycleTime)
{
}

void ScmSignalCollector::TryCreate()
{
  if (_driverParameters && _vehicleModelParameters && _sensorOutputSignal && _trafficRulesScm)
  {
    _scmDependencies = std::make_unique<ScmDependencies>(
        *_driverParameters, *_trafficRulesScm, *_vehicleModelParameters, *_sensorOutputSignal, _stochastics, _spawnVelocity, _cycleTime);
  }
}

void ScmSignalCollector::Update(const scm::signal::DriverInput& driverInput)
{
  if (_scmDependencies)
  {
    _scmDependencies->UpdateParametersScmSignal(driverInput);
    _scmDependencies->UpdateParametersVehicleSignal(driverInput);
    _scmDependencies->UpdateScmSensorData(driverInput);
  }
  else
  {
    _driverParameters = driverInput.driverParameters;
    _trafficRulesScm = driverInput.trafficRulesScm;
    _vehicleModelParameters = driverInput.vehicleParameters;
    _spawnVelocity = driverInput.sensorOutput.ownVehicleInformationSCM.absoluteVelocity;
    _sensorOutputSignal = driverInput.sensorOutput;
    TryCreate();
  }
}

ScmDependenciesInterface* ScmSignalCollector::GetDependencies()
{
  return _scmDependencies.get();
}

bool ScmSignalCollector::IsAlive()
{
  return _scmDependencies != nullptr;
}
