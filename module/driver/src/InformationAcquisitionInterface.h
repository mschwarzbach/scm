/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  InformationAcquisitionInterface.h

#pragma once

#include "MicroscopicCharacteristics.h"
#include "include/common/ScmDefinitions.h"

//! *******************************************************************
//! @brief Functionality for interfacing between the physical data and the data stored internally by an agent.
//! This component is responsible for acquiring data, applying possible perceptive distortions and biases, and for storing
//! that data in OpticalData, so that it can then be integrated into the MentalModel.
//! @note Currently only a small part has been moved to this interface!
//! *******************************************************************
class InformationAcquisitionInterface
{
public:
  virtual ~InformationAcquisitionInterface() = default;

  //! @brief  Set helper variables (only required to run the program properly) of the ego agent.
  virtual void SetOwnVehicleHelperVariables() const = 0;

  //! @brief Update surrounding vehicle data
  //! @param forceUpdate                             Flag if update should be forced
  //! @param isNewInformationAcquisitionRequested    Flag indicating wether new information is collected in the current time step
  virtual void UpdateSurroundingVehicleData(bool forceUpdate, bool isNewInformationAcquisitionRequested) const = 0;

  //! @brief Update the internal data of the ego vehicle.
  //! @param ownVehicle_GroundTruth  All ground truth ego vehicle data
  virtual void UpdateOwnVehicleData(OwnVehicleInformationSCM *ownVehicle_GroundTruth) = 0;

  //! @brief Extrapolate internal data of the ego vehicle.
  //! @param ownVehicle_GroundTruth  All ground truth ego vehicle data
  virtual void ExtrapolateOwnVehicleData(OwnVehicleInformationSCM *ownVehicle_GroundTruth) = 0;

  //! @brief Prepare the collection or extrapolation of the ego vehicle data in information acquisition.
  //! @param forceUpdate                       forceUpdate flag
  virtual void UpdateEgoVehicleData(bool forceUpdate) = 0;

  //! @brief Assign surrounding object optical data for empty area of interest.
  //! @param surroundingObject    Surrounding object data as known to the agent in the mental model
  virtual void ResetAoiDataForNoObject(ObjectInformationScmExtended *surroundingObject) const = 0;

  //! @brief Assign surrounding object optical data for area of interest containing a object.
  //! @param surroundingObject                    Surrounding object data as known to the agent in the mental model
  //! @param surroundingObject_GroundTruth        Ground truth surrounding vehicle data
  //! @param aoi                                  Observed area of interest
  virtual void SetAoiDataWithGroundTruth(ObjectInformationScmExtended *surroundingObject,
                                         const ObjectInformationSCM *surroundingObject_GroundTruth,
                                         AreaOfInterest aoi) const = 0;

  //! @brief Return ground truth perception data from fovea and ufov.
  //! @param idealPerception
  //! @return Perceived data with the corresponding AOI
  virtual std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM *>> PerceiveSurroundingVehicles(bool idealPerception) const = 0;

  //! @brief Calculates missing data that is not present in side areas.
  //! @param object Surrounding object data from the mental model, that moved out of a side AOI in this timestep.
  //! @param newAoi New Area of Interest that was assigned to the agent in this iteration.
  virtual void CorrectAoiDataDueToLongitudinalTransitionFromSideArea(ObjectInformationScmExtended *object, AreaOfInterest newAoi) const = 0;

  //! @brief Removed data that is not present in side areas.
  //! @param object Surrounding object data from the mental model, that moved into a side AOI in this timestep.
  virtual void CorrectAoiDataDueToLongitudinalTransitionToSideArea(ObjectInformationScmExtended *object) const = 0;

  //! @brief Return agent ids of all ground truth agents visible in fovea, ufov or periphery
  //! @return Vector with the ids of all visible agents
  virtual std::vector<int> GetVisibleAgentIds() const = 0;

  //! @brief  Checks if object is out of sight
  //! @param relativeLongitudinalDistance
  //! @return Is out of sight
  virtual bool IsOutOfSight(units::length::meter_t relativeLongitudinalDistance) const = 0;

  //! @brief Get the cycletime
  //! @return Cycle time
  virtual units::time::millisecond_t GetCycleTime() const = 0;

  //! @brief Get the current simulation time
  //! @return simulation time
  virtual units::time::millisecond_t GetTime() const = 0;
};
