/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include "ScmComponentsInterface.h"
#include "ScmLifetimeState.h"

/**
 * \brief LivingState represents the regular-behaving SCM state. It processes input data coming from simulation core via 'AlgorithmScmMonolithisch' and generates corresponding output data based on SCM features.
 */
class LivingState : public ScmLifetimeState
{
public:
  LivingState(const std::string& componentName,
              units::time::millisecond_t cycleTime,
              PeriodicLoggerInterface* pLog,
              ScmComponentsInterface& scmComponents,
              ExternalControlState externalControlState);

  void DetermineAgentStates() override;

private:
  void UpdateMicroscopicData() override;
  void UpdateAgentInformation(units::time::millisecond_t time) override;
  bool ShouldResetMerges() const;
  bool ShouldResetBaseTimes() const;
};
