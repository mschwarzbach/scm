/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  FeatureExtractor.h

#pragma once

#include <algorithm>  // std::find

#include "FeatureExtractorInterface.h"
#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h"
#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

class MentalModelInterface;

class MentalCalculationsInterface;

class IgnoringOuterLaneOvertakingProhibition;

//! @copydoc FeatureExtractorInterface
//! Implements the FeatureExtractorInterface.
class FeatureExtractor : public FeatureExtractorInterface
{
public:
  //! @brief Default Constructor.
  FeatureExtractor();

  virtual ~FeatureExtractor() = default;

  //! @brief Explicit SetMentalModel cause of circular dependency between FeatureExtractor and MentalModel (MentalModel needs to extract Features and FeatureExtractor makes use of data stored in MentalModel)
  //! @param mentalModel             MentalModel for the feature extractor
  virtual void SetMentalModel(const MentalModelInterface& mentalModel);

  //! @brief Explicit SetMentalCalculation because of circular dependency between FeatureExtractor and MentalCalculations
  //! @param mentalCalculations      MentalCalculations for the feature extractor
  void SetMentalCalculations(const MentalCalculationsInterface& mentalCalculations);

  //! @brief Explicit SetSurroundingVehicleQueryFactory because of circular dependency between FeatureExtractor and SetSurroundingVehicleQueryFactory
  //! @param queryFactory
  void SetSurroundingVehicleQueryFactory(const SurroundingVehicleQueryFactoryInterface* queryFactory);

  //! @brief Explicit SetOuterLaneOvertakingProhibitionQuota because of circular dependency between FeatureExtractor and SetOuterLaneOvertakingProhibitionQuota
  //! @param outerLaneOvertakingProhibitionQuota
  void SetOuterLaneOvertakingProhibitionQuota(IgnoringOuterLaneOvertakingProhibitionInterface* outerLaneOvertakingProhibitionQuota);

  bool IsVehicleInSideLane(const SurroundingVehicleInterface* vehicle) const override;

  bool IsMinimumFollowingDistanceViolated(const SurroundingVehicleInterface* vehicle, double reductionFactorUrgency = 1.0) const override;

  bool IsInfluencingDistanceViolated(const SurroundingVehicleInterface* vehicle) const override;

  bool IsUnfavorable(const SurroundingVehicleInterface* vehicle) const override;

  bool HasJamVelocity(const SurroundingVehicleInterface* vehicle) const override;

  bool HasJamVelocityEgo() const override;

  bool IsDeceleratingDueToSuspiciousSideVehicles() const override;

  bool IsAvoidingLaneNextToSuspiciousSideVehicles() const override;

  bool IsNearEnoughForFollowing(const SurroundingVehicleInterface& vehicle, double reductionFactor = 1.0) const override;

  bool IsExceptionallySlow(const SurroundingVehicleInterface* vehicle) const override;

  bool HasSuspiciousBehaviour(const SurroundingVehicleInterface* vehicle) const override;

  bool HasIntentionalLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const override;

  bool HasAnticipatedLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const override;

  bool IsCollisionCourseDetected(const SurroundingVehicleInterface* vehicle) const override;

  bool IsSideLaneSafe(Side side) const override;

  bool IsLaneChangeSafe(bool isTransitionTrigger) const override;

  bool AbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const override;

  bool AnticipatedAbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const override;

  bool IsOnEntryOrExitLane() const override;

  bool IsOnEntryLane() const override;

  bool IsEvadingEndOfLane() const override;

  bool IsLaneDriveablePerceived(RelativeLane relativeLane) const override;

  bool IsLaneDriveablePerceived() const override;

  bool IsLaneTypeDriveable(scm::LaneType type) const override;

  bool IsExceedingLateralMotionThresholdTowardsEgoLane(const SurroundingVehicleInterface* vehicle) const override;

  bool IsLaneChangePermittedDueToLaneMarkings(Side side) const override;

  bool IsLaneChangePermittedDueToLaneMarkingsForAoi(const SurroundingVehicleInterface* vehicle) const override;

  bool CheckLaneChange(Side side, bool isEmergecy = false) const override;

  bool DoesLaneMarkingBrokenBoldPreventLaneChange(const scm::LaneMarking::Entity& laneMarking, Side side) const override;

  bool IsRightLaneEmptyLaneConvenienceAndHigherThanEgo() const override;

  bool DoesVehicleSurroundingFit(AreaOfInterest aoiToEvaluate) const override;

  //! @brief Checks whether the outer lane overtaking prohibition is relevant for the current situation
  //! @param aoiToEvaluate
  //! @return
  bool IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition(AreaOfInterest aoiToEvaluate) const;

  bool DoesOuterLaneOvertakingProhibitionApply(AreaOfInterest aoiToEvaluate, bool rightHandTraffic) const override;

  units::time::second_t GetTimeAtTargetSpeedInLane(SurroundingLane lane) const override;

  bool WillEgoLeaveLaneBeforeCollision(const SurroundingVehicleInterface* vehicle) const override;

  bool HasSideLaneNoCollisionCourses(Side side) const override;

  bool CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface* vehicle) const override;

  bool HasActualLaneCrossingConflict(const SurroundingVehicleInterface& vehicle) const override;

  virtual bool HasDrivableSuccessor(bool isEmergency, RelativeLane relativeLane) const override;

protected:
  //! @brief Checks if there is a potential collision course for side area given a side Aoi and index
  //! @param vehicle     const SurroundingVehicleInterface*
  //! @return True if there is a possible collision course, false otherwise
  bool IsCollisionCoursePossibleForSideAreas(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Checks if there is a potential collision course for non-side area given a non-side Aoi
  //! @param vehicle     const SurroundingVehicleInterface*
  //! @return True if there is a possible collision course, false otherwise
  bool IsCollisionCoursePossibleForNonSideAreas(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Calculates needed relative lateral distance to be able to leave the own lane
  //! @param relativeVelocity
  //! @param relevantVehicleWidth
  //! @return distance to leave the lane
  units::length::meter_t CalculateLateralDistanceToLeaveLane(units::velocity::meters_per_second_t relativeVelocity, units::length::meter_t relevantVehicleWidth) const;

  //! @brief Returns the relevant vehicle width based on the lateral velocity, i.e. projected vehicle width left or right if there is lateral movement
  //! @param lateralVelocity
  //! @return relevant vehicle width
  virtual units::length::meter_t GetRelevantVehicleWidth(units::velocity::meters_per_second_t lateralVelocity) const;

  //! @brief Checks whether a vehicle is able to leave its lane before it's ending
  //! @param lateralDistanceToLeaveLane
  //! @param lateralVelocity
  //! @return True if ego can leave its lane before it is ending given a lateral velocity and distance
  bool CanLeaveLaneBeforeItIsEnding(const units::length::meter_t lateralDistanceToLeaveLane, const units::velocity::meters_per_second_t lateralVelocity) const;

  //! @brief Checks whether Agent is already in a preparing to merge left or right state
  //! @return True if agent is already in the lateral action "PreparingToMerge"
  virtual bool IsAlreadyPreparingToMerge() const;

  //! @brief Checks whether a collision course for a side area is detected or not
  //! @param vehicle
  //! @return True if there is a collision course detected with the vehicle, false otherwise
  bool IsCollisionCourseDetectedSideArea(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Checks whether a collision course for a non-side area is detected or not
  //! @param vehicle
  //! @return True if there is a collision course detected with the vehicle, false otherwise
  bool IsCollisionCourseDetectedNonSideArea(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Checks whether an aoi is relevant for a possible collision
  //! @param aoi
  //! @return True if the aoi is relevant for collision considerations
  bool IsCollisionRelevantArea(AreaOfInterest aoi) const;

  //! @brief Determine needed time to change sides
  //! @param vehicle   SurroundingVehicleInterface*
  //! @return Needed time
  virtual units::time::second_t DetermineTimeToChangeSides(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Checks whether a possible collision course can't be prevented
  //! @param vehicle
  //! @return True if the collision course cannot be simply avoided, false otherwise
  virtual bool CantPreventCollisionCourse(const SurroundingVehicleInterface* vehicle) const;

  bool IsObstacle(const SurroundingVehicleInterface* vehicle) const override;

  //! @brief Determine time to pass by other lane changer - e.g. used in HasActuaLaneCrossingConflict
  //! @param vehicle
  //! @param deltaVelocity
  //! @return passing time
  units::time::second_t GetPassingTime(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const;

protected:
  //! @brief Determine, if the minimal following distance is violated in case of jam and low ttc.
  //! If the ego speed is below jam speed and the ttc is below a threshold a lower minimum following distance is used.
  //! @throws Runtime error if called with a side AOI
  //! @param vehicle            const SurroundingVehicleInterface
  //! @return True if the minimum following distance is violated.
  virtual bool IsMinimumFollowingDistanceViolatedWhileCooperating(const SurroundingVehicleInterface* vehicle) const;

  //! @brief If two lanes are separated with broken-bold lane markings, they can be considered as different roadway thus allowing to driver faster on the 'outer' lane.
  //! @param rightHandTraffic
  //! @return True if the lane is considered as another road based on the lane markings, false otherwise
  bool IsSideLaneSeparateRoadway(bool rightHandTraffic) const;

  //! @brief Checks whether the AOI is relevant for outer lane overtaking based on rightHandTraffic
  //! @param aoi
  //! @param rightHandTraffic
  //! @return True if aoi has to be considered for outer lane overtaking (prohibition)
  bool IsAoiRelevantForOuterLaneOvertaking(AreaOfInterest aoi, bool rightHandTraffic = true) const;

  //! @brief Checks whether a side lane is safe
  //! @param side
  //! @return True if side lane is considered as safe, false otherwise
  bool IsLaneSafe(Side side) const;

  //! \brief The theshold above which a side movement is detected as significant [m/s].
  //! @brief Perceive time until aoi vehicle crosses into ego lane at anticipated lateral speed
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return time_to_crossing
  virtual units::time::second_t AnticipatedTimeToLaneCrossing(const SurroundingVehicleInterface* vehicle) const;

private:
  //! @brief Check whether the distance to the front and rear vehicle exceedes the minimum time headway.
  //! @return True if the current lane is considered as safe, false otherwise
  bool IsCurrentLaneSafe() const;

  //! @brief Perceive time until aoi vehicle crosses into ego lane at current lateral speed
  //! (zero if already perceived to have crossed).
  //! @param vehicle
  //! @param vwToEgoLane
  //! @return time to cross a lane
  virtual units::time::second_t TimeToLaneCrossing(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t vwToEgoLane) const;

  //! @brief Perceive time until aoi vehicle crosses into ego lane at current lateral speed
  //! (zero if already perceived to have crossed).
  //! @param vehicle             const SurroundingVehicleInterface*
  //! @return time_to_crossing
  virtual units::time::second_t PerceivedTimeToLaneCrossing(const SurroundingVehicleInterface* vehicle) const;

  //! @brief The theshold above which a side movement is detected as significant [m/s].
  const units::velocity::meters_per_second_t THRESHOLD_VELOCITY_SIDE_MOVEMENT = 0.5_mps;

  //! @brief pointer to mental model
  const MentalModelInterface* _mentalModel;

  //! @brief pointer to mental calculations
  const MentalCalculationsInterface* _mentalCalculations;

  //! @brief pointer to IgnoringOuterLaneOvertakingProhibition
  IgnoringOuterLaneOvertakingProhibitionInterface* _outerLaneOvertakingProhibitionQuota;

  //! @brief Redirect calculation of the time to collision too SCMCommons for testing
  //! @param vDeltaObservedMinusEgo    Relative longitudinal velocity between the two agents (measured from leading vehicle to following vehicle) in m/s.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @return Time to collision / Tau in s
  virtual units::time::second_t CalculateTimeToCollision(const units::velocity::meters_per_second_t vDeltaObservedMinusEgo, const units::length::meter_t relativeNetDistance) const;

  //! @brief Checks whether the ttc is greater than the lower bound but less than the upper bound
  //! @param ttc
  //! @return True if ttc within the limits, false otherwise
  bool IsTTCInBounds(const units::time::second_t ttc) const;

  //! @brief SurroundingVehicleQueryFactory module
  const SurroundingVehicleQueryFactoryInterface* _surroundingVehicleQueryFactory;
};
