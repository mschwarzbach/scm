/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmCommons.h"

#include <algorithm>
#include <cmath>

#include "include/common/ScmDefinitions.h"

AreaOfInterest ScmCommons::MapGazeStateToAreaOfInterest(GazeState state)
{
  //  GazeState::SACCADE,
  //  GazeState::UNDEFINED,
  //  GazeState::NumberOfGazeStates
  //  ... upper gaze states do not change the AreaOfInterest in the current system
  std::map<GazeState, AreaOfInterest> stateToInterest{
      {GazeState::EGO_FRONT, AreaOfInterest::EGO_FRONT},
      {GazeState::EGO_FRONT_FAR, AreaOfInterest::EGO_FRONT_FAR},
      {GazeState::EGO_REAR, AreaOfInterest::EGO_REAR},
      {GazeState::LEFT_FRONT, AreaOfInterest::LEFT_FRONT},
      {GazeState::LEFT_FRONT_FAR, AreaOfInterest::LEFT_FRONT_FAR},
      {GazeState::LEFT_REAR, AreaOfInterest::LEFT_REAR},
      {GazeState::LEFT_SIDE, AreaOfInterest::LEFT_SIDE},
      {GazeState::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT},
      {GazeState::RIGHT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR},
      {GazeState::RIGHT_REAR, AreaOfInterest::RIGHT_REAR},
      {GazeState::RIGHT_SIDE, AreaOfInterest::RIGHT_SIDE},
      {GazeState::LEFTLEFT_FRONT, AreaOfInterest::LEFTLEFT_FRONT},
      {GazeState::LEFTLEFT_REAR, AreaOfInterest::LEFTLEFT_REAR},
      {GazeState::LEFTLEFT_SIDE, AreaOfInterest::LEFTLEFT_SIDE},
      {GazeState::RIGHTRIGHT_FRONT, AreaOfInterest::RIGHTRIGHT_FRONT},
      {GazeState::RIGHTRIGHT_REAR, AreaOfInterest::RIGHTRIGHT_REAR},
      {GazeState::RIGHTRIGHT_SIDE, AreaOfInterest::RIGHTRIGHT_SIDE},
      {GazeState::INSTRUMENT_CLUSTER, AreaOfInterest::INSTRUMENT_CLUSTER},
      {GazeState::INFOTAINMENT, AreaOfInterest::INFOTAINMENT},
      {GazeState::HUD, AreaOfInterest::HUD},
      {GazeState::DISTRACTION, AreaOfInterest::DISTRACTION},
      {GazeState::SACCADE, AreaOfInterest::NumberOfAreaOfInterests},
      {GazeState::UNDEFINED, AreaOfInterest::NumberOfAreaOfInterests},
      {GazeState::NumberOfGazeStates, AreaOfInterest::NumberOfAreaOfInterests}  //
  };

  auto it{stateToInterest.find(state)};

  if (stateToInterest.end() == it)
  {
    return AreaOfInterest::NumberOfAreaOfInterests;
  }

  return it->second;
}

GazeState ScmCommons::MapAreaOfInterestToGazeState(AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::LEFT_FRONT:
      return GazeState::LEFT_FRONT;
    case AreaOfInterest::LEFT_FRONT_FAR:
      return GazeState::LEFT_FRONT_FAR;
    case AreaOfInterest::RIGHT_FRONT:
      return GazeState::RIGHT_FRONT;
    case AreaOfInterest::RIGHT_FRONT_FAR:
      return GazeState::RIGHT_FRONT_FAR;
    case AreaOfInterest::LEFT_REAR:
      return GazeState::LEFT_REAR;
    case AreaOfInterest::RIGHT_REAR:
      return GazeState::RIGHT_REAR;
    case AreaOfInterest::EGO_FRONT:
      return GazeState::EGO_FRONT;
    case AreaOfInterest::EGO_FRONT_FAR:
      return GazeState::EGO_FRONT_FAR;
    case AreaOfInterest::EGO_REAR:
      return GazeState::EGO_REAR;
    case AreaOfInterest::LEFT_SIDE:
      return GazeState::LEFT_SIDE;
    case AreaOfInterest::LEFTLEFT_SIDE:
      return GazeState::LEFTLEFT_SIDE;
    case AreaOfInterest::RIGHT_SIDE:
      return GazeState::RIGHT_SIDE;
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return GazeState::RIGHTRIGHT_SIDE;
    case AreaOfInterest::INSTRUMENT_CLUSTER:
      return GazeState::INSTRUMENT_CLUSTER;
    case AreaOfInterest::INFOTAINMENT:
      return GazeState::INFOTAINMENT;
    case AreaOfInterest::HUD:
      return GazeState::HUD;
    case AreaOfInterest::DISTRACTION:
      return GazeState::DISTRACTION;
    case AreaOfInterest::NumberOfAreaOfInterests:
      return GazeState::NumberOfGazeStates;
    default:
      throw std::runtime_error("ScmCommons::MapGazeStateToAreaOfInterest - received string that matches no AreaOfInterest");
  }
}

AreaOfInterest ScmCommons::ConvertFarAoiToCloseAoi(AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::EGO_FRONT_FAR:
      return AreaOfInterest::EGO_FRONT;
    case AreaOfInterest::LEFT_FRONT_FAR:
      return AreaOfInterest::LEFT_FRONT;
    case AreaOfInterest::RIGHT_FRONT_FAR:
      return AreaOfInterest::RIGHT_FRONT;
    default:
      return AreaOfInterest::NumberOfAreaOfInterests;
  }
}

std::string ScmCommons::FieldOfViewAssignmentToString(FieldOfViewAssignment qualityArea)
{
  switch (qualityArea)
  {
    case FieldOfViewAssignment::FOVEA:
      return "FOVEA";
    case FieldOfViewAssignment::UFOV:
      return "UFOV";
    case FieldOfViewAssignment::PERIPHERY:
      return "PERIPHERY";
    case FieldOfViewAssignment::NONE:
      return "NONE";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::AreaOfInterestToString(AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::LEFT_FRONT:
      return "LEFT_FRONT";
    case AreaOfInterest::LEFTLEFT_FRONT:
      return "LEFTLEFT_FRONT";
    case AreaOfInterest::LEFT_FRONT_FAR:
      return "LEFT_FRONT_FAR";
    case AreaOfInterest::RIGHT_FRONT:
      return "RIGHT_FRONT";
    case AreaOfInterest::RIGHTRIGHT_FRONT:
      return "RIGHTRIGHT_FRONT";
    case AreaOfInterest::RIGHT_FRONT_FAR:
      return "RIGHT_FRONT_FAR";
    case AreaOfInterest::LEFT_REAR:
      return "LEFT_REAR";
    case AreaOfInterest::RIGHT_REAR:
      return "RIGHT_REAR";
    case AreaOfInterest::LEFTLEFT_REAR:
      return "LEFTLEFT_REAR";
    case AreaOfInterest::RIGHTRIGHT_REAR:
      return "RIGHTRIGHT_REAR";
    case AreaOfInterest::EGO_FRONT:
      return "EGO_FRONT";
    case AreaOfInterest::EGO_FRONT_FAR:
      return "EGO_FRONT_FAR";
    case AreaOfInterest::EGO_REAR:
      return "EGO_REAR";
    case AreaOfInterest::LEFT_SIDE:
      return "LEFT_SIDE";
    case AreaOfInterest::RIGHT_SIDE:
      return "RIGHT_SIDE";
    case AreaOfInterest::LEFTLEFT_SIDE:
      return "LEFTLEFT_SIDE";
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return "RIGHTRIGHT_SIDE";
    case AreaOfInterest::INSTRUMENT_CLUSTER:
      return "INSTRUMENT_CLUSTER";
    case AreaOfInterest::INFOTAINMENT:
      return "INFOTAINMENT";
    case AreaOfInterest::HUD:
      return "HUD";
    case AreaOfInterest::NumberOfAreaOfInterests:
      return "NumberOfAreaOfInterests";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::HighCognitiveSituationToString(HighCognitiveSituation situation)
{
  switch (situation)
  {
    case HighCognitiveSituation::APPROACH:
      return "APPROACH";
    case HighCognitiveSituation::FOLLOW:
      return "FOLLOW";
    case HighCognitiveSituation::SALIENT_APPROACH:
      return "SALIENT_APPROACH";
    case HighCognitiveSituation::SALIENT_FOLLOW:
      return "SALIENT_FOLLOW";
    case HighCognitiveSituation::UNDEFINED:
      return "UNDEFINED";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::GazeStateToString(GazeState gazeState)
{
  switch (gazeState)
  {
    case GazeState::EGO_FRONT:
      return "EGO_FRONT";
    case GazeState::EGO_FRONT_FAR:
      return "EGO_FRONT_FAR";
    case GazeState::EGO_REAR:
      return "EGO_REAR";
    case GazeState::HUD:
      return "HUD";
    case GazeState::INFOTAINMENT:
      return "INFOTAINMENT";
    case GazeState::INSTRUMENT_CLUSTER:
      return "INSTRUMENT_CLUSTER";
    case GazeState::LEFT_FRONT:
      return "LEFT_FRONT";
    case GazeState::LEFT_FRONT_FAR:
      return "LEFT_FRONT_FAR";
    case GazeState::LEFT_REAR:
      return "LEFT_REAR";
    case GazeState::LEFT_SIDE:
      return "LEFT_SIDE";
    case GazeState::NumberOfGazeStates:
      return "NumberOfGazeStates";
    case GazeState::RIGHT_FRONT:
      return "RIGHT_FRONT";
    case GazeState::RIGHT_FRONT_FAR:
      return "RIGHT_FRONT_FAR";
    case GazeState::RIGHT_REAR:
      return "RIGHT_REAR";
    case GazeState::RIGHT_SIDE:
      return "RIGHT_SIDE";
    case GazeState::SACCADE:
      return "SACCADE";
    case GazeState::UNDEFINED:
      return "UNDEFINED";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::BooleanToString(bool boolean)
{
  return boolean ? "TRUE" : "FALSE";
}

std::string ScmCommons::IndicatorToString(scm::LightState::Indicator indicator)
{
  switch (indicator)
  {
    case scm::LightState::Indicator::Off:
      return "IndicatorState_Off";
    case scm::LightState::Indicator::Warn:
      return "IndicatorState_Warn";
    case scm::LightState::Indicator::Left:
      return "IndicatorState_Left";
    case scm::LightState::Indicator::Right:
      return "IndicatorState_Right";
    default:
      return "Undefined";
  }
}

std::string ScmCommons::SituationToString(Situation situation)
{
  switch (situation)
  {
    case Situation::LANE_CHANGER_FROM_LEFT:
      return "LANE_CHANGER_LEFT";
    case Situation::LANE_CHANGER_FROM_RIGHT:
      return "LANE_CHANGER_RIGHT";
    case Situation::FREE_DRIVING:
      return "FREE_DRIVING";
    case Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE:
      return "SUSPICIOUS_OBJECT_IN_LEFT_LANE";
    case Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE:
      return "SUSPICIOUS_OBJECT_IN_RIGHT_LANE";
    case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
      return "SIDE_COLLISION_RISK_FROM_RIGHT";
    case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
      return "SIDE_COLLISION_RISK_FROM_LEFT";
    case Situation::OBSTACLE_ON_CURRENT_LANE:
      return "OBSTACLE_ON_CURRENT_LANE";
    case Situation::FOLLOWING_DRIVING:
      return "FOLLOWING_DRIVING";
    case Situation::COLLISION:
      return "COLLISION";
    case Situation::UNDEFINED:
      return "UNDEFINED";
    case Situation::NumberOfSituations:
      return "NumberOfSituations";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::LateralActionToString(LateralAction action)
{
  return to_string(action);
}

std::string ScmCommons::LaneTypeToString(scm::LaneType type)
{
  switch (type)
  {
    case scm::LaneType::Shoulder:
      return "Shoulder";
    case scm::LaneType::Border:
      return "Border";
    case scm::LaneType::Driving:
      return "Driving";
    case scm::LaneType::Stop:
      return "Stop";
    case scm::LaneType::None:
      return "None";
    case scm::LaneType::Restricted:
      return "Restricted";
    case scm::LaneType::Parking:
      return "Parking";
    case scm::LaneType::Median:
      return "Median";
    case scm::LaneType::Biking:
      return "Biking";
    case scm::LaneType::Sidewalk:
      return "Sidewalk";
    case scm::LaneType::Curb:
      return "Curb";
    case scm::LaneType::Exit:
      return "Exit";
    case scm::LaneType::Entry:
      return "Entry";
    case scm::LaneType::OnRamp:
      return "OnRamp";
    case scm::LaneType::OffRamp:
      return "OffRamp";
    case scm::LaneType::ConnectingRamp:
      return "ConnectingRamp";
    default:
      return "Undefined";
  }
}

std::string ScmCommons::SwervingStateToString(SwervingState action)
{
  switch (action)
  {
    case SwervingState::DEFUSING:
      return "DEFUSING";
    case SwervingState::EVADING:
      return "EVADING";
    case SwervingState::NONE:
      return "NONE";
    case SwervingState::RETURNING:
      return "RETURNING";

    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::LaneChangeActionToString(LaneChangeAction action)
{
  switch (action)
  {
    case LaneChangeAction::None:
      return "None";
    case LaneChangeAction::IntentPositive:
      return "IntentPositive";
    case LaneChangeAction::IntentNegative:
      return "IntentNegative";
    case LaneChangeAction::ForcePositive:
      return "ForcePositive";
    case LaneChangeAction::ForceNegative:
      return "ForceNegative";
    default:
      return "Unkown";
  }
}

std::string ScmCommons::LongitudinalActionStateToString(LongitudinalActionState subAction)
{
  switch (subAction)
  {
    case LongitudinalActionState::APPROACHING:
      return "APPROACHING";
    case LongitudinalActionState::BRAKING_TO_END_OF_LANE:
      return "BRAKING_TO_END_OF_LANE";
    case LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE:
      return "FALL_BACK_TO_EQUILIBRIUM_DISTANCE";
    case LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE:
      return "FOLLOWING_AT_EQUILIBRIUM_DISTANCE";
    case LongitudinalActionState::SPEED_ADJUSTMENT:
      return "SPEED_ADJUSTMENT";
    // case LongitudinalActionState::SLOWLY_DRIVING:
    //  return "SLOWLY_DRIVING";
    case LongitudinalActionState::STOPPED:
      return "STOPPED";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::MinThwPerspectiveToString(MinThwPerspective perspective)
{
  switch (perspective)
  {
    case MinThwPerspective::NORM:
      return "NORM";
      break;
    case MinThwPerspective::EGO_ANTICIPATED_FRONT:
      return "EGO_ANTICIPATED_FRONT";
      break;
    case MinThwPerspective::EGO_ANTICIPATED_REAR:
      return "EGO_ANTICIPATED_REAR";
      break;
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::LaneChangeStateToString(LaneChangeState state)
{
  switch (state)
  {
    case LaneChangeState::LaneChangeLeft:
      return "LEFT";
    case LaneChangeState::LaneChangeRight:
      return "RIGHT";
    case LaneChangeState::NoLaneChange:
      return "NO CHANGE";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::CriticalActionStateToString(CriticalActionState state)
{
  switch (state)
  {
    case CriticalActionState::BRAKING:
      return "BRAKING";
    case CriticalActionState::EVADING_LEFT:
      return "EVADING LEFT";
    case CriticalActionState::EVADING_RIGHT:
      return "EVADING RIGHT";
    case CriticalActionState::BRAKING_OR_EVADING:
      return "BRAKING OR EVADING";
    case CriticalActionState::COLLISION_UNAVOIDABLE:
      return "COLLISION UNAVOIDABLE";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::SurroundingLaneToString(SurroundingLane lane)
{
  switch (lane)
  {
    case SurroundingLane::EGO:
      return "EGO_LANE";
    case SurroundingLane::LEFT:
      return "LEFT_LANE";
    case SurroundingLane::RIGHT:
      return "RIGHT_LANE";
    default:
      return "UNDEFINED";
  }
}

std::string ScmCommons::RelativeLongitudinalPositionToString(RelativeLongitudinalPosition position)
{
  switch (position)
  {
    case RelativeLongitudinalPosition::FRONT_FAR:
      return "FRONT_FAR";
    case RelativeLongitudinalPosition::FRONT:
      return "FRONT";
    case RelativeLongitudinalPosition::SIDE:
      return "SIDE";
    case RelativeLongitudinalPosition::REAR:
      return "REAR";
    case RelativeLongitudinalPosition::REAR_FAR:
      return "REAR_FAR";
    default:
      return "UNDEFINED";
  }
}

units::time::second_t ScmCommons::CalculateTTC(units::velocity::meters_per_second_t vDeltaFrontMinusRear, units::length::meter_t relativeNetDistance, AreaOfInterest aoi)
{
  if (aoi == AreaOfInterest::LEFT_SIDE || aoi == AreaOfInterest::RIGHT_SIDE || aoi == AreaOfInterest::RIGHTRIGHT_SIDE ||
      aoi == AreaOfInterest::LEFTLEFT_SIDE)
  {
    return 0._s;  // TTC = 0 if vehicle is next to my side
  }

  // use general method for calculation
  return CalculateTimeToCollision(vDeltaFrontMinusRear, relativeNetDistance);
}

units::time::second_t ScmCommons::CalculateTimeToCollision(units::velocity::meters_per_second_t vDeltaObservedMinusEgo, units::length::meter_t relativeNetDistance)
{
  if (relativeNetDistance < 0._m)
  {
    return 0_s;
  }
  else
  {
    return std::max(std::min(ScmDefinitions::TTC_LIMIT, -relativeNetDistance / vDeltaObservedMinusEgo), -ScmDefinitions::TTC_LIMIT);
  }
}

units::time::second_t ScmCommons::CalculateTtcThreshold(units::angle::radian_t opticalAngleSizeHorizontal, units::angle::radian_t opticalAngleTowardsViewAxis, DriverParameters driverParameters, bool applyCorticalMagnification)
{
  const auto thresholdRetinalProjectionAngularSpeedFovea{
      driverParameters.thresholdRetinalProjectionAngularSpeedFovea / (2 * M_PI)};
  const double corticalMagnificationFactor{
      applyCorticalMagnification ? CalculateCorticalMagnificationFactor(opticalAngleTowardsViewAxis) : 1.};
  const auto thresholdRetinalProjectionAngularSpeed = thresholdRetinalProjectionAngularSpeedFovea * corticalMagnificationFactor;
  return opticalAngleSizeHorizontal / thresholdRetinalProjectionAngularSpeed;
}

double ScmCommons::CalculateTauDot(units::velocity::meters_per_second_t vDeltaFrontMinusRear, units::length::meter_t relativeNetDistance, units::acceleration::meters_per_second_squared_t aDeltaFrontMinusRear, AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::EGO_REAR:
    case AreaOfInterest::LEFT_REAR:
    case AreaOfInterest::RIGHT_REAR:
    case AreaOfInterest::EGO_FRONT:
    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::RIGHT_FRONT:
    case AreaOfInterest::EGO_FRONT_FAR:
    case AreaOfInterest::LEFT_FRONT_FAR:
    case AreaOfInterest::RIGHT_FRONT_FAR:
    case AreaOfInterest::LEFTLEFT_REAR:
    case AreaOfInterest::RIGHTRIGHT_REAR:
    case AreaOfInterest::LEFTLEFT_FRONT:
    case AreaOfInterest::RIGHTRIGHT_FRONT:
      break;  // TauDot is calculated following rules below

    case AreaOfInterest::LEFT_SIDE:
    case AreaOfInterest::RIGHT_SIDE:
    case AreaOfInterest::LEFTLEFT_SIDE:
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return 0.;  // TauDot = 0 if vehicle is next to my side

    default:
      return 0.;
  }

  if (relativeNetDistance < 0._m)
  {
    return 0.;
  }
  else
  {
    return -(1. - relativeNetDistance * aDeltaFrontMinusRear / (vDeltaFrontMinusRear * vDeltaFrontMinusRear));
  }
}

units::time::second_t ScmCommons::CalculateNetGap(units::velocity::meters_per_second_t v_ego,
                                                  units::velocity::meters_per_second_t v_observed,
                                                  units::length::meter_t relativeNetDistance,
                                                  AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::EGO_REAR:
    case AreaOfInterest::LEFT_REAR:
    case AreaOfInterest::RIGHT_REAR:
    case AreaOfInterest::LEFTLEFT_REAR:
    case AreaOfInterest::RIGHTRIGHT_REAR:

      // all distances are positive values according to the current concept
      if (relativeNetDistance <= 0._m)
      {
        return 0._s;
      }

      return (relativeNetDistance / v_observed);

    case AreaOfInterest::EGO_FRONT:
    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::RIGHT_FRONT:
    case AreaOfInterest::EGO_FRONT_FAR:
    case AreaOfInterest::LEFT_FRONT_FAR:
    case AreaOfInterest::RIGHT_FRONT_FAR:
    case AreaOfInterest::LEFTLEFT_FRONT:
    case AreaOfInterest::RIGHTRIGHT_FRONT:

      // all distances are positive values according to the current concept
      if (relativeNetDistance <= 0._m)
      {
        return 0._s;
      }

      return (relativeNetDistance / v_ego);

    case AreaOfInterest::LEFT_SIDE:
    case AreaOfInterest::RIGHT_SIDE:
    case AreaOfInterest::LEFTLEFT_SIDE:
    case AreaOfInterest::RIGHTRIGHT_SIDE:

      return 0._s;  //  vehicle is currently in side area

    default:
      return 99.0_s;
  }
}

double ScmCommons::CalculateNetGapDot(units::velocity::meters_per_second_t v_ego,
                                      units::velocity::meters_per_second_t v_observed,
                                      units::acceleration::meters_per_second_squared_t a_ego,
                                      units::acceleration::meters_per_second_squared_t a_observed,
                                      units::length::meter_t relativeNetDistance,
                                      units::velocity::meters_per_second_t vDelta,
                                      AreaOfInterest aoi)
{
  switch (aoi)
  {
    case AreaOfInterest::EGO_REAR:
    case AreaOfInterest::LEFT_REAR:
    case AreaOfInterest::RIGHT_REAR:
    case AreaOfInterest::LEFTLEFT_REAR:
    case AreaOfInterest::RIGHTRIGHT_REAR:
      if (relativeNetDistance <= 0_m)  // GetDistanceToObject in InformationAcquisition already returns net gaps
      {
        return 0;
      }
      return (vDelta / v_observed) - ((relativeNetDistance * a_observed) / (v_observed * v_observed));  // v and a from observed vehicle due to rear aois

    case AreaOfInterest::EGO_FRONT:
    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::RIGHT_FRONT:
    case AreaOfInterest::EGO_FRONT_FAR:
    case AreaOfInterest::LEFT_FRONT_FAR:
    case AreaOfInterest::RIGHT_FRONT_FAR:
    case AreaOfInterest::LEFTLEFT_FRONT:
    case AreaOfInterest::RIGHTRIGHT_FRONT:
      if (relativeNetDistance <= 0_m)  // GetDistanceToObject in InformationAcquisition already returns net gaps
      {
        return 0;
      }
      return (vDelta / v_ego) - ((relativeNetDistance * a_ego) / (v_ego * v_ego));  // v and a from ego vehicle for front aois

    case AreaOfInterest::LEFT_SIDE:
    case AreaOfInterest::RIGHT_SIDE:
    case AreaOfInterest::LEFTLEFT_SIDE:
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return 0;  // Time headway dot = 0 if vehicle is next to my side

    default:
      return 0.0;
  }
}

std::string ScmCommons::LaneExistenceToString(LaneExistence laneExistence)
{
  switch (laneExistence)
  {
    case LaneExistence::FALSE:
    {
      return "FALSE";
      break;
    }
    case LaneExistence::TRUE:
    {
      return "TRUE";
      break;
    }
    case LaneExistence::UNKNOWN:
    {
      return "UNKNOWN";
      break;
    }
    default:
    {
      return "UNDEFINED";
      break;
    }
  }
}

const ObjectInformationSCM* ScmCommons::GetObjectInformationFromSurroundingObjectScm(AreaOfInterest aoi,
                                                                                     const SurroundingObjectsSCM* surroundingObjectsScm,
                                                                                     int indexSideAoi)
{
  auto index = surroundingObjectsScm->ongoingTraffic.GetIndexFrom(aoi);
  if (surroundingObjectsScm->ongoingTraffic[index].at(aoi)->empty())
  {
    return nullptr;
  }
  if (index == decltype(surroundingObjectsScm->ongoingTraffic)::Index::CLOSE)
  {
    return &surroundingObjectsScm->ongoingTraffic[index].at(aoi)->at(indexSideAoi);
  }
  return &surroundingObjectsScm->ongoingTraffic[index].at(aoi)->front();
}

double ScmCommons::CalculateCorticalMagnificationFactor(units::angle::radian_t opticalAngleTowardsViewAxis)
{
  const units::angle::degree_t angleInDeg = units::math::abs(opticalAngleTowardsViewAxis);
  return 1 + 0.29 * angleInDeg.value() + 0.000012 * angleInDeg.value() * angleInDeg.value();
}

AreaOfInterest ScmCommons::StringToAreaOfInterest(const std::string string)
{
  if (string == "DISTRACTION")
  {
    return AreaOfInterest::DISTRACTION;
  }
  else if (string == "EGO_FRONT")
  {
    return AreaOfInterest::EGO_FRONT;
  }
  else if (string == "EGO_FRONT_FAR")
  {
    return AreaOfInterest::EGO_FRONT_FAR;
  }
  else if (string == "EGO_REAR")
  {
    return AreaOfInterest::EGO_REAR;
  }
  else if (string == "HUD")
  {
    return AreaOfInterest::HUD;
  }
  else if (string == "INFOTAINMENT")
  {
    return AreaOfInterest::INFOTAINMENT;
  }
  else if (string == "INSTRUMENT_CLUSTER")
  {
    return AreaOfInterest::INSTRUMENT_CLUSTER;
  }
  else if (string == "LEFT_FRONT")
  {
    return AreaOfInterest::LEFT_FRONT;
  }
  else if (string == "LEFT_FRONT_FAR")
  {
    return AreaOfInterest::LEFT_FRONT_FAR;
  }
  else if (string == "LEFT_REAR")
  {
    return AreaOfInterest::LEFT_REAR;
  }
  else if (string == "LEFT_SIDE")
  {
    return AreaOfInterest::LEFT_SIDE;
  }
  else if (string == "RIGHT_FRONT")
  {
    return AreaOfInterest::RIGHT_FRONT;
  }
  else if (string == "RIGHT_FRONT_FAR")
  {
    return AreaOfInterest::RIGHT_FRONT_FAR;
  }
  else if (string == "RIGHT_REAR")
  {
    return AreaOfInterest::RIGHT_REAR;
  }
  else if (string == "RIGHT_SIDE")
  {
    return AreaOfInterest::RIGHT_SIDE;
  }
  else if (string == "NumberOfAreaOfInterests")
  {
    return AreaOfInterest::NumberOfAreaOfInterests;
  }

  throw std::runtime_error("ScmCommons::StringToAreaOfInterest - received string that matches no AreaOfInterest");
}

bool ScmCommons::IsSurroundingAreaOfInterest(AreaOfInterest aoiToCheck)
{
  return std::any_of(begin(SURROUNDING_AREAS_OF_INTEREST),
                     end(SURROUNDING_AREAS_OF_INTEREST),
                     [aoiToCheck](const AreaOfInterest aoi)
                     {
                       return aoi == aoiToCheck;
                     });
}

bool ScmCommons::IsSurroundingAreaOfInterestVector(AreaOfInterest aoiToCheck)
{
  return std::any_of(begin(SURROUNDING_AREAS_OF_INTEREST_VECTOR),
                     end(SURROUNDING_AREAS_OF_INTEREST_VECTOR),
                     [aoiToCheck](const AreaOfInterest aoi)
                     {
                       return aoi == aoiToCheck;
                     });
}

std::string ScmCommons::AoisToString(const std::vector<AreaOfInterest>& aois)
{
  if (aois.empty())
  {
    return "";
  }

  std::ostringstream ss;
  for (auto it = begin(aois); it != end(aois); ++it)
  {
    ss << AreaOfInterestToString(*it);
    if (it != end(aois) - 1)
    {
      ss << ';';
    }
  }

  return ss.str();
}

//! @brief Method which checks if vehicle classification is marked as the car
//!
//! @param vehicleClassification Vehicle classification
//! @return true if vehicle classification is marked as the car
bool ScmCommons::IsCar(scm::common::VehicleClass vehicleClassification)
{
  if (vehicleClassification == scm::common::VehicleClass::kSmall_car || vehicleClassification == scm::common::VehicleClass::kCompact_car || vehicleClassification == scm::common::VehicleClass::kMedium_car || vehicleClassification == scm::common::VehicleClass::kLuxury_car)
  {
    return true;
  }
  return false;
}

//! @brief Method which checks if vehicle classification is marked as the truck
//!
//! @param vehicleClassification Vehicle classification
//! @return true if vehicle classification is marked as the truck
bool ScmCommons::IsTruck(scm::common::VehicleClass vehicleClassification)
{
  if (vehicleClassification == scm::common::VehicleClass::kHeavy_truck || vehicleClassification == scm::common::VehicleClass::kSemitrailer)
  {
    return true;
  }
  return false;
}

Side ScmCommons::RelativeLaneToSide(RelativeLane relativeLane)
{
  if (relativeLane == RelativeLane::RIGHT || relativeLane == RelativeLane::RIGHTRIGHT)
    return Side::Right;
  else if (relativeLane == RelativeLane::LEFT || relativeLane == RelativeLane::LEFTLEFT)
    return Side::Left;
  else
    throw std::runtime_error("ScmCommons::RelativeLaneToSide: EgoLane is not a valid Side lane.");
}

RelativeLane ScmCommons::SideToRelativeLane(Side side)
{
  return side == Side::Left ? RelativeLane::LEFT : RelativeLane::RIGHT;
}
