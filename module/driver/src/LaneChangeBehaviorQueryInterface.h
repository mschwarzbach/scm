/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  LaneChangeBehaviorQueryInterface.h

#pragma once

#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicles.h"

struct ComponentsForLaneChangeIntensity
{
  bool isTimeAtTargetSpeed = false;
  bool isDistanceToEndOfLaneSmallerOrEqToInfluencingDistance = false;
  bool isSlowBlocker = false;
};

class LaneChangeBehaviorQueryInterface
{
public:
  virtual ~LaneChangeBehaviorQueryInterface() = default;

  //! \brief Calculate the lane change intensity for the given lane.
  //! Calculates the lane change intensity based on ego speed and the speed on the given lane.
  //! \param [in] targetLane Lane for which to calculate lane change intensity
  //! \param [in] struct ComponentsForLaneChangeIntensity with 3 boolean flags(isTimeAtTargetSpeed, isDistanceToEndOfLaneSmallerOrEqToInfluencingDistance,isSlowBlocker)
  //! \return Lane change intensity between 0. and 1.
  virtual double CalculateLaneChangeIntensity(SurroundingLane targetLane, ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity) const = 0;

  //! \brief Adjust the lane convenience due to a highway exit based on the urgency.
  //! \param intensities Lane conveniences of the surrounding lanes
  //! \return Adjusted lane conveniences.
  virtual LaneChangeIntensities AdjustLaneConvenienceDueToHighwayExit(const LaneChangeIntensities& inIntensities, bool isRightHandTraffic) const = 0;

  //! \brief Checks if there is an obstacle in front or to the side of ego on the given lane.
  //! \param [in] targetLane Lane that shold be checked for obstacles
  //! \return True if an obstacle is present.
  virtual bool HasObstacleOnLane(SurroundingLane targetLane) const = 0;

  //! \brief Determines if the agent should change lanes in case of jam.
  //! \param [in] targetLane Lane that should be checked
  //! \return True if a lane change should be executed, i.e. laneChangeIntensity=1.
  virtual bool ShouldChangeLaneInJam(SurroundingLane targetLane) const = 0;

  //! \brief Calculates the relative net distance for the given lane.
  //! \param [in] targetLane Lane for which the relative net distance is returned.
  //! \return Zero if a vehicle is in the side AOI of the lane, the relative net distance to the vehicle if it is in front of the ego, Infinity otherwise.
  virtual units::length::meter_t GetRelativeNetDistance(SurroundingLane targetLane) const = 0;

  //! \brief Adjust the lane convenience when driving in a jam.
  //! \param intensities       Lane conveniences of the surrounding lanes
  //! \return Adjusted lane conveniences
  virtual LaneChangeIntensities AdjustLaneChangeIntensitiesForJam(const LaneChangeIntensities& inIntensities) const = 0;

  //! \brief Checks if a lane change to the given lane is possible.
  //! Uses lane type information, desired velocity of ego and legal velocity of lane, time at target speed in the lane as well as obstacle checks to determine if a lane change is possible.
  //! \param [in] targetLane Lane that should be checked.
  //! \return True if the lane change safety checks are succesful.
  virtual bool IsLaneChangeSave(SurroundingLane targetLane) const = 0;

  //! \brief Determine the lane change wish from the intensities for ego, left and right lane.
  //! Internal helper function for GetLaneChangeWish().
  //! \return The lane with the highest lane change intensity
  virtual SurroundingLane DetermineLaneChangeWishFromIntensities(const LaneChangeIntensities& intensities) const = 0;

  //! \brief Determine if the given lane exists and is wide enough for ego.
  virtual bool EgoFitsInLane(SurroundingLane targetLane) const = 0;

  //! \brief Wrapper for FeatureExtractor::HasJamVelocityEgo
  virtual bool EgoBelowJamSpeed() const = 0;

  //! \brief Wrapper for FeatureExtractor::DoesOuterLaneOvertakingProhibitionApply(EGO_LANE)
  virtual bool IsOuterLaneOvertakingProhibited(bool rightHandTraffic) const = 0;

  virtual units::time::second_t GetTimeAtTargetSpeedInLane(SurroundingLane lane) const = 0;
  virtual bool CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface *vehicle) const = 0;
  virtual bool GetIsInfluencingDistanceViolated(const SurroundingVehicleInterface *vehicle) const = 0;

  //! \brief Wrapper for FeatureExtractor::IsLaneChangePermittedDueToLaneMarkings(AreaOfInterest aoi)
  virtual bool GetIsLaneChangePermittedDueToLaneMarkings(Side side) const = 0;

  //! \brief Normalization of LaneChangeIntensities between 0 to 1
  virtual LaneChangeIntensities NormalizeLaneChangeIntensities(const LaneChangeIntensities& intensities) const = 0;
};