/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Swerving.h"

#include <cmath>
#include <optional>

#include "LaneChangeBehavior.h"
#include "LaneChangeDimension.h"
#include "LaneQueryHelper.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

Swerving::Swerving(MentalModelInterface& mentalModel)
    : _mentalModel(mentalModel)
{
}

bool Swerving::IsSwervingSafe(units::length::meter_t lateralEvadeDistance, Side side, bool isUrgentSwerving) const
{
  units::length::meter_t distanceToBoundaryEgo{0_m};
  std::optional<units::length::meter_t> distanceToBoundarySide;
  std::optional<units::length::meter_t> distanceToBoundaryFront;

  AreaOfInterest aoi_side{side == Side::Left ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE};
  AreaOfInterest aoi_front{side == Side::Left ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT};
  const int sideAoiIndex = _mentalModel.DetermineIndexOfSideObject(aoi_side,
                                                                   SideAoiCriteria::MIN,
                                                                   [](const SurroundingVehicleInterface* vehicle)
                                                                   { return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value()); });

  if (side == Side::Left)
  {
    distanceToBoundaryEgo = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryLeft;
    if (_mentalModel.GetIsVehicleVisible(aoi_side, sideAoiIndex))
    {
      distanceToBoundarySide = _mentalModel.GetDistanceToBoundaryRight(aoi_side, sideAoiIndex);
    }

    if (_mentalModel.GetIsVehicleVisible(aoi_front))
    {
      distanceToBoundaryFront = _mentalModel.GetDistanceToBoundaryRight(aoi_front);
    }
  }
  else
  {
    distanceToBoundaryEgo = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryRight;
    if (_mentalModel.GetIsVehicleVisible(aoi_side, sideAoiIndex))
    {
      distanceToBoundarySide = _mentalModel.GetDistanceToBoundaryLeft(aoi_side, sideAoiIndex);
    }

    if (_mentalModel.GetIsVehicleVisible(aoi_front))
    {
      distanceToBoundaryFront = _mentalModel.GetDistanceToBoundaryLeft(aoi_front);
    }
  }

  // Calculate lateralSafetyDistance according to comfort or emergency swerving
  // Emergency
  units::length::meter_t lateralSafetyDistance{_mentalModel.GetDriverParameters().lateralSafetyDistanceForEvading};

  // Comfort
  if (!isUrgentSwerving)
  {
    lateralSafetyDistance = 0._m;  // Already included in lateralEvadeDistance at comfort swerving
  }

  // Evading by using own lane
  // Emergency and Comfort Swerving (different lateralSafetyDistances and lateralEvadeDistances)
  if (distanceToBoundaryEgo > 0._m && (distanceToBoundaryEgo - lateralSafetyDistance > units::math::fabs(lateralEvadeDistance)))
  {
    return true;
  }

  const auto relativeLane = LaneQueryHelper::GetRelativeLaneFromAoi(aoi_side);
  // Evading by using neigbouring lane (only urgent swerving)
  // Don't crash into other vehicles on neighbouring lane (if there is a neighbouring lane)
  if (isUrgentSwerving && _mentalModel.GetLaneExistence(relativeLane, true))
  {
    if (distanceToBoundarySide.has_value() && distanceToBoundaryFront.has_value())
    {
      if ((distanceToBoundaryEgo + distanceToBoundarySide.value() - lateralSafetyDistance > units::math::fabs(lateralEvadeDistance)) &&  // Vehicle to the side will not be touched
          (distanceToBoundaryEgo + distanceToBoundaryFront.value() - lateralSafetyDistance > units::math::fabs(lateralEvadeDistance)))   // Vehicle to the front side will not be touched
      {
        return true;
      }
    }
  }

  return false;
}

bool Swerving::IsPlannedSwervingPossible(AreaOfInterest aoi) const
{
  // Functionality only for Comfort Swerving
  if (!LaneQueryHelper::IsFrontArea(aoi))
  {
    return false;
  }

  // Check if Swerving is possible  (depending on lateral condition)
  auto requiredLateralDistanceLeft = DetermineLateralDistanceToEvadeComfort(aoi, Side::Left);
  auto requiredLateralDistanceRight = DetermineLateralDistanceToEvadeComfort(aoi, Side::Right);

  const auto distanceToLaneBoundaryLeft{units::math::fabs(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryLeft)};
  const auto distanceToLaneBoundaryRight{units::math::fabs(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->distanceToLaneBoundaryRight)};

  const auto relativeNetDistance{_mentalModel.GetRelativeNetDistance(aoi)};

  // No Swerving, if neither left nor right distance to lane boundary is big enough
  if (requiredLateralDistanceLeft > distanceToLaneBoundaryLeft && requiredLateralDistanceRight > distanceToLaneBoundaryRight)
  {
    _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->distanceForPlannedSwerving = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
    _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->lateralEvadeDistanceForPlannedSwerving = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
    return false;
  }

  // -> Planned Swerving is possible (depending on lateral condition)
  // Take possible and smallest swerving side

  units::length::meter_t possibleLateralSwervingDistance{0._m};

  // Take right swerving distance if left is not possible
  if (requiredLateralDistanceLeft > distanceToLaneBoundaryLeft)
  {
    possibleLateralSwervingDistance = -requiredLateralDistanceRight;
  }
  // Take left swerving distance if right is not possible
  else if (requiredLateralDistanceRight > distanceToLaneBoundaryRight)
  {
    possibleLateralSwervingDistance = requiredLateralDistanceLeft;
  }
  // Take smallest swerving distance if both is possible
  else if (requiredLateralDistanceRight > requiredLateralDistanceLeft)
  {
    possibleLateralSwervingDistance = requiredLateralDistanceLeft;
  }
  else
  {
    possibleLateralSwervingDistance = -requiredLateralDistanceRight;
  }

  // Check if Swerving is possible (depending on longitudinal condition)
  // Swerving inside lane -> use comfort acceleration
  const auto accComfort = _mentalModel.GetComfortLongitudinalAcceleration();

  LaneChangeDimension laneChangeDimension(_mentalModel);

  // Calculate swerving length and width
  _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->lateralEvadeDistanceForPlannedSwerving = possibleLateralSwervingDistance;
  const auto evadingLength = laneChangeDimension.EstimateLaneChangeLength(possibleLateralSwervingDistance, accComfort, _mentalModel.GetLongitudinalVelocityEgo());
  _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->distanceForPlannedSwerving = evadingLength;

  // Check if Planned Swerving is possible (depending on longitudinal condition)
  return evadingLength < relativeNetDistance;
}

bool Swerving::HasSwervingTargetBeenPassed() const
{
  bool targetPassed = false;
  std::vector<AreaOfInterest> fieldConsideredAsPassed = {AreaOfInterest::EGO_REAR, AreaOfInterest::LEFT_REAR, AreaOfInterest::RIGHT_REAR};
  for (AreaOfInterest aoi : fieldConsideredAsPassed)
  {
    if (_mentalModel.GetAgentId(aoi) == GetAgentIdOfSwervingTarget())
    {
      targetPassed = true;
    }
  }

  return targetPassed;
}

bool Swerving::DeterminePossibilityOfEvadingBySwerving() const
{
  const Situation situation{_mentalModel.GetCurrentSituation()};

  if (!(situation == Situation::OBSTACLE_ON_CURRENT_LANE || situation == Situation::FOLLOWING_DRIVING))
  {
    return false;
  }

  const AreaOfInterest aoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
  const bool isPossibleDueToGeometry{IsPlannedSwervingPossible(aoi)};
  const bool isPossibleDueToTraffic{true};  // Extend when swerving outside of ego lane
  const auto velocityDelta{_mentalModel.GetLongitudinalVelocityDelta(aoi)};
  const auto velocityEgo{_mentalModel.GetAbsoluteVelocityEgo(true)};
  const bool isPossibleDueToDeltaVelocity{-velocityDelta > 0.1 * velocityEgo};  // Negative delta since objects are approaching

  return isPossibleDueToGeometry && isPossibleDueToTraffic && isPossibleDueToDeltaVelocity;
}

units::length::meter_t Swerving::DetermineLateralDistanceToEvade(AreaOfInterest aoi, Side side, bool isUrgentSwerving, int sideAoiIndex) const
{
  // If ActionState is comfort, use different calculation
  if (!isUrgentSwerving)
  {
    return DetermineLateralDistanceToEvadeComfort(aoi, side);
  }

  const auto* vehicle{_mentalModel.GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  auto obstructionDistance = 0_m;
  if (side == Side::Left)
  {
    obstructionDistance = units::math::max(0._m, ObstructionScm(vehicle->GetLateralObstruction().GetValue()).left);
  }
  else if (side == Side::Right)
  {
    obstructionDistance = units::math::max(0._m, ObstructionScm(vehicle->GetLateralObstruction().GetValue()).right);
  }
  else
  {
    throw std::runtime_error("Swerving - DetermineLateralDistanceToEvade: Unexpected Side!");
  }
  return static_cast<units::length::meter_t>(obstructionDistance) + _mentalModel.GetDriverParameters().lateralSafetyDistanceForEvading;
}

units::length::meter_t Swerving::DetermineLateralDistanceToEvadeComfort(AreaOfInterest aoi, Side side) const
{
  units::length::meter_t distanceToEvade{};
  if (side == Side::Right)
  {
    distanceToEvade = units::math::fabs(_mentalModel.GetObstruction(aoi).right);
  }
  else
  {
    distanceToEvade = units::math::fabs(_mentalModel.GetObstruction(aoi).left);
  }

  const auto velocity{_mentalModel.GetAbsoluteVelocityEgo(true)};
  constexpr auto minSafetyDistance = 0.2_m;
  constexpr auto maxSafetyDistance = 1._m;
  const auto velocityForMaxDistance = (units::velocity::meters_per_second_t)100_kph;  // Velocity at which the maximum safety distance is reached
  const units::length::meter_t lateralSafetyDistance{units::math::max((maxSafetyDistance - minSafetyDistance) / velocityForMaxDistance *
                                                                          units::math::min(velocity, velocityForMaxDistance),
                                                                      minSafetyDistance)};
  return lateralSafetyDistance + distanceToEvade;
}

units::time::second_t Swerving::GetSwervingTime(AreaOfInterest aoi, Side side, bool isUrgentSwerving) const
{
  const units::length::meter_t lateralDistanceToTravelEvading = DetermineLateralDistanceToEvade(aoi, side, isUrgentSwerving);
  const units::acceleration::meters_per_second_squared_t possibleAcceleration = isUrgentSwerving ? _mentalModel.GetDriverParameters().maximumLateralAcceleration : _mentalModel.GetDriverParameters().comfortLateralAcceleration;
  const auto timeToEvade_Sq = 2. * units::math::fabs(lateralDistanceToTravelEvading) / possibleAcceleration;

  const auto timeToCollision_Sq{units::math::pow<2>(_mentalModel.GetTtc(aoi))};

  const bool evading{timeToEvade_Sq < timeToCollision_Sq && timeToEvade_Sq > (0_s * 0_s)};

  if (evading && IsSwervingSafe(lateralDistanceToTravelEvading, side, isUrgentSwerving))
  {
    return units::math::sqrt(timeToEvade_Sq);
  }

  return -999._s;
}

void Swerving::SetSwervingParameters(AreaOfInterest aoiCollision)
{
  SetAgentIdOfSwervingTarget(_mentalModel.GetAgentId(aoiCollision));
  SetLaneIdAtStartOfSwervingManeuver(_mentalModel.GetLaneId());
}

std::unordered_map<LateralAction, units::time::second_t, LateralActionHash> Swerving::GetSwervingTimes(AreaOfInterest aoi, bool isUrgentSwerving) const
{
  LateralAction::State actionState = isUrgentSwerving ? LateralAction::State::URGENT_SWERVING : LateralAction::State::COMFORT_SWERVING;
  LateralAction swerving_left(actionState, LateralAction::Direction::LEFT);
  LateralAction swerving_right(actionState, LateralAction::Direction::RIGHT);

  if (_mentalModel.GetObstruction(aoi).isOverlapping)
  {
    return {
        {swerving_left, GetSwervingTime(aoi, Side::Left, isUrgentSwerving)},
        {swerving_right, GetSwervingTime(aoi, Side::Right, isUrgentSwerving)}};
  }

  return {
      {swerving_left, units::make_unit<units::time::second_t>(ScmDefinitions::DEFAULT_VALUE)},
      {swerving_right, units::make_unit<units::time::second_t>(ScmDefinitions::DEFAULT_VALUE)}};
}

units::time::second_t Swerving::DetermineTimeToSwervePast(AreaOfInterest aoi, int sideAoiIndex) const
{
  if (LaneQueryHelper::IsRearArea(aoi) || LaneQueryHelper::IsSideArea(aoi))
  {
    throw std::runtime_error("MentalCalculations - DetermineTimeToSwervePast: Not a front area!");
  }

  const auto distanceToPass{_mentalModel.GetLongitudinalObstruction(aoi, sideAoiIndex == -1 ? 0 : sideAoiIndex).front};  // HOTFIX SideAoiIndex hotfix until all getters are changed to use zero indexing
  const auto velocityDelta{-_mentalModel.GetLongitudinalVelocityDelta(aoi, sideAoiIndex)};                               // approching = negative value
  auto timeToPass{distanceToPass / velocityDelta};

  if (timeToPass < 0._s && _mentalModel.GetLateralAction().state == LateralAction::State::URGENT_SWERVING)
  {
    timeToPass = 10._s;
  }

  return timeToPass;
}

void Swerving::DetermineSwervingIntensities(AreaOfInterest aoi, bool isUrgentSwerving, std::unordered_map<LateralAction, double, LateralActionHash>& actionPatternIntensities)
{
  auto swerving = GetSwervingTimes(aoi, isUrgentSwerving);

  // Set lateralActionState depending on urgency
  const LateralAction::State swervingState = isUrgentSwerving ? LateralAction::State::URGENT_SWERVING : LateralAction::State::COMFORT_SWERVING;
  LateralAction swerving_left(swervingState, LateralAction::Direction::LEFT);
  LateralAction swerving_right(swervingState, LateralAction::Direction::RIGHT);

  auto timeSwervingLeft = std::max(swerving.at(swerving_left), 0._s);
  auto timeSwervingRight = std::max(swerving.at(swerving_right), 0._s);

  if (timeSwervingLeft > 0._s && timeSwervingRight == 0._s)
  {
    actionPatternIntensities[swerving_left] = 1.;
    actionPatternIntensities[swerving_right] = 0.;
    actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 0.;
    SetSwervingParameters(aoi);
  }
  else if (timeSwervingRight > 0._s && timeSwervingLeft == 0._s)
  {
    actionPatternIntensities[swerving_left] = 0.;
    actionPatternIntensities[swerving_right] = 1.;
    actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 0.;
    SetSwervingParameters(aoi);
  }
  else if (timeSwervingLeft > 0._s || timeSwervingRight > 0._s)
  {
    actionPatternIntensities[swerving_left] = timeSwervingRight / (timeSwervingLeft + timeSwervingRight);
    actionPatternIntensities[swerving_right] = timeSwervingLeft / (timeSwervingLeft + timeSwervingRight);
    actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 0.;
    SetSwervingParameters(aoi);
  }
  else
  {
    actionPatternIntensities[swerving_left] = 0.;
    actionPatternIntensities[swerving_right] = 0.;
    actionPatternIntensities[LateralAction(LateralAction::State::LANE_KEEPING)] = 1.;
  }
}

AreaOfInterest Swerving::DetermineAoiForSwervingReturnParameters(const TrajectoryCalculations::TrajectoryDimensions& dimensions) const
{
  AreaOfInterest aoi;
  const auto halfWidthEgo{_mentalModel.GetVehicleWidth() / 2.};

  if (dimensions.width > 0._m && dimensions.width > _mentalModel.GetDistanceToBoundaryLeft() + halfWidthEgo)
    aoi = AreaOfInterest::LEFT_FRONT;

  else if (dimensions.width < 0._m && dimensions.width < -_mentalModel.GetDistanceToBoundaryRight() - halfWidthEgo)
    aoi = AreaOfInterest::RIGHT_FRONT;

  else
    aoi = AreaOfInterest::EGO_FRONT;

  if (NoVehicleInFrontSideLaneVisible(aoi))
    aoi = AreaOfInterest::EGO_FRONT;  // Use ego front as default

  return aoi;
}

bool Swerving::NoVehicleInFrontSideLaneVisible(const AreaOfInterest& aoi) const
{
  return (aoi == AreaOfInterest::LEFT_FRONT || aoi == AreaOfInterest::RIGHT_FRONT) && !_mentalModel.GetIsVehicleVisible(aoi);
}

units::length::meter_t Swerving::DetermineSwervingReturnDistance(const FeatureExtractorInterface &featureExtractor, const MentalCalculationsInterface &mentalCalculations) const
{
  auto currentLateralPosition{_mentalModel.GetLateralPosition()};

  const auto laneWidthEgo{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width};
  const auto laneWidthLeft{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Left().width};
  const auto laneWidthRight{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->Right().width};
  const auto lateralOffset{mentalCalculations.CalculateLateralOffsetNeutralPositionScaled()};
  LateralActionQuery action(_mentalModel.GetLateralAction());
  const bool swervingLeft{action.IsSwervingLeft()};

  auto distanceToEgoLaneCenter = -currentLateralPosition;
  auto distanceToLeftLanePosition = (laneWidthEgo + laneWidthLeft + 2.0 * lateralOffset) / 2.0 - currentLateralPosition;
  auto distanceToRightLanePosition = -(laneWidthEgo + laneWidthRight + 2.0 * lateralOffset) / 2.0 - currentLateralPosition;

  units::length::meter_t distanceToStartLaneCenter{ScmDefinitions::DEFAULT_VALUE};
  units::length::meter_t distanceToSideLaneCenter{ScmDefinitions::DEFAULT_VALUE};
  bool isStartLaneSafe{false};
  bool isSideLaneSafe{false};
  std::optional<units::time::second_t> ttcStartLane;
  auto ttcSideLane{ScmDefinitions::INF_TIME};

  // Analysis of situation and calculation of variables
  if (GetLaneIdAtStartOfSwervingManeuver() == _mentalModel.GetLaneId())  // Agent hasn't left it's starting lane -> Returning to start lane = EGO
  {
    distanceToStartLaneCenter = distanceToEgoLaneCenter;
    isStartLaneSafe = !featureExtractor.IsCollisionCourseDetected(_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT));
    ttcStartLane = _mentalModel.GetTtc(AreaOfInterest::EGO_FRONT);

    if (swervingLeft)
    {
      distanceToSideLaneCenter = distanceToLeftLanePosition;
      isSideLaneSafe = featureExtractor.HasSideLaneNoCollisionCourses(Side::Left);
      if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE))
      {
        ttcSideLane = 0._s;
      }
      else
      {
        ttcSideLane = _mentalModel.GetTtc(AreaOfInterest::LEFT_FRONT);
      }
    }
    else  // Swerving right
    {
      distanceToSideLaneCenter = distanceToRightLanePosition;
      isSideLaneSafe = featureExtractor.HasSideLaneNoCollisionCourses(Side::Right);
      if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE))
      {
        ttcSideLane = 0._s;
      }
      else
      {
        ttcSideLane = _mentalModel.GetTtc(AreaOfInterest::RIGHT_FRONT);
      }
    }
  }
  else  // Agent has left his starting lane -> Returning to start lane = opposite of swerving direction
  {
    distanceToSideLaneCenter = distanceToEgoLaneCenter;
    isSideLaneSafe = !featureExtractor.IsCollisionCourseDetected(_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT)) && _mentalModel.GetLaneExistence(RelativeLane::EGO);
    ttcSideLane = _mentalModel.GetTtc(AreaOfInterest::EGO_FRONT);

    if (swervingLeft)
    {
      distanceToStartLaneCenter = distanceToRightLanePosition;
      isStartLaneSafe = !featureExtractor.IsCollisionCourseDetected(_mentalModel.GetVehicle(AreaOfInterest::RIGHT_FRONT));
      ttcStartLane = _mentalModel.GetTtc(AreaOfInterest::RIGHT_FRONT);
    }
    else  // Swerving right
    {
      distanceToStartLaneCenter = distanceToLeftLanePosition;
      isStartLaneSafe = !featureExtractor.IsCollisionCourseDetected(_mentalModel.GetVehicle(AreaOfInterest::LEFT_FRONT));
      ttcStartLane = _mentalModel.GetTtc(AreaOfInterest::LEFT_FRONT);
    }
  }

  units::length::meter_t distanceToReturn{ScmDefinitions::DEFAULT_VALUE};
  // Decision making based on the situation
  if (isStartLaneSafe && !isSideLaneSafe)
  {
    distanceToReturn = distanceToStartLaneCenter;
  }
  else if (!isStartLaneSafe && isSideLaneSafe)
  {
    distanceToReturn = distanceToSideLaneCenter;
  }
  else if (isStartLaneSafe && isSideLaneSafe)
  {
    if (units::math::fabs(distanceToStartLaneCenter) <= units::math::fabs(distanceToSideLaneCenter))
    {
      distanceToReturn = distanceToStartLaneCenter;
    }
    else
    {
      distanceToReturn = distanceToSideLaneCenter;
    }
  }
  else  // both lanes are not safe
  {
    if (ttcStartLane >= ttcSideLane)
    {
      distanceToReturn = distanceToStartLaneCenter;
    }
    else
    {
      distanceToReturn = distanceToSideLaneCenter;
    }
  }

  return distanceToReturn;
}

void Swerving::UpdateAndCheckForEndOfSwerving(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, units::time::millisecond_t cycleTime)
{
  const units::time::second_t dt = cycleTime;

  _mentalModel.SetRemainInSwervingState(true);
  auto currentWidthTraveled = _mentalModel.GetLaneChangeWidthTraveled();
  currentWidthTraveled += dt * LateralVelocity;
  _mentalModel.SetLaneChangeWidthTraveled(currentWidthTraveled);

  SetHasSwervingStateSwitchedToReturning(false);

  if (units::math::fabs(currentWidthTraveled) >= units::math::fabs(.95 * LaneChangeWidth))
  {
    if (_swervingState == SwervingState::EVADING)
    {
      _swervingState = SwervingState::DEFUSING;
      _mentalModel.SetLaneChangeWidthTraveled(0.0_m);
      return;
    }
    if (_swervingState == SwervingState::RETURNING)
    {
      _swervingState = SwervingState::NONE;
      _mentalModel.SetLaneChangeWidthTraveled(0.0_m);
      _mentalModel.SetRemainInSwervingState(false);
      ResetAgentIdOfSwervingTarget();
      _mentalModel.SetIsEndOfLateralMovement(true);
      SetAoiToSwerve(AreaOfInterest::NumberOfAreaOfInterests);
    }
  }

  if (_swervingState == SwervingState::DEFUSING && HasSwervingTargetBeenPassed())
  {
    _swervingState = SwervingState::RETURNING;
    _mentalModel.SetLaneChangeWidthTraveled(0.0_m);
    SetHasSwervingStateSwitchedToReturning(true);
  }

  // action needs to evaluated again, though reactionBaseTime might not be done
  _mentalModel.SetReactionBaseTime(0.0_s);
}

SwervingState Swerving::GetSwervingState() const
{
  return _swervingState;
}

void Swerving::SetSwervingState(SwervingState state)
{
  _swervingState = state;
}

bool Swerving::IsPastSwervingEndTime() const
{
  return _swervingEndTime <= _mentalModel.GetTime();
}

void Swerving::SetSwervingEndTime(units::time::millisecond_t time)
{
  if (time <= _mentalModel.GetTime())
  {
    throw std::runtime_error("Swerving::SetSwervingEndTime - End time must be in the future!");
  }

  _swervingEndTime = time;
}

units::time::millisecond_t Swerving::GetSwervingEndTime() const
{
  return _swervingEndTime;
}

std::pair<AreaOfInterest, bool> Swerving::GetAoiToSwerve() const
{
  return _aoiToSwerve;
}

void Swerving::SetAoiToSwerve(AreaOfInterest aoi)
{
  const bool hasChanged{aoi != _aoiToSwerve.first};
  _aoiToSwerve = std::pair<AreaOfInterest, bool>{aoi, hasChanged};
}

bool Swerving::IsSwervingPlanned() const
{
  if (LaneQueryHelper::IsFrontArea(GetAoiToSwerve().first))
  {
    return true;
  }
  else  // NumberOfAreaOfInterest means no, all other AOI are not valid at the moment
  {
    return false;
  }
}

void Swerving::ResetAgentIdOfSwervingTarget()
{
  SetAgentIdOfSwervingTarget(-99);
}

void Swerving::SetLaneIdAtStartOfSwervingManeuver(int id)
{
  _laneIdAtStartOfSwerving = id;
}

int Swerving::GetLaneIdAtStartOfSwervingManeuver() const
{
  return _laneIdAtStartOfSwerving;
}

void Swerving::SetAgentIdOfSwervingTarget(int id)
{
  _agentIdOfSwervingTarget = id;
}

int Swerving::GetAgentIdOfSwervingTarget() const
{
  return _agentIdOfSwervingTarget;
}

bool Swerving::GetRemainInSwervingState() const
{
  return _remainInSwervingState;
}

void Swerving::SetRemainInSwervingState(bool remain)
{
  _remainInSwervingState = remain;
}

void Swerving::SetHasSwervingStateSwitchedToReturning(bool switchedToReturning)
{
  _hasSwervingStateSwitchedToReturning = switchedToReturning;
}

bool Swerving::HasSwervingStateSwitchedToReturning() const
{
  return _hasSwervingStateSwitchedToReturning;
}

void Swerving::CheckForPlannedEvading()
{
  const Situation currentSituation{_mentalModel.GetCurrentSituation()};
  const LateralActionQuery action{_mentalModel.GetLateralAction()};

  const bool isRelevantSituation{currentSituation == Situation::FOLLOWING_DRIVING || currentSituation == Situation::OBSTACLE_ON_CURRENT_LANE};
  const bool isRelevantActionState{action.IsIntentActive() || action.IsLaneKeeping()};

  if (!isRelevantSituation || !isRelevantActionState)
  {
    SetAoiToSwerve(AreaOfInterest::NumberOfAreaOfInterests);  // Reset AoiToSwerve if we are in no relevant sitatuation
    return;
  }

  const AreaOfInterest causingAoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
  if (units::math::abs(_mentalModel.GetLateralVelocity(causingAoi)) < 0.1_mps)
  {
    const bool isEvadingPossible = DeterminePossibilityOfEvadingBySwerving();

    if (isEvadingPossible)
    {
      SetAoiToSwerve(causingAoi);
    }
    else
    {
      SetAoiToSwerve(AreaOfInterest::NumberOfAreaOfInterests);
    }
  }
}
