/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  Swerving.h

#pragma once

#include <climits>

#include "MentalModelInterface.h"
#include "SwervingInterface.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"

class Swerving : public SwervingInterface
{
public:
  Swerving(MentalModelInterface& mentalModel);

  bool IsSwervingSafe(units::length::meter_t lateralEvadeDistance, Side side, bool isUrgentSwerving) const override;
  bool IsPlannedSwervingPossible(AreaOfInterest aoi) const override;
  bool DeterminePossibilityOfEvadingBySwerving() const override;
  bool HasSwervingTargetBeenPassed() const override;

  units::length::meter_t DetermineLateralDistanceToEvade(AreaOfInterest aoi, Side side, bool isUrgentSwerving, int sideAoiIndex = 0) const override;
  units::length::meter_t DetermineLateralDistanceToEvadeComfort(AreaOfInterest aoi, Side side) const override;

  std::unordered_map<LateralAction, units::time::second_t, LateralActionHash> GetSwervingTimes(AreaOfInterest aoiCollision, bool isUrgentSwerving) const override;

  units::time::second_t DetermineTimeToSwervePast(AreaOfInterest aoi, int sideAoiIndex = -1) const override;

  void SetSwervingParameters(AreaOfInterest aoiCollision) override;
  void DetermineSwervingIntensities(AreaOfInterest aoi, bool isUrgentSwerving, std::unordered_map<LateralAction, double, LateralActionHash>& actionPatternIntensities) override;

  AreaOfInterest DetermineAoiForSwervingReturnParameters(const TrajectoryCalculations::TrajectoryDimensions& dimensions) const override;
  bool NoVehicleInFrontSideLaneVisible(const AreaOfInterest& aoi) const override;
  units::length::meter_t DetermineSwervingReturnDistance(const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations) const override;

  void UpdateAndCheckForEndOfSwerving(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, units::time::millisecond_t cycleTime) override;

  SwervingState GetSwervingState() const override;

  void SetSwervingState(SwervingState state) override;

  bool IsPastSwervingEndTime() const override;
  void SetSwervingEndTime(units::time::millisecond_t time) override;
  units::time::millisecond_t GetSwervingEndTime() const override;

  void ResetAgentIdOfSwervingTarget() override;

  void SetLaneIdAtStartOfSwervingManeuver(int id) override;

  int GetLaneIdAtStartOfSwervingManeuver() const override;

  void SetAgentIdOfSwervingTarget(int id) override;

  int GetAgentIdOfSwervingTarget() const override;

  bool GetRemainInSwervingState() const override;

  void SetRemainInSwervingState(bool remain) override;

  bool HasSwervingStateSwitchedToReturning() const override;

  void SetHasSwervingStateSwitchedToReturning(bool switchedToReturning) override;

  bool IsSwervingPlanned() const override;

  std::pair<AreaOfInterest, bool> GetAoiToSwerve() const override;

  void SetAoiToSwerve(AreaOfInterest aoi) override;
  void CheckForPlannedEvading() override;

private:
  //! Helper method for GetSwervingTimes calculating the intensity for a given side
  units::time::second_t GetSwervingTime(AreaOfInterest aoi, Side side, bool isUrgentSwerving) const;

  MentalModelInterface& _mentalModel;
  SwervingState _swervingState{SwervingState::NONE};
  std::pair<AreaOfInterest, bool> _aoiToSwerve{AreaOfInterest::NumberOfAreaOfInterests, false};
  units::time::millisecond_t _swervingEndTime{std::numeric_limits<double>::max()};

  //! Id of the agent responsible for the serving maneuver [-].
  int _agentIdOfSwervingTarget{-99};

  //! Id of current ego lane at the start of a swerving maneuver [-].
  int _laneIdAtStartOfSwerving{-99};

  //! The ego vehicle remains in swerving state.
  bool _remainInSwervingState{false};

  //! The swerving state has switched to returning.
  bool _hasSwervingStateSwitchedToReturning{false};
};
