/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  SwervingInterface.h

#pragma once

#include "FeatureExtractorInterface.h"
#include "MentalCalculationsInterface.h"
#include "ScmCommons.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "include/common/ScmEnums.h"

//! \ingroup SwervingInterface
class SwervingInterface
{
public:
  virtual ~SwervingInterface() = default;
  //! \brief Is swerving safe with respect to other vehicles
  //! \param [in] lateralEvadeDistance     Lateral displacement required for evasion
  //! \param [in] side                     Side to evade to
  //! \param [in] checkSafety         Check side lane safety or not
  virtual bool IsSwervingSafe(units::length::meter_t lateralEvadeDistance, Side side, bool isUrgentSwerving) const = 0;

  //! \brief Is planned swerving possible
  //! \param [in] aoi             Area of interest
  virtual bool IsPlannedSwervingPossible(AreaOfInterest aoi) const = 0;

  //! \brief Determine, if it is possible to evade by swerving
  virtual bool DeterminePossibilityOfEvadingBySwerving() const = 0;

  //! \brief Has the swerved target been completely passed by the ego vehicle
  virtual bool HasSwervingTargetBeenPassed() const = 0;

  //! \brief Determine the absolute lateral distance to evade observed target [m].
  //! \param [in] aoi              Observed area of interest
  //! \param [in] side             Side at which the object is to be evaded
  //! \param [in] isUrgentSwerving Indicator if urgent or comfort swerving distances are needed
  //! \param [in] sideAoiIndex     Index of the object in the side aoi vector
  //! \return Distance to evade (or zero if already evading with current distance)
  virtual units::length::meter_t DetermineLateralDistanceToEvade(AreaOfInterest aoi, Side side, bool isUrgentSwerving, int sideAoiIndex = -1) const = 0;

  //! \brief Determine the absolute lateral distance to evade observed target [m] for COMFORT.
  //! \param [in] aoi             Observed area of interest
  //! \param [in] side            Side at which the object is to be evaded
  //! \return Distance to evade (or zero if already evading with current distance)
  virtual units::length::meter_t DetermineLateralDistanceToEvadeComfort(AreaOfInterest aoi, Side side) const = 0;

  //! \brief Determine, if a swerving maneuver is possible in the current state and return the time to swerve for each side.
  //! \param [in] aoiCollision        Area of interest containing the object of interest
  //! \param [in] isUrgentSwerving    If Urgent Swerving -> use next lane if no crash
  //! \return swervingPossibilities   Map containing the possibility to swerve to the right and left of the observed object
  virtual std::unordered_map<LateralAction, units::time::second_t, LateralActionHash> GetSwervingTimes(AreaOfInterest aoi, bool isUrgentSwerving) const = 0;

  //! \brief Determine time to swerve
  //! \param [in] aoi             Area of interest
  //! \param [in] sideAoiIndex    Index of the object in the side aoi vector
  //! \return time in s
  virtual units::time::second_t DetermineTimeToSwervePast(AreaOfInterest aoi, int sideAoiIndex = -1) const = 0;

  //! \brief Set the parameters required to execute a swerving maneuver
  //! \param [in] aoiCollision    Area of interest containing the object of interest
  virtual void SetSwervingParameters(AreaOfInterest aoiCollision) = 0;

  //! \brief Determineintensities for swerving maneuvers
  //! \param aoi                  The area of interest that is checked for possible impending collisions
  //! \param isUrgentSwerving     Perform a safety check of the lane that might be evaded to or just a simple lane existence check
  virtual void DetermineSwervingIntensities(AreaOfInterest aoi, bool isUrgentSwerving, std::unordered_map<LateralAction, double, LateralActionHash>& actionPatternIntensities) = 0;

  virtual AreaOfInterest DetermineAoiForSwervingReturnParameters(const TrajectoryCalculations::TrajectoryDimensions& dimensions) const = 0;
  virtual bool NoVehicleInFrontSideLaneVisible(const AreaOfInterest& aoi) const = 0;
  virtual units::length::meter_t DetermineSwervingReturnDistance(const FeatureExtractorInterface& featureExtractor,
                                                                 const MentalCalculationsInterface& mentalCalculations) const = 0;

  virtual void UpdateAndCheckForEndOfSwerving(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, units::time::millisecond_t cycleTime) = 0;

  //! \brief Getter for swervingState.
  //! \return swervingState
  virtual SwervingState GetSwervingState() const = 0;

  //! \brief Setter for swervingState.
  //! \param state    Value of swervingState
  virtual void SetSwervingState(SwervingState state) = 0;

  virtual bool IsPastSwervingEndTime() const = 0;
  virtual void SetSwervingEndTime(units::time::millisecond_t time) = 0;
  virtual units::time::millisecond_t GetSwervingEndTime() const = 0;

  //! \brief Setter for the id of the swerving.
  //! \param [in]     Id of the swerving target
  virtual void SetAgentIdOfSwervingTarget(int id) = 0;
  virtual void ResetAgentIdOfSwervingTarget() = 0;

  //! \brief Set ID of current ego lane at the start of a swerving maneuver.
  //! \param [in] id      ID of current ego lane
  virtual void SetLaneIdAtStartOfSwervingManeuver(int id) = 0;

  //! \brief Get ID of current ego lane at the start of a swerving maneuver.
  //! \return laneIdAtStartOfSwerving
  virtual int GetLaneIdAtStartOfSwervingManeuver() const = 0;
  virtual void SetHasSwervingStateSwitchedToReturning(bool switchedToReturning) = 0;

  //! \brief Getter for the id of the swerving.
  //! \return agentIdOfSwervingTarget
  virtual int GetAgentIdOfSwervingTarget() const = 0;

  //! \brief Setter for remainInSwervingState.
  //! \param remain   Value of remainInSwervingState
  virtual void SetRemainInSwervingState(bool remain) = 0;

  //! \brief Getter for remainInSwervingState.
  //! \return remainInSwervingState
  virtual bool GetRemainInSwervingState() const = 0;

  virtual bool HasSwervingStateSwitchedToReturning() const = 0;
  virtual bool IsSwervingPlanned() const = 0;

  virtual std::pair<AreaOfInterest, bool> GetAoiToSwerve() const = 0;
  virtual void SetAoiToSwerve(AreaOfInterest aoi) = 0;
  virtual void CheckForPlannedEvading() = 0;
};
