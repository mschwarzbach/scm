/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  LaneChangeBehavior.h

#pragma once

#include <vector>

#include "../../parameterParser/src/TrafficRules/TrafficRules.h"
#include "AccelerationCalculations/AccelerationCalculationsQuery.h"
#include "FeatureExtractorInterface.h"
#include "LaneChangeBehaviorInterface.h"
#include "LaneChangeBehaviorQuery.h"
#include "MentalCalculationsInterface.h"
#include "ScmCommons.h"
#include "include/common/ScmDefinitions.h"

class MentalModelInterface;
/*! \addtogroup LaneChangeBehavior
 * @{
 * \brief contains the functionality necessary for the lane change behavior of the stochastic cognitive model (SCM).
 * \details This component implements the lane change behavior, containing functions to calculate lane change intensities, calculation of lane change wishes, etc.
 */
//! \ingroup LaneChangeBehavior
class LaneChangeBehavior : public LaneChangeBehaviorInterface
{
public:
  int static constexpr LANE_DENIED = -1;
  explicit LaneChangeBehavior(std::shared_ptr<LaneChangeBehaviorQueryInterface> query, const MentalModelInterface& mentalModel);

  SurroundingLane GetLaneChangeWish() const override;
  void CalculateLaneIntensities(const TrafficRulesScm& trafficRulesScm) override;
  double CalculateBaseLaneConvenience(SurroundingLane targetLane) const override;
  double GetLaneConvenience(SurroundingLane targetLane) const override;

protected:
  std::shared_ptr<LaneChangeBehaviorQueryInterface> _laneChangeBehaviorQuery;
  units::velocity::meters_per_second_t CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane targetLane) const;
  units::velocity::meters_per_second_t CalculateLongitudinalVelocityDelta(SurroundingLane targetLane) const;
  units::velocity::meters_per_second_t CalculateVelocityDeltaToSlowBlockerDelta(SurroundingLane targetLane) const;
  units::velocity::meters_per_second_t CalculateVelocityToLaneMeanDelta(SurroundingLane targetLane) const;
  double CheckLaneIsDenied(double intensity) const;
  double CheckIsReplacementsEmpty(const std::vector<units::velocity::meters_per_second_t>& replacements, double outIntensity) const;
  LaneChangeIntensities DetermineBaseLaneConvenienceFromParameters(LaneChangeIntensities inIntensities);
  LaneChangeIntensities _laneChangeIntensities{};
  units::velocity::meters_per_second_t FindMaximumReplacementVelocity(std::vector<units::velocity::meters_per_second_t> replacements) const;
  double CalculateIntensity(std::vector<units::velocity::meters_per_second_t> velocityAdjustments, units::velocity::meters_per_second_t maxAdjustment) const;
  bool IsTimeAtTargetSpeed(SurroundingLane targetLane) const;

private:
  const MentalModelInterface& _mentalModel;
  std::vector<units::velocity::meters_per_second_t> DetermineLaneConvenienceParameters(SurroundingLane targetLane);
  units::velocity::meters_per_second_t DetermineDeltaVelocityTimeAtTargetSpeedWithObstacleOnLane(SurroundingLane targetLane, units::time::second_t timeAtTargetSpeedInLane) const;
  units::velocity::meters_per_second_t DetermineDeltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance(SurroundingLane targetLane) const;
  units::time::second_t DetermineTimeToEndOfLane(SurroundingLane targetLane) const;
  double CalculateIntensityParameters(SurroundingLane targetLane) const;
  std::vector<units::velocity::meters_per_second_t> CalculateReplacementVelocities(double intensity, SurroundingLane targetLane);
  bool IsDistanceToEndOfLaneSmallerOrEqToInfluencingDistance(SurroundingLane targetLane) const;
  bool IsSlowBlocker(SurroundingLane targetLane) const;
  LaneChangeIntensities ApplyLaneBiasIntensity(const LaneChangeIntensities &inIntensities);
  static constexpr units::time::second_t MinimumTimeAtTargetSpeedThreshold{20._s};
};

inline auto CreateLaneChangeBehavior(const MentalCalculationsInterface& mentalCalculations, const MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor)
{
  auto query = std::make_shared<LaneChangeBehaviorQuery>(mentalCalculations, mentalModel, featureExtractor);
  return std::make_shared<LaneChangeBehavior>(query, mentalModel);
}

inline auto CreateAccelerationCalculationsQuery(const MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations)
{
  return std::make_shared<AccelerationCalculationsQuery>(mentalModel, mentalCalculations);
}
/** @} */  // End of group LaneChangeBehavior
