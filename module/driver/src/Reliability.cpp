/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Reliability.h"

#include <algorithm>

#include "GazeControl/GazeParameters.h"
#include "ScmCommons.h"
#include "ScmDefinitions.h"

double ReliabilityCalculations::CalculateReliability(units::angle::radian_t angleToOpticalAxis)
{
  static constexpr double MAX_RELIABILITY{100.0};
  static constexpr double UFOV_BORDER_RELIABILITY{DataQuality::MEDIUM};

  const double slope{(UFOV_BORDER_RELIABILITY - MAX_RELIABILITY) / (GazeParameters::HORIZONTAL_SIZE_UFOV / 2.0)};
  const auto value{units::math::abs(angleToOpticalAxis)};
  const double intersect{MAX_RELIABILITY};

  const double reliability{slope * value.value() + intersect};
  return std::max(UFOV_BORDER_RELIABILITY, reliability);
}

double ReliabilityCalculations::DecayReliability(double currentReliability, ParameterChangeRate changeRate, units::time::millisecond_t cycleTime)
{
  // decay from FOVEA perception to zero in 7 seconds(=70 Timesteps)
  const double DECAY_RATE{100 * cycleTime / 7000_ms};
  currentReliability -= DECAY_RATE;
  return std::max(currentReliability, 0.0);
}

std::map<VisualPerceptionSector, double> VisualReliabilityCalculations::CalculateSectorUpdates(units::angle::radian_t angle)
{
  const units::angle::degree_t angleDeg = angle;
  units::angle::degree_t SECTOR_SIZE = 90.0_deg;
  units::angle::degree_t PERIPHERY_SIZE = GazeParameters::HORIZONTAL_SIZE_PERIPHERY;
  units::angle::degree_t PERIPHERY_SECTOR_OVERLAP = units::math::abs(PERIPHERY_SIZE / 2 - SECTOR_SIZE);

  std::map<VisualPerceptionSector, double> updateMap;

  if (angleDeg <= SECTOR_SIZE / 2 && angleDeg >= 0.0_deg)
  {
    updateMap[VisualPerceptionSector::FRONT] = 100;

    if (angleDeg >= SECTOR_SIZE / 2 - PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::LEFT] = DataQuality::MEDIUM;
    }
  }

  if (angleDeg >= -SECTOR_SIZE / 2 && angleDeg < 0.0_deg)
  {
    updateMap[VisualPerceptionSector::FRONT] = 100;

    if (angleDeg <= -SECTOR_SIZE / 2 + PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::RIGHT] = DataQuality::MEDIUM;
    }
  }

  if (angleDeg > SECTOR_SIZE / 2 && angleDeg <= 180.0_deg - SECTOR_SIZE / 2)
  {
    updateMap[VisualPerceptionSector::LEFT] = 100;

    if (angleDeg <= SECTOR_SIZE / 2 + PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::FRONT] = DataQuality::MEDIUM;
    }

    if (angleDeg >= SECTOR_SIZE * 1.5 - PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::REAR] = DataQuality::MEDIUM;
    }
  }

  if (angleDeg < -SECTOR_SIZE / 2 && angleDeg >= -180.0_deg + SECTOR_SIZE / 2)
  {
    updateMap[VisualPerceptionSector::RIGHT] = 100;

    if (angleDeg >= -SECTOR_SIZE / 2 - PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::FRONT] = DataQuality::MEDIUM;
    }

    if (angleDeg <= -SECTOR_SIZE * 1.5 + PERIPHERY_SECTOR_OVERLAP)
    {
      updateMap[VisualPerceptionSector::REAR] = DataQuality::MEDIUM;
    }
  }

  if (units::math::abs(angleDeg) > 180.0_deg - SECTOR_SIZE / 2)
  {
    updateMap[VisualPerceptionSector::REAR] = 100;
  }

  return updateMap;
}
