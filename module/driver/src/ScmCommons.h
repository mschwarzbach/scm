/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ScmCommons.h

#pragma once

#include <sstream>

#include "../../../module/parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "LateralActionQuery.h"
#include "ScmDefinitions.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "include/common/VehicleProperties.h"

struct ObjectInformationSCM;

//! @brief Helper methods used by several classes within the SCM driver.
//! This component contains the helper methods which are used by several classes within the SCM-Driver
//! it calculates the critical values to check if a car crash is potential.
class ScmCommons
{
public:
  ScmCommons() = default;
  ScmCommons(const ScmCommons&) = delete;
  ScmCommons(ScmCommons&&) = delete;
  ScmCommons& operator=(const ScmCommons&) = delete;
  ScmCommons& operator=(ScmCommons&&) = delete;
  virtual ~ScmCommons() = default;

  //! @brief Convert gaze state direction to area of interest.
  //! @param state
  //! @return converted aoi
  static AreaOfInterest MapGazeStateToAreaOfInterest(GazeState state);

  //! @brief Convert area of interest to gaze state direction.
  //! @param aoi
  //! @return converted gazeState
  static GazeState MapAreaOfInterestToGazeState(AreaOfInterest aoi);

  //! @brief Convert area of interest in far region to corresponding area of interest closer to ego
  //! @param aoi
  //! @return converted aoi
  static AreaOfInterest ConvertFarAoiToCloseAoi(AreaOfInterest aoi);

  //! @brief Convert focus area to string.
  //! @param qualityArea
  //! @return converted string
  static std::string FieldOfViewAssignmentToString(FieldOfViewAssignment qualityArea);

  //! @brief Convert indicator state to string.
  //! @param indicator
  //! @return converted string
  static std::string IndicatorToString(scm::LightState::Indicator indicator);

  //! @brief Convert area of interest to string.
  //! @param aoi
  //! @return converted string
  static std::string AreaOfInterestToString(AreaOfInterest aoi);

  //! @brief High cognitive situation to string.
  //! @param situation
  //! @return converted string
  static std::string HighCognitiveSituationToString(HighCognitiveSituation situation);

  //! @brief Convert gaze state to string.
  //! @param gazeState
  //! @return converted string
  static std::string GazeStateToString(GazeState gazeState);

  //! @brief Convert boolean to string
  //! @param boolean
  //! @return converted string
  static std::string BooleanToString(bool boolean);

  //! @brief Convert lane change state to string.
  //! @param state
  //! @return converted string
  static std::string LaneChangeStateToString(LaneChangeState state);

  //! @brief Convert critical action state to string.
  //! @param state
  //! @return converted string
  static std::string CriticalActionStateToString(CriticalActionState state);

  //! @brief Convert situation to string.
  //! @param situation
  //! @return converted string
  static std::string SituationToString(Situation situation);

  //! @brief Convert action state to string.
  //! @param action
  //! @return converted string
  static std::string LateralActionToString(LateralAction action);

  //! @brief Convert lane type to string.
  //! @param type
  //! @return converted string
  static std::string LaneTypeToString(scm::LaneType type);

  //! @brief Convert swerving state to string.
  //! @param action
  //! @return converted string
  static std::string SwervingStateToString(SwervingState action);

  //! @brief Convert LaneChangeAction to sting.
  //! @param action
  //! @return converted string
  static std::string LaneChangeActionToString(LaneChangeAction action);

  //! @brief Convert action sub state to string.
  //! @param subAction
  //! @return converted string
  static std::string LongitudinalActionStateToString(LongitudinalActionState subAction);

  //! @brief Convert MinThwPerspective to string.
  //! @param perspective
  //! @return converted string
  static std::string MinThwPerspectiveToString(MinThwPerspective perspective);

  //! @brief Convert SurroundingLane to string.
  //! @param lane
  //! @return converted string
  static std::string SurroundingLaneToString(SurroundingLane lane);

  //! @brief Convert strings to AreaOfInterest.
  //! @param string
  //! @return converted aoi
  static AreaOfInterest StringToAreaOfInterest(std::string string);

  //! @brief Convert RelativeLongitudinalPosition to string.
  //! @param position
  //! @return converted string
  static std::string RelativeLongitudinalPositionToString(RelativeLongitudinalPosition position);

  //! @brief Convert vector of AreaOfInterest to string.
  //! @param aois
  //! @return converted string
  static std::string AoisToString(const std::vector<AreaOfInterest>& aois);

  //! @brief Perceive the time to collision / the optical variable Tau between the current agent and the vehicle associated to a specific AreaOfInterest
  //! @param vDeltaFrontMinusRear    Relative longitudinal velocity between the two agents (measured from leading vehicle to following vehicle) in m/s.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @param aoi                     AreaOfInterest associated to the agent under observation.
  //! @return Time to collision / Tau in s
  static units::time::second_t CalculateTTC(units::velocity::meters_per_second_t vDeltaFrontMinusRear, units::length::meter_t relativeNetDistance, AreaOfInterest aoi);

  //! @brief Perceive the time to collision / the optical variable Tau between the current agent and the vehicle associated to a specific AreaOfInterest
  //! @param vDeltaObservedMinusEgo    Relative longitudinal velocity between the two agents (measured from leading vehicle to following vehicle) in m/s.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @return Time to collision / Tau in s
  static units::time::second_t CalculateTimeToCollision(units::velocity::meters_per_second_t vDeltaObservedMinusEgo, units::length::meter_t relativeNetDistance);

  //! @brief Calculate perception threshold for the time to collision / the optical variable Tau according to the projected size and position of an object on the retina
  //! @param opticalAngleSizeHorizontal      Angular size of the object on driver's retina in rad.
  //! @param opticalAngleTowardsViewAxis     Angular position of the object on driver's retina in relation to view axis in rad.
  //! @param driverParameters                Struct of driver behaviour parameters
  //! @param applyCorticalMagnification      Apply deteriation of data due to spot on retina (false will use foveal perception at every spot)
  //! @return Perception threshold for the time to collision / the optical variable Tau in s
  static units::time::second_t CalculateTtcThreshold(units::angle::radian_t opticalAngleSizeHorizontal, units::angle::radian_t opticalAngleTowardsViewAxis, DriverParameters driverParameters, bool applyCorticalMagnification = true);

  //! @brief Perceive time derivative of the time to collision / the optical variable Tau between the current agent and the vehicle associated to a specific AreaOfInterest
  //! @param vDeltaFrontMinusRear    Relative longitudinal velocity between the two agents (measured from leading vehicle to following vehicle) in m/s.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @param aDeltaFrontMinusRear    Relative longitudinal acceleration between the two agents (measured from leading vehicle to following vehicle) in m/s².
  //! @param aoi                     AreaOfInterest associated to the agent under observation.
  //! @return Tau_dot
  static double CalculateTauDot(units::velocity::meters_per_second_t vDeltaFrontMinusRear, units::length::meter_t relativeNetDistance, units::acceleration::meters_per_second_squared_t aDeltaFrontMinusRear, AreaOfInterest aoi);

  //! @brief Perceive the net time headway between the current agent and the vehicle associated to a specific AreaOfInterest
  //! @param v_ego                   Longitudinal velocity of the current agent in m/s.
  //! @param v_observed              Longitudinal velocity of the agent under observation in m/s.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @param aoi                     AreaOfInterest associated to the agent under observation.
  //! @return Time headway in s
  static units::time::second_t CalculateNetGap(units::velocity::meters_per_second_t v_ego,
                                               units::velocity::meters_per_second_t v_observed,
                                               units::length::meter_t relativeNetDistance,
                                               AreaOfInterest aoi);

  //! @brief Perceive the derivative net time headway between the current agent and the vehicle associated to a specific AreaOfInterest
  //! @param v_ego                   Longitudinal velocity of the current agent in m/s.
  //! @param v_observed              Longitudinal velocity of the agent under observation in m/s.
  //! @param a_ego                   Longitudinal acceleration of the currentagent in m/s^2.
  //! @param a_observed              Longitudinal acceleration of the agent under observation in m/s^2.
  //! @param relativeNetDistance     Relative net distance between the two agents in m.
  //! @param vDelta                  Relative longitudinal velocity between the two agents (measured from leading vehicle to following vehicle) in m/s.
  //! @param aoi                     AreaOfInterest associated to the agent under observation.
  //! @return Derivative of Time headway
  static double CalculateNetGapDot(units::velocity::meters_per_second_t v_ego,
                                   units::velocity::meters_per_second_t v_observed,
                                   units::acceleration::meters_per_second_squared_t a_ego,
                                   units::acceleration::meters_per_second_squared_t a_observed,
                                   units::length::meter_t relativeNetDistance,
                                   units::velocity::meters_per_second_t vDelta,
                                   AreaOfInterest aoi);

  //! @brief Convert relativeLane to side
  //! @param relativeLane
  //! @return side, expect throw ego lane is given
  static Side RelativeLaneToSide(RelativeLane relativeLane);

  //! @brief Convert LaneExistence to string
  //! @param laneExistence
  //! @return string
  static std::string LaneExistenceToString(LaneExistence laneExistence);

  //! @brief Convert side to relativeLane
  //! @param side
  //! @return relativeLane
  static RelativeLane SideToRelativeLane(Side side);

  //! @brief is a function that assigns a value its sign
  //! @param val
  //! @return results can be: -1 or 0 or 1
  template <typename T>
  static int sgn(T val)
  {
    return (T(0) < val) - (val < T(0));
  }

  //! @brief RelativeLaneMapping
  static constexpr std::array<const char*, 5> RelativeLaneMapping{
      "LEFTLEFT",
      "LEFT",
      "EGO",
      "RIGHT",
      "RIGHTRIGHT"};

  //! @brief Convert a relativeLane to const char*
  //! @param relativeLane
  //! @return const char*
  static constexpr const char* to_cstr(RelativeLane relativeLane)
  {
    return RelativeLaneMapping[static_cast<size_t>(relativeLane)];
  }

  //! @brief Convert a relativeLane to const string
  //! @param relativeLane
  //! @return string
  static inline std::string to_string(RelativeLane relativeLane) noexcept
  {
    return std::string(to_cstr(relativeLane));
  }

  //! @brief LateralActionStateMapping
  static constexpr std::array<const char*, 8> LateralActionStateMapping{
      "LANE_KEEPING",
      "INTENT_TO_CHANGE_LANES",
      "LANE_CHANGING",
      "PREPARING_TO_MERGE",
      "URGENT_LANE_CHANGE",
      "URGENT_SWERVING",
      "COMFORT_SWERVING",
      "EXIT"};

  //! @brief LateralActionDirectionMapping
  static constexpr std::array<const char*, 3> LateralActionDirectionMapping{
      "NONE",
      "LEFT",
      "RIGHT"};

  //! @brief Convert a lateralAction state to const char*
  //! @param state
  //! @return const char*
  static constexpr const char* to_cstr(LateralAction::State state)
  {
    return LateralActionStateMapping[static_cast<size_t>(state)];
  }

  //! @brief Convert a lateralAction direction to const string
  //! @param direction
  //! @return const char*
  static constexpr const char* to_cstr(LateralAction::Direction direction)
  {
    return LateralActionDirectionMapping[static_cast<size_t>(direction)];
  }

  //! @brief Convert a lateralAction to string
  //! @param lateralAction
  //! @return string
  static inline std::string to_string(LateralAction lateralAction) noexcept
  {
    std::string msg = std::string(to_cstr(lateralAction.state));
    if (lateralAction.direction != LateralAction::Direction::NONE)
    {
      msg += "_";
      msg += std::string(to_cstr(lateralAction.direction));
    }
    return msg;
  }

  //! @brief Convert a vector<T> to string, with separator sign ';' for each element
  //! @param &vec
  //! @return string
  template <typename T>
  static inline std::string ToIntegerString(const std::vector<T>& vec)
  {
    std::stringstream output;
    constexpr char SEPARATOR{';'};
    for (const T& elem : vec)
    {
      output << static_cast<int>(elem) << SEPARATOR;
    }

    return output.str();
  }

  //! @brief return objectInformationSCM from given aoi at specific index
  //! @param aoi
  //! @param surroundingObjectsScm
  //! @param indexSideAoi
  //! @return const ObjectInformationSCM*
  static const ObjectInformationSCM* GetObjectInformationFromSurroundingObjectScm(AreaOfInterest aoi,
                                                                                  const SurroundingObjectsSCM* surroundingObjectsScm,
                                                                                  int indexSideAoi);

  //! @brief Calculate cortival magnification factor for a given angular position on driver's retina in relation to view axis
  //! @param opticalAngleTowardsViewAxis     Angular position on driver's retina in relation to view axis in rad.
  //! @return Cortical magnification factor
  static double CalculateCorticalMagnificationFactor(units::angle::radian_t opticalAngleTowardsViewAxis);

  //! @brief Checks if the given AreaOfInterest is in the SURROUNDING_AREAS_OF_INTERST array.
  //! @param aoi
  //! @return is true when aoi in SURROUNDING_AREAS_OF_INTERST
  static bool IsSurroundingAreaOfInterest(AreaOfInterest aoi);

  //! @brief Checks if the given AreaOfInterest is in the SURROUNDING_AREAS_OF_INTERST_VECTOR array.
  //! @param aoi
  //! @return is true when aoi in SURROUNDING_AREAS_OF_INTERST_VECTOR
  static bool IsSurroundingAreaOfInterestVector(AreaOfInterest aoi);

  //! @brief Method which checks if vehicle classification is marked as the car
  //! @param vehicleClassification Vehicle classification
  //! @return true if vehicle classification is marked as the car
  static bool IsCar(scm::common::VehicleClass vehicleClassification);

  //! @brief Method which checks if vehicle classification is marked as the truck
  //! @param vehicleClassification Vehicle classification
  //! @return true if vehicle classification is marked as the truck
  static bool IsTruck(scm::common::VehicleClass vehicleClassification);
};
