/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AuditoryPerception.h"

#include <algorithm>

#include "AuditoryPerceptionInterface.h"

AuditoryPerception::AuditoryPerception(units::time::millisecond_t cycleTime, StochasticsInterface* stochastics)
{
  _cycleTime = static_cast<units::time::millisecond_t>(cycleTime);
  _durationOwnVehicleSignalActivity = -_cycleTime;
  _stochastics = stochastics;
  _auditoryProcessingTime = units::make_unit<units::time::millisecond_t>(_stochastics->GetLogNormalDistributed(meanAuditoryProcessingTime.value(), sigmaAuditoryProcessingTime.value()));
}

bool AuditoryPerception::IsAcousticStimulusDetected(std::vector<AdasHmiSignal> acousticAdasSignals) const
{
  auto hasActivity = [](AdasHmiSignal acousticAdasSignal)
  { return acousticAdasSignal.activity; };

  return std::find_if(acousticAdasSignals.begin(), acousticAdasSignals.end(), hasActivity) != acousticAdasSignals.end();
}

void AuditoryPerception::UpdateStimulusData(std::vector<AdasHmiSignal> acousticAdasSignals)
{
  for (const auto& acousticAdasSignal : acousticAdasSignals)
  {
    if (acousticAdasSignal.activity)
    {
      _acousticStimulusDirection = TranslateSpotOfPresentationToDirection(acousticAdasSignal.spotOfPresentation);
    }
  }

  // monitor time since activation and reaction time towards the stimulus
  bool isStimulusProcessed;

  if (IsAcousticStimulusDetected(acousticAdasSignals))
  {
    _durationOwnVehicleSignalActivity += _cycleTime;
    isStimulusProcessed = _durationOwnVehicleSignalActivity >= _auditoryProcessingTime;
  }
  else  // reset reaction values if there is no stimulus detected
  {
    _durationOwnVehicleSignalActivity = -_cycleTime;
    // draw signal processing time for the next detection
    _auditoryProcessingTime = units::make_unit<units::time::millisecond_t>(_stochastics->GetLogNormalDistributed(meanAuditoryProcessingTime.value(),
                                                                                                                 sigmaAuditoryProcessingTime.value()));
    isStimulusProcessed = false;
  }

  // set output variables of the sub-module for further processing
  if (isStimulusProcessed)
  {
    _acousticStimulusProcessed = true;
  }
  else
  {
    _acousticStimulusProcessed = false;
    _acousticStimulusDirection = DEFAULT;  // dummy value for non-perception of acoustic stimuli
  }
}

bool AuditoryPerception::IsAcousticStimulusProcessed() const
{
  return _acousticStimulusProcessed;
}

AcousticStimulusDirection AuditoryPerception::GetAcousticStimulusDirection() const
{
  return _acousticStimulusDirection;
}

AcousticStimulusDirection AuditoryPerception::TranslateSpotOfPresentationToDirection(const std::string& spotOfPresentation)
{
  if (spotOfPresentation == "front")
  {
    return FRONT;
  }
  else if (spotOfPresentation == "sideRight")
  {
    return RIGHT;
  }
  else if (spotOfPresentation == "rear")
  {
    return REAR;
  }
  else if (spotOfPresentation == "sideLeft")
  {
    return LEFT;
  }
  else
  {
    return DEFAULT;
  }
}
