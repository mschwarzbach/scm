/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  TopDownRequestInterface.h
#pragma once

#include <map>

#include "GazeControl/GazeParameters.h"
#include "LateralAction.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

//! @brief contains the gaze state intensities and durations
struct GazeStateIntensitiesAndDurations
{
  //! @brief gazeStateIntensities
  std::map<GazeState, double> gazeStateIntensities;
  //! @brief distributions
  std::map<GazeState, GazeParameters::Distribution> distributions{};
};

//! @brief Implements the OpticalInformationInterface
class TopDownRequestInterface
{
public:
  virtual ~TopDownRequestInterface() = default;

  //! @brief Update the Top down request
  //! @param actionState
  //! @param topDownRequestMap
  //! @param isInInitState
  //! @return gazestate intensities and durations
  virtual GazeStateIntensitiesAndDurations Update(LateralAction actionState, const std::map<AreaOfInterest, double> topDownRequestMap, bool isInInitState) const = 0;

  //! @brief Get intensities for lane keeping without leading vehicle
  //! @return map with gazeState and intensities
  virtual std::map<GazeState, double> GetIntensitiesForLaneKeepingWithoutLeadingVehicle() const = 0;
};