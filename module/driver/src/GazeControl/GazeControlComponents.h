/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeControlComponents.h
#pragma once
#include <iostream>
#include <memory>

#include "GazeControl/GazeControlComponentsInterface.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "MentalModelInterface.h"

//! @brief GazeControlComponents is a collection of all different Gaze components
class GazeControlComponents : public GazeControlComponentsInterface
{
public:
  //! @brief Constructor initializes and creates GazeControl.
  //! @param mentalModel
  //! @param stochastics
  //! @param featureExtractor
  GazeControlComponents(MentalModelInterface& mentalModel, StochasticsInterface* stochastics, const FeatureExtractorInterface& featureExtractor);

  BottomUpRequestInterface* GetBottomUpRequest() override;
  TopDownRequestInterface* GetTopDownRequest() override;
  ImpulseRequestInterface* GetImpulseRequest() override;
  StimulusRequestInterface* GetStimulusRequest() override;
  GazeFieldQueryInterface* GetGazeFieldQuery() override;
  GazePeripheryInterface* GetGazePeriphery() override;
  OpticalInformationInterface* GetOpticalInformation() override;
  GazeUsefulFieldOfViewInterface* GetUsefulFieldOfView() override;
  GazeFoveaInterface* GetGazeFovea() override;
  CurrentGazeStateInterface* GetCurrentGazeState() override;

private:
  //! @brief Receives informations from bottomUpRequest
  std::unique_ptr<BottomUpRequestInterface> _bottomUpRequest{nullptr};
  //! @brief Receives informations from topDownRequest
  std::unique_ptr<TopDownRequestInterface> _topDownRequest{nullptr};
  //! @brief Receives informations from impulseRequest
  std::unique_ptr<ImpulseRequestInterface> _impulseRequest{nullptr};
  //! @brief Receives informations from stimulusRequest
  std::unique_ptr<StimulusRequestInterface> _stimulusRequest{nullptr};
  //! @brief Receives informations from gazeFieldQuery
  std::unique_ptr<GazeFieldQueryInterface> _gazeFieldQuery{nullptr};
  //! @brief Receives informations from gazePeriphery
  std::unique_ptr<GazePeripheryInterface> _gazePeriphery{nullptr};
  //! @brief Receives informations from opticalInformation
  std::unique_ptr<OpticalInformationInterface> _opticalInformation{nullptr};
  //! @brief Receives informations from gazeUsefulFieldOfView
  std::unique_ptr<GazeUsefulFieldOfViewInterface> _gazeUsefulFieldOfView{nullptr};
  //! @brief Receives informations from gazeFovea
  std::unique_ptr<GazeFoveaInterface> _gazeFovea{nullptr};
  //! @brief Receives informations from _urrentGazeState
  std::unique_ptr<CurrentGazeStateInterface> _currentGazeState{nullptr};

  //! @brief mentalModel
  MentalModelInterface& _mentalModel;
  //! @brief stochastics
  StochasticsInterface* _stochastics;
  //! @brief featureExtractor
  const FeatureExtractorInterface& _featureExtractor;
};