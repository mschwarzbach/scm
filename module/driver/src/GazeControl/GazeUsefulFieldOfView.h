/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeUsefulFieldOfView.h
#pragma once

#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazeFieldQueryInterface.h"
#include "GazeControl/GazeUsefulFieldOfViewInterface.h"
#include "MentalModelInterface.h"

//! @brief This component determine the UFOV aois
class GazeUsefulFieldOfView : public GazeUsefulFieldOfViewInterface
{
public:
  //! @brief Constructor initializes and creates GazeControl.
  //! @param mentalModel
  //! @param gazeFieldQuery
  GazeUsefulFieldOfView(const MentalModelInterface& mentalModel, GazeFieldQueryInterface& gazeFieldQuery)
      : _mentalModel(mentalModel), _gazeFieldQuery(gazeFieldQuery)
  {
  }

  std::vector<AreaOfInterest> GetUsefulFieldOfViewAois(const SurroundingObjectsSCM& surroundingObjects, units::angle::radian_t gazeAngleHorizontal) const override;

protected:
  //! @brief Genereates 'Useful Field Of View' for front and sides
  //! @param  ufov
  //! @param  ongoingTraffic
  //! @param  gazeAngleHorizontal
  //! @return vector with Front and Sides Aois for ufov
  std::vector<AreaOfInterest> GenerateUfovForFrontAndSides(std::vector<AreaOfInterest> ufov, const OngoingTrafficObjectsSCM& ongoingTraffic, units::angle::radian_t gazeAngleHorizontal) const;

private:
  //! @brief mentalModel
  const MentalModelInterface& _mentalModel;
  //! @brief gazeFieldQuery
  const GazeFieldQueryInterface& _gazeFieldQuery;
};