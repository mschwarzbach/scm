/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TopDownRequest.h"

#include "GazeControl/GazeParameters.h"
#include "GazeControl/TopDownRequestInterface.h"
#include "LateralActionQuery.h"
#include "ScmCommons.h"
#include "include/common/ScmEnums.h"

GazeStateIntensitiesAndDurations TopDownRequest::Update(LateralAction actionState, std::map<AreaOfInterest, double> topDownRequestMap, bool isInInitState) const
{
  // Clear old gaze behaviour stochastic data from current t;
  GazeStateIntensitiesAndDurations gazeStateIntensitiesAndDurations{};

  gazeStateIntensitiesAndDurations.distributions[GazeState::SACCADE].mean = 8._ms;
  gazeStateIntensitiesAndDurations.distributions[GazeState::SACCADE].sigma = 6._ms;

  if (isInInitState)
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForLaneKeepingWithoutLeadingVehicle();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForLaneKeepingWithoutLeadingVehicle;
  }

  LateralActionQuery action(actionState);
  if (action.IsLaneKeeping())
  {
    if (_featureExtractor.HasJamVelocityEgo())
    {
      gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForSlowlyDriving();
      gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForSlowlyDriving;
    }
    else
    {
      auto gazeLaneKeepingIntensities = GetIntensitiesAndDurationsForLaneKeeping();
      gazeStateIntensitiesAndDurations.gazeStateIntensities = gazeLaneKeepingIntensities.gazeStateIntensities;
      gazeStateIntensitiesAndDurations.distributions = gazeLaneKeepingIntensities.distributions;
    }
  }
  else if (actionState.direction == LateralAction::Direction::RIGHT)
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForLaneChangingActionsToRightLane();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForLaneChangingActionsToRightLane;
  }
  else if (actionState.direction == LateralAction::Direction::LEFT)
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForLaneChangingActionsToLeftLane();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForLaneChangingActionsToLeftLane;
  }
  else
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForAmbientNoise();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForAmbientNoise;
  }

  // TOP DOWN Update
  if (topDownRequestMap.empty())
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = ChangeIntensitiesForVehiclesOvertaking(gazeStateIntensitiesAndDurations.gazeStateIntensities);
  }
  else  // Ambient noise regulations
  {
    gazeStateIntensitiesAndDurations.gazeStateIntensities = ChangeIntensitiesForInformationRequests(topDownRequestMap, gazeStateIntensitiesAndDurations.gazeStateIntensities);
  }

  gazeStateIntensitiesAndDurations.gazeStateIntensities = ChangeIntensitiesToAvoidImpossibilities(gazeStateIntensitiesAndDurations.gazeStateIntensities);
  return gazeStateIntensitiesAndDurations;
}

std::map<GazeState, double> TopDownRequest::ChangeIntensitiesToAvoidImpossibilities(std::map<GazeState, double> intensities) const
{
  // Apply top-down-driven influences on the gaze behaviour, which are independent from the current situaton/action
  if (!_mentalModel.GetLaneExistence(RelativeLane::RIGHT))
  {
    intensities[GazeState::RIGHT_FRONT_FAR] = 0.;
    intensities[GazeState::RIGHT_FRONT] = 0.;
    intensities[GazeState::RIGHT_SIDE] = 0.;
    intensities[GazeState::RIGHT_REAR] = 0.;
  }

  if (!_mentalModel.GetLaneExistence(RelativeLane::LEFT))
  {
    intensities[GazeState::LEFT_FRONT_FAR] = 0.;
    intensities[GazeState::LEFT_FRONT] = 0.;
    intensities[GazeState::LEFT_SIDE] = 0.;
    intensities[GazeState::LEFT_REAR] = 0.;
  }

  if (!_mentalModel.GetLaneExistence(RelativeLane::RIGHTRIGHT))
  {
    intensities[GazeState::RIGHTRIGHT_FRONT] = 0.;
    intensities[GazeState::RIGHTRIGHT_SIDE] = 0.;
    intensities[GazeState::RIGHTRIGHT_REAR] = 0.;
  }

  if (!_mentalModel.GetLaneExistence(RelativeLane::LEFTLEFT))
  {
    intensities[GazeState::LEFTLEFT_FRONT] = 0.;
    intensities[GazeState::LEFTLEFT_SIDE] = 0.;
    intensities[GazeState::LEFTLEFT_REAR] = 0.;
  }

  if (!_mentalModel.GetInteriorHudExistence())
  {
    intensities[GazeState::HUD] = 0.;
  }

  if (!_mentalModel.GetInteriorInfotainmentExistence())
  {
    intensities[GazeState::INFOTAINMENT] = 0.;
  }

  return intensities;
}

GazeStateIntensitiesAndDurations TopDownRequest::GetIntensitiesAndDurationsForLaneKeeping() const
{
  bool hasVehicleInEgoFront = _mentalModel.GetIsVehicleVisible(AreaOfInterest::EGO_FRONT);

  GazeStateIntensitiesAndDurations gazeStateIntensitiesAndDurations;

  if (hasVehicleInEgoFront)
  {
    // This implements gaze behaviour in the presence of a leading vehicle
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForLaneKeepingWithLeadingVehicle();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForLaneKeepingWithLeadingVehicle;
  }
  else
  {
    // This implements gaze behaviour for free driving (absence of a leading vehicle)
    gazeStateIntensitiesAndDurations.gazeStateIntensities = GetIntensitiesForLaneKeepingWithoutLeadingVehicle();
    gazeStateIntensitiesAndDurations.distributions = GazeParameters::MeanAndSigmaDurationsForLaneKeepingWithoutLeadingVehicle;
  }

  return gazeStateIntensitiesAndDurations;
}

std::map<GazeState, double> TopDownRequest::ChangeIntensitiesForInformationRequests(std::map<AreaOfInterest, double>& topDownRequestMap, const std::map<GazeState, double>& gazeStateIntensities) const
{
  std::map<GazeState, double> outputIntensities{gazeStateIntensities};
  if (outputIntensities[GazeState::SACCADE] > 0.)
  {
    return outputIntensities;
  }

  // Apply top-down-driven influences of current time step on the gaze behaviour for next time step
  for (const auto gs : GazeParameters::ALL_GAZESTATES)  // for-loop over all gaze states
  {
    if (gs != GazeState::NumberOfGazeStates)  // is the gaze state even an currently addressed area of interest?
    {
      AreaOfInterest aoi = ScmCommons::MapGazeStateToAreaOfInterest(gs);
      double intensity = 0.;
      intensity = topDownRequestMap[aoi];

      if (intensity > 0.)
      {
        outputIntensities[gs] = intensity;
      }
    }
  }

  return outputIntensities;
}

std::map<GazeState, double> TopDownRequest::ChangeIntensitiesForVehiclesOvertaking(const std::map<GazeState, double>& gazeStateIntensities) const
{
  // Top-down-mechanism "There was a car on an adjacent lane behind me, which I think must shortly overtake me"
  std::map<GazeState, double> outputIntensities{gazeStateIntensities};
  if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_REAR))
  {
    const auto ttc = _mentalModel.GetTtc(AreaOfInterest::LEFT_REAR);
    if (ttc < .2_s && ttc > -.5_s)
    {
      outputIntensities[GazeState::LEFT_FRONT] = .5;
      outputIntensities[GazeState::LEFT_SIDE] = 1.;
    }
  }

  if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_REAR))
  {
    const auto ttc = _mentalModel.GetTtc(AreaOfInterest::RIGHT_REAR);
    if (ttc < .2_s && ttc > -.5_s)
    {
      outputIntensities[GazeState::RIGHT_FRONT] = .5;
      outputIntensities[GazeState::RIGHT_SIDE] = 1.;
    }
  }

  return outputIntensities;
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForSlowlyDriving() const
{
  std::map<GazeState, double> intensities{};
  // This implements gaze behaviour for slowly driving scenario.
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.17, .027), 0.) * 1.5;
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.06, .037), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.063, .037), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.33, .053), 0.) * 1.5;
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.12, .073), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.127, .073), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = 0.;
  intensities[GazeState::LEFTLEFT_FRONT] = 0.;
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.01, .0082), 0.) / 2.;
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.004, .0028), 0.) / 2.;
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.) / 2.;
  intensities[GazeState::RIGHTRIGHT_REAR] = 0.;
  intensities[GazeState::LEFTLEFT_REAR] = 0.;
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.009, .012), 0.) / 2.;
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0022, .0019), 0.) / 2.;
  intensities[GazeState::RIGHTRIGHT_SIDE] = 0.;
  intensities[GazeState::LEFTLEFT_SIDE] = 0.;
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.0936, .0470), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForLaneKeepingWithLeadingVehicle() const
{
  std::map<GazeState, double> intensities{};
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.17, .027), 0.);
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.06, .037), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.063, .037), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.33, .053), 0.);
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.12, .073), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.127, .073), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = 0.;
  intensities[GazeState::LEFTLEFT_FRONT] = 0.;
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.01, .0082), 0.);
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.004, .0028), 0.);
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::RIGHTRIGHT_REAR] = 0.;
  intensities[GazeState::LEFTLEFT_REAR] = 0.;
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.009, .012), 0.);
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0022, .0019), 0.);
  intensities[GazeState::RIGHTRIGHT_SIDE] = 0.;
  intensities[GazeState::LEFTLEFT_SIDE] = 0.;
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.0936, .0470), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForLaneKeepingWithoutLeadingVehicle() const
{
  std::map<GazeState, double> intensities{};
  intensities[GazeState::SACCADE] = 0.;
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.14, .023), 0.);
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.066, .03), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.073, .027), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.28, .047), 0.);
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.133, .06), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.147, .053), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = 0.;
  intensities[GazeState::LEFTLEFT_FRONT] = 0.;
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.03, .02), 0.);
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.0070, .0053), 0.);
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.03, .01), 0.);
  intensities[GazeState::RIGHTRIGHT_REAR] = 0.;
  intensities[GazeState::LEFTLEFT_REAR] = 0.;
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.03, .02), 0.);
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0081, .0071), 0.);
  intensities[GazeState::RIGHTRIGHT_SIDE] = 0.;
  intensities[GazeState::LEFTLEFT_SIDE] = 0.;
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForLaneChangingActionsToRightLane() const
{
  std::map<GazeState, double> intensities{};
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.143, .03), 0.);
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.077, .043), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.043, .037), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.287, .06), 0.);
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.153, .087), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.077, .043), 0.);
  intensities[GazeState::LEFTLEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.043, .037), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.087, .073), 0.);
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.04, 0.04), 0.);
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.07, .06), 0.);
  intensities[GazeState::RIGHTRIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.04, 0.04), 0.);
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.0013, .003), 0.);
  intensities[GazeState::LEFTLEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.0006, .0015), 0.);
  intensities[GazeState::LEFTLEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0012, .0035), 0.);
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0025, .007), 0.);
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.04, .03), 0.);
  intensities[GazeState::RIGHTRIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.02, .015), 0.);
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.0610, .0408), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForLaneChangingActionsToLeftLane() const
{
  std::map<GazeState, double> intensities{};
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.1367, .027), 0.);
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.04, .0367), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.073, .0367), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.273, .053), 0.);
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.08, .0733), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.1467, .0733), 0.);
  intensities[GazeState::LEFTLEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.077, .043), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.043, .037), 0.);
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.01, .01), 0.);
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.0002, .05), 0.);
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.11, .08), 0.);
  intensities[GazeState::LEFTLEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.04, 0.04), 0.);
  intensities[GazeState::RIGHTRIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.0006, .0015), 0.);
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.04, .04), 0.);
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0003, .0011), 0.);
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.0847, .0428), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::LEFTLEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.02, .015), 0.);
  intensities[GazeState::RIGHTRIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0012, .0035), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}

std::map<GazeState, double> TopDownRequest::GetIntensitiesForAmbientNoise() const
{
  std::map<GazeState, double> intensities{};
  intensities[GazeState::EGO_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.14, .023), 0.);
  intensities[GazeState::RIGHT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.066, .03), 0.);
  intensities[GazeState::LEFT_FRONT_FAR] = std::max(_stochastics->GetNormalDistributed(.073, .027), 0.);
  intensities[GazeState::EGO_FRONT] = std::max(_stochastics->GetNormalDistributed(.28, .047), 0.);
  intensities[GazeState::RIGHT_FRONT] = std::max(_stochastics->GetNormalDistributed(.133, .06), 0.);
  intensities[GazeState::LEFT_FRONT] = std::max(_stochastics->GetNormalDistributed(.147, .053), 0.);
  intensities[GazeState::RIGHTRIGHT_FRONT] = 0.;
  intensities[GazeState::LEFTLEFT_FRONT] = 0.;
  intensities[GazeState::EGO_REAR] = std::max(_stochastics->GetNormalDistributed(.03, .02), 0.);
  intensities[GazeState::RIGHT_REAR] = std::max(_stochastics->GetNormalDistributed(.0070, .0053), 0.);
  intensities[GazeState::LEFT_REAR] = std::max(_stochastics->GetNormalDistributed(.03, .01), 0.);
  intensities[GazeState::RIGHTRIGHT_REAR] = 0.;
  intensities[GazeState::LEFTLEFT_REAR] = 0.;
  intensities[GazeState::LEFT_SIDE] = std::max(_stochastics->GetNormalDistributed(.03, .02), 0.);
  intensities[GazeState::RIGHT_SIDE] = std::max(_stochastics->GetNormalDistributed(.0081, .0071), 0.);
  intensities[GazeState::RIGHTRIGHT_SIDE] = 0.;
  intensities[GazeState::LEFTLEFT_SIDE] = 0.;
  intensities[GazeState::INSTRUMENT_CLUSTER] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::INFOTAINMENT] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);
  intensities[GazeState::HUD] = std::max(_stochastics->GetNormalDistributed(.01, .009), 0.);

  return ChangeIntensitiesToAvoidImpossibilities(intensities);
}
