/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeFoveaInterface.h
#pragma once

#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Get gazeAngleHorizontal for currentGazeState
class GazeFoveaInterface
{
public:
  virtual ~GazeFoveaInterface() = default;
  //! @brief Get gazeAngleHorizontal for currentGazeState based on mlp from SurroundingObjects, if mlp Defaultvalue set defaultGaze Angle
  //! @param currentGazeState
  //! @param surroundingObjects
  //! @return gazeAngleHorizontal
  virtual units::angle::radian_t GetFoveaGazeAngleHorizontal(GazeState currentGazeState, const SurroundingObjectsSCM& surroundingObjects) const = 0;
};