/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GazeFieldQuery.h"

#include "GazeControl/GazeParameters.h"
#include "ScmCommons.h"

std::vector<AreaOfInterest> GazeFieldQuery::AssignFieldOfViewToAoi(AreaOfInterest aoiToCheck, const std::vector<AreaOfInterest>& vectorAoi, bool isUfov, const std::vector<units::angle::radian_t>& orientations, units::angle::radian_t gazeAngleHorizontal) const
{
  if (orientations.empty()) return vectorAoi;

  auto aois = vectorAoi;
  if (_mentalModel.GetFovea() != aoiToCheck && gazeAngleHorizontal != units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE))
  {
    auto [leftBorder, rightBorder] = DetermineGazeAngleSideBorders(isUfov, gazeAngleHorizontal);

    for (const units::angle::radian_t orientation : orientations)
    {
      AreaOfInterest aoi = AreaOfInterest::NumberOfAreaOfInterests;

      BordersAndOrientation bordersAndOrientation{
          .orientation = orientation,
          .leftBorder = leftBorder,
          .rightBorder = rightBorder};

      if (isUfov)
      {
        aoi = AssignUsefulFieldOfViewToAoi(bordersAndOrientation, aoiToCheck, gazeAngleHorizontal);
      }
      else
      {
        aoi = AssignPeripheryFieldOfViewToAoi(bordersAndOrientation, aoiToCheck, gazeAngleHorizontal);
      }

      if (aoi != AreaOfInterest::NumberOfAreaOfInterests)
      {
        aois.push_back(aoi);
        break;
      }
    }
  }
  return aois;
}

std::tuple<units::angle::radian_t, units::angle::radian_t> GazeFieldQuery::DetermineGazeAngleSideBorders(bool isUfov, units::angle::radian_t gazeAngleHorizontal) const
{
  units::angle::radian_t leftBorder;
  units::angle::radian_t rightBorder;
  if (isUfov)
  {
    leftBorder = gazeAngleHorizontal + .5 * GazeParameters::HORIZONTAL_SIZE_UFOV;
    rightBorder = gazeAngleHorizontal - .5 * GazeParameters::HORIZONTAL_SIZE_UFOV;
  }
  else
  {
    leftBorder = gazeAngleHorizontal + .5 * GazeParameters::HORIZONTAL_SIZE_PERIPHERY;
    rightBorder = gazeAngleHorizontal - .5 * GazeParameters::HORIZONTAL_SIZE_PERIPHERY;
  }

  leftBorder = CorrectGazeAngleSideBordersDueChangeOfQuadrants(leftBorder);
  rightBorder = CorrectGazeAngleSideBordersDueChangeOfQuadrants(rightBorder);

  return std::make_tuple(leftBorder, rightBorder);
}

units::angle::radian_t GazeFieldQuery::CorrectGazeAngleSideBordersDueChangeOfQuadrants(units::angle::radian_t border) const
{
  if (border <= units::make_unit<units::angle::radian_t>(-M_PI))
  {
    border = 2._rad * M_PI + border;
  }
  else if (border >= units::make_unit<units::angle::radian_t>(M_PI))
  {
    border = -2._rad * M_PI + border;
  }

  return border;
}

AreaOfInterest GazeFieldQuery::AssignUsefulFieldOfViewToAoi(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal) const
{
  const auto ufovLimit = units::make_unit<units::angle::radian_t>(M_PI) - GazeParameters::HORIZONTAL_SIZE_UFOV / 2.0;
  if (IsAoiWithinHorizontalLimits(bordersAndOrientation, ufovLimit, gazeAngleHorizontal))
  {
    return aoiToCheck;
  }

  return AreaOfInterest::NumberOfAreaOfInterests;
}

bool GazeFieldQuery::IsAoiWithinHorizontalLimits(const BordersAndOrientation& bordersAndOrientation, units::angle::radian_t ufovLimit, units::angle::radian_t gazeAngleHorizontal) const
{
  if (((gazeAngleHorizontal > -ufovLimit && gazeAngleHorizontal < ufovLimit) && (bordersAndOrientation.orientation >= bordersAndOrientation.rightBorder && bordersAndOrientation.orientation <= bordersAndOrientation.leftBorder)) ||
      ((gazeAngleHorizontal < -ufovLimit || gazeAngleHorizontal > ufovLimit) && ((bordersAndOrientation.orientation <= bordersAndOrientation.leftBorder && bordersAndOrientation.orientation > units::make_unit<units::angle::radian_t>(-M_PI)) || (bordersAndOrientation.orientation >= bordersAndOrientation.rightBorder && bordersAndOrientation.orientation < units::make_unit<units::angle::radian_t>(M_PI)))))
  {
    return true;
  }

  return false;
}

AreaOfInterest GazeFieldQuery::AssignPeripheryFieldOfViewToAoi(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal) const
{
  std::vector<AreaOfInterest> ufov = _mentalModel.GetUfov();
  const auto peripheryLimit = units::make_unit<units::angle::radian_t>(M_PI) - GazeParameters::HORIZONTAL_SIZE_PERIPHERY / 2.0;

  if (!IsAreaOfInterestPartOfList(aoiToCheck, ufov) && IsAoiWithinHorizontalLimits(bordersAndOrientation, peripheryLimit, gazeAngleHorizontal))
  {
    return aoiToCheck;
  }

  return AreaOfInterest::NumberOfAreaOfInterests;
}

bool GazeFieldQuery::IsAreaOfInterestPartOfList(AreaOfInterest aoi, const std::vector<AreaOfInterest>& aoiList) const
{
  return std::find(std::begin(aoiList), std::end(aoiList), aoi) != std::end(aoiList);
}

std::vector<AreaOfInterest> GazeFieldQuery::UpdateAreaOfInterest(const ObjectInformationSCM& objectScmInformation, AreaOfInterest aoi, const std::vector<AreaOfInterest>& currentAoi, bool isUfov, units::angle::radian_t gazeAngleHorizontal) const
{
  std::vector<units::angle::radian_t> orientationRelativeToDriver{};
  if (!objectScmInformation.exist)
  {
    orientationRelativeToDriver = {GazeParameters::GetStandardGazeAngleForAoi(aoi)};
  }
  else
  {
    orientationRelativeToDriver = {objectScmInformation.orientationRelativeToDriver.frontLeft,
                                   objectScmInformation.orientationRelativeToDriver.frontRight,
                                   objectScmInformation.orientationRelativeToDriver.rearLeft,
                                   objectScmInformation.orientationRelativeToDriver.rearRight};
  }

  return AssignFieldOfViewToAoi(aoi, currentAoi, isUfov, orientationRelativeToDriver, gazeAngleHorizontal);
}