/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <vector>

#include "include/common/AreaOfInterest.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Implements the GazeUsefulFieldOfView
class GazeUsefulFieldOfViewInterface
{
public:
  virtual ~GazeUsefulFieldOfViewInterface() = default;
  //! @brief Assign AreaOfInterests to useful field of view, according to current gaze fixation.
  //! @param surroundingObjects
  //! @param gazeAngleHorizontal
  //! @return vector of ufov aois
  virtual std::vector<AreaOfInterest> GetUsefulFieldOfViewAois(const SurroundingObjectsSCM& surroundingObjects, units::angle::radian_t gazeAngleHorizontal) const = 0;
};