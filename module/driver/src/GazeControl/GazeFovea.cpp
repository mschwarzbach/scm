/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "GazeFovea.h"

#include "GazeControl/GazeParameters.h"
#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

units::angle::radian_t GazeFovea::GetFoveaGazeAngleHorizontal(GazeState currentGazeState, const SurroundingObjectsSCM& surroundingObjects) const
{
  units::angle::radian_t gazeAngleHorizontal{0.0_rad};
  switch (currentGazeState)
  {
    case GazeState::EGO_FRONT:
      if (!surroundingObjects.ongoingTraffic.Ahead().Close().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Ahead().Close().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::EGO_FRONT_FAR:
      if (!surroundingObjects.ongoingTraffic.FarAhead().Close().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.FarAhead().Close().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::EGO_REAR:
      gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InteriorMirrorOrientationRelativeToDriver;
      break;
    case GazeState::LEFT_FRONT:
      if (!surroundingObjects.ongoingTraffic.Ahead().Left().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Ahead().Left().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::LEFT_FRONT_FAR:
      if (!surroundingObjects.ongoingTraffic.FarAhead().Left().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.FarAhead().Left().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::LEFT_SIDE:
      if (!surroundingObjects.ongoingTraffic.Close().Left().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Close().Left().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::RIGHT_FRONT:
      if (!surroundingObjects.ongoingTraffic.Ahead().Right().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Ahead().Right().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::RIGHT_FRONT_FAR:
      if (!surroundingObjects.ongoingTraffic.FarAhead().Right().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.FarAhead().Right().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::RIGHT_SIDE:
      if (!surroundingObjects.ongoingTraffic.Close().Right().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Close().Right().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::LEFTLEFT_FRONT:
      if (!surroundingObjects.ongoingTraffic.Ahead().FarLeft().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Ahead().FarLeft().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::LEFTLEFT_SIDE:
      if (!surroundingObjects.ongoingTraffic.Close().FarLeft().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Close().FarLeft().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::LEFT_REAR:
    case GazeState::LEFTLEFT_REAR:
      gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->LeftMirrorOrientationRelativeToDriver;
      break;
    case GazeState::RIGHTRIGHT_FRONT:
      if (!surroundingObjects.ongoingTraffic.Ahead().FarRight().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Ahead().FarRight().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::RIGHTRIGHT_SIDE:
      if (!surroundingObjects.ongoingTraffic.Close().FarRight().empty())
        gazeAngleHorizontal = surroundingObjects.ongoingTraffic.Close().FarRight().at(0).orientationRelativeToDriver.frontCenter;
      break;
    case GazeState::RIGHT_REAR:
    case GazeState::RIGHTRIGHT_REAR:
      gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->RightMirrorOrientationRelativeToDriver;
      break;
    case GazeState::HUD:
      if (_mentalModel.GetInteriorHudExistence())
      {
        gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->HeadUpDisplayOrientationRelativeToDriver;
      }
      break;
    case GazeState::INSTRUMENT_CLUSTER:
      gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InstrumentClusterOrientationRelativeToDriver;
      break;
    case GazeState::INFOTAINMENT:
      if (_mentalModel.GetInteriorInfotainmentExistence())
      {
        gazeAngleHorizontal = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InfotainmentOrientationRelativeToDriver;
      }
      break;
    default:
      break;
  }

  if (gazeAngleHorizontal == units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE))
  {
    AreaOfInterest fovea{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->fovea};
    gazeAngleHorizontal = GazeParameters::GetStandardGazeAngleForAoi(fovea);
  }

  return gazeAngleHorizontal;
}
