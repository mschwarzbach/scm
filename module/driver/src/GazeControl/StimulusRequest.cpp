/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "StimulusRequest.h"

std::map<GazeState, double> StimulusRequest::UpdateStimulusDataFront(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<int>& knownObjectIds, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM)
{
  const std::vector<AreaOfInterest> relevantAoi{AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::EGO_FRONT};
  const std::vector<AreaOfInterest> periphery{_mentalModel.GetPeriphery()};
  std::map<GazeState, double> outputGazeStateIntensities = gazeStateIntensities;
  for (const AreaOfInterest aoi : relevantAoi)
  {
    const bool isPeriphery{std::find(periphery.begin(), periphery.end(), aoi) != periphery.end()};
    const auto vehicle{ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, &surroundingObjects, 0)};

    if (vehicle == nullptr) continue;

    if (isPeriphery && IsDataOutdated(vehicle->id))
    {
      bool setBottomUpRequest;
      const bool isUnknownObject{IsNewObject(vehicle->id, knownObjectIds)};
      auto ttcThreshold{0._s};
      double tauDotThreshold{0.};

      if (aoi == AreaOfInterest::EGO_FRONT)
      {
        if (isUnknownObject)
        {
          setBottomUpRequest = true;
        }
        else
        {
          ttcThreshold = 5._s;
          tauDotThreshold = -.5;
          setBottomUpRequest = IsObjectPerceivedAsStimuli(aoi, ttcThreshold, tauDotThreshold, surroundingObjects, ownVehicleInformationSCM);
        }
      }
      else  // Front areas in side lanes
      {
        if (ShouldReceiveNewBottomUpRequest(aoi, &surroundingObjects))
        {
          setBottomUpRequest = true;
        }
        else
        {
          ttcThreshold = isUnknownObject ? 98.9_s : 3._s;
          tauDotThreshold = 0.;
          setBottomUpRequest = IsObjectPerceivedAsStimuli(aoi, ttcThreshold, tauDotThreshold, surroundingObjects, ownVehicleInformationSCM);
        }
      }

      if (setBottomUpRequest)
      {
        outputGazeStateIntensities[ScmCommons::MapAreaOfInterestToGazeState(aoi)] = std::max(1., outputGazeStateIntensities[ScmCommons::MapAreaOfInterestToGazeState(aoi)]);
      }
    }
  }

  return outputGazeStateIntensities;
}

std::map<GazeState, double> StimulusRequest::UpdateStimulusDataSide(const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const std::vector<int>& knownObjectIds, const OwnVehicleInformationSCM& ownVehicleInformationSCM)
{
  std::map<GazeState, double> outputGazeStateIntensities = gazeStateIntensities;
  const std::vector<AreaOfInterest> relevantAoi{AreaOfInterest::LEFT_SIDE, AreaOfInterest::RIGHT_SIDE};

  for (const AreaOfInterest aoi : relevantAoi)
  {
    const std::vector<AreaOfInterest> periphery{_mentalModel.GetPeriphery()};
    const bool isPeriphery{std::find(periphery.begin(), periphery.end(), aoi) != periphery.end()};

    if (isPeriphery)
    {
      std::vector<ObjectInformationSCM> objects;
      GazeState gazeState;

      if (LaneQueryHelper::IsLeftLane(aoi))
      {
        objects = surroundingObjects.ongoingTraffic.Close().Left();
        gazeState = GazeState::LEFT_SIDE;
      }
      else
      {
        objects = surroundingObjects.ongoingTraffic.Close().Right();
        gazeState = GazeState::RIGHT_SIDE;
      }

      for (const auto& object : objects)
      {
        const int id{object.id};
        const bool bottomUpRequestNecessary{IsDataOutdated(id)};
        if (bottomUpRequestNecessary)
        {
          const bool isUnknownObject{IsNewObject(id, knownObjectIds)};
          const auto ttcThreshold = isUnknownObject ? 98.9_s : 3._s;
          const bool setBottomUpRequest = IsObjectPerceivedAsStimuli(aoi, ttcThreshold, 0., surroundingObjects, ownVehicleInformationSCM);

          if (setBottomUpRequest)
          {
            outputGazeStateIntensities[gazeState] = std::max(1., outputGazeStateIntensities[gazeState]);
            break;  // One trigger event is enough for each side
          }
        }
      }
    }
  }
  return outputGazeStateIntensities;
}

std::map<GazeState, double> StimulusRequest::UpdateStimulusDataAcoustic(const std::map<GazeState, double>& gazeStateIntensities, const AuditoryPerceptionInterface& auditoryPerception)
{
  std::map<GazeState, double> outputGazeStateIntensities = gazeStateIntensities;
  if (auditoryPerception.IsAcousticStimulusProcessed())
  {
    switch (auditoryPerception.GetAcousticStimulusDirection())
    {
      case 0:
        outputGazeStateIntensities[GazeState::EGO_FRONT] = HIGH_GAZE_INTENSITY;
        break;
      case 1:
        outputGazeStateIntensities[GazeState::RIGHT_SIDE] = HIGH_GAZE_INTENSITY;
        break;
      case 2:
        outputGazeStateIntensities[GazeState::EGO_REAR] = HIGH_GAZE_INTENSITY;
        break;
      case 3:
        outputGazeStateIntensities[GazeState::LEFT_SIDE] = HIGH_GAZE_INTENSITY;
        break;
      default:
        outputGazeStateIntensities[GazeState::EGO_FRONT] = HIGH_GAZE_INTENSITY;
    }
  }

  return outputGazeStateIntensities;
}

std::map<GazeState, double> StimulusRequest::UpdateStimulusDataOptical(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<AdasHmiSignal> opticAdasSignals)
{
  // check for presence of optical adas signals
  bool signalActive = false;
  std::map<GazeState, double> outputGazeStateIntensities = gazeStateIntensities;
  for (const auto& opticAdasSignal : opticAdasSignals)
  {
    if (opticAdasSignal.activity)
    {
      signalActive = true;
      _opticAdasSignalPosition = TranslateSpotOfPresentationToDirection(opticAdasSignal.spotOfPresentation);
    }
  }

  // check for intersection of optical adas signal with FOVEA or UFOV
  bool stimulusDetected = false;
  const AreaOfInterest fovea{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->fovea};
  const std::vector<AreaOfInterest> ufov{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->ufov};

  if (signalActive && (IsAreaOfInterestPartOfList(_opticAdasSignalPosition, ufov) || fovea == _opticAdasSignalPosition))
  {
    stimulusDetected = true;
  }

  // monitor time since detectability and reaction time towards the stimulus
  bool isStimulusProcessed = false;
  if (stimulusDetected)
  {
    _durationOpticStimulusDetectable += _mentalModel.GetCycleTime();
    isStimulusProcessed = (_durationOpticStimulusDetectable >= _opticProcessingTime);
  }
  else  // reset reaction values if there is no stimulus detected
  {
    _durationOpticStimulusDetectable = static_cast<units::time::millisecond_t>(-_mentalModel.GetCycleTime());
    // draw signal processing time for the next detection
    _opticProcessingTime = units::make_unit<units::time::millisecond_t>(_stochastics->GetLogNormalDistributed(MEAN_OPTIC_PROCESSING_TIME.value(), SIGMA_OPTIC_PROCESSING_TIME.value()));
  }

  // determine reaction towards the optical stimuli
  if (isStimulusProcessed)
  {
    outputGazeStateIntensities[GazeState::EGO_FRONT] = HIGH_GAZE_INTENSITY;
  }

  return outputGazeStateIntensities;
}

AreaOfInterest StimulusRequest::TranslateSpotOfPresentationToDirection(std::string spotOfPresentation)
{
  if (spotOfPresentation == "instrumentCluster")
  {
    return AreaOfInterest::INSTRUMENT_CLUSTER;
  }
  else if (spotOfPresentation == "headUpDisplay")
  {
    return AreaOfInterest::HUD;
  }
  else if (spotOfPresentation == "mirrorLeft")
  {
    return AreaOfInterest::LEFT_REAR;
  }
  else if (spotOfPresentation == "mirrorRight")
  {
    return AreaOfInterest::RIGHT_REAR;
  }
  else
  {
    return AreaOfInterest::NumberOfAreaOfInterests;
  }
}

bool StimulusRequest::IsAreaOfInterestPartOfList(AreaOfInterest aoi, std::vector<AreaOfInterest> aoiList)
{
  return std::find(std::begin(aoiList), std::end(aoiList), aoi) != std::end(aoiList);
}

bool StimulusRequest::IsDataOutdated(int id) const
{
  const auto* vehicle{_mentalModel.GetVehicle(id)};
  if (vehicle == nullptr)  // Id not found
  {
    return true;
  }

  // If no AOI could be assigned, most of the existing code does not work
  if (vehicle->GetAssignedAoi() == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return false;
  }

  // TODO Note that this does not implement the previous mechanism of setting data outdated in the spawn zone
  return vehicle->GetReliability() < DataQuality::HIGH;
}

bool StimulusRequest::IsObjectPerceivedAsStimuli(AreaOfInterest aoi, units::time::second_t ttcThreshold, double tauDotThreshold, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM) const
{
  units::time::second_t ttc{ScmDefinitions::TTC_LIMIT};
  units::time::second_t ttcGroundTruth{ScmDefinitions::TTC_LIMIT};
  units::time::second_t ttcPerceptionThreshold{ScmDefinitions::TTC_LIMIT};
  double tauDot{0.};
  units::velocity::meters_per_second_t velocityDelta = 0.0_mps;
  units::length::meter_t relativeNetDistance = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);

  if (LaneQueryHelper::IsSideArea(aoi))  // TTC_lateral (TauDot_Lateral is currently not defined)
  {
    bool isLeftSide{false};
    std::vector<ObjectInformationSCM> objectSide{};

    if (LaneQueryHelper::IsLeftLane(aoi))
    {
      objectSide = surroundingObjects.ongoingTraffic.Close().Left();
      isLeftSide = true;
    }
    else
    {
      objectSide = surroundingObjects.ongoingTraffic.Close().Right();
      isLeftSide = false;
    }

    for (const auto& object : objectSide)
    {
      ttcPerceptionThreshold = CalculateTtcThreshold(object.opticalAngleSizeHorizontal,
                                                     object.opticalAnglePositionHorizontal,
                                                     _mentalModel.GetDriverParameters());

      velocityDelta = object.lateralVelocity - ownVehicleInformationSCM.lateralVelocity;
      relativeNetDistance = isLeftSide ? -object.obstruction.right : -object.obstruction.left;
      ttcGroundTruth = ScmCommons::CalculateTimeToCollision(velocityDelta, relativeNetDistance);

      if (units::math::fabs(ttcGroundTruth) <= ttcPerceptionThreshold)
      {
        ttc = units::math::fmin(ttc, ttcGroundTruth);
      }
    }
  }
  else  // TTC_longitudinal and TauDot_Longitudinal
  {
    const ObjectInformationSCM* object{ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, &surroundingObjects, 0)};
    ttcPerceptionThreshold = CalculateTtcThreshold(object->opticalAngleSizeHorizontal,
                                                   object->opticalAnglePositionHorizontal,
                                                   _mentalModel.GetDriverParameters());
    velocityDelta = object->longitudinalVelocity - ownVehicleInformationSCM.longitudinalVelocity;
    relativeNetDistance = object->relativeLongitudinalDistance;
    auto accelerationDelta = object->acceleration - ownVehicleInformationSCM.acceleration;
    ttcGroundTruth = ScmCommons::CalculateTimeToCollision(velocityDelta, relativeNetDistance);

    if (units::math::fabs(ttcGroundTruth) <= ttcPerceptionThreshold)
    {
      ttc = ttcGroundTruth;
      // Adjust signs for 'front' - 'rear' object calculation
      velocityDelta = LaneQueryHelper::IsRearArea(aoi) ? -velocityDelta : velocityDelta;
      accelerationDelta = LaneQueryHelper::IsRearArea(aoi) ? -accelerationDelta : accelerationDelta;
      tauDot = ScmCommons::CalculateTauDot(velocityDelta, relativeNetDistance, accelerationDelta, aoi);
    }
  }

  if (ttcThreshold >= 0_s)
  {
    if (tauDotThreshold == 0.)
    {
      return (ttc >= 0_s && ttc <= ttcThreshold);
    }
    else
    {
      return (ttc >= 0_s && ttc <= ttcThreshold && tauDot <= tauDotThreshold);
    }
  }
  else  // consider moving away
  {
    if (tauDotThreshold == 0.)
    {
      return (ttc < 0_s && ttc >= ttcThreshold);
    }
    else
    {
      return (ttc < 0_s && ttc >= ttcThreshold) && tauDot <= tauDotThreshold;
    }
  }
}

bool StimulusRequest::IsNewObject(int id, const std::vector<int>& knownObjectIds) const
{
  return std::find(knownObjectIds.begin(), knownObjectIds.end(), id) != knownObjectIds.end();
}

bool StimulusRequest::ShouldReceiveNewBottomUpRequest(AreaOfInterest aoi, const SurroundingObjectsSCM* surroundingObjects)
{
  return ((aoi == AreaOfInterest::LEFT_FRONT || aoi == AreaOfInterest::LEFT_FRONT_FAR) && PeripheralPerceptionOfLateralMotion(ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, surroundingObjects, 0), false)) ||
         ((aoi == AreaOfInterest::RIGHT_FRONT || aoi == AreaOfInterest::RIGHT_FRONT_FAR) && PeripheralPerceptionOfLateralMotion(ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, surroundingObjects, 0), true));
}

units::time::second_t StimulusRequest::CalculateTtcThreshold(units::angle::radian_t opticalAngleSizeHorizontal, units::angle::radian_t opticalAngleTowardsViewAxis, DriverParameters driverParameters) const
{
  // Note: Functionality is only used for stimuli calculation and thus considers the cortical magnification deteriation effect.
  //       If used for any other functionalities reconsider this influence.
  return ScmCommons::CalculateTtcThreshold(opticalAngleSizeHorizontal, opticalAngleTowardsViewAxis, driverParameters, true);
}

bool StimulusRequest::PeripheralPerceptionOfLateralMotion(const ObjectInformationSCM* object, bool isRight)
{
  if (object && object->exist && !object->isStatic)
  {
    if (object->relativeLongitudinalDistance > 0.0_m)
    {
      units::frequency::hertz_t loomingCriteria = 0_Hz;
      if (isRight)
      {
        loomingCriteria = object->lateralVelocity / object->relativeLongitudinalDistance;
      }
      else
      {
        loomingCriteria = -(object->lateralVelocity / object->relativeLongitudinalDistance);
      }
      double thresholdLooming = object->thresholdLooming;
      return (loomingCriteria.value() > thresholdLooming);
    }
  }
  return false;
}

std::map<GazeState, double> StimulusRequest::UpdateStimulusDataCombinedSignal(std::map<GazeState, double>& gazeStateIntensities, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl)
{
  if (!combinedSignal.activity)
  {
    return gazeStateIntensities;
  }

  bool isStimulusProcessed = false;
  auto signalPosition = AreaOfInterest::NumberOfAreaOfInterests;

  if (combinedSignal.activity)
  {
    signalPosition = TranslateSpotOfPresentationToDirection(combinedSignal.spotOfPresentation);
    _durationCombinedStimulusDetectable += _mentalModel.GetCycleTime();
    isStimulusProcessed = (_durationCombinedStimulusDetectable >= _opticProcessingTime);
  }

  if (isStimulusProcessed)
  {
    gazeControl.SetHafSignalProcessed(true);
    gazeStateIntensities[ScmCommons::MapAreaOfInterestToGazeState(signalPosition)] = HIGH_GAZE_INTENSITY;
  }
  return gazeStateIntensities;
}