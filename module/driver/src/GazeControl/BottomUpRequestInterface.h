/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  BottomUpRequestInterface.h
#pragma once
#include <map>

#include "AuditoryPerceptionInterface.h"
#include "GazeControl/GazeControlInterface.h"
#include "LateralAction.h"
#include "include/common/AreaOfInterest.h"

//! @brief Interface for BottomUpRequest class to enable testing of dependent classes.
class BottomUpRequestInterface
{
public:
  virtual ~BottomUpRequestInterface() = default;

  //! @brief Update BottomUp intensities with impulse and stimulus Requests
  //! @param knownIds
  //! @param gazeStateIntensities
  //! @param surroundingObjects
  //! @param ownVehicleInformationSCM
  //! @param opticAdasSignals
  //! @param auditoryPerception
  //! @param combinedSignal
  //! @param gazeControl
  virtual void Update(const std::vector<int>& knownIds, const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM, const std::vector<AdasHmiSignal>& opticAdasSignals, AuditoryPerceptionInterface& auditoryPerception, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl) = 0;

  //! @brief Getter with updated BottomUp intensities
  //! @return updated BottomUp intensities
  virtual std::map<GazeState, double> GetBottomUpIntensities() const = 0;
};