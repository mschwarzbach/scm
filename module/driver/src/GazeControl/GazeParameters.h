/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cmath>
#include <map>

#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

//! @brief Defines opening angles of the field of view for use in gaze related functionality.

namespace GazeParameters
{
//! @brief Horizontal size of the useful field of view [rad].
constexpr units::angle::radian_t HORIZONTAL_SIZE_UFOV = 60._rad / 180. * M_PI;

//! @brief Horizontal size of the peripheral field of view [rad].
constexpr units::angle::radian_t HORIZONTAL_SIZE_PERIPHERY = 210._rad / 180. * M_PI;

//! @brief Horizontal size of a smaller peripheral field of view that is used for solving MentalModel inconsistencies
//! Is smaller than periphery to avoid removing barely visible agents and
//! to increase tolerance to differences between extrapolated and GroundTruth vehicle positions
constexpr units::angle::radian_t HORIZONTAL_SIZE_USEFUL_PERIPHERY = 200_rad / 180. * M_PI;

//! @brief Minimum fixation duration for a gaze state [ms].
static constexpr units::time::millisecond_t MINIMUM_DURATION_GAZE_STATE = 100_ms;

//! @brief Mean value of the variable part of the duration of a long saccade [ms].
static constexpr units::time::millisecond_t MEAN_LONG_SACCADE_DURATION = 84.0_ms;

//! @brief Standard deviation of the variable part of the duration of a long saccade [ms].
static constexpr units::time::millisecond_t SIGMA_LONG_SACCADE_DURATION = 8.0_ms;

//! @brief Static part of the saccade duration (also minimum saccade duration) [ms].
static constexpr units::time::millisecond_t STATIONARY_PART_SACCADE_DURATION = 21.0_ms;

//! @brief Duration of the saccadic supression - fade in case in [ms].
static constexpr units::time::millisecond_t DURATION_FADE_IN = 50.0_ms;

//! @brief Duration of the saccadic supression - fade out case in [ms].
static constexpr units::time::millisecond_t DURATION_FADE_OUT = 75.0_ms;

//! @brief Duration of the saccadic supression - fade in and fade out case together in [ms].
static constexpr units::time::millisecond_t DURATION_FADING = 125.0_ms;

inline units::angle::radian_t GetStandardGazeAngleForAoi(AreaOfInterest aoi)
{
  static const std::map<AreaOfInterest, units::angle::radian_t> gazeAngle{
      {AreaOfInterest::EGO_FRONT, 0._rad / 57.3},
      {AreaOfInterest::EGO_FRONT_FAR, 1._rad / 57.3},
      {AreaOfInterest::LEFT_FRONT, 15._rad / 57.3},
      {AreaOfInterest::LEFT_FRONT_FAR, 15._rad / 57.3},
      {AreaOfInterest::LEFTLEFT_FRONT, 25._rad / 57.3},
      {AreaOfInterest::RIGHT_FRONT, -15._rad / 57.3},
      {AreaOfInterest::RIGHT_FRONT_FAR, -15._rad / 57.3},
      {AreaOfInterest::RIGHTRIGHT_FRONT, -25._rad / 57.3},
      {AreaOfInterest::EGO_REAR, -10._rad / 57.3},
      {AreaOfInterest::LEFT_REAR, 50._rad / 57.3},
      {AreaOfInterest::LEFTLEFT_REAR, 55._rad / 57.3},
      {AreaOfInterest::RIGHT_REAR, -50._rad / 57.3},
      {AreaOfInterest::RIGHTRIGHT_REAR, -55._rad / 57.3},
      {AreaOfInterest::LEFT_SIDE, 90._rad / 57.3},
      {AreaOfInterest::LEFTLEFT_SIDE, 85._rad / 57.3},
      {AreaOfInterest::RIGHT_SIDE, -90._rad / 57.3},
      {AreaOfInterest::RIGHTRIGHT_SIDE, -85._rad / 57.3},
      {AreaOfInterest::INSTRUMENT_CLUSTER, units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)},
      {AreaOfInterest::INFOTAINMENT, units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)},
      {AreaOfInterest::HUD, units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)},
      {AreaOfInterest::DISTRACTION, units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)},
      {AreaOfInterest::NumberOfAreaOfInterests, units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)}};
  auto it{gazeAngle.find(aoi)};

  if (gazeAngle.end() == it)
  {
    return units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE);
  }
  return it->second;
}

static constexpr std::array<GazeState, 24> ALL_GAZESTATES{
    GazeState::SACCADE,
    GazeState::EGO_FRONT,
    GazeState::EGO_FRONT_FAR,
    GazeState::RIGHT_FRONT,
    GazeState::RIGHT_FRONT_FAR,
    GazeState::LEFT_FRONT,
    GazeState::LEFT_FRONT_FAR,
    GazeState::EGO_REAR,
    GazeState::RIGHT_REAR,
    GazeState::LEFT_REAR,
    GazeState::RIGHT_SIDE,
    GazeState::LEFT_SIDE,
    GazeState::UNDEFINED,
    GazeState::INSTRUMENT_CLUSTER,
    GazeState::INFOTAINMENT,
    GazeState::HUD,
    GazeState::RIGHTRIGHT_FRONT,
    GazeState::LEFTLEFT_FRONT,
    GazeState::RIGHTRIGHT_REAR,
    GazeState::LEFTLEFT_REAR,
    GazeState::RIGHTRIGHT_SIDE,
    GazeState::LEFTLEFT_SIDE,
    GazeState::DISTRACTION,
    GazeState::NumberOfGazeStates};

inline static std::map<AreaOfInterest, std::vector<AreaOfInterest>> GetRearAndInteriourAoiPeripheryMap()
{
  std::map<AreaOfInterest, std::vector<AreaOfInterest>> aoiPeripheryMap;
  aoiPeripheryMap[AreaOfInterest::LEFT_REAR] = {AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::LEFT_SIDE};
  aoiPeripheryMap[AreaOfInterest::RIGHT_REAR] = {AreaOfInterest::EGO_REAR, AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::RIGHT_SIDE};
  aoiPeripheryMap[AreaOfInterest::EGO_REAR] = {AreaOfInterest::LEFTLEFT_REAR, AreaOfInterest::RIGHTRIGHT_REAR, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::LEFT_FRONT};
  aoiPeripheryMap[AreaOfInterest::INSTRUMENT_CLUSTER] = {AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::LEFT_REAR};
  aoiPeripheryMap[AreaOfInterest::INFOTAINMENT] = {AreaOfInterest::EGO_FRONT};
  aoiPeripheryMap[AreaOfInterest::HUD] = {AreaOfInterest::EGO_REAR, AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_REAR, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_REAR};
  aoiPeripheryMap[AreaOfInterest::LEFTLEFT_REAR] = {AreaOfInterest::EGO_REAR, AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::LEFT_SIDE};
  aoiPeripheryMap[AreaOfInterest::RIGHTRIGHT_REAR] = {AreaOfInterest::EGO_REAR, AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::RIGHT_SIDE};
  return aoiPeripheryMap;
}

inline static std::map<AreaOfInterest, std::vector<AreaOfInterest>> GetRearAndInteriourAoiUfovMap()
{
  std::map<AreaOfInterest, std::vector<AreaOfInterest>> aoiUfovMap;
  aoiUfovMap[AreaOfInterest::LEFT_REAR] = {AreaOfInterest::LEFTLEFT_REAR, AreaOfInterest::EGO_REAR};
  aoiUfovMap[AreaOfInterest::RIGHT_REAR] = {AreaOfInterest::RIGHT_REAR};
  aoiUfovMap[AreaOfInterest::EGO_REAR] = {AreaOfInterest::LEFT_REAR, AreaOfInterest::RIGHT_REAR, AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::LEFT_FRONT_FAR};
  aoiUfovMap[AreaOfInterest::INSTRUMENT_CLUSTER] = {};
  aoiUfovMap[AreaOfInterest::INFOTAINMENT] = {};
  aoiUfovMap[AreaOfInterest::HUD] = {AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR};
  aoiUfovMap[AreaOfInterest::LEFTLEFT_REAR] = {AreaOfInterest::LEFT_REAR};
  aoiUfovMap[AreaOfInterest::RIGHTRIGHT_REAR] = {AreaOfInterest::RIGHT_REAR};

  return aoiUfovMap;
}

//! @brief this struct holds the distribution of mean and sigma
struct Distribution
{
  //! @brief mean
  units::time::millisecond_t mean;
  //! @brief sigma
  units::time::millisecond_t sigma;
};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForSlowlyDriving{
    {GazeState::SACCADE, {8.0_ms, 6.0_ms}},
    {GazeState::LEFT_FRONT_FAR, {107._ms, 37.0_ms}},
    {GazeState::RIGHT_FRONT_FAR, {100._ms, 27.0_ms}},
    {GazeState::EGO_FRONT_FAR, {173._ms, 80._ms}},
    {GazeState::EGO_FRONT, {347._ms, 160.0_ms}},
    {GazeState::RIGHT_FRONT, {200._ms, 53.0_ms}},
    {GazeState::LEFT_FRONT, {213._ms, 73.0_ms}},
    {GazeState::RIGHTRIGHT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::EGO_REAR, {270._ms, 120.0_ms}},
    {GazeState::RIGHT_REAR, {180._ms, 80.0_ms}},
    {GazeState::LEFT_REAR, {390._ms, 160.0_ms}},
    {GazeState::RIGHTRIGHT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFT_SIDE, {200._ms, 90.0_ms}},
    {GazeState::RIGHT_SIDE, {110._ms, 70.0_ms}},
    {GazeState::RIGHTRIGHT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::INSTRUMENT_CLUSTER, {380._ms, 130.0_ms}},
    {GazeState::INFOTAINMENT, {380._ms, 130.0_ms}},
    {GazeState::HUD, {380._ms, 130.0_ms}}};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForLaneKeepingWithLeadingVehicle{
    {GazeState::SACCADE, {8._ms, 6.0_ms}},
    {GazeState::EGO_FRONT_FAR, {173._ms, 80.0_ms}},
    {GazeState::RIGHT_FRONT_FAR, {100._ms, 27.0_ms}},
    {GazeState::LEFT_FRONT_FAR, {107._ms, 37.0_ms}},
    {GazeState::EGO_FRONT, {347._ms, 160.0_ms}},
    {GazeState::RIGHT_FRONT, {200._ms, 53.0_ms}},
    {GazeState::LEFT_FRONT, {213._ms, 73._ms}},
    {GazeState::RIGHTRIGHT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::EGO_REAR, {270._ms, 120.0_ms}},
    {GazeState::RIGHT_REAR, {180._ms, 80.0_ms}},
    {GazeState::LEFT_REAR, {390._ms, 160_ms}},
    {GazeState::RIGHTRIGHT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFT_SIDE, {200._ms, 90.0_ms}},
    {GazeState::RIGHT_SIDE, {110._ms, 70.0_ms}},
    {GazeState::RIGHTRIGHT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::INSTRUMENT_CLUSTER, {380._ms, 130.0_ms}},
    {GazeState::INFOTAINMENT, {380._ms, 130.0_ms}},
    {GazeState::HUD, {380._ms, 130.0_ms}}};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForLaneKeepingWithoutLeadingVehicle{
    {GazeState::SACCADE, {8._ms, 6.0_ms}},
    {GazeState::EGO_FRONT_FAR, {150._ms, 57.0_ms}},
    {GazeState::RIGHT_FRONT_FAR, {140._ms, 50._ms}},
    {GazeState::LEFT_FRONT_FAR, {140._ms, 93.0_ms}},
    {GazeState::EGO_FRONT, {300._ms, 113.0_ms}},
    {GazeState::RIGHT_FRONT, {280._ms, 100.0_ms}},
    {GazeState::LEFT_FRONT, {280._ms, 140.0_ms}},
    {GazeState::RIGHTRIGHT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_FRONT, {0._ms, 0.0_ms}},
    {GazeState::EGO_REAR, {270._ms, 130.0_ms}},
    {GazeState::RIGHT_REAR, {170._ms, 90._ms}},
    {GazeState::LEFT_REAR, {330._ms, 120.0_ms}},
    {GazeState::RIGHTRIGHT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_REAR, {0._ms, 0.0_ms}},
    {GazeState::LEFT_SIDE, {320._ms, 140.0_ms}},
    {GazeState::RIGHT_SIDE, {150._ms, 70.0_ms}},
    {GazeState::RIGHTRIGHT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::LEFTLEFT_SIDE, {0._ms, 0.0_ms}},
    {GazeState::INSTRUMENT_CLUSTER, {390._ms, 160.0_ms}},
    {GazeState::INFOTAINMENT, {390._ms, 160.0_ms}},
    {GazeState::HUD, {390._ms, 160.0_ms}}};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForLaneChangingActionsToRightLane{
    {GazeState::SACCADE, {8._ms, 6.0_ms}},
    {GazeState::EGO_FRONT_FAR, {160._ms, 83._ms}},
    {GazeState::RIGHT_FRONT_FAR, {77._ms, 40._ms}},
    {GazeState::LEFT_FRONT_FAR, {100._ms, 50._ms}},
    {GazeState::EGO_FRONT, {320._ms, 167._ms}},
    {GazeState::RIGHT_FRONT, {153._ms, 80._ms}},
    {GazeState::LEFT_FRONT, {200._ms, 100._ms}},
    {GazeState::RIGHTRIGHT_FRONT, {77._ms, 40._ms}},
    {GazeState::LEFTLEFT_FRONT, {77._ms, 40._ms}},
    {GazeState::EGO_REAR, {270._ms, 130._ms}},
    {GazeState::RIGHT_REAR, {190._ms, 90._ms}},
    {GazeState::LEFT_REAR, {240._ms, 280._ms}},
    {GazeState::RIGHTRIGHT_REAR, {100._ms, 50._ms}},
    {GazeState::LEFTLEFT_REAR, {80._ms, 40._ms}},
    {GazeState::LEFT_SIDE, {290._ms, 190._ms}},
    {GazeState::RIGHT_SIDE, {110._ms, 130._ms}},
    {GazeState::LEFTLEFT_SIDE, {100._ms, 50._ms}},
    {GazeState::RIGHTRIGHT_SIDE, {153._ms, 80._ms}},
    {GazeState::INSTRUMENT_CLUSTER, {280._ms, 180._ms}},
    {GazeState::INFOTAINMENT, {390._ms, 160._ms}},
    {GazeState::HUD, {390._ms, 160._ms}}};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForLaneChangingActionsToLeftLane{
    {GazeState::SACCADE, {8._ms, 6._ms}},
    {GazeState::EGO_FRONT_FAR, {140._ms, 53._ms}},
    {GazeState::RIGHT_FRONT_FAR, {80._ms, 33._ms}},
    {GazeState::LEFT_FRONT_FAR, {93._ms, 37._ms}},
    {GazeState::EGO_FRONT, {280._ms, 107._ms}},
    {GazeState::RIGHT_FRONT, {160._ms, 67._ms}},
    {GazeState::LEFT_FRONT, {187._ms, 73._ms}},
    {GazeState::RIGHTRIGHT_FRONT, {77._ms, 40._ms}},
    {GazeState::LEFTLEFT_FRONT, {77._ms, 40._ms}},
    {GazeState::EGO_REAR, {270._ms, 170._ms}},
    {GazeState::RIGHT_REAR, {220._ms, 250._ms}},
    {GazeState::LEFT_REAR, {370._ms, 110._ms}},
    {GazeState::LEFTLEFT_REAR, {80._ms, 40._ms}},
    {GazeState::RIGHTRIGHT_REAR, {80._ms, 40._ms}},
    {GazeState::LEFT_SIDE, {130._ms, 80._ms}},
    {GazeState::RIGHT_SIDE, {390._ms, 10._ms}},
    {GazeState::LEFTLEFT_SIDE, {153._ms, 80._ms}},
    {GazeState::RIGHTRIGHT_SIDE, {100._ms, 50._ms}},
    {GazeState::INSTRUMENT_CLUSTER, {300._ms, 140._ms}},
    {GazeState::INFOTAINMENT, {390._ms, 160._ms}},
    {GazeState::HUD, {390._ms, 160._ms}}};

static const std::map<GazeState, Distribution> MeanAndSigmaDurationsForAmbientNoise{
    {GazeState::SACCADE, {8._ms, 6._ms}},
    {GazeState::EGO_FRONT_FAR, {150._ms, 57._ms}},
    {GazeState::RIGHT_FRONT_FAR, {140._ms, 50._ms}},
    {GazeState::LEFT_FRONT_FAR, {140._ms, 93._ms}},
    {GazeState::EGO_FRONT, {300._ms, 113._ms}},
    {GazeState::RIGHT_FRONT, {280._ms, 100._ms}},
    {GazeState::LEFT_FRONT, {280._ms, 140._ms}},
    {GazeState::RIGHTRIGHT_FRONT, {0._ms, 0._ms}},
    {GazeState::LEFTLEFT_FRONT, {0._ms, 0._ms}},
    {GazeState::EGO_REAR, {270._ms, 130._ms}},
    {GazeState::RIGHT_REAR, {170._ms, 90._ms}},
    {GazeState::LEFT_REAR, {330._ms, 120._ms}},
    {GazeState::RIGHTRIGHT_REAR, {0._ms, 0._ms}},
    {GazeState::LEFTLEFT_REAR, {0._ms, 0._ms}},
    {GazeState::LEFT_SIDE, {320._ms, 140._ms}},
    {GazeState::RIGHTRIGHT_SIDE, {0._ms, 0._ms}},
    {GazeState::RIGHT_SIDE, {150._ms, 70._ms}},
    {GazeState::LEFTLEFT_SIDE, {0._ms, 0._ms}},
    {GazeState::INSTRUMENT_CLUSTER, {390._ms, 160._ms}},
    {GazeState::INFOTAINMENT, {390._ms, 160._ms}},
    {GazeState::HUD, {390._ms, 160._ms}}};
}  // namespace GazeParameters