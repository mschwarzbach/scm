/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  OpticalInformationInterface.h
#pragma once

#include <optional>

#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief contains the optical values
struct OpticalValues
{
  //! @brief surroundingObjects
  SurroundingObjectsSCM surroundingObjects{};
  //! @brief gazeAngleHorizontal
  units::angle::radian_t gazeAngleHorizontal{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief Implements the OpticalInformationInterface
class OpticalInformationInterface
{
public:
  virtual ~OpticalInformationInterface() = default;

  //! @brief Add opticalAnglePositionHorizontal, opticalAngleSizeHorizontal and thresholdLooming to ground truth data, according to field of view assignments
  //! @param surroundingObjects
  //! @param gazeAngleHorizontal
  //! @param isInit
  //! @param idealPerception
  //! @return OpticalValues
  virtual OpticalValues SupplementOpticalInformation(SurroundingObjectsSCM surroundingObjects, units::angle::radian_t gazeAngleHorizontal, bool isInit, bool idealPerception) const = 0;
};