/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GazeFollower.h"

#include "include/common/XmlParser.h"
using namespace units::literals;
void ThrowIfFalse(bool success, const std::string& message)
{
  if (!success)
  {
    throw std::runtime_error(message);
  }
}

GazeFollower::GazeFollower(const OwnVehicleInformationSCM& ownVehicleInformationSCM)
    : _ownVehicleInformationSCM(ownVehicleInformationSCM)
{
}

GazeOverwrite GazeFollower::EvaluateGazeOverwrite(units::time::millisecond_t time)
{
  _throwWarning = false;

  // Check for activation of the GazeFollower and parse gaze target time series information if activated
  if (_ownVehicleInformationSCM.gazeFollowerInformation.gazeFileName.size() > 0 && !gazeFollowerActive)
  {
    gazeFollowerActive = true;
    timeOfEventActivation = time;
    ParseGazeTargetTimeSeriesFromFile();
  }

  // Check for deactivation of the GazeFollower and reset relevant submodule parameters
  if (gazeOverwrite.gazeTargetIsLastPointInTimeSeries)
  {
    gazeFollowerActive = false;
    timeOfEventActivation = -999_ms;
    gazeTimeSeries.clear();
  }

  // Process gaze target time series if GazeFollower is active
  if (gazeFollowerActive)
  {
    ProcessGazeTargetTimeSeries(time);
  }
  else
  {
    gazeOverwrite.gazeFollowerActive = false;
    gazeOverwrite.gazeTarget = AreaOfInterest::NumberOfAreaOfInterests;
    gazeOverwrite.durationGazeTarget = -999_ms;
    gazeOverwrite.hasGazeTargetChanged = false;
    gazeOverwrite.gazeTargetIsLastPointInTimeSeries = false;
  }

  // Return current struct information
  return gazeOverwrite;
}

void GazeFollower::ParseGazeTargetTimeSeriesFromFile()
{
  xmlDocPtr document = xmlReadFile((_ownVehicleInformationSCM.gazeFollowerInformation.gazeFilePath).c_str(), "UTF-8", 0);
  xmlNodePtr documentRoot = xmlDocGetRootElement(document);

  ThrowIfFalse(documentRoot, "invalid document root ");

  xmlNodePtr gazeFollowerTimeSeriesPoint = scm::common::GetFirstChildElement(documentRoot, TAG::gazeFollowerTimeSeriesPoint);
  while (gazeFollowerTimeSeriesPoint)
  {
    GazeTimeSeriesPoint gazeTimeSeriesPoint;

    std::string gazeTargetString;
    ThrowIfFalse(scm::common::ParseAttributeDouble(gazeFollowerTimeSeriesPoint, ATTRIBUTE::time, gazeTimeSeriesPoint.time),
                 "GazeFollowerTimeSeriesPoint must contain attribute Time.");
    ThrowIfFalse(scm::common::ParseAttributeString(gazeFollowerTimeSeriesPoint, ATTRIBUTE::gazeTarget, gazeTargetString),
                 "GazeFollowerTimeSeriesPoint must contain attribute GazeTarget.");
    gazeTimeSeriesPoint.gazeTarget = ScmCommons::StringToAreaOfInterest(gazeTargetString);
    gazeTimeSeries.push_back(gazeTimeSeriesPoint);

    gazeFollowerTimeSeriesPoint = xmlNextElementSibling(gazeFollowerTimeSeriesPoint);
  }
}

void GazeFollower::ProcessGazeTargetTimeSeries(units::time::millisecond_t time)
{
  gazeOverwrite.gazeFollowerActive = true;
  auto relativeTime = time - timeOfEventActivation;

  if (relativeTime >= gazeTimeSeries.at(0).time)
  {
    gazeOverwrite.hasGazeTargetChanged = true;
    gazeOverwrite.gazeTarget = gazeTimeSeries.at(0).gazeTarget;

    if (gazeTimeSeries.size() == 1)
    {
      // set duration for last gaze target to zero,
      // will be overwritten with a random duration selected by `GazeControl::ChooseNextGazeState()`
      gazeOverwrite.durationGazeTarget = 0_ms;
      gazeOverwrite.gazeTargetIsLastPointInTimeSeries = true;
    }
    else
    {
      gazeOverwrite.durationGazeTarget = static_cast<units::time::millisecond_t>(gazeTimeSeries.at(1).time - relativeTime);
      gazeOverwrite.gazeTargetIsLastPointInTimeSeries = false;

      if (gazeOverwrite.durationGazeTarget < 250_ms)
      {
        _throwWarning = true;
      }
    }

    gazeTimeSeries.erase(gazeTimeSeries.begin());
  }
  else
  {
    gazeOverwrite.hasGazeTargetChanged = false;
  }
}

bool GazeFollower::GetThrowWarning() const
{
  return _throwWarning;
}
