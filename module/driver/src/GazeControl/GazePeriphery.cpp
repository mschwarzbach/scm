/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GazePeriphery.h"

#include <vector>

#include "GazeControl/GazeParameters.h"
#include "include/common/AreaOfInterest.h"

std::vector<AreaOfInterest> GazePeriphery::GetPeripheryAois(const SurroundingObjectsSCM& surroundingObjectsLocal, units::angle::radian_t gazeAngleHorizontal) const
{
  std::vector<AreaOfInterest> periphery{};
  AreaOfInterest fovea{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->fovea};
  auto rearAndInteriourPeripheries = GazeParameters::GetRearAndInteriourAoiPeripheryMap();

  if (rearAndInteriourPeripheries.count(fovea) > 0)
  {
    periphery = rearAndInteriourPeripheries[fovea];
  }
  else
  {
    periphery = GeneratePeripheryForFrontAndSides(periphery, surroundingObjectsLocal.ongoingTraffic, gazeAngleHorizontal);
  }
  return periphery;
}

std::vector<AreaOfInterest> GazePeriphery::GeneratePeripheryForFrontAndSides(std::vector<AreaOfInterest>& periphery, const OngoingTrafficObjectsSCM& ongoingTraffic, units::angle::radian_t gazeAngleHorizontal) const
{
  for (const auto& aoi : ALL_AREAS_OF_INTEREST)
  {
    std::vector<units::angle::radian_t> orientationRelativeToDriver{};
    switch (aoi)
    {
      case AreaOfInterest::HUD:
        if (_mentalModel.GetInteriorHudExistence())
        {
          orientationRelativeToDriver.push_back(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->HeadUpDisplayOrientationRelativeToDriver);
          periphery = _gazeFieldQuery.AssignFieldOfViewToAoi(AreaOfInterest::HUD, periphery, false, orientationRelativeToDriver, gazeAngleHorizontal);
        }
        break;

      case AreaOfInterest::INFOTAINMENT:
        orientationRelativeToDriver.push_back(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InfotainmentOrientationRelativeToDriver);
        periphery = _gazeFieldQuery.AssignFieldOfViewToAoi(AreaOfInterest::INFOTAINMENT, periphery, false, orientationRelativeToDriver, gazeAngleHorizontal);
        break;

      case AreaOfInterest::INSTRUMENT_CLUSTER:
        orientationRelativeToDriver.push_back(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InstrumentClusterOrientationRelativeToDriver);
        periphery = _gazeFieldQuery.AssignFieldOfViewToAoi(AreaOfInterest::INSTRUMENT_CLUSTER, periphery, false, orientationRelativeToDriver, gazeAngleHorizontal);
        break;
      case AreaOfInterest::DISTRACTION:
        break;

      default:
        if (aoi != AreaOfInterest::DISTRACTION)
        {
          if (auto object = ongoingTraffic.FromAreaOfInterest(aoi); !object->empty())
          {
            periphery = _gazeFieldQuery.UpdateAreaOfInterest(object->front(), aoi, periphery, false, gazeAngleHorizontal);
          }
        }
        break;
    }
  }
  return periphery;
}