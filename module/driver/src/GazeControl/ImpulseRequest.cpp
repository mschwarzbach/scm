/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ImpulseRequest.h"

std::map<GazeState, double> ImpulseRequest::UpdateImpulseDataFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects, AreaOfInterest aoi)
{
  if (IsNonExistingLaneObserved(aoi) || objects->empty())
  {
    return gazeStateIntensities;
  }
  auto object = objects->front();
  bool brakeLightLast{false};
  auto indicatorLast{scm::LightState::Indicator::Off};
  GazeState gazeStateFront{GazeState::UNDEFINED};

  if (aoi == AreaOfInterest::RIGHT_FRONT)
  {
    gazeStateFront = GazeState::RIGHT_FRONT;
    brakeLightLast = _prevBrakeLightRight;
    indicatorLast = _prevIndicatorRight;
  }
  else if (aoi == AreaOfInterest::LEFT_FRONT)
  {
    gazeStateFront = GazeState::LEFT_FRONT;
    brakeLightLast = _prevBrakeLightLeft;
    indicatorLast = _prevIndicatorLeft;
  }
  else if (aoi == AreaOfInterest::RIGHT_FRONT_FAR)
  {
    gazeStateFront = GazeState::RIGHT_FRONT_FAR;
    brakeLightLast = true;                              // Set "on" to avoid reaction due to being switched on
    indicatorLast = scm::LightState::Indicator::Right;  // Set "on" to avoid reaction due to being switched on
  }
  else if (aoi == AreaOfInterest::LEFT_FRONT_FAR)
  {
    gazeStateFront = GazeState::LEFT_FRONT_FAR;
    brakeLightLast = true;                             // Set "on" to avoid reaction due to being switched on
    indicatorLast = scm::LightState::Indicator::Left;  // Set "on" to avoid reaction due to being switched on
  }

  const std::vector<AreaOfInterest> ufov{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->ufov};
  const std::vector<AreaOfInterest> periphery{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->periphery};

  if (IsAreaOfInterestPartOfList(aoi, periphery) || IsAreaOfInterestPartOfList(aoi, ufov))
  {
    _impulsFrontSide = BrakeLightsSwitchedOn(brakeLightLast, object.brakeLightsActive) || IndicatorSwitchedOn(indicatorLast, object.indicatorState);

    if (_impulsFrontSide)
    {
      if (_mentalModel.IsUrgentlyLateWithUpdate(gazeStateFront))
      {
        gazeStateIntensities[gazeStateFront] = HIGH_GAZE_INTENSITY;
      }
      else
      {
        gazeStateIntensities[gazeStateFront] = std::max(1., gazeStateIntensities[gazeStateFront]);
      }
    }

    if (aoi == AreaOfInterest::RIGHT_FRONT)
    {
      _prevIndicatorRight = object.indicatorState;
      _prevBrakeLightRight = object.brakeLightsActive;
    }
    else if (aoi == AreaOfInterest::LEFT_FRONT)
    {
      _prevIndicatorLeft = object.indicatorState;
      _prevBrakeLightLeft = object.brakeLightsActive;
    }
  }

  return gazeStateIntensities;
}

std::map<GazeState, double> ImpulseRequest::UpdateImpulseDataEgoFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects)
{
  if (objects->empty())
  {
    return gazeStateIntensities;
  }
  auto object = objects->front();
  const std::vector<AreaOfInterest> ufov{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->ufov};
  const std::vector<AreaOfInterest> periphery{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->periphery};

  bool impulsFront = object.brakeLightsActive;

  if (impulsFront && (IsAreaOfInterestPartOfList(AreaOfInterest::EGO_FRONT, periphery) || IsAreaOfInterestPartOfList(AreaOfInterest::EGO_FRONT, ufov)))
  {
    gazeStateIntensities[GazeState::EGO_FRONT] = std::max(1., gazeStateIntensities[GazeState::EGO_FRONT]);
  }

  return gazeStateIntensities;
}

bool ImpulseRequest::IsNonExistingLaneObserved(AreaOfInterest aoi)
{
  return (LaneQueryHelper::IsLeftLane(aoi) && !_mentalModel.GetLaneExistence(RelativeLane::LEFT)) ||
         (LaneQueryHelper::IsRightLane(aoi) && !_mentalModel.GetLaneExistence(RelativeLane::RIGHT)) ||
         (LaneQueryHelper::IsLeftLeftLane(aoi) && !_mentalModel.GetLaneExistence(RelativeLane::LEFTLEFT)) ||
         (LaneQueryHelper::IsRightRightLane(aoi) && !_mentalModel.GetLaneExistence(RelativeLane::RIGHTRIGHT)) ||
         aoi == AreaOfInterest::NumberOfAreaOfInterests;
}

bool ImpulseRequest::IsAreaOfInterestPartOfList(AreaOfInterest aoi, std::vector<AreaOfInterest> aoiList)
{
  return std::find(std::begin(aoiList), std::end(aoiList), aoi) != std::end(aoiList);
}

bool ImpulseRequest::IndicatorSwitchedOn(const scm::LightState::Indicator lastState, const scm::LightState::Indicator currentState) const
{
  return lastState == scm::LightState::Indicator::Off && currentState != scm::LightState::Indicator::Off;
}

bool ImpulseRequest::BrakeLightsSwitchedOn(const bool lastState, const bool currentState) const
{
  return lastState == false && currentState;
}