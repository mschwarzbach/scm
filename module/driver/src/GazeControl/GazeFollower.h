/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file GazeFollower.h

#pragma once

#include "GazeControl/GazeFollowerInterface.h"
#include "ScmDefinitions.h"

//! @brief Central unit for handling an external dictation of a time series of gaze targets.
//! @details The GazeFollower implements an externally defined time series of gaze targets,
//! which is dictated as an event from the simulation core for a specific agent at a specific time.
//! The necessary information for starting the gaze event inside of AlgorithmScmMonolithisch comes from SensorDriverScm.
//! The GazeFollower is responsible for recognizing the start of a gaze event and keeping track of its course,
//! for parsing and processing the gaze target time series from the user input file, and for communicating this
//! information to the gaze controller to overwrite the stochastic gaze algorithm.

//! @brief Struct definition for the description of a single gaze target time series point
//! Struct definition for the description of a single gaze target time series point
struct GazeTimeSeriesPoint
{
  //! @brief Relative time step in ms
  units::time::millisecond_t time{-999_ms};
  //! @brief Associated gaze target
  AreaOfInterest gazeTarget{AreaOfInterest::NumberOfAreaOfInterests};
};

//! @brief Namespaces for xml parsing
namespace TAG
{
//! @brief gazeFollowerTimeSeriesPoint
constexpr char gazeFollowerTimeSeriesPoint[]{"GazeFollowerTimeSeriesPoint"};
}  // namespace TAG

namespace ATTRIBUTE
{
//! @brief time
constexpr char time[]{"Time"};
//! @brief gazeTarget
constexpr char gazeTarget[]{"GazeTarget"};
}  // namespace ATTRIBUTE

//! @brief This component takes the GazeControl from oudside
class GazeFollower : public GazeFollowerInterface
{
public:
  //! @brief GazeFollower
  //! @param ownVehicleInformationSCM
  explicit GazeFollower(const OwnVehicleInformationSCM& ownVehicleInformationSCM);
  virtual ~GazeFollower() = default;

  //! @brief Main function for the execution of GazeFollower from AlgorithmScmMonolithischImplementation
  //! @param  time    Current simulation time in ms
  //! @return Struct containing GazeFollower information
  GazeOverwrite EvaluateGazeOverwrite(units::time::millisecond_t time) override;

  bool GetThrowWarning() const override;

protected:
  //! @brief Flag indicating that the GazeFollower is currently processing a gaze target time series
  bool gazeFollowerActive{false};

  //! @brief Struct for communication with GazeControl
  GazeOverwrite gazeOverwrite;

  //! @brief Simulation time when the current gaze event was started (-999 if no gaze event is currently executed)
  units::time::millisecond_t timeOfEventActivation{-999_ms};

  //! @brief Function for processing the gaze target time series regarding output struct gazeOverwrite
  //! @param time - Current simulation time in ms
  virtual void ProcessGazeTargetTimeSeries(units::time::millisecond_t time);

  //! @brief Internal description of the gaze target time series
  std::vector<GazeTimeSeriesPoint> gazeTimeSeries{};

private:
  //! @brief Warning flag for too short gaze target durations
  bool _throwWarning{false};

  //! @brief Function for parsing the gaze target time series from the user input file
  virtual void ParseGazeTargetTimeSeriesFromFile();

  //! @brief ownVehicleInformationSCM
  const OwnVehicleInformationSCM& _ownVehicleInformationSCM;
};
