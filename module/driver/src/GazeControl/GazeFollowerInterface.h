/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file GazeFollowerInterface.h
#pragma once

#include <units.h>

#include "include/common/AreaOfInterest.h"

using namespace units::literals;
//! @brief Struct definition for communication with GazeControl
struct GazeOverwrite
{
  //! @brief Flag indicating that the GazeFollower is currently processing a gaze target time series
  bool gazeFollowerActive{false};
  //! @brief Gaze target that is currently observed or a saccade is currently executed to
  AreaOfInterest gazeTarget{AreaOfInterest::NumberOfAreaOfInterests};
  //! @brief Duration of (current) gaze target defined by time series in ms
  units::time::millisecond_t durationGazeTarget{-999_ms};
  //! @brief Flag indicating that the gaze target has changed in the current time step
  bool hasGazeTargetChanged{false};
  //! @brief Flag indicating that the current gaze target is the last one in the time series
  bool gazeTargetIsLastPointInTimeSeries{false};
};

//! @brief Interface for GazeFollowerInterface class to enable testing of dependent classes.
class GazeFollowerInterface
{
public:
  virtual ~GazeFollowerInterface() = default;

  //! @brief Main function for the execution of GazeFollower from AlgorithmScmMonolithischImplementation
  //! @param  time    Current simulation time in ms
  //! @return Struct containing GazeFollower information
  virtual GazeOverwrite EvaluateGazeOverwrite(units::time::millisecond_t time) = 0;

  //! @brief Get throw warnings
  //! @return yes/no
  virtual bool GetThrowWarning() const = 0;
};