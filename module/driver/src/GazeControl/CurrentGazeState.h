/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  CurrentGazeState.h
#pragma once

#include <memory>

#include "GazeControl/CurrentGazeStateInterface.h"
#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazeFovea.h"
#include "GazeControl/GazePeriphery.h"
#include "GazeControl/GazeUsefulFieldOfView.h"
#include "MentalModelInterface.h"

//! @brief This component update the current gaze state
class CurrentGazeState : public CurrentGazeStateInterface
{
public:
  //! @brief Constructor initializes and creates CurrentGazeState.
  //! @param mentalModel
  //! @param gazePeriphery
  //! @param gazeUsefulFieldOfView
  //! @param gazeFovea
  CurrentGazeState(MentalModelInterface& mentalModel, const GazePeripheryInterface& gazePeriphery, const GazeUsefulFieldOfViewInterface& gazeUsefulFieldOfView, const GazeFoveaInterface& gazeFovea)
      : _mentalModel(mentalModel), _gazePeriphery(gazePeriphery), _gazeUsefulFieldOfView(gazeUsefulFieldOfView), _gazeFovea(gazeFovea)
  {
  }

  GazeStateAndAngle UpdateCurrentGazeState(GazeState plannedGazeState, GazeStateAndAngle gazeStateAndAngle, const SurroundingObjectsSCM& surroundingObjects) const override;

private:
  //! @brief _mentalModel
  MentalModelInterface& _mentalModel;
  //! @brief _gazePeriphery
  const GazePeripheryInterface& _gazePeriphery;
  //! @brief _gazeUsefulFieldOfView
  const GazeUsefulFieldOfViewInterface& _gazeUsefulFieldOfView;
  //! @brief _gazeFovea
  const GazeFoveaInterface& _gazeFovea;
};
