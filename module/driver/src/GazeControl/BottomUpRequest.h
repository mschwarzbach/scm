/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  BottomUpRequest.h

#pragma once
#include <map>
#include <memory>

#include "BottomUpRequestInterface.h"
#include "GazeControl/StimulusRequestInterface.h"
#include "ImpulseRequest.h"
#include "LateralAction.h"
#include "MentalModelInterface.h"
#include "StimulusRequest.h"
#include "include/common/AreaOfInterest.h"

//! @brief This component update the bottom up request from given stimulus
class BottomUpRequest : public BottomUpRequestInterface
{
public:
  //! @brief Constructor initializes and creates BottomUpRequest.
  //! @param stochastics
  //! @param impulseRequest
  //! @param stimulusRequest
  BottomUpRequest(StochasticsInterface* stochastics, ImpulseRequestInterface& impulseRequest, StimulusRequestInterface& stimulusRequest)
      : _stochastics(stochastics), _impulseRequest(impulseRequest), _stimulusRequest(stimulusRequest)
  {
  }

  void Update(const std::vector<int>& knownIds, const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM, const std::vector<AdasHmiSignal>& opticAdasSignals, AuditoryPerceptionInterface& auditoryPerception, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl) override;
  std::map<GazeState, double> GetBottomUpIntensities() const override;

private:
  //! @brief ID of the Front Object of the previous cycle.
  int _prevFrontObjectId = -2000;
  //! @brief bottomUpIntensities
  std::map<GazeState, double> _bottomUpIntensities{};
  //! @brief stochastics
  StochasticsInterface* _stochastics;
  //! @brief impulseRequest
  ImpulseRequestInterface& _impulseRequest;
  //! @brief stimulusRequest
  StimulusRequestInterface& _stimulusRequest;
};