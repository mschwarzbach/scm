/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file GazeControlInterface.h

#pragma once

#include "AuditoryPerception.h"
#include "AuditoryPerceptionInterface.h"
#include "GazeControl/GazeFollower.h"
#include "include/common/ScmEnums.h"

//! @brief Implements the GazeControlInterface
class GazeControlInterface
{
public:
  virtual ~GazeControlInterface() = default;

  //! @brief Helper method for UpdateNewGazeState, taking care of the transition of the current gaze state
  //! @param gazeOverwrite
  virtual void AdvanceGazeState(GazeOverwrite gazeOverwrite) = 0;

  //! @brief Return the current gaze state of the agent
  //! @return Current gaze state of the agent
  virtual GazeState GetCurrentGazeState() const = 0;

  //! @brief Getter for IsNewInformationAcquisitionRequested.
  //! @return isNewInformationAcquisitionRequested
  virtual bool IsNewInformationAcquisitionRequested() const = 0;

  //! @brief Update GazeIntensities, Duration and Sigma durations with updated bottomUp and TopDown Requests
  //! @param actionState
  //! @param topDownRequestMap
  //! @param opticAdasSignals
  //! @param auditoryPerception
  //! @param distractionPercentage
  virtual void UpdateGazeRequests(LateralAction actionState, const std::map<AreaOfInterest, double>& topDownRequestMap, const std::vector<AdasHmiSignal>& opticAdasSignals, const AdasHmiSignal& combinedSignal, AuditoryPerceptionInterface& auditoryPerception, double distractionPercentage) = 0;

  //! @brief Set perceive agents ids
  //! @param knownIds
  virtual void SetKnownObjectIds(const std::vector<int>& knownIds) = 0;

  //! @brief Sets if haf signal has already been processed
  //! @param processedSignal
  virtual void SetHafSignalProcessed(bool processedSignal) = 0;

  //! @brief Checks whether a haf signal has already been processed
  //! @return True if it has been processed, false otherwise
  virtual bool GetHafSignalProcessed() const = 0;
};