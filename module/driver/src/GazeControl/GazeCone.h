/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeCone.h
#pragma once

#include "ScmDefinitions.h"

//! @brief This component check is given point inside the gaze cone
class GazeCone
{
public:
  //! @brief Constructor initializes and creates GazeCone.
  //! @param gazeAngle
  //! @param openingAngle
  GazeCone(units::angle::radian_t gazeAngle, units::angle::radian_t openingAngle);

  //! @brief Returns true if the angle to the point is inside the limits defined by the gaze cone.
  //! @param point
  //! @return yes/no
  bool IsPointInside(const Scm::Point& point) const;

private:
  //! @brief gazeAngle
  const units::angle::radian_t _gazeAngle = 0_rad;
  //! @brief minAngle
  const units::angle::radian_t _minAngle = 0_rad;
  //! @brief maxAngle
  const units::angle::radian_t _maxAngle = 0_rad;
};
