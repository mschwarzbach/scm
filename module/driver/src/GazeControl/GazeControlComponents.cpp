/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GazeControlComponents.h"

#include <memory>

#include "GazeControl/BottomUpRequest.h"
#include "GazeControl/CurrentGazeState.h"
#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazeFovea.h"
#include "GazeControl/GazeFoveaInterface.h"
#include "GazeControl/GazePeriphery.h"
#include "GazeControl/GazeUsefulFieldOfView.h"
#include "GazeControl/ImpulseRequest.h"
#include "GazeControl/OpticalInformation.h"
#include "GazeControl/StimulusRequest.h"
#include "GazeControl/TopDownRequest.h"

GazeControlComponents::GazeControlComponents(MentalModelInterface& mentalModel, StochasticsInterface* stochastics, const FeatureExtractorInterface& featureExtractor)
    : _mentalModel(mentalModel), _stochastics(stochastics), _featureExtractor(featureExtractor)
{
  _impulseRequest = std::make_unique<ImpulseRequest>(_mentalModel);
  _gazeFovea = std::make_unique<GazeFovea>(_mentalModel);
  _gazeFieldQuery = std::make_unique<GazeFieldQuery>(_mentalModel);
  _stimulusRequest = std::make_unique<StimulusRequest>(_mentalModel, _stochastics);
  _topDownRequest = std::make_unique<TopDownRequest>(_featureExtractor, _stochastics, _mentalModel);
  _opticalInformation = std::make_unique<OpticalInformation>(_mentalModel);
  _gazePeriphery = std::make_unique<GazePeriphery>(_mentalModel, *_gazeFieldQuery.get());
  _bottomUpRequest = std::make_unique<BottomUpRequest>(_stochastics, *_impulseRequest.get(), *_stimulusRequest.get());
  _gazeUsefulFieldOfView = std::make_unique<GazeUsefulFieldOfView>(_mentalModel, *_gazeFieldQuery.get());
  _currentGazeState = std::make_unique<CurrentGazeState>(_mentalModel, *_gazePeriphery.get(), *_gazeUsefulFieldOfView.get(), *_gazeFovea.get());
}

BottomUpRequestInterface* GazeControlComponents::GetBottomUpRequest()
{
  return _bottomUpRequest.get();
}

TopDownRequestInterface* GazeControlComponents::GetTopDownRequest()
{
  return _topDownRequest.get();
}

ImpulseRequestInterface* GazeControlComponents::GetImpulseRequest()
{
  return _impulseRequest.get();
}

StimulusRequestInterface* GazeControlComponents::GetStimulusRequest()
{
  return _stimulusRequest.get();
}

GazeFieldQueryInterface* GazeControlComponents::GetGazeFieldQuery()
{
  return _gazeFieldQuery.get();
}

GazePeripheryInterface* GazeControlComponents::GetGazePeriphery()
{
  return _gazePeriphery.get();
}

OpticalInformationInterface* GazeControlComponents::GetOpticalInformation()
{
  return _opticalInformation.get();
}

GazeUsefulFieldOfViewInterface* GazeControlComponents::GetUsefulFieldOfView()
{
  return _gazeUsefulFieldOfView.get();
}

GazeFoveaInterface* GazeControlComponents::GetGazeFovea()
{
  return _gazeFovea.get();
}

CurrentGazeStateInterface* GazeControlComponents::GetCurrentGazeState()
{
  return _currentGazeState.get();
}