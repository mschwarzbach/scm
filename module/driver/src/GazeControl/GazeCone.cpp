/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GazeCone.h"

units::angle::radian_t GetCorrectAngleDueToQuadrantShift(units::angle::radian_t angle)
{
  if (angle.value() < -M_PI)
  {
    angle += 2_rad * M_PI;
  }

  if (angle.value() > M_PI)
  {
    angle -= 2_rad * M_PI;
  }

  return angle;
}

GazeCone::GazeCone(units::angle::radian_t gazeAngle, units::angle::radian_t openingAngle)
    : _gazeAngle(gazeAngle),
      _minAngle(GetCorrectAngleDueToQuadrantShift(gazeAngle - openingAngle / 2)),
      _maxAngle(GetCorrectAngleDueToQuadrantShift(gazeAngle + openingAngle / 2))
{
}

bool GazeCone::IsPointInside(const Scm::Point& point) const
{
  const units::angle::radian_t angleToPoint{units::math::atan2(point.t, point.s)};

  if (_minAngle > _maxAngle)
  {
    return angleToPoint >= _minAngle ||
           angleToPoint <= _maxAngle;
  }

  return angleToPoint >= _minAngle &&
         angleToPoint <= _maxAngle;
}
