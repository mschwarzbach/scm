/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "CurrentGazeState.h"

GazeStateAndAngle CurrentGazeState::UpdateCurrentGazeState(GazeState plannedGazeState, GazeStateAndAngle gazeStateAndAngle, const SurroundingObjectsSCM& surroundingObjects) const
{
  if (plannedGazeState != gazeStateAndAngle.currentGazeState)
  {
    gazeStateAndAngle.currentGazeState = plannedGazeState;
    if (gazeStateAndAngle.currentGazeState != GazeState::SACCADE)
    {
      _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->fovea = ScmCommons::MapGazeStateToAreaOfInterest(gazeStateAndAngle.currentGazeState);
      gazeStateAndAngle.gazeAngleHorizontal = _gazeFovea.GetFoveaGazeAngleHorizontal(gazeStateAndAngle.currentGazeState, surroundingObjects);
      _mentalModel.SetHorizontalGazeAngle(gazeStateAndAngle.gazeAngleHorizontal);

      _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->ufov = _gazeUsefulFieldOfView.GetUsefulFieldOfViewAois(surroundingObjects, gazeStateAndAngle.gazeAngleHorizontal);
      _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->periphery = _gazePeriphery.GetPeripheryAois(surroundingObjects, gazeStateAndAngle.gazeAngleHorizontal);
    }
  }

  return gazeStateAndAngle;
}
