/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  StimulusRequestInterface.h
#pragma once

#include <map>

#include "AuditoryPerceptionInterface.h"
#include "GazeControl/GazeControlInterface.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Implements the StimulusRequestInterface
class StimulusRequestInterface
{
public:
  virtual ~StimulusRequestInterface() = default;
  //! @brief update the stimulus request in front
  //! @param gazeStateIntensities
  //! @param knownObjectIds
  //! @param surroundingObjects
  //! @param ownVehicleInformationSCM
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateStimulusDataFront(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<int>& knownObjectIds, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM) = 0;

  //! @brief update the stimulus request in side
  //! @param gazeStateIntensities
  //! @param surroundingObjects
  //! @param knownObjectIds
  //! @param ownVehicleInformationSCM
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateStimulusDataSide(const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const std::vector<int>& knownObjectIds, const OwnVehicleInformationSCM& ownVehicleInformationSCM) = 0;

  //! @brief update the stimulus request from acoustic data
  //! @param gazeStateIntensities
  //! @param auditoryPerception
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateStimulusDataAcoustic(const std::map<GazeState, double>& gazeStateIntensities, const AuditoryPerceptionInterface& auditoryPerception) = 0;

  //! @brief update the stimulus request from optical data
  //! @param gazeStateIntensities
  //! @param opticAdasSignals
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateStimulusDataOptical(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<AdasHmiSignal> opticAdasSignals) = 0;
  virtual std::map<GazeState, double> UpdateStimulusDataCombinedSignal(std::map<GazeState, double>& gazeStateIntensities, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl) = 0;
};