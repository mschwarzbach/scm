/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  GazeControl.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include <memory>
#include <optional>

#include "AuditoryPerceptionInterface.h"
#include "BottomUpRequest.h"
#include "FeatureExtractorInterface.h"
#include "GazeControl/BottomUpRequestInterface.h"
#include "GazeControl/CurrentGazeStateInterface.h"
#include "GazeControl/GazeControlComponentsInterface.h"
#include "GazeControl/GazeControlInterface.h"
#include "GazeControl/GazeFieldQueryInterface.h"
#include "GazeControl/GazeFoveaInterface.h"
#include "GazeControl/GazeParameters.h"
#include "GazeControl/GazePeripheryInterface.h"
#include "GazeControl/GazeUsefulFieldOfViewInterface.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "GazeControlInterface.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "TopDownRequestInterface.h"
#include "include/ScmSampler.h"

//! @brief controlls the gaze state of the driver by implementing gaze intensities.
//! The gaze control implements the Gaze state Markov model given transition probabilities
//! in the form of intensities over GazeState. The class implements
//! feedback between actions and gaze control.
//! One salient example is the
//! INTENT action, which generates high intensities for gazing in directions that are
//! important for completing the maneuver in question. For instance, when the intent
//! is to change lanes to the right, the gaze state intensity will be high for those areas of interest
//! on the right that have not recently been observed.
class GazeControl : public GazeControlInterface
{
public:
  //! @brief Constructor initializes and creates GazeControl.
  //! @param mentalModel
  //! @param stochastics
  //! @param featureExtractor
  //! @param surroundingObjects
  //! @param ownVehicleInformationSCM
  //! @param idealPerception
  //! @param gazeControlComponents
  GazeControl(MentalModelInterface& mentalModel,
              StochasticsInterface* stochastics,
              const FeatureExtractorInterface& featureExtractor,
              SurroundingObjectsSCM& surroundingObjects,
              const OwnVehicleInformationSCM& ownVehicleInformationSCM,
              bool idealPerception,
              GazeControlComponentsInterface& gazeControlComponents);

  void AdvanceGazeState(GazeOverwrite gazeOverwrite) override;
  GazeState GetCurrentGazeState() const override;
  bool IsNewInformationAcquisitionRequested() const override;
  void UpdateGazeRequests(LateralAction actionState, const std::map<AreaOfInterest, double>& topDownRequestMap, const std::vector<AdasHmiSignal>& opticAdasSignals, const AdasHmiSignal& combinedSignal, AuditoryPerceptionInterface& auditoryPerception, double distractionPercentage) override;
  void SetKnownObjectIds(const std::vector<int>& knownIds) override;
  void SetHafSignalProcessed(bool processedSignal) override;
  bool GetHafSignalProcessed() const override;

protected:
  //! @brief Handling overwrite of random gaze algorithm due to active GazeFollower
  //! @param gazeOverwrite    Struct containing necessary information from GazeFollower
  void OverwriteGazeBehaviour(GazeOverwrite gazeOverwrite);

  //! @brief Initialize a gaze state for the first time.
  void InitializeGazeState();

  //! @brief Update gaze state and field of view, when changed by decision in the previous cycle.
  void UpdateNewGazeState();

  //! @brief Chooses next gaze state, but does not set it yet.
  void ChooseNextGazeState();

  //! @brief Stochastic duration of a saccade.
  //! @return Duration of a saccade in [ms]
  units::time::millisecond_t DrawSaccadeDuration();

  //! @brief Rounds the rolled duration of a fixation target or saccade depending on the cycle time.
  //! @param duration
  //! @param minimumDuration
  //! @return Rounded duration in [ms]
  units::time::millisecond_t RoundGazeStateDurationToCycleTime(units::time::millisecond_t duration, units::time::millisecond_t minimumDuration = 0_ms) const;

  //! @brief InformationCycles
  //! @param cycleTime
  //! @param startTime
  //! @param endTime
  //! @return InformationCycles
  std::vector<int> GenerateInformationCycles(const units::time::millisecond_t startTime, const units::time::millisecond_t endTime);
  
  //! @brief knownObjectIds
  std::vector<int> _knownObjectIds{};

  //! @brief Duration of the actual gaze state [ms].
  units::time::millisecond_t _durationCurrentGazeState = 0_ms;

  //! @brief Planned gaze state (already chosen, but set next cycle time)
  GazeState _plannedGazeState{GazeState::SACCADE};

  //! @brief Current gaze state.
  GazeState _currentGazeState{GazeState::SACCADE};

  //! @brief Upcoming gaze state.
  GazeState _nextFixationTarget{GazeState::EGO_FRONT};

  //! @brief Minimum duration of a gaze state [ms].
  units::time::millisecond_t _requiredDurationCurrentGazeState = 0_ms;

  //! @brief Variable that indicates intial run
  bool _isInInitState = false;

  //! @brief Size of the time increment in [ms].
  units::time::millisecond_t _cycleTime = 0_ms;

  //! @brief Horizontal Angle of the current gaze direction [rad].
  units::angle::radian_t _gazeAngleHorizontal{0._rad};

  //! @brief Previous gaze fixation target.
  GazeState _previousFixationTarget{GazeState::EGO_FRONT};

  //! @brief surroundingObjects
  SurroundingObjectsSCM& _surroundingObjects;

  //! @brief Gaze state intensity map [].
  std::map<GazeState, double> _gazeStateIntensities{};

  //! @brief Trigger for acquisition of new information.
  bool _isNewInformationAcquisitionRequested = false;

private:
  //! @brief Stochastic duration of gaze state freeze.
  //! @return Duration of the gaze state in [ms]
  units::time::millisecond_t DrawFixationDuration();

  //! @brief Stochastic duration of distraction freeze
  //! @return Duration of the distraction in [ms]
  units::time::millisecond_t DrawDurationDistraction();

  //! @brief Draw fixation gaze state according to the gaze state intensities map.
  //! @return Gaze state
  GazeState DrawFixation();

  //! @brief Set intensities and durations for the gaze states in lane keeping without leading vehicle
  void SetIntensitiesAndDurationsForLaneKeepingWithoutLeadingVehicle();

  //! @brief Vector of cycles with information acquisition.
  std::vector<int> _informationCycles{};

  //! @brief Flag indicating if a HAF signal is already processed
  bool _hafSignalProcessed = false;

  std::map<GazeState, GazeParameters::Distribution> _meanAndSigmaDurations{};
  //! @brief ownVehicleInformationSCM
  const OwnVehicleInformationSCM& _ownVehicleInformationSCM;
  //! @brief idealPerception
  const bool _idealPerception;
  //! @brief mentalModel
  MentalModelInterface& _mentalModel;
  //! @brief stochastics
  StochasticsInterface* _stochastics = nullptr;
  //! @brief featureExtractor
  const FeatureExtractorInterface& _featureExtractor;
  //! @brief topDownRequest
  const TopDownRequestInterface& _topDownRequest;
  //! @brief opticalInformation
  const OpticalInformationInterface& _opticalInformation;
  //! @brief updateCurrentGazeState
  const CurrentGazeStateInterface& _updateCurrentGazeState;
  //! @brief bottomUpRequest
  BottomUpRequestInterface& _bottomUpRequest;
};