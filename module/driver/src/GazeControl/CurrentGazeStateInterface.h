/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  CurrentGazeStateInterface.h
#pragma once

#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Struct holds parameter for the current gazeState and gazeAngel horizontal
struct GazeStateAndAngle
{
  //! @brief currentGazeState
  GazeState currentGazeState;
  //! @brief gazeAngleHorizontal
  units::angle::radian_t gazeAngleHorizontal;
};

//! @brief Interface for CurrentGazeState class to enable testing of dependent classes.
class CurrentGazeStateInterface
{
public:
  virtual ~CurrentGazeStateInterface() = default;

  //! @brief Helper method for UpdateNewGazeState, taking care of the transition of the current gaze state
  //! @param plannedGazeState
  //! @param gazeStateAndAngle
  //! @param surroundingObjects
  //! @return the current gazeState and angle
  virtual GazeStateAndAngle UpdateCurrentGazeState(GazeState plannedGazeState, GazeStateAndAngle gazeStateAndAngle, const SurroundingObjectsSCM& surroundingObjects) const = 0;
};