/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazePeriphery.h
#pragma once

#include <memory>

#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazePeripheryInterface.h"
#include "MentalModelInterface.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Get the periphery aois from the current gaze
class GazePeriphery : public GazePeripheryInterface
{
public:
  //! @brief specialized constructor
  //! @param mentalModel
  //! @param gazeFieldQuery
  GazePeriphery(MentalModelInterface& mentalModel, GazeFieldQueryInterface& gazeFieldQuery)
      : _mentalModel(mentalModel), _gazeFieldQuery(gazeFieldQuery)
  {
  }

  std::vector<AreaOfInterest> GetPeripheryAois(const SurroundingObjectsSCM& surroundingObjectsLocal, units::angle::radian_t gazeAngleHorizontal) const override;

protected:
  //! @brief Generates periphery for front and sides
  //! @param periphery
  //! @param ongoingTraffic
  //! @param gazeAngleHorizontal
  //! @return vector with Periphery Aois
  std::vector<AreaOfInterest> GeneratePeripheryForFrontAndSides(std::vector<AreaOfInterest>& periphery, const OngoingTrafficObjectsSCM& ongoingTraffic, units::angle::radian_t gazeAngleHorizontal) const;

private:
  //! @brief Interface to the MentalModel
  MentalModelInterface& _mentalModel;
  //! @brief Interface to the GazeFieldQuery
  GazeFieldQueryInterface& _gazeFieldQuery;
};