/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  StimulusRequest.h
#pragma once
#include "GazeControl/GazeControlInterface.h"
#include "MentalModelInterface.h"
#include "StimulusRequestInterface.h"

//! @brief this class determine the stimulus request
class StimulusRequest : public StimulusRequestInterface
{
public:
  //! @brief Constructor initializes and creates StimulusRequest.
  //! @param mentalModel
  //! @param stochastics
  StimulusRequest(MentalModelInterface& mentalModel, StochasticsInterface* stochastics)
      : _mentalModel(mentalModel), _stochastics(stochastics)
  {
  }

  std::map<GazeState, double> UpdateStimulusDataFront(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<int>& knownObjectIds, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM) override;
  std::map<GazeState, double> UpdateStimulusDataSide(const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const std::vector<int>& knownObjectIds, const OwnVehicleInformationSCM& ownVehicleInformationSCM) override;
  std::map<GazeState, double> UpdateStimulusDataAcoustic(const std::map<GazeState, double>& gazeStateIntensities, const AuditoryPerceptionInterface& auditoryPerception) override;
  std::map<GazeState, double> UpdateStimulusDataOptical(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<AdasHmiSignal> opticAdasSignals) override;
  std::map<GazeState, double> UpdateStimulusDataCombinedSignal(std::map<GazeState, double>& gazeStateIntensities, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl) override;

protected:
  //! @brief is data outdated
  //! @param id
  //! @return yes/no
  bool IsDataOutdated(int id) const;

  //! Duration a presented optical ADAS signal is detectable to the driver due to intersection with his field of view in ms.
  units::time::millisecond_t _durationOpticStimulusDetectable{-100_ms};
  units::time::millisecond_t _durationCombinedStimulusDetectable{-100_ms};

  bool PeripheralPerceptionOfLateralMotion(const ObjectInformationSCM* object, bool isRight);
  bool IsObjectPerceivedAsStimuli(AreaOfInterest aoi, units::time::second_t ttcThreshold, double tauDotThreshold, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM) const;

private:
  //! Gaze intensity when it IsUrgentlyLateWithUpdate
  static constexpr double HIGH_GAZE_INTENSITY = 2.;

  //! Mean value of the human processing time for optical signals in ms.
  static constexpr auto MEAN_OPTIC_PROCESSING_TIME{292_ms};

  //! Standard deviation of the human processing time for optical signals in ms.
  static constexpr auto SIGMA_OPTIC_PROCESSING_TIME{88_ms};

  //! Position of a presented optical ADAS signal expressed as AreaOfInterest.
  AreaOfInterest _opticAdasSignalPosition{AreaOfInterest::NumberOfAreaOfInterests};

  //! Human processing time for optical signals in ms.
  units::time::millisecond_t _opticProcessingTime{MEAN_OPTIC_PROCESSING_TIME};

  bool IsNewObject(int id, const std::vector<int>& knownObjectIds) const;

  //! @brief Should receive new BottomUpRequest
  //! @param aoi
  //! @param surroundingObjects
  //! @return yes/no
  bool ShouldReceiveNewBottomUpRequest(AreaOfInterest aoi, const SurroundingObjectsSCM* surroundingObjects);

  //! @brief determine ttc threshold
  //! @param opticalAngleSizeHorizontal
  //! @param opticalAngleTowardsViewAxis
  //! @param driverParameters
  //! @return ttc threshold
   units::time::second_t CalculateTtcThreshold(units::angle::radian_t opticalAngleSizeHorizontal, units::angle::radian_t opticalAngleTowardsViewAxis, DriverParameters driverParameters) const;

  //! @brief determine ttc
  //! @param vDeltaObservedMinusEgo
  //! @param relativeNetDistance
  //! @return ttc
  units::time::second_t CalculateTimeToCollision(units::velocity::meters_per_second_t vDeltaObservedMinusEgo, units::length::meter_t relativeNetDistance) const;
  
  //! @brief determine TauDot
  //! @param vDeltaFrontMinusRear
  //! @param relativeNetDistance
  //! @param aDeltaFrontMinusRear
  //! @param aoi
  //! @return TauDot
  double CalculateTauDot(double vDeltaFrontMinusRear, double relativeNetDistance, double aDeltaFrontMinusRear, AreaOfInterest aoi) const;

  //! @brief translate of spot of presentation to direction
  //! @param spotOfPresentation
  //! @return Aoi of spot of presentation to direction
  AreaOfInterest TranslateSpotOfPresentationToDirection(std::string spotOfPresentation);

  //! @brief check is given aoi part of given list
  //! @param aoi
  //! @param aoiList
  //! @return yes/no
  bool IsAreaOfInterestPartOfList(AreaOfInterest aoi, std::vector<AreaOfInterest> aoiList);

  //! @brief mentalModel
  MentalModelInterface& _mentalModel;
  //! @brief stochastics
  StochasticsInterface* _stochastics;
};