/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "BottomUpRequest.h"

void BottomUpRequest::Update(const std::vector<int>& knownIds, const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM, const std::vector<AdasHmiSignal>& opticAdasSignals, AuditoryPerceptionInterface& auditoryPerception, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl)
{
  _bottomUpIntensities = gazeStateIntensities;
  _bottomUpIntensities = _impulseRequest.UpdateImpulseDataFront(_bottomUpIntensities, &surroundingObjects.ongoingTraffic.Ahead().Right(), AreaOfInterest::RIGHT_FRONT);
  _bottomUpIntensities = _impulseRequest.UpdateImpulseDataFront(_bottomUpIntensities, &surroundingObjects.ongoingTraffic.Ahead().Left(), AreaOfInterest::LEFT_FRONT);
  _bottomUpIntensities = _impulseRequest.UpdateImpulseDataFront(_bottomUpIntensities, &surroundingObjects.ongoingTraffic.FarAhead().Right(), AreaOfInterest::RIGHT_FRONT_FAR);
  _bottomUpIntensities = _impulseRequest.UpdateImpulseDataFront(_bottomUpIntensities, &surroundingObjects.ongoingTraffic.FarAhead().Left(), AreaOfInterest::LEFT_FRONT_FAR);

  _bottomUpIntensities = _impulseRequest.UpdateImpulseDataEgoFront(_bottomUpIntensities, &surroundingObjects.ongoingTraffic.Ahead().Close());

  _bottomUpIntensities = _stimulusRequest.UpdateStimulusDataFront(_bottomUpIntensities, knownIds, surroundingObjects, ownVehicleInformationSCM);
  _bottomUpIntensities = _stimulusRequest.UpdateStimulusDataSide(_bottomUpIntensities, surroundingObjects, knownIds, ownVehicleInformationSCM);

  _bottomUpIntensities = _stimulusRequest.UpdateStimulusDataAcoustic(_bottomUpIntensities, auditoryPerception);
  _bottomUpIntensities = _stimulusRequest.UpdateStimulusDataOptical(_bottomUpIntensities, opticAdasSignals);
  _bottomUpIntensities = _stimulusRequest.UpdateStimulusDataCombinedSignal(_bottomUpIntensities, combinedSignal, gazeControl);
}

std::map<GazeState, double> BottomUpRequest::GetBottomUpIntensities() const
{
  return _bottomUpIntensities;
}