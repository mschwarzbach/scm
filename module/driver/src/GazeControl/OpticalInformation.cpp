/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "OpticalInformation.h"

#include "GazeControl/GazeParameters.h"

OpticalValues OpticalInformation::SupplementOpticalInformation(SurroundingObjectsSCM surroundingObjects, units::angle::radian_t gazeAngleHorizontal, bool isInit, bool idealPerception) const
{
  const bool forceIdealPerception{isInit || idealPerception};

  auto aoiObjects = MapSurroundingAoiObjectsGroundTruth(surroundingObjects);

  for (auto& [aoi, objectData] : aoiObjects)
  {
    if (forceIdealPerception || !objectData->empty())
    {
      for (auto& data : *objectData)
      {
        std::vector<units::angle::radian_t> orientationsRelativeToDriver = {data.orientationRelativeToDriver.frontLeft,
                                                                            data.orientationRelativeToDriver.frontRight,
                                                                            data.orientationRelativeToDriver.rearLeft,
                                                                            data.orientationRelativeToDriver.rearRight};

        OpticalAngle opticalAngle{CalculateOpticalInformationForSurroundingObject(isInit, orientationsRelativeToDriver, aoi, gazeAngleHorizontal, idealPerception)};

        data = AdjustCurrentObjectOpticalAngleAndThresholdLooming(_mentalModel.IsFovea(aoi) || forceIdealPerception, data, opticalAngle);
      }
    }
  }
  OpticalValues opticalValues{
      .surroundingObjects = surroundingObjects,
      .gazeAngleHorizontal = gazeAngleHorizontal};

  return opticalValues;
}

std::map<AreaOfInterest, std::vector<ObjectInformationSCM>*> OpticalInformation::MapSurroundingAoiObjectsGroundTruth(SurroundingObjectsSCM& surroundingObjects_GroundTruth) const
{
  std::map<AreaOfInterest, std::vector<ObjectInformationSCM>*> aoiObjects;

  aoiObjects[AreaOfInterest::LEFT_FRONT] = &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Left();
  aoiObjects[AreaOfInterest::RIGHT_FRONT] = &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Right();
  aoiObjects[AreaOfInterest::LEFT_REAR] = &surroundingObjects_GroundTruth.ongoingTraffic.Behind().Left();
  aoiObjects[AreaOfInterest::RIGHT_REAR] = &surroundingObjects_GroundTruth.ongoingTraffic.Behind().Right();
  aoiObjects[AreaOfInterest::EGO_FRONT] = &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Close();
  aoiObjects[AreaOfInterest::EGO_REAR] = &surroundingObjects_GroundTruth.ongoingTraffic.Behind().Close();
  aoiObjects[AreaOfInterest::LEFT_SIDE] = &surroundingObjects_GroundTruth.ongoingTraffic.Close().Left();
  aoiObjects[AreaOfInterest::RIGHT_SIDE] = &surroundingObjects_GroundTruth.ongoingTraffic.Close().Right();
  aoiObjects[AreaOfInterest::LEFTLEFT_FRONT] = &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().FarLeft();
  aoiObjects[AreaOfInterest::RIGHTRIGHT_FRONT] = &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().FarRight();
  aoiObjects[AreaOfInterest::LEFTLEFT_REAR] = &surroundingObjects_GroundTruth.ongoingTraffic.Behind().FarLeft();
  aoiObjects[AreaOfInterest::RIGHTRIGHT_REAR] = &surroundingObjects_GroundTruth.ongoingTraffic.Behind().FarRight();
  aoiObjects[AreaOfInterest::LEFTLEFT_SIDE] = &surroundingObjects_GroundTruth.ongoingTraffic.Close().FarLeft();
  aoiObjects[AreaOfInterest::RIGHTRIGHT_SIDE] = &surroundingObjects_GroundTruth.ongoingTraffic.Close().FarRight();
  return aoiObjects;
}

OpticalAngle OpticalInformation::CalculateOpticalInformationForSurroundingObject(bool isInit, const std::vector<units::angle::radian_t>& orientationsRelativeToDriver, const AreaOfInterest aoi, units::angle::radian_t gazeAngleHorizontal, bool idealPerception) const
{
  GazeOrientations gazeOrientations{
      .evaluateMirror = IsMirrorAreaOfInterest(aoi),
      .opticalAnglePositionHorizontal = units::make_unit<units::angle::radian_t>(-ScmDefinitions::DEFAULT_VALUE),
      .opticalAngleSizeHorizontal = units::make_unit<units::angle::radian_t>(-ScmDefinitions::DEFAULT_VALUE),
      .minimumOrientationRelativToViewAxis = units::make_unit<units::angle::radian_t>(-ScmDefinitions::DEFAULT_VALUE),
      .maximumOrientationRelativToViewAxis = units::make_unit<units::angle::radian_t>(ScmDefinitions::DEFAULT_VALUE)};

  gazeOrientations = AnalyzeOrientationsRelativeToDriver(isInit, orientationsRelativeToDriver, gazeAngleHorizontal, gazeOrientations, idealPerception);

  gazeOrientations.opticalAnglePositionHorizontal = AdjustOpticalAnglePositionHorizontal(gazeOrientations);
  gazeOrientations.opticalAngleSizeHorizontal = AdjustOpticalAngleSizeHorizontal(gazeOrientations);

  // Correct optical angle position for mirror fixation
  if (gazeOrientations.evaluateMirror)
  {
    gazeOrientations.opticalAnglePositionHorizontal = CorrectOpticalAnglePositionForMirror(aoi, gazeOrientations.opticalAnglePositionHorizontal, gazeAngleHorizontal);
  }

  return {.opticalAnglePositionHorizontal = gazeOrientations.opticalAnglePositionHorizontal, .opticalAngleSizeHorizontal = gazeOrientations.opticalAngleSizeHorizontal};
}

ObjectInformationSCM OpticalInformation::AdjustCurrentObjectOpticalAngleAndThresholdLooming(bool IsFoveaOrSpawn, const ObjectInformationSCM& currentObjectInformationSCM, const OpticalAngle& opticalAngle) const
{
  auto outputCurrentObjectInformation = currentObjectInformationSCM;
  if (IsFoveaOrSpawn)
  {
    outputCurrentObjectInformation.opticalAnglePositionHorizontal = 0._rad;
    outputCurrentObjectInformation.thresholdLooming = _mentalModel.GetDriverParameters().thresholdLoomingFovea.value_or(ScmDefinitions::DEFAULT_VALUE);
  }
  else
  {
    outputCurrentObjectInformation.opticalAnglePositionHorizontal = opticalAngle.opticalAnglePositionHorizontal;
    outputCurrentObjectInformation.thresholdLooming = _mentalModel.GetDriverParameters().thresholdLoomingFovea.value_or(ScmDefinitions::DEFAULT_VALUE) * ScmCommons::CalculateCorticalMagnificationFactor(opticalAngle.opticalAnglePositionHorizontal);
  }

  outputCurrentObjectInformation.opticalAngleSizeHorizontal = opticalAngle.opticalAngleSizeHorizontal;

  return outputCurrentObjectInformation;
}

bool OpticalInformation::IsMirrorAreaOfInterest(const AreaOfInterest aoi) const
{
  return LaneQueryHelper::IsRearArea(aoi);
}

GazeOrientations OpticalInformation::AnalyzeOrientationsRelativeToDriver(bool isInit, const std::vector<units::angle::radian_t>& orientationsRelativeToDriver, units::angle::radian_t gazeAngleHorizontal, const GazeOrientations& gazeOrientations, bool idealPerception) const
{
  // Virtually move current gaze direction to the back if fixating a mirror
  auto gazeAngleToCompare{gazeOrientations.evaluateMirror ? units::make_unit<units::angle::radian_t>(M_PI) : gazeAngleHorizontal};
  auto outputGazeOrientations = gazeOrientations;
  for (const auto orientationRelativeToDriver : orientationsRelativeToDriver)
  {
    auto orientationRelativToViewAxis = orientationRelativeToDriver - gazeAngleToCompare;

    orientationRelativToViewAxis = ApplyHemicycleChanges(orientationRelativToViewAxis);

    // Bound to field of view boundaries if fixating object directly (ignore for spawn time step)
    if (!(outputGazeOrientations.evaluateMirror || isInit || idealPerception))
    {
      orientationRelativToViewAxis = units::math::fmin(GazeParameters::HORIZONTAL_SIZE_PERIPHERY / 2, units::math::max(orientationRelativToViewAxis, -GazeParameters::HORIZONTAL_SIZE_PERIPHERY / 2));
    }

    if (units::math::fabs(orientationRelativToViewAxis) < outputGazeOrientations.opticalAnglePositionHorizontal)
    {
      outputGazeOrientations.opticalAnglePositionHorizontal = orientationRelativToViewAxis;
    }
    if (orientationRelativToViewAxis < outputGazeOrientations.minimumOrientationRelativToViewAxis)
    {
      outputGazeOrientations.minimumOrientationRelativToViewAxis = orientationRelativToViewAxis;
    }
    if (orientationRelativToViewAxis > outputGazeOrientations.maximumOrientationRelativToViewAxis)
    {
      outputGazeOrientations.maximumOrientationRelativToViewAxis = orientationRelativToViewAxis;
    }
  }

  return outputGazeOrientations;
}

units::angle::radian_t OpticalInformation::AdjustOpticalAngleSizeHorizontal(const GazeOrientations& gazeOrientations) const
{
  return gazeOrientations.maximumOrientationRelativToViewAxis - gazeOrientations.minimumOrientationRelativToViewAxis;
}

units::angle::radian_t OpticalInformation::AdjustOpticalAnglePositionHorizontal(const GazeOrientations& gazeOrientations) const
{
  // Move optical angle position to view axis if projection intersects with view axis
  if (gazeOrientations.minimumOrientationRelativToViewAxis < 0_rad && gazeOrientations.maximumOrientationRelativToViewAxis > 0_rad)
  {
    return 0._rad;
  }

  return gazeOrientations.opticalAnglePositionHorizontal;
}

units::angle::radian_t OpticalInformation::CorrectOpticalAnglePositionForMirror(AreaOfInterest aoi, units::angle::radian_t opticalAnglePosition, units::angle::radian_t gazeAngleHorizontal) const
{
  units::angle::radian_t mirrorOrientationRelativeToDriver;
  switch (aoi)
  {
    case AreaOfInterest::EGO_REAR:
      mirrorOrientationRelativeToDriver = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->InteriorMirrorOrientationRelativeToDriver;
      break;
    case AreaOfInterest::LEFT_REAR:
    case AreaOfInterest::LEFTLEFT_REAR:
      mirrorOrientationRelativeToDriver = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->LeftMirrorOrientationRelativeToDriver;
      break;
    case AreaOfInterest::RIGHT_REAR:
    case AreaOfInterest::RIGHTRIGHT_REAR:
      mirrorOrientationRelativeToDriver = _mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->RightMirrorOrientationRelativeToDriver;
      break;
    default:
      return opticalAnglePosition;
  }
  return mirrorOrientationRelativeToDriver - gazeAngleHorizontal;
}

units::angle::radian_t OpticalInformation::ApplyHemicycleChanges(units::angle::radian_t orientationRelativToViewAxis) const
{
  if (orientationRelativToViewAxis > units::make_unit<units::angle::radian_t>(M_PI))
  {
    orientationRelativToViewAxis -= 2_rad * M_PI;
  }
  else if (orientationRelativToViewAxis < units::make_unit<units::angle::radian_t>(-M_PI))
  {
    orientationRelativToViewAxis += 2_rad * M_PI;
  }
  return orientationRelativToViewAxis;
}