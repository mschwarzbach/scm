/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file MentalModel.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "ActionManager.h"
#include "HighCognitive.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "LateralAction.h"
#include "MentalModelDefinitions.h"
#include "MentalModelInterface.h"
#include "Merging/Merging.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "ScmDependencies.h"
#include "SurroundingVehicles/SurroundingVehicle.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "TrafficFlow/TrafficFlowInterface.h"
#include "include/common/ScmDefinitions.h"

class ActionManager;
class MentalCalculationsInterface;
class HighCognitive;
class FeatureExtractorInterface;

//! @brief The MentalModel represents the mental processes of the stochastic cognitive model (SCM).
//! @details This central module stores and holds a lot of data and information about the cognitive processes and provides access for other modules.
//! Implements the MentalModelInterface
class MentalModel : public MentalModelInterface
{
public:
  //! @brief Constructor initializes and creates most of the mental model (storage).
  //! @details Keep in mind that the separate Initialize(..) method has to be called before the MentalModel is fully initialized.
  //! This is needed as the MentalModel needs information of other modules which have also access to the mental model storage.
  //! @param cycleTime the cycle time (time steps) of the simulation
  //! @param featureExtractor Interface to the featureExtractor; extracts features (mostly based on data of the mental model) in the vicinity of the driver
  //! @param mentalCalculations Interface to the mentalCalculations; provides calculations for the mental processes of the mental model
  //! @param scmDependencies Interface to the ScmDependencies; mainly sensor, parameter and vehicle data.
  MentalModel(units::time::millisecond_t cycleTime, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies);

  MentalModel(const MentalModel&) = delete;
  MentalModel(MentalModel&&) = delete;
  MentalModel& operator=(const MentalModel&) = delete;
  MentalModel& operator=(MentalModel&&) = delete;
  ~MentalModel() override = default;
  void Initialize(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleParameters, units::velocity::meters_per_second_t initVEgo, StochasticsInterface* stochastics) override;

  bool CheckForEgoLaneTransition() override;

  void Transition() override;

  void ResetReactionBaseTime() override;

  void SetReactionBaseTime(units::time::second_t time) override;

  void AdvanceReactionBaseTime() override;

  void CalculateAdjustmentBaseTime(ActionImplementationInterface* actionImplementation) override;

  bool IsAdjustmentBaseTimeExpired() const override;

  void AdvanceAdjustmentBaseTime() override;

  bool IsLateWithFrontUpdate() const override;

  bool IsUrgentlyLateWithUpdate(GazeState gs) const override;

  bool IsUrgentlyLateWithUpdate(AreaOfInterest aoi) const override;

  bool IsInconsistent(AreaOfInterest aoi) const override;

  void MarkInconsistent(AreaOfInterest aoi) override;

  void ClearInconsistency(AreaOfInterest aoi) override;

  const std::vector<AreaOfInterest>& GetInconsistentAois() const override;

  bool IsEarlyWithUpdate(GazeState gs) const override;

  bool IsEarlyWithUpdate(AreaOfInterest aoi) const override;

  bool IsLeadingVehicleAnticipatedLaneChanger() const override;

  bool IsAbleToCooperate() const override;

  bool IsAbleToCooperateByAccelerating() const override;

  void ResetMergeRegulate() override;

  bool IsMergePreparationActive() const override;

  void CalculateVLegal(bool spawn) override;

  void CheckForEndOfLaneChange(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, bool TrajectoryPlanningCompleted) override;

  void CheckSituationCooperativeBehavior() override;

  bool ImplementStochasticActivation(units::time::millisecond_t startTime, units::time::millisecond_t endTime, units::time::millisecond_t durationSinceInitiation, bool hasStateChanged, double probActivation, double maxProb, double minProb, bool activationState) const override;

  bool IsObjectInRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const override;

  bool IsObjectInSideArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const override;

  bool IsObjectInSurroundingArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool includeSideSideLanes) const override;

  bool IsObjectInSideOrRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool isSide) const override;

  bool IsInsideVehicleArea(AreaOfInterest aoi) const override;

  std::vector<double> AddInformationRequest(AreaOfInterest aoi, FieldOfViewAssignment sufficiencyLevel, double priority, InformationRequestTrigger requester) const override;

  void ResetInformationRequests() override;

  void ResetInformationRequestLevel(AreaOfInterest aoi, FieldOfViewAssignment sufficientViewAssignment) override;

  std::map<AreaOfInterest, double> ScaleTopDownRequestMap(std::map<AreaOfInterest, double> map) const override;

  bool CompareInformationRequest(const InformationRequest& a, const InformationRequest& b) const override;

  std::map<AreaOfInterest, double> GenerateTopDownAoiScoring(bool isHafSignalProcessed) const override;

  bool IsDataForLaneChangeReliable(Side side) const override;

  bool IsSideSideLaneObjectChangingIntoSide(RelativeLane relativeLane, bool merge) const override;

  void ReevaluateCurrentLaneChange() override;

  void ResetTransitionState() override;

  units::velocity::meters_per_second_t DetermineVelocityReason() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonSight() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonForDetectedTrafficJam() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonLaneWidth() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonCurvature() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonRightOvertakingProhibition() const override;

  units::velocity::meters_per_second_t DetermineVelocityReasonPassingSlowPlatoon() const override;

  void UpdateReliabilityMap() override;

  StochasticsInterface* GetStochastics() const override;

  bool IsFovea(AreaOfInterest aoi) const override;

  AreaOfInterest GetFovea() const override;

  std::vector<AreaOfInterest> GetUfov() const override;

  std::vector<AreaOfInterest> GetPeriphery() const override;

  bool GetIsNewEgoLane() const override;

  bool GetCollisionState() const override;

  bool GetIsVehicleVisible(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  bool GetIsStaticObject(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::time::second_t GetTtc(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::time::second_t GetGap(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::acceleration::meters_per_second_squared_t GetAccelerationDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetRelativeNetDistance(AreaOfInterest aoi) const override;

  units::velocity::meters_per_second_t GetLongitudinalVelocityDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::velocity::meters_per_second_t GetLateralVelocityDelta(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::velocity::meters_per_second_t GetAbsoluteVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::acceleration::meters_per_second_squared_t GetAcceleration(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::velocity::meters_per_second_t GetLateralVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetDistanceToBoundaryLeft(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetDistanceToBoundaryRight(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetDistanceToBoundaryLeft() const override;

  units::length::meter_t GetDistanceToBoundaryRight() const override;

  units::length::meter_t GetVehicleLength() const override;

  units::length::meter_t GetVehicleWidth() const override;

  scm::common::VehicleClass GetVehicleClassification() const override;

  units::length::meter_t GetVehicleLength(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetVehicleWidth(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetVehicleHeight(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  scm::common::VehicleClass GetVehicleClassification(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  void SetAgentId(int id) override;

  int GetAgentId() const override;

  int GetAgentId(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::time::second_t GetReactionBaseTime() const override;

  units::time::second_t GetAdjustmentBaseTime() const override;

  units::length::meter_t GetCarQueuingDistance() const override;

  units::length::meter_t GetCarRestartDistance() const override;

  units::length::meter_t GetLateralOffsetNeutralPosition() const override;

  units::length::meter_t GetLateralOffsetNeutralPositionRescueLane() const override;

  units::velocity::meters_per_second_t GetLateralVelocityTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::length::meter_t GetDistanceTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex = -1) const override;

  scm::LightState::Indicator GetIndicatorState(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::time::millisecond_t GetTimeSinceLastUpdate(AreaOfInterest aoi) const override;

  units::time::millisecond_t GetUrgentUpdateThreshold() const override;

  void SetUrgentUpdateThreshold(units::time::millisecond_t threshold) override;

  double GetPrioritySinceLastUpdate(GazeState gs) const override;

  bool GetLaneExistence(RelativeLane relativeLane, bool isEmergency = false) const override;

  bool GetInteriorHudExistence() const override;

  bool GetInteriorInfotainmentExistence() const override;

  units::velocity::meters_per_second_t GetMeanVelocityLaneLeft() const override;

  units::velocity::meters_per_second_t GetMeanVelocityLaneEgo() const override;

  units::velocity::meters_per_second_t GetMeanVelocityLaneRight() const override;

  units::velocity::meters_per_second_t GetMeanVelocity(SurroundingLane lane) const override;

  units::length::meter_t GetLateralPosition() const override;

  units::length::meter_t GetLateralPositionFrontAxle() const override;

  units::velocity::meters_per_second_t GetLateralVelocity() const override;

  units::velocity::meters_per_second_t GetLateralVelocityFrontAxle() const override;

  units::velocity::meters_per_second_t GetLongitudinalVelocityEgo() const override;

  units::velocity::meters_per_second_t GetLongitudinalVelocity(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  units::angle::radian_t GetHeading() const override;

  units::curvature::inverse_meter_t GetCurvatureOnCurrentPosition() const override;

  units::curvature::inverse_meter_t GetCurvatureInPreviewDistance() const override;

  units::length::meter_t GetDistanceToEndOfLane(int relativeLaneId, bool isEmergency = false) const override;

  units::length::meter_t GetDistanceToEndOfLane(RelativeLane relativeLane, bool isEmergency = false) const override;

  units::length::meter_t GetEgoLaneWidth() const override;

  units::velocity::meters_per_second_t GetAbsoluteVelocityEgo(bool isEstimatedValue) const override;

  units::velocity::meters_per_second_t GetAbsoluteVelocityTargeted() const override;

  units::velocity::meters_per_second_t GetVelocityLegalLeft() const override;

  units::velocity::meters_per_second_t GetVelocityLegalEgo() const override;

  units::velocity::meters_per_second_t GetVelocityLegalRight() const override;

  units::velocity::meters_per_second_t GetLegalVelocity(SurroundingLane lane) const override;

  units::velocity::meters_per_second_t GetDesiredVelocity() const override;

  units::acceleration::meters_per_second_squared_t GetAcceleration() const override;

  units::velocity::meters_per_second_t GetVelocityViolationDelta() const override;

  units::length::meter_t GetPreviewDistance() const override;

  void SetMergeRegulate(AreaOfInterest mergeRegulate) override;

  AreaOfInterest GetMergeRegulate() const override;

  bool IsSideLaneSafeWithMergePreparation(AreaOfInterest aoi) override;

  double GetHighCognitiveLaneChangeProbability(void) const override;

  Situation GetCurrentSituation() const override;

  void SetCurrentSituation(Situation currentSituation) override;

  double GetAgentCooperationFactor() const override;

  double GetAgentCooperationFactorForSuspiciousBehaviourEvasion() const override;

  int GetLaneId() const override;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> GetReliabilityMap(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  double GetTauDot(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  LateralAction GetLateralAction() const override;

  void SetLateralAction(LateralAction currentLateralAction) override;

  void SetDirection(int direction) override;

  SurroundingLane GetCurrentDirection() const override;

  units::time::millisecond_t GetDurationCurrentSituation() const override;

  void SetDurationCurrentSituation(units::time::millisecond_t durationCurrentSituation) override;

  std::vector<std::vector<scm::CommonTrafficSign::Entity>> GetTrafficSigns() const override;

  MicroscopicCharacteristicsInterface* GetMicroscopicData() const override;

  void SetTime(units::time::millisecond_t time) override;

  units::time::millisecond_t GetTime() const override;

  units::velocity::meters_per_second_t GetTrafficJamVelocityThreshold() const override;

  units::velocity::meters_per_second_t GetFormRescueLaneVelocityThreshold() const override;

  double GetIndicatorActiviationProbability() const override;

  double GetFlasherActiviationProbability() const override;

  void SetAdjustmentBaseTime(units::time::second_t adjustmentBaseTime) override;

  bool GetHasSituationChanged() const override;

  bool GetHasMesoscopicSituationChanged() const override;

  bool HasLeadingVehicleChanged() const override;

  bool GetIsLateralActionForced() const override;

  void SetLateralActionAsForced(LaneChangeAction isForced) override;

  bool GetIsLateralMovementStillSafe() const override;

  bool GetIsLaneChangePastTransition() const override;

  bool GetIsEndOfLateralMovement() const override;

  void SetIsEndOfLateralMovement(bool end) override;

  DriverParameters GetDriverParameters() const override;

  scm::common::vehicle::properties::EntityProperties GetVehicleModelParameters() const override;

  bool GetIsMergeRegulate(AreaOfInterest aoi, int sideAoiIndex = -1) const override;

  void SetIsAnticipating(bool anticipating) override;

  bool GetIsAnticipating() const override;

  InfrastructureCharacteristicsInterface* GetInfrastructureCharacteristics() const override;

  void CalculateMicroscopicData(bool updateLaneMeanVelocities = false) override;

  units::time::millisecond_t GetCycleTime() const override;

  units::length::meter_t GetDistanceToEndOfNextExit() const override;

  units::length::meter_t GetDistanceToStartOfNextExit() const override;

  LongitudinalActionState GetLongitudinalActionState() const override;

  void SetLongitudinalActionState(LongitudinalActionState state) override;

  void TriggerHighCognitive(bool externalControlActive) override;

  HighCognitiveSituation GetHighCognitiveSituation() const override;

  int GetClosestRelativeLaneIdForJunctionIngoing() const override;

  bool GetLaneChangePrevention() const override;

  void SetLaneChangePreventionAtSpawn(bool laneChangePrevention) override;

  bool GetLaneChangePreventionAtSpawn() const override;

  void SetLaneChangePreventionExternalControl(bool laneChangePrevention) override;

  bool GetLaneChangePreventionExternalControl() const override;

  bool GetRemainInLaneChangeState() const override;

  void SetRemainInLaneChangeState(bool remain) override;

  bool GetRemainInRealignState() const override;

  void SetRemainInRealignState(bool remain) override;

  bool GetRemainInSwervingState() const override;

  void SetRemainInSwervingState(bool remain) override;

  void ResetLaneChangeWidthTraveled() override;

  void SetLaneChangeWidthTraveled(units::length::meter_t width) override;

  units::length::meter_t GetLaneChangeWidthTraveled() const override;

  bool GetCooperativeBehavior() const override;

  ObstructionScm GetObstruction(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  AreaOfInterest GetCausingVehicleOfSituationFrontCluster() const override;

  void SetCausingVehicleOfSituationFrontCluster(Situation situation, AreaOfInterest aoi) override;

  void ResetCausingVehicleOfFrontCluster() override;

  AreaOfInterest GetCausingVehicleOfSituationSideCluster() const override;

  void SetCausingVehicleOfSituationSideCluster(Situation situation, AreaOfInterest aoi) override;

  void ResetCausingVehicleOfSideCluster() override;

  bool IsFrontSituation() const override;

  int DetermineIndexOfSideObject(AreaOfInterest aoi, SideAoiCriteria criteria, std::function<double(const SurroundingVehicleInterface*)> parameter = nullptr) const override;

  bool IsSituationAnticipated(Situation situation) const override;

  void SetSituationAnticipated(Situation situation, bool probability) override;

  Risk GetSituationRisk(Situation situation) const override;

  void SetSituationRisk(Situation situation, Risk risk) override;

  units::acceleration::meters_per_second_squared_t GetComfortLongitudinalAcceleration() const override;

  units::acceleration::meters_per_second_squared_t GetComfortLongitudinalDeceleration() const override;

  units::length::meter_t GetInfluencingDistanceToEndOfLane() const override;

  void CheckIsLaneChangeProhibitionIgnored() const override;

  void DrawLaneChangeProhibitionIgnored() const override;

  units::acceleration::meters_per_second_squared_t GetDecelerationLimitForMerging() const override;

  units::acceleration::meters_per_second_squared_t GetAccelerationLimitForMerging() const override;

  units::length::meter_t GetProjectedVehicleWidth() const override;

  units::length::meter_t GetProjectedVehicleWidthLeft() const override;

  units::length::meter_t GetProjectedVehicleWidthRight() const override;

  ProjectedSpacialDimensions GetProjectedDimensions(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  bool IsSideSituation() const override;

  ObstructionLongitudinal GetLongitudinalObstruction(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  AreaOfInterest GetObjectToEvade() const override;

  TrafficRuleInformationScmExtended* GetTrafficRuleInformation() const override;

  bool IsVisible(const Scm::BoundingBox& boundingBox, units::angle::radian_t gazeConeOpeningAngle) const override;

  bool IsPointVisible(const Scm::Point& point) const override;

  void SetHorizontalGazeAngle(units::angle::radian_t gazeAngle) override;

  units::angle::radian_t GetHorizontalGazeAngle() const override;

  void SetPossibleRegulateIds(const std::vector<int>& collisionCourseAgentIds) override;

  int GetLeadingVehicleId() const override;

  void SetLeadingVehicleId(int id) override;

  std::vector<int> GetPossibleRegulateIds() const override;

  const SurroundingVehicleInterface* GetVehicle(int id) const override;

  const SurroundingVehicleInterface* GetVehicle(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  const SurroundingVehicleInterface* GetLeadingVehicle() const override;

  void ActivateMesoscopicSituation(MesoscopicSituation situation) override;

  void DeactivateMesoscopicSituation(MesoscopicSituation situation) override;

  bool IsMesoscopicSituationActive(MesoscopicSituation situation) const override;

  std::vector<MesoscopicSituation> GetActiveMesoscopicSituations() const override;

  void SetLastLateralAction(const LateralAction& action) override;

  LateralAction GetLastLateralActionState() const override;

  std::vector<const SurroundingVehicleInterface*> GetSurroundingVehicles() const override;

  std::vector<const SurroundingVehicleInterface*> GetSurroundingVehicles(RelativeLane relativeLane) const override;

  SurroundingVehicles* UpdateSurroundingVehicles() override;

  void SetAoiMapping(const AoiVehicleMapping& mapping) override;

  void UpdateVisualReliabilityMap() override;

  bool IsVisualSectorReliable(VisualPerceptionSector sector, double requiredQuality) const override;

  double GetVisualReliability(VisualPerceptionSector sector) const override;

  void SetZipMerge(bool zipMerge) override;

  bool GetZipMerge() const override;

  double GetThresholdLooming(AreaOfInterest aoi, int sideAoiIndex = 0) const override;

  int GetMergeRegulateId() const override;

  void SetMergeRegulateId(int id) override;

  void SetMergeGap(Merge::Gap mergeGap) override;

  Merge::Gap GetMergeGap() const override;

  units::length::meter_t GetVisibilityDistance() const override;

  double GetVisibilityFactorForHighwayExit() const override;

  void SetOuterKeepingQuotaFulfilled(bool outerKeeping) override;

  bool GetOuterKeepingQuotaFulfilled() const override;

  void SetEgoLaneKeepingQuotaFulfilled(bool egoLaneKeeping) override;

  bool GetEgoLaneKeepingQuotaFulfilled() const override;

  VirtualAgentsSCM GetVirtualAgents() const override;

  bool IsRightHandTraffic() const override;

  AdasHmiSignal GetCombinedAcousticOpticSignal() const override;

  void SetCombinedAcousticOpticSignal(AdasHmiSignal acousticAndHapticAdasSignals) override;

  Merge::EgoInfo GetEgoInfo() const override;

  void DrawShoulderLaneUsage() const override;

  bool IsShoulderLaneUsageLegit(RelativeLane relativeLane) const override;

  const std::optional<TrajectoryPlanning::TrajectoryDimensions>& GetPlannedLaneChangeDimensions() const override;

  void SetPlannedLaneChangeDimensions(const TrajectoryPlanning::TrajectoryDimensions& dimensions) override;

  void ResetPlannedLaneChangeDimensions() override;

  double GetUrgencyReductionFactor() const override;

  std::vector<const SurroundingVehicleInterface*> GetVehicleVector(AreaOfInterest aoi) const override;

  void DrawDistractionUsage() const override;

  std::map<std::string, std::string> GetDebugValues() const override;

  void AddDebugValue(const std::string& description, const std::string& value) override;

  double GetReductionFactorUnderUrgency() const override;

  units::length::meter_t DetermineDistanceTraveledLeft(units::length::meter_t laneChangeWidth) override;
  units::length::meter_t DetermineDistanceTraveledRight(units::length::meter_t laneChangeWidth) override;
  bool HasEgoLateralDistanceReached(units::length::meter_t desiredLateralDisplacement, units::length::meter_t lateralPositionSet) const override;

  //! @brief Developer internal debug values map/storage to for easy access in simulation logging.
  std::map<std::string, std::string> _DEBUG_VALUES;
  //! @brief Id of current merge regulate vehicle, -1 if none present
  int _mergeRegulateId{-1};

  //! @copydoc MentalModelInterface::ReevaluateMergeGap
  void ReevaluateMergeGap() override;

  //! @brief SCM Merging module
  std::unique_ptr<MergingInterface> _merging{nullptr};

  //! @brief Current merge gap
  Merge::Gap _mergeGap{nullptr, nullptr};

  //! Probability of indicator activation in percent.
  double _probIndicatorActivation = 50.;

  //! Probability of flasher activation in percent.
  double _probFlasherActivation = 50.;

  //! @brief current surrounding vehicles of the agent
  SurroundingVehicles _surroundingVehicles;

#ifdef TESTING_ENABLED
  friend class TestMentalModel;
  friend class TestMentalModel2;
  friend class TestMentalModel3;
  friend class TestMentalModel5;

#endif

protected:
  //! @brief Map of all mesoscopic situations and their corresponding activity status
  std::map<MesoscopicSituation, bool> _mesoscopicSituations{
      {MesoscopicSituation::CURRENT_LANE_BLOCKED, false},
      {MesoscopicSituation::QUEUED_TRAFFIC, false},
      {MesoscopicSituation::MANDATORY_EXIT, false},
      {MesoscopicSituation::ACTIVE_ZIP_MERGING, false},
      {MesoscopicSituation::PASSIVE_ZIP_MERGING, false}};

  //! Infrastructure data as perceived by the agent.
  std::unique_ptr<InfrastructureCharacteristicsInterface> _infrastructureCollection{nullptr};

  //! Internal data of the mental model, including information on vehicle's surrounding.
  std::unique_ptr<MicroscopicCharacteristicsInterface> _microscopicData{nullptr};

  //! Fovea urgent update threshold [ms]
  int _updateThresholdFovea{2000};

  //! Ufov urgent update threshold [ms].
  int _updateThresholdUfov{4000};

  //! Peripial urgent update threshold [ms].
  int _updateThresholdPeriphery{6000};

  //! Has the transition happened for the current lane change action.
  bool _isLaneChangePastTransition{false};

  //! @brief SCMs traffic flow module (how fast each lane is going)
  std::shared_ptr<TrafficFlowInterface> _trafficFlow{nullptr};

  //! Transition Triggers as boolean in a vector
  //! newEgoLane, agentMoved, agentExtrapolate, logicAgentMovement
  bool _isNewEgoLane{false};

  double GetRequesterPriority(InformationRequestTrigger trigger) const;

  //! Cycle time [ms].
  units::time::millisecond_t _cycleTime{};

  //! @brief Query factory for surrounding vehicles
  std::unique_ptr<SurroundingVehicleQueryFactoryInterface> _vehicleQueryFactory;

  //! Stochastic parameter for probabilitiy calculation.
  StochasticsInterface* _stochastics = nullptr;

  //! @brief Flag indicating if there was a change to another leading vehicle
  bool _leadingVehicleChanged{false};

  //! @brief Query target absolute velocity of the vehicle and set information to internal data.
  void SetAbsoluteVelocityTargeted();

  //! @brief Creates and returns a LaneInformationTrafficRulesScmExtended with default values
  //! @return LaneInformationTrafficRulesScmExtended
  LaneInformationTrafficRulesScmExtended ResetSpeedLimitSign() const;

  //! Adjustment base time for expected events [s].
  units::time::second_t _adjustmentBaseTime{.1_s};

  //! Longitunidal wish acceleration of previous time step [m/s²]
  units::acceleration::meters_per_second_squared_t _accelerationWishLongitudinalLast{-99.};

  //! High cognitive
  std::unique_ptr<HighCognitiveInterface> _highCognitive{nullptr};

  //! Agents cooperative behavior in current situation
  bool _cooperativeBehavior = false;

  //! @brief Adjusts the given minimum distance with a factor when ego is below jam speed and is cooperative
  //! @param [in] minDistance Previously calculated minimum accepted distance for driver
  units::length::meter_t AdjustMinDistanceDueToCooperation(units::length::meter_t minDistance) const;

  //! @brief Check if the obstacle closer than end of lane and in this case set distanceToEndOfMergeConsideringObstacle
  bool IsObstacleCloserThanEndOfLane(double laneChangeSafetyFactor, units::velocity::meters_per_second_t velocityEgoAbsolute, units::length::meter_t distanceForFreeLaneChange, units::length::meter_t& distanceToEndOfMergeConsideringObstacle) const;

private:
  //! @brief Currently planned trajectory dimensions; std::nullopt if none trajectory is planned
  std::optional<TrajectoryPlanning::TrajectoryDimensions> _plannedTrajectoryDimensions{std::nullopt};

  //! @brief Lateral action of previous time step
  LateralAction _lastLateralAction;

  //! @brief Set up the values for different probalities, e.g. for flasher activation
  void InitializeHesitationProbabilities();

  //! @brief Checks if the point is closes than the visibilityDistance
  bool IsPointInVisibilityDistance(const Scm::Point& point) const;

  //! @brief Add the information request to the current list of information requests.
  //! @param [in] aoi                        Areas of interest for surrounding data.
  //! @param [in] sufficientViewAssignment   Sufficiency of the current view assignment.
  //! @param [in] priority                   Priority of the possible information request (default = 1).
  //! @param [in] trigger                    Trigger for information request.
  //! @return yes/no  The information request needed to be added
  void CreateInformationRequest(AreaOfInterest aoi, FieldOfViewAssignment sufficientViewAssignment, double priority, InformationRequestTrigger trigger) const;

  //! @brief Check, if the ego vehicle is located in a new lane.
  //! @param [in] actionMan                  ActionManager
  //! @param [in] isLaneEgoNew               Check whether lane of the ego vehicle is new.
  //! @param [in] hasNumberOfLanesChanged    Check whether the number of lanes has changed.
  //! @return yes/no
  bool TriggerNewEgoLane(int laneId_GroundTruth, units::length::meter_t lateralPositionInLane_GroundTruth);

  //! @brief Query the priority of the current trigger / requester of information.
  //! @param [in] trigger                    Trigger for information request.
  //! @return double  The priority of the requester only known in the mental model.

  //! The maximum number of remembered information request per area of interest
  const unsigned long long _maxRequests{5};

  //! @brief The current safety state of the active lane change (for unit tests)
  bool _isLateralMovementStillSafe{true};

  //! @brief The current state of fulfillment of the  lateral movement in progress
  bool _endOfLateralMovement{false};

  //! @brief Features in the vicinity of the driver.
  const FeatureExtractorInterface& _featureExtractor;

  //! Receives informations from module InternelData and estimates the current situation based on the high cognitive process.
  MentalCalculationsInterface& _mentalCalculations;

  //! @brief Receives informations from ScmDependencies
  ScmDependenciesInterface& _scmDependencies;

  //! Duration of current situation
  units::time::millisecond_t _durationCurrentSituation = 0._ms;

  //! @brief reaction base time for expected events [s].
  units::time::second_t _reactionBaseTime = 0._s;

  //! Gaze angle from gaze control
  units::angle::radian_t _gazeAngle{0};

  //! @brief Simulation time stamp
  units::time::millisecond_t time{0};

  //! @brief Timed threshold when an update is urgently late
  units::time::millisecond_t _urgentUpdateThreshold = 1000_ms;  // in ms URGENT_UPDATE_THRESHOLD

  //! @brief Flag indicating whether the class has been initialized
  bool _isInit{false};

  //! The ego vehicle remains in lane change state.
  bool _remainInLaneChangeState = false;

  //! @brief The ego vehicle remains in the realign state (relevant for lateral position or rather lateral offset driving)
  bool _remainInRealignState = false;

  //! @brief The traveled distance of lane change width.
  units::length::meter_t _laneChangeWidthTraveled = 0._m;

  //! @brief The current state of perceived lane change of the ego vehicle.
  LaneChangeState _currentPerceivedEgoLaneChangeState{LaneChangeState::NoLaneChange};

  //! @brief Mesoscopic influencing distance to end of current lane [m]. MEAN parameter for distribution.
  const units::length::meter_t INFLUENCING_DISTANCE_TO_END_OF_LANE_MEAN = 750._m;

  //! @brief Mesoscopic influencing distance to end of current lane [m]. SD parameter for distribution.
  const units::length::meter_t INFLUENCING_DISTANCE_TO_END_OF_LANE_SD = 100._m;

  //! @brief Mesoscopic influencing distance to end of current lane [m]. MIN parameter for distribution.
  const units::length::meter_t INFLUENCING_DISTANCE_TO_END_OF_LANE_MIN = 500._m;

  //! @brief Determine agents general influencing distance to end of lane [m].
  units::length::meter_t _agentsGeneralInfluencingDistanceToEndOfLane{750._m};

  //! @brief Flag indicating if the agent should still remain in the swerving state
  bool _remainInSwervingState{false};

  //! @brief Set of inconsistent AreasOfInterest
  std::vector<AreaOfInterest> _inconsistentAois{};

  void SetRelevantSpeedLimitForLane(LaneInformationTrafficRulesScmExtended in, LaneInformationTrafficRulesScmExtended& out);

  //! @brief A list of possible agents ids to regulate on; mostly for debugging purposes
  std::vector<int> _possibleRegulateIds{};

  //! @brief The ID of the currently selected leading vehicle
  int _leadingVehicleId{-1};

  //! @brief Flag indicating if the ego agent is currently in a zip merging maneuver
  bool _isZipMerge{false};

  //! @brief Resets certain flags relevant for lane change behavior
  void ResetLaneChangeFlags();

  //! @brief Map containing reliability values for the visual perception sectors
  std::map<VisualPerceptionSector, double> _visualReliabilityMap{
      {VisualPerceptionSector::FRONT, 100.0},
      {VisualPerceptionSector::REAR, 100.0},
      {VisualPerceptionSector::RIGHT, 100.0},
      {VisualPerceptionSector::LEFT, 100.0}};

  //! @brief Mapping of area of interests to their corresponding surrounding vehicle
  AoiVehicleMapping _aoiMapping;

  //! @brief Set of optical ADAS signals
  std::vector<AdasHmiSignal> _opticAdasSignals;

  //! @brief Set of acoustic ADAS signals
  std::vector<AdasHmiSignal> _acousticAdasSignals;

  //! @brief Single ADAS signal combined acoustic and optical
  AdasHmiSignal _acousticAndOpticAdasSignal{};

  bool _isInTargetLane = false;
};