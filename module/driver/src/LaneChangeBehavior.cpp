/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneChangeBehavior.h"

#include <algorithm>
#include <cmath>
#include <optional>
#include <vector>

#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

LaneChangeBehavior::LaneChangeBehavior(std::shared_ptr<LaneChangeBehaviorQueryInterface> query, const MentalModelInterface& mentalModel)
    : _laneChangeBehaviorQuery{query}, _mentalModel{mentalModel}
{
}

void LaneChangeBehavior::CalculateLaneIntensities(const TrafficRulesScm& trafficRulesScm)
{
  const bool outerLaneOvertakingProhibited{_laneChangeBehaviorQuery->IsOuterLaneOvertakingProhibited(trafficRulesScm.common.rightHandTraffic)};
  _laneChangeIntensities.EgoLaneIntensity = CalculateBaseLaneConvenience(SurroundingLane::EGO);
  if (trafficRulesScm.common.rightHandTraffic)
  {
    _laneChangeIntensities.LeftLaneIntensity = CalculateBaseLaneConvenience(SurroundingLane::LEFT);
    _laneChangeIntensities.RightLaneIntensity = outerLaneOvertakingProhibited ? LANE_DENIED : CalculateBaseLaneConvenience(SurroundingLane::RIGHT);
  }
  else
  {
    _laneChangeIntensities.LeftLaneIntensity = outerLaneOvertakingProhibited ? LANE_DENIED : CalculateBaseLaneConvenience(SurroundingLane::LEFT);
    _laneChangeIntensities.RightLaneIntensity = CalculateBaseLaneConvenience(SurroundingLane::RIGHT);
  }

  if (_laneChangeIntensities.EgoLaneIntensity != LANE_DENIED && _laneChangeIntensities.EgoLaneIntensity != 1)
  {
    _laneChangeIntensities.EgoLaneIntensity = CalculateIntensityParameters(SurroundingLane::EGO);
  }
  if (_laneChangeIntensities.LeftLaneIntensity != LANE_DENIED && _laneChangeIntensities.LeftLaneIntensity != 1)
  {
    _laneChangeIntensities.LeftLaneIntensity = CalculateIntensityParameters(SurroundingLane::LEFT);
  }
  if (_laneChangeIntensities.RightLaneIntensity != LANE_DENIED && _laneChangeIntensities.RightLaneIntensity != 1)
  {
    _laneChangeIntensities.RightLaneIntensity = CalculateIntensityParameters(SurroundingLane::RIGHT);
  }

  _laneChangeIntensities = DetermineBaseLaneConvenienceFromParameters(_laneChangeIntensities);

  _laneChangeIntensities = _laneChangeBehaviorQuery->AdjustLaneConvenienceDueToHighwayExit(_laneChangeIntensities, trafficRulesScm.common.rightHandTraffic);

  _laneChangeIntensities = ApplyLaneBiasIntensity(_laneChangeIntensities);
}

double LaneChangeBehavior::CalculateIntensityParameters(SurroundingLane targetLane) const
{
  ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity;
  componentsForLaneChangeIntensity.isTimeAtTargetSpeed = IsTimeAtTargetSpeed(targetLane);
  componentsForLaneChangeIntensity.isDistanceToEndOfLaneSmallerOrEqToInfluencingDistance = IsDistanceToEndOfLaneSmallerOrEqToInfluencingDistance(targetLane);
  componentsForLaneChangeIntensity.isSlowBlocker = IsSlowBlocker(targetLane);

  return _laneChangeBehaviorQuery->CalculateLaneChangeIntensity(targetLane, componentsForLaneChangeIntensity);
}

SurroundingLane LaneChangeBehavior::GetLaneChangeWish() const
{
  return _laneChangeBehaviorQuery->DetermineLaneChangeWishFromIntensities(_laneChangeIntensities);
}

LaneChangeIntensities LaneChangeBehavior::DetermineBaseLaneConvenienceFromParameters(LaneChangeIntensities inIntensities)
{
  LaneChangeIntensities outIntensities{inIntensities};

  if (inIntensities.EgoLaneIntensity == LANE_DENIED && inIntensities.LeftLaneIntensity == LANE_DENIED && inIntensities.RightLaneIntensity == LANE_DENIED)
  {
    outIntensities.LeftLaneIntensity = 0;
    outIntensities.EgoLaneIntensity = 1;
    outIntensities.RightLaneIntensity = 0;

    return outIntensities;
  }
  else if (inIntensities.EgoLaneIntensity > 0 || inIntensities.LeftLaneIntensity > 0 || inIntensities.RightLaneIntensity > 0)
  {
    outIntensities.EgoLaneIntensity = CheckLaneIsDenied(inIntensities.EgoLaneIntensity);
    outIntensities.LeftLaneIntensity = CheckLaneIsDenied(inIntensities.LeftLaneIntensity);
    outIntensities.RightLaneIntensity = CheckLaneIsDenied(inIntensities.RightLaneIntensity);

    return outIntensities;
  }
  else
  {
    std::vector<units::velocity::meters_per_second_t> left = CalculateReplacementVelocities(inIntensities.LeftLaneIntensity, SurroundingLane::LEFT);
    std::vector<units::velocity::meters_per_second_t> right = CalculateReplacementVelocities(inIntensities.RightLaneIntensity, SurroundingLane::RIGHT);
    std::vector<units::velocity::meters_per_second_t> ego = CalculateReplacementVelocities(inIntensities.EgoLaneIntensity, SurroundingLane::EGO);

    outIntensities.LeftLaneIntensity = CheckIsReplacementsEmpty(left, outIntensities.LeftLaneIntensity);
    outIntensities.RightLaneIntensity = CheckIsReplacementsEmpty(right, outIntensities.RightLaneIntensity);
    outIntensities.EgoLaneIntensity = CheckIsReplacementsEmpty(ego, outIntensities.EgoLaneIntensity);

    auto maxElementLeft = FindMaximumReplacementVelocity(left);
    auto maxElementRight = FindMaximumReplacementVelocity(right);
    auto maxElementEgo = FindMaximumReplacementVelocity(ego);

    auto maxAdjustment = std::max({maxElementLeft, maxElementRight, maxElementEgo});

    // left
    outIntensities.LeftLaneIntensity = CalculateIntensity(left, maxAdjustment);
    // right
    outIntensities.RightLaneIntensity = CalculateIntensity(right, maxAdjustment);
    // ego
    outIntensities.EgoLaneIntensity = CalculateIntensity(ego, maxAdjustment);

    if (outIntensities.EgoLaneIntensity > 0 || outIntensities.LeftLaneIntensity > 0 || outIntensities.RightLaneIntensity > 0)
    {
      return outIntensities;
    }
    else
    {
      outIntensities.LeftLaneIntensity = 0;
      outIntensities.EgoLaneIntensity = 1;
      outIntensities.RightLaneIntensity = 0;
    }
  }
  return outIntensities;
}

double LaneChangeBehavior::CheckLaneIsDenied(double intensity) const
{
  if (intensity == LANE_DENIED)
  {
    return 0;
  }

  return intensity;
}

std::vector<units::velocity::meters_per_second_t> LaneChangeBehavior::CalculateReplacementVelocities(double intensity, SurroundingLane targetLane)
{
  if (intensity == LANE_DENIED)
  {
    return {};
  }
  return DetermineLaneConvenienceParameters(targetLane);
}

double LaneChangeBehavior::CheckIsReplacementsEmpty(const std::vector<units::velocity::meters_per_second_t>& replacements, double outIntensity) const
{
  if (replacements.empty())
  {
    outIntensity = 0;
  }

  return outIntensity;
}

units::velocity::meters_per_second_t LaneChangeBehavior::FindMaximumReplacementVelocity(std::vector<units::velocity::meters_per_second_t> replacements) const
{
  if (replacements.empty())
  {
    return 0_mps;
  }
  return *max_element(replacements.begin(), replacements.end());
}

double LaneChangeBehavior::CalculateIntensity(std::vector<units::velocity::meters_per_second_t> velocityAdjustments, units::velocity::meters_per_second_t maxAdjustment) const
{
  if (velocityAdjustments.empty() || maxAdjustment == 0_mps)
  {
    return 0;
  }

  for (auto& parameter : velocityAdjustments)
  {
    parameter = -(parameter / maxAdjustment.value()) + 1_mps;
  }
  std::sort(velocityAdjustments.begin(), velocityAdjustments.end());

  for (const auto& velocity : velocityAdjustments)
  {
    if (velocity > 0_mps)
    {
      return velocity.value();
    }
  }
  return 0;
}

double LaneChangeBehavior::CalculateBaseLaneConvenience(SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  auto distanceToEndOfLane = _mentalModel.GetDistanceToEndOfLane(relativeLane);

 if (!_laneChangeBehaviorQuery->EgoFitsInLane(targetLane))
 {
   return LANE_DENIED;
 }
 const auto influencingDistance = _mentalModel.GetInfluencingDistanceToEndOfLane();
 const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
 const auto distanceToObstacle = _mentalModel.GetRelativeNetDistance(aoi);

  if (!_mentalModel.GetIsVehicleVisible(aoi))
  {
    if (influencingDistance < distanceToEndOfLane)
    {
      return 1;
    }
  }
  else
  {
    if (!_laneChangeBehaviorQuery->GetIsInfluencingDistanceViolated(_mentalModel.GetVehicle(aoi)) && influencingDistance < distanceToEndOfLane)
    {
      return 1;
    }
  }

  return 0;
}

std::vector<units::velocity::meters_per_second_t> LaneChangeBehavior::DetermineLaneConvenienceParameters(SurroundingLane targetLane)
{
  std::vector<units::velocity::meters_per_second_t> laneConvenienceParameter;

  if (targetLane != SurroundingLane::EGO)
  {
    laneConvenienceParameter.push_back(CalculateLongitudinalVelocityDelta(targetLane));
  }

  laneConvenienceParameter.push_back(CalculateVelocityDeltaTimeAtTargetSpeed(targetLane));
  laneConvenienceParameter.push_back(CalculateVelocityDeltaToSlowBlockerDelta(targetLane));
  laneConvenienceParameter.push_back(CalculateVelocityToLaneMeanDelta(targetLane));

  return laneConvenienceParameter;
}

bool LaneChangeBehavior::IsDistanceToEndOfLaneSmallerOrEqToInfluencingDistance(SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  return _mentalModel.GetDistanceToEndOfLane(relativeLane) <= _mentalModel.GetInfluencingDistanceToEndOfLane();
}

units::velocity::meters_per_second_t LaneChangeBehavior::CalculateLongitudinalVelocityDelta(SurroundingLane targetLane) const
{
 units::velocity::meters_per_second_t deltaLongitudinalVelocity = 0_mps;

  if (IsDistanceToEndOfLaneSmallerOrEqToInfluencingDistance(targetLane))
  {
    auto egolength = _mentalModel.GetVehicleLength();
    const int lanesToCross = 2;
    LaneChangeDimension laneChangeDimension(_mentalModel);

   const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
   auto agentAhead = _mentalModel.GetVehicleLength(aoi);
   auto overtakingLength{lanesToCross * laneChangeDimension.EstimateLaneChangeLength() + egolength + agentAhead};
   auto infrastructureCharacteristics = _mentalModel.GetInfrastructureCharacteristics();
   const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
   auto lane = infrastructureCharacteristics->GetLaneInformationGeometry(relativeLane);
   auto distanceToEndOfLane = lane.distanceToEndOfLane;
   auto laneChangeWidth = lane.width;

   if (overtakingLength > distanceToEndOfLane)
   {
     const auto lateralAcc{_mentalModel.GetDriverParameters().comfortLateralAcceleration};
     // safetybuffer and calculation from deltaLongitudinalVelocity comes from laneChangeDimension.EstimateLaneChangeLength()
     double constexpr safetybuffer = 1.2;
     auto longitudinalVelocity = distanceToEndOfLane / (units::math::sqrt(10. * units::math::fabs(laneChangeWidth) / std::sqrt(3.) / units::math::fabs(lateralAcc)) * safetybuffer);

     deltaLongitudinalVelocity = _mentalModel.GetAbsoluteVelocityEgo(false) - longitudinalVelocity;
   }
 }

 return units::math::abs(deltaLongitudinalVelocity);
}

bool LaneChangeBehavior::IsTimeAtTargetSpeed(SurroundingLane targetLane) const
{
 const auto timeAtTargetSpeedInLane{_laneChangeBehaviorQuery->GetTimeAtTargetSpeedInLane(targetLane)};

 if (timeAtTargetSpeedInLane != 99.0_s)
 {
   return timeAtTargetSpeedInLane < MinimumTimeAtTargetSpeedThreshold;
 }
 else
 {
   const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
   const auto relativeLaneId = RelativeLane2RelativeLaneId.at(relativeLane);
   const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(relativeLaneId)};

   auto timeToEndOfLane = 99.0_s;
   auto influencingDistanceToEndOfLane = _mentalModel.GetInfluencingDistanceToEndOfLane();
   if (distanceToEndOfLane < influencingDistanceToEndOfLane)
   {
     auto egoVelocity = _mentalModel.GetLongitudinalVelocityEgo();
     timeToEndOfLane = distanceToEndOfLane / egoVelocity;
   }

   if (timeToEndOfLane != 99.0_s)
   {
     return timeToEndOfLane < MinimumTimeAtTargetSpeedThreshold;
   }
 }

  return false;
}

units::velocity::meters_per_second_t LaneChangeBehavior::CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane targetLane) const
{
  const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
  const auto timeAtTargetSpeedInLane{_laneChangeBehaviorQuery->GetTimeAtTargetSpeedInLane(targetLane)};
  const auto timeToEndOfLane = DetermineTimeToEndOfLane(targetLane);

  if (timeAtTargetSpeedInLane == 99.0_s && timeToEndOfLane != 99.0_s)
  {
   return units::math::abs(DetermineDeltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance(targetLane));
  }

  auto deltaVelocityTimeAtTargetSpeedWithObstacleOnLane = DetermineDeltaVelocityTimeAtTargetSpeedWithObstacleOnLane(targetLane, timeAtTargetSpeedInLane);
  auto deltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance = 0_mps;

  if (timeToEndOfLane != 99.0_s)
  {
   deltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance = DetermineDeltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance(targetLane);
  }

  if (timeToEndOfLane == 99.0_s || units::math::abs(deltaVelocityTimeAtTargetSpeedWithObstacleOnLane) > units::math::abs(deltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance))
  {
   return units::math::abs(deltaVelocityTimeAtTargetSpeedWithObstacleOnLane);
  }
  else
  {
   return units::math::abs(deltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance);
  }
}

units::velocity::meters_per_second_t LaneChangeBehavior::DetermineDeltaVelocityTimeAtTargetSpeedWithObstacleOnLane(SurroundingLane targetLane, units::time::second_t timeAtTargetSpeedInLane) const
{
  const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
  bool timeAtTargetSpeedBelowThreshold = timeAtTargetSpeedInLane < MinimumTimeAtTargetSpeedThreshold;
  if (timeAtTargetSpeedBelowThreshold)
  {
    auto relativeLongitudinalDistance = _mentalModel.GetRelativeNetDistance(aoi);
    auto distanceAtThreshold = _mentalModel.GetLongitudinalVelocity(aoi) * MinimumTimeAtTargetSpeedThreshold;
    auto distanceAgentFront = relativeLongitudinalDistance + distanceAtThreshold;
    units::velocity::meters_per_second_t velocityAgentFront = distanceAgentFront / MinimumTimeAtTargetSpeedThreshold;

    return _mentalModel.GetLongitudinalVelocityEgo() - velocityAgentFront;
  }
  return 0.0_mps;
}

units::time::second_t LaneChangeBehavior::DetermineTimeToEndOfLane(SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(relativeLane)};

  auto timeToEndOfLane = 99.0_s;
  if (distanceToEndOfLane < _mentalModel.GetInfluencingDistanceToEndOfLane())
  {
    timeToEndOfLane = distanceToEndOfLane / _mentalModel.GetLongitudinalVelocityEgo();
  }

  return timeToEndOfLane;
}

units::velocity::meters_per_second_t LaneChangeBehavior::DetermineDeltaVelocityTimeAtTargetSpeedWithoutObstacleWithinInfluencingDistance(SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(relativeLane)};
  auto timeToEndOfLane = 99.0_s;
  auto influencingDistanceToEndOfLane = _mentalModel.GetInfluencingDistanceToEndOfLane();
  auto egoVelocity = _mentalModel.GetLongitudinalVelocityEgo();

  if (distanceToEndOfLane < influencingDistanceToEndOfLane)
  {
    timeToEndOfLane = distanceToEndOfLane / egoVelocity;
  }

  bool timeAtTargetSpeedBelowThresholdWithoutAgentFront = timeToEndOfLane < MinimumTimeAtTargetSpeedThreshold;
  if (timeAtTargetSpeedBelowThresholdWithoutAgentFront)
  {
    auto velocityEndOfLane = distanceToEndOfLane / MinimumTimeAtTargetSpeedThreshold;
    return egoVelocity - velocityEndOfLane;
  }
  return 0.0_mps;
}

bool LaneChangeBehavior::IsSlowBlocker(SurroundingLane targetLane) const
{
 const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
 return (_mentalModel.GetAbsoluteVelocity(aoi) < _mentalModel.GetAbsoluteVelocityTargeted()) && _laneChangeBehaviorQuery->CanVehicleCauseCollisionCourseForNonSideAreas(_mentalModel.GetVehicle(aoi));
}

units::velocity::meters_per_second_t LaneChangeBehavior::CalculateVelocityDeltaToSlowBlockerDelta(SurroundingLane targetLane) const
{
 units::velocity::meters_per_second_t deltaVelocityDeltaToSlowBlocker = 0_mps;

 if (IsSlowBlocker(targetLane))
 {
    const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
    deltaVelocityDeltaToSlowBlocker = _mentalModel.GetAbsoluteVelocityTargeted() - _mentalModel.GetAbsoluteVelocity(aoi);
 }
 return units::math::abs(deltaVelocityDeltaToSlowBlocker);
}

units::velocity::meters_per_second_t LaneChangeBehavior::CalculateVelocityToLaneMeanDelta(SurroundingLane targetLane) const
{
  return units::math::abs(_mentalModel.GetAbsoluteVelocityTargeted() - _mentalModel.GetMeanVelocity(targetLane));
}

double LaneChangeBehavior::GetLaneConvenience(SurroundingLane targetLane) const
{
  switch (targetLane)
  {
    case SurroundingLane::EGO:
      return _laneChangeIntensities.EgoLaneIntensity;
    case SurroundingLane::LEFT:
      return _laneChangeIntensities.LeftLaneIntensity;
    case SurroundingLane::RIGHT:
      return _laneChangeIntensities.RightLaneIntensity;
  }

  throw std::runtime_error("LaneChangeBehavior::GetLaneConvenience: Invalid targetLane passed");
}

LaneChangeIntensities LaneChangeBehavior::ApplyLaneBiasIntensity(const LaneChangeIntensities& inIntensities)
{
  LaneChangeIntensities outIntensities{inIntensities};

  if (outIntensities.EgoLaneIntensity > 0 && _mentalModel.GetEgoLaneKeepingQuotaFulfilled() && !_mentalModel.IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED))
  {
    outIntensities.EgoLaneIntensity += _mentalModel.GetDriverParameters().egoLaneKeepingIntensity.value_or(ScmDefinitions::DEFAULT_VALUE);
  }

  if (outIntensities.RightLaneIntensity > 0 && _mentalModel.GetOuterKeepingQuotaFulfilled())
  {
    const auto timeAtTargetSpeedRight{_laneChangeBehaviorQuery->GetTimeAtTargetSpeedInLane(SurroundingLane::RIGHT)};
    const auto timeAtTargetSpeedEgo{_laneChangeBehaviorQuery->GetTimeAtTargetSpeedInLane(SurroundingLane::EGO)};

    if ((timeAtTargetSpeedEgo > MinimumTimeAtTargetSpeedThreshold) && (timeAtTargetSpeedRight - timeAtTargetSpeedEgo >= (MinimumTimeAtTargetSpeedThreshold / 2) || (timeAtTargetSpeedRight == 99._s && timeAtTargetSpeedEgo == 99._s)))
    {
      outIntensities.RightLaneIntensity += _mentalModel.GetDriverParameters().outerKeepingIntensity.value_or(ScmDefinitions::DEFAULT_VALUE);
   }
  }
  return _laneChangeBehaviorQuery->NormalizeLaneChangeIntensities(outIntensities);
}