/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SpawningState.h"

#include "LaneQueryHelper.h"
#include "ScmDefinitions.h"
#include "include/common/ScmDefinitions.h"

SpawningState::SpawningState(const std::string& componentName,
                             units::time::millisecond_t cycleTime,
                             PeriodicLoggerInterface* pLog,
                             ScmComponentsInterface& scmComponents,
                             ExternalControlState externalControlState)
    : ScmLifetimeState(componentName, cycleTime, pLog, scmComponents, externalControlState), _scmComponents{&scmComponents}
{
}

void SpawningState::SetInitialParameters(units::time::millisecond_t time)
{
  _scmComponents->GetScmDependencies()->SetSpawnTime(time);
  _scmComponents->GetMentalModel()->GetMicroscopicData()->UpdateOwnVehicleData()->distributionForHighwayExit = _scmComponents->GetScmDependencies()->GetStochastics()->GetUniformDistributed(-0.2, 0.2);

  // All surrounding objects are known at spawn (add for bottom up requests)
  std::vector<int> knownId{};
  auto surroundingObjects = _scmComponents->GetScmDependencies()->GetSurroundingObjectsScm();
  for (const AreaOfInterest& aoi : _RELEVANT_AOIS_AT_SPAWN)
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      size_t sideSize = LaneQueryHelper::IsLeftLane(aoi) ? surroundingObjects->ongoingTraffic.Close().Left().size() : surroundingObjects->ongoingTraffic.Close().Right().size();

      for (size_t i = 0; i < sideSize; ++i)
      {
        const auto vehicle = ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, surroundingObjects, i);

        if (vehicle != nullptr && vehicle->id != -1)
        {
          knownId.push_back(vehicle->id);
        }
      }
    }
    else
    {
      const auto vehicle = ScmCommons::GetObjectInformationFromSurroundingObjectScm(aoi, surroundingObjects, 0);
      if (vehicle != nullptr && vehicle->id != -1)
      {
        knownId.push_back(vehicle->id);
      }
    }
  }
  _scmComponents->GetGazeControl()->SetKnownObjectIds(knownId);
  ScmLifetimeState::SetInitialParameters(time);
}

void SpawningState::UpdateMicroscopicData()
{
  _scmComponents->GetInformationAcquisition()->UpdateEgoVehicleData(true);
  _scmComponents->GetInformationAcquisition()->UpdateSurroundingVehicleData(true, _scmComponents->GetGazeControl()->IsNewInformationAcquisitionRequested());

  const bool doSpawnCorrections{!_scmComponents->GetScmDependencies()->GetDriverParameters().idealPerception};

  // overwrite offsets from initialisation to mark current sight as most recent information
  if (doSpawnCorrections)
  {
    _scmComponents->GetMentalModel()->UpdateReliabilityMap();
  }
  ScmLifetimeState::UpdateMicroscopicData();
}

void SpawningState::UpdateAgentInformation(units::time::millisecond_t time)
{
  _isNewEgoLane = false;
  ScmLifetimeState::UpdateAgentInformation(time);
}

void SpawningState::DetermineAgentStates()
{
  ScmLifetimeState::ResetLateralMovement();

  _scmComponents->GetMentalModel()->TriggerHighCognitive(_externalControlState.lateralActive && _externalControlState.longitudinalActive);
  _scmComponents->GetSituationManager()->GenerateIntensitiesAndSampleSituation(*_scmComponents->GetZipMerging());

  _scmComponents->GetMentalModel()->DrawLaneChangeProhibitionIgnored();
  _scmComponents->GetMentalModel()->DrawShoulderLaneUsage();
  _scmComponents->GetMentalModel()->DrawDistractionUsage();

  if (!_externalControlState.lateralActive)
  {
    _scmComponents->GetActionManager()->GenerateIntensityAndSampleLateralAction();
  }

  _scmComponents->GetActionManager()->RunLeadingVehicleSelector();

  if (!_externalControlState.longitudinalActive)
  {
    _scmComponents->GetActionManager()->DetermineLongitudinalActionState();
  }

  ScmLifetimeState::ResetBaseTimes();
  ScmLifetimeState::ResetMerges();
  ScmLifetimeState::HandleExternalControl();
}
