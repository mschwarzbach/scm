/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file LaneChangeTrajectoryCalculationsInterface.h

#pragma once

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! ************************************************************************************************************
//! @brief Interface class that provides calculations of trajectory dimensions and trajectory safety evaluation.
//! @details Provides helper methods to determine certain parameters, like the lane change length and width
//! or the reference object, to compute lane change trajectories.
//! ************************************************************************************************************
class LaneChangeTrajectoryCalculationsInterface
{
public:
  virtual ~LaneChangeTrajectoryCalculationsInterface() = default;

  //! @brief Returns the most relevant lane change obstacle or nullptr if no object needs to be considered.
  //! @param currentSituation the current situation of the agent, influences which reference object could be chosen
  //! @param relevantVehicles a set of possible vehicles used for reference (causing vehicles of front and side situations and ego front)
  //! @return the most suitable of the relevant vehicles for planning the available lane change length
  virtual const SurroundingVehicleInterface* DetermineReferenceObjectForLaneChange(Situation currentSituation, const TrajectoryPlanning::RelevantVehiclesForLaneChangeLength& relevantVehicles) const = 0;

  //! @brief Calculates the maximum available lane change length to execute a safe lane change with the given parameters.
  //! @param parameters Previously determined, relevant parameters for planning the length of the currently planned lane change
  //! @param surroundingVehicleQueryFactory factory which enables querying data from surrounding vehicles
  //! @return The calculated maximum lane change length.
  virtual units::length::meter_t DetermineLaneChangeLength(const TrajectoryPlanning::LaneChangeLengthParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const = 0;

  //! @brief Calculates desired lane change width based on current and neighboring lane width as well as current and desired t position.
  //! @param parameters Previously determined, relevant parameters for planning the width of the currently planned lane change
  //! @return The desired width to be covered during the currently planned lane change.
  virtual units::length::meter_t DetermineLaneChangeWidth(const TrajectoryPlanning::LaneChangeWidthParameters& parameters) const = 0;

  //! @brief Checks if the estimated distance to the given vehicle at the end of the lane change is smaller than the minDistance.
  //! @param parameters Previously determined, relevant parameters to evaluate if the minDistance to the included vehicle is violated after the lane change
  //! @param surroundingVehicleQueryFactory factory which enables querying data from surrounding vehicles
  //! @return true when a certain lane change can be done without violation the minimum distance
  virtual bool CanFinishLaneChangeWithoutEnteringMinDistance(const TrajectoryPlanning::SafetyParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const = 0;
};