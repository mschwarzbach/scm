/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneChangeLengthCalculationsInterface.h

#pragma once

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Interface class for calculations of lane change lengths determined by different factors.
class LaneChangeLengthCalculationsInterface
{
public:
  virtual ~LaneChangeLengthCalculationsInterface() = default;

  //! @brief Calculates the maximum lane change length to be able to avoid the given obstacle.
  //! @details Returns the net distance to the given obstacle + (TTC - THW) * egoVelocity or INFINITY if no vehicle is given or has an invalid TTC.
  //! @param laneChangeObstacle Vehicle to avoid by changing lanes
  //! @param egoVelocityLongitudinal Longitudinal velocity of the ego vehicle
  //! @param surroundingVehicleQueryFactory Factory for SurroundingVehicleQuery
  //! @return The minimum distance to avoid the given obstacle.
  virtual units::length::meter_t DetermineLaneChangeLengthFromObstacle(const SurroundingVehicleInterface* laneChangeObstacle, units::velocity::meters_per_second_t egoVelocityLongitudinal, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const = 0;

  //! @brief Calculates the product of timeHeadway for a free lane change and ego velocity, with a lower cap at 40m.
  //! @param timeHeadwayFreeLaneChange Driver parameter for time length of a free lane change
  //! @param egoVelocityLongitudinal Current longitudinal ego velocity
  //! @return Calculated free lane change length in meter
  virtual units::length::meter_t DetermineFreeLaneChangeLength(units::time::second_t timeHeadwayFreeLaneChange, units::velocity::meters_per_second_t egoVelocityLongitudinal) const = 0;

  //! @brief Calculate maximum LaneChangeLength to be able to safely reach the highway exit.
  //! @param exitInformation Contains information about the exit to take, like number of required lane changes and distance to end of exit
  //! @param egoVelocityLongitudinal Longitudinal velocity of the ego vehicle
  //! @return The maximum lane change distance to reach the exit while respecting the minimumTimeOnExitLane or INFINITY if the exit cannot be reached anymore.
  virtual units::length::meter_t DetermineLaneChangeLengthFromHighwayExit(const TrajectoryPlanning::HighwayExitInformation& exitInformation, units::velocity::meters_per_second_t egoVelocityLongitudinal) const = 0;
};