/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h>

#include <cmath>

LaneChangeTrajectoryCalculations::LaneChangeTrajectoryCalculations(std::shared_ptr<LaneChangeLengthCalculationsInterface> laneChangeLengthCalculations)
    : _laneChangeLengthCalculations{laneChangeLengthCalculations}
{
}

units::length::meter_t LaneChangeTrajectoryCalculations::DetermineLaneChangeLength(const TrajectoryPlanning::LaneChangeLengthParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const
{
  const auto distanceToEndOfLane{parameters.distanceToEndOfEgoLane};
  const auto freeLaneChangeLength{_laneChangeLengthCalculations->DetermineFreeLaneChangeLength(parameters.timeHeadwayFreeLaneChange, parameters.egoVelocityLongitudinal)};
  const auto laneChangeLengthFromObstacle{_laneChangeLengthCalculations->DetermineLaneChangeLengthFromObstacle(parameters.laneChangeObstacle, parameters.egoVelocityLongitudinal, surroundingVehicleQueryFactory)};

  const auto laneChangeLengthToReachExit{parameters.highwayExitInformation.has_value() ? _laneChangeLengthCalculations->DetermineLaneChangeLengthFromHighwayExit(*parameters.highwayExitInformation, parameters.egoVelocityLongitudinal) : ScmDefinitions::INF_DISTANCE};

  return std::min({distanceToEndOfLane,
                   laneChangeLengthFromObstacle,
                   freeLaneChangeLength,
                   laneChangeLengthToReachExit});
}

bool LaneChangeTrajectoryCalculations::EgoFrontVehicleMoreUrgentThanReference(const SurroundingVehicleInterface* egoFrontVehicle, const SurroundingVehicleInterface* referenceVehicle) const
{
  if (referenceVehicle == nullptr ||
      referenceVehicle->ToTheSideOfEgo() ||
      referenceVehicle->BehindEgo())
  {
    return true;
  }

  if (egoFrontVehicle != nullptr &&
      egoFrontVehicle->GetTtc().GetValue() < referenceVehicle->GetTtc().GetValue())
  {
    return true;
  }

  return false;
}

namespace
{
bool IsSuspiciousObjectOrLaneChanger(Situation situation)
{
  return situation == Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE ||
         situation == Situation::LANE_CHANGER_FROM_LEFT ||
         situation == Situation::LANE_CHANGER_FROM_RIGHT;
}

bool IsSideCollisionRisk(Situation situation)
{
  return situation == Situation::SIDE_COLLISION_RISK_FROM_LEFT || situation == Situation::SIDE_COLLISION_RISK_FROM_RIGHT;
}
}  // namespace

const SurroundingVehicleInterface* LaneChangeTrajectoryCalculations::DetermineReferenceObjectForLaneChange(Situation currentSituation, const TrajectoryPlanning::RelevantVehiclesForLaneChangeLength& relevantVehicles) const
{
  if (currentSituation == Situation::FOLLOWING_DRIVING || currentSituation == Situation::OBSTACLE_ON_CURRENT_LANE)
  {
    return relevantVehicles.causingVehicleFrontSituation;
  }

  if (IsSideCollisionRisk(currentSituation))
  {
    return relevantVehicles.causingVehicleSideSituation;
  }

  if (IsSuspiciousObjectOrLaneChanger(currentSituation))
  {
    auto* referenceVehicle{relevantVehicles.causingVehicleSideSituation};

    if (EgoFrontVehicleMoreUrgentThanReference(relevantVehicles.egoFrontVehicle, referenceVehicle))
    {
      return relevantVehicles.egoFrontVehicle;
    }

    return referenceVehicle;
  }

  return nullptr;
}

bool LaneChangeTrajectoryCalculations::CanFinishLaneChangeWithoutEnteringMinDistance(const TrajectoryPlanning::SafetyParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const
{
  const auto estimatedLaneChangeDuration{parameters.laneChangeLength / parameters.egoVelocityLongitudinal};

  if (parameters.closestVehicleOnTargetLane == nullptr)
  {
    return true;
  }

  const auto* opponent{parameters.closestVehicleOnTargetLane};
  auto aOpponent{0.0_mps_sq};
  auto deltaV{0.0_mps};
  if (opponent->BehindEgo())
  {
    aOpponent = -opponent->GetLongitudinalAcceleration().GetValue();
    deltaV = -surroundingVehicleQueryFactory.GetQuery(*opponent)->GetLongitudinalVelocityDelta();
  }
  else
  {
    aOpponent = opponent->GetLongitudinalAcceleration().GetValue();
    deltaV = surroundingVehicleQueryFactory.GetQuery(*opponent)->GetLongitudinalVelocityDelta();  // Negate sign, because distance gets smaller if ego is faster
  }

  const auto ds{(aOpponent / 2.0) * units::math::pow<2>(estimatedLaneChangeDuration) + deltaV * estimatedLaneChangeDuration};
  const auto currentDistance{opponent->GetRelativeNetDistance().GetValue()};

  const auto estimatedEndDistance{currentDistance + ds};
  return estimatedEndDistance >= parameters.minDistance;
}

units::length::meter_t LaneChangeTrajectoryCalculations::DetermineLaneChangeWidth(const TrajectoryPlanning::LaneChangeWidthParameters& parameters) const
{
  switch (parameters.relativeLane)
  {
    case RelativeLane::LEFT:
      return (parameters.egoLaneWidth / 2.0) - parameters.tStart + (parameters.neighbourLaneWidth / 2.0) + parameters.tEnd;
    case RelativeLane::RIGHT:
      return -1 * ((parameters.egoLaneWidth / 2.0) + parameters.tStart + (parameters.neighbourLaneWidth / 2.0) - parameters.tEnd);
    default:
      std::cerr << "ActionImplementation::DetermineLaneChangeWidth: Currently not supported RelativeLane called - return fallback of ego lane width: " << parameters.egoLaneWidth << std::endl;  // TODO Switch to scm logging
      return parameters.egoLaneWidth;
  }
}