/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  MentalModelDefinitions.h

//-----------------------------------------------------------------------------
//! @brief This file contains several structs to collect merge information
//-----------------------------------------------------------------------------

#pragma once
#include <optional>

#include "ScmCommons.h"
#include "SurroundingVehicles/SurroundingVehicle.h"
#include "include/common/ScmDefinitions.h"

namespace
{
//! Maximum time to force FRONT information update [ms].
const units::time::millisecond_t CRITICAL_UPDATE_TIME_EGO_FRONT{1000_ms};  // in ms criticalUpdateTimeEgoFront
//! Minimum time to avoid information update [ms].
const units::time::millisecond_t RECENT_UPDATE_THRESHOLD{2000_ms};
}  // namespace

//! @brief Merge action represents the different phases of a merging maneuver (longitudinal control)
enum class MergeAction
{
  DECELERATING = -1,
  CONSTANT_DRIVING = 0,
  ACCELERATING = 1
};

namespace Merge
{

//! @brief Collection of ego agent data needed for a merge gap
struct EgoInfo
{
  //! @brief length of ego agent
  units::length::meter_t length{0._m};
  //! @brief current velocity of ego agent
  units::velocity::meters_per_second_t velocity{0._mps};
  //! @brief factor determining the current need for a lane change
  double urgencyFactorForLaneChange{0.};

  //! @brief Checks whether two EgoInfo's are the same
  //! @param other the other to compare to
  //! @return true if they're the same, false otherwise
  bool operator==(const EgoInfo& other) const
  {
    return scm::common::DoubleEquality(length.value(), other.length.value()) &&
           scm::common::DoubleEquality(velocity.value(), other.velocity.value()) &&
           scm::common::DoubleEquality(urgencyFactorForLaneChange, other.urgencyFactorForLaneChange);
  }
};

//! @brief Indication if the gap position is in front or behind of the ego vehicle.
enum class GapPosition
{
  inFront,
  behind
};

//! @brief For a merge gap, there a usually two other agents involved (the leader and the follower of a gap between them).
//! The RegulatingVehicle is the one that is regulated on.
enum RegulatingVehicle
{
  leader,
  follower
};

//! @brief Minimum and maximum velocity which is considered for a merging maneuver.
struct VelocityLimits
{
  //! @brief minimum velocity
  units::velocity::meters_per_second_t vMin;
  //! @brief maximum velocity
  units::velocity::meters_per_second_t vMax;
};

//! @brief A merging maneuver is divided into different phases. A Phase consists of a MergeAction, the estimated time the phase should need and a remaining distance to the merge gap when the phase is over.
struct Phase
{
  //! @brief the merge action for this phase
  MergeAction mergeAction;
  //! @brief the estimated time this phase should need
  units::time::second_t time;
  //! @brief the remaining distance to the merge gap when this phase is done
  units::length::meter_t distance;

  //! @brief Checks whether two phases are the same
  //! @param phase the phase to compare to
  //! @return true if two phases are the same, false otherwise
  bool operator==(const Phase& phase) const
  {
    return (mergeAction == phase.mergeAction && scm::common::DoubleEquality(time.value(), phase.time.value()) && scm::common::DoubleEquality(distance.value(), phase.distance.value()));
  }
};

//! @brief The merge gap is mainly a storage for the data needed to perform complex merge maneuver.
struct Gap
{
  //! @brief Constructs a merge gap given a leader and follower
  //! @param leader leader of the merge gap (can be null)
  //! @param follower follower of the merge gap (can be null)
  Gap(const SurroundingVehicleInterface* leader, const SurroundingVehicleInterface* follower)
      : leader(leader), follower(follower)
  {
    if (leader != nullptr) leaderId = leader->GetId();
    if (follower != nullptr) followerId = follower->GetId();
    DetermineFullMergeDistance();
  };

  //! @brief The leading vehicle of a gap (can be null in certain situations where ego agent wants to merge in front of another vehicle when there is no other in reachable distance)
  const SurroundingVehicleInterface* leader{nullptr};
  //! @brief The following vehicle of a gap (can be null in certain situations where ego agents wants to merge behind another vehicle when there is no other in reachable distance)
  const SurroundingVehicleInterface* follower{nullptr};

  //! @brief The id of the gap leader - backup if for some reason the pointer gets invalidated after creation of this gap
  int leaderId{-1};
  //! @brief The id of the gap follower - backup if for some reason the pointer gets invalidated after creation of this gap
  int followerId{-1};
  //! @brief The time at which this gap/manoeuvre was planned in ms (from MentalModel::GetTime()).
  units::time::millisecond_t timestep{-1};
  //! @brief The relative lane (lateral direction) of this merge gap.
  RelativeLane relativeLane{RelativeLane::EGO};

  //! @brief The velocity the gap (i.e. the point behind the leader/ahead of the follower ego aims to reach) is moving at.
  units::velocity::meters_per_second_t velocity{ScmDefinitions::DEFAULT_VALUE};  // previous vGap

  //! @brief Indicates whether the targeted point for the lane change in the gap is in front of or behind ego.
  GapPosition position{};  // TODO is still needed considering the information provided by distance and regulatingVehicle?
  //! @brief Indicates whether the point ego want to start their lane change at is behind the leader or in front of the follower.
  RegulatingVehicle regulatingVehicle{};

  //! @brief A largely increased/decreased velocity ego uses before merging to approach the gap rapidly.
  units::velocity::meters_per_second_t approachingVelocity{ScmDefinitions::DEFAULT_VALUE};

  //! @brief The velocity ego aims to have when reaching the gap/the start of the lane change for a differential merge.
  //! Will explicitly deviate from `velocity` depending on the extra space available that allows for entering with and
  //! then reducing a speed difference.
  units::velocity::meters_per_second_t differentialMergingVelocity{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The total space of the gap, i.e. the distance between the rear of the leading vehicle and the front of
  //! the following vehicle.
  units::length::meter_t fullMergeSpace{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The relative distance to the point in the gap ego would start a lane change at. A negative distance
  //! indicates that ego is ahead of the gap. Measured at the front (mainLocatePoint) of the ego vehicle.
  //! (previously "wayToMergeRegulate")
  units::length::meter_t distance{ScmDefinitions::DEFAULT_VALUE};
  //! @brief The minimum distance ego intends to keep to the leader when changing into their lane. Measured from the
  //! rear of the leader vehicle to the front of the ego vehicle.
  units::length::meter_t safetyClearanceToLeader{0};
  //! @brief The minimum distance ego intends to keep to the follower when changing into their lane. Measured from the
  //! rear of the ego vehicle to the front of the follower vehicle.
  units::length::meter_t safetyClearanceToFollower{0};
  //! @brief The details for a merging manoeuvre using acceleration and deceleration to reach a gap with no velocity
  //! difference.
  std::vector<Phase> regularMergePhases{};  // previous "reachabilities"
  //! @brief The details for a merging manoeuvre starting with driving at a constant velocity and only adjusting it at
  //! the end.
  std::vector<Phase> constantDrivingMergePhases{};
  //! @brief The details for a merging manoeuvre with a merging velocity different from gap velocity.
  std::vector<Phase> differentialMergePhases{};

  //! @brief The details for the merging manoeuvre deemed best for the current gap.
  std::vector<Phase> selectedMergePhases{};

  //! @brief Determines the available distance between the leader and the follower of this merge gap. Can be INFINITY if there is either no leader or follower.
  void DetermineFullMergeDistance()
  {
    if (leader == nullptr || follower == nullptr)
    {
      fullMergeSpace = ScmDefinitions::INF_DISTANCE;
    }
    else
    {
      const ObstructionLongitudinal leaderObstructionLongitudinal{leader->GetLongitudinalObstruction()};
      const ObstructionLongitudinal followerObstructionLongitudinal{follower->GetLongitudinalObstruction()};
      fullMergeSpace = leaderObstructionLongitudinal.mainLaneLocator - leader->GetLength().GetValue() - followerObstructionLongitudinal.mainLaneLocator;
    }
  }

  void AddPhase(Phase phase)
  {
    regularMergePhases.push_back(phase);
  }
  void AddConstantDrivingPhase(Phase phase)
  {
    constantDrivingMergePhases.push_back(phase);
  }

  const SurroundingVehicleInterface* GetMergeRegulate() const  // TODO check if this method and all connected logic can be removed
  {
    if (leader == nullptr)
    {
      return follower;
    }
    else
    {
      return leader;
    }
  }

  //! @brief Checks whether a merge gap is valid or not
  //! @return True if a gap is valid, false otherwise
  bool IsValid() const
  {
    return (leader != nullptr || follower != nullptr) && !selectedMergePhases.empty();
  }

  MergeAction GetMergeAction() const
  {
    return selectedMergePhases.at(0).mergeAction;
  }
};

}  // namespace Merge