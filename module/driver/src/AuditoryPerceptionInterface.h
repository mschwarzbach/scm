/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "ScmDefinitions.h"

//! @brief enumeration of different acoustic stimulus directions
enum AcousticStimulusDirection
{
  FRONT = 0,
  RIGHT = 1,
  REAR = 2,
  LEFT = 3,
  DEFAULT = -1
};

//! @brief modells the driver's perception of acustic signals.
//! The auditory perception models the driver's hearing abilities. It detects ADAS warnings and information from
//! inside the driver's own vehicle and also sounds from other surrounding vehicles (e.g. horns and sirens).
//! After processing these sound information, the auditory perception affects the gaze behaviour of the driver
//! in the sub-module GazeControl (bottom-up gaze behaviour) and the information from the detected sound signals
class AuditoryPerceptionInterface
{
public:
  virtual ~AuditoryPerceptionInterface() = default;

  //! @brief Check for the perception of acoustic signals (sub-module main function)
  //! @param acousticAdasSignals    Information about acoustic warning signals from the current agent's own vehicle.
  //! @return is true when acoustic stimulus detected, otherwise false
  virtual bool IsAcousticStimulusDetected(std::vector<AdasHmiSignal> acousticAdasSignals) const = 0;

  //! @brief is acoustic signal active update stimulus data
  //! @param acousticAdasSignals
  virtual void UpdateStimulusData(std::vector<AdasHmiSignal> acousticAdasSignals) = 0;

  //! @brief Getter for flag if acoustic stimulus is processed.
  //! @return _acousticStimulusProcessed
  virtual bool IsAcousticStimulusProcessed() const = 0;

  //! @brief Getter for acoustic stimulus direction.
  //! @return acousticStimulusDirection
  virtual AcousticStimulusDirection GetAcousticStimulusDirection() const = 0;
};