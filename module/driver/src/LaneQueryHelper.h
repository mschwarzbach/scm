/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  LaneQueryHelper.h

#pragma once

#include "ScmCommons.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

/*! \addtogroup LaneQueryHelper
 * @{
 * \brief This component acts as a helper for checking a specific lane.
 *
 * \details static functions to check a specific lane against several conditions*/

//! \ingroup LaneQueryHelper
class LaneQueryHelper
{
  /** \name Public Members
   *  @{
   */
public:
  //! \brief Is the observed area of interest part of the egos left lane.
  //! \param aoi  Observed area of interest
  [[nodiscard]] static bool IsLeftLane(AreaOfInterest aoi) noexcept;

  //! \brief Is the observed area of interest part of the lane two lanes left of ego.
  //! \param aoi  Observed area of interest
  [[nodiscard]] static bool IsLeftLeftLane(AreaOfInterest aoi) noexcept;

  //! \brief Is the observed area of interest part of the egos right lane.
  //! \param aoi  Observed area of interest
  [[nodiscard]] static bool IsRightLane(AreaOfInterest aoi) noexcept;

  //! \brief Is the observed area of interest part of the lane two lanes right of ego.
  //! \param aoi  Observed area of interest
  [[nodiscard]] static bool IsRightRightLane(AreaOfInterest aoi) noexcept;

  //! \brief Is the observed area of interest part of the egos lane.
  //! \param aoi  Observed area of interest
  [[nodiscard]] static bool IsEgoLane(AreaOfInterest aoi) noexcept;

  //! \brief Is the observed lane marking of bold width.
  //! \param laneMarking  Observed lane marking
  [[nodiscard]] static bool IsLaneMarkingBold(const scm::LaneMarking::Entity& laneMarking) noexcept;

  //! \brief Is the observed lane marking of standard width
  //! \param laneMarking  Observed lane marking
  [[nodiscard]] static bool IsLaneMarkingStandardWidth(const scm::LaneMarking::Entity& laneMarking) noexcept;

  //! \brief Check whether AOI is in front of the ego vehicle.
  //! \param [in] aoi AreaOfInterest
  [[nodiscard]] static bool IsFrontArea(AreaOfInterest aoi) noexcept;

  //! \brief Check whether AOI is in in a front far region of the ego vehicle.
  //! \param [in] aoi AreaOfInterest
  [[nodiscard]] static bool IsFrontFarArea(AreaOfInterest aoi) noexcept;

  //! \brief Check whether AOI is behind the ego vehicle.
  //! \param [in] aoi AreaOfInterest
  [[nodiscard]] static bool IsRearArea(AreaOfInterest aoi) noexcept;

  //! \brief Check whether AOI is next to the ego vehicle.
  //! \param [in] aoi AreaOfInterest
  [[nodiscard]] static bool IsSideArea(AreaOfInterest aoi) noexcept;

  //! \brief Returns the relative lane given an AOI
  //! \param [in] aoi AreaOfInterest
  //! \return RelativeLane
  [[nodiscard]] static RelativeLane GetRelativeLaneFromAoi(AreaOfInterest aoi);

  //! \brief Returns the corresponding 'side' of a given relative lane
  //! \param [in] relativeLane RelativeLane
  //! \return Side
  [[nodiscard]] static Side GetSideFromRelativeLane(RelativeLane relativeLane);

  //! \brief Check wheter a given lane information is considered as 'shoulder lane' in SCM
  //! \param [in] laneInformation LaneInformationGeometrySCM
  //! \return true if lane is of type 'shoulder' or 'stop'
  [[nodiscard]] static bool IsLaneConsideredAsShoulder(const LaneInformationGeometrySCM& laneInformation) noexcept;
  /**
   * @} */
  // End of Public Members

private:
  static constexpr std::array<AreaOfInterest, 5> _frontAois{AreaOfInterest::LEFTLEFT_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHTRIGHT_FRONT, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::EGO_FRONT};
  static constexpr std::array<AreaOfInterest, 3> _frontFarAois{AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::EGO_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR};
  static constexpr std::array<AreaOfInterest, 5> _rearAois{AreaOfInterest::LEFT_REAR, AreaOfInterest::LEFTLEFT_REAR, AreaOfInterest::EGO_REAR, AreaOfInterest::RIGHT_REAR, AreaOfInterest::RIGHTRIGHT_REAR};
  static constexpr std::array<AreaOfInterest, 4> _sideAois{AreaOfInterest::LEFTLEFT_SIDE, AreaOfInterest::LEFT_SIDE, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHTRIGHT_SIDE};
};
/** @} */  // End of group LaneQueryHelper
