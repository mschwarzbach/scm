/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneChangeBehaviorQuery.h"

LaneChangeBehaviorQuery::LaneChangeBehaviorQuery(const MentalCalculationsInterface& mentalCalculations, const MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor)
    : _mentalModel{mentalModel},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations}
{
}

bool LaneChangeBehaviorQuery::EgoFitsInLane(SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  const bool laneExists = _mentalModel.IsShoulderLaneUsageLegit(SurroundingLane2RelativeLane.at(targetLane)) ? _mentalModel.GetLaneExistence(relativeLane, true) : _mentalModel.GetLaneExistence(relativeLane);
  const bool laneSmallerThanVehicle = _mentalModel.GetVehicleModelParameters().bounding_box.dimension.width >
                                      _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane).width;

  return laneExists && !laneSmallerThanVehicle;
}

SurroundingLane LaneChangeBehaviorQuery::DetermineLaneChangeWishFromIntensities(const LaneChangeIntensities& intensities) const
{
  if (intensities.EgoLaneIntensity >= intensities.LeftLaneIntensity &&
      intensities.EgoLaneIntensity >= intensities.RightLaneIntensity)
  {
    return SurroundingLane::EGO;
  }

  if (intensities.EgoLaneIntensity == 1. && intensities.LeftLaneIntensity == 1. && intensities.RightLaneIntensity == 1.)
  {
    return LaneWishAllIntensitiesAreEqual();
  }
  else if (intensities.EgoLaneIntensity == 1. && intensities.LeftLaneIntensity == 1.)
  {
    const auto meanLaneVelocityLeft{_mentalModel.GetMeanVelocity(SurroundingLane::LEFT)};
    std::optional<SurroundingLane> laneWish = LaneWishTwoIntensitiesAreEqual(meanLaneVelocityLeft);

    return laneWish.has_value() ? laneWish.value() : SurroundingLane::LEFT;
  }
  else if (intensities.EgoLaneIntensity == 1. && intensities.RightLaneIntensity == 1.)
  {
    const auto meanLaneVelocityRight{_mentalModel.GetMeanVelocity(SurroundingLane::RIGHT)};
    std::optional<SurroundingLane> laneWish = LaneWishTwoIntensitiesAreEqual(meanLaneVelocityRight);

    return laneWish.has_value() ? laneWish.value() : SurroundingLane::RIGHT;
  }

  if (intensities.LeftLaneIntensity > intensities.RightLaneIntensity)
  {
    return SurroundingLane::LEFT;
  }

  return SurroundingLane::RIGHT;
}

SurroundingLane LaneChangeBehaviorQuery::LaneWishAllIntensitiesAreEqual() const
{
  const auto meanLaneVelocityLeft{_mentalModel.GetMeanVelocity(SurroundingLane::LEFT)};
  const auto meanLaneVelocityEgo{_mentalModel.GetMeanVelocity(SurroundingLane::EGO)};
  const auto meanLaneVelocityRight{_mentalModel.GetMeanVelocity(SurroundingLane::RIGHT)};
  auto vMeanMax = std::max({meanLaneVelocityLeft, meanLaneVelocityEgo, meanLaneVelocityRight});

  const auto velocityMeanEgo = meanLaneVelocityEgo / vMeanMax;
  const auto velocityMeanLeft = meanLaneVelocityLeft / vMeanMax;
  const auto velocityMeanRight = meanLaneVelocityRight / vMeanMax;
  const auto maxScaledIntensity = std::max({velocityMeanEgo, velocityMeanLeft, velocityMeanRight});

  if (maxScaledIntensity == velocityMeanEgo)
  {
    return SurroundingLane::EGO;
  }

  if (maxScaledIntensity == velocityMeanLeft)
  {
    return SurroundingLane::LEFT;
  }
  return SurroundingLane::RIGHT;
}

std::optional<SurroundingLane> LaneChangeBehaviorQuery::LaneWishTwoIntensitiesAreEqual(units::velocity::meters_per_second_t meanLaneVelocity) const
{
  const auto meanLaneVelocityEgo{_mentalModel.GetMeanVelocity(SurroundingLane::EGO)};
  const auto vMeanMax = units::math::max(meanLaneVelocity, meanLaneVelocityEgo);

  const auto velocityMeanEgo = meanLaneVelocityEgo / vMeanMax;
  const auto velocityMean = meanLaneVelocity / vMeanMax;
  const auto maxScaledIntensity = std::max(velocityMeanEgo, velocityMean);

  if (maxScaledIntensity == velocityMeanEgo)
  {
    return SurroundingLane::EGO;
  }

  return std::nullopt;
}

double LaneChangeBehaviorQuery::CalculateLaneChangeIntensity(SurroundingLane targetLane, ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity) const
{
  constexpr auto MinimumVelocityDeltaForLaneChange = -10._kph;
  constexpr auto MaximumVelocityDeltaForLaneChange = 25._kph;

  const auto estimatedMeanLaneVelocity{_mentalModel.GetMeanVelocity(targetLane)};
  const auto targetVelocity = _mentalModel.GetAbsoluteVelocityTargeted();

  if (!componentsForLaneChangeIntensity.isDistanceToEndOfLaneSmallerOrEqToInfluencingDistance && !componentsForLaneChangeIntensity.isTimeAtTargetSpeed && !componentsForLaneChangeIntensity.isSlowBlocker)
  {
    if (targetVelocity > estimatedMeanLaneVelocity &&
        MinimumVelocityDeltaForLaneChange < (estimatedMeanLaneVelocity - targetVelocity))
    {
      return 1. - ((estimatedMeanLaneVelocity - targetVelocity) / MinimumVelocityDeltaForLaneChange);
    }

    if (targetVelocity < estimatedMeanLaneVelocity &&
        (estimatedMeanLaneVelocity - targetVelocity) < MaximumVelocityDeltaForLaneChange)
    {
      return 1. - ((estimatedMeanLaneVelocity - targetVelocity) / MaximumVelocityDeltaForLaneChange);
    }
  }
  return 0;
}

LaneChangeIntensities LaneChangeBehaviorQuery::AdjustLaneConvenienceDueToHighwayExit(const LaneChangeIntensities& inIntensities, bool isRightHandTraffic) const
{
  LaneChangeIntensities outIntensities{inIntensities};
  auto distanceToNextExit = _mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->distanceToEndOfNextExit;

  if (isRightHandTraffic)
  {
    auto lanetype = _mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->FromLane(SurroundingLane::RIGHT).laneType;
    if (!_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->takeNextExit && (lanetype == scm::LaneType::Exit || lanetype == scm::LaneType::OffRamp))
    {
      outIntensities.RightLaneIntensity = 0.0;
    }
  }
  else
  {
    auto lanetype = _mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->FromLane(SurroundingLane::LEFT).laneType;
    if (!_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->takeNextExit && (lanetype == scm::LaneType::Exit || lanetype == scm::LaneType::OffRamp))
    {
      outIntensities.LeftLaneIntensity = 0.0;
    }
  }

  if (distanceToNextExit <= 0._m)
  {
    return outIntensities;
  }

  LaneChangeDimension laneChangeDimension(_mentalModel);
  const double urgencyFactor{_mentalCalculations.DetermineUrgencyFactorForNextExit(true, laneChangeDimension)};
  const int relativeLaneIdJunction{_mentalModel.GetClosestRelativeLaneIdForJunctionIngoing()};
  const std::vector<int> exitLanes{_mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->relativeLaneIdForJunctionIngoing};
  const bool isOnExitLane{std::find(exitLanes.begin(), exitLanes.end(), 0) != exitLanes.end()};

  if (relativeLaneIdJunction < 0 && !isOnExitLane)  // Exit to agent's right
  {
    if (_mentalModel.GetLaneExistence(RelativeLane::RIGHT))
    {
      outIntensities.RightLaneIntensity = std::min(1., inIntensities.RightLaneIntensity + urgencyFactor);
      outIntensities.EgoLaneIntensity = std::max(0., inIntensities.EgoLaneIntensity - urgencyFactor);
    }
    else
    {
      outIntensities.RightLaneIntensity = 0.;
      if (outIntensities.EgoLaneIntensity != 0)
      {
        outIntensities.EgoLaneIntensity = std::min(1., inIntensities.EgoLaneIntensity + urgencyFactor);
      }
    }
    outIntensities.LeftLaneIntensity = CalculateIntensityHighwayExitLeft(urgencyFactor, outIntensities.LeftLaneIntensity);
  }
  else if (relativeLaneIdJunction > 0 && !isOnExitLane)  // Exit to agent's left
  {
    if (_mentalModel.GetLaneExistence(RelativeLane::LEFT))
    {
      outIntensities.LeftLaneIntensity = CalculateIntensityHighwayExitLeft(urgencyFactor, outIntensities.LeftLaneIntensity);
      outIntensities.EgoLaneIntensity = std::max(0., inIntensities.EgoLaneIntensity - urgencyFactor);
    }
    else
    {
      outIntensities.LeftLaneIntensity = CalculateIntensityHighwayExitLeft(urgencyFactor, outIntensities.LeftLaneIntensity);
      if (outIntensities.EgoLaneIntensity != 0)
      {
        outIntensities.EgoLaneIntensity = std::min(1., inIntensities.EgoLaneIntensity + urgencyFactor);
      }
    }

    outIntensities.RightLaneIntensity = std::max(0., inIntensities.RightLaneIntensity - urgencyFactor);
  }
  else if (isOnExitLane)  // Agent is on exit lane --> Prevent agent from crossing back onto the highway
  {
    auto laneMarkingLeft{_mentalCalculations.GetLaneMarkingAtDistance(0._m, Side::Left)};
    auto laneMarkingRight{_mentalCalculations.GetLaneMarkingAtDistance(0._m, Side::Right)};
    if (laneMarkingLeft.type == scm::LaneMarking::Type::Broken &&
        LaneQueryHelper::IsLaneMarkingBold(laneMarkingLeft))
    {
      outIntensities.LeftLaneIntensity = CalculateIntensityHighwayExitLeft(urgencyFactor, outIntensities.LeftLaneIntensity);
    }

    if (laneMarkingRight.type == scm::LaneMarking::Type::Broken &&
        LaneQueryHelper::IsLaneMarkingBold(laneMarkingRight))
    {
      outIntensities.RightLaneIntensity = 0.;
    }
  }

  return outIntensities;
}

double LaneChangeBehaviorQuery::CalculateIntensityHighwayExitLeft(const double urgencyFactor, double leftLaneIntensity) const
{
  // Enough Space
  const int closestRelativeExitLane{_mentalModel.GetClosestRelativeLaneIdForJunctionIngoing()};
  const int lanesToCross{std::abs(closestRelativeExitLane)};

  LaneChangeDimension laneChangeDimension(_mentalModel);
  auto overtakingLength{(lanesToCross + 2.5) * laneChangeDimension.EstimateLaneChangeLength()};
  const auto distanceToEndOfExit{_mentalModel.GetDistanceToEndOfNextExit()};

  if (distanceToEndOfExit < overtakingLength)
  {
    return leftLaneIntensity;
  }
  else
  {
    bool isCollision = false;
    bool isStatic = false;
    if (!_mentalModel.GetMicroscopicData()->GetSurroundingVehicleInformation()->Ahead().Close().empty())
    {
      isCollision = _mentalModel.GetMicroscopicData()->GetSurroundingVehicleInformation()->Ahead().Close().front().collision;
      isStatic = _mentalModel.GetMicroscopicData()->GetSurroundingVehicleInformation()->Ahead().Close().front().isStatic;
    }
    const RelativeLane relativeLane = RelativeLane::RIGHT;
    auto lane = _mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane);
    bool isLaneDrivable = _featureExtractor.IsLaneTypeDriveable(lane.laneType) || (LaneQueryHelper::IsLaneConsideredAsShoulder(lane) && _mentalModel.IsShoulderLaneUsageLegit(relativeLane));

    if (!isLaneDrivable && (isCollision || isStatic))
    {
      return leftLaneIntensity;
    }
    else
    {
      return leftLaneIntensity - urgencyFactor;
    }
  }
}

bool LaneChangeBehaviorQuery::HasObstacleOnLane(SurroundingLane targetLane) const
{
  if (targetLane == SurroundingLane::LEFT)
  {
    return _featureExtractor.IsObstacle(_mentalModel.GetVehicle(AreaOfInterest::LEFT_SIDE)) ||
           _featureExtractor.IsObstacle(_mentalModel.GetVehicle(AreaOfInterest::LEFT_FRONT));
  }
  if (targetLane == SurroundingLane::RIGHT)
  {
    return _featureExtractor.IsObstacle(_mentalModel.GetVehicle(AreaOfInterest::RIGHT_SIDE)) ||
           _featureExtractor.IsObstacle(_mentalModel.GetVehicle(AreaOfInterest::RIGHT_FRONT));
  }
  if (targetLane == SurroundingLane::EGO)
  {
    return _featureExtractor.IsObstacle(_mentalModel.GetVehicle(AreaOfInterest::EGO_FRONT));
  }
  throw std::runtime_error("HasObstacleOnLane - Invalid targetLane");
}

bool LaneChangeBehaviorQuery::IsLaneChangeSave(SurroundingLane targetLane) const
{
  constexpr auto MinimumTimeAtTargetSpeedThreshold{20._s};

  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};
  const auto relativeLaneId = RelativeLane2RelativeLaneId.at(relativeLane);
  const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};

  const auto laneType{_mentalModel.GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane).laneType};
  const bool laneDrivable{_featureExtractor.IsLaneTypeDriveable(laneType)};
  const auto legalVelocity{_mentalModel.GetLegalVelocity(targetLane)};
  const auto distanceToEndOfLane{_mentalModel.GetDistanceToEndOfLane(relativeLane)};
  const auto timeAtTargetSpeedInLane{_featureExtractor.GetTimeAtTargetSpeedInLane(targetLane)};
  const auto velocityTargeted{_mentalModel.GetAbsoluteVelocityTargeted()};
  const auto legalSpeedViolation{_mentalModel.GetDriverParameters().amountOfSpeedLimitViolation + legalVelocity};

  const bool distanceToEndOfLaneSmallerOrEqToInfluencingDistance{distanceToEndOfLane <= _mentalModel.GetInfluencingDistanceToEndOfLane()};
  const bool targetVelocityAboveViolationLimit{velocityTargeted > legalSpeedViolation};
  const bool timeAtTargetSpeedBelowThreshold{timeAtTargetSpeedInLane < MinimumTimeAtTargetSpeedThreshold};

  return !(HasObstacleOnLane(targetLane) ||
           distanceToEndOfLaneSmallerOrEqToInfluencingDistance ||
           targetVelocityAboveViolationLimit ||
           timeAtTargetSpeedBelowThreshold ||
           !laneDrivable);
}

bool LaneChangeBehaviorQuery::ShouldChangeLaneInJam(SurroundingLane targetLane) const
{
  constexpr auto MinimumEgoSpeed{5._kph};

  const auto estimatedMeanLaneVelocity{_mentalModel.GetMeanVelocity(targetLane)};
  const auto relativeNetDistance{GetRelativeNetDistance(targetLane)};
  const auto egoSpeed{_mentalModel.GetAbsoluteVelocityEgo(true)};
  const auto minimumMeanVelocityThreshold{std::max(egoSpeed * 1.1, 1._mps)};  // Minimum required velocity to consider the lane preferable
  const bool aboveVelocityThreshold{egoSpeed >= MinimumEgoSpeed};
  const bool hasHigherMeanVelocity{estimatedMeanLaneVelocity > minimumMeanVelocityThreshold};
  const auto enoughSpaceThreshold{_mentalModel.GetCarQueuingDistance() * 2.};
  const bool hasEnoughSpace{relativeNetDistance > enoughSpaceThreshold};

  return (hasHigherMeanVelocity && aboveVelocityThreshold);  // TODO use after clarification || hasEnoughSpace;
}

units::length::meter_t LaneChangeBehaviorQuery::GetRelativeNetDistance(SurroundingLane targetLane) const
{
  if (targetLane == SurroundingLane::LEFT)
  {
    if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE))
    {
      return 0._m;
    }

    if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT))
    {
      return _mentalModel.GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  if (targetLane == SurroundingLane::RIGHT)
  {
    if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE))
    {
      return 0._m;
    }

    if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT))
    {
      return _mentalModel.GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  if (targetLane == SurroundingLane::EGO)
  {
    if (_mentalModel.GetIsVehicleVisible(AreaOfInterest::EGO_FRONT))
    {
      return _mentalModel.GetRelativeNetDistance(AreaOfInterest::EGO_FRONT);
    }

    return ScmDefinitions::INF_DISTANCE;
  }

  throw std::runtime_error("GetRelativeNetDistance called with invalid tagetLane");
}

double LaneChangeBehaviorQuery::CalculateIntensityForQueuedTraffic(double laneIntensities, SurroundingLane targetLane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(targetLane)};

  if (!_mentalModel.GetLaneExistence(relativeLane))
  {
    return 0;
  }

  const auto aoi{SurroundingLane2AreaOfInterest.at(targetLane)};
  if (aoi != AreaOfInterest::EGO_FRONT)
  {
    const auto relativeNetDistance{GetRelativeNetDistance(targetLane)};
    const auto enoughSpaceThreshold{_mentalModel.GetCarQueuingDistance() + _mentalModel.GetVehicleLength()};

    bool isEnoughSpaceForLaneChange = false;
    if (relativeNetDistance > enoughSpaceThreshold)
    {
      isEnoughSpaceForLaneChange = true;
    }

    Side side = targetLane == SurroundingLane::LEFT ? Side::Left : Side::Right;
    if (!GetIsLaneChangePermittedDueToLaneMarkings(side) || !isEnoughSpaceForLaneChange)
    {
      return 0;
    }
  }

  auto laneIntensity = _mentalModel.GetMeanVelocity(targetLane).value() / _mentalModel.GetTrafficJamVelocityThreshold().value();

  return units::math::max(0., laneIntensity);
}

LaneChangeIntensities LaneChangeBehaviorQuery::AdjustLaneChangeIntensitiesForJam(const LaneChangeIntensities& inIntensities) const
{
  LaneChangeIntensities outIntensities{inIntensities};
  outIntensities.LeftLaneIntensity = CalculateIntensityForQueuedTraffic(outIntensities.LeftLaneIntensity, SurroundingLane::LEFT);
  outIntensities.RightLaneIntensity = CalculateIntensityForQueuedTraffic(outIntensities.RightLaneIntensity, SurroundingLane::RIGHT);
  outIntensities.EgoLaneIntensity = CalculateIntensityForQueuedTraffic(outIntensities.EgoLaneIntensity, SurroundingLane::EGO);

  return NormalizeLaneChangeIntensities(outIntensities);
}

bool LaneChangeBehaviorQuery::EgoBelowJamSpeed() const
{
  return _featureExtractor.HasJamVelocityEgo();
}

bool LaneChangeBehaviorQuery::IsOuterLaneOvertakingProhibited(bool rightHandTraffic) const
{
  return _featureExtractor.DoesOuterLaneOvertakingProhibitionApply(AreaOfInterest::EGO_FRONT, rightHandTraffic);
}

units::time::second_t LaneChangeBehaviorQuery::GetTimeAtTargetSpeedInLane(SurroundingLane lane) const
{
  return _featureExtractor.GetTimeAtTargetSpeedInLane(lane);
}

bool LaneChangeBehaviorQuery::CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface* vehicle) const
{
  return _featureExtractor.CanVehicleCauseCollisionCourseForNonSideAreas(vehicle);
}

bool LaneChangeBehaviorQuery::GetIsInfluencingDistanceViolated(const SurroundingVehicleInterface* vehicle) const
{
  return _featureExtractor.IsInfluencingDistanceViolated(vehicle);
}

bool LaneChangeBehaviorQuery::GetIsLaneChangePermittedDueToLaneMarkings(Side side) const
{
  return _featureExtractor.IsLaneChangePermittedDueToLaneMarkings(side);
}

LaneChangeIntensities LaneChangeBehaviorQuery::NormalizeLaneChangeIntensities(const LaneChangeIntensities& intensities) const
{
  LaneChangeIntensities outIntensities = intensities;

  if (outIntensities.LeftLaneIntensity > 1.0 || outIntensities.RightLaneIntensity > 1.0 || outIntensities.EgoLaneIntensity > 1.0)
  {
    auto maxLaneIntensity = std::max({outIntensities.LeftLaneIntensity, outIntensities.RightLaneIntensity, outIntensities.EgoLaneIntensity});

    outIntensities.LeftLaneIntensity /= maxLaneIntensity;
    outIntensities.RightLaneIntensity /= maxLaneIntensity;
    outIntensities.EgoLaneIntensity /= maxLaneIntensity;
  }
  return outIntensities;
}