/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "LivingState.h"
#include "ScmComponentsInterface.h"
#include "ScmDependencies.h"
#include "ScmSignalCollector.h"
#include "SpawningState.h"
#include "include/Logging/PublisherInterface.h"
#include "include/module/driverInterface.h"

namespace scm
{
class ScmDriver : public scm::driver::Interface
{
public:
  const std::string COMPONENT_NAME = "ScmDriver";

  ScmDriver(StochasticsInterface* stochastics, units::time::millisecond_t cycleTime, scm::publisher::PublisherInterface* const scmPublisher);
  virtual ~ScmDriver() = default;

  scm::signal::DriverOutput Trigger(const scm::signal::DriverInput& driverInput, units::time::millisecond_t time) override;
  void Trigger(units::time::millisecond_t time);

#ifdef TESTING_ENABLED
  friend class TestScmImpl;
#endif

protected:
  bool HasExternalControlStateChanged();
  void SetExternalControlFlags(const HafParameters& hafParameters);

  ScmSignalCollector _scmSignalCollector;
  std::unique_ptr<ScmComponentsInterface> _scmComponents{nullptr};
  ExternalControlState _externalControlState;
  ExternalControlState _externalControlStateLastStep;
  bool _isEgoViewInInstrumentCluster = false;

private:
  scm::publisher::PublisherInterface* const _scmPublisher;
  std::variant<std::monostate, SpawningState, LivingState, CollidedState> _scmModelState;
  std::unique_ptr<ScmDependenciesInterface> _scmDependencies{nullptr};
  bool _initialized = false;

  units::time::millisecond_t _cycleTime{};
  std::unique_ptr<PeriodicLoggerInterface> _pLog{nullptr};

  std::vector<AdasHmiSignal> opticAdasSignals;
  std::vector<AdasHmiSignal> acousticAdasSignals;
  AdasHmiSignal acousticAndOpticAdasSignal;

  void ProgressScmModelState();
  bool IsInitializable();
  bool IsSpawningState();
  bool IsLivingState();

  //  void SetSensoryStimuli(const Warnings &compCtrlWarnings);

  bool _isHafTakeoverRequest = false;
};

}  // namespace scm