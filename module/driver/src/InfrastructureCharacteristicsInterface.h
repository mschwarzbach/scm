/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  InfrastructureCharacteristicsInterface.h

#pragma once
#include "ScmDefinitions.h"

struct LaneInformationTrafficRulesScmExtended : public LaneInformationTrafficRulesSCM
{
  // Speed limit, relative distance to traffic sign, time of last detection, speed of ego when detecting
  units::velocity::meters_per_second_t speedLimit{ScmDefinitions::INF_VELOCITY};
  units::length::meter_t relativeDistanceToTrafficsign{999.0_m};
  units::time::millisecond_t timeOfLastDetection{0.0_ms};
  units::velocity::meters_per_second_t speedWhenDetected{0.0_mps};
  units::velocity::meters_per_second_t previousRelevantSpeedLimit{ScmDefinitions::INF_VELOCITY};
  bool signDetected{false};
};

using TrafficRuleInformationScmExtended = scm::LateralCategorization<LaneInformationTrafficRulesScmExtended, 5>;

//! \ingroup InfrastructureCharacteristics

//! *******************************************************************************************************************************************************************
//! @brief Contains the infrastructure related information about traffic controls and geometric features of the lanes.
//! @details This component contains the infrastructure related information about traffic controls and geometric features of the lanes as known by the current agent.
//! *******************************************************************************************************************************************************************
class InfrastructureCharacteristicsInterface
{
public:
  virtual ~InfrastructureCharacteristicsInterface() = default;
  //! \brief Set parameters when class is initialized.
  //! \param [in] cycleTime               Cycle time of the simulation
  //! \param [in] trafficRuleInformation  Data struct from sensor driver
  //! \param [in] geometryInformation     Data struct from sensor driver
  virtual void Initialize(units::time::millisecond_t cycleTime, TrafficRuleInformationScmExtended* trafficRuleInformation = nullptr, GeometryInformationSCM* geometryInformation = nullptr) = 0;

  //! \brief Set the current type of the left lane marking
  //! \param [in] laneTypeLeftLast   Current type of the left lane marking.
  virtual void SetLaneMarkingTypeLeftLast(scm::LaneMarking::Type laneTypeLeftLast) = 0;

  //! \brief Get the current type of the left lane marking
  //! \return int  Current type of the left lane marking.
  virtual scm::LaneMarking::Type GetLaneMarkingTypeLeftLast() const = 0;

  //! \brief Set the current type of the right lane marking
  //! \param [in] laneTypeRightLast   Current type of the right lane marking.
  virtual void SetLaneMarkingTypeRightLast(scm::LaneMarking::Type laneTypeRightLast) = 0;

  //! \brief Get the current type of the right lane marking
  //! \return int  Current type of the right lane marking.
  virtual scm::LaneMarking::Type GetLaneMarkingTypeRightLast() const = 0;

  //! \brief Set traffic rule information (traffic signs, lane markings). Alteration due to the agents perception is possible.
  //! \param [in] trafficRuleInformation   Data struct containing traffic rule information (traffic signs, lane markings)
  virtual void SetTrafficRuleInformation(TrafficRuleInformationSCM* trafficRuleInformation) = 0;

  //! \brief Set road geometry information. Alteration due to the agents perception is possible.
  //! \param [in] geometryInformation     Data struct containing road geometry information
  //! \param [in] velocityEgoEstimated    Estimated velocity of the ego agent
  //! \param [in] takeNextExit            Is the driver supposed to take the next highway exit or not?
  //! \param [in] laneIdEgo               Absolute Lane ID of the ego lane
  virtual void SetGeometryInformation(GeometryInformationSCM* geometryInformation, units::velocity::meters_per_second_t velocityEgoEstimated, bool takeNextExit, int laneIdEgo) = 0;

  //! \brief Helper method for shortcutting GetTrafficRuleInformation->GetLane()
  //! \param [in] relativeLane  Relative lane for which the traffic rule information is returned.
  //! \return Traffic rule information for the given relative lane.
  virtual const LaneInformationTrafficRulesScmExtended& GetLaneInformationTrafficRules(RelativeLane relativeLane) const = 0;
  virtual LaneInformationTrafficRulesScmExtended* UpdateLaneInformationTrafficRules(RelativeLane relativeLane) = 0;

  //! \brief Getter for the TrafficRuleInformation struct.
  //! \return trafficRuleInformation
  virtual TrafficRuleInformationScmExtended* GetTrafficRuleInformation() const = 0;

  //! \brief Getter for GeometryInformationSCM, the geometry and lanegeometry information of SCM
  //! \return GeometryInformationSCM
  virtual GeometryInformationSCM* GetGeometryInformation() const = 0;

  //! \brief Helper method for shortcutting GetGeometryInformation->GetLane()
  //! \param [in] relativeLane  Relative lane for which the geometry information is returned.
  //! \return Lane Geometry information for the given relative lane.
  virtual const LaneInformationGeometrySCM& GetLaneInformationGeometry(RelativeLane relativeLane) const = 0;

  //! \brief Returns the extended Traffic Rule Information
  //! \return TrafficRuleInformationScmExtended
  virtual TrafficRuleInformationScmExtended* UpdateTrafficRuleInformation() = 0;

  //! \brief Update Geometry Information
  //! \return GeometryInformationSCM*
  virtual GeometryInformationSCM* UpdateGeometryInformation() = 0;

  //! \brief Update Lane Information Geometry
  //! \return LaneInformationGeometrySCM*
  virtual LaneInformationGeometrySCM* UpdateLaneInformationGeometry(RelativeLane relativeLane) = 0;

  //! \brief Returns if a given relativeLane is useable by the driver. In case of an emergency, the mere existence of a lane is sufficient, otherwise it must also have a valid LaneType.
  //! [in] RelativeLane relativeLane
  //! [in] bool isEmergency
  virtual bool GetLaneExistence(RelativeLane relativeLane, bool isEmergency = false) const = 0;

  //! \brief Check lane existence of a side lane
  //! \param aoi      Area of interest
  //! \param bool     isEmergency
  virtual bool CheckLaneExistence(AreaOfInterest aoi, bool isEmergency = true) const = 0;
};