/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SurroundingVehicleInterface.h

#pragma once

#include "ExtrapolationInterface.h"
#include "InformationAcquisitionInterface.h"
#include "ScmDefinitions.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! ************************************************************************************************
//! @brief Interface class, a collection of functionality for dealing with the surrounding vehicles.
//! @details This includes information updates, the perception, metadata and vehicle parameter,
//! the localization of the surrounding vehicle and much more.
//! ************************************************************************************************
class SurroundingVehicleInterface
{
public:
  virtual ~SurroundingVehicleInterface() = default;

  //! @brief Extrapolates the surrounding object instance using the Extrapolate method from the InformationAcquisition class.
  //! @param extrapolation Reference to the Extrapolation of the InformationAcquisition instance that contains this SurroundingVehicle
  virtual void Extrapolate(const ExtrapolationInterface& extrapolation) = 0;

  //! @brief Update the agent information with ground truth data.
  //! Sets the groundTruth and perceptionAoi internal data.
  //! @param gtObject Ground truth data for this agent
  //! @param aoi Area of interest in which the agent was perceived
  //! @param infoAcquisition Pointer to InformationAcquisition that contains this agent
  virtual void UpdateWithGroundTruth(const ObjectInformationSCM* gtObject, AreaOfInterest aoi, const InformationAcquisitionInterface* infoAcquisition) = 0;

  //! @brief Handles setting of data, not present in side AOIs using the method CorrectAoiDataDueToLongitudinalTransitionFromSideArea from InformationAcquisition.
  //! @param newAoi AreaOfInterest that is assigned to this vehicle in the current timestep
  //! @param infoAcquisition Pointer to InformationAcquisition that contains this agent
  virtual void HandleTransitionFromSideArea(AreaOfInterest newAoi, const InformationAcquisitionInterface* infoAcquisition) = 0;

  //! @brief Handles resetting of data, not present in side AOIs using the method CorrectAoiDataDueToLongitudinalTransitionToSideArea from InformationAcquisition.
  //! @param infoAcquisition  Pointer to InformationAcquisition that contains this agent
  virtual void HandleTransitionToSideArea(const InformationAcquisitionInterface* infoAcquisition) = 0;

  //! @brief Set the time when the surrounding vehicle was seen.
  //! @param time simulation time of the last perception
  virtual void SetTimeOfLastPerception(units::time::millisecond_t time) = 0;

  //! @brief Set the aoi where the surrounding vehicle was seen.
  //! @param aoi Area of Interest of the surrounding vehicle
  virtual void AssignAoi(AreaOfInterest aoi) = 0;

  //! @brief Set the (relative) lane of the surrounding vehicle.
  //! @param lane relative lane of the surrounding vehicle.
  virtual void AssignLane(RelativeLane lane) = 0;

  //! @brief Reset the flag if the surrounding vehicle was just perceived.
  virtual void ResetWasPerceivedFlag() = 0;

  //! @brief Set the reliability for all change rates to the given value.
  //! @param reliability
  virtual void SetReliability(double reliability) = 0;

  //! @brief Decay all reliability classes using the functions defined in Reliability.h
  //! @param cycleTime
  virtual void DecayReliability(units::time::millisecond_t cycleTime) = 0;

  //! @brief Gets the id of the surrounding vehicle.
  //! @return the id
  virtual int GetId() const = 0;

  //! @brief Gets the flag if this vehicle was just perceived.
  //! @return true if this agent contains ground truth information
  virtual bool WasPerceived() const = 0;

  //! @brief Returns the object information contained in this instance.
  virtual const ObjectInformationScmExtended* GetObjectInformation() const = 0;

  //! @brief Gets the ttc threshold looming.
  //! @return the ObjectInformationSCM::thresholdLooming
  virtual double GetTtcThresholdLooming() const = 0;

  //! @brief Checks if the required quality is still reliable
  //! @param requiredQuality
  //! @param changingRate
  //! @return true if the data of the given changing rate still suffices the requested required quality
  virtual bool IsReliable(double requiredQuality, ParameterChangeRate changingRate = ParameterChangeRate::FAST) const = 0;

  //! @brief Get the reliability for the given changing rate class.
  //! @param changingRate
  //! @return the reliability
  virtual double GetReliability(ParameterChangeRate changingRate = ParameterChangeRate::FAST) const = 0;

  //! @brief Get the angle to the optical axis.
  //! @return ObjectInformationScmExtended::opticalAnglePositionHorizontal
  virtual units::angle::radian_t GetAngleToOpticalAxis() const = 0;

  //! @brief Get the time when the surrounding vehicle was last perceived.
  //! @return time of last perception
  virtual units::time::millisecond_t GetTimeOfLastPerception() const = 0;

  //! @brief Gets the longitudinal distance from ego front to the front of this agent.
  //! @return longitudinalObstruction.mainLaneLocator value
  virtual VehicleParameter<units::length::meter_t> GetRelativeLongitudinalPosition() const = 0;

  //! @brief Gets the relative lateral position to this surrounding vehicle.
  //! @return the lateral offset between the ego MLP and this agents MLP
  virtual VehicleParameter<units::length::meter_t> GetRelativeLateralPosition() const = 0;

  //! @brief Get the front distance.
  //! @details Wrapper for GetRelativeLongitudinalPosition
  //! @return the longitudinal distance from ego front to the front of this agent
  virtual VehicleParameter<units::length::meter_t> FrontDistance() const = 0;

  //! @brief Get the rear distance.
  //! @return the longitudinal distance from ego front to the rear of this agent
  virtual VehicleParameter<units::length::meter_t> RearDistance() const = 0;

  //! @brief Get the left distance.
  //! @return the lateral distance from the ego center to the left edge of the bounding box of this agent.
  virtual VehicleParameter<units::length::meter_t> LeftDistance() const = 0;

  //! @brief Get the right distance.
  //! @return the lateral distance from the ego center to the right edge of the bounding box of this agent.
  virtual VehicleParameter<units::length::meter_t> RightDistance() const = 0;

  //! @brief Get if there is any overlapping bounding boxes of the two surrounding vehicles.
  //! @param other Opposing agent to check for overlaps with
  //! @return true if the bounding box of this agent overlaps with the bounding box of the given agent.
  virtual VehicleParameter<bool> Overlaps(const SurroundingVehicleInterface& other) const = 0;

  //! @brief Checks if there is a longitudinal overlap between the ego and this agent.
  //! @return longitudinalObstruction.isOverlapping
  virtual VehicleParameter<bool> ToTheSideOfEgo() const = 0;

  //! @brief Checks if the surrounding vehicle is behind.
  //! @return true if the agent is behind the ego vehicle
  virtual VehicleParameter<bool> BehindEgo() const = 0;

  //! @brief Checks if the surrounding vehicle is in front.
  //! @return true if the agent is in front of the ego vehicle
  virtual VehicleParameter<bool> InFrontOfEgo() const = 0;

  //! @brief Checks whether there is a lateral overlap with this surrounding vehicle.
  //! @return true if there is a lateral overlap between this agent and the ego agent
  virtual VehicleParameter<bool> LateralObstructionOverlapping() const = 0;

  //! @brief Gets the relative lane of this surrounding vehicle.
  //! @return the relative lane of the AOI in which the agent was perceived or assigned in the last iteration.
  virtual VehicleParameter<RelativeLane> GetLaneOfPerception() const = 0;

  //! @brief Gets the assigned Area of Interest.
  //! @return the AOI in which the agent was perceived or assigned in the last iteration.
  virtual VehicleParameter<AreaOfInterest> GetAssignedAoi() const = 0;

  //! @brief Returns the bounding box of this surrounding vehicle.
  //! @return the corners of the vehicles bounding box.
  virtual VehicleParameter<Scm::BoundingBox> GetBoundingBox() const = 0;

  //! @brief Check if vehicle has collided.
  //! @return ObjectInformationScmExtended::collision
  virtual VehicleParameter<bool> IsCollided() const = 0;

  //! @brief Checks if the lateral motion is perceivable.
  //! @return true if the lateral motion of the object is perceivable
  virtual VehicleParameter<bool> IsExceedingLateralMotionThreshold() const = 0;

  //! @brief Get the time to collision to this surrounding vehicle.
  //! @return ObjectInformationScmExtended::ttc
  virtual VehicleParameter<units::time::second_t> GetTtc() const = 0;

  //! @brief Get the change in tau of this surrounding vehicle..
  //! @return ObjectInformationScmExtended::tauDot
  virtual VehicleParameter<double> GetTauDot() const = 0;

  //! @brief Get the time headway of this surrounding vehicle..
  //! @return ObjectInformationScmExtended::gap
  virtual VehicleParameter<units::time::second_t> GetThw() const = 0;

  //! @brief Get the lateral obstruction of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::obstruction
  virtual VehicleParameter<ObstructionScm> GetLateralObstruction() const = 0;

  //! @brief Gets the longitudinal obstruction of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::longitudinalObstruction
  virtual VehicleParameter<ObstructionLongitudinal> GetLongitudinalObstruction() const = 0;

  //! @brief Gets the lateral velocity of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::lateralVelocity
  virtual VehicleParameter<units::velocity::meters_per_second_t> GetLateralVelocity() const = 0;

  //! @brief Gets the longitudinal velocity of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::longitudinalVelocity
  virtual VehicleParameter<units::velocity::meters_per_second_t> GetLongitudinalVelocity() const = 0;

  //! @brief Gets the longitudinal acceleration of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::acceleration
  virtual VehicleParameter<units::acceleration::meters_per_second_squared_t> GetLongitudinalAcceleration() const = 0;

  //! @brief Gets the absolute velocity of this surrounding vehicle.
  //! @return absolute velocity (combined lateral and longitudinal velocity)
  virtual VehicleParameter<units::velocity::meters_per_second_t> GetAbsoluteVelocity() const = 0;

  //! @brief Gets the length of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::length
  virtual VehicleParameter<units::length::meter_t> GetLength() const = 0;

  //! @brief Gets the width of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::width
  virtual VehicleParameter<units::length::meter_t> GetWidth() const = 0;

  //! @brief Gets the height of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::height
  virtual VehicleParameter<units::length::meter_t> GetHeight() const = 0;

  //! @brief Gets the lateral position in lane (t-coordinate).
  //! @return ObjectInformationScmExtended::lateralPositionInLane
  virtual VehicleParameter<units::length::meter_t> GetLateralPositionInLane() const = 0;

  //! @brief Gets if the surrounding vehicle is considered as static.
  //! @return ObjectInformationScmExtended::isStatic
  virtual VehicleParameter<bool> IsStatic() const = 0;

  //! @brief Gets the relative net distance to this surrounding vehicle (depends if the vehicle is behind or in front of ego!).
  //! @details Note: this is not the same as the distance returned by GetRelativeLongitudinalPosition!
  //! @return Either -front or -rear of ObjectInformationScmExtended::longitudinalObstruction
  virtual VehicleParameter<units::length::meter_t> GetRelativeNetDistance() const = 0;

  //! @brief Gets the distance to the lane boundary.
  //! @param side
  //! @return ObjectInformationScmExtended::distanceToLaneBoundaryLeft or ObjectInformationScmExtended::distanceToLaneBoundaryRight
  virtual VehicleParameter<units::length::meter_t> GetDistanceToLaneBoundary(Side side) const = 0;

  //! @brief Get the vehicle classification of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::vehicleClassification
  virtual VehicleParameter<scm::common::VehicleClass> GetVehicleClassification() const = 0;

  //! @brief Get the projected dimensions of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::projectedDimensions
  virtual VehicleParameter<ProjectedSpacialDimensions> GetProjectedDimensions() const = 0;

  //! @brief Get the indicator state of this surrounding vehicle.
  //! @return ObjectInformationScmExtended::indicatorState
  virtual VehicleParameter<scm::LightState::Indicator> GetIndicatorState() const = 0;
};
