/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LeadingVehicleSelector.h

#pragma once

#include "LaneQueryHelper.h"
#include "SurroundingVehicles/LeadingVehicleFilter.h"
#include "SurroundingVehicles/LeadingVehicleQueryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryInterface.h"

//! **********************************************************************************************************
//! @brief Class to select a suitable leading vehicle, i.e. the prevalent vehicle for longitudinal guidance.
//! @details If the agent is relatively slow (within the limits of traffic jam velocity threshold), then out
//! of the relevant vehicles, the one with the lowest net distance is selected, otherwise with the lowest ttc.
//! **********************************************************************************************************
class LeadingVehicleSelector
{
public:
  LeadingVehicleSelector(const SurroundingVehicleQueryFactoryInterface& factory, const LeadingVehicleFilterInterface& filter, const LeadingVehicleQueryInterface& leadingVehicleQuery);

  //! @brief Selects the most suitable leading vehicle.
  //! @param vehicles surrounding vehicles
  //! @return the chosen leading vehicle or std::nullopt if no leading vehicle was found. Collision course vehicles are given precedence over other vehicles
  std::optional<const SurroundingVehicleInterface*> FindLeadingVehicle(const std::vector<const SurroundingVehicleInterface*>& vehicles) const;

  //! @brief Determines a set of vehicles which could be potential leading vehicles.
  //! @param vehicles surrounding vehicles
  //! @return the filtered vehicle vector with all filters of LeadingVehicleFilter applied sequentially.
  std::vector<const SurroundingVehicleInterface*> DeterminePossibleRegulates(std::vector<const SurroundingVehicleInterface*> vehicles) const;

  //! @brief Determines a set of vehicles which are on a potential collision course.
  //! @param vehicles surrounding vehicles
  //! @return all vehicles for which HasCollisionCourse returns true.
  std::vector<const SurroundingVehicleInterface*> DetermineCollisionCourseVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const;

private:
  const SurroundingVehicleQueryFactoryInterface& _queryFactory;
  const LeadingVehicleFilterInterface& _filter;
  const LeadingVehicleQueryInterface& _leadingVehicleQuery;

  //! @brief Determines the most important vehicle based on the given sorting criteria.
  //! @param vehicles relevant surrounding vehicles as potential leading vehicle
  //! @param sortingCriterion std::function<bool> as sorting criterion
  //! @return the element of the vehicles vector that has the smallest predicate defined by the given sorting criterion.
  const SurroundingVehicleInterface* FindMostImportantVehicle(const std::vector<const SurroundingVehicleInterface*>& vehicles, const std::function<bool(const SurroundingVehicleInterface*, const SurroundingVehicleInterface*)>& sortingCriterion) const;
};