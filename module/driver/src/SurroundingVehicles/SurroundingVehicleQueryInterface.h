/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SurroundingVehicleQueryInterface.h

#pragma once

#include "ScmDefinitions.h"

//! *****************************************************************************************
//! @brief Interface class for queries to relate between ego agent and a surrounding vehicle.
//! *****************************************************************************************
class SurroundingVehicleQueryInterface
{
public:
  virtual ~SurroundingVehicleQueryInterface() = default;
  //! @brief The expected time to enter and leave the lateral obstruction for the current vehicle.
  //! @return ObstructionDynamics
  virtual ObstructionDynamics GetLateralObstructionDynamics() const = 0;

  //! @brief Gets the lateral velocity delta.
  //! @return the difference between lateral ego velocity and observed vehicle velocity
  virtual units::velocity::meters_per_second_t GetLateralVelocityDelta() const = 0;

  //! @brief Gets the longitudinal velocity delta.
  //! @return the difference between longitudinal ego velocity and observed vehicle velocity
  virtual units::velocity::meters_per_second_t GetLongitudinalVelocityDelta() const = 0;

  //! @brief Gets the longitudinal acceleration delta.
  //! @return the difference between longitudinal ego acceleration and observed vehicle acceleration
  virtual units::acceleration::meters_per_second_squared_t GetLongitudinalAccelerationDelta() const = 0;

  //! @brief Check if ego is moving faster in lateral direction than observed vehicle.
  //! @return true if ego moves faster in lateral direction
  virtual bool EgoMovingFasterLateral() const = 0;

  //! @brief Gets the longitudinal delta distance based on given time.
  //! @details Note that the delta caused by acceleration/deceleration is extrapolated for a maximum of 1 second because of high volatility.
  //! @param time time frame
  //! @return the covered distance delta between the observed vehicle and ego during the given time
  virtual units::length::meter_t DeltaDistancePerTime(units::time::second_t time) const = 0;

  //! @brief Checks whether the longitudinal obstruction can be cleared in time.
  //! @return true if a side vehicle will overlap with the ego vehicle at some point until ego is fully ahead
  virtual bool CannotClearLongitudinalObstructionBeforeCollision() const = 0;

  //! @brief Checks whether the given minimum distance would be violated before the obstruction can be cleared.
  //! @param minDistance minimum distance
  //! @return true if the ego vehicle will enter the given minimum following distance before the lateral obstruction between ego and the observed vehicle is cleared
  virtual bool WillEnterMinDistanceBeforeObstructionIsCleared(units::length::meter_t minDistance) const = 0;

  //! @brief Checks whether the vehicle has a collision course with ego, i.e. if there are critical ttcs known or obstruction which cannot be cleared in time.
  //! @details The is no collision course considered with vehicle which are behind of ego.
  //! @param minDistance minimum distance
  //! @return true if a collision course is detected
  virtual bool HasCollisionCourse(units::length::meter_t minDistance) const = 0;

  //! @brief Checks whether the observed agent has started a lane change but did not cross the lane border yet.
  //! @return true if the observed agent is moving lateral but has not crossed the lane border
  virtual bool IsChangingLanesStillOnStartLane() const = 0;

  //! @brief Checks whether the observed agent hast just started a lane change into the ego agents' lane.
  //! @return true if the observed agent is on a neighboring lane and is moving laterally across the lane center
  virtual bool IsChangingIntoEgoLane() const = 0;

  //! @brief Checks whether the observed agent performs a lane change from a neighbor neighbor lane to ego's neighbor lane, thus becomes relevant for egos safety.
  //! @return true if the observed agent is on either LEFTLEFT or RIGHTRIGHT lane and is leaving its lane towards a neighboring lane
  virtual bool IsSideSideVehicleChangingIntoSide() const = 0;
};