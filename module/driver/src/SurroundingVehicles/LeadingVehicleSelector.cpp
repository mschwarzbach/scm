/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LeadingVehicleSelector.h"

#include <cmath>

#include "LeadingVehicleQueryInterface.h"
#include "SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"

namespace SortingCriteria
{
static const auto NetDistance = [](const SurroundingVehicleInterface* lhs, const SurroundingVehicleInterface* rhs)
{
  const auto lhsDistance{units::math::hypot(lhs->GetRelativeLongitudinalPosition().GetValue(), lhs->GetRelativeLateralPosition().GetValue())};
  const auto rhsDistance{units::math::hypot(rhs->GetRelativeLongitudinalPosition().GetValue(), rhs->GetRelativeLateralPosition().GetValue())};

  return lhsDistance < rhsDistance;
};

static const auto THW = [](const SurroundingVehicleInterface* lhs, const SurroundingVehicleInterface* rhs)
{
  return lhs->GetThw().GetValue() < rhs->GetThw().GetValue();
};

static const auto TTC = [](const SurroundingVehicleInterface* lhs, const SurroundingVehicleInterface* rhs)
{
  return lhs->GetTtc().GetValue() < rhs->GetTtc().GetValue();
};
};  // namespace SortingCriteria

LeadingVehicleSelector::LeadingVehicleSelector(const SurroundingVehicleQueryFactoryInterface& factory, const LeadingVehicleFilterInterface& filter, const LeadingVehicleQueryInterface& leadingVehicleQuery)
    : _queryFactory{factory},
      _filter{filter},
      _leadingVehicleQuery{leadingVehicleQuery}
{
}

const SurroundingVehicleInterface* LeadingVehicleSelector::FindMostImportantVehicle(const std::vector<const SurroundingVehicleInterface*>& vehicles, const std::function<bool(const SurroundingVehicleInterface*, const SurroundingVehicleInterface*)>& sortingCriterion) const
{
  return *std::min_element(begin(vehicles),
                           end(vehicles),
                           sortingCriterion);
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleSelector::DetermineCollisionCourseVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  auto collisionDetector = [this](const SurroundingVehicleInterface* vehicle)
  {
    const auto query{_queryFactory.GetQuery(*vehicle)};
    const auto minDistance{_leadingVehicleQuery.GetMinDistance(*vehicle)};
    return query->HasCollisionCourse(minDistance);
  };

  std::vector<const SurroundingVehicleInterface*> collisionCourseVehicles{};
  std::copy_if(begin(vehicles), end(vehicles), std::back_inserter(collisionCourseVehicles), collisionDetector);

  return collisionCourseVehicles;
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleSelector::DeterminePossibleRegulates(std::vector<const SurroundingVehicleInterface*> vehicles) const
{
  vehicles = _filter.FilterRearVehicles(vehicles);
  vehicles = _filter.FilterSideSideVehicles(vehicles);
  vehicles = _filter.FilterSwervingTarget(vehicles);
  vehicles = _filter.FilterSideLaneVehicles(vehicles);

  return vehicles;
}

std::optional<const SurroundingVehicleInterface*> LeadingVehicleSelector::FindLeadingVehicle(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  const auto relevantVehicles{DeterminePossibleRegulates(vehicles)};
  const auto collisionVehicles{DetermineCollisionCourseVehicles(relevantVehicles)};
  if (!collisionVehicles.empty())
  {
    const auto sortBy{_leadingVehicleQuery.BelowJamVelocity() ? SortingCriteria::NetDistance : SortingCriteria::TTC};
    return {FindMostImportantVehicle(collisionVehicles, sortBy)};
  }

  const auto mergeRegulateAoi{_leadingVehicleQuery.GetMergeRegulateIfDominating(vehicles)};
  if (mergeRegulateAoi.has_value())
  {
    const auto mergeRegulate = std::find_if(begin(vehicles), end(vehicles), [mergeRegulateAoi](const auto* vehicle)
                                            { return vehicle->GetAssignedAoi() == mergeRegulateAoi.value(); });

    if (mergeRegulate != end(vehicles))
    {
      return *mergeRegulate;
    }
  }

  if (!relevantVehicles.empty())
  {
    const auto sortBy{_leadingVehicleQuery.BelowJamVelocity() ? SortingCriteria::NetDistance : SortingCriteria::THW};
    return {FindMostImportantVehicle(relevantVehicles, sortBy)};
  }

  return std::nullopt;
}