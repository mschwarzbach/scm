/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "SurroundingVehicle.h"

#include <cmath>

#include "Reliability.h"

SurroundingVehicle::SurroundingVehicle(const ObjectInformationScmExtended& objectData, AreaOfInterest perceptionAoi)
    : _objectData{objectData},
      _assignedAoi(perceptionAoi)
{
}

void SurroundingVehicle::Extrapolate(const ExtrapolationInterface& extrapolation)
{
  extrapolation.Extrapolate(&_objectData, _assignedAoi);
  _groundTruthData = false;
}

int SurroundingVehicle::GetId() const
{
  return _objectData.id;
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetRelativeLongitudinalPosition() const
{
  return {_objectData.longitudinalObstruction.mainLaneLocator, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetRelativeLateralPosition() const
{
  return {_objectData.relativeLateralDistance, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::FrontDistance() const
{
  return GetRelativeLongitudinalPosition().GetValue();
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::RearDistance() const
{
  const auto frontDistance{FrontDistance()};
  return {frontDistance.GetValue() - _objectData.length, frontDistance.GetReliability()};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::LeftDistance() const
{
  const auto lateralPosition{GetRelativeLateralPosition()};
  return {lateralPosition.GetValue() + _objectData.width / 2, lateralPosition.GetReliability()};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::RightDistance() const
{
  const auto lateralPosition{GetRelativeLateralPosition()};
  return {lateralPosition.GetValue() - _objectData.width / 2, lateralPosition.GetReliability()};
}

VehicleParameter<bool> SurroundingVehicle::Overlaps(const SurroundingVehicleInterface& other) const
{
  // https://stackoverflow.com/a/306332
  return {LeftDistance().GetValue() > other.RightDistance().GetValue() &&
              RightDistance().GetValue() < other.LeftDistance().GetValue() &&
              FrontDistance().GetValue() > other.RearDistance().GetValue() &&
              RearDistance().GetValue() < other.FrontDistance().GetValue(),
          _reliabilities.at(ParameterChangeRate::SLOW)};
}

void SurroundingVehicle::UpdateWithGroundTruth(const ObjectInformationSCM* gtObject, const AreaOfInterest aoi, const InformationAcquisitionInterface* infoAcquisition)
{
  _groundTruthData = true;
  _assignedAoi = aoi;
  _assignedLane = AreaOfInterest2RelativeLane.at(aoi);
  infoAcquisition->SetAoiDataWithGroundTruth(&_objectData, gtObject, aoi);
  SetTimeOfLastPerception(infoAcquisition->GetTime());
}

VehicleParameter<bool> SurroundingVehicle::ToTheSideOfEgo() const
{
  return {_objectData.longitudinalObstruction.isOverlapping, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<bool> SurroundingVehicle::BehindEgo() const
{
  return {_objectData.longitudinalObstruction.mainLaneLocator < 0._m && !_objectData.longitudinalObstruction.isOverlapping,
          _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<bool> SurroundingVehicle::InFrontOfEgo() const
{
  return {_objectData.longitudinalObstruction.mainLaneLocator > 0.0_m, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<bool> SurroundingVehicle::LateralObstructionOverlapping() const
{
  return {_objectData.obstruction.isOverlapping, _reliabilities.at(ParameterChangeRate::SLOW)};
}

bool SurroundingVehicle::WasPerceived() const
{
  return _groundTruthData;
}

VehicleParameter<RelativeLane> SurroundingVehicle::GetLaneOfPerception() const
{
  return {_assignedLane, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<AreaOfInterest> SurroundingVehicle::GetAssignedAoi() const
{
  return {_assignedAoi, _reliabilities.at(ParameterChangeRate::SLOW)};
}

const ObjectInformationScmExtended* SurroundingVehicle::GetObjectInformation() const
{
  return &_objectData;
}

void SurroundingVehicle::HandleTransitionFromSideArea(const AreaOfInterest newAoi, const InformationAcquisitionInterface* infoAcquisition)
{
  infoAcquisition->CorrectAoiDataDueToLongitudinalTransitionFromSideArea(&_objectData, newAoi);
}

void SurroundingVehicle::HandleTransitionToSideArea(const InformationAcquisitionInterface* infoAcquisition)
{
  infoAcquisition->CorrectAoiDataDueToLongitudinalTransitionToSideArea(&_objectData);
}

VehicleParameter<Scm::BoundingBox> SurroundingVehicle::GetBoundingBox() const
{
  Scm::BoundingBox boundingBox{
      .leftFront = {FrontDistance().GetValue(), LeftDistance().GetValue()},
      .rightFront = {FrontDistance().GetValue(), RightDistance().GetValue()},
      .leftRear = {RearDistance().GetValue(), LeftDistance().GetValue()},
      .rightRear = {RearDistance().GetValue(), RightDistance().GetValue()}};

  return {boundingBox, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<bool> SurroundingVehicle::IsCollided() const
{
  return {_objectData.collision, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

VehicleParameter<units::time::second_t> SurroundingVehicle::GetTtc() const
{
  return {_objectData.ttc, _reliabilities.at(ParameterChangeRate::FAST)};
}

VehicleParameter<units::time::second_t> SurroundingVehicle::GetThw() const
{
  return {_objectData.gap, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

VehicleParameter<ObstructionScm> SurroundingVehicle::GetLateralObstruction() const
{
  return {_objectData.obstruction, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<ObstructionLongitudinal> SurroundingVehicle::GetLongitudinalObstruction() const
{
  return {_objectData.longitudinalObstruction, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<units::velocity::meters_per_second_t> SurroundingVehicle::GetLateralVelocity() const
{
  return {_objectData.lateralVelocity, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

VehicleParameter<units::velocity::meters_per_second_t> SurroundingVehicle::GetLongitudinalVelocity() const
{
  return {_objectData.longitudinalVelocity, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

VehicleParameter<units::velocity::meters_per_second_t> SurroundingVehicle::GetAbsoluteVelocity() const
{
  const auto lateralVelocity{GetLateralVelocity().GetValue()};
  const auto longitudinalVelocity{GetLongitudinalVelocity().GetValue()};

  const auto absoluteVelocity{units::math::sqrt(units::math::pow<2>(lateralVelocity) + units::math::pow<2>(longitudinalVelocity))};
  return {absoluteVelocity, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

VehicleParameter<units::acceleration::meters_per_second_squared_t> SurroundingVehicle::GetLongitudinalAcceleration() const
{
  return {_objectData.acceleration, _reliabilities.at(ParameterChangeRate::FAST)};
}

VehicleParameter<double> SurroundingVehicle::GetTauDot() const
{
  return {_objectData.tauDot, _reliabilities.at(ParameterChangeRate::FAST)};
}

VehicleParameter<bool> SurroundingVehicle::IsExceedingLateralMotionThreshold() const
{
  const auto lateralVelocity{GetLateralVelocity()};
  const auto reliability{lateralVelocity.GetReliability()};
  const auto lateralVelocityAbsolute{units::math::abs(lateralVelocity.GetValue())};

  if (lateralVelocityAbsolute == 0._mps)
  {
    return {false, reliability};
  }

  if (ToTheSideOfEgo())
  {
    constexpr auto THRESHOLD_VELOCITY_SIDE_MOVEMENT{0.5_mps};
    return {lateralVelocityAbsolute > THRESHOLD_VELOCITY_SIDE_MOVEMENT, reliability};
  }

  const auto netDistance{units::math::abs(RearDistance().GetValue() > 0_m ? RearDistance().GetValue() : FrontDistance().GetValue())};
  const auto loomingCriteria = lateralVelocityAbsolute / netDistance;
  return {(loomingCriteria.value() > _objectData.thresholdLooming), reliability};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetLength() const
{
  return {_objectData.length, _reliabilities.at(ParameterChangeRate::CONSTANT)};
}

void SurroundingVehicle::AssignAoi(AreaOfInterest aoi)
{
  _assignedAoi = aoi;
}

void SurroundingVehicle::AssignLane(RelativeLane lane)
{
  _assignedLane = lane;
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetLateralPositionInLane() const
{
  return {_objectData.lateralPositionInLane, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<bool> SurroundingVehicle::IsStatic() const
{
  return {_objectData.isStatic, _reliabilities.at(ParameterChangeRate::CONSTANT)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetRelativeNetDistance() const
{
  // Note: this is not the same as the distance returned by GetRelativeLongitudinalPosition!
  return {BehindEgo() ? -_objectData.longitudinalObstruction.front : -_objectData.longitudinalObstruction.rear,
          _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetDistanceToLaneBoundary(Side side) const
{
  if (side == Side::Left)
  {
    return {_objectData.distanceToLaneBoundaryLeft, _reliabilities.at(ParameterChangeRate::SLOW)};
  }

  return {_objectData.distanceToLaneBoundaryRight, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetWidth() const
{
  return {_objectData.width, _reliabilities.at(ParameterChangeRate::CONSTANT)};
}

VehicleParameter<units::length::meter_t> SurroundingVehicle::GetHeight() const
{
  return {_objectData.height, _reliabilities.at(ParameterChangeRate::CONSTANT)};
}

VehicleParameter<scm::common::VehicleClass> SurroundingVehicle::GetVehicleClassification() const
{
  return {_objectData.vehicleClassification, _reliabilities.at(ParameterChangeRate::CONSTANT)};
}

VehicleParameter<ProjectedSpacialDimensions> SurroundingVehicle::GetProjectedDimensions() const
{
  return {_objectData.projectedDimensions, _reliabilities.at(ParameterChangeRate::SLOW)};
}

VehicleParameter<scm::LightState::Indicator> SurroundingVehicle::GetIndicatorState() const
{
  return {_objectData.indicatorState, _reliabilities.at(ParameterChangeRate::MEDIUM)};
}

double SurroundingVehicle::GetTtcThresholdLooming() const
{
  return _objectData.thresholdLooming;
}

bool SurroundingVehicle::IsReliable(double requiredQuality, ParameterChangeRate changingRate) const
{
  return _reliabilities.at(changingRate) >= requiredQuality;
}

double SurroundingVehicle::GetReliability(ParameterChangeRate changingRate) const
{
  return _reliabilities.at(changingRate);
}

units::angle::radian_t SurroundingVehicle::GetAngleToOpticalAxis() const
{
  return _objectData.opticalAnglePositionHorizontal;
}

void SurroundingVehicle::SetTimeOfLastPerception(units::time::millisecond_t time)
{
  _timeOfLastPerception = time;
}

units::time::millisecond_t SurroundingVehicle::GetTimeOfLastPerception() const
{
  return _timeOfLastPerception;
}

void SurroundingVehicle::ResetWasPerceivedFlag()
{
  _groundTruthData = false;
}

void SurroundingVehicle::SetReliability(double reliability)
{
  for (const auto& [changeRate, _] : _reliabilities)
  {
    _reliabilities.at(changeRate) = reliability;
  }
}

void SurroundingVehicle::DecayReliability(units::time::millisecond_t cycleTime)
{
  for (const auto& [changeRate, currentReliability] : _reliabilities)
  {
    double newReliability{ReliabilityCalculations::DecayReliability(currentReliability, changeRate, cycleTime)};
    _reliabilities.at(changeRate) = newReliability;
  }
}