/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "SurroundingVehiclesModifier.h"

#include <algorithm>
#include <memory>

#include "GazeControl/GazeParameters.h"
#include "LaneQueryHelper.h"
#include "Reliability.h"
#include "SurroundingVehicleDefinitions.h"
#include "SurroundingVehicleInterface.h"
#include "include/ScmMathUtils.h"
#include "include/common/ScmDefinitions.h"

SurroundingVehiclesModifier::SurroundingVehiclesModifier(SurroundingVehicles& data)
    : _data(data)
{
}

void SurroundingVehiclesModifier::HandleSideAreaTransitions(AoiVehicleMapping* aoiMapping, const InformationAcquisitionInterface* informationAcquisition)
{
  for (const auto& [assignedAoi, vehicleVec] : *aoiMapping)
  {
    if (LaneQueryHelper::IsSideArea(assignedAoi))
    {
      for (auto* vehicle : vehicleVec)
      {
        if (!LaneQueryHelper::IsSideArea(vehicle->GetAssignedAoi()))
        {
          vehicle->HandleTransitionToSideArea(informationAcquisition);
        }
      }
    }
    else
    {
      for (auto* vehicle : vehicleVec)
      {
        if (LaneQueryHelper::IsSideArea(vehicle->GetAssignedAoi()))
        {
          vehicle->HandleTransitionFromSideArea(assignedAoi, informationAcquisition);
        }
      }
    }
  }
}

void SurroundingVehiclesModifier::WriteToMicroscopicData(const AoiVehicleMapping& aoiMapping, MicroscopicCharacteristicsInterface* microscopicCharacteristics, const InformationAcquisitionInterface* infoAcquisiion)
{
  for (const auto aoi : SURROUNDING_AREAS_OF_INTEREST)
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      auto* microscopicSideVehicleVector{microscopicCharacteristics->UpdateSideObjectsInformation(aoi)};
      if (aoiMapping.count(aoi) > 0 && !aoiMapping.at(aoi).empty())
      {
        microscopicSideVehicleVector->clear();
        for (const auto& surroundingVehicle : aoiMapping.at(aoi))
        {
          auto updatedObject{*surroundingVehicle->GetObjectInformation()};
          microscopicSideVehicleVector->push_back(updatedObject);
        }
      }
      else
      {
        if (!microscopicSideVehicleVector->empty())
        {
          auto first_element{microscopicSideVehicleVector->at(0)};
          infoAcquisiion->ResetAoiDataForNoObject(&first_element);
          microscopicSideVehicleVector->clear();
          microscopicSideVehicleVector->push_back(first_element);
        }
        else
        {
          // should not happen, but can be handled
          microscopicSideVehicleVector->emplace_back();
        }
      }
    }
    else
    {
      if (aoiMapping.count(aoi) > 0 && !aoiMapping.at(aoi).empty())
      {
        const auto& updatedObject{*aoiMapping.at(aoi).front()->GetObjectInformation()};
        if (auto* information{microscopicCharacteristics->UpdateObjectInformation(aoi)})
        {
          *information = updatedObject;
        }
      }
      else if (auto* vehicleObj{microscopicCharacteristics->UpdateObjectInformation(aoi)})
      {
        infoAcquisiion->ResetAoiDataForNoObject(vehicleObj);
      }
    }
  }
}

void SurroundingVehiclesModifier::WriteToMicroscopicDataVector(const AoiVehicleMapping& aoiMapping, MicroscopicCharacteristicsInterface* microscopicCharacteristics)
{
  for (const auto aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    auto objectdata = microscopicCharacteristics->GetObjectVector(aoi);
    objectdata->clear();

    if (aoiMapping.count(aoi) > 0 && !aoiMapping.at(aoi).empty())
    {
      for (const auto vehicle : aoiMapping.at(aoi))
      {
        objectdata->push_back(*vehicle->GetObjectInformation());
      }
    }
    else
    {
      objectdata->push_back(ObjectInformationScmExtended{});
    }
  }
}

void SetReliability(SurroundingVehicleInterface* vehicle, bool idealPerception)
{
  static constexpr double MAXIMUM_RELIABILITY{100.0};
  double reliability{vehicle->GetReliability()};
  double opticalReliability{idealPerception ? MAXIMUM_RELIABILITY : ReliabilityCalculations::CalculateReliability(static_cast<units::angle::radian_t>(vehicle->GetAngleToOpticalAxis()))};
  reliability = std::max(reliability, opticalReliability);
  vehicle->SetReliability(reliability);
}

std::vector<AreaOfInterest> SurroundingVehiclesModifier::RemoveVisibleVehiclesNotPresentInGroundTruth(const std::vector<int>& visibleAgentIdsGt, const MentalModelInterface* mentalModel)
{
  auto vehicleVisibleButNotInGroundTruth = [visibleAgentIdsGt, mentalModel](const std::unique_ptr<SurroundingVehicleInterface>& vehicle)
  {
    const bool isVisible{mentalModel->IsVisible(vehicle->GetBoundingBox(), GazeParameters::HORIZONTAL_SIZE_USEFUL_PERIPHERY)};
    const bool notInGt{std::find(begin(visibleAgentIdsGt), end(visibleAgentIdsGt), vehicle->GetId()) == end(visibleAgentIdsGt)};

    return isVisible && notInGt;
  };

  // Reorders vehicles so that all plausible observations are at front
  // beginVehiclesToRemove is the iterator to the first element to be removed
  auto beginVehiclesToRemove = std::partition(begin(_data._surroundingVehicles),
                                              end(_data._surroundingVehicles),
                                              std::not_fn(vehicleVisibleButNotInGroundTruth));
  std::vector<AreaOfInterest> removedAois;

  if (beginVehiclesToRemove != end(_data._surroundingVehicles))
  {
    std::vector<std::pair<int, AreaOfInterest>> removedVehicles;
    for (auto it = beginVehiclesToRemove; it != end(_data._surroundingVehicles); it++)
    {
      const auto aoi{(*it)->GetAssignedAoi()};
      removedVehicles.emplace_back((*it)->GetId(), aoi);
      removedAois.push_back(aoi);
    }

    _data._surroundingVehicles.erase(beginVehiclesToRemove, end(_data._surroundingVehicles));
  }

  // delete duplicate aois
  removedAois.erase(std::unique(begin(removedAois), end(removedAois)), end(removedAois));
  return removedAois;
}

void ResetWasPerceivedFlags(const std::vector<std::unique_ptr<SurroundingVehicleInterface>>& vehicles)
{
  for (auto& vehicle : vehicles)
  {
    vehicle->ResetWasPerceivedFlag();
  }
}

void SurroundingVehiclesModifier::UpdateVehicles(const InformationAcquisitionInterface* infoAcquisition, const ExtrapolationInterface& extrapolation, bool perceiveVehicles, bool idealPerception)
{
  ResetWasPerceivedFlags(_data._surroundingVehicles);

  for (auto& vehicle : _data._surroundingVehicles)
  {
    vehicle->Extrapolate(extrapolation);
    vehicle->DecayReliability(infoAcquisition->GetCycleTime());
  }

  if (perceiveVehicles || idealPerception)
  {
    const auto perceivedVehicles{infoAcquisition->PerceiveSurroundingVehicles(idealPerception)};

    UpdateVisibleVehicles(perceivedVehicles, infoAcquisition, idealPerception);
  }

  RemoveOutOfSightVehicles(infoAcquisition);
  ForgetLongUnseenVehicles(infoAcquisition->GetTime());
}

void SurroundingVehiclesModifier::RemoveOutOfSightVehicles(const InformationAcquisitionInterface* infoAcquisition)
{
  _data._surroundingVehicles.erase(
      std::remove_if(begin(_data._surroundingVehicles), end(_data._surroundingVehicles), [infoAcquisition](const std::unique_ptr<SurroundingVehicleInterface>& vehicle)
                     { return infoAcquisition->IsOutOfSight(vehicle->GetRelativeLongitudinalPosition().GetValue()); }),
      _data._surroundingVehicles.end());
}

std::vector<SurroundingVehicleInterface*> SurroundingVehiclesModifier::Update()
{
  std::vector<SurroundingVehicleInterface*> returnVector;
  returnVector.reserve(_data._surroundingVehicles.size());
  std::transform(begin(_data._surroundingVehicles), end(_data._surroundingVehicles), std::back_inserter(returnVector), [](const auto& ptr)
                 { return ptr.get(); });
  return returnVector;
}

bool SurroundingVehiclesModifier::CheckForOverlaps(const SurroundingVehicleInterface* vehicle) const
{
  return std::any_of(begin(_data._surroundingVehicles), end(_data._surroundingVehicles), [vehicle](const auto& comparisonVehicle)
                     { return vehicle != comparisonVehicle.get() &&
                              vehicle->Overlaps(*comparisonVehicle); });
}

std::vector<AreaOfInterest> SurroundingVehiclesModifier::DetectInconsistenciesFromExtrapolation(const AoiVehicleMapping& aoiVehicleMapping, const InfrastructureCharacteristicsInterface& infrastructureData) const
{
  std::vector<AreaOfInterest> inconsistentAois;
  for (const auto& [assignedAoi, vehiclesInAoi] : aoiVehicleMapping)
  {
    for (const auto& vehicle : vehiclesInAoi)
    {
      if (!vehicle->IsCollided() && CheckForOverlaps(vehicle))
      {
        inconsistentAois.push_back(assignedAoi);
        break;
      }

      // if lane of new aoi does not exist make last known aoi invalid
      if (!infrastructureData.CheckLaneExistence(assignedAoi, true) &&
          infrastructureData.CheckLaneExistence(vehicle->GetAssignedAoi(), true))
      {
        inconsistentAois.push_back(vehicle->GetAssignedAoi());
        break;
      }
    }
  }

  return inconsistentAois;
}

void SurroundingVehiclesModifier::Clear()
{
  _data._surroundingVehicles.clear();
}

void SurroundingVehiclesModifier::ResetAllAois()
{
  for (auto& vehicle : _data._surroundingVehicles)
  {
    vehicle->AssignAoi(AreaOfInterest::NumberOfAreaOfInterests);
  }
}

void SurroundingVehiclesModifier::AssignAreasOfInterest(const AoiVehicleMapping& aoiVehicleMapping)
{
  ResetAllAois();
  for (const auto& [aoi, vehiclesInAoi] : aoiVehicleMapping)
  {
    for (auto* vehicle : vehiclesInAoi)
    {
      vehicle->AssignAoi(aoi);
    }
  }
}

void SurroundingVehiclesModifier::ForgetLongUnseenVehicles(units::time::millisecond_t currentTimestep)
{
  static constexpr auto MAX_UNSEEN_DURATION{7000_ms};

  _data._surroundingVehicles.erase(
      std::remove_if(begin(_data._surroundingVehicles), end(_data._surroundingVehicles), [currentTimestep](const std::unique_ptr<SurroundingVehicleInterface>& vehicle)
                     { return vehicle->GetTimeOfLastPerception() + MAX_UNSEEN_DURATION < currentTimestep; }),
      _data._surroundingVehicles.end());
}

void SurroundingVehiclesModifier::AssignRelativeLane(const LaneVehicleMapping& laneMapping)
{
  for (auto& [relativeLane, vehicleVector] : laneMapping)
  {
    for (auto& vehicle : vehicleVector)
    {
      vehicle->AssignLane(relativeLane);
    }
  }
}

void SurroundingVehiclesModifier::UpdateVisibleVehicles(const std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>>& gtVehicles, const InformationAcquisitionInterface* infoAcquisition, bool idealPerception)
{
  for (const auto& [aoi, gtObject] : gtVehicles)
  {
    if (!gtObject || gtObject->id == -1)
    {
      continue;
    }
    const auto vehicleId = gtObject->id;
    auto surroundingVehicleToUpdate = std::find_if(begin(_data._surroundingVehicles), end(_data._surroundingVehicles), [vehicleId](std::unique_ptr<SurroundingVehicleInterface>& vehicle)
                                                   { return vehicleId == vehicle->GetId(); });
    if (surroundingVehicleToUpdate == end(_data._surroundingVehicles))
    {
      ObjectInformationScmExtended newObject;
      _data.Add(newObject, aoi);
      surroundingVehicleToUpdate = std::prev(_data._surroundingVehicles.end());
      surroundingVehicleToUpdate->get()->UpdateWithGroundTruth(gtObject, aoi, infoAcquisition);
      SetReliability(surroundingVehicleToUpdate->get(), idealPerception);
    }
    else if (units::math::abs(gtObject->opticalAnglePositionHorizontal) > GazeParameters::HORIZONTAL_SIZE_UFOV)  // Periphery Case
    {
      const auto calculatedReliability{ReliabilityCalculations::CalculateReliability(gtObject->opticalAnglePositionHorizontal)};

      auto detectedPeripheralInaccuracy = ShouldUpdateDueDetectedPeripheralInaccuracy(gtObject, surroundingVehicleToUpdate->get());
      auto currentReliability = surroundingVehicleToUpdate->get()->GetReliability();
      if (calculatedReliability > surroundingVehicleToUpdate->get()->GetReliability() || idealPerception || detectedPeripheralInaccuracy)
      {
        surroundingVehicleToUpdate->get()->UpdateWithGroundTruth(gtObject, aoi, infoAcquisition);

        if (detectedPeripheralInaccuracy)
        {
          /* SetReliability(..) would always take the max reliability of the old and new one.
           * In this case though, we want to be able to explicitly set a lower reliability caused by a peripheral update
           * thus it is set directly in the vehicle. */
          surroundingVehicleToUpdate->get()->SetReliability(calculatedReliability);
        }
        else
        {
          SetReliability(surroundingVehicleToUpdate->get(), idealPerception);
        }
      }
    }
    else
    {
      surroundingVehicleToUpdate->get()->UpdateWithGroundTruth(gtObject, aoi, infoAcquisition);
      SetReliability(surroundingVehicleToUpdate->get(), idealPerception);
    }
  }
}

bool static NeedsUpdateLongitudinal(const ObjectInformationSCM* object, const SurroundingVehicleInterface* surroundingVehicleToUpdate, units::length::meter_t threshold)
{
  return units::math::abs(surroundingVehicleToUpdate->GetRelativeLongitudinalPosition().GetValue() - object->longitudinalObstruction.mainLaneLocator) > threshold;
}

units::length::meter_t static ComputeLongitudinalInaccuracyThreshold(units::length::meter_t currentDistance, units::length::meter_t minDistance, units::length::meter_t maxDistance)
{
  const units::length::meter_t MIN_DETECTABLE_INACCURACY = 0.2_m;
  const units::length::meter_t MAX_DETECTABLE_INACCURACY = 10.0_m;
  return ScmMathUtils::LinearInterpolation(currentDistance, minDistance, maxDistance, MIN_DETECTABLE_INACCURACY, MAX_DETECTABLE_INACCURACY);
}

bool static NeedsUpdateLateral(const ObjectInformationSCM* object, const SurroundingVehicleInterface* surroundingVehicleToUpdate, units::length::meter_t threshold)
{
  return units::math::abs(surroundingVehicleToUpdate->GetRelativeLateralPosition().GetValue() - object->obstruction.mainLaneLocator) > threshold;
}

units::length::meter_t static ComputeLateralInaccuracyThreshold(units::length::meter_t vehicleWidth, units::length::meter_t currentDistance, units::length::meter_t minDistance, units::length::meter_t maxDistance)
{
  const units::length::meter_t MIN_DETECTABLE_INACCURACY = 0.1 * vehicleWidth;
  const units::length::meter_t MAX_DETECTABLE_INACCURACY = vehicleWidth;
  return ScmMathUtils::LinearInterpolation(currentDistance, minDistance, maxDistance, MIN_DETECTABLE_INACCURACY, MAX_DETECTABLE_INACCURACY);
}

bool SurroundingVehiclesModifier::ShouldUpdateDueDetectedPeripheralInaccuracy(const ObjectInformationSCM* gtObject, const SurroundingVehicleInterface* surroundingVehicleToUpdate) const
{
  const units::length::meter_t MIN_DISTANCE = 0.0_m;
  const units::length::meter_t MAX_DISTANCE = 100.0_m;
  const auto currentDistance = gtObject->relativeLongitudinalDistance;

  auto longitudinalInaccuracyThreshold = ComputeLongitudinalInaccuracyThreshold(currentDistance, MIN_DISTANCE, MAX_DISTANCE);
  auto lateralInaccuracyThreshold = ComputeLateralInaccuracyThreshold(gtObject->width, currentDistance, MIN_DISTANCE, MAX_DISTANCE);

  auto gazeAngleScalingFactor = units::math::abs(gtObject->opticalAnglePositionHorizontal / (GazeParameters::HORIZONTAL_SIZE_UFOV / 2.0));
  longitudinalInaccuracyThreshold *= gazeAngleScalingFactor;
  lateralInaccuracyThreshold *= gazeAngleScalingFactor;

  bool needsUpdateLongitudinal = NeedsUpdateLongitudinal(gtObject, surroundingVehicleToUpdate, longitudinalInaccuracyThreshold);
  bool needsUpdateLateral = NeedsUpdateLateral(gtObject, surroundingVehicleToUpdate, lateralInaccuracyThreshold);
  return needsUpdateLongitudinal || needsUpdateLateral;
}
