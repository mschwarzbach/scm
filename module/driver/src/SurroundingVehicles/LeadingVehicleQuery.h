/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LeadingVehicleQuery.h

#pragma once

#include "FeatureExtractorInterface.h"
#include "LaneQueryHelper.h"
#include "LeadingVehicleQueryInterface.h"
#include "MentalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "SurroundingVehicleInterface.h"
#include "SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicleQueryInterface.h"
#include "include/common/ScmDefinitions.h"

class SwervingInterface;

//! @brief Implements the LeadingVehicleQueryInterface
class LeadingVehicleQuery : public LeadingVehicleQueryInterface
{
public:
  LeadingVehicleQuery(const MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations, const FeatureExtractorInterface& featureExtractor, const SwervingInterface& swerving)
      : _mentalModel{mentalModel},
        _mentalCalculations{mentalCalculations},
        _featureExtractor{featureExtractor},
        _swerving{swerving}
  {
  }

  std::optional<AreaOfInterest> GetMergeRegulateIfDominating(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;
  std::optional<SurroundingLane> GetEgoLaneChangeDirection() const override;
  bool DoesOvertakingProhibitionApply(const SurroundingVehicleInterface& vehicle) const override;
  std::optional<int> GetSwervingTargetId() const override;
  units::length::meter_t GetEqDistance(const SurroundingVehicleInterface& vehicle) const override;
  units::length::meter_t GetMinDistance(const SurroundingVehicleInterface& vehicle) const override;
  std::optional<int> GetCausingVehicleIdSideSituation() const override;
  std::optional<int> GetCausingVehicleIdFrontSituation() const override;
  bool BelowJamVelocity() const override;
  bool IsSwervingPlanned() const override;

private:
  const MentalModelInterface& _mentalModel;
  const MentalCalculationsInterface& _mentalCalculations;
  const FeatureExtractorInterface& _featureExtractor;
  const SwervingInterface& _swerving;
};