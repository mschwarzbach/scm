/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LeadingVehicleFilter.h
#pragma once

#include "LaneQueryHelper.h"
#include "SurroundingVehicles/LeadingVehicleFilterInterface.h"
#include "SurroundingVehicles/LeadingVehicleQuery.h"
#include "SurroundingVehicles/LeadingVehicleQueryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryInterface.h"

//! @brief Implements the LeadingVehicleFilterInterface
class LeadingVehicleFilter : public LeadingVehicleFilterInterface
{
public:
  LeadingVehicleFilter(const LeadingVehicleQueryInterface& leadingVehicleQuery, const SurroundingVehicleQueryFactoryInterface& vehicleQueryFactory);

  std::vector<const SurroundingVehicleInterface*> FilterRearVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;
  std::vector<const SurroundingVehicleInterface*> FilterSideLaneVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;
  std::vector<const SurroundingVehicleInterface*> FilterSideSideVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;
  std::vector<const SurroundingVehicleInterface*> FilterSwervingTarget(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;
  std::vector<const SurroundingVehicleInterface*> FilterVehiclesWithUnassignedAoi(const std::vector<const SurroundingVehicleInterface*>& vehicles) const override;

private:
  const LeadingVehicleQueryInterface& _leadingVehicleQuery;
  const SurroundingVehicleQueryFactoryInterface& _vehicleQueryFactory;
};
