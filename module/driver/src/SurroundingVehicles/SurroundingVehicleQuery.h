/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file SurroundingVehicleQuery.h

#pragma once

#include "ScmDefinitions.h"
#include "SurroundingVehicleInterface.h"
#include "SurroundingVehicleQueryInterface.h"

//! @brief Implements the SurroundingVehicleQueryInterface
class SurroundingVehicleQuery : public SurroundingVehicleQueryInterface
{
public:
  SurroundingVehicleQuery(const SurroundingVehicleInterface& vehicle, const OwnVehicleInformationScmExtended& egoInfo)
      : _vehicle{vehicle}, _egoInfo{egoInfo}
  {
  }

  ObstructionDynamics GetLateralObstructionDynamics() const override;
  units::velocity::meters_per_second_t GetLateralVelocityDelta() const override;
  units::velocity::meters_per_second_t GetLongitudinalVelocityDelta() const override;
  units::acceleration::meters_per_second_squared_t GetLongitudinalAccelerationDelta() const override;
  bool EgoMovingFasterLateral() const override;
  bool CannotClearLongitudinalObstructionBeforeCollision() const override;
  units::length::meter_t DeltaDistancePerTime(units::time::second_t time) const override;
  bool WillEnterMinDistanceBeforeObstructionIsCleared(units::length::meter_t minDistance) const override;
  bool HasCollisionCourse(units::length::meter_t minDistance) const override;
  bool IsChangingLanesStillOnStartLane() const override;
  bool IsChangingIntoEgoLane() const override;
  bool IsSideSideVehicleChangingIntoSide() const override;

private:
  const SurroundingVehicleInterface& _vehicle;
  const OwnVehicleInformationScmExtended& _egoInfo;
};