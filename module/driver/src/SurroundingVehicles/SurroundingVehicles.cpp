/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "SurroundingVehicles.h"

#include <stdexcept>
#include <string>

#include "GazeControl/GazeParameters.h"
#include "LaneQueryHelper.h"
#include "ScmCommons.h"
#include "SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"

SurroundingVehicles::SurroundingVehicles(MicroscopicCharacteristicsInterface* microscopicCharacteristics)
{
  for (const auto aoi : SURROUNDING_AREAS_OF_INTEREST)
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      const std::vector<ObjectInformationScmExtended>* sideObjects{microscopicCharacteristics->GetSideObjectsInformation(aoi)};
      for (const ObjectInformationScmExtended& object : *sideObjects)
      {
        if (object.exist)
        {
          Add(object, aoi);
        }
      }
    }
    else
    {
      const ObjectInformationScmExtended* objectInAoi{microscopicCharacteristics->GetObjectInformation(aoi)};
      if (objectInAoi->exist)
      {
        Add(*objectInAoi, aoi);
      }
    }
  }
}

std::vector<const SurroundingVehicleInterface*> SurroundingVehicles::Get() const
{
  std::vector<const SurroundingVehicleInterface*> returnVector;
  returnVector.reserve(_surroundingVehicles.size());
  std::transform(begin(_surroundingVehicles), end(_surroundingVehicles), std::back_inserter(returnVector), [](const auto& ptr)
                 { return ptr.get(); });
  return returnVector;
}

void SurroundingVehicles::Add(const ObjectInformationScmExtended& vehicle, AreaOfInterest aoi)
{
  auto newSurroundingVehicle{std::make_unique<SurroundingVehicle>(vehicle, aoi)};
  _surroundingVehicles.push_back(std::move(newSurroundingVehicle));
}
