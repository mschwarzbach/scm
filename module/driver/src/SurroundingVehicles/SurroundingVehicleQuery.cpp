/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SurroundingVehicleQuery.h"

#include <cmath>

#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

ObstructionDynamics SurroundingVehicleQuery::GetLateralObstructionDynamics() const
{
  static constexpr auto LATERAL_VELOCITY_THRESHOLD{0.01_mps};

  const ObstructionScm obstruction{_vehicle.GetLateralObstruction()};
  const auto deltaV{GetLateralVelocityDelta()};
  const auto alreadyInFrontOfEgo{obstruction.isOverlapping};

  if (units::math::abs(deltaV) < LATERAL_VELOCITY_THRESHOLD)  // Avoid division by zero, cap at a minimum of 0.01 m/s
  {
    return {
        .timeToEnter = alreadyInFrontOfEgo ? 0_s : ScmDefinitions::INF_TIME,
        .timeToLeave = alreadyInFrontOfEgo ? ScmDefinitions::INF_TIME : 0_s  // this maybe needs to be changed to 99. since TTC is capped
    };
  }

  if (deltaV < 0_mps)
  {
    return {
        .timeToEnter = alreadyInFrontOfEgo ? 0_s : obstruction.left / deltaV,
        .timeToLeave = -obstruction.right / deltaV};
  }

  return {
      .timeToEnter = alreadyInFrontOfEgo ? 0_s : -obstruction.right / deltaV,
      .timeToLeave = obstruction.left / deltaV};
}

units::velocity::meters_per_second_t SurroundingVehicleQuery::GetLateralVelocityDelta() const
{
  return _egoInfo.lateralVelocity - _vehicle.GetLateralVelocity().GetValue();
}

units::velocity::meters_per_second_t SurroundingVehicleQuery::GetLongitudinalVelocityDelta() const
{
  return _egoInfo.longitudinalVelocity - _vehicle.GetLongitudinalVelocity().GetValue();
}

units::acceleration::meters_per_second_squared_t SurroundingVehicleQuery::GetLongitudinalAccelerationDelta() const
{
  static constexpr auto ACCELERATION_SENSING_DISTANCE{100._m};
  if (units::math::abs(_vehicle.GetRelativeNetDistance().GetValue()) > ACCELERATION_SENSING_DISTANCE)
  {
    return 0._mps_sq;
  }

  return _egoInfo.acceleration - _vehicle.GetLongitudinalAcceleration().GetValue();
}

bool SurroundingVehicleQuery::EgoMovingFasterLateral() const
{
  return units::math::abs(_egoInfo.lateralVelocity) > units::math::abs(_vehicle.GetLateralVelocity().GetValue());
}

bool SurroundingVehicleQuery::CannotClearLongitudinalObstructionBeforeCollision() const
{
  const auto timeToImpact{GetLateralObstructionDynamics().timeToEnter};

  if (units::math::isinf(timeToImpact) || timeToImpact < 0_s)
  {
    return false;
  }

  const auto coveredDistanceDeltaUntilImpact{DeltaDistancePerTime(timeToImpact)};
  const ObstructionLongitudinal obstructionLongitudinal{_vehicle.GetLongitudinalObstruction()};

  return coveredDistanceDeltaUntilImpact < obstructionLongitudinal.front;
}

units::length::meter_t SurroundingVehicleQuery::DeltaDistancePerTime(units::time::second_t time) const
{
  const auto velocityDelta{GetLongitudinalVelocityDelta()};
  const auto accelerationDelta{GetLongitudinalAccelerationDelta()};

  const auto accelerationTime(units::math::min(time, 1._s));  // Cap lookahead time of acceleration to a maximum of 1 second
  return 0.5 * accelerationDelta * units::math::pow<2>(accelerationTime) + velocityDelta * time;
}

bool SurroundingVehicleQuery::WillEnterMinDistanceBeforeObstructionIsCleared(units::length::meter_t minDistance) const
{
  const auto timeToLeave{GetLateralObstructionDynamics().timeToLeave};
  const auto coveredDistanceUntilClearance{DeltaDistancePerTime(timeToLeave)};
  const auto deltaToMinDistance{_vehicle.RearDistance().GetValue() - minDistance};

  return _vehicle.FrontDistance().GetValue() > 0_m && coveredDistanceUntilClearance > deltaToMinDistance;
}

namespace{
  bool TtcCritical(const SurroundingVehicleInterface& vehicle)
  {
  constexpr auto CRITICALITY_THRESHOLD{4_s};

  const auto ttc{vehicle.GetTtc().GetValue()};
  const auto tauDot{vehicle.GetTauDot()};

  return ttc >= 0.0_s &&
         ttc <= CRITICALITY_THRESHOLD &&
         tauDot <= ScmDefinitions::TAU_DOT_LIMIT;
  }

  bool SideTtcCritical(units::time::second_t sideTtc)
  {
    constexpr auto CRITICALITY_THRESHOLD{5_s};

    return sideTtc >= 0.0_s &&
           sideTtc <= CRITICALITY_THRESHOLD;
  }
} // namespace


bool SurroundingVehicleQuery::HasCollisionCourse(units::length::meter_t minDistance) const
{
  if (_vehicle.BehindEgo())
  {
    return false;
  }

  if (_vehicle.ToTheSideOfEgo())
  {
    const auto obstructionDynamics{GetLateralObstructionDynamics()};
    const auto sideTtc{obstructionDynamics.timeToEnter};
    return SideTtcCritical(sideTtc) && CannotClearLongitudinalObstructionBeforeCollision();
  }

  if (!TtcCritical(_vehicle))
  {
    return false;
  }

  if (!_vehicle.LateralObstructionOverlapping())  // agent is not yet in front of ego
  {
    return CannotClearLongitudinalObstructionBeforeCollision();
  }

  // agent in front of ego, can ego clear lateral obstruction before collision
  return WillEnterMinDistanceBeforeObstructionIsCleared(minDistance);
}

bool SurroundingVehicleQuery::IsChangingLanesStillOnStartLane() const
{
  return _vehicle.IsExceedingLateralMotionThreshold() &&
         (_vehicle.GetLateralVelocity().GetValue() > 0_mps && _vehicle.GetLateralPositionInLane().GetValue() > 0_m ||
          _vehicle.GetLateralVelocity().GetValue() < 0_mps && _vehicle.GetLateralPositionInLane().GetValue() < 0_m);
}

bool SurroundingVehicleQuery::IsChangingIntoEgoLane() const
{
  return IsChangingLanesStillOnStartLane() &&
         ((_vehicle.GetLaneOfPerception() == RelativeLane::LEFT && _vehicle.GetLateralVelocity().GetValue() < 0_mps) ||
          (_vehicle.GetLaneOfPerception() == RelativeLane::RIGHT && _vehicle.GetLateralVelocity().GetValue() > 0_mps));
}

bool SurroundingVehicleQuery::IsSideSideVehicleChangingIntoSide() const
{
  return IsChangingLanesStillOnStartLane() &&
         ((_vehicle.GetLaneOfPerception() == RelativeLane::LEFTLEFT && _vehicle.GetLateralVelocity().GetValue() < 0_mps) ||
          (_vehicle.GetLaneOfPerception() == RelativeLane::RIGHTRIGHT && _vehicle.GetLateralVelocity().GetValue() > 0_mps));
}