/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LeadingVehicleQueryInterface.h

#pragma once

#include "SurroundingVehicleInterface.h"
#include "SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicleQueryInterface.h"
#include "include/common/ScmDefinitions.h"

//! ********************************************************************************************
//! @brief Interface class which allows certain queries regarding the leading vehicle selection.
//! ********************************************************************************************
class LeadingVehicleQueryInterface
{
public:
  virtual ~LeadingVehicleQueryInterface() = default;
  //! @brief Gets the current merge regulate if it is currently set and most important for the situation.
  //! @param vehicles surrounding vehicles
  //! @return the current merge regulate AreaOfInterest if a merge regulate is set and the agent in front is far enough away to concentrate on merging only.
  virtual std::optional<AreaOfInterest> GetMergeRegulateIfDominating(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;

  //! @brief Get the direction of the lane change in form of a SurroundingLane
  //! @return the target lane of the current lane change while agent is still on the starting lane, std::nullopt otherwise.
  virtual std::optional<SurroundingLane> GetEgoLaneChangeDirection() const = 0;

  //! @brief Checks if it's allowed to overtake a vehicle
  //! @param vehicle for which the check is applied
  //! @return true if overtaking on the right is currently prohibited
  virtual bool DoesOvertakingProhibitionApply(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Returns the vehicle id of the current swerving target or std::nullopt if the ego agent is not in a relevant swerving state.
  virtual std::optional<int> GetSwervingTargetId() const = 0;

  //! @brief Returns the equilibrium following distance for the given vehicle.
  virtual units::length::meter_t GetEqDistance(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Wrapper for the calculation of the minimum distance from MentalCalculations::GetMinDistance
  //! @param vehicle surrounding vehicle
  //! @return the minimum following distance for the given vehicle.
  virtual units::length::meter_t GetMinDistance(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Getter for the vehicle id which is causing a 'side situation'
  //! @return the vehicle id of the causing vehicle when in a side situation or std::nullopt otherwise.
  virtual std::optional<int> GetCausingVehicleIdSideSituation() const = 0;

  //! @brief Getter for the vehicle id which is causing a 'front situation'
  //! @return the vehicle id of the causing vehicle when in a front situation or std::nullopt otherwise.
  virtual std::optional<int> GetCausingVehicleIdFrontSituation() const = 0;

  //! @brief Indicates if the agent is currently slower than the traffic jam velocity threshold.
  //! @return true if the ego vehicle is driving below jam speed
  virtual bool BelowJamVelocity() const = 0;

  //! @brief Indicates a planned swerving maneuver.
  //! @return true if a swerving maneuver is currently planned
  virtual bool IsSwervingPlanned() const = 0;
};