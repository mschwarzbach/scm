/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  SurroundingVehicles.h

#pragma once
#include <memory>
#include <vector>

#include "InformationAcquisition.h"
#include "InformationAcquisitionInterface.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "MentalModelInterface.h"
#include "MicroscopicCharacteristics.h"
#include "SurroundingVehicle.h"
#include "include/common/ScmDefinitions.h"

//! ****************************************************
//! @brief Container class for the surrounding vehicles.
//! ****************************************************
class SurroundingVehicles
{
public:
  SurroundingVehicles() = default;
  virtual ~SurroundingVehicles() = default;

  SurroundingVehicles(const SurroundingVehicles&) = default;

  //! @brief Construct a new SurroundingVehicles object and fill in all vehicles present in the given MicroscopicCharacteristics.
  //! @param microscopicCharacteristics
  explicit SurroundingVehicles(MicroscopicCharacteristicsInterface* microscopicCharacteristics);

  //! @brief Gets all SurroundingVehicles
  //! @return a copy to the vector containing all SurroundingVehicles
  std::vector<const SurroundingVehicleInterface*> Get() const;

private:
  friend class TestSurroundingVehicles;
  friend class SurroundingVehiclesModifier;

  //! @brief Adds a new unique pointer to a SurroundingVehicle to the vector.
  //! @param vehicle Information of the vehicle that should be added
  //! @param aoi AreaOfInterest that should be used as the perceptionAOI for the vehicle
  void Add(const ObjectInformationScmExtended& vehicle, AreaOfInterest aoi);

  std::vector<std::unique_ptr<SurroundingVehicleInterface>> _surroundingVehicles;
};
