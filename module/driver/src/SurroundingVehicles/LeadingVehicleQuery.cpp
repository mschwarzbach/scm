/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LeadingVehicleQuery.h"

#include <algorithm>
#include <optional>

#include "LaneQueryHelper.h"
#include "Swerving/SwervingInterface.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

std::optional<AreaOfInterest> LeadingVehicleQuery::GetMergeRegulateIfDominating(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  const auto mergeRegulateAoi{_mentalModel.GetMergeRegulate()};
  if (mergeRegulateAoi == AreaOfInterest::NumberOfAreaOfInterests ||
      mergeRegulateAoi == AreaOfInterest::EGO_REAR)
  {
    return std::nullopt;
  }

  // Only use merge regulate if ego front is far enough away
  const auto egoFrontVehicle{std::find_if(begin(vehicles), end(vehicles), [](const auto* vehicle)
                                          { return vehicle->GetAssignedAoi() == AreaOfInterest::EGO_FRONT; })};

  if (egoFrontVehicle == end(vehicles) ||
      (*egoFrontVehicle)->GetRelativeLongitudinalPosition().GetValue() > _mentalCalculations.GetMinDistance(AreaOfInterest::EGO_FRONT) * (1 - _mentalCalculations.GetUrgencyFactorForLaneChange()))
  {
    return mergeRegulateAoi;
  }

  return std::nullopt;
}

std::optional<SurroundingLane> LeadingVehicleQuery::GetEgoLaneChangeDirection() const
{
  const auto laneChangeDirection{_mentalModel.GetCurrentDirection()};
  const auto lateralActionQuery{LateralActionQuery(_mentalModel.GetLateralAction())};
  if (laneChangeDirection == SurroundingLane::EGO || !lateralActionQuery.IsLeavingEgoLane() || _mentalModel.GetIsLaneChangePastTransition())
  {
    return std::nullopt;
  }

  return laneChangeDirection;
}

bool LeadingVehicleQuery::DoesOvertakingProhibitionApply(const SurroundingVehicleInterface& vehicle) const
{
  const auto rightHandTraffic = _mentalModel.IsRightHandTraffic();
  const AreaOfInterest aoiSideFront = rightHandTraffic ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;

  const bool vehicleInRelevantAoi {vehicle.GetAssignedAoi() == aoiSideFront};
  const bool doesProhibitionApply {_featureExtractor.DoesOuterLaneOvertakingProhibitionApply(aoiSideFront, rightHandTraffic)};
  const bool targetVelocityAboveVehicleVelocity{_mentalModel.GetAbsoluteVelocityTargeted() >= vehicle.GetLongitudinalVelocity().GetValue()};

  return vehicleInRelevantAoi && doesProhibitionApply && targetVelocityAboveVehicleVelocity;
}

std::optional<int> LeadingVehicleQuery::GetSwervingTargetId() const
{
  if (_swerving.GetSwervingState() == SwervingState::NONE ||
      _swerving.GetSwervingState() == SwervingState::RETURNING)
  {
    return std::nullopt;
  }

  return _swerving.GetAgentIdOfSwervingTarget();
}

units::length::meter_t LeadingVehicleQuery::GetEqDistance(const SurroundingVehicleInterface& vehicle) const
{
  return _mentalCalculations.GetEqDistance(vehicle.GetAssignedAoi());
}

units::length::meter_t LeadingVehicleQuery::GetMinDistance(const SurroundingVehicleInterface& vehicle) const
{
  return _mentalCalculations.GetMinDistance(vehicle.GetAssignedAoi());
}

bool IsSideSituation(const MentalModelInterface& mentalModel)
{
  const auto situation = mentalModel.GetCurrentSituation();
  return situation == Situation::LANE_CHANGER_FROM_RIGHT ||
         situation == Situation::LANE_CHANGER_FROM_LEFT ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE ||
         situation == Situation::SIDE_COLLISION_RISK_FROM_RIGHT ||
         situation == Situation::SIDE_COLLISION_RISK_FROM_LEFT;
}

std::optional<int> LeadingVehicleQuery::GetCausingVehicleIdSideSituation() const
{
  const auto currentSituation{_mentalModel.GetCurrentSituation()};
  if (!IsSideSituation(_mentalModel))
  {
    return std::nullopt;
  }

  const auto causingAoi{_mentalModel.GetCausingVehicleOfSituationSideCluster()};
  if (LaneQueryHelper::IsSideArea(causingAoi))
  {
    return _mentalModel.GetAgentId(causingAoi, 0);
  }

  return _mentalModel.GetAgentId(causingAoi);
}

std::optional<int> LeadingVehicleQuery::GetCausingVehicleIdFrontSituation() const
{
  if (!_mentalModel.IsFrontSituation())
  {
    return std::nullopt;
  }

  const auto causingAoi{_mentalModel.GetCausingVehicleOfSituationFrontCluster()};
  return _mentalModel.GetAgentId(causingAoi);
}

bool LeadingVehicleQuery::BelowJamVelocity() const
{
  return _featureExtractor.HasJamVelocityEgo();
}

bool LeadingVehicleQuery::IsSwervingPlanned() const
{
  return _swerving.IsSwervingPlanned();
}