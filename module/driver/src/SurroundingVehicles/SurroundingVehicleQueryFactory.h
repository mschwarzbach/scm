/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "ScmDefinitions.h"
#include "SurroundingVehicleQuery.h"
#include "SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicleQueryInterface.h"
#include "include/common/SensorDriverScmDefinitions.h"

class SurroundingVehicleQueryFactory : public SurroundingVehicleQueryFactoryInterface
{
public:
  SurroundingVehicleQueryFactory(const OwnVehicleInformationScmExtended& ownVehicleInformation)
      : _ownVehicleInformation{ownVehicleInformation}
  {
  }

  std::shared_ptr<SurroundingVehicleQueryInterface> GetQuery(const SurroundingVehicleInterface& vehicle) const override
  {
    return std::make_shared<SurroundingVehicleQuery>(vehicle, _ownVehicleInformation);
  }

private:
  const OwnVehicleInformationScmExtended& _ownVehicleInformation;
};
