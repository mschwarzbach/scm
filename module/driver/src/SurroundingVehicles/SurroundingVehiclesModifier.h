/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  SurroundingVehiclesModifier.h

#pragma once
#include <memory>
#include <vector>

#include "AoiAssignerInterface.h"
#include "ExtrapolationInterface.h"
#include "InformationAcquisition.h"
#include "InfrastructureCharacteristics.h"
#include "MentalModelInterface.h"
#include "MicroscopicCharacteristics.h"
#include "SurroundingVehicle.h"
#include "SurroundingVehicleInterface.h"
#include "SurroundingVehicles.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

class SurroundingVehiclesModifier
{
public:
  explicit SurroundingVehiclesModifier(SurroundingVehicles& data);
  virtual ~SurroundingVehiclesModifier() = default;

  //! @brief Checks if agent have transitioned to or from the side area and resets or calculates data not present in side AOIs.
  //! @param aoiVehicleMapping Mapping between AreasOfInterest and the Vehicles that are contained
  //! @param infoAcquisition Pointer to InformationAcquisition that contains this instance
  static void HandleSideAreaTransitions(AoiVehicleMapping* aoiVehicleMapping, const InformationAcquisitionInterface* infoAcquisition);

  //! @brief Writes the contained vehicle information back into the MicroscopicCharacteristics struct.
  //! @details Empty AOIs are reset using the ResetAoiDataForNoObject method from InformatinAcquisition.
  //! @param aoiVehicleMapping Mapping between AreasOfInterest and the Vehicles that are contained
  //! @param microscopicData Pointer to the MicroscopicCharacterisitcs instance to write back to
  //! @param infoAcquisition Pointer to InformationAcquisition that contains this instance
  static void WriteToMicroscopicData(const AoiVehicleMapping& aoiVehicleMapping, MicroscopicCharacteristicsInterface* microscopicData, const InformationAcquisitionInterface* infoAcquisition);
  static void WriteToMicroscopicDataVector(const AoiVehicleMapping& aoiMapping, MicroscopicCharacteristicsInterface* microscopicCharacteristics);

  //! @brief Removes agents that appear in the field of view but are not visible in GT data
  //! @param visibleAgentIdsGt    Agent Ids of all agents that are currently visible in the ground truth data
  //! @param mentalModel          Pointer to the mentalModel  TODO change to Gaze Cone
  //! @return Aois where vehicles have been removed
  std::vector<AreaOfInterest> RemoveVisibleVehiclesNotPresentInGroundTruth(const std::vector<int>& visibleAgentIdsGt, const MentalModelInterface* mentalModel);

  //! @brief Updates the positions of all SurroundingVehicles using either GroundTruth information or by extrapolation and fills periphery vector.
  //! @param infoAcquisition  Pointer to InformationAcquisition that contains this agent
  //! @param extrapolation Reference to the Extrapolation of infoAcquisition
  //! @param perceiveVehicles Flag that controls if an update with ground truth data should be done
  //! @param idealPerception Flag to let InformationAcquisition treat all vehicles as visible
  void UpdateVehicles(const InformationAcquisitionInterface* infoAcquisition, const ExtrapolationInterface& extrapolation, bool perceiveVehicles, bool idealPerception = false);

  void UpdatePerceivedVehicle(AreaOfInterest aoi, const ObjectInformationSCM* objectToUpdate, const InformationAcquisitionInterface* infoAcquisition);

  std::vector<SurroundingVehicleInterface*> Update();

  std::vector<AreaOfInterest> DetectInconsistenciesFromExtrapolation(const AoiVehicleMapping& aoiVehicleMapping, const InfrastructureCharacteristicsInterface& infrastructureData) const;

  void Clear();

  void AssignAreasOfInterest(const AoiVehicleMapping& aoiVehicleMapping);
  void ForgetLongUnseenVehicles(units::time::millisecond_t currentTimestep);
  void AssignRelativeLane(const LaneVehicleMapping& laneMapping);
  bool ShouldUpdateDueDetectedPeripheralInaccuracy(const ObjectInformationSCM* gtObject, const SurroundingVehicleInterface* surroundingVehicleToUpdate) const;

protected:
  bool CheckForOverlaps(const SurroundingVehicleInterface* vehicle) const;

  //! @brief Handles updates with ground truth information.
  //! Either adds a new vehicle or updates an existing vehicle if the ID of the object is already known.
  //! @param gtVehicles       Vector of vehicles visible in UFOV
  //! @param infoAcquisition  Pointer to InformationAcquisition that contains this instance
  //! @param idealPerception  bool Value of ideal Perception
  void UpdateVisibleVehicles(const std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>>& gtVehicles, const InformationAcquisitionInterface* infoAcquisition, bool idealPerception);

private:
  SurroundingVehicles& _data;
  //! @brief Remove vehicles that have been extrapolated out of sight
  void RemoveOutOfSightVehicles(const InformationAcquisitionInterface* infoAcquisition);
  void ResetAllAois();
};
