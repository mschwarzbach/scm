/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  SurroundingVehicles.h

#pragma once
#include <vector>

#include "ExtrapolationInterface.h"
#include "InformationAcquisition.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicleInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"

//! @brief Implements the SurroundingVehicleInterface
class SurroundingVehicle : public SurroundingVehicleInterface
{
public:
  // Setters and manipulation functions
  explicit SurroundingVehicle(const ObjectInformationScmExtended& objectData, AreaOfInterest perceptionAoi = AreaOfInterest::NumberOfAreaOfInterests);

  //! @copydoc SurroundingVehicleInterface::Extrapolate
  void Extrapolate(const ExtrapolationInterface& extrapolation) override;

  //! @copydoc SurroundingVehicleInterface::UpdateWithGroundTruth
  void UpdateWithGroundTruth(const ObjectInformationSCM* gtObject, AreaOfInterest aoi, const InformationAcquisitionInterface* infoAcquisition) override;

  //! @copydoc SurroundingVehicleInterface::HandleTransitionFromSideArea
  void HandleTransitionFromSideArea(AreaOfInterest newAoi, const InformationAcquisitionInterface* infoAcquisition) override;

  //! @copydoc SurroundingVehicleInterface::HandleTransitionToSideArea
  void HandleTransitionToSideArea(const InformationAcquisitionInterface* infoAcquisition) override;

  //! @copydoc SurroundingVehicleInterface::SetTimeOfLastPerception
  void SetTimeOfLastPerception(units::time::millisecond_t time) override;

  //! @copydoc SurroundingVehicleInterface::AssignAoi
  void AssignAoi(AreaOfInterest aoi) override;

  //! @copydoc SurroundingVehicleInterface::AssignLane
  void AssignLane(RelativeLane lane) override;

  //! @copydoc SurroundingVehicleInterface::ResetWasPerceivedFlag
  void ResetWasPerceivedFlag() override;

  //! @copydoc SurroundingVehicleInterface::SetReliability
  void SetReliability(double reliability) override;

  //! @copydoc SurroundingVehicleInterface::DecayReliability
  void DecayReliability(units::time::millisecond_t cycleTime) override;

  // Getters of algorithm interal constants and flags

  //! @copydoc SurroundingVehicleInterface::GetObjectInformation
  const ObjectInformationScmExtended* GetObjectInformation() const override;

  //! @copydoc SurroundingVehicleInterface::GetId
  int GetId() const override;

  //! @copydoc SurroundingVehicleInterface::WasPerceived
  bool WasPerceived() const override;

  //! @copydoc SurroundingVehicleInterface::GetTimeOfLastPerception
  units::time::millisecond_t GetTimeOfLastPerception() const override;

  //! @copydoc SurroundingVehicleInterface::GetAngleToOpticalAxis
  units::angle::radian_t GetAngleToOpticalAxis() const override;

  //! @copydoc SurroundingVehicleInterface::GetTtcThresholdLooming
  double GetTtcThresholdLooming() const override;

  //! @copydoc SurroundingVehicleInterface::IsReliable
  bool IsReliable(double requiredQuality, ParameterChangeRate changingRate = ParameterChangeRate::FAST) const override;

  //! @copydoc SurroundingVehicleInterface::GetReliability
  double GetReliability(ParameterChangeRate changingRate = ParameterChangeRate::FAST) const override;

  // Vehicle parameters

  //! @copydoc SurroundingVehicleInterface::GetRelativeLongitudinalPosition
  VehicleParameter<units::length::meter_t> GetRelativeLongitudinalPosition() const override;

  //! @copydoc SurroundingVehicleInterface::GetRelativeLateralPosition
  VehicleParameter<units::length::meter_t> GetRelativeLateralPosition() const override;

  //! @copydoc SurroundingVehicleInterface::FrontDistance
  VehicleParameter<units::length::meter_t> FrontDistance() const override;

  //! @copydoc SurroundingVehicleInterface::RearDistance
  VehicleParameter<units::length::meter_t> RearDistance() const override;

  //! @copydoc SurroundingVehicleInterface::LeftDistance
  VehicleParameter<units::length::meter_t> LeftDistance() const override;

  //! @copydoc SurroundingVehicleInterface::RightDistance
  VehicleParameter<units::length::meter_t> RightDistance() const override;

  //! @copydoc SurroundingVehicleInterface::Overlaps
  VehicleParameter<bool> Overlaps(const SurroundingVehicleInterface& other) const override;

  //! @copydoc SurroundingVehicleInterface::ToTheSideOfEgo
  VehicleParameter<bool> ToTheSideOfEgo() const override;

  //! @copydoc SurroundingVehicleInterface::InFrontOfEgo
  VehicleParameter<bool> InFrontOfEgo() const override;

  //! @copydoc SurroundingVehicleInterface::BehindEgo
  VehicleParameter<bool> BehindEgo() const override;

  //! @copydoc SurroundingVehicleInterface::LateralObstructionOverlapping
  VehicleParameter<bool> LateralObstructionOverlapping() const override;

  //! @copydoc SurroundingVehicleInterface::GetLaneOfPerception
  VehicleParameter<RelativeLane> GetLaneOfPerception() const override;

  //! @copydoc SurroundingVehicleInterface::GetAssignedAoi
  VehicleParameter<AreaOfInterest> GetAssignedAoi() const override;

  //! @copydoc SurroundingVehicleInterface::GetBoundingBox
  VehicleParameter<Scm::BoundingBox> GetBoundingBox() const override;

  //! @copydoc SurroundingVehicleInterface::IsCollided
  VehicleParameter<bool> IsCollided() const override;

  //! @copydoc SurroundingVehicleInterface::GetTtc
  VehicleParameter<units::time::second_t> GetTtc() const override;

  //! @copydoc SurroundingVehicleInterface::GetThw
  VehicleParameter<units::time::second_t> GetThw() const override;

  //! @copydoc SurroundingVehicleInterface::GetLateralObstruction
  VehicleParameter<ObstructionScm> GetLateralObstruction() const override;

  //! @copydoc SurroundingVehicleInterface::GetLongitudinalObstruction
  VehicleParameter<ObstructionLongitudinal> GetLongitudinalObstruction() const override;

  //! @copydoc SurroundingVehicleInterface::GetLateralVelocity
  VehicleParameter<units::velocity::meters_per_second_t> GetLateralVelocity() const override;

  //! @copydoc SurroundingVehicleInterface::GetLongitudinalVelocity
  VehicleParameter<units::velocity::meters_per_second_t> GetLongitudinalVelocity() const override;

  //! @copydoc SurroundingVehicleInterface::GetLongitudinalAcceleration
  VehicleParameter<units::acceleration::meters_per_second_squared_t> GetLongitudinalAcceleration() const override;

  //! @copydoc SurroundingVehicleInterface::GetAbsoluteVelocity
  VehicleParameter<units::velocity::meters_per_second_t> GetAbsoluteVelocity() const override;

  //! @copydoc SurroundingVehicleInterface::GetTauDot
  VehicleParameter<double> GetTauDot() const override;

  //! @copydoc SurroundingVehicleInterface::IsExceedingLateralMotionThreshold
  VehicleParameter<bool> IsExceedingLateralMotionThreshold() const override;

  //! @copydoc SurroundingVehicleInterface::GetLength
  VehicleParameter<units::length::meter_t> GetLength() const override;

  //! @copydoc SurroundingVehicleInterface::GetWidth
  VehicleParameter<units::length::meter_t> GetWidth() const override;

  //! @copydoc SurroundingVehicleInterface::GetHeight
  VehicleParameter<units::length::meter_t> GetHeight() const override;

  //! @copydoc SurroundingVehicleInterface::GetLateralPositionInLane
  VehicleParameter<units::length::meter_t> GetLateralPositionInLane() const override;

  //! @copydoc SurroundingVehicleInterface::IsStatic
  VehicleParameter<bool> IsStatic() const override;

  //! @copydoc SurroundingVehicleInterface::GetRelativeNetDistance
  VehicleParameter<units::length::meter_t> GetRelativeNetDistance() const override;

  //! @copydoc SurroundingVehicleInterface::GetDistanceToLaneBoundary
  VehicleParameter<units::length::meter_t> GetDistanceToLaneBoundary(Side side) const override;

  //! @copydoc SurroundingVehicleInterface::GetVehicleClassification
  VehicleParameter<scm::common::VehicleClass> GetVehicleClassification() const override;

  //! @copydoc SurroundingVehicleInterface::GetProjectedDimensions
  VehicleParameter<ProjectedSpacialDimensions> GetProjectedDimensions() const override;

  //! @copydoc SurroundingVehicleInterface::GetIndicatorState
  VehicleParameter<scm::LightState::Indicator> GetIndicatorState() const override;

private:
  bool _groundTruthData{false};
  AreaOfInterest _assignedAoi{AreaOfInterest::NumberOfAreaOfInterests};
  RelativeLane _assignedLane;
  std::map<ParameterChangeRate, double> _reliabilities{{ParameterChangeRate::FAST, 0}, {ParameterChangeRate::MEDIUM, 0}, {ParameterChangeRate::SLOW, 0}, {ParameterChangeRate::CONSTANT, 100}};
  units::time::millisecond_t _timeOfLastPerception;
  ObjectInformationScmExtended _objectData;
};
