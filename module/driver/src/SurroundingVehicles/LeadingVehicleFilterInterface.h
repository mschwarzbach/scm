/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LeadingVehicleFilterInterface.h

#pragma once

#include "SurroundingVehicleInterface.h"

//! ******************************************************************************
//! @brief Interface class to filter all vehicles unsuitable as a leading vehicle.
//! ******************************************************************************
class LeadingVehicleFilterInterface
{
public:
  virtual ~LeadingVehicleFilterInterface() = default;

  //! @brief Filters all vehicles that are behind the ego vehicle.
  //! @param vehicles all currently still unfiltered surrounding vehicles
  //! @return vehicles vehicles excepts those behind ego
  virtual std::vector<const SurroundingVehicleInterface*> FilterRearVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;

  //! @brief Keeps vehicles that are causing vehicles of the current side situation, vehicles approaching from the side, vehicles on the target lane if ego is currently changing lanes and the left front vehicle if right overtaking is prohibited.
  //! @param vehicles all currently still unfiltered surrounding vehicles
  //! @return vehicles on the side lane which fulfill the description above
  virtual std::vector<const SurroundingVehicleInterface*> FilterSideLaneVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;

  //! @brief Filters all vehicles on side side lanes except the ones changing into the side lane of ego.
  //! @param vehicles all currently still unfiltered surrounding vehicles
  //! @return vehicles without those on side side lanes except when they're already changing into a side lane
  virtual std::vector<const SurroundingVehicleInterface*> FilterSideSideVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;

  //! @brief Removes the current swerving target if swerving is planned or already in progress.
  //! @param vehicles all currently still unfiltered surrounding vehicles
  //! @return vehicles without the potential swerving target
  virtual std::vector<const SurroundingVehicleInterface*> FilterSwervingTarget(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;

  //! @brief Removes vehicles with a assigned AOI of NumberOfAreasOfInterest.
  //! @param vehicles all currently still unfiltered surrounding vehicles
  //! @return vehicles without those without a valid assigned aoi
  // TODO Remove once LongitudinalAction selection only uses the leading vehicle directly.
  virtual std::vector<const SurroundingVehicleInterface*> FilterVehiclesWithUnassignedAoi(const std::vector<const SurroundingVehicleInterface*>& vehicles) const = 0;
};
