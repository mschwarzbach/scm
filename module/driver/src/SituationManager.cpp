/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SituationManager.h"

#include <Stochastics/StochasticsInterface.h>

#include <algorithm>
#include <numeric>

#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "Swerving/Swerving.h"
#include "include/ScmSampler.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

SituationManager::SituationManager(units::time::millisecond_t cycleTime, StochasticsInterface* stochastics, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving)
    : _cycleTime{cycleTime},
      _stochastics{stochastics},
      _mentalModel{mentalModel},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations},
      _swerving{swerving}
{
  _situationIntensities[Situation::FREE_DRIVING] = 1.;
}

void SituationManager::SetIgnoringOuterLaneOvertakingProhibition(IgnoringOuterLaneOvertakingProhibitionInterface* ignoringOuterLaneOvertakingProhibition)
{
  _outerLaneOvertakingProhibitionQuota = ignoringOuterLaneOvertakingProhibition;
}

int SituationManager::GetRiskClass(double intensityFactoredWithRisk)
{
  if (intensityFactoredWithRisk <= static_cast<int>(Risk::LOW))
  {
    return 1;
  }
  else if (intensityFactoredWithRisk <= static_cast<int>(Risk::MEDIUM))
  {
    return 2;
  }
  else if (intensityFactoredWithRisk <= static_cast<int>(Risk::HIGH))
  {
    return 3;
  }
  else
  {
    return 1;
  }
}

int SituationManager::GetRiskValue(Risk risk) const
{
  return static_cast<int>(risk);
}

bool SituationManager::HigherRiskClassIgnored(Situation currentSituation, std::map<Situation, double> observedIntensities)
{
  const int currentRiskClass = GetRiskClass(observedIntensities[currentSituation]);

  for (std::map<Situation, double>::iterator it = observedIntensities.begin(); it != observedIntensities.end(); it++)
  {
    const int riskClass = GetRiskClass(it->second);
    if (currentRiskClass < riskClass)
    {
      return true;
    }
  }

  return false;
}

void SituationManager::GenerateIntensitiesAndSampleSituation(ZipMergingInterface& zipMerging)
{
  const auto mesoscopicSituationFeatures{GenerateSituationIntensityVector()};
  DetectMesoscopicSituations(mesoscopicSituationFeatures, zipMerging);

  Situation newSituation = _situationLastTick;

  std::map<Situation, double> intensities = MostIntensiveSituations(_situationIntensities, 4);

  // obtain new situations
  if (HaveIntensitiesChangedSignificantly(intensities, _lastSituationIntensities) || _lastIgnoreRiskState)
  {
    newSituation = ObtainNewSituation(intensities);
    SetLastIgnoreRiskState(newSituation, intensities);
  }

  if (newSituation != _situationLastTick)
  {
    _durationCurrentSituation = 0_ms;
    _mentalModel.SetDurationCurrentSituation(0_ms);
    _mentalModel.CheckSituationCooperativeBehavior();
  }

  _mentalModel.SetCurrentSituation(newSituation);
  _outerLaneOvertakingProhibitionQuota->Update();
  _swerving.CheckForPlannedEvading();
}

Situation SituationManager::ObtainNewSituation(std::map<Situation, double> intensities)
{
  _lastSituationIntensities = intensities;
  return ScmSampler::Sample(intensities, Situation::FREE_DRIVING, _stochastics->GetUniformDistributed(0, 1));
}

void SituationManager::SetLastIgnoreRiskState(Situation newSituation, std::map<Situation, double> intensities)
{
  if (HigherRiskClassIgnored(newSituation, intensities))
    _lastIgnoreRiskState = true;
  else
    _lastIgnoreRiskState = false;
}

std::map<Situation, double> SituationManager::MostIntensiveSituations(std::map<Situation, double> intensities, int maxLength)
{
  std::map<Situation, double> mostIntensiveSituations;
  for (int i = 0; i < maxLength; ++i)
  {
    auto maxElement = std::max_element(intensities.begin(),
                                       intensities.end(),
                                       [](const std::pair<Situation, double>& lhs, const std::pair<Situation, double>& rhs)
                                       { return lhs.second < rhs.second; });
    static constexpr double NOISE_FILTER_THRESHOLD = 0.0;
    if (maxElement->second > NOISE_FILTER_THRESHOLD)
    {
      mostIntensiveSituations.emplace(std::pair<Situation, double>(maxElement->first, maxElement->second));
    }
    intensities.erase(maxElement->first);
  }
  return mostIntensiveSituations;
}

bool SituationManager::HaveIntensitiesChangedSignificantly(std::map<Situation, double> intensities,
                                                           std::map<Situation, double> intensitiesLast) const
{
  if (intensities.empty() || intensitiesLast.empty())
  {
    return true;
  }

  for (int it = 0; it < static_cast<int>(Situation::NumberOfSituations); ++it)
  {
    const Situation situation = static_cast<Situation>(it);
    const bool existsInIntensities = intensities.find(situation) != intensities.end();
    const bool existsInIntensitiesLast = intensitiesLast.find(situation) != intensitiesLast.end();

    if ((existsInIntensitiesLast && intensitiesLast.at(situation) > 0.) &&
        (!existsInIntensities || existsInIntensities && intensities.at(situation) == 0.))
    {
      return true;  // Entry was either removed since last check or was set to zero (0 is an edge case so it will always trigger).
    }

    const bool hasChanged = IsSignificantIntensityChange(
        existsInIntensities ? intensities.at(situation) : 0.,
        existsInIntensitiesLast ? intensitiesLast.at(situation) : 0.);

    if (hasChanged)
    {
      return true;
    }
  }

  return false;
}

bool SituationManager::IsSignificantIntensityChange(double intensity, double intensityLast) const
{
  const double changeInValue = std::abs(intensity - intensityLast);
  const double changeInPercentage = intensityLast != 0. ? std::abs(changeInValue / intensityLast) : 0.;  // Note: No previous intensity value will ignore percentage condition

  const double minimumDeltaThreshold = .4;   // Change in intensity must be at least +-.4
  const double minimumDeltaPercentage = .2;  // Change in intensity must be at least +-20%

  if (intensity < 1.)  // Small values below 1 will only use threshold criteria
  {
    return changeInValue > minimumDeltaThreshold;
  }

  return changeInValue > minimumDeltaThreshold || changeInPercentage > minimumDeltaPercentage;
}

std::map<AreaOfInterest, bool> SituationManager::GetClusterVehiclesSideLane(std::vector<AreaOfInterest> areas)
{
  // vehicles should be present and in the side lane
  std::map<AreaOfInterest, bool> clusterVehicles = {};

  for (AreaOfInterest aoi : areas)
  {
    if (AreaOfInterest::RIGHT_FRONT == aoi)
    {
      auto vehicles = _mentalModel.GetVirtualAgents();
      if (!vehicles.empty())
      {
        std::vector<units::length::meter_t> distanceToVirtualAgents;
        for (const auto& virtualVehicle : vehicles)
        {
          distanceToVirtualAgents.push_back(virtualVehicle.relativeLongitudinalDistance);
        }
        std::vector<units::length::meter_t>::iterator result = std::min_element(distanceToVirtualAgents.begin(), distanceToVirtualAgents.end());
        int minDistance = std::distance(distanceToVirtualAgents.begin(), result);

        if (vehicles[minDistance].lanetype == scm::LaneType::Entry ||
            vehicles[minDistance].lanetype == scm::LaneType::OnRamp)
        {
          clusterVehicles.insert(std::make_pair(aoi, true));
        }
        else
        {
          clusterVehicles.insert(std::make_pair(aoi, _featureExtractor.IsVehicleInSideLane(_mentalModel.GetVehicle(aoi))));
        }
      }
      else
      {
        clusterVehicles.insert(std::make_pair(aoi, _featureExtractor.IsVehicleInSideLane(_mentalModel.GetVehicle(aoi))));
      }
    }
    else
    {
      clusterVehicles.insert(std::make_pair(aoi, _featureExtractor.IsVehicleInSideLane(_mentalModel.GetVehicle(aoi))));
    }
  }

  return clusterVehicles;
}

std::vector<AreaOfInterest> SituationManager::GetClusterVehicles(std::vector<AreaOfInterest> areas)
{
  // vehicles should be present
  std::vector<AreaOfInterest> clusterVehicles({});

  for (AreaOfInterest aoi : areas)
  {
    if (_mentalModel.GetIsVehicleVisible(aoi))
    {
      clusterVehicles.push_back(aoi);
    }
  }

  return clusterVehicles;
}

double SituationManager::DetermineIntensityFromAccelerationLongitudinal(units::acceleration::meters_per_second_squared_t acceleration, bool isFrontObject) const
{
  units::acceleration::meters_per_second_squared_t comfortAcceleration{-999.9_mps_sq};
  units::acceleration::meters_per_second_squared_t maxAcceleration{-999.9_mps_sq};

  if (isFrontObject)  // Ego tries to decelerate to defuse the situation
  {
    comfortAcceleration = _mentalModel.GetComfortLongitudinalDeceleration();
    maxAcceleration = _mentalModel.GetDriverParameters().maximumLongitudinalDeceleration;
    acceleration = units::math::min(-acceleration, maxAcceleration);
  }
  else  // Ego tries to accelerate to defuse the situation
  {
    comfortAcceleration = _mentalModel.GetComfortLongitudinalAcceleration();
    maxAcceleration = _mentalModel.GetDriverParameters().maximumLongitudinalAcceleration;
    acceleration = units::math::min(acceleration, maxAcceleration);
  }

  return DetermineIntensityFromAcceleration(acceleration, comfortAcceleration, maxAcceleration);
}

double SituationManager::DetermineIntensityFromAccelerationLateral(units::acceleration::meters_per_second_squared_t acceleration) const
{
  auto comfortAcceleration = _mentalModel.GetDriverParameters().comfortLateralAcceleration;
  auto maxAcceleration = _mentalModel.GetDriverParameters().maximumLateralAcceleration;
  return DetermineIntensityFromAcceleration(acceleration, comfortAcceleration, maxAcceleration);
}

double SituationManager::DetermineIntensityFromAcceleration(units::acceleration::meters_per_second_squared_t baseAcceleration, units::acceleration::meters_per_second_squared_t comfortAcceleration, units::acceleration::meters_per_second_squared_t maxAcceleration) const
{
  if (baseAcceleration <= 0.0_mps_sq)
  {
    // no risk (no acceleration necessary)
    return 0.0;
  }

  if (baseAcceleration < comfortAcceleration)
  {
    // low risk - 0 to comfortAcceleration
    return GetRiskValue(Risk::LOW);
  }

  double lowIntensity;
  double highIntensity;
  units::acceleration::meters_per_second_squared_t lowAcceleration;
  units::acceleration::meters_per_second_squared_t highAcceleration;
  if (baseAcceleration < comfortAcceleration * ScmDefinitions::COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER)
  {
    // low to medium risk - comfortAcceleration to comfortAcceleration * MULTIPLIER
    lowIntensity = GetRiskValue(Risk::LOW);
    highIntensity = GetRiskValue(Risk::MEDIUM);
    lowAcceleration = comfortAcceleration;
    highAcceleration = comfortAcceleration * ScmDefinitions::COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER;
  }
  else
  {
    // medium to high risk - comfortAcceleration * MULTIPLIER to maxAcceleration
    lowIntensity = GetRiskValue(Risk::MEDIUM);
    highIntensity = GetRiskValue(Risk::HIGH);
    lowAcceleration = comfortAcceleration * ScmDefinitions::COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER;
    highAcceleration = maxAcceleration;
  }

  // Determine a risk value (intensity) between low and high risk values proportionally to the acceleration value
  // (baseAcceleration) relative to the acceleration thresholds (low/highAcceleration).
  // Calculation according to the point-slope form of a linear equation.
  const double deltaIntensity{highIntensity - lowIntensity};
  const double deltaAcceleration{highAcceleration - lowAcceleration};
  const double slope{deltaIntensity / deltaAcceleration};
  return slope * units::unit_cast<double>((baseAcceleration - lowAcceleration)) + lowIntensity;
}

void SituationManager::AddRelevantSideClusterIntensities(double intensityFromAccelerationLaneChange,
                                                         double probabilityLaneChanger,
                                                         bool isAnticipatedLaneChanger,
                                                         double intensityFromAccelerationSuspiciousObject,
                                                         double probabilitySuspiciousObject,
                                                         double intensityFromAccelerationSideCollision,
                                                         double probabilitySideCollision,
                                                         std::map<Situation, double>& situationIntensitiesSideCluster,
                                                         Side side,
                                                         AreaOfInterest aoi)
{
  const double intensityLaneChange = intensityFromAccelerationLaneChange * probabilityLaneChanger;
  const double intensitySuspiciousObject = intensityFromAccelerationSuspiciousObject * probabilitySuspiciousObject;
  const double intensitySideCollision = intensityFromAccelerationSideCollision * probabilitySideCollision;

  if (side == Side::Right)
  {
    if (intensityLaneChange > situationIntensitiesSideCluster.at(Situation::LANE_CHANGER_FROM_RIGHT))
    {
      SetSideClusterIntensities(Situation::LANE_CHANGER_FROM_RIGHT, isAnticipatedLaneChanger, intensityLaneChange, intensityFromAccelerationLaneChange, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::LANE_CHANGER_FROM_RIGHT, aoi);
    }

    if (intensitySuspiciousObject > situationIntensitiesSideCluster.at(Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE))
    {
      SetSideClusterIntensities(Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, isAnticipatedLaneChanger, intensitySuspiciousObject, intensityFromAccelerationSuspiciousObject, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, aoi);
    }

    if (intensitySideCollision > situationIntensitiesSideCluster.at(Situation::SIDE_COLLISION_RISK_FROM_RIGHT))
    {
      SetSideClusterIntensities(Situation::SIDE_COLLISION_RISK_FROM_RIGHT, isAnticipatedLaneChanger, intensitySideCollision, intensityFromAccelerationSideCollision, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::SIDE_COLLISION_RISK_FROM_RIGHT, aoi);
    }
  }

  if (side == Side::Left)
  {
    if (intensityLaneChange > situationIntensitiesSideCluster.at(Situation::LANE_CHANGER_FROM_LEFT))
    {
      SetSideClusterIntensities(Situation::LANE_CHANGER_FROM_LEFT, isAnticipatedLaneChanger, intensityLaneChange, intensityFromAccelerationLaneChange, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::LANE_CHANGER_FROM_LEFT, aoi);
    }

    if (intensitySuspiciousObject > situationIntensitiesSideCluster.at(Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE))
    {
      SetSideClusterIntensities(Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, isAnticipatedLaneChanger, intensitySuspiciousObject, intensityFromAccelerationSuspiciousObject, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, aoi);
    }

    if (intensitySideCollision > situationIntensitiesSideCluster.at(Situation::SIDE_COLLISION_RISK_FROM_LEFT))
    {
      SetSideClusterIntensities(Situation::SIDE_COLLISION_RISK_FROM_LEFT, isAnticipatedLaneChanger, intensitySideCollision, intensityFromAccelerationSideCollision, situationIntensitiesSideCluster);
      _mentalModel.SetCausingVehicleOfSituationSideCluster(Situation::SIDE_COLLISION_RISK_FROM_LEFT, aoi);
    }
  }
}

void SituationManager::SetSideClusterIntensities(Situation situation, bool isAnticipated, double situationIntensity, double intensityFromAcceleration, std::map<Situation, double>& situationIntensitiesSideCluster)
{
  _mentalModel.SetSituationAnticipated(situation, isAnticipated);
  situationIntensitiesSideCluster.at(situation) = situationIntensity;
  Risk risk = intensityFromAcceleration <= 1.0 ? Risk::LOW : Risk::MEDIUM;
  risk = intensityFromAcceleration > 4.0 ? Risk::HIGH : risk;

  // Check HighCognitive risks
  HighCognitiveSituation situationHc{_mentalModel.GetHighCognitiveSituation()};
  if (situation == Situation::LANE_CHANGER_FROM_RIGHT && isAnticipated &&
      (situationHc == HighCognitiveSituation::SALIENT_APPROACH || situationHc == HighCognitiveSituation::SALIENT_FOLLOW))
  {
    risk = Risk::HIGH;
  }

  _mentalModel.SetSituationRisk(situation, risk);
}

void SituationManager::DetermineIntensitiesForFrontClusterAoi(AreaOfInterest frontVehicleArea, bool* frontC_hasJamVelocity)
{
  const auto* vehicle{_mentalModel.GetVehicle(frontVehicleArea)};
  if (vehicle == nullptr)
  {
    return;
  }

  const auto netDistance{vehicle->GetRelativeNetDistance()};
  // needs at least periphery to be reliable
  const bool isInfluencingDistanceViolated = netDistance.IsReliable(DataQuality::LOW) && netDistance.GetValue() <= _mentalCalculations.GetInfDistance(frontVehicleArea);

  if (!isInfluencingDistanceViolated)
  {
    return;
  }

  const bool ufovReliability = vehicle->IsReliable(DataQuality::MEDIUM);
  // needs ufovReliability
  auto frontVehicle = _mentalModel.GetVehicle(frontVehicleArea);
  const bool isMinimumFollowingDistanceViolated = ufovReliability && _featureExtractor.IsMinimumFollowingDistanceViolated(frontVehicle);
  const bool isObstacle = ufovReliability && _featureExtractor.IsObstacle(frontVehicle);
  const bool nearEnoughForFollowing = ufovReliability && _featureExtractor.IsNearEnoughForFollowing(*frontVehicle);

  if (ufovReliability && frontVehicleArea == AreaOfInterest::EGO_FRONT)
  {
    *frontC_hasJamVelocity = _featureExtractor.HasJamVelocity(_mentalModel.GetVehicle(frontVehicleArea));
  }

  // Determine probability of occurance
  const bool lateralObstruction = LaneQueryHelper::IsEgoLane(frontVehicleArea) || _mentalModel.GetObstruction(frontVehicleArea).isOverlapping;  // Obstruction only relevant in side lanes

  const double probabilityFollowingDriving = (lateralObstruction && (isInfluencingDistanceViolated || nearEnoughForFollowing)) ? 1.0 : 0.0;

  const double probabilityObstacleOnCurrentLane = lateralObstruction ? GetIntensity_ObstacleOnCurrentLane(isObstacle, isMinimumFollowingDistanceViolated) : 0.0;

  const auto accelerationLimit = -_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration;
  const auto accelerationForAvoidance = _mentalCalculations.CalculateAccelerationToDefuse(frontVehicleArea, accelerationLimit);

  // Determine intensity from acceleration
  double intensityFromAcceleration = DetermineIntensityFromAccelerationLongitudinal(accelerationForAvoidance, true);
  double intensityFromAccelerationAtFollowingDriving = probabilityFollowingDriving > 0.0 ? std::max(intensityFromAcceleration, 0.1) : intensityFromAcceleration;
  const double intensityObstacleOnCurrentLane = intensityFromAcceleration;

  AddRelevantFrontClusterIntensities(frontVehicleArea, intensityFromAccelerationAtFollowingDriving, probabilityFollowingDriving, intensityObstacleOnCurrentLane, probabilityObstacleOnCurrentLane);
}

void SituationManager::AddRelevantFrontClusterIntensities(AreaOfInterest frontVehicleArea,
                                                          double intensityFromAccelerationAtFollowingDriving,
                                                          double probabilityFollowingDriving,
                                                          double intensityFromAccelerationAtObstacleOnCurrentLane,
                                                          double probabilityObstacleOnCurrentLane)
{
  const double intensityFollowingDriving{intensityFromAccelerationAtFollowingDriving * probabilityFollowingDriving};
  const double intensityObstacleOnCurrentLane{intensityFromAccelerationAtObstacleOnCurrentLane * probabilityObstacleOnCurrentLane};
  if (intensityFollowingDriving > _situationIntensities.at(Situation::FOLLOWING_DRIVING))
  {
    _situationIntensities.at(Situation::FOLLOWING_DRIVING) = intensityFollowingDriving;
    _mentalModel.SetCausingVehicleOfSituationFrontCluster(Situation::FOLLOWING_DRIVING, frontVehicleArea);
    _mentalModel.SetSituationAnticipated(Situation::FOLLOWING_DRIVING, probabilityFollowingDriving == 1.0);
    Risk situationRisk{intensityFromAccelerationAtFollowingDriving <= 1.0 ? Risk::LOW : Risk::MEDIUM};
    situationRisk = intensityFromAccelerationAtFollowingDriving > 4.0 ? Risk::HIGH : situationRisk;
    _mentalModel.SetSituationRisk(Situation::FOLLOWING_DRIVING, situationRisk);
  }

  if (intensityObstacleOnCurrentLane > _situationIntensities.at(Situation::OBSTACLE_ON_CURRENT_LANE))
  {
    _situationIntensities.at(Situation::OBSTACLE_ON_CURRENT_LANE) = intensityObstacleOnCurrentLane;
    _mentalModel.SetCausingVehicleOfSituationFrontCluster(Situation::OBSTACLE_ON_CURRENT_LANE, frontVehicleArea);
    _mentalModel.SetSituationAnticipated(Situation::OBSTACLE_ON_CURRENT_LANE, probabilityObstacleOnCurrentLane == 1.0);
    Risk situationRisk{intensityFromAccelerationAtObstacleOnCurrentLane <= 1.0 ? Risk::LOW : Risk::MEDIUM};
    situationRisk = intensityFromAccelerationAtObstacleOnCurrentLane > 4.0 ? Risk::HIGH : situationRisk;
    _mentalModel.SetSituationRisk(Situation::OBSTACLE_ON_CURRENT_LANE, situationRisk);
  }
}

void SituationManager::FilterVehicle(std::vector<AreaOfInterest>& vehicles, AreaOfInterest near, AreaOfInterest far)
{
  const bool containsNear{std::find(vehicles.begin(), vehicles.end(), near) != vehicles.end()};
  const bool containsFar{std::find(vehicles.begin(), vehicles.end(), far) != vehicles.end()};
  if (containsNear && containsFar)
  {
    AreaOfInterest aoiToErase = GetAreaToErase(near, far);
    vehicles.erase(std::find(vehicles.begin(), vehicles.end(), aoiToErase));
  }
}

void SituationManager::FilterFrontClusterSideVehicle(std::vector<AreaOfInterest>& vehicles)
{
  FilterVehicle(vehicles, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR);
  FilterVehicle(vehicles, AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR);
  for (auto aoi = vehicles.begin(); aoi != vehicles.end();)
  {
    if (!LaneQueryHelper::IsEgoLane(*aoi))
    {
      const auto* vehicle{_mentalModel.GetVehicle(*aoi)};
      if (vehicle != nullptr)
      {
        const auto obstruction{vehicle->GetLateralObstruction()};
        if (obstruction.IsReliable(DataQuality::MEDIUM) &&
            !ObstructionScm(obstruction).isOverlapping)
        {
          aoi = vehicles.erase(std::find(vehicles.begin(), vehicles.end(), *aoi));
          continue;
        }
      }
    }
    ++aoi;
  }
}

AreaOfInterest SituationManager::GetAreaToErase(AreaOfInterest near, AreaOfInterest far)
{
  const auto* frontVehicle{_mentalModel.GetVehicle(near)};
  const bool frontUfovReliable{frontVehicle != nullptr && frontVehicle->IsReliable(DataQuality::MEDIUM)};
  const int sideAoiIndex{_mentalModel.DetermineIndexOfSideObject(near,
                                                                 SideAoiCriteria::MAX,
                                                                 [](const SurroundingVehicleInterface* vehicle)
                                                                 { return std::abs(vehicle->GetLateralVelocity().GetValue().value()); })};
  const bool isFarOverlappingButNotNear = _mentalModel.GetObstruction(far).isOverlapping && !_mentalModel.GetObstruction(near).isOverlapping;

  if (isFarOverlappingButNotNear || (!_featureExtractor.IsCollisionCourseDetected(frontVehicle) &&
                                     frontUfovReliable &&
                                     frontVehicle->GetLaneOfPerception() == RelativeLane::EGO &&
                                     frontVehicle->IsExceedingLateralMotionThreshold()))
  {
    return near;
  }
  return far;
}

std::vector<AreaOfInterest> SituationManager::FilterFrontClusterVehicle(std::vector<AreaOfInterest> vehicles)
{
  FilterVehicle(vehicles, AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR);
  FilterFrontClusterSideVehicle(vehicles);
  return vehicles;
}

void SituationManager::ResetIntensitiesFrontCluster()
{
  _situationIntensities[Situation::FOLLOWING_DRIVING] = 0;
  _mentalModel.SetCausingVehicleOfSituationFrontCluster(Situation::FOLLOWING_DRIVING, AreaOfInterest::NumberOfAreaOfInterests);
  _situationIntensities[Situation::OBSTACLE_ON_CURRENT_LANE] = 0;
  _mentalModel.SetCausingVehicleOfSituationFrontCluster(Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::NumberOfAreaOfInterests);
}

void SituationManager::GetIntensitiesFrontCluster(bool* frontC_hasJamVelocity)
{
  ResetIntensitiesFrontCluster();
  std::vector<AreaOfInterest> frontClusterVehicles = GetClusterVehicles(_frontClusterAreas);
  frontClusterVehicles = FilterFrontClusterVehicle(frontClusterVehicles);

  if (frontClusterVehicles.size() > 0)
  {
    for (const AreaOfInterest& frontVehicleArea : frontClusterVehicles)
    {
      DetermineIntensitiesForFrontClusterAoi(frontVehicleArea, frontC_hasJamVelocity);
    }
  }
}

void SituationManager::GetIntensitiesLeftCluster(bool* leftC_hasJamVelocity)
{
  std::map<AreaOfInterest, bool> clusterVehicles = GetClusterVehiclesSideLane(_leftClusterAreas);

  std::map<Situation, double> situationIntensitiesLeftCluster = {{Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, 0.0},
                                                                 {Situation::LANE_CHANGER_FROM_LEFT, 0.0},
                                                                 {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.0}};
  GetIntensitiesSideCluster(leftC_hasJamVelocity, clusterVehicles, situationIntensitiesLeftCluster, Side::Left);
}

void SituationManager::GetIntensitiesRightCluster(bool* rightC_hasJamVelocity)
{
  std::map<AreaOfInterest, bool> clusterVehicles = GetClusterVehiclesSideLane(_rightClusterAreas);

  std::map<Situation, double> situationIntensitiesRightCluster = {{Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.0},
                                                                  {Situation::LANE_CHANGER_FROM_RIGHT, 0.0},
                                                                  {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.0}};
  GetIntensitiesSideCluster(rightC_hasJamVelocity, clusterVehicles, situationIntensitiesRightCluster, Side::Right);
}

void SituationManager::GetIntensitiesSideCluster(bool* hasJamVelocity, std::map<AreaOfInterest, bool> clusterVehicles, std::map<Situation, double> situationIntensitiesCluster, Side side)
{
  bool isAnticipatingDriver = _mentalModel.GetIsAnticipating();
  for (std::pair<const AreaOfInterest, bool> pair : clusterVehicles)
  {
    AreaOfInterest vehicleArea = pair.first;
    bool clusterEnabled = pair.second;

    // enabler feature side cluster
    if (clusterEnabled)
    {
      const auto* vehicle{_mentalModel.GetVehicle(vehicleArea)};
      bool foveaReliable = vehicle != nullptr && vehicle->IsReliable(DataQuality::HIGH);
      bool ufovReliable = vehicle != nullptr && vehicle->IsReliable(DataQuality::MEDIUM);
      bool peripheryReliable = vehicle != nullptr && vehicle->IsReliable(DataQuality::LOW);

      std::map<Situation, double> situationIntensities = {};
      bool isLaneDriveablePerceived = true;
      bool isLaneChangePermittedDueToLaneMarkingsForAoi = false;
      double intensityForNeedToChangeLane = 0.;
      double intensityForSlowerLeadingVehicle = 0.;
      bool hasIntentionalLaneCrossingConflict = false;
      bool hasActualLaneCrossingConflict = false;
      bool hasAnticipatedLaneCrossingConflict = false;
      bool isInfluencingDistanceViolated = false;
      bool isNearEnoughForFollowing = false;
      bool hasSuspiciousVehicleOnSideLane = false;

      // without reliability level -> information is always concidered as reliable
      isLaneDriveablePerceived = _featureExtractor.IsLaneDriveablePerceived(AreaOfInterest2RelativeLane.at(vehicleArea));

      // needs at least periphery quality to be reliable
      if (peripheryReliable)
      {
        hasIntentionalLaneCrossingConflict = _featureExtractor.HasIntentionalLaneCrossingConflict(vehicle);
        isInfluencingDistanceViolated = _featureExtractor.IsInfluencingDistanceViolated(vehicle);
      }

      if (vehicleArea == AreaOfInterest::RIGHT_FRONT || vehicleArea == AreaOfInterest::LEFT_FRONT)
      {
        *hasJamVelocity = _featureExtractor.HasJamVelocity(vehicle);
      }

      if (!isInfluencingDistanceViolated)
      {
        continue;
      }

      // needs at least ufov quality to be reliable
      if (ufovReliable)
      {
        intensityForNeedToChangeLane =
            _mentalCalculations.GetIntensityForNeedToChangeLane(vehicleArea);
        intensityForSlowerLeadingVehicle =
            _mentalCalculations.GetIntensityForSlowerLeadingVehicle(vehicleArea);
        hasAnticipatedLaneCrossingConflict =
            _featureExtractor.HasAnticipatedLaneCrossingConflict(vehicle);
        isNearEnoughForFollowing =
            _featureExtractor.IsNearEnoughForFollowing(*_mentalModel.GetVehicle(vehicleArea));
        isLaneChangePermittedDueToLaneMarkingsForAoi =
            _featureExtractor.IsLaneChangePermittedDueToLaneMarkingsForAoi(_mentalModel.GetVehicle(vehicleArea));

        const int sideAoiIndex{_mentalModel.DetermineIndexOfSideObject(vehicleArea, SideAoiCriteria::FRONT)};
        hasSuspiciousVehicleOnSideLane =
            _featureExtractor.HasSuspiciousBehaviour(_mentalModel.GetVehicle(vehicleArea, sideAoiIndex == -1 ? 0 : sideAoiIndex));
      }

      // needs at least fovea quality to be reliable
      if (foveaReliable && vehicle != nullptr)
      {
        hasActualLaneCrossingConflict = _featureExtractor.HasActualLaneCrossingConflict(*vehicle);
      }

      double intensityFromAccelerationLaneChange{0.0};
      double probabilityLaneChange{0.0};
      bool isAnticipatedLaneChange{false};
      double intensityFromAccelerationSuspiciousObject{0.0};
      double probabilitySuspiciousObject{0.0};
      double intensityFromAccelerationSideCollision{0.0};
      double probabilitySideCollision{0.0};

      // check intensities for cluster
      if (vehicleArea != AreaOfInterest::RIGHT_SIDE && vehicleArea != AreaOfInterest::LEFT_SIDE)
      {
        const bool isFrontArea = LaneQueryHelper::IsFrontArea(vehicleArea);
        const auto accelerationLimit = -_mentalModel.GetDriverParameters().maximumLongitudinalDeceleration;
        const auto accelerationForAvoidance = _mentalCalculations.CalculateAccelerationToDefuse(vehicleArea, accelerationLimit);
        intensityFromAccelerationSuspiciousObject = DetermineIntensityFromAccelerationLongitudinal(accelerationForAvoidance, isFrontArea);
        probabilitySuspiciousObject = hasSuspiciousVehicleOnSideLane ? 1.0 : 0.0;

        intensityFromAccelerationLaneChange = intensityFromAccelerationSuspiciousObject;  // Currently same object and criteria

        if (isInfluencingDistanceViolated && hasActualLaneCrossingConflict)  // detected lane changer
        {
          std::vector<bool> queryVector = {hasIntentionalLaneCrossingConflict, hasActualLaneCrossingConflict};
          probabilityLaneChange = GetIntensityFromQueryVector(queryVector);
          isAnticipatedLaneChange = false;
        }
        else if (isAnticipatingDriver)
        {
          const HighCognitiveSituation situationHc{_mentalModel.GetHighCognitiveSituation()};
          const bool hasHighCognitiveSituation{situationHc != HighCognitiveSituation::UNDEFINED};
          const auto distanceToEndOfLaneRight{_mentalModel.GetDistanceToEndOfLane(-1)};
          const bool hasNoEndOfLaneRight{distanceToEndOfLaneRight < 0._m};  // HighCognitive is not trained for lane endings

          if (side == Side::Right && hasHighCognitiveSituation && hasNoEndOfLaneRight)  // Use HighCognitive if available
          {
            probabilityLaneChange = _mentalModel.GetHighCognitiveLaneChangeProbability();
            isAnticipatedLaneChange = true;
          }
          else  // Not HighCognitive or no HCSituation found
          {
            probabilityLaneChange = GetIntensity_AnticipatedLaneChanger(false,
                                                                        isNearEnoughForFollowing,
                                                                        hasAnticipatedLaneCrossingConflict,
                                                                        hasIntentionalLaneCrossingConflict,
                                                                        isLaneDriveablePerceived,
                                                                        isLaneChangePermittedDueToLaneMarkingsForAoi,
                                                                        intensityForNeedToChangeLane,
                                                                        intensityForSlowerLeadingVehicle);
            isAnticipatedLaneChange = true;
          }
        }
      }
      else
      {
        // Determine propability of occurance
        if (side == Side::Right)
        {
          probabilitySideCollision = GetIntensity_RightSideCollisionRisk(hasActualLaneCrossingConflict, hasIntentionalLaneCrossingConflict);
        }
        if (side == Side::Left)
        {
          probabilitySideCollision = GetIntensity_LeftSideCollisionRisk(hasActualLaneCrossingConflict, hasIntentionalLaneCrossingConflict);
        }
        // Determine strength of act of avoidance
        const auto accelerationForAvoidance{_mentalCalculations.CalculateAccelerationToDefuseLateral(vehicleArea)};
        intensityFromAccelerationSideCollision = DetermineIntensityFromAccelerationLateral(accelerationForAvoidance);
      }

      AddRelevantSideClusterIntensities(intensityFromAccelerationLaneChange,
                                        probabilityLaneChange,
                                        isAnticipatedLaneChange,
                                        intensityFromAccelerationSuspiciousObject,
                                        probabilitySuspiciousObject,
                                        intensityFromAccelerationSideCollision,
                                        probabilitySideCollision,
                                        situationIntensitiesCluster,
                                        side,
                                        vehicleArea);
    }
  }

  // Transfer final situation intensities
  for (const auto& situation : situationIntensitiesCluster)
  {
    _situationIntensities.at(situation.first) = situation.second;
  }
}

void SituationManager::DetectMesoscopicSituations(const MesoscopicSituationFeatures& features, ZipMergingInterface& zipMerging)
{
  // mesoscopic information is always reliable till implemented otherwise
  // => currently there will be no explicit information request from the mesoscopic cluster
  const bool currentLaneBlocked{!_featureExtractor.IsLaneDriveablePerceived()};
  currentLaneBlocked ? _mentalModel.ActivateMesoscopicSituation(MesoscopicSituation::CURRENT_LANE_BLOCKED) : _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::CURRENT_LANE_BLOCKED);

  const bool mandatoryExit{_mentalModel.GetDistanceToEndOfNextExit() > 0_m};
  mandatoryExit ? _mentalModel.ActivateMesoscopicSituation(MesoscopicSituation::MANDATORY_EXIT) : _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::MANDATORY_EXIT);

  const bool queuedTraffic{CheckMesoscopicSituationQueuedTraffic(features)};
  queuedTraffic ? _mentalModel.ActivateMesoscopicSituation(MesoscopicSituation::QUEUED_TRAFFIC) : _mentalModel.DeactivateMesoscopicSituation(MesoscopicSituation::QUEUED_TRAFFIC);

  zipMerging.UpdateMesoscopicSituation();
}

bool SituationManager::IsSituationAmbigious(double situationIntensity, bool reliableFovea, bool reliableUfov, bool reliablePeriphery)
{
  if (reliableFovea || situationIntensity == 1.)
  {
    return false;
  }

  if (!reliablePeriphery)
  {
    return true;
  }

  if ((situationIntensity == static_cast<double>(Risk::HIGH) && !reliableFovea) ||
      (situationIntensity == static_cast<double>(Risk::MEDIUM) && !reliableUfov))
  {
    return true;
  }

  return false;
}

SituationManager::MesoscopicSituationFeatures SituationManager::GenerateSituationIntensityVector()
{
  // drop old intensity vectors
  _situationIntensities = {{Situation::UNDEFINED, 0.0},
                           {Situation::FREE_DRIVING, 0.0},
                           {Situation::FOLLOWING_DRIVING, 0.0},
                           {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, 0.0},
                           {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.0},
                           {Situation::LANE_CHANGER_FROM_LEFT, 0.0},
                           {Situation::LANE_CHANGER_FROM_RIGHT, 0.0},
                           {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.0},
                           {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.0},
                           {Situation::OBSTACLE_ON_CURRENT_LANE, 0.0},
                           {Situation::COLLISION, 0.0},
                           {Situation::NumberOfSituations, 0.0}};

  // in collision case stop the analysis of the situation as long as a collision is interpreted as no possibility to move
  if (_mentalModel.GetCollisionState())
  {
    _situationIntensities[Situation::COLLISION] = 1;
    return {false, false, false};
  }

  // features for evaluation of QUEUED_TRAFFIC in mesoscopic cluster
  MesoscopicSituationFeatures features;
  _mentalModel.ResetCausingVehicleOfFrontCluster();
  _mentalModel.ResetCausingVehicleOfSideCluster();

  GetIntensitiesFrontCluster(&features.frontClusterJam);
  GetIntensitiesLeftCluster(&features.leftClusterJam);
  GetIntensitiesRightCluster(&features.rightClusterJam);

  // if there are no special situations with a higher value than the treshold
  // the recognized situation is set to standard free driving
  if (std::none_of(std::begin(_situationIntensities), std::end(_situationIntensities), [](const auto& intensity)
                   { return intensity.second > 0; }))
  {
    _situationIntensities[Situation::FREE_DRIVING] = GetIntensity_FreeDriving();
  }

  return features;
}

double SituationManager::GetIntensityFromQueryVector(std::vector<bool>& queryVector)
{
  unsigned int count = 0;
  double intensity = 0.;

  if (queryVector.empty())
  {
    return 0.;
  }

  unsigned int vecSize = static_cast<unsigned int>(queryVector.size());
  double intensityAdd = 1. / vecSize;

  for (unsigned int i = 0; i < vecSize; i++)
  {
    if (queryVector[i])
    {
      count++;
      intensity += intensityAdd;
    }
  }

  // make 1 a precise number without loss
  if (count == vecSize)
  {
    return 1.;
  }

  return intensity;
}

double SituationManager::GetIntensity_FreeDriving()
{
  return 1.;
}

double SituationManager::GetIntensity_CollisionCase()
{
  return 1.;
}

double SituationManager::GetIntensity_ObstacleOnCurrentLane(bool frontC_isObstacle,
                                                            bool frontC_isMinimumFollowingDistanceViolated)
{
  if (frontC_isMinimumFollowingDistanceViolated)  // this will lead to TOO_CLOSE and this situation is more risky than OBSTACLE_ON_CURRENT_LANE
  {
    return 0.;
  }
  else if (frontC_isObstacle)
  {
    return 1.;
  }
  else
  {
    return 0.;
  }
}

bool SituationManager::CheckMesoscopicSituationQueuedTraffic(const MesoscopicSituationFeatures& features)
{
  if (features.frontClusterJam)
  {
    // Lane left and right exist
    if (_mentalModel.GetLaneExistence(RelativeLane::LEFT) && features.leftClusterJam &&
        _mentalModel.GetLaneExistence(RelativeLane::RIGHT) && features.rightClusterJam)
    {
      return true;
    }

    // Lane left exists, right does not exist
    if (_mentalModel.GetLaneExistence(RelativeLane::LEFT) && !_mentalModel.GetLaneExistence(RelativeLane::RIGHT) && features.leftClusterJam)
    {
      return true;
    }

    // Lane left does not exist, right exists
    if (!_mentalModel.GetLaneExistence(RelativeLane::LEFT) && _mentalModel.GetLaneExistence(RelativeLane::RIGHT) && features.rightClusterJam)
    {
      return true;
    }

    // Only ego lane exists
    if (!_mentalModel.GetLaneExistence(RelativeLane::LEFT) && !_mentalModel.GetLaneExistence(RelativeLane::RIGHT))
    {
      return true;
    }

    return false;  // Only front has jam velocity
  }

  auto egoMeanLaneVelocity = _mentalModel.GetMeanVelocityLaneEgo();
  if (egoMeanLaneVelocity != _mentalModel.GetDriverParameters().desiredVelocity && egoMeanLaneVelocity < _mentalModel.GetTrafficJamVelocityThreshold())
  {
    return true;
  }

  return false;  // Front does not have jam velocity
}

double SituationManager::GetIntensity_LeftSideCollisionRisk(bool leftC_hasActualLaneCrossingConflict,
                                                            bool leftC_hasIntentionalLaneCrossingConflict)
{
  if (leftC_hasActualLaneCrossingConflict)
  {
    return 1.;
  }

  std::vector<bool> queryVector = {leftC_hasActualLaneCrossingConflict, leftC_hasIntentionalLaneCrossingConflict, leftC_hasActualLaneCrossingConflict};
  return GetIntensityFromQueryVector(queryVector);
}

double SituationManager::GetIntensity_RightSideCollisionRisk(bool rightC_hasActualLaneCrossingConflict,
                                                             bool rightC_hasIntentionalLaneCrossingConflict)
{
  if (rightC_hasActualLaneCrossingConflict)
  {
    return 1.;
  }

  std::vector<bool> queryVector = {rightC_hasActualLaneCrossingConflict, rightC_hasIntentionalLaneCrossingConflict, rightC_hasActualLaneCrossingConflict};
  return GetIntensityFromQueryVector(queryVector);
}

double SituationManager::GetIntensity_HighRiskLaneChanger(bool hasIntentionalLaneCrossingConflict,
                                                          bool hasActualLaneCrossingConflict,
                                                          bool isMinimumFollowingDistanceViolated)
{
  if (isMinimumFollowingDistanceViolated && hasActualLaneCrossingConflict)
  {
    std::vector<bool> queryVector = {hasIntentionalLaneCrossingConflict, hasActualLaneCrossingConflict};
    return GetIntensityFromQueryVector(queryVector);
  }

  return 0.;
}

double SituationManager::GetIntensity_AnticipatedHighRiskLaneChanger(bool fromLeft,
                                                                     bool isMinimumFollowingDistanceViolated,
                                                                     bool isInfluencingDistanceViolated,
                                                                     bool hasAnticipatedLaneCrossingConflict,
                                                                     bool hasIntentionalLaneCrossingConflict,
                                                                     bool isLaneDriveablePerceived,
                                                                     bool isLaneChangePermittedDueToLaneMarkingsForAoi,
                                                                     double intensityForNeedToChangeLane,
                                                                     double intensityForSlowerLeadingVehicle)
{
  if (isMinimumFollowingDistanceViolated)
  {
    return GetIntensity_AnticipatedLaneChanger(fromLeft,
                                               isInfluencingDistanceViolated,
                                               hasAnticipatedLaneCrossingConflict,
                                               hasIntentionalLaneCrossingConflict,
                                               isLaneDriveablePerceived,
                                               isLaneChangePermittedDueToLaneMarkingsForAoi,
                                               intensityForNeedToChangeLane,
                                               intensityForSlowerLeadingVehicle);
  }

  return 0.;
}

double SituationManager::GetIntensity_AnticipatedLaneChanger(bool fromLeft,
                                                             bool isNearEnoughForFollowing,
                                                             bool hasAnticipatedLaneCrossingConflict,
                                                             bool hasIntentionalLaneCrossingConflict,
                                                             bool isLaneDriveablePerceived,
                                                             bool isLaneChangePermittedDueToLaneMarkingsForAoi,
                                                             double intensityForNeedToChangeLane,
                                                             double intensityForSlowerLeadingVehicle)
{
  auto virtualAgents = _mentalModel.GetVirtualAgents();
  if (!virtualAgents.empty() && _mentalModel.GetCooperativeBehavior())
  {
    for (auto virtualAgent : virtualAgents)
    {
      auto virtualType = virtualAgent.lanetype;
      if (virtualType == scm::LaneType::OnRamp || virtualType == scm::LaneType::Entry)
      {
        return 1.0;
      }
    }
  }
  // influencing distance is reached
  if (isNearEnoughForFollowing)
  {
    double intensity = 0.0;

    if (hasIntentionalLaneCrossingConflict)  // indicator is seen
    {
      intensity = 1.0;
    }
    else
    {
      if (!isLaneDriveablePerceived)  // lane of observed agent ends
      {
        // calculated lane change intensity due to ending lane of observed agent and
        // slower leading vehicle (not higher than 1)
        intensity = std::min(1.0, intensityForNeedToChangeLane + intensityForSlowerLeadingVehicle);
      }
      else
      {
        // no anticipation for right overtaking (intensity = 0)
        if (!fromLeft)
        {
          // only intensity due to slower leading vehicle, no ending lane
          intensity = std::min(1.0, intensityForSlowerLeadingVehicle);
        }
      }
    }

    return hasAnticipatedLaneCrossingConflict || intensityForNeedToChangeLane > 0. ? intensity : 0.0;
  }

  if (isLaneChangePermittedDueToLaneMarkingsForAoi && hasIntentionalLaneCrossingConflict)  // indicator is seen
  {
    return 1.0;
  }

  return 0.0;
}

units::time::millisecond_t SituationManager::GetCurrentSituationDuration()
{
  return _durationCurrentSituation;
}

void SituationManager::SetCurrentSituationDuration(units::time::millisecond_t durationCurrentSituation)
{
  _durationCurrentSituation = durationCurrentSituation;
}

void SituationManager::SetSituationLastTick(Situation situation)
{
  _situationLastTick = situation;
}