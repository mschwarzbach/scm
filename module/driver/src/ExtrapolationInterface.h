/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ExtrapolationInterface.h

#pragma once

#include "ScmDefinitions.h"
class MentalModelInterface;

class ExtrapolationInterface
{
public:
  virtual ~ExtrapolationInterface() = default;

  //! \brief Extrapolates vehicle data for the passed objectInformation pointer.
  //! \param objectInformation Surrounding object data to extrapolate
  //! \param aoi               Area of Interest to which the object was assigned in the last iteration.
  virtual void Extrapolate(ObjectInformationScmExtended* objectInformation, AreaOfInterest aoi) const = 0;
};
