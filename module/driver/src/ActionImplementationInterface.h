/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  ActionImplementationInterface.h

#pragma once
#include "ActionManagerInterface.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "include/common/LightState.h"

//! *******************************************************************
//! @brief Interface class for the implementation of planned actions.
//! @note Currently only a small part has been moved to this interface!
//! *******************************************************************

//! @brief Struct holds parameter for the current lane change trajectory and internal values to progress it
struct TrajectoryState
{
  //! @brief Actual trajectory
  TrajectoryCalculations::Trajectory trajectory;
  //! @brief traveled width since the start of the maneuver
  units::length::meter_t traveledWidth{0};
  //! @brief traveled distance along the reference line since the start of the maneuver
  units::length::meter_t traveledDistanceAlongReferenceLine{0};
  //! @brief traveled distance along the trajectory since the start of the maneuver
  units::length::meter_t traveledDistanceAlongTrajectory{0};
  //! @brief lateral offset when maneuver was first planned
  units::length::meter_t initialLateralOffset{0};
  //! @brief the desired lateral displacement for the maneuver
  units::length::meter_t desiredDisplacement{0};
  //! @brief index to loop through the trajectory
  size_t lastUsedIndex{0};
};

class ActionImplementationInterface
{
public:
  virtual ~ActionImplementationInterface() = default;

  //! @brief Transmit ego data to the ActionImplementation instance for further use.
  //! @param actionManagerInterface   ActionManagerInterface
  virtual void UpdateInformationFromActionManager(ActionManagerInterface* actionManagerInterface) = 0;

  //! @brief Decide for action depending on situation and cognitive data.
  virtual void ImplementAction() = 0;

  //! @brief Get the lateral distance that ego has traveled since the beginning of a lane change.
  //! @return currently traveled lateral distance
  virtual units::length::meter_t GetLateralDisplacementTraveled() const = 0;

  //! @brief Getter for desired lateral displacement in the next time step [m].
  //! @return desiredLateralDisplacement
  virtual units::length::meter_t GetDesiredLateralDisplacement() const = 0;

  //! @brief Get the flag whether the trajectory planning to reach the target lateral displacement in a lane change was completed.
  //! @return lateralDisplacementReached
  virtual bool GetLateralDisplacementReached() const = 0;

  //! @brief Getter for the longitudinal acceleration wish for the next time step [m/s²].
  //! @return accelerationWishLongitudinal
  virtual units::acceleration::meters_per_second_squared_t GetLongitudinalAccelerationWish() const = 0;

  //! @brief Getter for the indicator state of the next time step.
  //! @return indicatorStateNext
  virtual scm::LightState::Indicator GetNextIndicatorState() const = 0;

  //! @brief Getter for is flasher active of the next time step.
  //! @return flasherActiveNext
  virtual bool GetFlasherActiveNext() const = 0;

  //! @brief Getter for the lateral deviation gain used in the calculation for the next time step.
  //! @return _gainLateralDeviation
  virtual units::angular_acceleration::radians_per_second_squared_t GetLateralDeviationGain() const = 0;

  //! @brief Getter for the lateral deviation in the next time step at current trajectory [m].
  //! @return lateralDeviationNext
  virtual units::length::meter_t GetLateralDeviationNext() const = 0;

  //! @brief Getter for the heading error gain used in the calculation for the next time step.
  //! @return _gainHeadingError
  virtual units::frequency::hertz_t GetHeadingErrorGain() const = 0;

  //! @brief Getter for the heading error in the next time step at current trajectory [rad].
  //! @return headingErrorNext
  virtual units::angle::radian_t GetHeadingErrorNext() const = 0;

  //! @brief Getter for the curvature of the current manoeuvre [1/m].
  //! @return _kappaManoeuvre
  virtual units::curvature::inverse_meter_t GetKappaManoeuvre() const = 0;

  //! @brief Getter for the current curvature of the road [1/m].
  //! @return _kappaRoad
  virtual units::curvature::inverse_meter_t GetKappaRoad() const = 0;

  //! @brief Getter for the expected curvature of the maneuver (including road curvature) [1/m].
  //! @return _kappaSet
  virtual units::curvature::inverse_meter_t GetKapaSet() const = 0;

  //! @brief The deceleration from the powertrain drag
  //! @return DecelerationFromPowertrainDrag property from the VehicleModelParameters
  virtual units::acceleration::meters_per_second_squared_t GetDecelerationFromPowertrainDrag() const = 0;
};