/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmDriver.h"

#include "LateralAction.h"
#include "ScmComponents.h"
#include "include/common/OverloadPattern.h"

using namespace units::literals;
namespace scm
{
ScmDriver::ScmDriver(StochasticsInterface* stochastics, units::time::millisecond_t cycleTime, scm::publisher::PublisherInterface* const scmPublisher)
    : _scmSignalCollector(stochastics, cycleTime),
      _cycleTime(cycleTime),
      _pLog{std::make_unique<PeriodicLogger>(_scmPublisher)},
      _scmPublisher{scmPublisher}
{
}

struct VisitOutput
{
  scm::signal::DriverOutput operator()(CollidedState& state) { return state.GetOutput(); }
  scm::signal::DriverOutput operator()(ScmLifetimeState& state) { return state.GetOutput(); }
  scm::signal::DriverOutput operator()(std::monostate&) { return scm::signal::DriverOutput(); }
};

scm::signal::DriverOutput ScmDriver::Trigger(const scm::signal::DriverInput& driverInput, units::time::millisecond_t time)
{
  _scmSignalCollector.Update(driverInput);

  if (!_initialized)
  {
    _scmComponents = std::make_unique<ScmComponents>(*_scmSignalCollector.GetDependencies());
    _initialized = true;
  }

  SetExternalControlFlags(driverInput.automatedDriving);
  //  SetSensoryStimuli(driverInput.CompCtrlWarnings);

  ProgressScmModelState();
  Trigger(time);

  return std::visit(VisitOutput(), _scmModelState);
}

static auto TriggerScmModel(units::time::millisecond_t time)
{
  return overloaded{
      [time](auto&& agent)
      { agent.Trigger(time); },
      [](CollidedState) {},
      [](std::monostate) {}};
}

void ScmDriver::Trigger(units::time::millisecond_t time)
{
  std::visit(TriggerScmModel(time), _scmModelState);
}

void ScmDriver::ProgressScmModelState()
{
  if (IsInitializable())
  {
    _scmModelState = SpawningState(COMPONENT_NAME, _cycleTime, _pLog.get(), *_scmComponents, _externalControlState);
  }
  else if (IsSpawningState() || HasExternalControlStateChanged())
  {
    _scmModelState = LivingState(COMPONENT_NAME, _cycleTime, _pLog.get(), *_scmComponents, _externalControlState);
  }
  else if (IsLivingState() && _scmSignalCollector.GetDependencies()->GetOwnVehicleInformationScm()->collision)
  {
    _scmModelState = CollidedState(COMPONENT_NAME, _cycleTime, _pLog.get(), *_scmComponents, _externalControlState);
  }
}

bool ScmDriver::HasExternalControlStateChanged()
{
  auto hasChanged = _externalControlState.lateralActive != _externalControlStateLastStep.lateralActive || _externalControlState.longitudinalActive != _externalControlStateLastStep.longitudinalActive;
  _externalControlStateLastStep.lateralActive = _externalControlState.lateralActive;
  _externalControlStateLastStep.longitudinalActive = _externalControlState.longitudinalActive;
  return hasChanged;
}

bool ScmDriver::IsInitializable()
{
  return std::holds_alternative<std::monostate>(_scmModelState) && _scmSignalCollector.IsAlive();
}

bool ScmDriver::IsSpawningState()
{
  return std::holds_alternative<SpawningState>(_scmModelState);
}

bool ScmDriver::IsLivingState()
{
  return std::holds_alternative<LivingState>(_scmModelState);
}

// void ScmDriver::SetSensoryStimuli(const Warnings &compCtrlWarnings)
//{
//   opticAdasSignals.clear();
//
//   for (const auto &[componentName, componentWarnings] : compCtrlWarnings)
//   {
//     for (const auto &warning : componentWarnings)
//     {
//       AdasHmiSignal adasSignal;
//       adasSignal.activity = warning.activity;
//       adasSignal.type = warning.type;
//       adasSignal.level = warning.level;
//       adasSignal.intensity = warning.intensity;
//       // adasSignal.spotOfPresentation = warning.second.spotOfPresentation;
//       adasSignal.sendingSystem = componentName;
//
//       if (adasSignal.type == ComponentWarningType::OPTIC)
//       {
//         adasSignal.spotOfPresentation = "headUpDisplay";
//         opticAdasSignals.push_back(adasSignal);
//       }
//       else if (adasSignal.type == ComponentWarningType::ACOUSTIC)
//       {
//         adasSignal.spotOfPresentation = "undirected";
//         acousticAdasSignals.push_back(adasSignal);
//       }
//     }
//   }
// }

void ScmDriver::SetExternalControlFlags(const HafParameters& hafParameters)
{
  if (hafParameters.activateHaf)
  {
    _externalControlState.lateralActive = true;
    _externalControlState.longitudinalActive = true;
  }

  if (hafParameters.activateTakeoverRequest)
  {
    _isHafTakeoverRequest = true;
  }

  if (_isHafTakeoverRequest)
  {
    if (_scmComponents->GetGazeControl()->GetHafSignalProcessed() && _scmComponents->GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->fovea == AreaOfInterest::INSTRUMENT_CLUSTER)
    {
      _isEgoViewInInstrumentCluster = true;
    }

    if (_isEgoViewInInstrumentCluster && _scmComponents->GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->fovea == AreaOfInterest::EGO_FRONT)
    {
      _scmComponents->GetMentalModel()->ResetReactionBaseTime();
      _scmComponents->GetMentalModel()->SetAdjustmentBaseTime(0_s);
      _scmComponents->GetMentalModel()->SetRemainInLaneChangeState(false);
      _scmComponents->GetMentalModel()->SetIsEndOfLateralMovement(false);
      _scmComponents->GetMentalModel()->SetLaneChangeWidthTraveled(0_m);
      _scmComponents->GetMentalModel()->ResetTransitionState();
      _scmComponents->GetGazeControl()->SetHafSignalProcessed(false);

      _isEgoViewInInstrumentCluster = false;
      _externalControlState.lateralActive = false;
      _externalControlState.longitudinalActive = false;
      _isHafTakeoverRequest = false;
    }
    else
    {
      AdasHmiSignal adasSignal{};
      adasSignal.spotOfPresentation = "instrumentCluster";
      adasSignal.sendingSystem = "HAF";
      adasSignal.activity = true;
      _scmComponents->GetMentalModel()->SetCombinedAcousticOpticSignal(adasSignal);
    }
  }
}

}  // namespace scm