/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "LaneKeepingCalculations.h"

#include "include/common/ScmDefinitions.h"

LaneKeepingCalculations::LaneKeepingCalculations(const MentalModelInterface& mentalModel, const LateralActionQuery lateralAction)
    : _mentalModel{mentalModel}, _lateralAction{lateralAction}
{
}

std::pair<AreaOfInterest, AreaOfInterest> LaneKeepingCalculations::ChooseFrontAndRearSideAois() const
{
  AreaOfInterest aoiFront;
  AreaOfInterest aoiRear;
  const bool isInTargetLane = _mentalModel.GetIsLaneChangePastTransition();

  if ((_lateralAction.IsLaneChangingLeft() && !isInTargetLane) || (_lateralAction.IsLaneChangingRight() && isInTargetLane))
  {
    aoiFront = AreaOfInterest::LEFT_FRONT;
    aoiRear = AreaOfInterest::LEFT_REAR;
  }
  else if ((_lateralAction.IsLaneChangingLeft() && isInTargetLane) || (_lateralAction.IsLaneChangingRight() && !isInTargetLane))
  {
    aoiFront = AreaOfInterest::RIGHT_FRONT;
    aoiRear = AreaOfInterest::RIGHT_REAR;
  }
  else
  {
    throw std::runtime_error("ActionManager::ChooseFrontAndRearAois: Unknown state! Should not be possible!");
  }
  return std::make_pair(aoiFront, aoiRear);
}

units::velocity::meters_per_second_t LaneKeepingCalculations::CalculateVelocityDelta(AreaOfInterest referenceAoiFront, AreaOfInterest referenceAoiRear) const
{
  const auto ttcFront = _mentalModel.GetIsVehicleVisible(referenceAoiFront) ? _mentalModel.GetTtc(referenceAoiFront) : ScmDefinitions::INF_TIME;
  const auto ttcRear = _mentalModel.GetIsVehicleVisible(referenceAoiRear) ? _mentalModel.GetTtc(referenceAoiRear) : ScmDefinitions::INF_TIME;
  const auto velocityDeltaFront = CalculateVelocityDeltaAtCollision(ttcFront, referenceAoiFront);
  const auto velocityDeltaRear = -CalculateVelocityDeltaAtCollision(ttcRear, referenceAoiRear);  // Note: Negative, to make it compareble to front aoi (negative sign means ego is faster)
  const auto velocityDelta = units::math::min(velocityDeltaFront, velocityDeltaRear);            // Note: min since velocity delta is more negative when rear participant is faster
  return velocityDelta;
}

units::velocity::meters_per_second_t LaneKeepingCalculations::CalculateVelocityDeltaAtCollision(units::time::second_t ttc, AreaOfInterest aoi) const
{
  if (ttc > ScmDefinitions::TTC_LIMIT)
    return 0._mps;

  if (ttc == ScmDefinitions::TTC_LIMIT && !_mentalModel.GetIsVehicleVisible(aoi))
    throw std::runtime_error("ActionManager::CalculateVelocityDeltaAtCollision: A valid ttc is required for the calculation!");

  if (!(aoi == AreaOfInterest::EGO_FRONT || aoi == AreaOfInterest::EGO_REAR || aoi == AreaOfInterest::LEFT_FRONT ||
        aoi == AreaOfInterest::LEFT_REAR || aoi == AreaOfInterest::RIGHT_FRONT || aoi == AreaOfInterest::RIGHT_REAR))
    throw std::runtime_error("ActionManager::CalculateVelocityDeltaAtCollision: A valid aoi is required for the calculation!");

  const auto accelerationMax{_mentalModel.GetDriverParameters().maximumLongitudinalAcceleration};
  const auto accelerationObject{_mentalModel.GetAcceleration(aoi)};
  const auto velocityEgo{_mentalModel.GetAbsoluteVelocityEgo(true)};
  const auto velocityObject{_mentalModel.GetAbsoluteVelocity(aoi)};

  const auto velocityDelta{(accelerationObject - accelerationMax) * ttc + (velocityObject - velocityEgo)};
  return velocityDelta;
}

bool LaneKeepingCalculations::ChangingRightAppropriate() const
{
  const bool hasRightLane = _mentalModel.GetLaneExistence(RelativeLane::RIGHT, true);
  const bool isInTargetLane = _mentalModel.GetIsLaneChangePastTransition();
  const bool isEndOfLateralMovement = _mentalModel.GetIsEndOfLateralMovement();
  const bool targetReached = isInTargetLane || isEndOfLateralMovement;

  return _lateralAction.IsLaneChangingRight() && !targetReached && !hasRightLane;
}

bool LaneKeepingCalculations::ChangingLeftAppropriate() const
{
  const bool hasLeftLane = _mentalModel.GetLaneExistence(RelativeLane::LEFT, true);
  const bool isInTargetLane = _mentalModel.GetIsLaneChangePastTransition();
  const bool isEndOfLateralMovement = _mentalModel.GetIsEndOfLateralMovement();
  const bool targetReached = isInTargetLane || isEndOfLateralMovement;

  return _lateralAction.IsLaneChangingLeft() && !targetReached && !hasLeftLane;
}

bool LaneKeepingCalculations::IsAtEndOfLateralMovement() const
{
  return _mentalModel.GetIsEndOfLateralMovement();
}

bool LaneKeepingCalculations::IsObjectInTargetSide() const
{
  const bool isInTargetLane = _mentalModel.GetIsLaneChangePastTransition();

  return (_lateralAction.IsLaneChangingLeft() && !isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)) ||
         (_lateralAction.IsLaneChangingRight() && !isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)) ||
         (_lateralAction.IsLaneChangingLeft() && isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)) ||
         (_lateralAction.IsLaneChangingRight() && isInTargetLane && _mentalModel.GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0));
}

bool LaneKeepingCalculations::EgoFasterThanDeltaSide() const
{
  auto [aoiFront, aoiRear] = ChooseFrontAndRearSideAois();
  const auto velocityDeltaSide = CalculateVelocityDelta(aoiFront, aoiRear);
  const auto velocityDeltaEgo = CalculateVelocityDelta(AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_REAR);

  return velocityDeltaEgo >= velocityDeltaSide;
}