/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include "ScmDefinitions.h"

class IgnoringOuterLaneOvertakingProhibitionInterface
{
public:
  virtual ~IgnoringOuterLaneOvertakingProhibitionInterface() = default;

  //! @brief Checks whether there is a new relevant agent found and rolls again if ego will ignore the prohibition.
  //! @param egoFrontId id of vehicle directly in front
  //! @param sideFrontId id of side vehicle
  virtual void Update(int egoFrontId, int sideFrontId) = 0;

  //! @brief Checks whether ego is in a new situation an rolls again if the prohibition will be ignored.
  virtual void Update() = 0;

  //! @brief Checks whether ego is ignoring the prohibition for a given aoi.
  //! @param aoi aoi to check
  //! @return true if ego will ignore the overtaking prohibition for the agent in the given aoi
  virtual bool IsFulfilled(AreaOfInterest aoi) const = 0;
};