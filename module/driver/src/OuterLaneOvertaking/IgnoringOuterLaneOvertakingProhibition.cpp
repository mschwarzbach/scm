/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "IgnoringOuterLaneOvertakingProhibition.h"

#include "MentalModelInterface.h"

IgnoringOuterLaneOvertakingProhibition::IgnoringOuterLaneOvertakingProhibition(MentalModelInterface& mentalModel)
    : _mentalModel(mentalModel)
{
  _aoiToCausingAgents[AreaOfInterest::EGO_FRONT] = _agentEgoFront;
  _aoiToCausingAgents[AreaOfInterest::LEFT_FRONT] = _agentLeftFront;
  _aoiToCausingAgents[AreaOfInterest::RIGHT_FRONT] = _agentRightFront;
};

void IgnoringOuterLaneOvertakingProhibition::Update(int egoFrontId, int sideFrontId)
{
  if (IsNewAgentInAoi(AreaOfInterest::EGO_FRONT, egoFrontId))
  {
    _aoiToCausingAgents[AreaOfInterest::EGO_FRONT]._id = egoFrontId;
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::EGO_FRONT);
  }

  if (IsNewAgentInAoi(AreaOfInterest::LEFT_FRONT, sideFrontId))
  {
    _aoiToCausingAgents[AreaOfInterest::LEFT_FRONT]._id = sideFrontId;
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::LEFT_FRONT);
  }

  if (IsNewAgentInAoi(AreaOfInterest::RIGHT_FRONT, sideFrontId))
  {
    _aoiToCausingAgents[AreaOfInterest::RIGHT_FRONT]._id = sideFrontId;
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::RIGHT_FRONT);
  }
}

bool IgnoringOuterLaneOvertakingProhibition::IsNewAgentInAoi(AreaOfInterest aoi, int id) const
{
  if (_aoiToCausingAgents.at(aoi)._id != id)
  {
    return true;
  }

  return false;
}

void IgnoringOuterLaneOvertakingProhibition::Update()
{
  if (_mentalModel.GetHasSituationChanged())
  {
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::EGO_FRONT);
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::LEFT_FRONT);
    RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest::RIGHT_FRONT);
  }
}

void IgnoringOuterLaneOvertakingProhibition::RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest aoi)
{
  const double quota = _mentalModel.GetDriverParameters().rightOvertakingProhibitionIgnoringQuota.value_or(ScmDefinitions::DEFAULT_VALUE);
  if (quota == 0.0)
  {
    _aoiToCausingAgents[aoi]._quotaFulfilled = false;
  }
  else
  {
    const double rolledValue = _mentalModel.GetStochastics()->GetUniformDistributed(0, 1);
    _aoiToCausingAgents[aoi]._quotaFulfilled = rolledValue <= quota;
  }
}

bool IgnoringOuterLaneOvertakingProhibition::IsFulfilled(AreaOfInterest aoi) const
{
  return _aoiToCausingAgents.at(aoi)._quotaFulfilled;
}
