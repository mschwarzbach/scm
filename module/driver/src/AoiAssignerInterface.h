/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "InfrastructureCharacteristicsInterface.h"
#include "SurroundingVehicles/SurroundingVehicleDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
//! @brief Implements the AoiAssignerInterface
class AoiAssignerInterface
{
public:
  virtual ~AoiAssignerInterface() = default;

  //! @brief SurroundingVehicleVector
  using SurroundingVehicleVector = std::vector<SurroundingVehicleInterface*>;

  //! @brief Assigns relative lane to the vehicles in the given vector based on relative position to ego and surrounding lane widths.
  //! @param surroundingVehicles Vector containing all surrounding vehicles around ego
  //! @return AoiVehicleMapping  Mapping between relative lanes and the assigned vehicles
  virtual LaneVehicleMapping AssignToLanes(const SurroundingVehicleVector& surroundingVehicles) const = 0;

  //! @brief Assigns an AOI to the vehicles in the given lane mapping based on relative position to ego and assigned surrounding lanes.
  //! @param laneMapping Previously generated lane assignment.
  //! @return AoiVehicleMapping  Mapping between AOIs and the assigned vehicles
  virtual AoiVehicleMapping AssignToAois(const LaneVehicleMapping& laneMapping) const = 0;

  //! @brief Assigns an AOI to the vehicles in the given lane mapping based on relative position to ego and assigned surrounding lanes.
  //! @param laneMapping Previously generated lane assignment.
  //! @return AoiVehicleMapping  Mapping between AOIs and the assigned vehicles
  virtual AoiVehicleMapping AssignToAoisVector(const LaneVehicleMapping& laneMapping) const = 0;

  //! @brief Assigns an AOI to the vehicles in the given vector based on relative position to ego and assigned surrounding lanes.
  //! @param surroundingVehicles Vector containing all surrounding vehicles around ego
  //! @return AoiVehicleMapping  Mapping between AOIs and the assigned vehicles
  virtual AoiVehicleMapping AssignToAois(const SurroundingVehicleVector& surroundingVehicles) const = 0;
};