/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AccelerationAdjustmentParameters.h

#pragma once
#include "AccelerationCalculationsQueryInterface.h"

//! @brief Struct definition for the description of a AccelerationAdjustmentParameters
struct AccelerationAdjustmentParameters
{
  //! @brief desiredDistance
  units::length::meter_t desiredDistance;
  //! @brief regulationWindowSize
  units::length::meter_t regulationWindowSize;
  //! @brief approachingFactor
  double approachingFactor;
  //! @brief maxDeceleration
  units::acceleration::meters_per_second_squared_t maxDeceleration;
};

namespace AccelerationParameters
{
//! @brief Calculates parameters to determine required acceleration.
//! @param query Query object that hides underlying calls to various scm modules
//! @param vehicle Leading vehicle to be followed
//! @param maxDeceleration Maximum deceleration to use during acceleration calculation - is used unchanged
//! @param mergeRegulateId Id of MergeRegulate vehicle
//! @param deltaDistanceToAdjustment Offset to determined desired distance that shall be applied
//! @return AccelerationAdjustmentParameters
AccelerationAdjustmentParameters CalculateAccelerationAdjustmentParameters(const AccelerationCalculationsQueryInterface& query,
                                                                           const SurroundingVehicleInterface& vehicle,
                                                                           units::acceleration::meters_per_second_squared_t maxDeceleration,
                                                                           int mergeRegulateId,
                                                                           units::length::meter_t deltaDistanceToAdjustment = 0_m);

//! @brief Calculates the desired following distance for the given leading vehicle.
//! @param query Query object that hides underlying calls to various scm modules
//! @param vehicle Leading vehicle to be followed
//! @param mergeRegulateId Id of MergeRegulate vehicle
//! @return Desired following distance
units::length::meter_t CalculateDesiredDistance(const AccelerationCalculationsQueryInterface& query,
                                                const SurroundingVehicleInterface& vehicle,
                                                int mergeRegulateId);

//! @brief Calculates the regulation window size that is used during acceleration calculation.
//! @param query Query object that hides underlying calls to various scm modules
//! @param vehicle Leading vehicle to be followed
//! @param desiredDistance Previously determined, desired following distance
//! @param mergeRegulateId Id of MergeRegulate vehicle
//! @return Size of the regulation window to be used in the acceleration controller.
units::length::meter_t CalculateRegulationWindowSize(const AccelerationCalculationsQueryInterface& query,
                                                     const SurroundingVehicleInterface& vehicle,
                                                     units::length::meter_t desiredDistance,
                                                     int mergeRegulateId);

//! @brief Calculates multiplication factor when approaching the leading vehicle.
//! @param vehicle Leading vehicle to be followed
//! @param desiredDistance Previously determined, desired following distance
//! @param regulationWindowSize Previously determined, regulation window
//! @return Calculated approaching factor
double CalculateApproachingFactor(const SurroundingVehicleInterface& vehicle,
                                  units::length::meter_t desiredDistance,
                                  units::length::meter_t regulationWindowSize);
}  // namespace AccelerationParameters