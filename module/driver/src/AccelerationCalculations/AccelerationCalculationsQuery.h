/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AccelerationCalculationsQuery.h
#pragma once

#include "../MentalCalculationsInterface.h"
#include "../MentalModelInterface.h"
#include "AccelerationCalculationsQueryInterface.h"

//! @brief Implements the AccelerationCalculationsQueryInterface
class AccelerationCalculationsQuery : public AccelerationCalculationsQueryInterface
{
public:
  AccelerationCalculationsQuery(const MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations);

  bool IsEgoBelowJamSpeed() const override;
  bool IsDetectedLaneChangerLeadingVehicle(const SurroundingVehicleInterface& leadingVehicle) const override;
  units::length::meter_t GetEqDistance(const SurroundingVehicleInterface& vehicle) const override;
  units::length::meter_t GetMinDistance(const SurroundingVehicleInterface& vehicle) const override;
  units::length::meter_t GetMinDistanceDuringMerge(const SurroundingVehicleInterface &vehicle) const override;
  units::time::second_t GetReactionBaseTimeMean() const override;
  units::time::second_t GetPedalChangeTimeMean() const override;
  units::length::meter_t GetCarQueuingDistance() const override;
  double GetUrgency() const override;
  bool IsEgoNearStandstill() const override;
  units::velocity::meters_per_second_t GetEgoVelocity() const override;
  units::acceleration::meters_per_second_squared_t GetComfortAcceleration() const override;
  units::acceleration::meters_per_second_squared_t GetMergingAcceleration() const override;
  bool IsEgoCooperative() const override;
  int GetMergeRegulateId() const override;
  Merge::Gap GetMergeGap() const override;
  const MentalModelInterface& GetMentalModel() const override;

private:
  const MentalModelInterface& _mentalModel;
  const MentalCalculationsInterface& _mentalCalculations;
};