/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AccelerationComponents.h"

#include <cmath>

#include "AccelerationCalculations/AccelerationAdjustmentParameters.h"
#include "AccelerationCalculationsQueryInterface.h"

AccelerationComponents::AccelerationComponents(const AccelerationCalculationsQueryInterface& query, const AccelerationAdjustmentParameters& parameters, const SurroundingVehicleInterface& vehicle)
    : _query{query},
      _parameters{parameters},
      _vehicle{vehicle}
{
}

units::acceleration::meters_per_second_squared_t AccelerationComponents::CalculateAccelerationFromDistance() const
{
  const auto deltaS{_vehicle.RearDistance().GetValue() - _parameters.desiredDistance};
  const auto distanceRegulationRatio{std::clamp(deltaS.value() / _parameters.regulationWindowSize.value(), -1.0, 1.0)};

  if (_vehicle.RearDistance().GetValue() <= _parameters.desiredDistance)
  {
    return _parameters.maxDeceleration * distanceRegulationRatio;
  }

  if (_vehicle.GetId() == _query.GetMergeRegulateId())
  {
    return _query.GetMergingAcceleration() * distanceRegulationRatio;
  }

  return _query.GetComfortAcceleration() * distanceRegulationRatio;
}

units::acceleration::meters_per_second_squared_t AccelerationComponents::CalculateAccelerationFromVelocity() const
{
  const bool egoFartherThanDesired{_vehicle.RearDistance().GetValue() > _parameters.desiredDistance};
  auto deltaV{_query.GetEgoVelocity() - _vehicle.GetLongitudinalVelocity().GetValue()};

  if (egoFartherThanDesired)
  {
    const auto velocityBuffer{units::math::min(0.25 * _vehicle.GetLongitudinalVelocity().GetValue(), 10_kph)};
    deltaV -= velocityBuffer;
  }

  auto acceleration{units::math::pow<2>(deltaV) / (2 * _parameters.regulationWindowSize)};

  if (deltaV > 0_mps)
  {
    acceleration *= _parameters.approachingFactor;
    if (egoFartherThanDesired)
    {
      acceleration /= 2.;
    }
  }

  return deltaV < 0_mps ? acceleration : -acceleration;
}

units::acceleration::meters_per_second_squared_t AccelerationComponents::CalculateAccelerationFromAcceleration() const
{
  const auto accelerationVehicle{_vehicle.GetLongitudinalAcceleration().GetValue()};

  if (accelerationVehicle < 0_mps_sq)
  {
    return accelerationVehicle * _parameters.approachingFactor;
  }

  return accelerationVehicle;
}