/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AccelerationCalculationsQueryInterface.h
#pragma once

#include "../SurroundingVehicles/SurroundingVehicleInterface.h"
#include "MentalModelDefinitions.h"
#include "MentalModelInterface.h"

//! *********************************************************************************************************************
//! @brief This interface class wraps methods of MentalModel and MentalCalculations and thus provides useful queries for
//! the calculations of individual acceleration components.
//! *********************************************************************************************************************
class AccelerationCalculationsQueryInterface
{
public:
  virtual ~AccelerationCalculationsQueryInterface() = default;

  //! @brief Wrapper for MentalCalculations::EgoBelowJamSpeed
  //! @return True if the ego speed is below traffic jam velocity
  virtual bool IsEgoBelowJamSpeed() const = 0;

  //! @brief
  //! @param leadingVehicle
  //! @return
  virtual bool IsDetectedLaneChangerLeadingVehicle(const SurroundingVehicleInterface& leadingVehicle) const = 0;

  //! @brief Wrapper for MentalCalculations::GetEqDistance
  //! @param vehicle reference vehicle for equilibrium distance calculation
  //! @return the equilibrium distance
  virtual units::length::meter_t GetEqDistance(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Wrapper for MentalCalculations::GetMinDistance
  //! @param vehicle reference vehicle for min distance calculation
  //! @return the minimum distance
  virtual units::length::meter_t GetMinDistance(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Get the minimum distance of the merge gap.
  //! @param vehicle
  //! @return the set minimum distance for the current merge gap
  virtual units::length::meter_t GetMinDistanceDuringMerge(const SurroundingVehicleInterface& vehicle) const = 0;

  //! @brief Get the mean value of the reaction base time.
  //! @return DriverParameter::reactionBaseTimeMean
  virtual units::time::second_t GetReactionBaseTimeMean() const = 0;

  //! @brief Get the mean value of the pedal change time .
  //! @return DriverParameter::pedalChangeTimeMean
  virtual units::time::second_t GetPedalChangeTimeMean() const = 0;

  //! @brief Get the car queuing distance.
  //! @return DriverParameter::carQueuingDistance
  virtual units::length::meter_t GetCarQueuingDistance() const = 0;

  //! @brief Get the current (inverse) urgency factor from the MentalCalculations for lane changes.
  //! @return 1 - MentalCalculations::GetUrgencyFactorForLaneChange
  virtual double GetUrgency() const = 0;

  //! @brief Checks whether the Ego agent is basically not moving anymore
  //! @return True if egos velocity is below 0.1 ms
  virtual bool IsEgoNearStandstill() const = 0;

  //! @brief Wrapper for MentalModel::GetLongitudinalVelocityEgo
  //! @return Egos longitudinal velocity
  virtual units::velocity::meters_per_second_t GetEgoVelocity() const = 0;

  //! @brief Wrapper for MentalModel::GetComfortLongitudinalAcceleration
  //! @return DriverParameter::comfortLongitudinalAcceleration
  virtual units::acceleration::meters_per_second_squared_t GetComfortAcceleration() const = 0;

  //! @brief Wrapper for MentalModel::GetAccelerationLimitForMerging
  //! @return the deviating acceleration limit for merging maneuvers
  virtual units::acceleration::meters_per_second_squared_t GetMergingAcceleration() const = 0;

  //! @brief Wrapper for MentalModel::IsEgoCooperative
  //! @return True if ego is set to be cooperative
  virtual bool IsEgoCooperative() const = 0;

  //! @brief Wrapper for MentalModel::GetMergeRegulateId
  //! @return the merge regulate agent id
  virtual int GetMergeRegulateId() const = 0;

  //! @brief Wrapper for MentalModel::GetMergeGap
  //! @return the current merge gap
  virtual Merge::Gap GetMergeGap() const = 0;

  //! @brief MentalModel getter
  //! @return reference to mental model interface
  virtual const MentalModelInterface& GetMentalModel() const = 0;
};