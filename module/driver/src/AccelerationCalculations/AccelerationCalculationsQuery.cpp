/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AccelerationCalculationsQuery.h"

#include "../SurroundingVehicles/SurroundingVehicleInterface.h"

AccelerationCalculationsQuery::AccelerationCalculationsQuery(const MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations)
    : _mentalModel{mentalModel},
      _mentalCalculations{mentalCalculations}
{
}

bool AccelerationCalculationsQuery::IsEgoBelowJamSpeed() const
{
  return _mentalCalculations.EgoBelowJamSpeed();
}

units::length::meter_t AccelerationCalculationsQuery::GetEqDistance(const SurroundingVehicleInterface& vehicle) const
{
  return _mentalCalculations.GetEqDistance(&vehicle);
}

units::length::meter_t AccelerationCalculationsQuery::GetMinDistance(const SurroundingVehicleInterface& vehicle) const
{
  return _mentalCalculations.GetMinDistance(&vehicle);
}

units::length::meter_t AccelerationCalculationsQuery::GetMinDistanceDuringMerge(const SurroundingVehicleInterface& vehicle) const
{
  // TODO check if this method and all connected logic can be removed
  const auto mergeGap{_mentalModel.GetMergeGap()};
  return mergeGap.regulatingVehicle == Merge::RegulatingVehicle::leader
             ? mergeGap.safetyClearanceToLeader
             : mergeGap.safetyClearanceToFollower;
}

units::time::second_t AccelerationCalculationsQuery::GetReactionBaseTimeMean() const
{
  return _mentalModel.GetDriverParameters().reactionBaseTimeMean;
}

units::time::second_t AccelerationCalculationsQuery::GetPedalChangeTimeMean() const
{
  return _mentalModel.GetDriverParameters().pedalChangeTimeMean;
}

units::length::meter_t AccelerationCalculationsQuery::GetCarQueuingDistance() const
{
  return _mentalModel.GetDriverParameters().carQueuingDistance;
}

double AccelerationCalculationsQuery::GetUrgency() const
{
  return 1.0 - _mentalCalculations.GetUrgencyFactorForLaneChange();
}

bool AccelerationCalculationsQuery::IsEgoNearStandstill() const
{
  static constexpr auto minimumVelocityThreshold{0.1_mps};
  return _mentalModel.GetAbsoluteVelocityEgo(false) < minimumVelocityThreshold;
}

units::velocity::meters_per_second_t AccelerationCalculationsQuery::GetEgoVelocity() const
{
  return _mentalModel.GetLongitudinalVelocityEgo();
}

units::acceleration::meters_per_second_squared_t AccelerationCalculationsQuery::GetComfortAcceleration() const
{
  return _mentalModel.GetComfortLongitudinalAcceleration();
}

units::acceleration::meters_per_second_squared_t AccelerationCalculationsQuery::GetMergingAcceleration() const
{
  return _mentalModel.GetAccelerationLimitForMerging();
}

bool AccelerationCalculationsQuery::IsDetectedLaneChangerLeadingVehicle(const SurroundingVehicleInterface& leadingVehicle) const
{
  const auto situation{_mentalModel.GetCurrentSituation()};
  const bool isLaneChangerDetected{situation == Situation::LANE_CHANGER_FROM_LEFT || situation == Situation::LANE_CHANGER_FROM_RIGHT};
  return isLaneChangerDetected &&
         leadingVehicle.GetAssignedAoi() == _mentalModel.GetCausingVehicleOfSituationSideCluster();
}

bool AccelerationCalculationsQuery::IsEgoCooperative() const
{
  return _mentalModel.GetCooperativeBehavior();
}

int AccelerationCalculationsQuery::GetMergeRegulateId() const
{
  return _mentalModel.GetMergeRegulateId();
}
Merge::Gap AccelerationCalculationsQuery::GetMergeGap() const
{
  return _mentalModel.GetMergeGap();
}

const MentalModelInterface& AccelerationCalculationsQuery::GetMentalModel() const
{
  return _mentalModel;
}