/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AccelerationComponentsInterface.h

#pragma once

//! ******************************************************************************************************************************
//! @brief Interface class for the three components needed to determine a suitable acceleration for EGO to follow another vehicle.
//! ******************************************************************************************************************************
#include <units.h>
class AccelerationComponentsInterface
{
public:
  virtual ~AccelerationComponentsInterface() = default;

  //! @brief Calculates the required acceleration for following the leading vehicle based on the distance delta.
  //! @return needed acceleration to compensate the distance component
  virtual units::acceleration::meters_per_second_squared_t CalculateAccelerationFromDistance() const = 0;

  //! @brief Calculates the required acceleration for following the leading vehicle based on the velocity delta.
  //! @return needed acceleration to compensate the velocity component
  virtual units::acceleration::meters_per_second_squared_t CalculateAccelerationFromVelocity() const = 0;

  //! @brief Calculates the required acceleration for following the leading vehicle based on the acceleration delta.
  //! @return needed acceleration to compensate the acceleration component
  virtual units::acceleration::meters_per_second_squared_t CalculateAccelerationFromAcceleration() const = 0;
};