/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file DriverFactory.h
#pragma once

#include <memory>

#include "../../../include/module/driverInterface.h"
#include "Driver.h"

namespace scm::driver::factory
{
//! @brief Factory to create the SCM Driver model
//! @param stochastics
//! @param cycleTime
//! @param publisher publisher for cyclic data (legacy, to be deleted soon)
//! @param scmPublisher publisher for cyclic data
//! @return unique_ptr to scm::driver::Interface
inline std::unique_ptr<scm::driver::Interface> Create(StochasticsInterface* stochastics,
                                                      units::time::millisecond_t cycleTime,
                                                      scm::publisher::PublisherInterface* const scmPublisher)
{
  return std::unique_ptr<scm::driver::Interface>(scm::driver::Create(stochastics,
                                                                     cycleTime,
                                                                     scmPublisher));
}

}  // namespace scm::driver::factory