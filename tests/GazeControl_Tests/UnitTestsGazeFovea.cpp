/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/GazeFovea.h"
#include "include/common/AreaOfInterest.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;

/*************************************
 * CHECK GetFoveaGazeAngleHorizontal *
 *************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GazeFovea_GetFoveaGazeAngleHorizontal
{
  units::angle::radian_t mlp;
  units::angle::radian_t interiorMirrorOrientationRelativeToDriver;
  GazeState currentGazeState;
  units::angle::radian_t result;
};

class GazeFovea_GetFoveaGazeAngleHorizontal : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_GazeFovea_GetFoveaGazeAngleHorizontal>
{
};

TEST_P(GazeFovea_GetFoveaGazeAngleHorizontal, GetFoveaGazeAngleHorizontal)
{
  DataFor_GazeFovea_GetFoveaGazeAngleHorizontal data = GetParam();
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  ON_CALL(fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  OwnVehicleInformationScmExtended ego;
  ego.fovea = AreaOfInterest::EGO_FRONT;
  ego.InteriorMirrorOrientationRelativeToDriver = data.interiorMirrorOrientationRelativeToDriver;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));

  SurroundingObjectsSCM surroundingObject;
  ObjectInformationSCM egoFront;
  egoFront.orientationRelativeToDriver.frontCenter = data.mlp;
  surroundingObject.ongoingTraffic.Ahead().Close().push_back(egoFront);

  GazeFovea gazeFovea(fakeMentalModel);
  auto result = gazeFovea.GetFoveaGazeAngleHorizontal(data.currentGazeState, surroundingObject);

  ASSERT_EQ(data.result, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFovea_GetFoveaGazeAngleHorizontal,
    testing::Values(
        DataFor_GazeFovea_GetFoveaGazeAngleHorizontal{
            .mlp = 10.0_rad,
            .interiorMirrorOrientationRelativeToDriver = -999.0_rad,
            .currentGazeState = GazeState::EGO_FRONT,
            .result = 10.0_rad},
        DataFor_GazeFovea_GetFoveaGazeAngleHorizontal{
            .mlp = -999.0_rad,
            .interiorMirrorOrientationRelativeToDriver = 10.0_rad,
            .currentGazeState = GazeState::EGO_REAR,
            .result = 10.0_rad},
        DataFor_GazeFovea_GetFoveaGazeAngleHorizontal{
            .mlp = -999.0_rad,
            .interiorMirrorOrientationRelativeToDriver = -999.0_rad,
            .currentGazeState = GazeState::EGO_FRONT,
            .result = 0._rad}));