/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <vector>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "GazeControl/TopDownRequest.h"
#include "LateralAction.h"
#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct TopDownRequestTester
{
  class TopDownRequestUnderTest : TopDownRequest
  {
  public:
    template <typename... Args>
    TopDownRequestUnderTest(Args&&... args)
        : TopDownRequest{std::forward<Args>(args)...} {};

    using TopDownRequest::ChangeIntensitiesForInformationRequests;
    using TopDownRequest::ChangeIntensitiesForVehiclesOvertaking;
    using TopDownRequest::ChangeIntensitiesToAvoidImpossibilities;
    using TopDownRequest::GetIntensitiesForLaneKeepingWithoutLeadingVehicle;
    using TopDownRequest::Update;
  };

  TopDownRequestTester()
      : fakeFeatureExtractor{},
        fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        topDownRequest(fakeFeatureExtractor,
                       &fakeStochastics,
                       fakeMentalModel)
  {
  }

  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  TopDownRequestUnderTest topDownRequest;
};

/*******************************
 * CHECK TopDownRequest Update *
 *******************************/
struct DataFor_TopDownRequest_Update
{
  bool isInit;
  bool jamVelocity;
  LateralAction::Direction direction;
  LateralAction::State state;
  double resultEGO_FRONT_FAR;
  double resultRIGHT_FRONT_FAR;
  double resultLEFT_FRONT_FAR;
  double resultEGO_FRONT;
  double resultRIGHT_FRONT;
  double resultLEFT_FRONT;
  double resultRIGHTRIGHT_FRONT;
  double resultLEFTLEFT_FRONT;
  double resultEGO_REAR;
  double resultRIGHT_REAR;
  double resultLEFT_REAR;
  double resultRIGHTRIGHT_REAR;
  double resultLEFTLEFT_REAR;
  double resultLEFT_SIDE;
  double resultRIGHTRIGHT_SIDE;
  double resultRIGHT_SIDE;
  double resultLEFTLEFT_SIDE;
  double resultINSTRUMENT_CLUSTER;
  double resultINFOTAINMENT;
  double resultHUD;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_TopDownRequest_Update& obj)
  {
    return os
           << " isInit (bool): " << obj.isInit
           << " jamVelocity (bool): " << obj.jamVelocity
           << " resultEGO_FRONT_FAR (double) : " << obj.resultEGO_FRONT_FAR
           << " resultRIGHT_FRONT_FAR (double) : " << obj.resultRIGHT_FRONT_FAR
           << " resultLEFT_FRONT_FAR(double) : " << obj.resultLEFT_FRONT_FAR
           << " resultEGO_FRONT (double) : " << obj.resultEGO_FRONT
           << " resultRIGHT_FRONT (double) : " << obj.resultRIGHT_FRONT
           << " resultLEFT_FRONT (double) : " << obj.resultLEFT_FRONT
           << " resultRIGHTRIGHT_FRONT (double) : " << obj.resultRIGHTRIGHT_FRONT
           << " resultLEFTLEFT_FRONT (double) : " << obj.resultLEFTLEFT_FRONT
           << " resultEGO_REAR (double) : " << obj.resultEGO_REAR
           << " resultRIGHT_REAR(double) : " << obj.resultRIGHT_REAR
           << " resultLEFT_REAR(double): " << obj.resultLEFT_REAR
           << " resultRIGHTRIGHT_REAR (double) : " << obj.resultRIGHTRIGHT_REAR
           << " resultLEFTLEFT_REAR (double) : " << obj.resultLEFTLEFT_REAR
           << " resultLEFT_SIDE (double) : " << obj.resultLEFT_SIDE
           << " resultRIGHTRIGHT_SIDE (double): " << obj.resultRIGHTRIGHT_SIDE
           << " resultRIGHT_SIDE (double): " << obj.resultRIGHT_SIDE
           << " resultLEFTLEFT_SIDE (double): " << obj.resultLEFTLEFT_SIDE
           << " resultINSTRUMENT_CLUSTER (double): " << obj.resultINSTRUMENT_CLUSTER
           << " resultINFOTAINMENT (double): " << obj.resultINFOTAINMENT
           << " resultHUD (double): " << obj.resultHUD;
  }
};
class TopDownRequest_Update : public ::testing::Test,
                              public ::testing::WithParamInterface<DataFor_TopDownRequest_Update>
{
};
TEST_P(TopDownRequest_Update, Update)
{
  DataFor_TopDownRequest_Update data = GetParam();
  TopDownRequestTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(false));

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.preventLaneChangeDueToSpawn = false;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(TEST_HELPER.fakeStochastics, GetNormalDistributed(_, _)).WillByDefault(Return(1));
  LateralAction lateralAction;
  lateralAction.state = data.state;
  lateralAction.direction = data.direction;

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasJamVelocityEgo()).WillByDefault(Return(data.jamVelocity));

  std::map<AreaOfInterest, double> topDownRequestMap;
  auto result = TEST_HELPER.topDownRequest.Update(lateralAction, topDownRequestMap, data.isInit);

  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::EGO_FRONT_FAR], data.resultEGO_FRONT_FAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFT_FRONT_FAR], data.resultLEFT_FRONT_FAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::EGO_FRONT], data.resultEGO_FRONT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHT_FRONT], data.resultRIGHT_FRONT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFT_FRONT], data.resultLEFT_FRONT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHTRIGHT_FRONT], data.resultRIGHTRIGHT_FRONT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFTLEFT_FRONT], data.resultLEFTLEFT_FRONT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::EGO_REAR], data.resultEGO_REAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHT_REAR], data.resultRIGHT_REAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFT_REAR], data.resultLEFT_REAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHTRIGHT_REAR], data.resultRIGHTRIGHT_REAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFTLEFT_REAR], data.resultLEFTLEFT_REAR);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFT_SIDE], data.resultLEFT_SIDE);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHTRIGHT_SIDE], data.resultRIGHTRIGHT_SIDE);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::RIGHT_SIDE], data.resultRIGHT_SIDE);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::LEFTLEFT_SIDE], data.resultLEFTLEFT_SIDE);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::INSTRUMENT_CLUSTER], data.resultINSTRUMENT_CLUSTER);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::INFOTAINMENT], data.resultINFOTAINMENT);
  ASSERT_EQ(result.gazeStateIntensities[GazeState::HUD], data.resultHUD);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TopDownRequest_Update,
    testing::Values(
        DataFor_TopDownRequest_Update{
            .isInit = true,
            .jamVelocity = true,
            .direction = LateralAction::Direction::RIGHT,
            .state = LateralAction::State::LANE_KEEPING,
            .resultEGO_FRONT_FAR = 1.5,
            .resultRIGHT_FRONT_FAR = 0,
            .resultLEFT_FRONT_FAR = 0,
            .resultEGO_FRONT = 1.5,
            .resultRIGHT_FRONT = 0,
            .resultLEFT_FRONT = 0,
            .resultRIGHTRIGHT_FRONT = 0,
            .resultLEFTLEFT_FRONT = 0,
            .resultEGO_REAR = 0.5,
            .resultRIGHT_REAR = 0,
            .resultLEFT_REAR = 0,
            .resultRIGHTRIGHT_REAR = 0,
            .resultLEFTLEFT_REAR = 0,
            .resultLEFT_SIDE = 0,
            .resultRIGHTRIGHT_SIDE = 0,
            .resultRIGHT_SIDE = 0,
            .resultLEFTLEFT_SIDE = 0,
            .resultINSTRUMENT_CLUSTER = 1,
            .resultINFOTAINMENT = 0,
            .resultHUD = 0},
        DataFor_TopDownRequest_Update{
            .isInit = false,
            .jamVelocity = false,
            .direction = LateralAction::Direction::RIGHT,
            .state = LateralAction::State::LANE_KEEPING,
            .resultEGO_FRONT_FAR = 1.0,
            .resultRIGHT_FRONT_FAR = 0,
            .resultLEFT_FRONT_FAR = 0,
            .resultEGO_FRONT = 1.0,
            .resultRIGHT_FRONT = 0,
            .resultLEFT_FRONT = 0,
            .resultRIGHTRIGHT_FRONT = 0,
            .resultLEFTLEFT_FRONT = 0,
            .resultEGO_REAR = 1.0,
            .resultRIGHT_REAR = 0,
            .resultLEFT_REAR = 0,
            .resultRIGHTRIGHT_REAR = 0,
            .resultLEFTLEFT_REAR = 0,
            .resultLEFT_SIDE = 0,
            .resultRIGHTRIGHT_SIDE = 0,
            .resultRIGHT_SIDE = 0,
            .resultLEFTLEFT_SIDE = 0,
            .resultINSTRUMENT_CLUSTER = 1.0,
            .resultINFOTAINMENT = 0,
            .resultHUD = 0},
        DataFor_TopDownRequest_Update{
            .isInit = false,
            .jamVelocity = false,
            .direction = LateralAction::Direction::RIGHT,
            .state = LateralAction::State::LANE_CHANGING,
            .resultEGO_FRONT_FAR = 1.0,
            .resultRIGHT_FRONT_FAR = 0,
            .resultLEFT_FRONT_FAR = 0,
            .resultEGO_FRONT = 1.0,
            .resultRIGHT_FRONT = 0,
            .resultLEFT_FRONT = 0,
            .resultRIGHTRIGHT_FRONT = 0,
            .resultLEFTLEFT_FRONT = 0,
            .resultEGO_REAR = 1.0,
            .resultRIGHT_REAR = 0,
            .resultLEFT_REAR = 0,
            .resultRIGHTRIGHT_REAR = 0,
            .resultLEFTLEFT_REAR = 0,
            .resultLEFT_SIDE = 0,
            .resultRIGHTRIGHT_SIDE = 0,
            .resultRIGHT_SIDE = 0,
            .resultLEFTLEFT_SIDE = 0,
            .resultINSTRUMENT_CLUSTER = 1.0,
            .resultINFOTAINMENT = 0,
            .resultHUD = 0},
        DataFor_TopDownRequest_Update{
            .isInit = false,
            .jamVelocity = false,
            .direction = LateralAction::Direction::LEFT,
            .state = LateralAction::State::LANE_CHANGING,
            .resultEGO_FRONT_FAR = 1.0,
            .resultRIGHT_FRONT_FAR = 0,
            .resultLEFT_FRONT_FAR = 0,
            .resultEGO_FRONT = 1.0,
            .resultRIGHT_FRONT = 0,
            .resultLEFT_FRONT = 0,
            .resultRIGHTRIGHT_FRONT = 0,
            .resultLEFTLEFT_FRONT = 0,
            .resultEGO_REAR = 1.0,
            .resultRIGHT_REAR = 0,
            .resultLEFT_REAR = 0,
            .resultRIGHTRIGHT_REAR = 0,
            .resultLEFTLEFT_REAR = 0,
            .resultLEFT_SIDE = 0,
            .resultRIGHTRIGHT_SIDE = 0,
            .resultRIGHT_SIDE = 0,
            .resultLEFTLEFT_SIDE = 0,
            .resultINSTRUMENT_CLUSTER = 1.0,
            .resultINFOTAINMENT = 0,
            .resultHUD = 0}));

/****************************************************************
 * CHECK TopDownRequest ChangeIntensitiesToAvoidImpossibilities *
 ****************************************************************/

struct DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities
{
  RelativeLane relativLane;
  bool laneExistence;
  bool hudExistence;
  bool interiorExistence;
  double resultSACCADE;
  double resultEGO_FRONT_FAR;
  double resultRIGHT_FRONT_FAR;
  double resultLEFT_FRONT_FAR;
  double resultEGO_FRONT;
  double resultRIGHT_FRONT;
  double resultLEFT_FRONT;
  double resultRIGHTRIGHT_FRONT;
  double resultLEFTLEFT_FRONT;
  double resultEGO_REAR;
  double resultRIGHT_REAR;
  double resultLEFT_REAR;
  double resultRIGHTRIGHT_REAR;
  double resultLEFTLEFT_REAR;
  double resultLEFT_SIDE;
  double resultRIGHTRIGHT_SIDE;
  double resultRIGHT_SIDE;
  double resultLEFTLEFT_SIDE;
  double resultINSTRUMENT_CLUSTER;
  double resultINFOTAINMENT;
  double resultHUD;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities& obj)
  {
    return os
           << " relativLane (Relativlane): " << ScmCommons::to_string(obj.relativLane)
           << " laneExistence (bool): " << obj.laneExistence
           << " hudExistence (bool): " << obj.hudExistence
           << " interiorExistence (bool): " << obj.interiorExistence
           << " resultSACCADE (double) : " << obj.resultSACCADE
           << " resultEGO_FRONT_FAR (double) : " << obj.resultEGO_FRONT_FAR
           << " resultRIGHT_FRONT_FAR (double) : " << obj.resultRIGHT_FRONT_FAR
           << " resultLEFT_FRONT_FAR(double) : " << obj.resultLEFT_FRONT_FAR
           << " resultEGO_FRONT (double) : " << obj.resultEGO_FRONT
           << " resultRIGHT_FRONT (double) : " << obj.resultRIGHT_FRONT
           << " resultLEFT_FRONT (double) : " << obj.resultLEFT_FRONT
           << " resultRIGHTRIGHT_FRONT (double) : " << obj.resultRIGHTRIGHT_FRONT
           << " resultLEFTLEFT_FRONT (double) : " << obj.resultLEFTLEFT_FRONT
           << " resultEGO_REAR (double) : " << obj.resultEGO_REAR
           << " resultRIGHT_REAR(double) : " << obj.resultRIGHT_REAR
           << " resultLEFT_REAR(double): " << obj.resultLEFT_REAR
           << " resultRIGHTRIGHT_REAR (double) : " << obj.resultRIGHTRIGHT_REAR
           << " resultLEFTLEFT_REAR (double) : " << obj.resultLEFTLEFT_REAR
           << " resultLEFT_SIDE (double) : " << obj.resultLEFT_SIDE
           << " resultRIGHTRIGHT_SIDE (double): " << obj.resultRIGHTRIGHT_SIDE
           << " resultRIGHT_SIDE (double): " << obj.resultRIGHT_SIDE
           << " resultLEFTLEFT_SIDE (double): " << obj.resultLEFTLEFT_SIDE
           << " resultINSTRUMENT_CLUSTER (double): " << obj.resultINSTRUMENT_CLUSTER
           << " resultINFOTAINMENT (double): " << obj.resultINFOTAINMENT
           << " resultHUD (double): " << obj.resultHUD;
  }
};
class TopDownRequest_ChangeIntensitiesToAvoidImpossibilities : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities>
{
};

TEST_P(TopDownRequest_ChangeIntensitiesToAvoidImpossibilities, ChangeIntensitiesToAvoidImpossibilities)
{
  DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities data = GetParam();
  TopDownRequestTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(data.relativLane, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInteriorHudExistence()).WillByDefault(Return(data.hudExistence));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInteriorInfotainmentExistence()).WillByDefault(Return(data.interiorExistence));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));

  std::map<GazeState, double> intensities;
  intensities[GazeState::SACCADE] = 1;
  intensities[GazeState::EGO_FRONT_FAR] = 2;
  intensities[GazeState::RIGHT_FRONT_FAR] = 3;
  intensities[GazeState::LEFT_FRONT_FAR] = 4;
  intensities[GazeState::EGO_FRONT] = 5;
  intensities[GazeState::RIGHT_FRONT] = 6;
  intensities[GazeState::LEFT_FRONT] = 7;
  intensities[GazeState::RIGHTRIGHT_FRONT] = 8;
  intensities[GazeState::LEFTLEFT_FRONT] = 9;
  intensities[GazeState::EGO_REAR] = 10;
  intensities[GazeState::RIGHT_REAR] = 11;
  intensities[GazeState::LEFT_REAR] = 12;
  intensities[GazeState::RIGHTRIGHT_REAR] = 13;
  intensities[GazeState::LEFTLEFT_REAR] = 14;
  intensities[GazeState::LEFT_SIDE] = 15;
  intensities[GazeState::RIGHTRIGHT_SIDE] = 16;
  intensities[GazeState::RIGHT_SIDE] = 17;
  intensities[GazeState::LEFTLEFT_SIDE] = 18;
  intensities[GazeState::INSTRUMENT_CLUSTER] = 19;
  intensities[GazeState::INFOTAINMENT] = 20;
  intensities[GazeState::HUD] = 21;

  auto result = TEST_HELPER.topDownRequest.ChangeIntensitiesToAvoidImpossibilities(intensities);

  ASSERT_EQ(result[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::SACCADE], data.resultSACCADE);
  ASSERT_EQ(result[GazeState::EGO_FRONT_FAR], data.resultEGO_FRONT_FAR);
  ASSERT_EQ(result[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::LEFT_FRONT_FAR], data.resultLEFT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::EGO_FRONT], data.resultEGO_FRONT);
  ASSERT_EQ(result[GazeState::RIGHT_FRONT], data.resultRIGHT_FRONT);
  ASSERT_EQ(result[GazeState::LEFT_FRONT], data.resultLEFT_FRONT);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_FRONT], data.resultRIGHTRIGHT_FRONT);
  ASSERT_EQ(result[GazeState::LEFTLEFT_FRONT], data.resultLEFTLEFT_FRONT);
  ASSERT_EQ(result[GazeState::EGO_REAR], data.resultEGO_REAR);
  ASSERT_EQ(result[GazeState::RIGHT_REAR], data.resultRIGHT_REAR);
  ASSERT_EQ(result[GazeState::LEFT_REAR], data.resultLEFT_REAR);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_REAR], data.resultRIGHTRIGHT_REAR);
  ASSERT_EQ(result[GazeState::LEFTLEFT_REAR], data.resultLEFTLEFT_REAR);
  ASSERT_EQ(result[GazeState::LEFT_SIDE], data.resultLEFT_SIDE);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_SIDE], data.resultRIGHTRIGHT_SIDE);
  ASSERT_EQ(result[GazeState::RIGHT_SIDE], data.resultRIGHT_SIDE);
  ASSERT_EQ(result[GazeState::LEFTLEFT_SIDE], data.resultLEFTLEFT_SIDE);
  ASSERT_EQ(result[GazeState::INSTRUMENT_CLUSTER], data.resultINSTRUMENT_CLUSTER);
  ASSERT_EQ(result[GazeState::INFOTAINMENT], data.resultINFOTAINMENT);
  ASSERT_EQ(result[GazeState::HUD], data.resultHUD);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TopDownRequest_ChangeIntensitiesToAvoidImpossibilities,
    testing::Values(
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::RIGHT,
            .laneExistence = false,
            .hudExistence = true,
            .interiorExistence = true,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 0,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 0,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 0,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 0,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 21},
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::LEFT,
            .laneExistence = false,
            .hudExistence = true,
            .interiorExistence = true,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 0,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 0,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 0,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 0,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 21},
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::RIGHTRIGHT,
            .laneExistence = false,
            .hudExistence = true,
            .interiorExistence = true,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 0,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 0,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 0,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 21},
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::LEFTLEFT,
            .laneExistence = false,
            .hudExistence = true,
            .interiorExistence = true,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 0,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 0,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 0,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 21},
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::EGO,
            .laneExistence = false,
            .hudExistence = false,
            .interiorExistence = true,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 0},
        DataFor_TopDownRequest_ChangeIntensitiesToAvoidImpossibilities{
            .relativLane = RelativeLane::EGO,
            .laneExistence = false,
            .hudExistence = true,
            .interiorExistence = false,
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 0,
            .resultHUD = 21}));

/****************************************************************
 * CHECK TopDownRequest ChangeIntensitiesForInformationRequests *
 ****************************************************************/

struct DataFor_ChangeIntensitiesForInformationRequests
{
  double resultSACCADE;
  double resultEGO_FRONT_FAR;
  double resultRIGHT_FRONT_FAR;
  double resultLEFT_FRONT_FAR;
  double resultEGO_FRONT;
  double resultRIGHT_FRONT;
  double resultLEFT_FRONT;
  double resultRIGHTRIGHT_FRONT;
  double resultLEFTLEFT_FRONT;
  double resultEGO_REAR;
  double resultRIGHT_REAR;
  double resultLEFT_REAR;
  double resultRIGHTRIGHT_REAR;
  double resultLEFTLEFT_REAR;
  double resultLEFT_SIDE;
  double resultRIGHTRIGHT_SIDE;
  double resultRIGHT_SIDE;
  double resultLEFTLEFT_SIDE;
  double resultINSTRUMENT_CLUSTER;
  double resultINFOTAINMENT;
  double resultHUD;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChangeIntensitiesForInformationRequests& obj)
  {
    return os
           << " resultSACCADE (double) : " << obj.resultSACCADE
           << " resultEGO_FRONT_FAR (double) : " << obj.resultEGO_FRONT_FAR
           << " resultRIGHT_FRONT_FAR (double) : " << obj.resultRIGHT_FRONT_FAR
           << " resultLEFT_FRONT_FAR(double) : " << obj.resultLEFT_FRONT_FAR
           << " resultEGO_FRONT (double) : " << obj.resultEGO_FRONT
           << " resultRIGHT_FRONT (double) : " << obj.resultRIGHT_FRONT
           << " resultLEFT_FRONT (double) : " << obj.resultLEFT_FRONT
           << " resultRIGHTRIGHT_FRONT (double) : " << obj.resultRIGHTRIGHT_FRONT
           << " resultLEFTLEFT_FRONT (double) : " << obj.resultLEFTLEFT_FRONT
           << " resultEGO_REAR (double) : " << obj.resultEGO_REAR
           << " resultRIGHT_REAR(double) : " << obj.resultRIGHT_REAR
           << " resultLEFT_REAR(double): " << obj.resultLEFT_REAR
           << " resultRIGHTRIGHT_REAR (double) : " << obj.resultRIGHTRIGHT_REAR
           << " resultLEFTLEFT_REAR (double) : " << obj.resultLEFTLEFT_REAR
           << " resultLEFT_SIDE (double) : " << obj.resultLEFT_SIDE
           << " resultRIGHTRIGHT_SIDE (double): " << obj.resultRIGHTRIGHT_SIDE
           << " resultRIGHT_SIDE (double): " << obj.resultRIGHT_SIDE
           << " resultLEFTLEFT_SIDE (double): " << obj.resultLEFTLEFT_SIDE
           << " resultINSTRUMENT_CLUSTER (double): " << obj.resultINSTRUMENT_CLUSTER
           << " resultINFOTAINMENT (double): " << obj.resultINFOTAINMENT
           << " resultHUD (double): " << obj.resultHUD;
  }
};
class TopDownRequest_ChangeIntensitiesForInformationRequests : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_ChangeIntensitiesForInformationRequests>
{
};

TEST_P(TopDownRequest_ChangeIntensitiesForInformationRequests, ChangeIntensitiesForInformationRequests)
{
  DataFor_ChangeIntensitiesForInformationRequests data = GetParam();
  TopDownRequestTester TEST_HELPER;

  std::map<AreaOfInterest, double> topDownRequestMap;
  topDownRequestMap[AreaOfInterest::EGO_FRONT_FAR] = 1;
  topDownRequestMap[AreaOfInterest::RIGHT_FRONT_FAR] = 2;
  topDownRequestMap[AreaOfInterest::LEFT_FRONT_FAR] = 3;
  topDownRequestMap[AreaOfInterest::EGO_FRONT] = 4;
  topDownRequestMap[AreaOfInterest::RIGHT_FRONT] = 5;
  topDownRequestMap[AreaOfInterest::LEFT_FRONT] = 6;
  topDownRequestMap[AreaOfInterest::RIGHTRIGHT_FRONT] = 7;
  topDownRequestMap[AreaOfInterest::LEFTLEFT_FRONT] = 8;
  topDownRequestMap[AreaOfInterest::EGO_REAR] = 9;
  topDownRequestMap[AreaOfInterest::RIGHT_REAR] = 10;
  topDownRequestMap[AreaOfInterest::LEFT_REAR] = 11;
  topDownRequestMap[AreaOfInterest::RIGHTRIGHT_REAR] = 12;
  topDownRequestMap[AreaOfInterest::LEFTLEFT_REAR] = 13;
  topDownRequestMap[AreaOfInterest::LEFT_SIDE] = 14;
  topDownRequestMap[AreaOfInterest::RIGHTRIGHT_SIDE] = 15;
  topDownRequestMap[AreaOfInterest::RIGHT_SIDE] = 16;
  topDownRequestMap[AreaOfInterest::LEFTLEFT_SIDE] = 17;
  topDownRequestMap[AreaOfInterest::INSTRUMENT_CLUSTER] = 18;
  topDownRequestMap[AreaOfInterest::INFOTAINMENT] = 19;
  topDownRequestMap[AreaOfInterest::HUD] = 20;

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::SACCADE] = data.resultSACCADE;
  gazeStateIntensities[GazeState::EGO_FRONT_FAR] = 2;
  gazeStateIntensities[GazeState::RIGHT_FRONT_FAR] = 3;
  gazeStateIntensities[GazeState::LEFT_FRONT_FAR] = 4;
  gazeStateIntensities[GazeState::EGO_FRONT] = 5;
  gazeStateIntensities[GazeState::RIGHT_FRONT] = 6;
  gazeStateIntensities[GazeState::LEFT_FRONT] = 7;
  gazeStateIntensities[GazeState::RIGHTRIGHT_FRONT] = 8;
  gazeStateIntensities[GazeState::LEFTLEFT_FRONT] = 9;
  gazeStateIntensities[GazeState::EGO_REAR] = 10;
  gazeStateIntensities[GazeState::RIGHT_REAR] = 11;
  gazeStateIntensities[GazeState::LEFT_REAR] = 12;
  gazeStateIntensities[GazeState::RIGHTRIGHT_REAR] = 13;
  gazeStateIntensities[GazeState::LEFTLEFT_REAR] = 14;
  gazeStateIntensities[GazeState::LEFT_SIDE] = 15;
  gazeStateIntensities[GazeState::RIGHTRIGHT_SIDE] = 16;
  gazeStateIntensities[GazeState::RIGHT_SIDE] = 17;
  gazeStateIntensities[GazeState::LEFTLEFT_SIDE] = 18;
  gazeStateIntensities[GazeState::INSTRUMENT_CLUSTER] = 19;
  gazeStateIntensities[GazeState::INFOTAINMENT] = 20;
  gazeStateIntensities[GazeState::HUD] = 21;

  auto result = TEST_HELPER.topDownRequest.ChangeIntensitiesForInformationRequests(topDownRequestMap, gazeStateIntensities);

  ASSERT_EQ(result[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::SACCADE], data.resultSACCADE);
  ASSERT_EQ(result[GazeState::EGO_FRONT_FAR], data.resultEGO_FRONT_FAR);
  ASSERT_EQ(result[GazeState::RIGHT_FRONT_FAR], data.resultRIGHT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::LEFT_FRONT_FAR], data.resultLEFT_FRONT_FAR);
  ASSERT_EQ(result[GazeState::EGO_FRONT], data.resultEGO_FRONT);
  ASSERT_EQ(result[GazeState::RIGHT_FRONT], data.resultRIGHT_FRONT);
  ASSERT_EQ(result[GazeState::LEFT_FRONT], data.resultLEFT_FRONT);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_FRONT], data.resultRIGHTRIGHT_FRONT);
  ASSERT_EQ(result[GazeState::LEFTLEFT_FRONT], data.resultLEFTLEFT_FRONT);
  ASSERT_EQ(result[GazeState::EGO_REAR], data.resultEGO_REAR);
  ASSERT_EQ(result[GazeState::RIGHT_REAR], data.resultRIGHT_REAR);
  ASSERT_EQ(result[GazeState::LEFT_REAR], data.resultLEFT_REAR);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_REAR], data.resultRIGHTRIGHT_REAR);
  ASSERT_EQ(result[GazeState::LEFTLEFT_REAR], data.resultLEFTLEFT_REAR);
  ASSERT_EQ(result[GazeState::LEFT_SIDE], data.resultLEFT_SIDE);
  ASSERT_EQ(result[GazeState::RIGHTRIGHT_SIDE], data.resultRIGHTRIGHT_SIDE);
  ASSERT_EQ(result[GazeState::RIGHT_SIDE], data.resultRIGHT_SIDE);
  ASSERT_EQ(result[GazeState::LEFTLEFT_SIDE], data.resultLEFTLEFT_SIDE);
  ASSERT_EQ(result[GazeState::INSTRUMENT_CLUSTER], data.resultINSTRUMENT_CLUSTER);
  ASSERT_EQ(result[GazeState::INFOTAINMENT], data.resultINFOTAINMENT);
  ASSERT_EQ(result[GazeState::HUD], data.resultHUD);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TopDownRequest_ChangeIntensitiesForInformationRequests,
    testing::Values(
        DataFor_ChangeIntensitiesForInformationRequests{
            .resultSACCADE = 1,
            .resultEGO_FRONT_FAR = 2,
            .resultRIGHT_FRONT_FAR = 3,
            .resultLEFT_FRONT_FAR = 4,
            .resultEGO_FRONT = 5,
            .resultRIGHT_FRONT = 6,
            .resultLEFT_FRONT = 7,
            .resultRIGHTRIGHT_FRONT = 8,
            .resultLEFTLEFT_FRONT = 9,
            .resultEGO_REAR = 10,
            .resultRIGHT_REAR = 11,
            .resultLEFT_REAR = 12,
            .resultRIGHTRIGHT_REAR = 13,
            .resultLEFTLEFT_REAR = 14,
            .resultLEFT_SIDE = 15,
            .resultRIGHTRIGHT_SIDE = 16,
            .resultRIGHT_SIDE = 17,
            .resultLEFTLEFT_SIDE = 18,
            .resultINSTRUMENT_CLUSTER = 19,
            .resultINFOTAINMENT = 20,
            .resultHUD = 21},
        DataFor_ChangeIntensitiesForInformationRequests{
            .resultSACCADE = 0,
            .resultEGO_FRONT_FAR = 1,
            .resultRIGHT_FRONT_FAR = 2,
            .resultLEFT_FRONT_FAR = 3,
            .resultEGO_FRONT = 4,
            .resultRIGHT_FRONT = 5,
            .resultLEFT_FRONT = 6,
            .resultRIGHTRIGHT_FRONT = 7,
            .resultLEFTLEFT_FRONT = 8,
            .resultEGO_REAR = 9,
            .resultRIGHT_REAR = 10,
            .resultLEFT_REAR = 11,
            .resultRIGHTRIGHT_REAR = 12,
            .resultLEFTLEFT_REAR = 13,
            .resultLEFT_SIDE = 14,
            .resultRIGHTRIGHT_SIDE = 15,
            .resultRIGHT_SIDE = 16,
            .resultLEFTLEFT_SIDE = 17,
            .resultINSTRUMENT_CLUSTER = 18,
            .resultINFOTAINMENT = 19,
            .resultHUD = 20}));

/***************************************************************
 * CHECK GazePeriphery ChangeIntensitiesForVehiclesOvertaking *
 ***************************************************************/

struct DataFor_ChangeIntensitiesForVehiclesOvertaking
{
  AreaOfInterest aoi;
  double leftFront;
  double leftSide;
  double rightFront;
  double rightSide;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChangeIntensitiesForVehiclesOvertaking& obj)
  {
    return os
           << " aoi (AreaOfInterest) : " << ScmCommons::AreaOfInterestToString(obj.aoi)
           << " leftFront (double) : " << obj.leftFront
           << " leftSide (double) : " << obj.leftSide
           << " rightFront (double) : " << obj.rightFront
           << " rightSide (double) : " << obj.rightSide;
  }
};
class TopDownRequest_ChangeIntensitiesForVehiclesOvertaking : public ::testing::Test,
                                                              public ::testing::WithParamInterface<DataFor_ChangeIntensitiesForVehiclesOvertaking>
{
};

TEST_P(TopDownRequest_ChangeIntensitiesForVehiclesOvertaking, ChangeIntensitiesForVehiclesOvertaking)
{
  DataFor_ChangeIntensitiesForVehiclesOvertaking data = GetParam();
  TopDownRequestTester TEST_HELPER;

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::LEFT_FRONT] = 0;
  gazeStateIntensities[GazeState::LEFT_SIDE] = 0;
  gazeStateIntensities[GazeState::RIGHT_FRONT] = 0;
  gazeStateIntensities[GazeState::RIGHT_SIDE] = 0;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.aoi, _)).WillByDefault(Return(true));
  // ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(data.aoi, _)).WillByDefault(Return(0.0_s));

  auto result = TEST_HELPER.topDownRequest.ChangeIntensitiesForVehiclesOvertaking(gazeStateIntensities);

  ASSERT_EQ(result[GazeState::LEFT_FRONT], data.leftFront);
  ASSERT_EQ(result[GazeState::LEFT_SIDE], data.leftSide);
  ASSERT_EQ(result[GazeState::RIGHT_FRONT], data.rightFront);
  ASSERT_EQ(result[GazeState::RIGHT_SIDE], data.rightSide);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TopDownRequest_ChangeIntensitiesForVehiclesOvertaking,
    testing::Values(
        DataFor_ChangeIntensitiesForVehiclesOvertaking{
            .aoi = AreaOfInterest::LEFT_REAR,
            .leftFront = 0.5,
            .leftSide = 1.0,
            .rightFront = 0.0,
            .rightSide = 0.0},
        DataFor_ChangeIntensitiesForVehiclesOvertaking{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .leftFront = 0.0,
            .leftSide = 0.0,
            .rightFront = 0.5,
            .rightSide = 1.0}));
