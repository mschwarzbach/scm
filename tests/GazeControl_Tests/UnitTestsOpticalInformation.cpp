/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <vector>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/OpticalInformation.h"
#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct OpticalInformationTester
{
  class OpticalInformationUnderTest : OpticalInformation
  {
  public:
    template <typename... Args>
    OpticalInformationUnderTest(Args&&... args)
        : OpticalInformation{std::forward<Args>(args)...} {};

    using OpticalInformation::AdjustCurrentObjectOpticalAngleAndThresholdLooming;
    using OpticalInformation::AdjustOpticalAnglePositionHorizontal;
    using OpticalInformation::AdjustOpticalAngleSizeHorizontal;
    using OpticalInformation::AnalyzeOrientationsRelativeToDriver;
    using OpticalInformation::ApplyHemicycleChanges;
    using OpticalInformation::CalculateOpticalInformationForSurroundingObject;
    using OpticalInformation::CorrectOpticalAnglePositionForMirror;
    using OpticalInformation::SupplementOpticalInformation;
  };

  OpticalInformationTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        opticalInformation(fakeMentalModel)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  OpticalInformationUnderTest opticalInformation;
};

/*********************************************************
 * CHECK OpticalInformation SupplementOpticalInformation *
 *********************************************************/

TEST(OpticalInformation_SupplementOpticalInformation, SupplementOpticalInformation)
{
  OpticalInformationTester TEST_HELPER;

  ObjectInformationSCM obinfo;
  obinfo.id = 1;
  obinfo.orientationRelativeToDriver.frontLeft = 10.0_rad;
  obinfo.orientationRelativeToDriver.frontRight = -10.0_rad;
  obinfo.orientationRelativeToDriver.rearLeft = 20.0_rad;
  obinfo.orientationRelativeToDriver.rearRight = -20.0_rad;

  SurroundingObjectsSCM surroundingObjects;

  surroundingObjects.ongoingTraffic.Ahead().Close().push_back(obinfo);

  auto result = TEST_HELPER.opticalInformation.SupplementOpticalInformation(surroundingObjects, 10.0_rad, false, false);
  ASSERT_EQ(result.gazeAngleHorizontal, 10.0_rad);
}

/****************************************************************************
 * CHECK OpticalInformation CalculateOpticalInformationForSurroundingObject *
 ****************************************************************************/
/// \brief Data table for definition of individual test cases
struct DataFor_CalculateOpticalInformationForSurroundingObject
{
  AreaOfInterest aoi;
  units::angle::radian_t result_opticalAnglePositionHorizontal;
  units::angle::radian_t result_opticalAngleSizeHorizontal;
};
class OpticalInformation_CalculateOpticalInformationForSurroundingObject : public ::testing::Test,
                                                                           public ::testing::WithParamInterface<DataFor_CalculateOpticalInformationForSurroundingObject>
{
};

TEST_P(OpticalInformation_CalculateOpticalInformationForSurroundingObject, CalculateOpticalInformationForSurroundingObject)
{
  DataFor_CalculateOpticalInformationForSurroundingObject data = GetParam();
  OpticalInformationTester TEST_HELPER;

  std::vector<units::angle::radian_t> orientationsRelativeToDriver{
      10.0_rad,
      11.0_rad,
      12.0_rad,
      13.0_rad,
  };

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.InteriorMirrorOrientationRelativeToDriver = 10.0_rad;
  mockInfo.LeftMirrorOrientationRelativeToDriver = 11.0_rad;
  mockInfo.RightMirrorOrientationRelativeToDriver = 12.0_rad;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  auto result = TEST_HELPER.opticalInformation.CalculateOpticalInformationForSurroundingObject(false, orientationsRelativeToDriver, data.aoi, 10_rad, false);

  ASSERT_EQ(result.opticalAnglePositionHorizontal, data.result_opticalAnglePositionHorizontal);
  ASSERT_NEAR(result.opticalAngleSizeHorizontal.value(), data.result_opticalAngleSizeHorizontal.value(), 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_CalculateOpticalInformationForSurroundingObject,
    testing::Values(
        DataFor_CalculateOpticalInformationForSurroundingObject{
            .aoi = AreaOfInterest::EGO_REAR,
            .result_opticalAnglePositionHorizontal = 0.0_rad,
            .result_opticalAngleSizeHorizontal = 3.0_rad},
        DataFor_CalculateOpticalInformationForSurroundingObject{
            .aoi = AreaOfInterest::EGO_FRONT,
            .result_opticalAnglePositionHorizontal = 0.0_rad,
            .result_opticalAngleSizeHorizontal = 1.83_rad}));

/*******************************************************************************
 * CHECK OpticalInformation AdjustCurrentObjectOpticalAngleAndThresholdLooming *
 ******************************************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AdjustCurrentObjectOpticalAngleAndThresholdLooming
{
  double thresholdLoomingFovea;
  bool isFoveaOrSpawn;
  units::angle::radian_t result_opticalAnglePositionHorizontal;
  double result_thresholdLooming;
  units::angle::radian_t result_opticalAngleSizeHorizontal;
};

class OpticalInformation_AdjustCurrentObjectOpticalAngleAndThresholdLooming : public ::testing::Test,
                                                                              public ::testing::WithParamInterface<DataFor_AdjustCurrentObjectOpticalAngleAndThresholdLooming>
{
};

TEST_P(OpticalInformation_AdjustCurrentObjectOpticalAngleAndThresholdLooming, AdjustCurrentObjectOpticalAngleAndThresholdLooming)
{
  DataFor_AdjustCurrentObjectOpticalAngleAndThresholdLooming data = GetParam();
  OpticalInformationTester TEST_HELPER;

  OpticalAngle opticalAngle{};
  opticalAngle.opticalAnglePositionHorizontal = 10.0_rad;
  opticalAngle.opticalAngleSizeHorizontal = 5.0_rad;

  ObjectInformationSCM objectInformationSCM{};

  DriverParameters driverParameters{};
  driverParameters.thresholdLoomingFovea = data.thresholdLoomingFovea;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  auto result = TEST_HELPER.opticalInformation.AdjustCurrentObjectOpticalAngleAndThresholdLooming(data.isFoveaOrSpawn, objectInformationSCM, opticalAngle);

  ASSERT_EQ(result.opticalAnglePositionHorizontal, data.result_opticalAnglePositionHorizontal);
  ASSERT_NEAR(result.thresholdLooming, data.result_thresholdLooming, 0.01);
  ASSERT_EQ(result.opticalAngleSizeHorizontal, data.result_opticalAngleSizeHorizontal);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_AdjustCurrentObjectOpticalAngleAndThresholdLooming,
    testing::Values(
        DataFor_AdjustCurrentObjectOpticalAngleAndThresholdLooming{
            .thresholdLoomingFovea = 2.0,
            .isFoveaOrSpawn = true,
            .result_opticalAnglePositionHorizontal = 0.0_rad,
            .result_thresholdLooming = 2.0,
            .result_opticalAngleSizeHorizontal = 5.0_rad},
        DataFor_AdjustCurrentObjectOpticalAngleAndThresholdLooming{
            .thresholdLoomingFovea = 3.0,
            .isFoveaOrSpawn = false,
            .result_opticalAnglePositionHorizontal = 10.0_rad,
            .result_thresholdLooming = 3.0 * ScmCommons::CalculateCorticalMagnificationFactor(10.0_rad),
            .result_opticalAngleSizeHorizontal = 5.0_rad}));

/****************************************************************
 * CHECK OpticalInformation AnalyzeOrientationsRelativeToDriver *
 ****************************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AnalyzeOrientationsRelativeToDriver
{
  bool evaluateMirror;
  units::angle::radian_t result_minimumOrientationRelativToViewAxis;
  units::angle::radian_t result_maximumOrientationRelativToViewAxis;
  units::angle::radian_t result_opticalAnglePositionHorizontal;
};

class OpticalInformation_AnalyzeOrientationsRelativeToDriver : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_AnalyzeOrientationsRelativeToDriver>
{
};

TEST_P(OpticalInformation_AnalyzeOrientationsRelativeToDriver, AnalyzeOrientationsRelativeToDriver)
{
  DataFor_AnalyzeOrientationsRelativeToDriver data = GetParam();
  OpticalInformationTester TEST_HELPER;

  GazeOrientations gazeOrientations{};
  gazeOrientations.evaluateMirror = data.evaluateMirror;
  gazeOrientations.opticalAnglePositionHorizontal = 20.0_rad;
  gazeOrientations.minimumOrientationRelativToViewAxis = 5.0_rad;
  gazeOrientations.maximumOrientationRelativToViewAxis = 15.0_rad;

  std::vector<units::angle::radian_t> orientationsRelativeToDriver{
      10.0_rad,
      11.0_rad,
      12.0_rad,
      13.0_rad,
  };

  auto result = TEST_HELPER.opticalInformation.AnalyzeOrientationsRelativeToDriver(false, orientationsRelativeToDriver, 10.0_rad, gazeOrientations, false);

  ASSERT_NEAR(result.minimumOrientationRelativToViewAxis.value(), data.result_minimumOrientationRelativToViewAxis.value(), 0.01);
  ASSERT_EQ(result.maximumOrientationRelativToViewAxis, data.result_maximumOrientationRelativToViewAxis);
  ASSERT_NEAR(result.opticalAnglePositionHorizontal.value(), data.result_opticalAnglePositionHorizontal.value(), 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_AnalyzeOrientationsRelativeToDriver,
    testing::Values(
        DataFor_AnalyzeOrientationsRelativeToDriver{
            .evaluateMirror = false,
            .result_minimumOrientationRelativToViewAxis = 0.0_rad,
            .result_maximumOrientationRelativToViewAxis = 15.0_rad,
            .result_opticalAnglePositionHorizontal = 0.0_rad},
        DataFor_AnalyzeOrientationsRelativeToDriver{
            .evaluateMirror = true,
            .result_minimumOrientationRelativToViewAxis = 0.57_rad,
            .result_maximumOrientationRelativToViewAxis = 15.0_rad,
            .result_opticalAnglePositionHorizontal = 0.57_rad}));

/*************************************************************
 * CHECK OpticalInformation AdjustOpticalAngleSizeHorizontal *
 *************************************************************/

TEST(OpticalInformation_AdjustOpticalAngleSizeHorizontal, AdjustOpticalAngleSizeHorizontal)
{
  OpticalInformationTester TEST_HELPER;

  GazeOrientations gazeOrientations{};
  gazeOrientations.maximumOrientationRelativToViewAxis = 10.0_rad;
  gazeOrientations.minimumOrientationRelativToViewAxis = 5.0_rad;

  auto result = TEST_HELPER.opticalInformation.AdjustOpticalAngleSizeHorizontal(gazeOrientations);

  ASSERT_EQ(result, 5_rad);
}

/*****************************************************************
 * CHECK OpticalInformation AdjustOpticalAnglePositionHorizontal *
 *****************************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AdjustOpticalAnglePositionHorizontal
{
  units::angle::radian_t maximumOrientationRelativToViewAxis;
  units::angle::radian_t minimumOrientationRelativToViewAxis;
  units::angle::radian_t opticalAnglePositionHorizontal;
  units::angle::radian_t result;
};

class OpticalInformation_AdjustOpticalAnglePositionHorizontal : public ::testing::Test,
                                                                public ::testing::WithParamInterface<DataFor_AdjustOpticalAnglePositionHorizontal>
{
};

TEST_P(OpticalInformation_AdjustOpticalAnglePositionHorizontal, AdjustOpticalAnglePositionHorizontal)
{
  DataFor_AdjustOpticalAnglePositionHorizontal data = GetParam();
  OpticalInformationTester TEST_HELPER;
  GazeOrientations gazeOrientations{};
  gazeOrientations.maximumOrientationRelativToViewAxis = data.maximumOrientationRelativToViewAxis;
  gazeOrientations.minimumOrientationRelativToViewAxis = data.minimumOrientationRelativToViewAxis;
  gazeOrientations.opticalAnglePositionHorizontal = data.opticalAnglePositionHorizontal;

  auto result = TEST_HELPER.opticalInformation.AdjustOpticalAnglePositionHorizontal(gazeOrientations);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_AdjustOpticalAnglePositionHorizontal,
    testing::Values(
        DataFor_AdjustOpticalAnglePositionHorizontal{
            .maximumOrientationRelativToViewAxis = 1.0_rad,
            .minimumOrientationRelativToViewAxis = -1.0_rad,
            .opticalAnglePositionHorizontal = 10.0_rad,
            .result = 0.0_rad},
        DataFor_AdjustOpticalAnglePositionHorizontal{
            .maximumOrientationRelativToViewAxis = -1.0_rad,
            .minimumOrientationRelativToViewAxis = 1.0_rad,
            .opticalAnglePositionHorizontal = 10.0_rad,
            .result = 10.0_rad}));

/*****************************************************************
 * CHECK OpticalInformation CorrectOpticalAnglePositionForMirror *
 *****************************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_CorrectOpticalAnglePositionForMirror
{
  AreaOfInterest aoi;
  units::angle::radian_t result;
};

class OpticalInformation_CorrectOpticalAnglePositionForMirror : public ::testing::Test,
                                                                public ::testing::WithParamInterface<DataFor_CorrectOpticalAnglePositionForMirror>
{
};

TEST_P(OpticalInformation_CorrectOpticalAnglePositionForMirror, CorrectOpticalAnglePositionForMirror)
{
  DataFor_CorrectOpticalAnglePositionForMirror data = GetParam();
  OpticalInformationTester TEST_HELPER;

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.InteriorMirrorOrientationRelativeToDriver = 10.0_rad;
  mockInfo.LeftMirrorOrientationRelativeToDriver = 11.0_rad;
  mockInfo.RightMirrorOrientationRelativeToDriver = 12.0_rad;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  auto result = TEST_HELPER.opticalInformation.CorrectOpticalAnglePositionForMirror(data.aoi, 10.0_rad, 10.0_rad);

  ASSERT_EQ(data.result, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_CorrectOpticalAnglePositionForMirror,
    testing::Values(
        DataFor_CorrectOpticalAnglePositionForMirror{
            .aoi = AreaOfInterest::EGO_REAR,
            .result = 0.0_rad},
        DataFor_CorrectOpticalAnglePositionForMirror{
            .aoi = AreaOfInterest::LEFT_REAR,
            .result = 1.0_rad},
        DataFor_CorrectOpticalAnglePositionForMirror{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .result = 2.0_rad},
        DataFor_CorrectOpticalAnglePositionForMirror{
            .aoi = AreaOfInterest::EGO_FRONT,
            .result = 10.0_rad}));

/*************************************************
 * CHECK OpticalInformation ApplyHemicycleChange *
 *************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_OpticalInformation_ApplyHemicycleChanges
{
  units::angle::radian_t orientationRelativToViewAxis;
  units::angle::radian_t result;
};

class OpticalInformation_ApplyHemicycleChanges : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_OpticalInformation_ApplyHemicycleChanges>
{
};

TEST_P(OpticalInformation_ApplyHemicycleChanges, ApplyHemicycleChanges)
{
  DataFor_OpticalInformation_ApplyHemicycleChanges data = GetParam();
  OpticalInformationTester TEST_HELPER;

  auto result = TEST_HELPER.opticalInformation.ApplyHemicycleChanges(data.orientationRelativToViewAxis);

  ASSERT_EQ(data.result, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    OpticalInformation_ApplyHemicycleChanges,
    testing::Values(
        DataFor_OpticalInformation_ApplyHemicycleChanges{
            .orientationRelativToViewAxis = 10.0_rad,
            .result = 10_rad - 2_rad * M_PI},
        DataFor_OpticalInformation_ApplyHemicycleChanges{
            .orientationRelativToViewAxis = -10_rad,
            .result = -10_rad + 2_rad * M_PI}));
