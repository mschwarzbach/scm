/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeGazeFovea.h"
#include "../Fakes/FakeGazePeriphery.h"
#include "../Fakes/FakeGazeUsefulFieldOfView.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/CurrentGazeState.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct CurrentGazeStateTester
{
  class CurrentGazeStateUnderTest : CurrentGazeState
  {
  public:
    template <typename... Args>
    CurrentGazeStateUnderTest(Args&&... args)
        : CurrentGazeState{std::forward<Args>(args)...} {};

    using CurrentGazeState::UpdateCurrentGazeState;
  };

  CurrentGazeStateTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        fakeGazePeriphery{},
        fakeGazeUsefulFieldOfView{},
        fakeGazeFovea{},
        currentGazeState(fakeMentalModel,
                         fakeGazePeriphery,
                         fakeGazeUsefulFieldOfView,
                         fakeGazeFovea)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeGazePeriphery> fakeGazePeriphery;
  NiceMock<FakeGazeUsefulFieldOfView> fakeGazeUsefulFieldOfView;
  NiceMock<FakeGazeFovea> fakeGazeFovea;

  CurrentGazeStateUnderTest currentGazeState;
};

/***************************************************
 * CHECK OpticalInformation UpdateCurrentGazeState *
 ***************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_UpdateCurrentGazeState
{
  GazeState plannedGazeState;
  GazeState resultGazeState;
  int callSetHorizontalGazeAngle;
  int callUpdateOwnVehicleData;
  units::angle::radian_t resultGazeAngleHorizontal;
};
class CurrentGazeState_UpdateCurrentGazeState : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_UpdateCurrentGazeState>
{
};

TEST_P(CurrentGazeState_UpdateCurrentGazeState, UpdateCurrentGazeState)
{
  CurrentGazeStateTester TEST_HELPER;
  DataFor_UpdateCurrentGazeState data = GetParam();

  GazeStateAndAngle gazeStateAndAngle{
      .currentGazeState = GazeState::EGO_FRONT,
      .gazeAngleHorizontal = 5.0_rad};

  SurroundingObjectsSCM surroundingObjects{};

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.id = 0;
  agentEgo.fovea = AreaOfInterest::LEFT_FRONT;
  agentEgo.ufov = {AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_SIDE};
  agentEgo.periphery = {AreaOfInterest::RIGHT_SIDE, AreaOfInterest::LEFT_REAR};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&agentEgo));
  ON_CALL(TEST_HELPER.fakeGazeFovea, GetFoveaGazeAngleHorizontal(_, _)).WillByDefault(Return(2.0_rad));
  ON_CALL(TEST_HELPER.fakeGazeUsefulFieldOfView, GetUsefulFieldOfViewAois(_, _)).WillByDefault(Return(agentEgo.ufov));
  ON_CALL(TEST_HELPER.fakeGazePeriphery, GetPeripheryAois(_, _)).WillByDefault(Return(agentEgo.periphery));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetHorizontalGazeAngle(_)).Times(data.callSetHorizontalGazeAngle);
  EXPECT_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).Times(data.callUpdateOwnVehicleData);

  auto result = TEST_HELPER.currentGazeState.UpdateCurrentGazeState(data.plannedGazeState, gazeStateAndAngle, surroundingObjects);

  ASSERT_EQ(result.currentGazeState, data.resultGazeState);
  ASSERT_EQ(result.gazeAngleHorizontal, data.resultGazeAngleHorizontal);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    CurrentGazeState_UpdateCurrentGazeState,
    testing::Values(
        DataFor_UpdateCurrentGazeState{
            .plannedGazeState = GazeState::EGO_FRONT,
            .resultGazeState = GazeState::EGO_FRONT,
            .callSetHorizontalGazeAngle = 0,
            .callUpdateOwnVehicleData = 0,
            .resultGazeAngleHorizontal = 5.0_rad},
        DataFor_UpdateCurrentGazeState{
            .plannedGazeState = GazeState::SACCADE,
            .resultGazeState = GazeState::SACCADE,
            .callSetHorizontalGazeAngle = 0,
            .callUpdateOwnVehicleData = 0,
            .resultGazeAngleHorizontal = 5.0_rad},
        DataFor_UpdateCurrentGazeState{
            .plannedGazeState = GazeState::EGO_REAR,
            .resultGazeState = GazeState::EGO_REAR,
            .callSetHorizontalGazeAngle = 1,
            .callUpdateOwnVehicleData = 3,
            .resultGazeAngleHorizontal = 2.0_rad}));