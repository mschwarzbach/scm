/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/ImpulseRequest.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::Matcher;
using ::testing::NiceMock;
using ::testing::Return;

struct ImpulseRequestTester
{
  class ImpulseRequestUnderTest : ImpulseRequest
  {
  public:
    template <typename... Args>
    ImpulseRequestUnderTest(Args&&... args)
        : ImpulseRequest{std::forward<Args>(args)...} {};

    using ImpulseRequest::UpdateImpulseDataEgoFront;
    using ImpulseRequest::UpdateImpulseDataFront;
  };

  ImpulseRequestTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        impulseRequest(fakeMentalModel)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  ImpulseRequestUnderTest impulseRequest;
};

/***********************************************
 * CHECK ImpulseRequest UpdateImpulseDataFront *
 ***********************************************/

TEST(ImpulseRequest_UpdateImpulseDataFront, UpdateImpulseDataFront)
{
  ImpulseRequestTester TEST_HELPER;

  std::map<GazeState, double> gazeStateIntensities;
  ObjectInformationSCM object;
  object.brakeLightsActive = true;
  std::vector<ObjectInformationSCM> objects{object};
  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.ufov = {AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_SIDE};
  mockInfo.periphery = {AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_SIDE};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsUrgentlyLateWithUpdate(Matcher<GazeState>(_))).WillByDefault(Return(true));

  auto result = TEST_HELPER.impulseRequest.UpdateImpulseDataFront(gazeStateIntensities, &objects, AreaOfInterest::LEFT_FRONT);

  ASSERT_EQ(result[GazeState::LEFT_FRONT], 2);
}

/**************************************************
 * CHECK ImpulseRequest UpdateImpulseDataEgoFront *
 **************************************************/

TEST(ImpulseRequest_UpdateImpulseDataEgoFront, UpdateImpulseDataEgoFront)
{
  ImpulseRequestTester TEST_HELPER;

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::EGO_FRONT] = 2.0;

  ObjectInformationSCM object;
  object.brakeLightsActive = true;
  std::vector<ObjectInformationSCM> objects{object};

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.ufov = {AreaOfInterest::LEFT_FRONT, AreaOfInterest::EGO_FRONT};
  mockInfo.periphery = {AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_SIDE};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  auto result = TEST_HELPER.impulseRequest.UpdateImpulseDataEgoFront(gazeStateIntensities, &objects);

  ASSERT_EQ(result[GazeState::EGO_FRONT], 2);
}