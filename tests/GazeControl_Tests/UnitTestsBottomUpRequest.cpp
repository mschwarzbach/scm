/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock-nice-strict.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeAuditoryPerception.h"
#include "../Fakes/FakeImpulseRequest.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStimulusRequest.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "Fakes/FakeGazeControl.h"
#include "GazeControl/BottomUpRequest.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
struct BottomUpRequestTester
{
  class BottomUpRequestUnderTest : BottomUpRequest
  {
  public:
    template <typename... Args>
    BottomUpRequestUnderTest(Args&&... args)
        : BottomUpRequest{std::forward<Args>(args)...} {};

    using BottomUpRequest::GetBottomUpIntensities;
    using BottomUpRequest::Update;
  };

  BottomUpRequestTester()
      : fakeMicroscopicCharacteristics{},
        fakeStochastics{},
        fakeImpulseRequest{},
        fakeStimulusRequest{},
        bottomUpRequest(&fakeStochastics,
                        fakeImpulseRequest,
                        fakeStimulusRequest)
  {
  }

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeImpulseRequest> fakeImpulseRequest;
  NiceMock<FakeStimulusRequest> fakeStimulusRequest;

  BottomUpRequestUnderTest bottomUpRequest;
};

/********************************
 * CHECK BottomUpRequest Update *
 ********************************/

TEST(BottomUpRequest_Update, Check_Update_Function_EXPECT_CALLS)
{
  BottomUpRequestTester TEST_HELPER;

  std::vector<int> knownIds{};
  std::map<GazeState, double> gazeStateIntensities{};
  SurroundingObjectsSCM surroundingObjects{};
  const OwnVehicleInformationSCM ownVehicleInformationSCM{};
  std::vector<AdasHmiSignal> opticAdasSignals{};
  AdasHmiSignal combinedSignal{};
  int cycleTime = 100;
  NiceMock<FakeAuditoryPerception> fakeAuditoryPercetion;
  NiceMock<FakeGazeControl> fakeGazeControl;

  std::map<GazeState, double> gazeState;
  gazeState[GazeState::EGO_FRONT] = {10.0};

  EXPECT_CALL(TEST_HELPER.fakeImpulseRequest, UpdateImpulseDataFront(_, _ /*&surroundingObjects.ongoingTraffic.objectFrontRight*/, AreaOfInterest::RIGHT_FRONT)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeImpulseRequest, UpdateImpulseDataFront(_, _ /*&surroundingObjects.ongoingTraffic.objectFrontLeft*/, AreaOfInterest::LEFT_FRONT)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeImpulseRequest, UpdateImpulseDataFront(_, _ /*&surroundingObjects.ongoingTraffic.objectFrontFarRight*/, AreaOfInterest::RIGHT_FRONT_FAR)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeImpulseRequest, UpdateImpulseDataFront(_, _ /*&surroundingObjects.ongoingTraffic.objectFrontFarLeft*/, AreaOfInterest::LEFT_FRONT_FAR)).Times(1).WillOnce(Return(gazeState));

  EXPECT_CALL(TEST_HELPER.fakeImpulseRequest, UpdateImpulseDataEgoFront(_, _ /*&surroundingObjects.ongoingTraffic.objectFront*/)).Times(1).WillOnce(Return(gazeState));

  EXPECT_CALL(TEST_HELPER.fakeStimulusRequest, UpdateStimulusDataFront(_, knownIds, _, _)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeStimulusRequest, UpdateStimulusDataSide(_, _, knownIds, _)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeStimulusRequest, UpdateStimulusDataAcoustic(_, _)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeStimulusRequest, UpdateStimulusDataOptical(_, _ /*opticAdasSignals*/)).Times(1).WillOnce(Return(gazeState));
  EXPECT_CALL(TEST_HELPER.fakeStimulusRequest, UpdateStimulusDataCombinedSignal(_, _, _)).Times(1).WillOnce(Return(gazeState));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles{&fakeVehicle};

  TEST_HELPER.bottomUpRequest.Update(knownIds, gazeStateIntensities, surroundingObjects, ownVehicleInformationSCM, opticAdasSignals, fakeAuditoryPercetion, combinedSignal, fakeGazeControl);
  auto result = TEST_HELPER.bottomUpRequest.GetBottomUpIntensities();

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(GazeState::EGO_FRONT), 10.0);
}