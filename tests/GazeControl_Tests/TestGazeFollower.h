/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/GazeControl/GazeFollower.h"

class TestGazeFollower : public GazeFollower
{
public:
  TestGazeFollower(const OwnVehicleInformationSCM& ownVehicleInformationSCM);

  MOCK_METHOD0(ParseGazeTargetTimeSeriesFromFile, void());
  MOCK_METHOD1(ProcessGazeTargetTimeSeries, void(units::time::millisecond_t time));

  void SetGazeFollowerActive(bool input);
  void SetGazeOverwrite(GazeOverwrite input);
};

class TestGazeFollower2 : public GazeFollower
{
public:
  TestGazeFollower2(const OwnVehicleInformationSCM& ownVehicleInformationSCM);

  void SetGazeOverwrite(GazeOverwrite input);
  GazeOverwrite GetGazeOverwrite();
  void SetTimeOfEventActivation(units::time::millisecond_t input);
  void SetGazeTimeSeries(std::vector<GazeTimeSeriesPoint> input);
  bool TestGetThrowWarning();
  void TestProcessGazeTargetTimeSeries(units::time::millisecond_t time);
};