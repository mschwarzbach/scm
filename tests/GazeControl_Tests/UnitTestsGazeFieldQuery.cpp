/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazeFieldQueryInterface.h"
#include "include/common/AreaOfInterest.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct GazeFieldQueryTester
{
  class GazeFieldQueryUnderTest : GazeFieldQuery
  {
  public:
    template <typename... Args>
    GazeFieldQueryUnderTest(Args&&... args)
        : GazeFieldQuery{std::forward<Args>(args)...} {};

    using GazeFieldQuery::AssignPeripheryFieldOfViewToAoi;
    using GazeFieldQuery::AssignUsefulFieldOfViewToAoi;
    using GazeFieldQuery::CorrectGazeAngleSideBordersDueChangeOfQuadrants;
    using GazeFieldQuery::DetermineGazeAngleSideBorders;
    using GazeFieldQuery::IsAoiWithinHorizontalLimits;
    using GazeFieldQuery::UpdateAreaOfInterest;
  };

  GazeFieldQueryTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        gazeFieldQuery(fakeMentalModel)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  GazeFieldQueryUnderTest gazeFieldQuery;
};

/*****************************************************
 * CHECK GazeFieldQuery AssignUsefulFieldOfViewToAoi *
 *****************************************************/

struct DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi
{
  units::angle::radian_t orientation;
  units::angle::radian_t leftBorder;
  units::angle::radian_t RightBorder;
  units::angle::radian_t gazeAngle;
  AreaOfInterest resultAoi;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi& obj)
  {
    return os
           << "  orientation (double): " << obj.orientation
           << "  leftBorder (double): " << obj.leftBorder
           << "  RightBorder (double): " << obj.RightBorder
           << "  gazeAngle (double): " << obj.gazeAngle
           << "  resultAoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.resultAoi);
  }
};
class GazeFieldQuery_AssignUsefulFieldOfViewToAoi : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi>
{
};

TEST_P(GazeFieldQuery_AssignUsefulFieldOfViewToAoi, AssignUsefulFieldOfViewToAoi)
{
  DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi data = GetParam();
  GazeFieldQueryTester TEST_HELPER;

  BordersAndOrientation bordersAndOrientation{
      .orientation = data.orientation,
      .leftBorder = data.leftBorder,
      .rightBorder = data.RightBorder};

  auto result = TEST_HELPER.gazeFieldQuery.AssignUsefulFieldOfViewToAoi(bordersAndOrientation, AreaOfInterest::EGO_FRONT, data.gazeAngle);

  ASSERT_EQ(result, data.resultAoi);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_AssignUsefulFieldOfViewToAoi,
    testing::Values(
        DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi{
            .orientation = 5.0_rad,
            .leftBorder = 6.0_rad,
            .RightBorder = 7.0_rad,
            .gazeAngle = 8.0_rad,
            .resultAoi = AreaOfInterest::EGO_FRONT},
        DataFor_GazeFieldQuery_AssignUsefulFieldOfViewToAoi{
            .orientation = 5.0_rad,
            .leftBorder = 6.0_rad,
            .RightBorder = 7.0_rad,
            .gazeAngle = -1.0_rad,
            .resultAoi = AreaOfInterest::NumberOfAreaOfInterests}));

/************************************************************************
 * CHECK GazeFieldQuery CorrectGazeAngleSideBordersDueChangeOfQuadrants *
 ************************************************************************/

struct DataFor_GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants
{
  units::angle::radian_t border;
  units::angle::radian_t result;
};
class GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants : public ::testing::Test,
                                                                       public ::testing::WithParamInterface<DataFor_GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants>
{
};

TEST_P(GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants, CorrectGazeAngleSideBordersDueChangeOfQuadrants)
{
  DataFor_GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants data = GetParam();
  GazeFieldQueryTester TEST_HELPER;
  auto result = TEST_HELPER.gazeFieldQuery.CorrectGazeAngleSideBordersDueChangeOfQuadrants(data.border);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants,
    testing::Values(
        DataFor_GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants{
            .border = -5.0_rad,
            .result = 2_rad * M_PI + -5.0_rad},
        DataFor_GazeFieldQuery_CorrectGazeAngleSideBordersDueChangeOfQuadrants{
            .border = 5.0_rad,
            .result = -2_rad * M_PI + 5.0_rad}));

/******************************************************
 * CHECK GazeFieldQuery DetermineGazeAngleSideBorders *
 ******************************************************/

struct DataFor_GazeFieldQuery_DetermineGazeAngleSideBorders
{
  bool ufov;
  units::angle::radian_t result_left;
  units::angle::radian_t result_right;
};
class GazeFieldQuery_DetermineGazeAngleSideBorders : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_GazeFieldQuery_DetermineGazeAngleSideBorders>
{
};

TEST_P(GazeFieldQuery_DetermineGazeAngleSideBorders, DetermineGazeAngleSideBorders)
{
  DataFor_GazeFieldQuery_DetermineGazeAngleSideBorders data = GetParam();
  GazeFieldQueryTester TEST_HELPER;

  auto [leftBorder, rightBorder] = TEST_HELPER.gazeFieldQuery.DetermineGazeAngleSideBorders(data.ufov, 5.0_rad);

  ASSERT_NEAR(leftBorder.value(), data.result_left.value(), 0.01);
  ASSERT_NEAR(rightBorder.value(), data.result_right.value(), 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_DetermineGazeAngleSideBorders,
    testing::Values(
        DataFor_GazeFieldQuery_DetermineGazeAngleSideBorders{
            .ufov = true,
            .result_left = -0.75_rad,
            .result_right = -1.8_rad},
        DataFor_GazeFieldQuery_DetermineGazeAngleSideBorders{
            .ufov = false,
            .result_left = 0.54_rad,
            .result_right = -3.11_rad}));

/*****************************************************
 * CHECK GazeFieldQuery GetAoiWithinHorizontalLimits *
 *****************************************************/

struct DataFor_GazeFieldQuery_GetAoiWithinHorizontalLimits
{
  units::angle::radian_t orientation;
  units::angle::radian_t leftBorder;
  units::angle::radian_t RightBorder;
  units::angle::radian_t gazeAngle;
  bool isHorizontalLimit;
};
class GazeFieldQuery_GetAoiWithinHorizontalLimits : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_GazeFieldQuery_GetAoiWithinHorizontalLimits>
{
};

TEST_P(GazeFieldQuery_GetAoiWithinHorizontalLimits, GetAoiWithinHorizontalLimits)
{
  DataFor_GazeFieldQuery_GetAoiWithinHorizontalLimits data = GetParam();
  GazeFieldQueryTester TEST_HELPER;

  BordersAndOrientation bordersAndOrientation{
      .orientation = data.orientation,
      .leftBorder = data.leftBorder,
      .rightBorder = data.RightBorder};

  auto result = TEST_HELPER.gazeFieldQuery.IsAoiWithinHorizontalLimits(bordersAndOrientation, 5.0_rad, data.gazeAngle);

  ASSERT_EQ(result, data.isHorizontalLimit);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_GetAoiWithinHorizontalLimits,
    testing::Values(
        DataFor_GazeFieldQuery_GetAoiWithinHorizontalLimits{
            .orientation = 5.0_rad,
            .leftBorder = 6.0_rad,
            .RightBorder = 7.0_rad,
            .gazeAngle = 8.0_rad,
            .isHorizontalLimit = true},
        DataFor_GazeFieldQuery_GetAoiWithinHorizontalLimits{
            .orientation = 5.0_rad,
            .leftBorder = 6.0_rad,
            .RightBorder = 7.0_rad,
            .gazeAngle = -1.0_rad,
            .isHorizontalLimit = false}));

/********************************************************
 * CHECK GazeFieldQuery AssignPeripheryFieldOfViewToAoi *
 ********************************************************/

struct DataFor_GazeFieldQuery_AssignPeripheryFieldOfViewToAoi
{
  AreaOfInterest aoiToCheck;
  units::angle::radian_t gazeAngle;
  AreaOfInterest resultAoi;
};
class GazeFieldQuery_AssignPeripheryFieldOfViewToAoi : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_GazeFieldQuery_AssignPeripheryFieldOfViewToAoi>
{
};

TEST_P(GazeFieldQuery_AssignPeripheryFieldOfViewToAoi, AssignPeripheryFieldOfViewToAoi)
{
  DataFor_GazeFieldQuery_AssignPeripheryFieldOfViewToAoi data = GetParam();
  GazeFieldQueryTester TEST_HELPER;

  std::vector<AreaOfInterest> ufov{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_REAR};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetUfov()).WillByDefault(Return(ufov));

  BordersAndOrientation bordersAndOrientation{
      .orientation = 5.0_rad,
      .leftBorder = 6.0_rad,
      .rightBorder = 7.0_rad};

  auto result = TEST_HELPER.gazeFieldQuery.AssignPeripheryFieldOfViewToAoi(bordersAndOrientation, data.aoiToCheck, data.gazeAngle);
  ASSERT_EQ(result, data.resultAoi);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_AssignPeripheryFieldOfViewToAoi,
    testing::Values(
        DataFor_GazeFieldQuery_AssignPeripheryFieldOfViewToAoi{
            .aoiToCheck = AreaOfInterest::EGO_FRONT,
            .gazeAngle = 6.0_rad,
            .resultAoi = AreaOfInterest::NumberOfAreaOfInterests},
        DataFor_GazeFieldQuery_AssignPeripheryFieldOfViewToAoi{
            .aoiToCheck = AreaOfInterest::RIGHT_FRONT,
            .gazeAngle = 6.0_rad,
            .resultAoi = AreaOfInterest::RIGHT_FRONT}));

/*********************************************
 * CHECK GazeFieldQuery UpdateAreaOfInterest *
 *********************************************/
struct DataFor_GazeFieldQuery_UpdateAreaOfInterest
{
  bool exist;
  int result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GazeFieldQuery_UpdateAreaOfInterest& obj)
  {
    return os
           << "  bool (exist): " << obj.exist
           << "  result (double): " << obj.result;
  }
};
class GazeFieldQuery_UpdateAreaOfInterest : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_GazeFieldQuery_UpdateAreaOfInterest>
{
};

TEST_P(GazeFieldQuery_UpdateAreaOfInterest, UpdateAreaOfInterest)
{
  DataFor_GazeFieldQuery_UpdateAreaOfInterest data = GetParam();
  GazeFieldQueryTester TEST_HELPER;

  ObjectInformationSCM objectInformationSCM;
  objectInformationSCM.exist = data.exist;

  std::vector<AreaOfInterest> currentAoi{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_REAR};
  auto result = TEST_HELPER.gazeFieldQuery.UpdateAreaOfInterest(objectInformationSCM, AreaOfInterest::EGO_FRONT, currentAoi, true, 5.0_rad);

  ASSERT_EQ(result.size(), data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFieldQuery_UpdateAreaOfInterest,
    testing::Values(
        DataFor_GazeFieldQuery_UpdateAreaOfInterest{
            .exist = true,
            .result = 3},
        DataFor_GazeFieldQuery_UpdateAreaOfInterest{
            .exist = false,
            .result = 4}));
