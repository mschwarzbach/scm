/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeAuditoryPerception.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "Fakes/FakeGazeControl.h"
#include "GazeControl/StimulusRequest.h"
#include "GazeControl/StimulusRequestInterface.h"
#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::Matcher;
using ::testing::NiceMock;
using ::testing::Return;

struct StimulusRequestTester
{
  class StimulusRequestUnderTest : StimulusRequest
  {
  public:
    template <typename... Args>
    StimulusRequestUnderTest(Args&&... args)
        : StimulusRequest{std::forward<Args>(args)...} {};

    using StimulusRequest::IsDataOutdated;
    using StimulusRequest::IsObjectPerceivedAsStimuli;
    using StimulusRequest::PeripheralPerceptionOfLateralMotion;
    using StimulusRequest::UpdateStimulusDataAcoustic;
    using StimulusRequest::UpdateStimulusDataCombinedSignal;
    using StimulusRequest::UpdateStimulusDataFront;
    using StimulusRequest::UpdateStimulusDataOptical;
    using StimulusRequest::UpdateStimulusDataSide;

    void SET_DURATION_OPTICAL_STIMULUS_DETECTABLE(units::time::millisecond_t duration)
    {
      _durationOpticStimulusDetectable = duration;
    }

    void SET_DURATION_COMBINED_STIMULUS_DETECTABLE(units::time::millisecond_t duration)
    {
      _durationCombinedStimulusDetectable = duration;
    }
  };

  StimulusRequestTester()
      : fakeMentalModel{},
        fakeStochastics{},
        fakeMicroscopicCharacteristics{},
        stimulusRequest(fakeMentalModel,
                        &fakeStochastics)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  StimulusRequestUnderTest stimulusRequest;
};

/************************************************
 * CHECK StimulusRequest UpdateImpulseDataFront *
 ************************************************/
struct DataFor_UpdateImpulseDataFront
{
  AreaOfInterest aoi;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_UpdateImpulseDataFront& obj)
  {
    return os
           << "  aoi (AreaOfInteresst): " << ScmCommons::AreaOfInterestToString(obj.aoi);
  }
};
class StimulusRequest_UpdateImpulseDataFront : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_UpdateImpulseDataFront>
{
};
TEST_P(StimulusRequest_UpdateImpulseDataFront, UpdateImpulseDataFront)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_UpdateImpulseDataFront data = GetParam();

  std::map<GazeState, double> gazeStateIntensities;
  std::vector<int> knownObjectIds;
  SurroundingObjectsSCM surroundingObjects;
  ObjectInformationSCM objectFrontLeft, objectFrontRight, objectFrontFarLeft, objectFrontFarRight, objectFront;

  objectFrontLeft.id = 0;
  objectFrontRight.id = 1;
  objectFrontFarLeft.id = 3;
  objectFrontFarRight.id = 4;
  objectFront.id = 5;

  surroundingObjects.ongoingTraffic.Ahead().Left().push_back(objectFrontLeft);
  surroundingObjects.ongoingTraffic.Ahead().Right().push_back(objectFrontRight);
  surroundingObjects.ongoingTraffic.FarAhead().Left().push_back(objectFrontFarLeft);
  surroundingObjects.ongoingTraffic.FarAhead().Right().push_back(objectFrontFarRight);
  surroundingObjects.ongoingTraffic.Ahead().Close().push_back(objectFront);

  OwnVehicleInformationSCM ownVehicleInformationSCM;

  std::vector<AreaOfInterest> periphery{data.aoi};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPeriphery()).WillByDefault(Return(periphery));

  auto result = TEST_HELPER.stimulusRequest.UpdateStimulusDataFront(gazeStateIntensities, knownObjectIds, surroundingObjects, ownVehicleInformationSCM);

  if (data.aoi == AreaOfInterest::LEFT_FRONT)
  {
    ASSERT_EQ(result[GazeState::LEFT_FRONT], 1.0);
  }
  else
  {
    EXPECT_TRUE(result.empty());
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_UpdateImpulseDataFront,
    testing::Values(
        DataFor_UpdateImpulseDataFront{
            .aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_UpdateImpulseDataFront{
            .aoi = AreaOfInterest::EGO_FRONT}));

/************************************************
 * CHECK StimulusRequest UpdateStimulusDataSide *
 ************************************************/

struct DataFor_UpdateStimulusDataSide
{
  AreaOfInterest aoi;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_UpdateStimulusDataSide& obj)
  {
    return os
           << "  aoi (AreaOfInteresst): " << ScmCommons::AreaOfInterestToString(obj.aoi);
  }
};
class StimulusRequest_UpdateStimulusDataSide : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_UpdateStimulusDataSide>
{
};
TEST_P(StimulusRequest_UpdateStimulusDataSide, UpdateStimulusDataSide)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_UpdateStimulusDataSide data = GetParam();

  ObjectInformationSCM sideObject;
  sideObject.id = 0;
  sideObject.lateralVelocity = 0.0_mps;
  sideObject.obstruction.left = 1.0_m;
  sideObject.obstruction.right = 2.0_m;
  sideObject.opticalAngleSizeHorizontal = 5.0_rad;
  sideObject.opticalAnglePositionHorizontal = 6.0_rad;

  std::map<GazeState, double> gazeStateIntensities;
  std::vector<int> knownObjectIds;
  SurroundingObjectsSCM surroundingObjects;
  surroundingObjects.ongoingTraffic.Close().Left().push_back(sideObject);
  surroundingObjects.ongoingTraffic.Close().Right().push_back(sideObject);

  OwnVehicleInformationSCM ownVehicleInformationSCM;
  ownVehicleInformationSCM.lateralVelocity = 3_mps;

  std::vector<AreaOfInterest> periphery{data.aoi};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPeriphery()).WillByDefault(Return(periphery));
  DriverParameters driverParameters;
  driverParameters.thresholdRetinalProjectionAngularSpeedFovea = 3_rad_per_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  auto result = TEST_HELPER.stimulusRequest.UpdateStimulusDataSide(gazeStateIntensities, surroundingObjects, knownObjectIds, ownVehicleInformationSCM);

  if (data.aoi == AreaOfInterest::LEFT_SIDE)
  {
    ASSERT_EQ(result[GazeState::LEFT_SIDE], 1.0);
  }
  else
  {
    ASSERT_EQ(result[GazeState::RIGHT_SIDE], 1.0);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_UpdateStimulusDataSide,
    testing::Values(
        DataFor_UpdateStimulusDataSide{
            .aoi = AreaOfInterest::LEFT_SIDE},
        DataFor_UpdateStimulusDataSide{
            .aoi = AreaOfInterest::RIGHT_SIDE}));

/****************************************************
 * CHECK StimulusRequest UpdateStimulusDataAcoustic *
 ****************************************************/
struct DataFor_UpdateStimulusDataAcoustic
{
  AcousticStimulusDirection direction;
  int gazeIntensityEgoFront;
  int gazeIntensityRightSide;
  int gazeIntensityEgoRear;
  int gazeIntensityEgoLeftSide;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_UpdateStimulusDataAcoustic& obj)
  {
    return os
           << " int (direction): " << obj.direction
           << " int (gazeIntensityEgoFront): " << obj.gazeIntensityEgoFront
           << " int (gazeIntensityRightSide): " << obj.gazeIntensityRightSide
           << " int (gazeIntensityEgoRear): " << obj.gazeIntensityEgoRear
           << " int (gazeIntensityEgoLeftSide): " << obj.gazeIntensityEgoLeftSide;
  }
};
class StimulusRequest_UpdateStimulusDataAcoustic : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_UpdateStimulusDataAcoustic>
{
};

TEST_P(StimulusRequest_UpdateStimulusDataAcoustic, UpdateStimulusDataAcoustic)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_UpdateStimulusDataAcoustic data = GetParam();

  NiceMock<FakeAuditoryPerception> fakeAuditoryPerception;

  ON_CALL(fakeAuditoryPerception, IsAcousticStimulusProcessed()).WillByDefault(Return(true));
  ON_CALL(fakeAuditoryPerception, GetAcousticStimulusDirection()).WillByDefault(Return(data.direction));

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::EGO_FRONT] = 0;
  gazeStateIntensities[GazeState::RIGHT_SIDE] = 0;
  gazeStateIntensities[GazeState::EGO_REAR] = 0;
  gazeStateIntensities[GazeState::LEFT_SIDE] = 0;

  auto result = TEST_HELPER.stimulusRequest.UpdateStimulusDataAcoustic(gazeStateIntensities, fakeAuditoryPerception);

  ASSERT_EQ(result[GazeState::EGO_FRONT], data.gazeIntensityEgoFront);
  ASSERT_EQ(result[GazeState::RIGHT_SIDE], data.gazeIntensityRightSide);
  ASSERT_EQ(result[GazeState::EGO_REAR], data.gazeIntensityEgoRear);
  ASSERT_EQ(result[GazeState::LEFT_SIDE], data.gazeIntensityEgoLeftSide);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_UpdateStimulusDataAcoustic,
    testing::Values(
        DataFor_UpdateStimulusDataAcoustic{
            .direction = FRONT,
            .gazeIntensityEgoFront = 2,
            .gazeIntensityRightSide = 0,
            .gazeIntensityEgoRear = 0,
            .gazeIntensityEgoLeftSide = 0},
        DataFor_UpdateStimulusDataAcoustic{
            .direction = RIGHT,
            .gazeIntensityEgoFront = 0,
            .gazeIntensityRightSide = 2,
            .gazeIntensityEgoRear = 0,
            .gazeIntensityEgoLeftSide = 0},
        DataFor_UpdateStimulusDataAcoustic{
            .direction = REAR,
            .gazeIntensityEgoFront = 0,
            .gazeIntensityRightSide = 0,
            .gazeIntensityEgoRear = 2,
            .gazeIntensityEgoLeftSide = 0},
        DataFor_UpdateStimulusDataAcoustic{
            .direction = LEFT,
            .gazeIntensityEgoFront = 0,
            .gazeIntensityRightSide = 0,
            .gazeIntensityEgoRear = 0,
            .gazeIntensityEgoLeftSide = 2},
        DataFor_UpdateStimulusDataAcoustic{
            .direction = DEFAULT,
            .gazeIntensityEgoFront = 2,
            .gazeIntensityRightSide = 0,
            .gazeIntensityEgoRear = 0,
            .gazeIntensityEgoLeftSide = 0}));

/***************************************************
 * CHECK StimulusRequest UpdateStimulusDataOptical *
 ***************************************************/

TEST(StimulusRequest_UpdateStimulusDataOptical, UpdateStimulusDataOptical)
{
  StimulusRequestTester TEST_HELPER;

  std::map<GazeState, double> gazeStateIntensities;

  AdasHmiSignal signal;
  signal.activity = true;
  signal.spotOfPresentation = "instrumentCluster";
  std::vector<AdasHmiSignal> opticAdasSignals{signal};

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.ufov = {AreaOfInterest::INSTRUMENT_CLUSTER, AreaOfInterest::HUD, AreaOfInterest::LEFT_REAR, AreaOfInterest::RIGHT_REAR, AreaOfInterest::NumberOfAreaOfInterests};
  mockInfo.fovea = {};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(TEST_HELPER.fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1.0));

  TEST_HELPER.stimulusRequest.SET_DURATION_OPTICAL_STIMULUS_DETECTABLE(400_ms);
  auto result = TEST_HELPER.stimulusRequest.UpdateStimulusDataOptical(gazeStateIntensities, opticAdasSignals);

  ASSERT_EQ(result[GazeState::EGO_FRONT], 2);
}

/****************************************
 * CHECK StimulusRequest IsDataOutdated *
 ****************************************/
struct DataFor_IsDataOutdated
{
  AreaOfInterest aoi;
  double reliability;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_IsDataOutdated& obj)
  {
    return os
           << " AreaOfInterest (aoi): " << ScmCommons::AreaOfInterestToString(obj.aoi)
           << " double (reliability): " << obj.reliability
           << " bool (result): " << obj.result;
  }
};
class StimulusRequest_IsDataOutdated : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsDataOutdated>
{
};
TEST_P(StimulusRequest_IsDataOutdated, IsDataOutdated)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_IsDataOutdated data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetReliability(_)).WillByDefault(Return(data.reliability));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_)).WillByDefault(Return(&fakeVehicle));

  auto result = TEST_HELPER.stimulusRequest.IsDataOutdated(0);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_IsDataOutdated,
    testing::Values(
        DataFor_IsDataOutdated{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .reliability = 1.0,
            .result = false},
        DataFor_IsDataOutdated{
            .aoi = AreaOfInterest::EGO_FRONT,
            .reliability = 2.0,
            .result = true},
        DataFor_IsDataOutdated{
            .aoi = AreaOfInterest::EGO_FRONT,
            .reliability = 500.0,
            .result = false}));

/****************************************************
 * CHECK StimulusRequest IsObjectPerceivedAsStimuli *
 ****************************************************/
struct DataFor_IsObjectPerceivedAsStimuli
{
  AreaOfInterest aoi;
  units::time::second_t ttcThreshold;
  double tauDotThreshold;
  bool result;
};
class StimulusRequest_IsObjectPerceivedAsStimuli : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_IsObjectPerceivedAsStimuli>
{
};
TEST_P(StimulusRequest_IsObjectPerceivedAsStimuli, IsObjectPerceivedAsStimuli)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_IsObjectPerceivedAsStimuli data = GetParam();

  ObjectInformationSCM objectSideLeft;
  objectSideLeft.opticalAngleSizeHorizontal = 5.0_rad;
  objectSideLeft.opticalAnglePositionHorizontal = 6.0_rad;
  objectSideLeft.lateralVelocity = 0.0_mps;
  objectSideLeft.obstruction.left = 1.0_m;
  objectSideLeft.obstruction.right = 2.0_m;

  ObjectInformationSCM objectFront;
  objectFront.opticalAngleSizeHorizontal = 5.0_rad;
  objectFront.opticalAnglePositionHorizontal = 6.0_rad;
  objectFront.lateralVelocity = 0.0_mps;
  objectFront.obstruction.left = 1.0_m;
  objectFront.obstruction.right = 2.0_m;

  SurroundingObjectsSCM surroundingObjects;
  surroundingObjects.ongoingTraffic.Close().Left().push_back(objectSideLeft);
  surroundingObjects.ongoingTraffic.Ahead().Close().push_back(objectFront);

  OwnVehicleInformationSCM ownVehicleInformation;
  ownVehicleInformation.lateralVelocity = 0.0_mps;

  DriverParameters param{};
  param.comfortLongitudinalDeceleration = 2._mps_sq;
  param.maximumLongitudinalDeceleration = 9._mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(param));

  auto result = TEST_HELPER.stimulusRequest.IsObjectPerceivedAsStimuli(data.aoi, data.ttcThreshold, data.tauDotThreshold, surroundingObjects, ownVehicleInformation);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_IsObjectPerceivedAsStimuli,
    testing::Values(
        DataFor_IsObjectPerceivedAsStimuli{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .ttcThreshold = 5.0_s,
            .tauDotThreshold = -5.0,
            .result = false},
        DataFor_IsObjectPerceivedAsStimuli{
            .aoi = AreaOfInterest::EGO_FRONT,
            .ttcThreshold = 99.0_s,
            .tauDotThreshold = 0.0,
            .result = true}));

/*************************************************************
 * CHECK StimulusRequest PeripheralPerceptionOfLateralMotion *
 *************************************************************/
struct DataFor_PeripheralPerceptionOfLateralMotion
{
  bool exist;
  bool isStatic;
  units::length::meter_t relativeLongitudinalDistance;
  bool isRight;
  double thresholdLooming;
  bool result;
};
class StimulusRequest_PeripheralPerceptionOfLateralMotion : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_PeripheralPerceptionOfLateralMotion>
{
};
TEST_P(StimulusRequest_PeripheralPerceptionOfLateralMotion, PeripheralPerceptionOfLateralMotion)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_PeripheralPerceptionOfLateralMotion data = GetParam();

  ObjectInformationSCM object;
  object.exist = data.exist;
  object.isStatic = data.isStatic;
  object.lateralVelocity = 1.0_mps;
  object.relativeLateralDistance = data.relativeLongitudinalDistance;
  object.thresholdLooming = data.thresholdLooming;

  auto result = TEST_HELPER.stimulusRequest.PeripheralPerceptionOfLateralMotion(&object, data.isRight);

  ASSERT_EQ(result, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_PeripheralPerceptionOfLateralMotion,
    testing::Values(
        DataFor_PeripheralPerceptionOfLateralMotion{
            .exist = false,
            .isStatic = true,
            .relativeLongitudinalDistance = 5.0_m,
            .isRight = true,
            .thresholdLooming = 10.0,
            .result = false},
        DataFor_PeripheralPerceptionOfLateralMotion{
            .exist = true,
            .isStatic = false,
            .relativeLongitudinalDistance = -5.0_m,
            .isRight = false,
            .thresholdLooming = -10.0,
            .result = true}));

/**********************************************************
 * CHECK StimulusRequest UpdateStimulusDataCombinedSignal *
 **********************************************************/

struct DataFor_UpdateStimulusDataCombinedSignal
{
  bool signalActive;
  GazeState state;
  units::time::millisecond_t duration;
  double result;
};
class StimulusRequest_UpdateStimulusDataCombinedSignal : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_UpdateStimulusDataCombinedSignal>
{
};
TEST_P(StimulusRequest_UpdateStimulusDataCombinedSignal, Check_UpdateStimulusDataCombinedSignal)
{
  StimulusRequestTester TEST_HELPER;
  DataFor_UpdateStimulusDataCombinedSignal data = GetParam();
  NiceMock<FakeGazeControl> fakeGazeControl;

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::LEFT_FRONT] = 1.0;
  gazeStateIntensities[GazeState::INSTRUMENT_CLUSTER] = 1.0;

  AdasHmiSignal combinedSignal{
      .activity = data.signalActive,
      .spotOfPresentation = "instrumentCluster",
      .sendingSystem = "Haf",
  };

  TEST_HELPER.stimulusRequest.SET_DURATION_COMBINED_STIMULUS_DETECTABLE(data.duration);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));
  auto result = TEST_HELPER.stimulusRequest.UpdateStimulusDataCombinedSignal(gazeStateIntensities, combinedSignal, fakeGazeControl);

  ASSERT_EQ(result[data.state], data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    StimulusRequest_UpdateStimulusDataCombinedSignal,
    testing::Values(
        DataFor_UpdateStimulusDataCombinedSignal{
            .signalActive = false,
            .state = GazeState::LEFT_FRONT,
            .duration = -100_ms,
            .result = 1.0},
        DataFor_UpdateStimulusDataCombinedSignal{
            .signalActive = true,
            .state = GazeState::LEFT_FRONT,
            .duration = -100_ms,
            .result = 1.0},
        DataFor_UpdateStimulusDataCombinedSignal{
            .signalActive = true,
            .state = GazeState::INSTRUMENT_CLUSTER,
            .duration = 400_ms,
            .result = 2.0}));