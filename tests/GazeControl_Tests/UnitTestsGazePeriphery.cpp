/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeGazeFieldQuery.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/GazePeriphery.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/SensorDriverScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct GazePeripheryTester
{
  class GazePeripheryUnderTest : GazePeriphery
  {
  public:
    template <typename... Args>
    GazePeripheryUnderTest(Args&&... args)
        : GazePeriphery{std::forward<Args>(args)...} {};

    using GazePeriphery::GeneratePeripheryForFrontAndSides;
    using GazePeriphery::GetPeripheryAois;
  };

  GazePeripheryTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        fakeGazeFieldQuery{},
        gazePeriphery(fakeMentalModel, fakeGazeFieldQuery)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeGazeFieldQuery> fakeGazeFieldQuery;

  GazePeripheryUnderTest gazePeriphery;
};

/****************************************
 * CHECK GazePeriphery GetPeripheryAois *
 ****************************************/

TEST(GazePeriphery_GetPeripheryAois, GetPeripheryAois)
{
  GazePeripheryTester TEST_HELPER;

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.fovea = AreaOfInterest::LEFT_REAR;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  ObjectInformationSCM egoFront;
  egoFront.id = 1;

  SurroundingObjectsSCM surroundingObjects;
  surroundingObjects.ongoingTraffic.Ahead().Close().push_back(egoFront);

  auto result = TEST_HELPER.gazePeriphery.GetPeripheryAois(surroundingObjects, 5.0_rad);
  ASSERT_EQ(result.size(), 4);
  ASSERT_EQ(result.at(0), AreaOfInterest::EGO_FRONT);
  ASSERT_EQ(result.at(1), AreaOfInterest::EGO_FRONT_FAR);
  ASSERT_EQ(result.at(2), AreaOfInterest::LEFT_FRONT_FAR);
  ASSERT_EQ(result.at(3), AreaOfInterest::LEFT_SIDE);
}

/*********************************************************
 * CHECK GazePeriphery GeneratePeripheryForFrontAndSides *
 *********************************************************/

TEST(GazePeriphery_GeneratePeripheryForFrontAndSides, GeneratePeripheryForFrontAndSides)
{
  GazePeripheryTester TEST_HELPER;

  std::vector<AreaOfInterest> periphery{};

  SurroundingObjectsSCM surroundingObjects{};

  ObjectInformationSCM objectEgoFront;
  objectEgoFront.id = 1;

  ObjectInformationSCM objectLeftFront;
  objectLeftFront.id = 2;

  std::map<AreaOfInterest, ObjectInformationSCM> aoiObjects;
  aoiObjects[AreaOfInterest::EGO_FRONT] = objectEgoFront;
  aoiObjects[AreaOfInterest::LEFT_FRONT] = objectLeftFront;

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.HeadUpDisplayOrientationRelativeToDriver = 5.0_rad;
  mockInfo.InfotainmentOrientationRelativeToDriver = 6.0_rad;
  mockInfo.InstrumentClusterOrientationRelativeToDriver = 7.0_rad;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInteriorHudExistence()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, FillSurroundingAoiObjects(_)).WillByDefault(Return(aoiObjects));

  std::vector<AreaOfInterest> infotaiment{AreaOfInterest::INFOTAINMENT};
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, AssignFieldOfViewToAoi(AreaOfInterest::INFOTAINMENT, _, false, _, _)).WillByDefault(Return(infotaiment));

  std::vector<AreaOfInterest> instrument{AreaOfInterest::INFOTAINMENT, AreaOfInterest::INSTRUMENT_CLUSTER};
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, AssignFieldOfViewToAoi(AreaOfInterest::INSTRUMENT_CLUSTER, _, false, _, _)).WillByDefault(Return(instrument));

  std::vector<AreaOfInterest> peripheryAois{AreaOfInterest::INFOTAINMENT, AreaOfInterest::INSTRUMENT_CLUSTER, AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT};
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, UpdateAreaOfInterest(_, _, _, false, _)).WillByDefault(Return(peripheryAois));

  auto result = TEST_HELPER.gazePeriphery.GeneratePeripheryForFrontAndSides(periphery, surroundingObjects.ongoingTraffic, 5.0_rad);

  ASSERT_EQ(result.size(), 2);
  ASSERT_EQ(result.at(0), AreaOfInterest::INFOTAINMENT);
  ASSERT_EQ(result.at(1), AreaOfInterest::INSTRUMENT_CLUSTER);
}