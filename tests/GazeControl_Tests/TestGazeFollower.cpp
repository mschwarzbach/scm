/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestGazeFollower.h"

TestGazeFollower::TestGazeFollower(const OwnVehicleInformationSCM& ownVehicleInformationSCM)
    : GazeFollower(ownVehicleInformationSCM) {}

void TestGazeFollower::SetGazeFollowerActive(bool input)
{
  gazeFollowerActive = input;
}

void TestGazeFollower::SetGazeOverwrite(GazeOverwrite input)
{
  gazeOverwrite = input;
}

TestGazeFollower2::TestGazeFollower2(const OwnVehicleInformationSCM& ownVehicleInformationSCM)
    : GazeFollower(ownVehicleInformationSCM) {}

void TestGazeFollower2::SetGazeOverwrite(GazeOverwrite input)
{
  gazeOverwrite = input;
}

GazeOverwrite TestGazeFollower2::GetGazeOverwrite()
{
  return gazeOverwrite;
}

void TestGazeFollower2::SetTimeOfEventActivation(units::time::millisecond_t input)
{
  timeOfEventActivation = input;
}

void TestGazeFollower2::SetGazeTimeSeries(std::vector<GazeTimeSeriesPoint> input)
{
  gazeTimeSeries = input;
}

void TestGazeFollower2::TestProcessGazeTargetTimeSeries(units::time::millisecond_t time)
{
  ProcessGazeTargetTimeSeries(time);
}

bool TestGazeFollower2::TestGetThrowWarning()
{
  return GetThrowWarning();
}