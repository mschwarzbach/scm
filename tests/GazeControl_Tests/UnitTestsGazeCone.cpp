/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/GazeControl/GazeCone.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

/***********************
 * CHECK IsPointInside *
 ***********************/

Scm::Point GET_POINT_FROM_ANGLE(units::angle::radian_t angle)
{
  return {units::make_unit<units::length::meter_t>(units::math::cos(angle).value()), units::make_unit<units::length::meter_t>(units::math::sin(angle).value())};
}

struct DataFor_IsPointInGazeCone
{
  std::string testcase;
  units::angle::degree_t in_openingAngle;
  units::angle::degree_t in_gazeAngle;
  units::angle::degree_t in_testAngle;
  bool expected_result;
};

class GazeCone_IsPointInside : public ::testing::Test,
                               public ::testing::WithParamInterface<DataFor_IsPointInGazeCone>
{
};

TEST_P(GazeCone_IsPointInside, ReturnTrueIfPointIsWithinMinAndMaxAngle)
{
  const auto param = GetParam();

  const Scm::Point point{GET_POINT_FROM_ANGLE(param.in_testAngle)};

  GazeCone gazeCone(param.in_gazeAngle, param.in_openingAngle);

  const auto result = gazeCone.IsPointInside(point);

  ASSERT_EQ(result, param.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeCone_IsPointInside,
    testing::Values(
        DataFor_IsPointInGazeCone{
            .testcase = "20 Degree opening in front, test angle 0",
            .in_openingAngle = 20_deg,
            .in_gazeAngle = 0_deg,
            .in_testAngle = 0_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "20 Degree opening in front, test angle 10",
            .in_openingAngle = 20_deg,
            .in_gazeAngle = 0_deg,
            .in_testAngle = 10_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "20 Degree opening in front, test angle -10",
            .in_openingAngle = 20_deg,
            .in_gazeAngle = 0_deg,
            .in_testAngle = -10_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "20 Degree opening fully in negative area, test angle 0",
            .in_openingAngle = 20_deg,
            .in_gazeAngle = -20_deg,
            .in_testAngle = 0_deg,
            .expected_result = false},
        DataFor_IsPointInGazeCone{
            .testcase = "20 Degree opening fully in negative area, test angle -10",
            .in_openingAngle = 20_deg,
            .in_gazeAngle = -20_deg,
            .in_testAngle = -10_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "180 Degree opening, minAngle -180, test angle -90",
            .in_openingAngle = 180_deg,
            .in_gazeAngle = -90_deg,
            .in_testAngle = -90_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "180 Degree opening, minAngle crosses -180 -> minAngle positive, test angle -90",
            .in_openingAngle = 180_deg,
            .in_gazeAngle = -100_deg,
            .in_testAngle = -90_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "180 Degree opening, minAngle crosses -180 -> minAngle positive, test angle 0",
            .in_openingAngle = 180_deg,
            .in_gazeAngle = -100_deg,
            .in_testAngle = 0_deg,
            .expected_result = false},
        DataFor_IsPointInGazeCone{
            .testcase = "180 Degree opening, maxAngle crosses 180 -> maxAngle negative, test angle 90",
            .in_openingAngle = 180_deg,
            .in_gazeAngle = 100_deg,
            .in_testAngle = 90_deg,
            .expected_result = true},
        DataFor_IsPointInGazeCone{
            .testcase = "180 Degree opening, maxAngle crosses 180 -> maxAngle negative, test angle 0",
            .in_openingAngle = 180_deg,
            .in_gazeAngle = 100_deg,
            .in_testAngle = 0_deg,
            .expected_result = false}));