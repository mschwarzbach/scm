/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeBottomUpRequest.h"
#include "../Fakes/FakeCurrentGazeState.h"
#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeGazeComponents.h"
#include "../Fakes/FakeGazeFieldQuery.h"
#include "../Fakes/FakeGazeFovea.h"
#include "../Fakes/FakeGazePeriphery.h"
#include "../Fakes/FakeGazeUsefulFieldOfView.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeOpticalInformation.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeTopDownRequest.h"
#include "GazeControl/BottomUpRequestInterface.h"
#include "GazeControl/CurrentGazeStateInterface.h"
#include "GazeControl/GazeControl.h"
#include "GazeControl/GazeFollower.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "GazeControl/TopDownRequestInterface.h"
#include "ScmCommons.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
struct GazeControlTester
{
  class GazeControlUnderTest : GazeControl
  {
  public:
    template <typename... Args>
    GazeControlUnderTest(Args&&... args)
        : GazeControl{std::forward<Args>(args)...} {};

    using GazeControl::AdvanceGazeState;
    using GazeControl::ChooseNextGazeState;
    using GazeControl::DrawSaccadeDuration;
    using GazeControl::GenerateInformationCycles;
    using GazeControl::GetHafSignalProcessed;
    using GazeControl::InitializeGazeState;
    using GazeControl::OverwriteGazeBehaviour;
    using GazeControl::RoundGazeStateDurationToCycleTime;
    using GazeControl::SetHafSignalProcessed;
    using GazeControl::UpdateNewGazeState;

    void SET_ISINIT(bool isInit)
    {
      _isInInitState = isInit;
    }

    void SET_CYCLETIME(units::time::millisecond_t cycleTime)
    {
      _cycleTime = cycleTime;
    }

    void SET_DURATION_CURRENT_GAZE_STATE(units::time::millisecond_t durationCurrentGazeState)
    {
      _durationCurrentGazeState = durationCurrentGazeState;
    }

    units::time::millisecond_t GET_DURATION_CURRENT_GAZE_STATE()
    {
      return _durationCurrentGazeState;
    }

    void SET_PLANNED_GAZE_STATE(GazeState gazeState)
    {
      _plannedGazeState = gazeState;
    }

    GazeState GET_PLANNED_GAZE_STATE()
    {
      return _plannedGazeState;
    }

    void SET_CURRENT_GAZE_STATE(GazeState gazeState)
    {
      _currentGazeState = gazeState;
    }

    GazeState GET_CURRENT_GAZE_STATE()
    {
      return _currentGazeState;
    }

    void SET_NEXT_FIXATION_TARGET(GazeState gazeState)
    {
      _nextFixationTarget = gazeState;
    }

    GazeState GET_NEXT_FIXATION_TARGET()
    {
      return _nextFixationTarget;
    }

    void SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(units::time::millisecond_t requiredDurationCurrentGazeState)
    {
      _requiredDurationCurrentGazeState = requiredDurationCurrentGazeState;
    }

    units::time::millisecond_t GET_REQUIRED_DURATION_CURRENT_GAZE_STATE()
    {
      return _requiredDurationCurrentGazeState;
    }

    units::angle::radian_t GET_GAZE_ANGLE_HORIZONTAL()
    {
      return _gazeAngleHorizontal;
    }

    SurroundingObjectsSCM GET_SURROUNDING_OBJECTS()
    {
      return _surroundingObjects;
    }

    GazeState GET_PREVIOUS_FIXATION_TARGET()
    {
      return _previousFixationTarget;
    }

    void SET_GAZE_STATE_INTENSITIES(std::map<GazeState, double> gazeStateIntensities)
    {
      _gazeStateIntensities = gazeStateIntensities;
    }

    bool GET_NEW_INFORMATION_ACQUISITION_REQUESTED()
    {
      return _isNewInformationAcquisitionRequested;
    }
  };
  template <typename T>
  T& WITH_GAZE_COMPONENTS(T& fakeGazeComponents)
  {
    ON_CALL(fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));
    ON_CALL(fakeGazeComponents, GetBottomUpRequest()).WillByDefault(Return(&fakeBottomUpRequest));
    ON_CALL(fakeGazeComponents, GetTopDownRequest()).WillByDefault(Return(&fakeTopDownRequest));
    ON_CALL(fakeGazeComponents, GetCurrentGazeState()).WillByDefault(Return(&fakeUpdateCurrentGazeState));
    ON_CALL(fakeGazeComponents, GetOpticalInformation()).WillByDefault(Return(&fakeOpticalInformation));

    return fakeGazeComponents;
  }
  GazeControlTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        fakeStochastics{},
        fakeFeatureExtractor{},
        surroundingObjects{},
        ownVehicleInformation{},
        fakeGazeComponents{},
        fakeBottomUpRequest{},
        fakeTopDownRequest{},
        fakeGazeFieldQuery{},
        fakeGazePeriphery{},
        fakeOpticalInformation{},
        fakeGazeFovea{},
        fakeUpdateCurrentGazeState{},
        gazeControl(fakeMentalModel,
                    &fakeStochastics,
                    fakeFeatureExtractor,
                    surroundingObjects,
                    ownVehicleInformation,
                    false,
                    WITH_GAZE_COMPONENTS(fakeGazeComponents))
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;

  NiceMock<FakeBottomUpRequest> fakeBottomUpRequest;
  NiceMock<FakeTopDownRequest> fakeTopDownRequest;
  NiceMock<FakeGazeFieldQuery> fakeGazeFieldQuery;
  NiceMock<FakeGazePeriphery> fakeGazePeriphery;
  NiceMock<FakeOpticalInformation> fakeOpticalInformation;
  NiceMock<FakeGazeUsefulFieldOfView> fakeGazeUsefulFieldOfView;
  NiceMock<FakeGazeFovea> fakeGazeFovea;
  NiceMock<FakeCurrentGazeState> fakeUpdateCurrentGazeState;
  NiceMock<FakeGazeComponents> fakeGazeComponents;

  SurroundingObjectsSCM surroundingObjects;
  OwnVehicleInformationSCM ownVehicleInformation;

  GazeControlUnderTest gazeControl;
};

/**************************************
 * CHECK GazeControl AdvanceGazeState *
 **************************************/
struct DataFor_AdvanceGazeState
{
  bool isInit;
  int callBottomUp;
  bool gazeFollower;
  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_AdvanceGazeState& obj)
  {
    return os
           << "  isInit (bool): " << obj.isInit
           << "  callBottomUp (int): " << obj.callBottomUp
           << " gazeFollower (bool): " << obj.gazeFollower;
  }
};
class GazeControl_AdvanceGazeState : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_AdvanceGazeState>
{
};

TEST_P(GazeControl_AdvanceGazeState, AdvanceGazeState)
{
  DataFor_AdvanceGazeState data = GetParam();
  GazeControlTester TEST_HELPER;

  TEST_HELPER.gazeControl.SET_ISINIT(data.isInit);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100._ms));

  std::map<GazeState, double> bottomUpRequest;
  bottomUpRequest[GazeState::EGO_FRONT] = 1.5;

  ON_CALL(TEST_HELPER.fakeBottomUpRequest, GetBottomUpIntensities()).WillByDefault(Return(bottomUpRequest));

  GazeOverwrite gazeOverwrite;
  gazeOverwrite.gazeFollowerActive = data.gazeFollower;
  gazeOverwrite.hasGazeTargetChanged = true;
  gazeOverwrite.gazeTargetIsLastPointInTimeSeries = false;

  struct GazeStateAndAngle gazeStateAndAngle
  {
    .currentGazeState = GazeState::SACCADE,
    .gazeAngleHorizontal = 1.0_rad
  };

  struct OpticalValues opticalValues
  {
    .surroundingObjects = {},
    .gazeAngleHorizontal = 2.0_rad
  };

  ON_CALL(TEST_HELPER.fakeUpdateCurrentGazeState, UpdateCurrentGazeState(_, _, _)).WillByDefault(Return(gazeStateAndAngle));
  ON_CALL(TEST_HELPER.fakeOpticalInformation, SupplementOpticalInformation(_, _, _, _)).WillByDefault(Return(opticalValues));

  TEST_HELPER.gazeControl.SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(10.0_ms);

  EXPECT_CALL(TEST_HELPER.fakeBottomUpRequest, GetBottomUpIntensities()).Times(data.callBottomUp);
  EXPECT_CALL(TEST_HELPER.fakeOpticalInformation, SupplementOpticalInformation(_, _, _, _)).Times(1);

  TEST_HELPER.gazeControl.AdvanceGazeState(gazeOverwrite);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeControl_AdvanceGazeState,
    testing::Values(
        DataFor_AdvanceGazeState{
            .isInit = false,
            .callBottomUp = 1,
            .gazeFollower = false},
        DataFor_AdvanceGazeState{
            .isInit = true,
            .callBottomUp = 0,
            .gazeFollower = true}));

/********************************************
 * CHECK GazeControl OverwriteGazeBehaviour *
 ********************************************/

TEST(GazeControl_OverwriteGazeBehaviour, OverwriteGazeBehaviour_withoutGazeTargetChanged)
{
  GazeControlTester TEST_HELPER;

  GazeOverwrite gazeOverwrite;
  gazeOverwrite.hasGazeTargetChanged = false;
  gazeOverwrite.durationGazeTarget = 10.0_ms;

  TEST_HELPER.gazeControl.SET_DURATION_CURRENT_GAZE_STATE(5.0_ms);
  TEST_HELPER.gazeControl.SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(10.0_ms);

  TEST_HELPER.gazeControl.OverwriteGazeBehaviour(gazeOverwrite);

  auto result = TEST_HELPER.gazeControl.GET_REQUIRED_DURATION_CURRENT_GAZE_STATE();

  ASSERT_EQ(result, 10.0_ms);
}

TEST(GazeControl_OverwriteGazeBehaviour, OverwriteGazeBehaviour_withGazeTargetChanged)
{
  GazeControlTester TEST_HELPER;

  GazeOverwrite gazeOverwrite;
  gazeOverwrite.hasGazeTargetChanged = true;
  gazeOverwrite.durationGazeTarget = 10.0_ms;

  TEST_HELPER.gazeControl.SET_CURRENT_GAZE_STATE(GazeState::EGO_FRONT);
  TEST_HELPER.gazeControl.SET_DURATION_CURRENT_GAZE_STATE(10.0_ms);
  TEST_HELPER.gazeControl.SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(5.0_ms);

  ON_CALL(TEST_HELPER.fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1.0));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100._ms));

  TEST_HELPER.gazeControl.OverwriteGazeBehaviour(gazeOverwrite);
  auto previousFixationTarget = TEST_HELPER.gazeControl.GET_PREVIOUS_FIXATION_TARGET();

  ASSERT_EQ(previousFixationTarget, GazeState::EGO_FRONT);
}

/*****************************************
 * CHECK GazeControl InitializeGazeState *
 *****************************************/
struct DataFor_InitializeGazeState
{
  double distribution;
  GazeState nextFixationTarget;
  units::time::millisecond_t requiredDurationCurrentGazeState;
  GazeState plannedGazeState;
};
class GazeControl_InitializeGazeState : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_InitializeGazeState>
{
};
TEST_P(GazeControl_InitializeGazeState, InitializeGazeState)
{
  GazeControlTester TEST_HELPER;
  DataFor_InitializeGazeState data = GetParam();

  ON_CALL(TEST_HELPER.fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(data.distribution));
  ON_CALL(TEST_HELPER.fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(data.distribution));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100._ms));

  TEST_HELPER.gazeControl.InitializeGazeState();
  auto plannedGazeState = TEST_HELPER.gazeControl.GET_PLANNED_GAZE_STATE();
  auto requiredDurationCurrentGazeState = TEST_HELPER.gazeControl.GET_REQUIRED_DURATION_CURRENT_GAZE_STATE();
  auto nextFixationTarget = TEST_HELPER.gazeControl.GET_NEXT_FIXATION_TARGET();

  ASSERT_EQ(nextFixationTarget, data.nextFixationTarget);
  ASSERT_EQ(requiredDurationCurrentGazeState, data.requiredDurationCurrentGazeState);
  ASSERT_EQ(plannedGazeState, data.plannedGazeState);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeControl_InitializeGazeState,
    testing::Values(
        DataFor_InitializeGazeState{
            .distribution = 0.0,
            .nextFixationTarget = GazeState::EGO_FRONT,
            .requiredDurationCurrentGazeState = 0_ms,
            .plannedGazeState = GazeState::SACCADE},
        DataFor_InitializeGazeState{
            .distribution = 1.0,
            .nextFixationTarget = GazeState::EGO_FRONT,
            .requiredDurationCurrentGazeState = 100_ms,
            .plannedGazeState = GazeState::EGO_FRONT}));

/***********************************************
 * CHECK GazeControl GenerateInformationCycles *
 ***********************************************/

TEST(GazeControl_GenerateInformationCycles, GenerateInformationCycles)
{
  GazeControlTester TEST_HELPER;

  auto result = TEST_HELPER.gazeControl.GenerateInformationCycles(1000.0_ms, 2000.0_ms);

  ASSERT_EQ(result.size(), 10);
  ASSERT_EQ(result.at(0), 11);
  ASSERT_EQ(result.at(9), 20);
}

/****************************************
 * CHECK GazeControl UpdateNewGazeState *
 ****************************************/
struct DataFor_UpdateNewGazeState
{
  GazeState currentGazeState;
  units::angle::radian_t gazeAngleHorizontal;
  bool isNewInformationAcquisitionRequested;
  units::time::millisecond_t cycleTime;
  units::time::millisecond_t requiredDurationCurrentGazeState;
  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_UpdateNewGazeState& obj)
  {
    return os
           << "  currentGazeState (GazeState): " << ScmCommons::GazeStateToString(obj.currentGazeState)
           << "  gazeAngleHorizontal (double): " << obj.gazeAngleHorizontal
           << " isNewInformationAcquisitionRequested (bool): " << obj.isNewInformationAcquisitionRequested
           << " cycleTime (int): " << obj.cycleTime
           << " requiredDurationCurrentGazeState (int): " << obj.requiredDurationCurrentGazeState;
  }
};
class GazeControl_UpdateNewGazeState : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_UpdateNewGazeState>
{
};
TEST_P(GazeControl_UpdateNewGazeState, UpdateNewGazeState)
{
  GazeControlTester TEST_HELPER;
  DataFor_UpdateNewGazeState data = GetParam();

  GazeStateAndAngle gazeStateAndAngle{
      .currentGazeState = data.currentGazeState,
      .gazeAngleHorizontal = data.gazeAngleHorizontal};

  ON_CALL(TEST_HELPER.fakeUpdateCurrentGazeState, UpdateCurrentGazeState(_, _, _)).WillByDefault(Return(gazeStateAndAngle));

  TEST_HELPER.gazeControl.SET_CYCLETIME(data.cycleTime);
  TEST_HELPER.gazeControl.SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(data.requiredDurationCurrentGazeState);
  TEST_HELPER.gazeControl.SET_DURATION_CURRENT_GAZE_STATE(0_ms);

  TEST_HELPER.gazeControl.UpdateNewGazeState();
  auto isNewInformationAcquisitionRequested = TEST_HELPER.gazeControl.GET_NEW_INFORMATION_ACQUISITION_REQUESTED();

  ASSERT_EQ(isNewInformationAcquisitionRequested, data.isNewInformationAcquisitionRequested);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeControl_UpdateNewGazeState,
    testing::Values(
        DataFor_UpdateNewGazeState{
            .currentGazeState = GazeState::SACCADE,
            .gazeAngleHorizontal = 0_rad,
            .isNewInformationAcquisitionRequested = false,
            .cycleTime = 0_ms,
            .requiredDurationCurrentGazeState = 0_ms},
        DataFor_UpdateNewGazeState{
            .currentGazeState = GazeState::EGO_FRONT,
            .gazeAngleHorizontal = 100_rad,
            .isNewInformationAcquisitionRequested = true,
            .cycleTime = 100_ms,
            .requiredDurationCurrentGazeState = 0_ms},
        DataFor_UpdateNewGazeState{
            .currentGazeState = GazeState::EGO_FRONT,
            .gazeAngleHorizontal = 100_rad,
            .isNewInformationAcquisitionRequested = false,
            .cycleTime = 0_ms,
            .requiredDurationCurrentGazeState = 100_ms},
        DataFor_UpdateNewGazeState{
            .currentGazeState = GazeState::EGO_FRONT,
            .gazeAngleHorizontal = 100_rad,
            .isNewInformationAcquisitionRequested = false,
            .cycleTime = 30_ms,
            .requiredDurationCurrentGazeState = 126_ms}));

/***************************************
 * CHECK GazeControl ChooseNextGazeState *
 ****************************************/

struct DataFor_ChooseNextGazeState
{
  units::time::millisecond_t durationCurrentGazeState;
  units::time::millisecond_t requiredDurationCurrentGazeState;
  units::time::millisecond_t resultRequiredDurationCurrentGazeState;
  GazeState currentGazeState;
  GazeState nextFixationTarget;
  GazeState resultPlannedGazeState;
};
class GazeControl_ChooseNextGazeState : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_ChooseNextGazeState>
{
};
TEST_P(GazeControl_ChooseNextGazeState, ChooseNextGazeState)
{
  GazeControlTester TEST_HELPER;
  DataFor_ChooseNextGazeState data = GetParam();

  TEST_HELPER.gazeControl.SET_DURATION_CURRENT_GAZE_STATE(data.durationCurrentGazeState);
  TEST_HELPER.gazeControl.SET_REQUIRED_DURATION_CURRENT_GAZE_STATE(data.requiredDurationCurrentGazeState);

  TEST_HELPER.gazeControl.SET_CYCLETIME(100_ms);
  TEST_HELPER.gazeControl.SET_CURRENT_GAZE_STATE(data.currentGazeState);
  TEST_HELPER.gazeControl.SET_NEXT_FIXATION_TARGET(data.nextFixationTarget);
  ON_CALL(TEST_HELPER.fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1.0));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));

  std::map<GazeState, double> gazeStateIntensities;
  gazeStateIntensities[GazeState::EGO_FRONT] = 5.0;
  TEST_HELPER.gazeControl.SET_GAZE_STATE_INTENSITIES(gazeStateIntensities);

  TEST_HELPER.gazeControl.ChooseNextGazeState();
  auto resultRequiredDurationCurrentGazeState = TEST_HELPER.gazeControl.GET_REQUIRED_DURATION_CURRENT_GAZE_STATE();
  auto resultPlannedGazeState = TEST_HELPER.gazeControl.GET_PLANNED_GAZE_STATE();

  ASSERT_EQ(resultRequiredDurationCurrentGazeState, data.resultRequiredDurationCurrentGazeState);
  ASSERT_EQ(resultPlannedGazeState, data.resultPlannedGazeState);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeControl_ChooseNextGazeState,
    testing::Values(
        DataFor_ChooseNextGazeState{
            .durationCurrentGazeState = 100_ms,
            .requiredDurationCurrentGazeState = 200_ms,
            .resultRequiredDurationCurrentGazeState = 200_ms,
            .currentGazeState = GazeState::SACCADE,
            .nextFixationTarget = GazeState::EGO_FRONT,
            .resultPlannedGazeState = GazeState::SACCADE},
        DataFor_ChooseNextGazeState{
            .durationCurrentGazeState = 200_ms,
            .requiredDurationCurrentGazeState = 100_ms,
            .resultRequiredDurationCurrentGazeState = 100_ms,
            .currentGazeState = GazeState::SACCADE,
            .nextFixationTarget = GazeState::EGO_FRONT,
            .resultPlannedGazeState = GazeState::EGO_FRONT},
        DataFor_ChooseNextGazeState{
            .durationCurrentGazeState = 200_ms,
            .requiredDurationCurrentGazeState = 100_ms,
            .resultRequiredDurationCurrentGazeState = 0_ms,
            .currentGazeState = GazeState::EGO_FRONT,
            .nextFixationTarget = GazeState::EGO_FRONT,
            .resultPlannedGazeState = GazeState::SACCADE}));

/*****************************************
 * CHECK GazeControl DrawSaccadeDuration *
 *****************************************/

struct DataFor_DrawSaccadeDuration
{
  GazeState nextFixationTarget;
  GazeState previousFixationTarget;
  int normalDistribution;
  units::time::millisecond_t result;
};
class GazeControl_DrawSaccadeDuration : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_DrawSaccadeDuration>
{
};
TEST_P(GazeControl_DrawSaccadeDuration, DrawSaccadeDuration)
{
  GazeControlTester TEST_HELPER;
  DataFor_DrawSaccadeDuration data = GetParam();

  TEST_HELPER.gazeControl.SET_NEXT_FIXATION_TARGET(data.nextFixationTarget);

  ON_CALL(TEST_HELPER.fakeStochastics, GetNormalDistributed(_, _)).WillByDefault(Return(data.normalDistribution));
  ON_CALL(TEST_HELPER.fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1.0));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));

  auto result = TEST_HELPER.gazeControl.DrawSaccadeDuration();

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeControl_DrawSaccadeDuration,
    testing::Values(
        DataFor_DrawSaccadeDuration{
            .nextFixationTarget = GazeState::EGO_REAR,
            .previousFixationTarget = GazeState::LEFT_REAR,
            .normalDistribution = 200,
            .result = 200_ms},
        DataFor_DrawSaccadeDuration{
            .nextFixationTarget = GazeState::EGO_REAR,
            .previousFixationTarget = GazeState::EGO_REAR,
            .normalDistribution = 100,
            .result = 100_ms},
        DataFor_DrawSaccadeDuration{
            .nextFixationTarget = GazeState::EGO_FRONT,
            .previousFixationTarget = GazeState::LEFT_REAR,
            .normalDistribution = 1,
            .result = 0_ms}));

/*******************************************************
 * CHECK GazeControl RoundGazeStateDurationToCycleTime *
 *******************************************************/

TEST(GazeControl_RoundGazeStateDurationToCycleTime, DrawSaccadeDuration)
{
  GazeControlTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));
  auto result = TEST_HELPER.gazeControl.RoundGazeStateDurationToCycleTime(100_ms, 50_ms);

  ASSERT_EQ(result, 100_ms);
}

/*******************************
 * CHECK GetHafSignalProcessed *
 *******************************/

TEST(GazeControl_GetHafSignalProcessed, Check_GetHafSignalProcessed)
{
  GazeControlTester TEST_HELPER;

  TEST_HELPER.gazeControl.SetHafSignalProcessed(true);
  EXPECT_TRUE(TEST_HELPER.gazeControl.GetHafSignalProcessed());
}
