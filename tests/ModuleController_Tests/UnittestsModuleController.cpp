/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Fakes/FakeFmiLogInterface.h"
#include "src/ModuleController.h"

struct ModuleControllerTester
{
  class ModuleControllerUnderTest : scm::ModuleController
  {
  public:
    template <typename... Args>
    ModuleControllerUnderTest(Args&&... args)
        : ModuleController{std::forward<Args>(args)...} {};

    using ModuleController::ExtractHostVehicleProperties;

    scm::signal::SystemInput GET_SYSTEMINPUT()
    {
      return _system_input;
    }
  };

  template <typename T>
  T& WITH_GROUNDTRUTH(T& groundTruth)
  {
    groundTruth.mutable_host_vehicle_id()->set_value(1);
    osi3::MovingObject* vehicle = groundTruth.add_moving_object();
    vehicle->mutable_base()->mutable_position()->set_x(0);
    vehicle->mutable_base()->mutable_position()->set_y(0);
    vehicle->mutable_base()->mutable_dimension()->set_width(2);
    vehicle->mutable_base()->mutable_dimension()->set_length(1.5);
    vehicle->mutable_base()->mutable_position()->set_y(0);
    vehicle->mutable_id()->set_value(1);

    return groundTruth;
  }

  ModuleControllerTester()
      : fakeFmiLogInterface{},
        parameters{{"Vehicle.SteeringRatio", 10.0},
                   {"Vehicle.MaxSteering", 5.0},
                   {"Simulation.CycleTime", 100},
                   {"Simulation.RandomSeed", 123},
                   {"Simulation.OutputPath", "/Agent/path"},
                   {"Simulation.ConfigPath", "/path"}},
        moduleController(parameters, WITH_GROUNDTRUTH(groundTruth), fakeFmiLogInterface)
  {
  }

  FmiParameters parameters;
  osi3::GroundTruth groundTruth;
  ::testing::NiceMock<FakeFmiLogInterface> fakeFmiLogInterface;
  ModuleControllerUnderTest moduleController;
};

/**************************************
 * CHECK ExtractHostVehicleProperties *
 **************************************/

TEST(ModuleController_ExtractHostVehicleProperties, Check_ExtractHostVehicleProperties)
{
  ModuleControllerTester TEST_HELPER;
  osi3::MovingObject* vehicle = TEST_HELPER.groundTruth.mutable_moving_object(0);
  vehicle->mutable_base()->mutable_position()->set_x(0);
  vehicle->mutable_base()->mutable_position()->set_y(0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_x(4.0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_y(5.0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_z(6.0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_x(2.0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_y(3.0);
  vehicle->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_z(4.0);

  FmiParameters vehicleParameters;
  TEST_HELPER.moduleController.ExtractHostVehicleProperties(*vehicle, TEST_HELPER.parameters);

  auto result = TEST_HELPER.moduleController.GET_SYSTEMINPUT();

  ASSERT_DOUBLE_EQ(result.steeringRatio, 10.0);
  ASSERT_DOUBLE_EQ(result.frontAxleMaxSteering, std::get<double>(TEST_HELPER.parameters["Vehicle.SteeringRatio"]) * std::get<double>(TEST_HELPER.parameters["Vehicle.MaxSteering"]));
  ASSERT_EQ(result.wheelBase, 2.0_m);
}