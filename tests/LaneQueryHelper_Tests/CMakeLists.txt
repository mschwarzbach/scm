################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME LaneQueryHelper_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN

  SOURCES
  UnittestsLaneQueryHelper.cpp
    ${DRIVER_DIR}/src/LaneQueryHelper.cpp
    
    HEADERS
    ${DRIVER_DIR}/src/LaneQueryHelper.h
    ${ROOT_DIR}/include/common/ScmDefinitions.h

  INCDIRS
  ${DRIVER_DIR}
)

target_compile_definitions(${COMPONENT_TEST_NAME} PRIVATE
  PARAMETERS_SCM_TEST
  PARAMETERS_VEHICLE_TEST
  SCM_TESTING
  TESTING_ENABLED
)
