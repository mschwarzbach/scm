/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/LaneQueryHelper.h"

/********************************
 * CHECK GetRelativeLaneFromAoi *
 ********************************/

struct DataFor_GetRelativeLaneFromAoi
{
  AreaOfInterest aoi;
  RelativeLane expectedResult;
};

class LaneQueryHelper_GetRelativeLaneFromAoi : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_GetRelativeLaneFromAoi>
{
};

TEST_P(LaneQueryHelper_GetRelativeLaneFromAoi, Check_GetRelativeLaneFromAoi)
{
  DataFor_GetRelativeLaneFromAoi data = GetParam();
  auto result = LaneQueryHelper::GetRelativeLaneFromAoi(data.aoi);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneQueryHelper_GetRelativeLaneFromAoi,
    testing::Values(
        DataFor_GetRelativeLaneFromAoi{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .expectedResult = RelativeLane::LEFTLEFT},
        DataFor_GetRelativeLaneFromAoi{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = RelativeLane::LEFT},
        DataFor_GetRelativeLaneFromAoi{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .expectedResult = RelativeLane::RIGHT},
        DataFor_GetRelativeLaneFromAoi{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .expectedResult = RelativeLane::RIGHTRIGHT}));

TEST(LaneQueryHelper_GetRelativeLaneFromAoi, Check_Assert)
{
  EXPECT_THROW(LaneQueryHelper::GetRelativeLaneFromAoi(AreaOfInterest::DISTRACTION), std::runtime_error);
}

/*********************************
 * CHECK GetSideFromRelativeLane *
 *********************************/

struct DataFor_GetSideFromRelativeLane
{
  RelativeLane lane;
  Side expectedResult;
};

class LaneQueryHelper_GetSideFromRelativeLane : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_GetSideFromRelativeLane>
{
};

TEST_P(LaneQueryHelper_GetSideFromRelativeLane, Check_GetSideFromRelativeLane)
{
  DataFor_GetSideFromRelativeLane data = GetParam();
  auto result = LaneQueryHelper::GetSideFromRelativeLane(data.lane);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneQueryHelper_GetSideFromRelativeLane,
    testing::Values(
        DataFor_GetSideFromRelativeLane{
            .lane = RelativeLane::RIGHT,
            .expectedResult = Side::Right},
        DataFor_GetSideFromRelativeLane{
            .lane = RelativeLane::LEFT,
            .expectedResult = Side::Left}));

TEST(LaneQueryHelper_GetSideFromRelativeLane, Check_Assert)
{
  EXPECT_THROW(LaneQueryHelper::GetSideFromRelativeLane(RelativeLane::EGO), std::runtime_error);
}