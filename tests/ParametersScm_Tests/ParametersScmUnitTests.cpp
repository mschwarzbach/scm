/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "DriverParameterModifier.h"

/***************************************************
 * CHECK AdjustDriverParameter ComfortAcceleration *
 ***************************************************/

struct DataFor_AdjustDriverParameter_ComfortAcceleration
{
  std::string testCase;
  units::velocity::meters_per_second_t velocity;
  units::acceleration::meters_per_second_squared_t expected_comfortLongitdinalAcceleration;
  units::acceleration::meters_per_second_squared_t expected_comfortLongitdinalDeceleration;
};

class ParameterScm_Unittests_AdjustDriverParameter_ComfortAcceleration : public ::testing::Test,
                                                                         public ::testing::WithParamInterface<DataFor_AdjustDriverParameter_ComfortAcceleration>
{
};

TEST_P(ParameterScm_Unittests_AdjustDriverParameter_ComfortAcceleration, ParameterScm_AdjustComfortAcceleration)
{
  DataFor_AdjustDriverParameter_ComfortAcceleration data = GetParam();

  DriverParameterModifications modifications;
  modifications.maxComfortAccelerationFactor = 4.0;
  modifications.maxComfortDecelerationFactor = 2.0;
  modifications.accelerationAdjustmentThresholdVelocity = 25.0_mps;
  modifications.velocityForMaxAcceleration = 8.0_mps;

  ComfortAccelerationParameterSet set;
  set.comfortAcceleration = 1.0_mps_sq;
  set.comfortDeceleration = 1.2_mps_sq;
  set.modifications = modifications;

  DriverParameters driverParameters;
  DriverParameterModifier::ComfortAcceleration(data.velocity, set, driverParameters);

  EXPECT_NEAR(driverParameters.comfortLongitudinalAcceleration.value(), data.expected_comfortLongitdinalAcceleration.value(), 0.01);
  EXPECT_NEAR(driverParameters.comfortLongitudinalDeceleration.value(), data.expected_comfortLongitdinalDeceleration.value(), 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ParameterScm_Unittests_AdjustDriverParameter_ComfortAcceleration,
    testing::Values(
        DataFor_AdjustDriverParameter_ComfortAcceleration{
            .testCase = "Velocity-Larger-Than-Threshold_Leads-To-Default-Accelerations",
            .velocity = 30.0_mps,
            .expected_comfortLongitdinalAcceleration = 1.0_mps_sq,
            .expected_comfortLongitdinalDeceleration = 1.2_mps_sq},
        DataFor_AdjustDriverParameter_ComfortAcceleration{
            .testCase = "Velocity-Lower-Than-And-Close-To-Threshold_Leads-To-Slightly-Higher-Accelerations",
            .velocity = 20.0_mps,
            .expected_comfortLongitdinalAcceleration = 1.88_mps_sq,
            .expected_comfortLongitdinalDeceleration = 1.55_mps_sq},
        DataFor_AdjustDriverParameter_ComfortAcceleration{
            .testCase = "Velocity-Lower-Than-And-Not-So-Close-To-Threshold_Leads-To-Higher-Accelerations",
            .velocity = 14.0_mps,
            .expected_comfortLongitdinalAcceleration = 2.94_mps_sq,
            .expected_comfortLongitdinalDeceleration = 1.97_mps_sq},
        DataFor_AdjustDriverParameter_ComfortAcceleration{
            .testCase = "Velocity-Equal-To-Threshold_Leads-To-Max-Accelerations",
            .velocity = 8.0_mps,
            .expected_comfortLongitdinalAcceleration = 4.0_mps_sq,
            .expected_comfortLongitdinalDeceleration = 2.4_mps_sq},
        DataFor_AdjustDriverParameter_ComfortAcceleration{
            .testCase = "Velocity-Equal-To-Zero_Leads-Also-To-Max-Accelerations",
            .velocity = 0.0_mps,
            .expected_comfortLongitdinalAcceleration = 4.0_mps_sq,
            .expected_comfortLongitdinalDeceleration = 2.4_mps_sq}));
