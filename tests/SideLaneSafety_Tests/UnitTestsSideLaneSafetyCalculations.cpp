/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-actions.h>
#include <gmock/gmock.h>
#include <gtest/gtest-param-test.h>
#include <gtest/gtest.h>

#include <ostream>

#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "SideLaneSafety/SideLaneSafetyCalculations.h"
#include "SideLaneSafety/SideLaneSafetyTypes.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/******************************************************
 * CHECK CanFinishLaneChangeWithoutEnteringMinDistance *
 *******************************************************/

struct DataFor_CanFinishLaneChangeWithoutEnteringMinDistance
{
  std::string testcase;
  units::length::meter_t minDistance;
  units::length::meter_t currentDistance;
  units::acceleration::meters_per_second_squared_t opponentAcceleration;
  units::velocity::meters_per_second_t longitudinalVelocityDelta;
  units::time::second_t laneChangeDuration;
  bool expected_canFinishLaneChange;
};

class TestCanFinishLaneChangeWithoutEnteringMinDistance : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CanFinishLaneChangeWithoutEnteringMinDistance>
{
};

TEST_P(TestCanFinishLaneChangeWithoutEnteringMinDistance, ReturnsTrueIfEstimatedEndDistanceIsLargerOrEqualToMinDistance)
{
  const auto param = GetParam();
  NiceMock<FakeSurroundingVehicle> opponent;

  const bool BEHIND_EGO{param.currentDistance < 0_m};
  ON_CALL(opponent, BehindEgo).WillByDefault(Return(BEHIND_EGO));
  ON_CALL(opponent, GetLongitudinalAcceleration).WillByDefault(Return(param.opponentAcceleration));
  ON_CALL(opponent, GetRelativeNetDistance).WillByDefault(Return(param.currentDistance));

  NiceMock<FakeSurroundingVehicleQueryFactory> queryFactory;
  auto query = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(queryFactory, GetQuery(::testing::Ref(opponent))).WillByDefault(Return(query));
  EXPECT_CALL(*query, GetLongitudinalVelocityDelta).Times(1).WillOnce(Return(param.longitudinalVelocityDelta));

  SideLaneSafetyTypes::TargetLaneVehicleInformation vehicleInfo;
  vehicleInfo.vehicle = &opponent;
  vehicleInfo.minDistance = param.minDistance;

  SideLaneSafetyCalculations sideLaneSafetyCalculations;
  const auto result = sideLaneSafetyCalculations.CanFinishLaneChangeWithoutEnteringMinDistance(param.laneChangeDuration, vehicleInfo, queryFactory);

  ASSERT_EQ(param.expected_canFinishLaneChange, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TestCanFinishLaneChangeWithoutEnteringMinDistance,
    ::testing::Values(
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Vehicle in front of ego, ego faster, enough space in front",
            .minDistance = 10.0_m,
            .currentDistance = 20.0_m,
            .opponentAcceleration = 0.0_mps_sq,
            .longitudinalVelocityDelta = 1.0_mps,
            .laneChangeDuration = 1.0_s,
            .expected_canFinishLaneChange = true},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Vehicle in front of ego, ego faster, not enough space - lane change too long",
            .minDistance = 10.0_m,
            .currentDistance = 20.0_m,
            .opponentAcceleration = 0.0_mps_sq,
            .longitudinalVelocityDelta = 1.0_mps,
            .laneChangeDuration = 11.0_s,
            .expected_canFinishLaneChange = false},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Opponent decelerating, ego faster, not enough space",
            .minDistance = 10.0_m,
            .currentDistance = 15.0_m,
            .opponentAcceleration = -2.0_mps_sq,
            .longitudinalVelocityDelta = 1.0_mps,
            .laneChangeDuration = 2.0_s,
            .expected_canFinishLaneChange = false},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Opponent decelerating, ego slower, enough space",
            .minDistance = 10.0_m,
            .currentDistance = 15.0_m,
            .opponentAcceleration = -2.0_mps_sq,
            .longitudinalVelocityDelta = -0.5_mps,
            .laneChangeDuration = 2.0_s,
            .expected_canFinishLaneChange = true},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Opponent behind, ego slower, not enough space",
            .minDistance = 10.0_m,
            .currentDistance = -15.0_m,
            .opponentAcceleration = 0.0_mps_sq,
            .longitudinalVelocityDelta = -1.0_mps,
            .laneChangeDuration = 6.0_s,
            .expected_canFinishLaneChange = false},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .testcase = "Opponent behind, ego slower, laneChange faster, enough space",
            .minDistance = 10.0_m,
            .currentDistance = -15.0_m,
            .opponentAcceleration = 0.0_mps_sq,
            .longitudinalVelocityDelta = -1.0_mps,
            .laneChangeDuration = 5.0_s,
            .expected_canFinishLaneChange = false}));