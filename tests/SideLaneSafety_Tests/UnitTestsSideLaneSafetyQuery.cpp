/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-actions.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "SideLaneSafety/SideLaneSafetyQuery.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/**************************************
 * CHECK GetLaneChangeSafetyParameters *
 ***************************************/

struct DataFor_GetLaneChangeSafetyParameters
{
  RelativeLane targetLane;
  units::length::meter_t laneChangeLength;
  units::velocity::meters_per_second_t egoVelocityLongitudinal;
  bool hasFrontVehicle;
  bool hasRearVehicle;
  double reductionFactorUrgency;
  units::length::meter_t minDistanceFront;
  units::length::meter_t minDistanceRear;
};

class TestGetLaneChangeSafetyParameters : public ::testing::Test, public ::testing::WithParamInterface<DataFor_GetLaneChangeSafetyParameters>
{
};

TEST_P(TestGetLaneChangeSafetyParameters, GivenVehicleIsPresentInTargetLane_ReturnsVehiclesWithCorrespondingMinDistance)
{
  const auto param = GetParam();
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  NiceMock<FakeSurroundingVehicle> frontVehicle;
  NiceMock<FakeSurroundingVehicle> rearVehicle;

  const auto FRONT_AOI{param.targetLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT};
  const auto REAR_AOI{param.targetLane == RelativeLane::LEFT ? AreaOfInterest::LEFT_REAR : AreaOfInterest::RIGHT_REAR};
  EXPECT_CALL(fakeMentalModel, GetVehicle(FRONT_AOI, _)).Times(1).WillOnce(Return(param.hasFrontVehicle ? &frontVehicle : nullptr));
  EXPECT_CALL(fakeMentalModel, GetVehicle(REAR_AOI, _)).Times(1).WillOnce(Return(param.hasRearVehicle ? &rearVehicle : nullptr));
  ON_CALL(fakeMentalModel, GetUrgencyReductionFactor).WillByDefault(Return(param.reductionFactorUrgency));
  ON_CALL(fakeMentalModel, GetLongitudinalVelocityEgo).WillByDefault(Return(param.egoVelocityLongitudinal));

  ON_CALL(fakeMentalCalculations, GetMinDistance(&frontVehicle, MinThwPerspective::EGO_ANTICIPATED_REAR, _)).WillByDefault(Return(param.minDistanceFront));
  ON_CALL(fakeMentalCalculations, GetMinDistance(&rearVehicle, MinThwPerspective::EGO_ANTICIPATED_FRONT, _)).WillByDefault(Return(param.minDistanceRear));

  SideLaneSafetyQuery sideLaneSafetyQuery(fakeMentalModel, fakeMentalCalculations, fakeSurroundingVehicleQueryFactory);
  auto result = sideLaneSafetyQuery.GetLaneChangeSafetyParameters(param.laneChangeLength, param.targetLane);

  const units::time::second_t EXPECTED_LANE_CHANGE_DURATION{param.laneChangeLength / param.egoVelocityLongitudinal};
  const auto EXPECTED_MIN_DISTANCE_FRONT{param.hasFrontVehicle ? param.minDistanceFront * param.reductionFactorUrgency : 0.0_m};
  const SurroundingVehicleInterface* EXPECTED_VEHICLE_FRONT{param.hasFrontVehicle ? &frontVehicle : nullptr};
  const auto EXPECTED_MIN_DISTANCE_REAR{param.hasRearVehicle ? param.minDistanceRear * param.reductionFactorUrgency : 0.0_m};
  const SurroundingVehicleInterface* EXPECTED_VEHICLE_REAR{param.hasRearVehicle ? &rearVehicle : nullptr};

  ASSERT_EQ(result.estimatedLaneChangeDuration, EXPECTED_LANE_CHANGE_DURATION);
  ASSERT_EQ(result.front.minDistance, EXPECTED_MIN_DISTANCE_FRONT);
  ASSERT_EQ(result.front.vehicle, EXPECTED_VEHICLE_FRONT);
  ASSERT_EQ(result.rear.minDistance, EXPECTED_MIN_DISTANCE_REAR);
  ASSERT_EQ(result.rear.vehicle, EXPECTED_VEHICLE_REAR);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TestGetLaneChangeSafetyParameters,
    ::testing::Values(
        DataFor_GetLaneChangeSafetyParameters{
            .targetLane = RelativeLane::RIGHT,
            .laneChangeLength = 100_m,
            .egoVelocityLongitudinal = 10_mps,
            .hasFrontVehicle = false,
            .hasRearVehicle = false,
            .reductionFactorUrgency = 0.5,
            .minDistanceFront = ScmDefinitions::INF_DISTANCE,
            .minDistanceRear = ScmDefinitions::INF_DISTANCE},
        DataFor_GetLaneChangeSafetyParameters{
            .targetLane = RelativeLane::LEFT,
            .laneChangeLength = 100_m,
            .egoVelocityLongitudinal = 10_mps,
            .hasFrontVehicle = true,
            .hasRearVehicle = false,
            .reductionFactorUrgency = 0.5,
            .minDistanceFront = 10_m,
            .minDistanceRear = ScmDefinitions::INF_DISTANCE},
        DataFor_GetLaneChangeSafetyParameters{
            .targetLane = RelativeLane::LEFT,
            .laneChangeLength = 100_m,
            .egoVelocityLongitudinal = 10_mps,
            .hasFrontVehicle = false,
            .hasRearVehicle = true,
            .reductionFactorUrgency = 0.5,
            .minDistanceFront = ScmDefinitions::INF_DISTANCE,
            .minDistanceRear = 5_m},
        DataFor_GetLaneChangeSafetyParameters{
            .targetLane = RelativeLane::LEFT,
            .laneChangeLength = 100_m,
            .egoVelocityLongitudinal = 10_mps,
            .hasFrontVehicle = true,
            .hasRearVehicle = true,
            .reductionFactorUrgency = 0.5,
            .minDistanceFront = 5_m,
            .minDistanceRear = 10_m}));