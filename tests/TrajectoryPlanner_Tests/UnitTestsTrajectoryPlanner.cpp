/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-spec-builders.h>
#include <gmock/gmock.h>
#include <gtest/gtest-param-test.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeLaneChangeTrajectoryCalculations.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "../Fakes/FakeTrajectoryPlanningQuery.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanner.h"
#include "TrajectoryPlanner/TrajectoryPlanningQueryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/*
 *  Test CalculateLaneChangeDimensions
 */
struct DataFor_CalculateLaneChangeDimensions
{
  RelativeLane targetLane;
  units::angle::radian_t mockHeading;
  Situation mockSituation;
  units::length::meter_t mockLaneChangeLength;
  units::length::meter_t mockLaneChangeWidth;
};

class TestCalculateLaneChangeDimensions : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CalculateLaneChangeDimensions>
{
};

TEST_P(TestCalculateLaneChangeDimensions, GivenQueryWithValidLane_AssertQueryAndTrajectoryCalcFunctionsCalledWithCorrectArgs)
{
  const auto param = GetParam();
  FakeSurroundingVehicleQueryFactory surroundingVehicleQueryFactory;
  auto fakeCalculations = std::make_shared<FakeLaneChangeTrajectoryCalculations>();
  auto fakeQuery = std::make_shared<NiceMock<FakeTrajectoryPlanningQuery>>();

  ON_CALL(*fakeQuery, GetSituation).WillByDefault(Return(param.mockSituation));
  ON_CALL(*fakeQuery, GetHeading).WillByDefault(Return(param.mockHeading));

  FakeSurroundingVehicle laneChangeObstacle;
  EXPECT_CALL(*fakeQuery, GetRelevantVehiclesForLaneChangeLength()).Times(1).WillOnce(Return(TrajectoryPlanning::RelevantVehiclesForLaneChangeLength()));
  EXPECT_CALL(*fakeCalculations, DetermineReferenceObjectForLaneChange(param.mockSituation, _)).Times(1).WillOnce(Return(&laneChangeObstacle));
  EXPECT_CALL(*fakeQuery, CreateLaneChangeLengthInput(param.targetLane, &laneChangeObstacle)).Times(1).WillOnce(Return(TrajectoryPlanning::LaneChangeLengthParameters()));
  EXPECT_CALL(*fakeCalculations, DetermineLaneChangeLength(_, _)).Times(1).WillOnce(Return(param.mockLaneChangeLength));

  EXPECT_CALL(*fakeQuery, CreateLaneChangeWidthInput(param.targetLane)).Times(1).WillOnce(Return(TrajectoryPlanning::LaneChangeWidthParameters()));
  EXPECT_CALL(*fakeCalculations, DetermineLaneChangeWidth(_)).Times(1).WillOnce(Return(param.mockLaneChangeWidth));

  TrajectoryPlanner trajectoryPlanner(fakeQuery,
                                      fakeCalculations,
                                      surroundingVehicleQueryFactory);

  const auto result = trajectoryPlanner.CalculateLaneChangeDimensions(param.targetLane);

  ASSERT_EQ(result.width, param.mockLaneChangeWidth);
  ASSERT_EQ(result.length, param.mockLaneChangeLength);
  ASSERT_EQ(result.headingStart, param.mockHeading);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TestCalculateLaneChangeDimensions,
    ::testing::Values(
        DataFor_CalculateLaneChangeDimensions{
            .targetLane = RelativeLane::RIGHT,
            .mockHeading = 0.123_rad,
            .mockSituation = Situation::FREE_DRIVING,
            .mockLaneChangeLength = 456.0_m,
            .mockLaneChangeWidth = 7.8_m},
        DataFor_CalculateLaneChangeDimensions{
            .targetLane = RelativeLane::LEFT,
            .mockHeading = -0.123_rad,
            .mockSituation = Situation::FREE_DRIVING,
            .mockLaneChangeLength = 456.0_m,
            .mockLaneChangeWidth = -7.8_m}));