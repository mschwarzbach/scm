/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-actions.h>
#include <gmock/gmock-spec-builders.h>
#include <gmock/gmock.h>
#include <gtest/gtest-param-test.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "TrajectoryPlanner/TrajectoryPlanningQuery.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

/********************************
 * CHECK CreateDriverParameters *
 ********************************/

TEST(TrajectoryPlanningQuery_CreateDriverParameters, ReadsValuesFromDriverParametersAndWritesToOutputStruct)
{
  const auto COMFORT_STEERING_WHEEL_VELOCITY{1.23_rad_per_s};
  const auto MAX_STEERING_WHEEL_VELOCITY{4.56_rad_per_s};
  const units::acceleration::meters_per_second_squared_t COMFORT_LATERAL_ACCELERATION{7.8_mps_sq};
  const units::acceleration::meters_per_second_squared_t MAX_LATERAL_ACCELERATION{9.1_mps_sq};

  DriverParameters driverParams;
  driverParams.comfortAngularVelocitySteeringWheel = COMFORT_STEERING_WHEEL_VELOCITY;
  driverParams.maximumAngularVelocitySteeringWheel = MAX_STEERING_WHEEL_VELOCITY;
  driverParams.comfortLateralAcceleration = COMFORT_LATERAL_ACCELERATION;
  driverParams.maximumLateralAcceleration = MAX_LATERAL_ACCELERATION;

  FakeMentalModel mentalModel;
  ON_CALL(mentalModel, GetDriverParameters).WillByDefault(Return(driverParams));

  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.CreateDriverParameters();

  ASSERT_EQ(COMFORT_STEERING_WHEEL_VELOCITY, result.comfortAngularSteeringWheelVelocity);
  ASSERT_EQ(MAX_STEERING_WHEEL_VELOCITY, result.maxAngularSteeringWheelVelocity);
  ASSERT_EQ(COMFORT_LATERAL_ACCELERATION, result.comfortLateralAcceleration);
  ASSERT_EQ(MAX_LATERAL_ACCELERATION, result.maxLateralAcceleration);
}

/********************************
 * CHECK CreateVehicleParameters *
 ********************************/

TEST(TrajectoryPlanningQuery_CreateVehicleParameters, ReadsValuesFromMentalModelAndWritesToOutputStruct)
{
  FakeMentalModel mentalModel;

  const units::length::meter_t WHEELBASE{10.0_m};
  const auto DISTANCE_BB_CENTER_FRONT_AXLE{4.0_m};
  const auto DISTANCE_BB_CENTER_REAR_AXLE{-(WHEELBASE - DISTANCE_BB_CENTER_FRONT_AXLE)};
  const double STEERING_RATIO{0.123};
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  vehicleParameters.front_axle.bb_center_to_axle_center = {DISTANCE_BB_CENTER_FRONT_AXLE.value(), 0.0, 0.0};
  vehicleParameters.rear_axle.bb_center_to_axle_center = {DISTANCE_BB_CENTER_REAR_AXLE.value(), 0.0, 0.0};
  vehicleParameters.properties["SteeringRatio"] = std::to_string(STEERING_RATIO);
  ON_CALL(mentalModel, GetVehicleModelParameters).WillByDefault(Return(vehicleParameters));

  const auto EGO_VELOCITY{50.0_mps};
  const auto LATERAL_POSITION_FRONT_AXLE{4.56_m};
  ON_CALL(mentalModel, GetAbsoluteVelocityEgo).WillByDefault(Return(EGO_VELOCITY));
  ON_CALL(mentalModel, GetLateralPositionFrontAxle).WillByDefault(Return(LATERAL_POSITION_FRONT_AXLE));

  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.CreateVehicleParameters();

  ASSERT_DOUBLE_EQ(STEERING_RATIO, result.steeringRatio);
  ASSERT_EQ(WHEELBASE, result.wheelbase);
  ASSERT_EQ(EGO_VELOCITY, result.absoluteVelocity);
  ASSERT_EQ(LATERAL_POSITION_FRONT_AXLE, result.tPosition);
}

/************************************
 * CHECK CreateLaneChangeLengthInput *
 *************************************/

TEST(TrajectoryPlanningQuery_CreateLaneChangeLengthParameters, ReadsValuesFromMentalModelAndWritesToOutputStruct)
{
  NiceMock<FakeMentalModel> mentalModel;

  const auto TIME_HEADWAY_FREE_LANE_CHANGE{45.0_s};
  DriverParameters driverParams;
  driverParams.timeHeadwayFreeLaneChange = TIME_HEADWAY_FREE_LANE_CHANGE;
  ON_CALL(mentalModel, GetDriverParameters).WillByDefault(Return(driverParams));

  const units::velocity::meters_per_second_t EGO_VELOCITY{50.0};
  ON_CALL(mentalModel, GetLongitudinalVelocityEgo).WillByDefault(Return(EGO_VELOCITY));

  const auto DISTANCE_TO_END_OF_LANE_EGO{123.0_m};
  LaneInformationGeometrySCM egoLaneInformation;
  egoLaneInformation.distanceToEndOfLane = DISTANCE_TO_END_OF_LANE_EGO;
  FakeInfrastructureCharacteristics infrastructureCharacteristics;
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(egoLaneInformation));
  ON_CALL(mentalModel, GetInfrastructureCharacteristics).WillByDefault(Return(&infrastructureCharacteristics));

  NiceMock<FakeSurroundingVehicle> laneChangeObstacle;
  RelativeLane targetLane{RelativeLane::LEFT};
  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.CreateLaneChangeLengthInput(targetLane, &laneChangeObstacle);

  ASSERT_EQ(TIME_HEADWAY_FREE_LANE_CHANGE, result.timeHeadwayFreeLaneChange);
  ASSERT_EQ(EGO_VELOCITY, result.egoVelocityLongitudinal);
  ASSERT_EQ(DISTANCE_TO_END_OF_LANE_EGO, result.distanceToEndOfEgoLane);
  ASSERT_EQ(&laneChangeObstacle, result.laneChangeObstacle);
}

/**********************************************
 * CHECK LaneChangeLengthParametersHighwayExit *
 ***********************************************/

struct DataFor_LaneChangeLengthParametersHighwayExit
{
  std::string testcase;
  RelativeLane targetLane;
  bool rightHandTraffic;
  bool mandatoryExit;
  int numberOfLaneChangesRequired;
  units::length::meter_t distanceToStartOfExit;
  units::length::meter_t distanceToEndOfExit;
  std::optional<TrajectoryPlanning::HighwayExitInformation> expected_information;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_LaneChangeLengthParametersHighwayExit& rhs)
  {
    os << "testcase: " << rhs.testcase;
    return os;
  }
};

class TrajectoryPlanningQuery_CreateLaneChangeLengthParametersTestHighwayExit : public ::testing::Test,
                                                                                public ::testing::WithParamInterface<DataFor_LaneChangeLengthParametersHighwayExit>
{
public:
  void SET_MENTAL_MODEL_FAKES()
  {
    const auto TIME_HEADWAY_FREE_LANE_CHANGE{45.0_s};
    driverParams.timeHeadwayFreeLaneChange = TIME_HEADWAY_FREE_LANE_CHANGE;
    ON_CALL(mentalModel, GetDriverParameters).WillByDefault(Return(driverParams));
    ON_CALL(mentalModel, GetInfrastructureCharacteristics).WillByDefault(Return(&infrastructureCharacteristics));
    ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry).WillByDefault(ReturnRef(laneInformation));
  }

  NiceMock<FakeMentalModel> mentalModel;
  DriverParameters driverParams;
  NiceMock<FakeInfrastructureCharacteristics> infrastructureCharacteristics;
  LaneInformationGeometrySCM laneInformation;
};

namespace
{
static constexpr units::length::meter_t APPROXIMATE_EXIT_LENGTH{250.0_m};
static constexpr bool MANDATORY_EXIT_ACTIVE{true};
static constexpr bool MANDATORY_EXIT_INACTIVE{false};
static constexpr bool RIGHTHAND_TRAFFIC{true};
static constexpr bool LEFTHAND_TRAFFIC{false};
}  // namespace

TEST_P(TrajectoryPlanningQuery_CreateLaneChangeLengthParametersTestHighwayExit, GivenMandatoryExitAndLaneChangeRequired_ReturnExitInformation_ElseNullopt)
{
  const auto data = GetParam();
  SET_MENTAL_MODEL_FAKES();

  ON_CALL(mentalModel, IsRightHandTraffic).WillByDefault(Return(data.rightHandTraffic));
  ON_CALL(mentalModel, IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT)).WillByDefault(Return(data.mandatoryExit));
  ON_CALL(mentalModel, GetClosestRelativeLaneIdForJunctionIngoing).WillByDefault(Return(data.numberOfLaneChangesRequired));
  ON_CALL(mentalModel, GetDistanceToStartOfNextExit).WillByDefault(Return(data.distanceToStartOfExit));
  ON_CALL(mentalModel, GetDistanceToEndOfNextExit).WillByDefault(Return(data.distanceToEndOfExit));

  auto query = TrajectoryPlanningQuery(mentalModel);
  NiceMock<FakeSurroundingVehicle> laneChangeObstacle;
  auto result = query.CreateLaneChangeLengthInput(data.targetLane, &laneChangeObstacle);

  ASSERT_EQ(result.highwayExitInformation.has_value(), data.expected_information.has_value());
  if (data.expected_information.has_value())
  {
    ASSERT_EQ(*result.highwayExitInformation, *data.expected_information);
  }
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    TrajectoryPlanningQuery_CreateLaneChangeLengthParametersTestHighwayExit,
    ::testing::Values(
        DataFor_LaneChangeLengthParametersHighwayExit{
            .testcase = "GivenRightHandTraffic, LaneChangeToTheRight, MandatoryExit, Return distanceToEndOfExit",
            .targetLane = RelativeLane::RIGHT,
            .rightHandTraffic = RIGHTHAND_TRAFFIC,
            .mandatoryExit = MANDATORY_EXIT_ACTIVE,
            .numberOfLaneChangesRequired = 2,
            .distanceToStartOfExit = 100_m,
            .distanceToEndOfExit = 200_m,
            .expected_information = TrajectoryPlanning::HighwayExitInformation{200_m, 2}},
        DataFor_LaneChangeLengthParametersHighwayExit{
            .testcase = "GivenRightHandTraffic, LaneChangeToTheLeft, MandatoryExit, Return nullopt",
            .targetLane = RelativeLane::LEFT,
            .rightHandTraffic = RIGHTHAND_TRAFFIC,
            .mandatoryExit = MANDATORY_EXIT_ACTIVE,
            .numberOfLaneChangesRequired = 2,
            .distanceToStartOfExit = 100_m,
            .distanceToEndOfExit = 200_m,
            .expected_information = std::nullopt},
        DataFor_LaneChangeLengthParametersHighwayExit{
            .testcase = "GivenRightHandTraffic, LaneChangeToTheRight, NoMandatoryExit, Return nullopt",
            .targetLane = RelativeLane::RIGHT,
            .rightHandTraffic = RIGHTHAND_TRAFFIC,
            .mandatoryExit = MANDATORY_EXIT_INACTIVE,
            .numberOfLaneChangesRequired = 2,
            .distanceToStartOfExit = 100_m,
            .distanceToEndOfExit = 200_m,
            .expected_information = std::nullopt},
        DataFor_LaneChangeLengthParametersHighwayExit{
            .testcase = "GivenRightHandTraffic, LaneChangeToTheRight, MandatoryExit, DistanceToEndOfExit invalid, Return approximate distanceToEndOfExit",
            .targetLane = RelativeLane::RIGHT,
            .rightHandTraffic = RIGHTHAND_TRAFFIC,
            .mandatoryExit = MANDATORY_EXIT_ACTIVE,
            .numberOfLaneChangesRequired = -1,
            .distanceToStartOfExit = 100_m,
            .distanceToEndOfExit = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE),
            .expected_information = TrajectoryPlanning::HighwayExitInformation{100_m + APPROXIMATE_EXIT_LENGTH, 1}}));

/***********************************************
 * CHECK GetRelevantVehiclesForLaneChangeLength *
 ************************************************/

TEST(TrajectoryPlanningQuery_CreateRelevantVehiclesForLaneChangeLength, ReadsValuesFromMentalModelAndWritesToOutputStruct)
{
  FakeMentalModel mentalModel;

  FakeSurroundingVehicle egoFrontVehicle;
  FakeSurroundingVehicle causingVehicleFront;
  FakeSurroundingVehicle causingVehicleSide;

  AreaOfInterest CAUSING_AOI_FRONT{AreaOfInterest::EGO_FRONT_FAR};
  AreaOfInterest CAUSING_AOI_SIDE{AreaOfInterest::RIGHT_FRONT};
  ON_CALL(mentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&egoFrontVehicle));
  ON_CALL(mentalModel, GetVehicle(CAUSING_AOI_FRONT, _)).WillByDefault(Return(&causingVehicleFront));
  ON_CALL(mentalModel, GetVehicle(CAUSING_AOI_SIDE, _)).WillByDefault(Return(&causingVehicleSide));
  ON_CALL(mentalModel, GetCausingVehicleOfSituationFrontCluster).WillByDefault(Return(CAUSING_AOI_FRONT));
  ON_CALL(mentalModel, GetCausingVehicleOfSituationSideCluster).WillByDefault(Return(CAUSING_AOI_SIDE));

  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.GetRelevantVehiclesForLaneChangeLength();

  ASSERT_EQ(&egoFrontVehicle, result.egoFrontVehicle);
  ASSERT_EQ(&causingVehicleFront, result.causingVehicleFrontSituation);
  ASSERT_EQ(&causingVehicleSide, result.causingVehicleSideSituation);
}

/***********************************
 * CHECK CreateLaneChangeWidthInput *
 ************************************/

TEST(TrajectoryPlanningQuery_CreateLaneChangeWidthParameters, GivenEgoFitsInTargetLane_ReturnEgoLaneHalfWidthPlusTargetLaneHalfWidth)
{
  NiceMock<FakeMentalModel> mentalModel;

  const units::length::meter_t EGO_VEHICLE_WIDTH{2};
  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.bounding_box = scm::common::BoundingBox();
  vehicleProperties.bounding_box.dimension.width = EGO_VEHICLE_WIDTH;
  ON_CALL(mentalModel, GetVehicleModelParameters).WillByDefault(Return(vehicleProperties));

  const units::length::meter_t EGO_LANE_WIDTH{10.0};
  LaneInformationGeometrySCM egoLaneGeometry;
  egoLaneGeometry.width = EGO_LANE_WIDTH;

  const units::length::meter_t TARGET_LANE_WIDTH{20.0};
  LaneInformationGeometrySCM targetLaneGeometry;
  targetLaneGeometry.width = TARGET_LANE_WIDTH;

  const RelativeLane TARGET_LANE{RelativeLane::RIGHT};
  FakeInfrastructureCharacteristics infrastructureCharacteristics;
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(egoLaneGeometry));
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(TARGET_LANE)).WillByDefault(ReturnRef(targetLaneGeometry));
  ON_CALL(mentalModel, GetInfrastructureCharacteristics).WillByDefault(Return(&infrastructureCharacteristics));

  const units::length::meter_t LATERAL_POSITION{12.3_m};
  ON_CALL(mentalModel, GetLateralPositionFrontAxle).WillByDefault(Return(LATERAL_POSITION));

  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.CreateLaneChangeWidthInput(TARGET_LANE);

  ASSERT_EQ(EGO_LANE_WIDTH, result.egoLaneWidth);
  ASSERT_EQ(TARGET_LANE_WIDTH, result.neighbourLaneWidth);
  ASSERT_EQ(LATERAL_POSITION, result.tStart);
  ASSERT_EQ(TARGET_LANE, result.relativeLane);
}

TEST(TrajectoryPlanningQuery_CreateLaneChangeWidthParameters, GivenEgoDoesNotFitInTargetLane_ReturnEgoLaneWidthForTargetLane)
{
  NiceMock<FakeMentalModel> mentalModel;

  const auto EGO_VEHICLE_WIDTH{4_m};
  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.bounding_box = scm::common::BoundingBox();
  vehicleProperties.bounding_box.dimension.width = EGO_VEHICLE_WIDTH;
  ON_CALL(mentalModel, GetVehicleModelParameters).WillByDefault(Return(vehicleProperties));

  const auto EGO_LANE_WIDTH{10.0_m};
  LaneInformationGeometrySCM egoLaneGeometry;
  egoLaneGeometry.width = EGO_LANE_WIDTH;

  const auto TARGET_LANE_WIDTH{3.0_m};
  LaneInformationGeometrySCM targetLaneGeometry;
  targetLaneGeometry.width = TARGET_LANE_WIDTH;

  const RelativeLane TARGET_LANE{RelativeLane::RIGHT};
  FakeInfrastructureCharacteristics infrastructureCharacteristics;
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(egoLaneGeometry));
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(TARGET_LANE)).WillByDefault(ReturnRef(targetLaneGeometry));
  ON_CALL(mentalModel, GetInfrastructureCharacteristics).WillByDefault(Return(&infrastructureCharacteristics));

  const units::length::meter_t LATERAL_POSITION{12.3_m};
  ON_CALL(mentalModel, GetLateralPositionFrontAxle).WillByDefault(Return(LATERAL_POSITION));

  auto query = TrajectoryPlanningQuery(mentalModel);
  const auto result = query.CreateLaneChangeWidthInput(TARGET_LANE);

  ASSERT_EQ(EGO_LANE_WIDTH, result.egoLaneWidth);
  ASSERT_EQ(EGO_LANE_WIDTH, result.neighbourLaneWidth);
  ASSERT_EQ(LATERAL_POSITION, result.tStart);
  ASSERT_EQ(TARGET_LANE, result.relativeLane);
}