/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../include/signal/longitudinalControllerInput.h"
#include "../module/longitudinalController/src/LongitudinalController.h"
#include "Fakes/FakeStochastics.h"

using ::testing::NiceMock;

/*****************
 * CHECK Trigger *
 *****************/

struct DataFor_Trigger
{
  units::velocity::meters_per_second_t absoluteVelocity;
  units::acceleration::meters_per_second_squared_t acceleration;
  std::string gearRatio;
  double expectedResultAccPedalPos;
  double expectedResultBrakePedalPos;
  int expectedResultGear;
};

class LongitudinalController_Trigger : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_Trigger>
{
};

TEST_P(LongitudinalController_Trigger, Check_Trigger)
{
  DataFor_Trigger data = GetParam();
  NiceMock<FakeStochastics> fakeStochastics;
  scm::LongitudinalController controller(&fakeStochastics);

  std::map<std::string, std::string> properties{};
  properties["AirDragCoefficient"] = "1";
  properties["FrontSurface"] = "2";
  properties["NumberOfGears"] = "1";
  properties["AxleRatio"] = "1";
  properties["GearRatio1"] = data.gearRatio;
  properties["MaximumEngineTorque"] = "400";
  properties["MaximumEngineSpeed"] = "8000";
  properties["MinimumEngineSpeed"] = "600";

  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  vehicleParameters.properties = properties;
  vehicleParameters.mass = 1500_kg;
  vehicleParameters.rear_axle.wheel_diameter = 20_m;

  scm::signal::LongitudinalControllerInput signal;
  signal.cycleTime = 100_ms;
  signal.vehicleParameters = vehicleParameters;
  signal.ownVehicleInformationSCM.absoluteVelocity = data.absoluteVelocity;
  signal.acceleration = data.acceleration;
  signal.driverParameters.pedalChangeTimeMinimum = 0.5_s;
  signal.driverParameters.pedalChangeTimeMean = 1.0_s;
  signal.driverParameters.pedalChangeTimeStandardDeviation = 0.5_s;

  auto result = controller.Trigger(signal, 100_ms);

  EXPECT_NEAR(result.accPedalPos, data.expectedResultAccPedalPos, 0.01);
  ASSERT_EQ(result.brakePedalPos, data.expectedResultBrakePedalPos);
  ASSERT_EQ(result.gear, data.expectedResultGear);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalController_Trigger,
    testing::Values(
        DataFor_Trigger{
            .absoluteVelocity = 20.0_mps,
            .acceleration = 2.0_mps_sq,
            .gearRatio = "1",
            .expectedResultAccPedalPos = 0.45,
            .expectedResultBrakePedalPos = 0,
            .expectedResultGear = 1},
        DataFor_Trigger{
            .absoluteVelocity = 0.0_mps,
            .acceleration = 0.0_mps_sq,
            .gearRatio = "1",
            .expectedResultAccPedalPos = 0.0,
            .expectedResultBrakePedalPos = 0,
            .expectedResultGear = 0},
        DataFor_Trigger{
            .absoluteVelocity = 50.0_mps,
            .acceleration = 2.0_mps_sq,
            .gearRatio = "15",
            .expectedResultAccPedalPos = 0.45,
            .expectedResultBrakePedalPos = 0,
            .expectedResultGear = 1},
        DataFor_Trigger{
            .absoluteVelocity = 50.0_mps,
            .acceleration = -2.0_mps_sq,
            .gearRatio = "15",
            .expectedResultAccPedalPos = 0.09,
            .expectedResultBrakePedalPos = 0,
            .expectedResultGear = 1}));