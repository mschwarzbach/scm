/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock-actions.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <cinttypes>
#include <cmath>
#include <iterator>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "TrajectoryCalculations/SteeringManeuver.h"
#include "TrajectoryCalculations/SteeringManeuverTypes.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "TrajectoryCalculations/TrajectoryCalculations.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"

/***************************
 * CHECK CalculateHeadings *
 ***************************/

TEST(TrajectoryCalculations, CalculateHeadings)
{
  TrajectoryCalculations::TrajectoryDimensions dimensions{};
  dimensions.width = 10.0_m;
  dimensions.length = 2.0_m;
  dimensions.headingStart = 1.0_rad;
  dimensions.headingEnd = 3.0_rad;

  auto result = TrajectoryCalculations::CalculateHeadings(dimensions);

  ASSERT_NEAR(result.headingManeuver.value(), 1.3734, 0.0001);
  ASSERT_NEAR(result.deltaHeadingStart.value(), 0.3734, 0.0001);
  ASSERT_NEAR(result.deltaHeadingEnd.value(), 1.6265, 0.0001);
}