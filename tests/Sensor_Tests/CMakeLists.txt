################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(COMPONENT_TEST_NAME Sensor_Tests)
set(ROOT_DIR ${CMAKE_CURRENT_LIST_DIR}/../..)
set(SCM_SENSOR_DIR ${CMAKE_CURRENT_LIST_DIR}/src)
set(DRIVER_DIR ${ROOT_DIR}module/driver)

add_scm_target(
    NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
    DEFAULT_MAIN

    HEADERS
    ${ROOT_DIR}/include/common/SensorDriverScmDefinitions.h
    GroundTruth.h

    SOURCES
    GroundTruth.cpp
    UnittestsSensor.cpp

    INCDIRS
    ${ROOT_DIR}/include
    ${ROOT_DIR}/include/module
    ${CMAKE_CURRENT_LIST_DIR}/include
    ${SCM_SENSOR_DIR}
    ${SCM_SENSOR_DIR}/Signals

    LIBRARIES
    OsiQueryLibrary
)