/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <initializer_list>
#include <string>

#include <OsiQueryLibrary/osiql.h>

namespace osiql::test {

using Boundary = std::vector<osiql::Vector2d>;
using BoundaryChain = std::vector<Boundary>;
using LaneRow = std::vector<osi3::LogicalLane *>;

struct Road
{
    Road(std::string id, osi3::ReferenceLine *referenceLine, size_t referenceLineIndex);
    std::string id;
    osi3::ReferenceLine *referenceLine;
    size_t referenceLineIndex;
    std::vector<std::vector<osi3::LogicalLaneBoundary *>> boundaryChains{};
    std::vector<std::vector<osi3::LogicalLane *>> laneRows{};
};

struct GroundTruth
{
    //! Creates a full road including reference line, boundaries and lanes
    //!
    //! \param referenceLineIndex Index of the boundary chain that also serves as the reference line
    //! \param boundaryChains Sorted from right to left
    //! \param laneRows vectors of vectors of s-coordinates representing the start/end of a lane
    //! \return Road&
    Road &AddRoad(const std::vector<BoundaryChain> &boundaryChains, size_t referenceLineIndex, std::vector<std::vector<double>> &laneRows);

    osi3::GroundTruth handle;
    std::vector<Road> roads;

private:
    osi3::ReferenceLine *AddReferenceLine(const BoundaryChain &boundaryChain);
    void AddBoundaryChain(Road &road, const BoundaryChain &boundaryChain);
    void AddLanes(Road &road, std::vector<std::vector<double>> &laneRows);

    //! Checks if a point already exists in the given boundary chain at the given s-coordinate and adds one if no point exists.
    //! Returns an iterator past the first boundary in the chain containing the s-coordinate.
    //!
    //! \param boundaryChain
    //! \param s
    std::vector<osi3::LogicalLaneBoundary *>::iterator InsertPointAtCoordinate(std::vector<osi3::LogicalLaneBoundary *> &boundaryChain, double s);

    uint64_t roadIdCounter{0};
    uint64_t idCounter{0};
};
} // namespace osiql::test
