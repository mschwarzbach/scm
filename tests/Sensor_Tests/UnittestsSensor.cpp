/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osiql.h>

#include <cstddef>
#include <memory>
#include <vector>

#include "GroundTruth.h"
#include "fakeAgent.h"
#include "fakeWorld.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "include/radioInterface.h"
#include "module/sensor/src/Dependency.h"
#include "module/sensor/src/DependencyInterface.h"
#include "module/sensor/src/MesoscopicInformationExtractor.h"
#include "module/sensor/src/MesoscopicInformationExtractorInterface.h"

using ::testing::NiceMock;

class OsiQlTest : public ::testing::Test
{
public:
  OsiQlTest()
  {
    using Boundary = std::vector<osiql::Vector2d>;
    using BoundaryChain = std::vector<Boundary>;
    using Road = std::vector<BoundaryChain>;

    const std::vector<Road> roads{Road{BoundaryChain{Boundary{{0.0, 0.0}, {1.0, 0.0}}}, BoundaryChain{Boundary{{0.0, 1.0}, {1.0, 1.0}}}},
                                  Road{BoundaryChain{Boundary{{1.0, 0.0}, {2.0, 0.0}}}, BoundaryChain{Boundary{{1.0, 1.0}, {2.0, 1.0}}}}};

    std::vector<std::vector<std::vector<double>>> lanes{std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}}, std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}}};

    for (size_t i = 0; i < roads.size(); ++i)
    {
      groundTruth.AddRoad(roads[i], roads[i].size() >> 1, lanes[i]);
    }

    // Connect lanes       
    osi3::LogicalLane_LaneConnection *connection{groundTruth.roads[0].laneRows[0][0]->add_successor_lane()};
    connection->set_at_begin_of_other_lane(true);
    connection->mutable_other_lane_id()->set_value(groundTruth.roads[1].laneRows[0][0]->id().value());
    connection = groundTruth.roads[1].laneRows[0][0]->add_predecessor_lane();
    connection->set_at_begin_of_other_lane(false);
    connection->mutable_other_lane_id()->set_value(groundTruth.roads[0].laneRows[0][0]->id().value());
    // Add objects       
    osi3::MovingObject *object{groundTruth.handle.add_moving_object()};
    object->mutable_id()->set_value(1001);

    {
      osi3::Dimension3d *dimensions{object->mutable_base()->mutable_dimension()};
      dimensions->set_length(0.75);
      dimensions->set_width(0.5);
      dimensions->set_height(0.5);
      osi3::Vector3d *position{object->mutable_base()->mutable_position()};
      position->set_x(0.5);
      position->set_y(0.5);
      object->mutable_base()->mutable_velocity()->set_x(3.0);
      object->mutable_base()->mutable_velocity()->set_y(4.0);
    }
    object = groundTruth.handle.add_moving_object();
    object->mutable_id()->set_value(1002);
    {
      osi3::Dimension3d *dimensions{object->mutable_base()->mutable_dimension()};
      dimensions->set_length(0.75);
      dimensions->set_width(0.5);
      dimensions->set_height(0.5);
      osi3::Vector3d *position{object->mutable_base()->mutable_position()};
      position->set_x(1.5);
      position->set_y(0.5);
      object->mutable_base()->mutable_velocity()->set_x(3.0);
      object->mutable_base()->mutable_velocity()->set_y(4.0);
    }
    query = std::make_unique<osiql::Query>(groundTruth.handle);
  }

  std::unique_ptr<osiql::Query> query;
  osiql::test::GroundTruth groundTruth;
};

TEST_F(OsiQlTest, MesoscopicInformationExtractor)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  std::shared_ptr<scm::Dependency> dependency = std::make_shared<scm::Dependency>(100, "", &fakeAgent, &fakeWorld, nullptr, nullptr, nullptr);

  scm::MesoscopicInformationExtractor mesoscopicInformationExtractor(query.get(), dependency);

  auto result = mesoscopicInformationExtractor.Update(100);
  ASSERT_EQ(result, 5);
}