/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <memory>
#include <ostream>

#include "../Fakes/FakeLaneChangeLengthCalculations.h"
#include "../Fakes/FakeLaneChangeTrajectoryCalculations.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct LaneChangeTrajectoryCalculationsTester
{
  class LaneChangeTrajectoryCalculationsUnderTest : LaneChangeTrajectoryCalculations
  {
  public:
    template <typename... Args>
    LaneChangeTrajectoryCalculationsUnderTest(std::shared_ptr<LaneChangeLengthCalculationsInterface> lengthCalculations)
        : LaneChangeTrajectoryCalculations{lengthCalculations} {};

    using LaneChangeTrajectoryCalculations::EgoFrontVehicleMoreUrgentThanReference;
  };

  LaneChangeTrajectoryCalculationsTester()
      : laneChangeTrajectoryCalculations(laneChangeLengthCalculations)
  {
  }
  std::shared_ptr<LaneChangeLengthCalculationsInterface> laneChangeLengthCalculations{std::make_shared<FakeLaneChangeLengthCalculations>()};
  LaneChangeTrajectoryCalculationsUnderTest laneChangeTrajectoryCalculations;
};

/***********************************
 * CHECK DetermineLaneChangeLength *
 ***********************************/

struct DataFor_DetermineLaneChangeLength
{
  units::length::meter_t distanceToEndOfEgoLane;
  units::length::meter_t freeLaneChangeLength;
  units::length::meter_t laneChangeLengthFromObstacle;
  units::length::meter_t laneChangeLengthToReachExit;
  units::length::meter_t result_laneChangeLength;
};

class LaneChangeTrajectoryCalculations_DetermineLaneChangeLength : public ::testing::Test,
                                                                   public ::testing::WithParamInterface<DataFor_DetermineLaneChangeLength>
{
};

TEST_P(LaneChangeTrajectoryCalculations_DetermineLaneChangeLength, ReturnsMinimumAvailableLaneChangeLengthCalculated)
{
  DataFor_DetermineLaneChangeLength data = GetParam();
  auto laneChangeLengthCalculations = std::make_shared<NiceMock<FakeLaneChangeLengthCalculations>>();
  FakeSurroundingVehicle fakeLaneChangeObstacle;
  TrajectoryPlanning::HighwayExitInformation exitInformation;

  TrajectoryPlanning::LaneChangeLengthParameters laneChangeLengthParameters{
      12._s,                        // timeHeadwayFreeLaneChange
      34._mps,                      // egoVelocityLongitudinal
      data.distanceToEndOfEgoLane,  // distanceToEndOfLane
      &fakeLaneChangeObstacle,
      exitInformation};

  EXPECT_CALL(*laneChangeLengthCalculations, DetermineFreeLaneChangeLength(laneChangeLengthParameters.timeHeadwayFreeLaneChange, laneChangeLengthParameters.egoVelocityLongitudinal))
      .WillOnce(Return(data.freeLaneChangeLength));
  EXPECT_CALL(*laneChangeLengthCalculations, DetermineLaneChangeLengthFromObstacle(laneChangeLengthParameters.laneChangeObstacle, laneChangeLengthParameters.egoVelocityLongitudinal, _))
      .WillOnce(Return(data.laneChangeLengthFromObstacle));
  EXPECT_CALL(*laneChangeLengthCalculations, DetermineLaneChangeLengthFromHighwayExit(exitInformation, laneChangeLengthParameters.egoVelocityLongitudinal))
      .WillOnce(Return(data.laneChangeLengthToReachExit));

  LaneChangeTrajectoryCalculations laneChangeTrajectoryCalculations(laneChangeLengthCalculations);
  NiceMock<FakeSurroundingVehicleQueryFactory> queryFactory;
  auto result = laneChangeTrajectoryCalculations.DetermineLaneChangeLength(laneChangeLengthParameters, queryFactory);

  ASSERT_EQ(result, data.result_laneChangeLength);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_DetermineLaneChangeLength,
    testing::Values(
        DataFor_DetermineLaneChangeLength{
            .distanceToEndOfEgoLane = 1._m,
            .freeLaneChangeLength = 12._m,
            .laneChangeLengthFromObstacle = 34._m,
            .laneChangeLengthToReachExit = 56._m,
            .result_laneChangeLength = 1._m},
        DataFor_DetermineLaneChangeLength{
            .distanceToEndOfEgoLane = 12._m,
            .freeLaneChangeLength = 2._m,
            .laneChangeLengthFromObstacle = 34._m,
            .laneChangeLengthToReachExit = 56._m,
            .result_laneChangeLength = 2._m},
        DataFor_DetermineLaneChangeLength{
            .distanceToEndOfEgoLane = 12._m,
            .freeLaneChangeLength = 34._m,
            .laneChangeLengthFromObstacle = 3._m,
            .laneChangeLengthToReachExit = 56._m,
            .result_laneChangeLength = 3._m},
        DataFor_DetermineLaneChangeLength{
            .distanceToEndOfEgoLane = 12._m,
            .freeLaneChangeLength = 34._m,
            .laneChangeLengthFromObstacle = 56._m,
            .laneChangeLengthToReachExit = 4._m,
            .result_laneChangeLength = 4._m}));

/************************************************
 * CHECK EgoFrontVehicleMoreUrgentThanReference *
 ************************************************/

struct DataFor_EgoFrontVehicleMoreUrgentThanReference
{
  bool toTheSideOfEgo;
  bool behindEgo;
  units::time::second_t egoTtc;
  units::time::second_t referenceTtc;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_EgoFrontVehicleMoreUrgentThanReference& obj)
  {
    return os
           << " toTheSideOfEgo (bool): " << obj.toTheSideOfEgo
           << "| behindEgo (bool): " << obj.behindEgo
           << "| egoTtc (double):" << obj.egoTtc
           << "| referenceTtc (double):" << obj.referenceTtc
           << "| result_laneChangeLength (bool): " << obj.result;
  }
};

class LaneChangeTrajectoryCalculations_EgoFrontVehicleMoreUrgentThanReference : public ::testing::Test,
                                                                                public ::testing::WithParamInterface<DataFor_EgoFrontVehicleMoreUrgentThanReference>
{
};

TEST_P(LaneChangeTrajectoryCalculations_EgoFrontVehicleMoreUrgentThanReference, EgoFrontVehicleMoreUrgentThanReference)
{
  LaneChangeTrajectoryCalculationsTester TEST_HELPER;
  DataFor_EgoFrontVehicleMoreUrgentThanReference data = GetParam();
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  const SurroundingVehicleInterface* egoFrontVehicle{&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeReferenceVehicle;
  const SurroundingVehicleInterface* referenceVehicle{&fakeReferenceVehicle};

  ON_CALL(fakeReferenceVehicle, ToTheSideOfEgo).WillByDefault(Return(data.toTheSideOfEgo));
  ON_CALL(fakeReferenceVehicle, BehindEgo).WillByDefault(Return(data.behindEgo));

  ON_CALL(fakeEgoFrontVehicle, GetTtc).WillByDefault(Return(data.egoTtc));
  ON_CALL(fakeReferenceVehicle, GetTtc).WillByDefault(Return(data.referenceTtc));

  auto result = TEST_HELPER.laneChangeTrajectoryCalculations.EgoFrontVehicleMoreUrgentThanReference(egoFrontVehicle, referenceVehicle);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_EgoFrontVehicleMoreUrgentThanReference,
    testing::Values(
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = true,
            .behindEgo = true,
            .egoTtc = 10_s,
            .referenceTtc = 10_s,
            .result = true},
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = true,
            .behindEgo = false,
            .egoTtc = 10_s,
            .referenceTtc = 10_s,
            .result = true},
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = false,
            .behindEgo = true,
            .egoTtc = 10_s,
            .referenceTtc = 10_s,
            .result = true},
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = false,
            .behindEgo = false,
            .egoTtc = 10_s,
            .referenceTtc = 10_s,
            .result = false},
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = false,
            .behindEgo = false,
            .egoTtc = 10_s,
            .referenceTtc = 15_s,
            .result = true},
        DataFor_EgoFrontVehicleMoreUrgentThanReference{
            .toTheSideOfEgo = false,
            .behindEgo = false,
            .egoTtc = 15_s,
            .referenceTtc = 10_s,
            .result = false}));

/***********************************************
 * CHECK DetermineReferenceObjectForLaneChange *
 ***********************************************/

struct DataFor_DetermineReferenceObjectForLaneChange
{
  Situation situation;
  units::time::second_t ttcEgoFront;
  units::time::second_t ttcSideVehicle;
  int result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineReferenceObjectForLaneChange& obj)
  {
    return os
           << " situation (Situation): " << ScmCommons::SituationToString(obj.situation)
           << " ttcEgoFront(double): " << obj.ttcEgoFront
           << " ttcSideVehicle(double): " << obj.ttcSideVehicle
           << "| result (int): " << obj.result;
  }
};

class LaneChangeTrajectoryCalculations_DetermineReferenceObjectForLaneChange : public ::testing::Test,
                                                                               public ::testing::WithParamInterface<DataFor_DetermineReferenceObjectForLaneChange>
{
};

TEST_P(LaneChangeTrajectoryCalculations_DetermineReferenceObjectForLaneChange, DetermineReferenceObjectForLaneChange)
{
  LaneChangeTrajectoryCalculationsTester TEST_HELPER;
  DataFor_DetermineReferenceObjectForLaneChange data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeEgoVehicle;
  ON_CALL(fakeEgoVehicle, GetId).WillByDefault(Return(1));
  ON_CALL(fakeEgoVehicle, GetTtc).WillByDefault(Return(data.ttcEgoFront));
  const SurroundingVehicleInterface* fakeEgofrontVehicle{&fakeEgoVehicle};

  NiceMock<FakeSurroundingVehicle> fakeCausingFrontVehicle;
  ON_CALL(fakeCausingFrontVehicle, GetId).WillByDefault(Return(2));
  const SurroundingVehicleInterface* causingVehicleFrontVehicle{&fakeCausingFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeCausingSideVehicle;
  ON_CALL(fakeCausingSideVehicle, GetId).WillByDefault(Return(3));
  ON_CALL(fakeCausingSideVehicle, GetTtc).WillByDefault(Return(data.ttcSideVehicle));
  const SurroundingVehicleInterface* causingVehicleSideVehicle{&fakeCausingSideVehicle};

  TrajectoryPlanning::RelevantVehiclesForLaneChangeLength relevantVehicles{
      .egoFrontVehicle = fakeEgofrontVehicle,
      .causingVehicleFrontSituation = causingVehicleFrontVehicle,
      .causingVehicleSideSituation = causingVehicleSideVehicle,
  };

  LaneChangeTrajectoryCalculations laneChangeTrajectoryCalculations(TEST_HELPER.laneChangeLengthCalculations);
  auto result = laneChangeTrajectoryCalculations.DetermineReferenceObjectForLaneChange(data.situation, relevantVehicles);

  ASSERT_EQ(result->GetId(), data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_DetermineReferenceObjectForLaneChange,
    testing::Values(
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::FOLLOWING_DRIVING,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 2},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 2},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 3},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 3},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 3.0_s,
            .result = 1},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 3},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::LANE_CHANGER_FROM_LEFT,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 3.0_s,
            .result = 1},
        DataFor_DetermineReferenceObjectForLaneChange{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .ttcEgoFront = 1.0_s,
            .ttcSideVehicle = 1.0_s,
            .result = 3}));

/*******************************************************
 * CHECK CanFinishLaneChangeWithoutEnteringMinDistance *
 *******************************************************/

struct DataFor_CanFinishLaneChangeWithoutEnteringMinDistance
{
  bool behindEgo;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CanFinishLaneChangeWithoutEnteringMinDistance& obj)
  {
    return os
           << " behindEgo(double): " << obj.behindEgo
           << "| result (bool): " << obj.result;
  }
};

class LaneChangeTrajectoryCalculations_CanFinishLaneChangeWithoutEnteringMinDistance : public ::testing::Test,
                                                                                       public ::testing::WithParamInterface<DataFor_CanFinishLaneChangeWithoutEnteringMinDistance>
{
};

TEST_P(LaneChangeTrajectoryCalculations_CanFinishLaneChangeWithoutEnteringMinDistance, CanFinishLaneChangeWithoutEnteringMinDistance)
{
  LaneChangeTrajectoryCalculationsTester TEST_HELPER;
  DataFor_CanFinishLaneChangeWithoutEnteringMinDistance data = GetParam();
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  NiceMock<FakeSurroundingVehicle> fakeClosestVehicleOnTargetLane;

  const SurroundingVehicleInterface* fakeVehicle{&fakeClosestVehicleOnTargetLane};

  ON_CALL(fakeClosestVehicleOnTargetLane, BehindEgo).WillByDefault(Return(data.behindEgo));
  ON_CALL(fakeClosestVehicleOnTargetLane, GetLongitudinalAcceleration).WillByDefault(Return(10.0_mps_sq));
  ON_CALL(fakeClosestVehicleOnTargetLane, GetRelativeNetDistance).WillByDefault(Return(30.0_m));

  const auto closestVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*closestVehicleQuery, GetLongitudinalVelocityDelta).WillByDefault(Return(15.0_mps));
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(::testing::Ref(fakeClosestVehicleOnTargetLane))).WillByDefault(Return(closestVehicleQuery));

  TrajectoryPlanning::SafetyParameters safetyParameters{
      .egoVelocityLongitudinal = 10.0_mps,
      .laneChangeLength = 100.0_m,
      .minDistance = 30.0_m,
      .closestVehicleOnTargetLane = fakeVehicle};

  LaneChangeTrajectoryCalculations laneChangeTrajectoryCalculations(TEST_HELPER.laneChangeLengthCalculations);
  auto result = laneChangeTrajectoryCalculations.CanFinishLaneChangeWithoutEnteringMinDistance(safetyParameters, fakeSurroundingVehicleQueryFactory);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_CanFinishLaneChangeWithoutEnteringMinDistance,
    testing::Values(
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .behindEgo = true,
            .result = false},
        DataFor_CanFinishLaneChangeWithoutEnteringMinDistance{
            .behindEgo = false,
            .result = true}));