/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock-actions.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ostream>

#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

using ::testing::NiceMock;
using ::testing::Return;

/************************************************
 * CHECK DetermineLaneChangeLengthFromObstacle *
 ************************************************/
namespace
{
static constexpr double DONT_CARE{123.0};
static constexpr auto TTC_INVALID_1{ScmDefinitions::TTC_LIMIT};
static constexpr auto TTC_INVALID_2{-10_s};
}  // namespace
struct DataFor_DetermineLaneChangeLengthFromObstacle
{
  bool toTheSideOfEgo;
  units::time::second_t timeToEnter;
  units::length::meter_t netDistance;
  units::time::second_t ttc;
  units::time::second_t gap;
  units::velocity::meters_per_second_t egoVelocity;
  units::length::meter_t result_laneChangeLengthFromObstacle;
};

class LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromObstacle : public ::testing::Test,
                                                                               public ::testing::WithParamInterface<DataFor_DetermineLaneChangeLengthFromObstacle>
{
};

TEST_P(LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromObstacle, DetermineLaneChangeLengthFromObstacle)
{
  DataFor_DetermineLaneChangeLengthFromObstacle data = GetParam();
  NiceMock<FakeSurroundingVehicle> laneChangeObstacle;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;

  auto vehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(::testing::Ref(laneChangeObstacle))).WillByDefault(Return(vehicleQuery));

  ObstructionDynamics obstructionDynamics{data.timeToEnter, 0._s};
  ON_CALL(*vehicleQuery, GetLateralObstructionDynamics).WillByDefault(Return(obstructionDynamics));
  ON_CALL(laneChangeObstacle, ToTheSideOfEgo).WillByDefault(Return(data.toTheSideOfEgo));
  ON_CALL(laneChangeObstacle, GetTtc).WillByDefault(Return(data.ttc));
  ON_CALL(laneChangeObstacle, GetRelativeNetDistance).WillByDefault(Return(data.netDistance));
  ON_CALL(laneChangeObstacle, GetThw).WillByDefault(Return(data.gap));

  LaneChangeLengthCalculations laneChangeLengthCalculationsUnderTest;
  auto result = laneChangeLengthCalculationsUnderTest.DetermineLaneChangeLengthFromObstacle(&laneChangeObstacle, data.egoVelocity, fakeSurroundingVehicleQueryFactory);

  if (units::math::isinf(data.result_laneChangeLengthFromObstacle))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.result_laneChangeLengthFromObstacle);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromObstacle,
    testing::Values(
        DataFor_DetermineLaneChangeLengthFromObstacle{
            .toTheSideOfEgo = false,
            .timeToEnter = units::make_unit<units::time::second_t>(DONT_CARE),
            .netDistance = 100._m,
            .ttc = 10.0_s,
            .gap = 2.0_s,
            .egoVelocity = 2.0_mps,
            .result_laneChangeLengthFromObstacle = 100._m + ((10._m - 2._m) * 2.)},
        DataFor_DetermineLaneChangeLengthFromObstacle{
            .toTheSideOfEgo = true,
            .timeToEnter = TTC_INVALID_1,
            .netDistance = units::make_unit<units::length::meter_t>(DONT_CARE),
            .ttc = units::make_unit<units::time::second_t>(DONT_CARE),
            .gap = units::make_unit<units::time::second_t>(DONT_CARE),
            .egoVelocity = units::make_unit<units::velocity::meters_per_second_t>(DONT_CARE),
            .result_laneChangeLengthFromObstacle = ScmDefinitions::INF_DISTANCE},
        DataFor_DetermineLaneChangeLengthFromObstacle{
            .toTheSideOfEgo = false,
            .timeToEnter = units::make_unit<units::time::second_t>(DONT_CARE),
            .netDistance = 100._m,
            .ttc = TTC_INVALID_2,
            .gap = 2.0_s,
            .egoVelocity = 2.0_mps,
            .result_laneChangeLengthFromObstacle = ScmDefinitions::INF_DISTANCE},
        DataFor_DetermineLaneChangeLengthFromObstacle{
            .toTheSideOfEgo = true,
            .timeToEnter = 10.0_s,
            .netDistance = 100._m,
            .ttc = units::make_unit<units::time::second_t>(DONT_CARE),
            .gap = units::make_unit<units::time::second_t>(DONT_CARE),
            .egoVelocity = 2._mps,
            .result_laneChangeLengthFromObstacle = 10._m * 2.}));

TEST(LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromObstacle, DetermineLaneChangeLengthFromObstacleWithNonObstacle)
{
  NiceMock<FakeSurroundingVehicle> laneChangeObstacle;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;

  auto vehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(::testing::Ref(laneChangeObstacle))).WillByDefault(Return(vehicleQuery));

  ObstructionDynamics obstructionDynamics{units::make_unit<units::time::second_t>(DONT_CARE), 0._s};
  ON_CALL(*vehicleQuery, GetLateralObstructionDynamics).WillByDefault(Return(obstructionDynamics));
  ON_CALL(laneChangeObstacle, ToTheSideOfEgo).WillByDefault(Return(false));
  ON_CALL(laneChangeObstacle, GetTtc).WillByDefault(Return(units::make_unit<units::time::second_t>(DONT_CARE)));
  ON_CALL(laneChangeObstacle, GetRelativeNetDistance).WillByDefault(Return(units::make_unit<units::length::meter_t>(DONT_CARE)));
  ON_CALL(laneChangeObstacle, GetThw).WillByDefault(Return(units::make_unit<units::time::second_t>(DONT_CARE)));

  LaneChangeLengthCalculations laneChangeLengthCalculationsUnderTest;
  auto result = laneChangeLengthCalculationsUnderTest.DetermineLaneChangeLengthFromObstacle(nullptr, units::make_unit<units::velocity::meters_per_second_t>(DONT_CARE), fakeSurroundingVehicleQueryFactory);

  ASSERT_TRUE(units::math::isinf(result));
}

/***************************************
 * CHECK DetermineFreeLaneChangeLength *
 ***************************************/
namespace
{
static constexpr units::length::meter_t MINIMUM_FREE_LANE_CHANGE_LENGTH{40._m};
}
struct DataFor_DetermineFreeLaneChangeLength
{
  units::time::second_t timeHeadwayFreeLaneChange;
  units::velocity::meters_per_second_t egoVelocity;
  units::length::meter_t expected_laneChangeLength;
};

class LaneChangeTrajectoryCalculations_DetermineFreeLaneChange : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataFor_DetermineFreeLaneChangeLength>
{
};

TEST_P(LaneChangeTrajectoryCalculations_DetermineFreeLaneChange, GivenMinThwAndEgoVelocity_returnProductOrMinFreeLaneChangeLength)
{
  const auto data{GetParam()};

  LaneChangeLengthCalculations laneChangeLengthCalculationsUnderTest;
  const auto result{laneChangeLengthCalculationsUnderTest.DetermineFreeLaneChangeLength(data.timeHeadwayFreeLaneChange, data.egoVelocity)};

  ASSERT_EQ(result, data.expected_laneChangeLength);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_DetermineFreeLaneChange,
    testing::Values(
        DataFor_DetermineFreeLaneChangeLength{
            .timeHeadwayFreeLaneChange = 10._s,
            .egoVelocity = 2._mps,
            .expected_laneChangeLength = MINIMUM_FREE_LANE_CHANGE_LENGTH},
        DataFor_DetermineFreeLaneChangeLength{
            .timeHeadwayFreeLaneChange = 50._s,
            .egoVelocity = 2._mps,
            .expected_laneChangeLength = 50._m * 2.},
        DataFor_DetermineFreeLaneChangeLength{
            .timeHeadwayFreeLaneChange = 50._s,
            .egoVelocity = 5._mps,
            .expected_laneChangeLength = 50._m * 5.}));

/**************************************************
 * CHECK DetermineLaneChangeLengthFromHighwayExit *
 **************************************************/
namespace
{
static constexpr units::length::meter_t MINIMUM_DISTANCE_BUFFER{20._m};
}

struct DataFor_DetermineLaneChangeLengthFromHighwayExit
{
  units::length::meter_t distanceToEndOfExit;
  int numberOfLaneChanges;
  units::time::second_t minimumTimeOnExitLane;
  units::velocity::meters_per_second_t egoVelocity;
  units::length::meter_t expected_laneChangeLength;
};

class LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromHighwayExit : public ::testing::Test,
                                                                                  public ::testing::WithParamInterface<DataFor_DetermineLaneChangeLengthFromHighwayExit>
{
};

TEST_P(LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromHighwayExit, ReturnDistanceToEndOfExitMinusVelocityTimesMinimumTime_InfinityIfInvalid)
{
  const auto data{GetParam()};
  TrajectoryPlanning::HighwayExitInformation highwayExitInformation{
      data.distanceToEndOfExit,
      data.numberOfLaneChanges,
      data.minimumTimeOnExitLane};

  LaneChangeLengthCalculations laneChangeLengthCalculationsUnderTest;
  const auto result{laneChangeLengthCalculationsUnderTest.DetermineLaneChangeLengthFromHighwayExit(highwayExitInformation, data.egoVelocity)};

  if (units::math::isinf(data.expected_laneChangeLength))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expected_laneChangeLength);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeTrajectoryCalculations_DetermineLaneChangeLengthFromHighwayExit,
    testing::Values(
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 100._m,
            .numberOfLaneChanges = 1,
            .minimumTimeOnExitLane = 5._s,
            .egoVelocity = 10._mps,
            .expected_laneChangeLength = 50._m},
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 100._m,
            .numberOfLaneChanges = 2,
            .minimumTimeOnExitLane = 5._s,
            .egoVelocity = 10._mps,
            .expected_laneChangeLength = 25._m},
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 100._m,
            .numberOfLaneChanges = 1,
            .minimumTimeOnExitLane = 5._s,
            .egoVelocity = 2._mps,
            .expected_laneChangeLength = 100._m - MINIMUM_DISTANCE_BUFFER},
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 80._m,
            .numberOfLaneChanges = 1,
            .minimumTimeOnExitLane = 10._s,
            .egoVelocity = 10._mps,
            .expected_laneChangeLength = ScmDefinitions::INF_DISTANCE},
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 100._m,
            .numberOfLaneChanges = 2,
            .minimumTimeOnExitLane = 10._s,
            .egoVelocity = 10._mps,
            .expected_laneChangeLength = ScmDefinitions::INF_DISTANCE},
        DataFor_DetermineLaneChangeLengthFromHighwayExit{
            .distanceToEndOfExit = 0._m,
            .numberOfLaneChanges = 1,
            .minimumTimeOnExitLane = 10._s,
            .egoVelocity = 11._mps,
            .expected_laneChangeLength = ScmDefinitions::INF_DISTANCE}));