################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(COMPONENT_TEST_NAME ScmMathUtils_Tests)
set(INCLUDE_DIR ${ROOT_DIR}/include)

add_scm_target(
    NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
    DEFAULT_MAIN

    SOURCES
    UnitTestsScmMathUtils.cpp

    HEADERS
    ${INCLUDE_DIR}/ScmMathUtils.h
)
