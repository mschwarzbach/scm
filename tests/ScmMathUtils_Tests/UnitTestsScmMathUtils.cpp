/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gtest/gtest.h>

#include "include/ScmMathUtils.h"

/****************************
 * CHECK LinearInterplation *
 ****************************/

TEST(ScmMathUtils_LinearInterplation, XLiesBetweenXMinAndXMax_LeadsToYLiesBetweenYMinAndYMax)
{
  double X_MIN = 0.0;
  double X_MAX = 20.0;
  double Y_MIN = 0.0;
  double Y_MAX = 5.0;

  for (int test_x = 0; test_x <= X_MAX; ++test_x)
  {
    auto result = ScmMathUtils::LinearInterpolation(static_cast<double>(test_x), X_MIN, X_MAX, Y_MIN, Y_MAX);
    ASSERT_GE(result, Y_MIN);
    ASSERT_LE(result, Y_MAX);
  }
}

TEST(ScmMathUtils_LinearInterplation, ChecksKnownSelectedValues)
{
  double X_MIN = 0.0;
  double X_MAX = 10.0;
  double Y_MIN = 0.0;
  double Y_MAX = 1.0;
  auto result = ScmMathUtils::LinearInterpolation(0., X_MIN, X_MAX, Y_MIN, Y_MAX);
  ASSERT_DOUBLE_EQ(result, 0.0);
  result = ScmMathUtils::LinearInterpolation(5., X_MIN, X_MAX, Y_MIN, Y_MAX);
  ASSERT_DOUBLE_EQ(result, 0.5);
  result = ScmMathUtils::LinearInterpolation(10., X_MIN, X_MAX, Y_MIN, Y_MAX);
  ASSERT_DOUBLE_EQ(result, 1.0);
  result = ScmMathUtils::LinearInterpolation(15., X_MIN, X_MAX, Y_MIN, Y_MAX);
  ASSERT_DOUBLE_EQ(result, 1.5);
}

TEST(ScmMathUtils_LinearInterplation, MonothonicIncreaseInX_LeadsToMonothonicIncreaseInY)
{
  double X_MIN = 0.0;
  double X_MAX = 20.0;
  double Y_MIN = 0.0;
  double Y_MAX = 5.0;

  std::vector<double> result{};
  const int size = 100;
  for (int test_x = 0; test_x <= size; ++test_x)
  {
    result.push_back(ScmMathUtils::LinearInterpolation(static_cast<double>(test_x), X_MIN, X_MAX, Y_MIN, Y_MAX));
  }
  for (int i = 0; i < size; ++i)
  {
    ASSERT_LT(result[i], result[i + 1]);
  }
}

TEST(ScmMathUtils_LinearInterplation, XMinGreaterXMax_ThrowsException)
{
  double X_MIN = 1.0;
  double X_MAX = 0.0;
  double Y_MIN = 0.0;
  double Y_MAX = 5.0;
  double test_x = 1.0;
  EXPECT_THROW(ScmMathUtils::LinearInterpolation(test_x, X_MIN, X_MAX, Y_MIN, Y_MAX), std::runtime_error);
}

TEST(ScmMathUtils_LinearInterplation, XMinEqualsXMax_ThrowsException)
{
  double X_MIN = 0.0;
  double X_MAX = 0.0;
  double Y_MIN = 0.0;
  double Y_MAX = 5.0;
  double test_x = 1.0;
  EXPECT_THROW(ScmMathUtils::LinearInterpolation(test_x, X_MIN, X_MAX, Y_MIN, Y_MAX), std::runtime_error);
}

TEST(ScmMathUtils_LinearInterplation, YMinGreaterYMax_ThrowsException)
{
  double X_MIN = 0.0;
  double X_MAX = 1.0;
  double Y_MIN = 1.0;
  double Y_MAX = 0.0;
  double test_x = 1.0;
  EXPECT_THROW(ScmMathUtils::LinearInterpolation(test_x, X_MIN, X_MAX, Y_MIN, Y_MAX), std::runtime_error);
}
