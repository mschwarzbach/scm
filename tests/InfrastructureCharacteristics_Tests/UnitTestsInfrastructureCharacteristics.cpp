/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <ostream>

#include "InfrastructureCharacteristics.h"
#include "include/common/ScmDefinitions.h"
#include "module/driver/src/InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

struct InfrastructureCharacteristicsTester
{
  class InfrastructureCharacteristicsUnderTest : InfrastructureCharacteristics
  {
  public:
    template <typename... Args>
    InfrastructureCharacteristicsUnderTest(Args&&... args)
        : InfrastructureCharacteristics{std::forward<Args>(args)...} {};

    using InfrastructureCharacteristics::CalculateDistanceToStartOfExitNew;
    using InfrastructureCharacteristics::DeriveDistanceToEndOfExitFromTrafficSigns;
    using InfrastructureCharacteristics::DeriveDistanceToEndOfLaneFromTrafficSigns;
    using InfrastructureCharacteristics::DeriveDistanceToStartOfExitFromTrafficSigns;
    using InfrastructureCharacteristics::EstimateRelativeLaneIdOfExitLane;
    using InfrastructureCharacteristics::Initialize;
    using InfrastructureCharacteristics::SetGeometryInformation;
    using InfrastructureCharacteristics::SetTrafficRuleInformation;
    using InfrastructureCharacteristics::TrafficSignRelevantToEndOfLane;
    using InfrastructureCharacteristics::UpdateDistanceToEndOfLane;
    using InfrastructureCharacteristics::UpdateDistanceToStartAndEndOfNextExit;

    void SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_END_OF_EXIT(units::length::meter_t lastTrafficSignIndicatingEndOfExit)
    {
      _valueLastTrafficSignIndicatingEndOfExit = lastTrafficSignIndicatingEndOfExit;
    }

    void SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_START_OF_EXIT(units::length::meter_t lastTrafficSignIndicatingStartOfExit)
    {
      _valueLastTrafficSignIndicatingStartOfExit = lastTrafficSignIndicatingStartOfExit;
    }

    void SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_END_OF_LANE(int relativeLaneId, double lastTrafficSignEndOfLane)
    {
      switch (relativeLaneId)
      {
        case 0:  // ego
          _valuesLastTrafficSignEndOfLane[SurroundingLane::EGO] = lastTrafficSignEndOfLane;
          break;
        case 1:  // right
          _valuesLastTrafficSignEndOfLane[SurroundingLane::RIGHT] = lastTrafficSignEndOfLane;
          break;
        case -1:  // left
          _valuesLastTrafficSignEndOfLane[SurroundingLane::LEFT] = lastTrafficSignEndOfLane;
          break;
        default:
          break;
      }
    }

    void SET_CYCLE_TIME(units::time::millisecond_t cycleTime)
    {
      _cycleTime = cycleTime;
    }
  };

  InfrastructureCharacteristicsTester()
      : infrastructureCharacteristics()
  {
  }

  InfrastructureCharacteristicsUnderTest infrastructureCharacteristics;
};

/******************************************************************
 * CHECK Processing of end of lane information from traffic signs *
 ******************************************************************/

struct DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane
{
  std::string description;
  SurroundingLane input_laneToEvaluate;
  int input_egoLaneId;
  std::tuple<SurroundingLane, units::length::meter_t> input_DistanceToEndOfLaneGroundTruth;
  std::tuple<SurroundingLane, units::length::meter_t> input_DistanceToEndOfLanePerceived;
  scm::CommonTrafficSign::Type input_signType;
  units::length::meter_t input_signRelativeDistance;
  scm::CommonTrafficSign::Type input_supplementarySignType;
  double input_supplementarySignValue;

  std::tuple<SurroundingLane, units::length::meter_t> expected_DistanceToEndOfLane;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane& rhs)
  {
    os << "\ninput_laneToEvaluate: " << ScmCommons::SurroundingLaneToString(rhs.input_laneToEvaluate)
       << "\n input_egoLaneId: " << rhs.input_egoLaneId
       << "\n input_DistanceToEndOfLaneGroundTruth: "
       << "Lane: " << ScmCommons::SurroundingLaneToString(std::get<0>(rhs.input_DistanceToEndOfLaneGroundTruth))
       << "Distance: " << std::get<1>(rhs.input_DistanceToEndOfLaneGroundTruth)
       << "\n input_DistanceToEndOfLanePerceived: "
       << "Lane: " << ScmCommons::SurroundingLaneToString(std::get<0>(rhs.input_DistanceToEndOfLanePerceived))
       << "Distance: " << std::get<1>(rhs.input_DistanceToEndOfLanePerceived)
       << "\n input_signType: " << rhs.input_signType
       << "\n input_signRelativeDistance: " << rhs.input_signRelativeDistance
       << "\n input_supplementarySignType: " << rhs.input_supplementarySignType
       << "\n input_supplementarySignValue: " << rhs.input_supplementarySignValue
       << "\n expected_DistanceToEndOfLane: "
       << "Lane: " << ScmCommons::SurroundingLaneToString(std::get<0>(rhs.input_DistanceToEndOfLanePerceived))
       << "Distance: " << std::get<1>(rhs.input_DistanceToEndOfLanePerceived);
    return os;
  }
};

/***********************************
 * CHECK UpdateDistanceToEndOfLane *
 ***********************************/

class InfrastructureCharacteristics_UpdateDistanceToEndOfLaneParam : public ::testing::Test,
                                                                     public ::testing::WithParamInterface<DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane>
{
protected:
  GeometryInformationSCM geometryInformationGroundTruth;
  GeometryInformationSCM geometryInformation;

  virtual void SetUp()
  {
    geometryInformationGroundTruth.Close().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.Left().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.Right().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.numberOfLanes = 3;
    geometryInformation.Close().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.Left().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.Right().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.numberOfLanes = 3;
  }
};

TEST_P(InfrastructureCharacteristics_UpdateDistanceToEndOfLaneParam, CheckFunction_UpdateDistanceToEndOfLane)
{
  const auto data = GetParam();
  // Get resources
  InfrastructureCharacteristicsTester TEST_HELPER;

  TrafficRuleInformationSCM trafficRuleInformation;
  scm::CommonTrafficSign::Entity trafficSign;
  scm::CommonTrafficSign::Entity supplementarySign;

  constexpr units::velocity::meters_per_second_t EGO_VELOCITY{10._mps};

  geometryInformationGroundTruth.FromLane(std::get<0>(data.input_DistanceToEndOfLaneGroundTruth)).distanceToEndOfLane = std::get<1>(data.input_DistanceToEndOfLaneGroundTruth);
  supplementarySign.type = data.input_supplementarySignType;
  supplementarySign.value = data.input_supplementarySignValue;
  trafficSign.type = data.input_signType;
  trafficSign.relativeDistance = data.input_signRelativeDistance;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);
  geometryInformation.FromLane(std::get<0>(data.input_DistanceToEndOfLanePerceived)).distanceToEndOfLane = std::get<1>(data.input_DistanceToEndOfLanePerceived);

  TEST_HELPER.infrastructureCharacteristics.Initialize(100_ms, nullptr, &geometryInformation);
  //  run test
  TEST_HELPER.infrastructureCharacteristics.UpdateDistanceToEndOfLane(data.input_laneToEvaluate, &geometryInformationGroundTruth, EGO_VELOCITY, data.input_egoLaneId);
  ASSERT_EQ(geometryInformationGroundTruth.FromLane(std::get<0>(data.expected_DistanceToEndOfLane)).distanceToEndOfLane.value(), std::get<1>(data.expected_DistanceToEndOfLane).value());
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_UpdateDistanceToEndOfLaneParam,
    testing::Values(
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of ego lane can already be seen -> no traffic signs must be evaluated and no extrapolation is necessary",
            .input_laneToEvaluate = SurroundingLane::EGO,
            .input_egoLaneId = -1,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, 200._m},
            // Decoy from mental data from last time step -> distance = 175 m (through extrapolation with velocityEgoEstimated = 10 m/s)
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::EGO, 176._m},
            // Decoy from traffic sign -> distance = 150 m
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::EGO, 200._m}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of left most lane is announced, left most lane is ego lane, and ego lane is evaluated",
            .input_laneToEvaluate = SurroundingLane::EGO,
            .input_egoLaneId = -1,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::EGO, 176._m},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::EGO, 150._m}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of right most lane is announced, right most lane is ego lane, and ego lane is evaluated",
            .input_laneToEvaluate = SurroundingLane::EGO,
            .input_egoLaneId = -3,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::EGO, 176._m},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::EGO, 150._m}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of left most lane is announced, left most lane is left lane of driver, and left lane is evaluated",
            .input_laneToEvaluate = SurroundingLane::LEFT,
            .input_egoLaneId = -2,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::LEFT, 176._m},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::LEFT, 150._m}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of right most lane is announced, right most lane is right lane of driver, and right lane is evaluated",
            .input_laneToEvaluate = SurroundingLane::RIGHT,
            .input_egoLaneId = -2,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::RIGHT, 176._m},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::RIGHT, 150._m}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of left most lane is announced, left most lane is ego lane, but right lane is evaluated -> neighter of both lanes should be altered - 1",
            .input_laneToEvaluate = SurroundingLane::RIGHT,
            .input_egoLaneId = -1,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of left most lane is announced, left most lane is ego lane, but right lane is evaluated -> neighter of both lanes should be altered - 2",
            .input_laneToEvaluate = SurroundingLane::RIGHT,
            .input_egoLaneId = -1,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::RIGHT, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::RIGHT, ScmDefinitions::INF_DISTANCE},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::DistanceIndication,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::RIGHT, ScmDefinitions::INF_DISTANCE}},
        DataFor_InfrastructureCharacteristics_UpdateDistanceToEndOfLane{
            .description = "End of ego lane is announced, but no distance information is given -> ignore traffic sign",
            .input_laneToEvaluate = SurroundingLane::EGO,
            .input_egoLaneId = -1,
            .input_DistanceToEndOfLaneGroundTruth = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_DistanceToEndOfLanePerceived = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE},
            .input_signType = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_signRelativeDistance = 50.0_m,
            .input_supplementarySignType = scm::CommonTrafficSign::Type::SpeedLimitZoneBegin,
            .input_supplementarySignValue = 100.,
            .expected_DistanceToEndOfLane = {SurroundingLane::EGO, ScmDefinitions::INF_DISTANCE}}));

/*****************************************************************
 * CHECK InfrastructureCharacteristics UpdateDistanceToEndOfLane *
 *****************************************************************/

class InfrastructureCharacteristics_UpdateDistanceToEndOfLane : public ::testing::Test
{
protected:
  GeometryInformationSCM geometryInformationGroundTruth;
  GeometryInformationSCM geometryInformation;

  virtual void SetUp()
  {
    geometryInformationGroundTruth.Close().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.Left().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.Right().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformationGroundTruth.numberOfLanes = 3;
    geometryInformation.Close().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.Left().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.Right().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
    geometryInformation.numberOfLanes = 3;
  }
};

TEST_F(InfrastructureCharacteristics_UpdateDistanceToEndOfLane, CheckFunction_UpdateDistanceToEndOfLane_SignWasAlreadyObservedBefore_IgnoreSign)
{
  // Get resources
  InfrastructureCharacteristicsTester TEST_HELPER;
  TrafficRuleInformationSCM trafficRuleInformation;
  scm::CommonTrafficSign::Entity trafficSign;
  scm::CommonTrafficSign::Entity supplementarySign;

  constexpr units::velocity::meters_per_second_t EGO_VELOCITY{10._mps};

  // TEST CASE: End of ego lane is announced, but sign was already observed before -> ignore traffic sign and extrapolate distance
  trafficSign.type = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd;
  trafficSign.relativeDistance = 50.0_m;
  supplementarySign.type = scm::CommonTrafficSign::Type::DistanceIndication;
  supplementarySign.value = 100.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);
  TEST_HELPER.infrastructureCharacteristics.SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_END_OF_LANE(0, 100.);
  // mental data from last time step -> distance = 75 m (through extrapolation with velocityEgoEstimated = 10 m/s)
  geometryInformation.FromLane(SurroundingLane::EGO).distanceToEndOfLane = 76._m;

  TEST_HELPER.infrastructureCharacteristics.Initialize(100_ms, nullptr, &geometryInformation);

  // run test
  TEST_HELPER.infrastructureCharacteristics.UpdateDistanceToEndOfLane(SurroundingLane::EGO, &geometryInformationGroundTruth, EGO_VELOCITY, -1);

  ASSERT_EQ(geometryInformationGroundTruth.FromLane(SurroundingLane::EGO).distanceToEndOfLane, 75._m);
}

TEST_F(InfrastructureCharacteristics_UpdateDistanceToEndOfLane, CheckFunction_UpdateDistanceToEndOfLane_TwoSignsAvailableChooseFartherSign)
{
  // Get resources
  InfrastructureCharacteristicsTester TEST_HELPER;
  TrafficRuleInformationSCM trafficRuleInformation;
  scm::CommonTrafficSign::Entity trafficSign;
  scm::CommonTrafficSign::Entity supplementarySign;

  constexpr units::velocity::meters_per_second_t EGO_VELOCITY{10._mps};

  // TEST CASE: Two signs for end of ego lane are available -> choose farthest sign
  trafficSign.type = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd;
  trafficSign.relativeDistance = 50.0_m;

  // Decoy "nearer" sign -> distance = 150 m
  trafficSign.relativeDistance = 50.0_m;
  supplementarySign.type = scm::CommonTrafficSign::Type::DistanceIndication;
  supplementarySign.value = 110.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);

  trafficSign.supplementarySigns.clear();

  // Relevant sign -> distance = 250 m
  trafficSign.relativeDistance = 100.0_m;
  supplementarySign.value = 150.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);

  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);
  TEST_HELPER.infrastructureCharacteristics.Initialize(100_ms, nullptr, &geometryInformation);

  // run test
  TEST_HELPER.infrastructureCharacteristics.UpdateDistanceToEndOfLane(SurroundingLane::EGO, &geometryInformationGroundTruth, EGO_VELOCITY, -1);

  ASSERT_EQ(geometryInformationGroundTruth.FromLane(SurroundingLane::EGO).distanceToEndOfLane, 250._m);
}

/*********************************************************
 * CHECK Helper function TrafficSignRelevantToEndOfLane  *
 *********************************************************/

struct DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane
{
  std::string description;
  scm::CommonTrafficSign::Type input_trafficSign;
  int input_numberOfLanes;
  int input_egoLaneId;
  SurroundingLane input_laneToEvaluate;

  bool expected_isRelevant;
};

class InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane : public ::testing::Test,
                                                                     public ::testing::WithParamInterface<DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane>
{
};

TEST_P(InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane, CheckFunction_TrafficSignRelevantToEndOfLane)
{
  const auto data = GetParam();
  InfrastructureCharacteristicsTester TEST_HELPER;
  scm::CommonTrafficSign::Entity trafficSign;

  trafficSign.type = data.input_trafficSign;
  const bool result = TEST_HELPER.infrastructureCharacteristics.TrafficSignRelevantToEndOfLane(trafficSign,
                                                                                               data.input_egoLaneId,
                                                                                               data.input_laneToEvaluate,
                                                                                               data.input_numberOfLanes);
  ASSERT_EQ(result, data.expected_isRelevant);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane,
    testing::Values(
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on ending right most lane, therefore lane ego ends",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_numberOfLanes = 3,
            .input_egoLaneId = -3,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = true},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on ending left most lane, therefore lane ego ends",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_numberOfLanes = 3,
            .input_egoLaneId = -1,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = true},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agents neighbouring right lane can only end when agent is on second right most lane",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -4,
            .input_laneToEvaluate = SurroundingLane::RIGHT,
            .expected_isRelevant = true},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agents neighbouring left lane can only end when agent is on second left most lane",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -2,
            .input_laneToEvaluate = SurroundingLane::LEFT,
            .expected_isRelevant = true},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on right most lane, Sign announces left lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -5,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = false},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is not on right most lane, Ego Lane is evaluated, Sign announces right lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -4,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = false},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on second to left lane, EgoLane is evaluated, Sign announces left lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -2,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = false},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on second to right lane, EgoLane is evaluated, Sign announces right lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -4,
            .input_laneToEvaluate = SurroundingLane::EGO,
            .expected_isRelevant = false},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on third to right lane, Right lane is evaluated, Sign announces right lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceRightLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -3,
            .input_laneToEvaluate = SurroundingLane::RIGHT,
            .expected_isRelevant = false},
        DataFor_InfrastructureCharacteristics_TrafficSignRelevantToEndOfLane{
            .description = "Agent is on third to left lane, Left lane is evaluated, Sign announces left lane end, not relevant",
            .input_trafficSign = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd,
            .input_numberOfLanes = 5,
            .input_egoLaneId = -3,
            .input_laneToEvaluate = SurroundingLane::LEFT,
            .expected_isRelevant = false}));

/********************************************************************
 * CHECK Helper function DeriveDistanceToEndOfLaneFromTrafficSigns  *
 ********************************************************************/

class InfrastructureCharacteristics_DeriveDistanceToEndOfLaneFromTrafficSigns : public ::testing::Test
{
};
TEST_F(InfrastructureCharacteristics_DeriveDistanceToEndOfLaneFromTrafficSigns, ThreeSignsGiven_TwoSignsRelevant_ReturnsDistanceDerivedFromFarthestSign)
{
  InfrastructureCharacteristicsTester TEST_HELPER;
  std::vector<scm::CommonTrafficSign::Entity> trafficSigns;
  scm::CommonTrafficSign::Entity trafficSign;
  scm::CommonTrafficSign::Entity supplementarySign;

  /* Define first traffic sign */
  trafficSign.type = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd;
  trafficSign.relativeDistance = 100.0_m;
  supplementarySign.type = scm::CommonTrafficSign::Type::DistanceIndication;
  supplementarySign.value = 150.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  supplementarySign.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  supplementarySign.value = 30.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficSigns.push_back(trafficSign);

  /* Define second traffic sign */
  trafficSign.type = scm::CommonTrafficSign::Type::AnnounceHighwayExit;
  trafficSign.relativeDistance = 300.0_m;
  supplementarySign.type = scm::CommonTrafficSign::Type::DistanceIndication;
  supplementarySign.value = 50.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  supplementarySign.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  supplementarySign.value = 30.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficSigns.push_back(trafficSign);

  /* Define third traffic sign */

  auto supplementarySignDistance = 50_m;

  trafficSign.type = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd;
  trafficSign.relativeDistance = 200.0_m;
  supplementarySign.type = scm::CommonTrafficSign::Type::DistanceIndication;
  supplementarySign.value = supplementarySignDistance.value();
  trafficSign.supplementarySigns.push_back(supplementarySign);
  supplementarySign.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  supplementarySign.value = 30.;
  trafficSign.supplementarySigns.push_back(supplementarySign);
  trafficSigns.push_back(trafficSign);

  const auto result = TEST_HELPER.infrastructureCharacteristics.DeriveDistanceToEndOfLaneFromTrafficSigns(trafficSigns, -1, SurroundingLane::EGO, 3);

  ASSERT_EQ(result, trafficSign.relativeDistance + supplementarySignDistance);
}

/*******************************************************************
 * CHECK Processing of Highway exit information from traffic signs *
 *******************************************************************/

struct DataFor_UpdateDistanceToStartAndEndOfNextExit
{
  std::string description;
  units::length::meter_t input_groundTruth_distanceToStartOfNextExit;
  units::length::meter_t input_groundTruth_distanceToEndOfNextExit;
  scm::LaneType input_egoLaneType;
  scm::LaneType input_rightLaneType;
  scm::LaneType input_rightRightLaneType;

  units::length::meter_t expected_groundTruth_distanceToStartOfNextExit;
  units::length::meter_t expected_groundTruth_distanceToEndOfNextExit;
  int expected_sizeOfRelativeLaneIdForJunctionIngoing;
  double expected_relativeLaneIdForJunctionIngoing;
};

class InfrastructureCharacteristics_UpdateDistanceToStartAndEndOfNextExit : public ::testing::Test,
                                                                            public ::testing::WithParamInterface<DataFor_UpdateDistanceToStartAndEndOfNextExit>
{
};

TEST_P(InfrastructureCharacteristics_UpdateDistanceToStartAndEndOfNextExit, InfrastructureCharacteristics_CheckFunction_UpdateDistanceToStartAndEndOfNextExit)
{
  DataFor_UpdateDistanceToStartAndEndOfNextExit data = GetParam();

  // Get resources
  InfrastructureCharacteristicsTester TEST_HELPER;
  // Construct default objects
  GeometryInformationSCM geometryInformationGroundTruth;
  GeometryInformationSCM geometryInformation;

  geometryInformationGroundTruth.distanceToStartOfNextExit = data.input_groundTruth_distanceToStartOfNextExit;
  geometryInformationGroundTruth.distanceToEndOfNextExit = data.input_groundTruth_distanceToEndOfNextExit;

  geometryInformationGroundTruth.Close().laneType = data.input_egoLaneType;
  geometryInformationGroundTruth.Right().laneType = data.input_rightLaneType;
  geometryInformationGroundTruth.FarRight().laneType = data.input_rightRightLaneType;

  geometryInformation.distanceToEndOfNextExit = 176._m;
  geometryInformation.distanceToStartOfNextExit = 76._m;
  TEST_HELPER.infrastructureCharacteristics.Initialize(100_ms, nullptr, &geometryInformation);

  // run test
  TEST_HELPER.infrastructureCharacteristics.UpdateDistanceToStartAndEndOfNextExit(&geometryInformationGroundTruth, 10._mps);
  ASSERT_EQ(geometryInformationGroundTruth.distanceToStartOfNextExit, data.expected_groundTruth_distanceToStartOfNextExit);
  ASSERT_EQ(geometryInformationGroundTruth.distanceToEndOfNextExit, data.expected_groundTruth_distanceToEndOfNextExit);
  ASSERT_EQ(geometryInformationGroundTruth.relativeLaneIdForJunctionIngoing.size(), data.expected_sizeOfRelativeLaneIdForJunctionIngoing);

  if (geometryInformationGroundTruth.relativeLaneIdForJunctionIngoing.size() > 0)
  {
    ASSERT_EQ(geometryInformationGroundTruth.relativeLaneIdForJunctionIngoing.at(0), data.expected_relativeLaneIdForJunctionIngoing);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_UpdateDistanceToStartAndEndOfNextExit,
    testing::Values(
        DataFor_UpdateDistanceToStartAndEndOfNextExit{
            .description = "already on exit lane",
            .input_groundTruth_distanceToStartOfNextExit = 150._m,
            .input_groundTruth_distanceToEndOfNextExit = 50._m,
            .input_egoLaneType = scm::LaneType::Exit,
            .input_rightLaneType = scm::LaneType::Exit,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_groundTruth_distanceToStartOfNextExit = 150._m,
            .expected_groundTruth_distanceToEndOfNextExit = 50._m,
            .expected_sizeOfRelativeLaneIdForJunctionIngoing = 0,
            .expected_relativeLaneIdForJunctionIngoing = 0},
        DataFor_UpdateDistanceToStartAndEndOfNextExit{
            .description = "right lane is no driving lane -> stop lane or exit lane",
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_groundTruth_distanceToEndOfNextExit = -999._m,
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Stop,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_groundTruth_distanceToStartOfNextExit = 75._m,
            .expected_groundTruth_distanceToEndOfNextExit = 175._m,
            .expected_sizeOfRelativeLaneIdForJunctionIngoing = 1,
            .expected_relativeLaneIdForJunctionIngoing = -1},
        DataFor_UpdateDistanceToStartAndEndOfNextExit{
            .description = "right-right lane is no driving lane -> stop lane or exit lane",
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_groundTruth_distanceToEndOfNextExit = -999._m,
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Driving,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_groundTruth_distanceToStartOfNextExit = 75._m,
            .expected_groundTruth_distanceToEndOfNextExit = 175_m,
            .expected_sizeOfRelativeLaneIdForJunctionIngoing = 1,
            .expected_relativeLaneIdForJunctionIngoing = -2},
        DataFor_UpdateDistanceToStartAndEndOfNextExit{
            .description = "cannot look farther than two lanes to the right -> exit lane is at least three lanes to the right",
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_groundTruth_distanceToEndOfNextExit = -999._m,
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Driving,
            .input_rightRightLaneType = scm::LaneType::Driving,
            .expected_groundTruth_distanceToStartOfNextExit = 75._m,
            .expected_groundTruth_distanceToEndOfNextExit = 175._m,
            .expected_sizeOfRelativeLaneIdForJunctionIngoing = 1,
            .expected_relativeLaneIdForJunctionIngoing = -3}));

/*******************************************
 * CHECK CalculateDistanceToStartOfExitNew *
 *******************************************/

struct DataFor_CalculateDistanceToStartOfExitNew
{
  std::string description;
  std::vector<scm::CommonTrafficSign::Type> input_signType;
  std::vector<units::length::meter_t> input_signRelativeDistance;
  std::vector<double> input_signValue;

  units::length::meter_t input_groundTruth_distanceToStartOfNextExit;
  units::length::meter_t input_lastTrafficSignIndicatingStartOfExit;

  units::length::meter_t input_distanceToStartOfExitExtrapolated;
  units::length::meter_t input_distanceToStartOfExitLast;
  bool input_isSeenStartOfExit;

  units::length::meter_t expected_distanceToStartOfExitNew;
};

class InfrastructureCharacteristics_CalculateDistanceToStartOfExitNew : public ::testing::Test,
                                                                        public ::testing::WithParamInterface<DataFor_CalculateDistanceToStartOfExitNew>
{
};

TEST_P(InfrastructureCharacteristics_CalculateDistanceToStartOfExitNew, InfrastructureCharacteristics_CheckFunction_CalculateDistanceToStartOfExitNew)
{
  DataFor_CalculateDistanceToStartOfExitNew data = GetParam();

  // Get resources
  InfrastructureCharacteristicsTester TEST_HELPER;
  TEST_HELPER.infrastructureCharacteristics.SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_START_OF_EXIT(data.input_lastTrafficSignIndicatingStartOfExit);
  TEST_HELPER.infrastructureCharacteristics.SET_CYCLE_TIME(100_ms);

  // Construct default objects
  TrafficRuleInformationSCM trafficRuleInformation;
  scm::CommonTrafficSign::Entity trafficSign;

  for (int i = 0; i < data.input_signType.size(); i++)
  {
    scm::CommonTrafficSign::Entity trafficSign{
        .type = data.input_signType[i],
        .relativeDistance = data.input_signRelativeDistance[i],
        .value = data.input_signValue[i]};
    trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  }

  trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);
  //  ON_CALL(TEST_HELPER.infrastructureCharacteristics, ExtrapolateDistanceToExitFromEarlierTrafficSignInformation(data.input_distanceToStartOfExitLast, 10.)).WillByDefault(testing::Return(data.input_distanceToStartOfExitExtrapolated));

  // run test
  auto result = TEST_HELPER.infrastructureCharacteristics.CalculateDistanceToStartOfExitNew(data.input_groundTruth_distanceToStartOfNextExit, data.input_distanceToStartOfExitLast, 10._mps);
  ASSERT_EQ(result, data.expected_distanceToStartOfExitNew);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_CalculateDistanceToStartOfExitNew,
    testing::Values(
        DataFor_CalculateDistanceToStartOfExitNew{
            .description = "Start of next exit can already be seen -> no traffic signs must be evaluated and no extrapolation is necessary",
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {50.0_m},
            .input_signValue = {50.},
            .input_groundTruth_distanceToStartOfNextExit = 50._m,
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_distanceToStartOfExitExtrapolated = 75._m,
            .input_distanceToStartOfExitLast = 76._m,
            .input_isSeenStartOfExit = true,
            .expected_distanceToStartOfExitNew = 50._m},
        DataFor_CalculateDistanceToStartOfExitNew{
            .description = "Start of next exit is announced -> get information from traffic signs",
            .input_signType = {scm::CommonTrafficSign::Type::HighwayExitPole},
            .input_signRelativeDistance = {50.0_m},
            .input_signValue = {50.},
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_lastTrafficSignIndicatingStartOfExit = 30._m,
            .input_distanceToStartOfExitExtrapolated = 75._m,
            .input_distanceToStartOfExitLast = 76._m,
            .input_isSeenStartOfExit = false,
            .expected_distanceToStartOfExitNew = 100._m},
        DataFor_CalculateDistanceToStartOfExitNew{
            .description = "Start of next exit is announced, but signs were already observed before -> ignore signs and extrapolate based on velocityEgoEstimated",
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {50.0_m},
            .input_signValue = {50.},
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_distanceToStartOfExitExtrapolated = 75._m,
            .input_distanceToStartOfExitLast = 76._m,
            .input_isSeenStartOfExit = false,
            .expected_distanceToStartOfExitNew = 75._m},
        DataFor_CalculateDistanceToStartOfExitNew{
            .description = "TEST CASE 4",
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {50.0_m},
            .input_signValue = {50.},
            .input_groundTruth_distanceToStartOfNextExit = -999._m,
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_distanceToStartOfExitExtrapolated = 75._m,
            .input_distanceToStartOfExitLast = -999._m,
            .input_isSeenStartOfExit = false,
            .expected_distanceToStartOfExitNew = -999_m}));

/**************************************************
 * CHECK eriveDistanceToEndOfExitFromTrafficSigns *
 **************************************************/

struct DataFor_DeriveDistanceToEndOfExitFromTrafficSigns
{
  std::string description;
  units::length::meter_t input_lastTrafficSignIndicatingEndOfExit;
  std::vector<scm::CommonTrafficSign::Type> input_signType;
  std::vector<units::length::meter_t> input_signRelativeDistance;
  std::vector<double> input_signValue;

  units::length::meter_t expected_distanceToEndOfExit_derivedFromTrafficSign;
};

class InfrastructureCharacteristics_DeriveDistanceToEndOfExitFromTrafficSigns : public ::testing::Test,
                                                                                public ::testing::WithParamInterface<DataFor_DeriveDistanceToEndOfExitFromTrafficSigns>
{
};

TEST_P(InfrastructureCharacteristics_DeriveDistanceToEndOfExitFromTrafficSigns, InfrastructureCharacteristics_CheckFunction_DeriveDistanceToEndOfExitFromTrafficSigns)
{
  DataFor_DeriveDistanceToEndOfExitFromTrafficSigns data = GetParam();
  InfrastructureCharacteristicsTester TEST_HELPER;
  // Get resources

  // Construct default objects
  TrafficRuleInformationSCM trafficRuleInformation;

  TEST_HELPER.infrastructureCharacteristics.SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_END_OF_EXIT(data.input_lastTrafficSignIndicatingEndOfExit);

  ASSERT_EQ(data.input_signType.size(), data.input_signRelativeDistance.size());
  ASSERT_EQ(data.input_signRelativeDistance.size(), data.input_signValue.size());

  for (int i = 0; i < data.input_signType.size(); i++)
  {
    scm::CommonTrafficSign::Entity trafficSign{
        .type = data.input_signType[i],
        .relativeDistance = data.input_signRelativeDistance[i],
        .value = data.input_signValue[i]};
    trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  }

  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);

  // run test
  auto result = TEST_HELPER.infrastructureCharacteristics.DeriveDistanceToEndOfExitFromTrafficSigns(trafficRuleInformation.Close().trafficSigns);
  ASSERT_EQ(result, data.expected_distanceToEndOfExit_derivedFromTrafficSign);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_DeriveDistanceToEndOfExitFromTrafficSigns,
    testing::Values(
        DataFor_DeriveDistanceToEndOfExitFromTrafficSigns{
            .description = "End of next exit is announced -> but signs were already observed before -> ignore signs",
            .input_lastTrafficSignIndicatingEndOfExit = 1000._m,
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {100.0_m},
            .input_signValue = {1000.},
            .expected_distanceToEndOfExit_derivedFromTrafficSign = -999._m},
        DataFor_DeriveDistanceToEndOfExitFromTrafficSigns{
            .description = "End of next exit is announced -> get information from traffic signs",
            .input_lastTrafficSignIndicatingEndOfExit = -999._m,
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {100.0_m},
            .input_signValue = {1000.},
            .expected_distanceToEndOfExit_derivedFromTrafficSign = 1100._m}));

/*****************************************************
 * CHECK DeriveDistanceToStartOfExitFromTrafficSigns *
 *****************************************************/

struct DataFor_DeriveDistanceToStartOfExitFromTrafficSigns
{
  std::string description;
  units::length::meter_t input_lastTrafficSignIndicatingStartOfExit;
  std::vector<scm::CommonTrafficSign::Type> input_signType;
  std::vector<units::length::meter_t> input_signRelativeDistance;
  std::vector<double> input_signValue;

  units::length::meter_t expected_distanceToStartOfExit_derivedFromTrafficSign;
};

class InfrastructureCharacteristics_DeriveDistanceToStartOfExitFromTrafficSigns : public ::testing::Test,
                                                                                  public ::testing::WithParamInterface<DataFor_DeriveDistanceToStartOfExitFromTrafficSigns>
{
};

TEST_P(InfrastructureCharacteristics_DeriveDistanceToStartOfExitFromTrafficSigns, InfrastructureCharacteristics_CheckFunction_DeriveDistanceToStartOfExitFromTrafficSigns)
{
  DataFor_DeriveDistanceToStartOfExitFromTrafficSigns data = GetParam();
  InfrastructureCharacteristicsTester TEST_HELPER;
  // Get resources

  // Construct default objects
  TrafficRuleInformationSCM trafficRuleInformation;

  TEST_HELPER.infrastructureCharacteristics.SET_VALUE_LAST_TRAFFIC_SIGN_INDICATING_START_OF_EXIT(data.input_lastTrafficSignIndicatingStartOfExit);

  ASSERT_EQ(data.input_signType.size(), data.input_signRelativeDistance.size());
  ASSERT_EQ(data.input_signRelativeDistance.size(), data.input_signValue.size());

  for (int i = 0; i < data.input_signType.size(); i++)
  {
    scm::CommonTrafficSign::Entity trafficSign{
        .type = data.input_signType[i],
        .relativeDistance = data.input_signRelativeDistance[i],
        .value = data.input_signValue[i]};
    trafficRuleInformation.Close().trafficSigns.push_back(trafficSign);
  }

  TEST_HELPER.infrastructureCharacteristics.SetTrafficRuleInformation(&trafficRuleInformation);

  // run test
  auto result = TEST_HELPER.infrastructureCharacteristics.DeriveDistanceToStartOfExitFromTrafficSigns(trafficRuleInformation.Close().trafficSigns);
  ASSERT_EQ(result, data.expected_distanceToStartOfExit_derivedFromTrafficSign);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_DeriveDistanceToStartOfExitFromTrafficSigns,
    testing::Values(
        DataFor_DeriveDistanceToStartOfExitFromTrafficSigns{
            .description = "Start of next exit is announced -> get information from traffic signs",
            .input_lastTrafficSignIndicatingStartOfExit = 1000._m,
            .input_signType = {scm::CommonTrafficSign::Type::HighwayExitPole},
            .input_signRelativeDistance = {100.0_m},
            .input_signValue = {100.},
            .expected_distanceToStartOfExit_derivedFromTrafficSign = 200._m},
        DataFor_DeriveDistanceToStartOfExitFromTrafficSigns{
            .description = "Start of next exit is announced -> get information from traffic signs",
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_signType = {scm::CommonTrafficSign::Type::HighwayExitPole, scm::CommonTrafficSign::Type::HighwayExitPole},
            .input_signRelativeDistance = {50.0_m, 100.0_m},
            .input_signValue = {200, 100.},
            .expected_distanceToStartOfExit_derivedFromTrafficSign = 200._m},
        DataFor_DeriveDistanceToStartOfExitFromTrafficSigns{
            .description = "Start of next exit is announced -> Skip sign -> sign doesn't fit",
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_signType = {scm::CommonTrafficSign::Type::HighwayExitPole, scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {50.0_m, 100.0_m},
            .input_signValue = {200, 100.},
            .expected_distanceToStartOfExit_derivedFromTrafficSign = 250._m},
        DataFor_DeriveDistanceToStartOfExitFromTrafficSigns{
            .description = "Start of next exit is announced -> Skip sign -> sign doesn't fit",
            .input_lastTrafficSignIndicatingStartOfExit = -999._m,
            .input_signType = {scm::CommonTrafficSign::Type::AnnounceHighwayExit},
            .input_signRelativeDistance = {50.0_m},
            .input_signValue = {200},
            .expected_distanceToStartOfExit_derivedFromTrafficSign = -999._m}));

/******************************************
 * CHECK EstimateRelativeLaneIdOfExitLane *
 ******************************************/

struct DataFor_EstimateRelativeLaneIdOfExitLane
{
  std::string description;
  scm::LaneType input_egoLaneType;
  scm::LaneType input_rightLaneType;
  scm::LaneType input_rightRightLaneType;

  int expected_relativeLaneIdForJunctionIngoing;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_EstimateRelativeLaneIdOfExitLane& obj)
  {
    return os
           << " | input_egoLaneType (LaneType): " << ScmCommons::LaneTypeToString(obj.input_egoLaneType)
           << " | input_rightLaneType (LaneType): " << ScmCommons::LaneTypeToString(obj.input_rightLaneType)
           << " | input_righRightLaneType (LaneType): " << ScmCommons::LaneTypeToString(obj.input_rightRightLaneType)
           << " | expected_relativeLaneIdForJunctionIngoing (int): " << obj.expected_relativeLaneIdForJunctionIngoing;
  }
};

class InfrastructureCharacteristics_EstimateRelativeLaneIdOfExitLane : public ::testing::Test,
                                                                       public ::testing::WithParamInterface<DataFor_EstimateRelativeLaneIdOfExitLane>
{
};

TEST_P(InfrastructureCharacteristics_EstimateRelativeLaneIdOfExitLane, InfrastructureCharacteristics_CheckFunction_EstimateRelativeLaneIdOfExitLane)
{
  InfrastructureCharacteristicsTester TEST_HELPER;
  DataFor_EstimateRelativeLaneIdOfExitLane data = GetParam();

  // Get resources
  TEST_HELPER.infrastructureCharacteristics.Initialize(100_ms, nullptr, nullptr);

  // Construct default objects
  GeometryInformationSCM geometryInformationGroundTruth;

  geometryInformationGroundTruth.Close().laneType = data.input_egoLaneType;
  geometryInformationGroundTruth.Right().laneType = data.input_rightLaneType;
  geometryInformationGroundTruth.FarRight().laneType = data.input_rightRightLaneType;

  // run test
  int result = TEST_HELPER.infrastructureCharacteristics.EstimateRelativeLaneIdOfExitLane(&geometryInformationGroundTruth);
  ASSERT_EQ(result, data.expected_relativeLaneIdForJunctionIngoing);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    InfrastructureCharacteristics_EstimateRelativeLaneIdOfExitLane,
    testing::Values(
        DataFor_EstimateRelativeLaneIdOfExitLane{
            .description = "Already on exit lane",
            .input_egoLaneType = scm::LaneType::Exit,
            .input_rightLaneType = scm::LaneType::Exit,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_relativeLaneIdForJunctionIngoing = 0},
        DataFor_EstimateRelativeLaneIdOfExitLane{
            .description = "Right lane is stop lane",
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Stop,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_relativeLaneIdForJunctionIngoing = -1},
        DataFor_EstimateRelativeLaneIdOfExitLane{
            .description = "Right-Right lane is exit lane",
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Driving,
            .input_rightRightLaneType = scm::LaneType::Exit,
            .expected_relativeLaneIdForJunctionIngoing = -2},
        DataFor_EstimateRelativeLaneIdOfExitLane{
            .description = "exit lane is at least three lanes to the right",
            .input_egoLaneType = scm::LaneType::Driving,
            .input_rightLaneType = scm::LaneType::Driving,
            .input_rightRightLaneType = scm::LaneType::Driving,
            .expected_relativeLaneIdForJunctionIngoing = -3}));
