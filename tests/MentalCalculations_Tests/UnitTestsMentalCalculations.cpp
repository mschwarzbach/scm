/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-nice-strict.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeLaneChangeDimension.h"
#include "../Fakes/FakeLongitudinalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeScmDependencies.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "Fakes/FakeAccelerationCalculationsQuery.h"
#include "MentalCalculations.h"
#include "TestMentalCalculations.h"
#include "include/common/ScmEnums.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::Matcher;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct MentalCalculationsTester
{
  class MentalCalculationsUnderTest : MentalCalculations
  {
  public:
    template <typename... Args>
    MentalCalculationsUnderTest(Args&&... args)
        : MentalCalculations{std::forward<Args>(args)...} {};

    using MentalCalculations::AdjustDesiredFollowingDistanceDueToCooperation;
    using MentalCalculations::AnticipatedLaneChangerPresent;
    using MentalCalculations::CalculateAccelerationToDefuse;
    using MentalCalculations::CalculateAccelerationToDefuseLateral;
    using MentalCalculations::CalculateFollowingAcceleration;
    using MentalCalculations::CalculateLateralOffsetNeutralPositionScaled;
    using MentalCalculations::CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion;
    using MentalCalculations::CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion;
    using MentalCalculations::CalculatePlatoonPassingVelocityLimits;
    using MentalCalculations::CalculateReachedDistanceForUniformlyAcceleratedMotion;
    using MentalCalculations::CalculateRiskObstacle;
    using MentalCalculations::CalculateUrgencyFactor;
    using MentalCalculations::CalculateVelocityAtCurrentParameters;
    using MentalCalculations::CalculateVLegal;
    using MentalCalculations::CheckRelativeDistanceIsPlatoon;
    using MentalCalculations::CheckVelocityDifferenceIsPlatoon;
    using MentalCalculations::DetermineLateralOffsetDueToCooperation;
    using MentalCalculations::DetermineUrgencyFactorForNextExit;
    using MentalCalculations::EstimatedTimeToLeaveLane;
    using MentalCalculations::EvaluateAOIsAndDistances;
    using MentalCalculations::GetDistanceForRegulatingVelocity;
    using MentalCalculations::GetDistanceToPointOfNoReturnForBrakingToEndOfExit;
    using MentalCalculations::GetDistanceToPointOfNoReturnForBrakingToEndOfLane;
    using MentalCalculations::GetEqDistance;
    using MentalCalculations::GetInfDistance;
    using MentalCalculations::GetInfluencingDistance;
    using MentalCalculations::GetIntensityForSlowerLeadingVehicle;
    using MentalCalculations::GetLaneMarkingAtAoi;
    using MentalCalculations::GetLaneMarkingAtDistance;
    using MentalCalculations::GetMinDistance;
    using MentalCalculations::GetRelativeNetDistance;
    using MentalCalculations::GetRelativeNetDistanceEq;
    using MentalCalculations::GetSideTtc;
    using MentalCalculations::GetTauDotEq;
    using MentalCalculations::GetTimeHeadwayBetweenTwoAreasOfInterest;
    using MentalCalculations::GetUrgencyFactorForAcceleration;
    using MentalCalculations::Initialize;
    using MentalCalculations::InterpreteBrokenBoldLaneMarking;
    using MentalCalculations::PlatoonBelowJamSpeed;
    using MentalCalculations::PossiblePlatoonCarsVisible;
    using MentalCalculations::TransitionSurroundingData;
    using MentalCalculations::VReasonPassingSlowPlatoon;

    using MentalCalculations::_persistentCooperativeBehavior;

    void SET_PERSITENT_COOPERATIVE_BEHAVIOR(bool coop)
    {
      _persistentCooperativeBehavior = coop;
    }

    void SET_ACCELERATION_CALCULATION_QUERY(std::shared_ptr<AccelerationCalculationsQueryInterface> fakeAccelerationCalculationsQueryInterface)
    {
      _accelerationCalculationQuery = fakeAccelerationCalculationsQueryInterface;
    }
  };

  MentalCalculationsTester()
      : fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeLongitudinalCalculations{},
        fakeSurroundingVehicleQueryFactory{},
        mentalCalculations(fakeFeatureExtractor,
                           true,
                           ScmDefinitions::INF_VELOCITY,
                           true,
                           true)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;

  MentalCalculationsUnderTest mentalCalculations;
};

/***********************************************************
 * CHECK GetDistanceToPointOfNoReturnForBrakingToEndOfLane *
 ***********************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLane
{
  units::acceleration::meters_per_second_squared_t input_maximumLongitudinalDecceleration;
  units::length::meter_t expected_Result;
};

class MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLane : public ::testing::Test,
                                                                             public ::testing::WithParamInterface<DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLane>
{
};

TEST_P(MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLane, MentalCalculations_CheckFunction_GetDistanceToPointOfNoReturnForBrakingToEndOfLane)
{
  // Get resources for testing
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLane data = GetParam();

  // Set up tests
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalDeceleration = data.input_maximumLongitudinalDecceleration;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  // Run Test
  auto result = TEST_HELPER.mentalCalculations.GetDistanceToPointOfNoReturnForBrakingToEndOfLane(20.0_mps, 500.0_m);

  // Evaluate results
  ASSERT_EQ(data.expected_Result, result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_CASE_P(
    Default,
    MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLane,
    testing::Values(
        DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLane{
            .input_maximumLongitudinalDecceleration = 5._mps_sq,
            .expected_Result = 500._m - (20._m * 20._m) / (2. * 0.75 * 5._m)},
        DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLane{
            .input_maximumLongitudinalDecceleration = 7.5_mps_sq,
            .expected_Result = 500._m - (20._m * 20._m) / (2. * 0.75 * 7.5_m)}));

/***********************************************
 * CHECK MIN_DISTANCE and EQUILIBRIUM_DISTANCE *
 ***********************************************/

struct DataForAoi_EgoFront
{
  units::time::second_t rt;
  units::acceleration::meters_per_second_squared_t comfortLongitudinalDeceleration;
  units::acceleration::meters_per_second_squared_t maxLongitudinalDeceleration;
  double maxAnticipatedLongitudinalDecelerationFront;
  units::velocity::meters_per_second_t EgoVelocity;
  units::velocity::meters_per_second_t ObservedVelocity;
  AreaOfInterest ObservedAreaOfInterest;
};

class MentalCalculations_MinEqDistance : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataForAoi_EgoFront>
{
};

TEST_P(MentalCalculations_MinEqDistance, CompareMinEqDistance_ForEgoFront)
{
  // Get test data
  MentalCalculationsTester TEST_HELPER;
  DataForAoi_EgoFront data = GetParam();

  // Get resources
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set up test
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  OwnVehicleInformationScmExtended ego;
  ego.longitudinalVelocity = 1.23_mps;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));

  AreaOfInterest aoi = data.ObservedAreaOfInterest;

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  NiceMock<FakeSurroundingVehicle> fakeObserved;
  ON_CALL(fakeObserved, GetAssignedAoi()).WillByDefault(Return(aoi));
  ON_CALL(fakeObserved, GetLongitudinalVelocity()).WillByDefault(Return(data.ObservedVelocity));
  ON_CALL(fakeObserved, GetLongitudinalAcceleration()).WillByDefault(Return(0.0_mps_sq));
  ON_CALL(fakeObserved, GetLength()).WillByDefault(Return(5.2_m));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(data.EgoVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration()).WillByDefault(Return(0.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(aoi, _)).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(aoi, _)).WillByDefault(Return(&fakeObserved));

  ON_CALL(fakeObserved, ToTheSideOfEgo()).WillByDefault(Return(false));
  AreaOfInterest aoiReference = AreaOfInterest::NumberOfAreaOfInterests;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(aoiReference, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInSideArea(aoiReference, aoi)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInSurroundingArea(aoiReference, aoi, _)).WillByDefault(Return(true));

  DriverParameters driverParameters{};
  driverParameters.pedalChangeTimeMinimum = 0.373_s;
  driverParameters.carQueuingDistance = 5.0_m;
  driverParameters.proportionalityFactorForFollowingDistance = 6.0;
  driverParameters.reactionBaseTimeMinimum = data.rt;
  driverParameters.comfortLongitudinalDeceleration = data.comfortLongitudinalDeceleration;
  driverParameters.maximumLongitudinalDeceleration = data.maxLongitudinalDeceleration;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  // Call test
  auto minimumDistance = TEST_HELPER.mentalCalculations.GetMinDistance(aoi, MinThwPerspective::NORM, aoiReference, units::velocity::meters_per_second_t(ScmDefinitions::DEFAULT_VALUE));
  auto equilibriumDistance = TEST_HELPER.mentalCalculations.GetEqDistance(aoi, aoiReference);

  // Evaluate results
  ASSERT_TRUE(minimumDistance <= equilibriumDistance) << std::endl
                                                      << "minimumDistance ("
                                                      << minimumDistance << ") <= "
                                                      << "equilibriumDistance (" << equilibriumDistance << ")";
  ASSERT_TRUE(minimumDistance >= 0._m);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_MinEqDistance,
    testing::Values(
        DataForAoi_EgoFront{
            .rt = .3_s,
            .comfortLongitudinalDeceleration = 2._mps_sq,
            .maxLongitudinalDeceleration = 7._mps_sq,
            .maxAnticipatedLongitudinalDecelerationFront = 7.,
            .EgoVelocity = 33.33_mps,
            .ObservedVelocity = units::velocity::meters_per_second_t(110._kph),
            .ObservedAreaOfInterest = AreaOfInterest::EGO_FRONT},
        DataForAoi_EgoFront{
            .rt = .4_s,
            .comfortLongitudinalDeceleration = 2.5_mps_sq,
            .maxLongitudinalDeceleration = 8._mps_sq,
            .maxAnticipatedLongitudinalDecelerationFront = 8.,
            .EgoVelocity = 33.33_mps,
            .ObservedVelocity = units::velocity::meters_per_second_t(110._kph),
            .ObservedAreaOfInterest = AreaOfInterest::EGO_FRONT},
        DataForAoi_EgoFront{
            .rt = .6_s,
            .comfortLongitudinalDeceleration = 1.5_mps_sq,
            .maxLongitudinalDeceleration = 6.5_mps_sq,
            .maxAnticipatedLongitudinalDecelerationFront = 6.5,
            .EgoVelocity = 33.33_mps,
            .ObservedVelocity = units::velocity::meters_per_second_t(110._kph),
            .ObservedAreaOfInterest = AreaOfInterest::EGO_FRONT},
        DataForAoi_EgoFront{
            .rt = .3_s,
            .comfortLongitudinalDeceleration = 2._mps_sq,
            .maxLongitudinalDeceleration = 7._mps_sq,
            .maxAnticipatedLongitudinalDecelerationFront = 7.,
            .EgoVelocity = 33.33_mps,
            .ObservedVelocity = units::velocity::meters_per_second_t(110._kph),
            .ObservedAreaOfInterest = AreaOfInterest::EGO_REAR}));

/**********************************
 * CHECK GetRelativeNetDistanceEq *
 **********************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataForGetRelativeNetDistanceEq
{
  AreaOfInterest input_Aoi;
  units::velocity::meters_per_second_t input_VTarget;
  units::velocity::meters_per_second_t input_VEgo;
  bool input_IsMergeRegulate;
  units::length::meter_t input_NetDistance;
  units::velocity::meters_per_second_t mock_GetVReasonRightOvertakingProhibition;
  units::length::meter_t result_AEqFollowing;
};

class MentalCalculations_GetRelativeNetDistanceEq : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataForGetRelativeNetDistanceEq>
{
};

TEST_P(MentalCalculations_GetRelativeNetDistanceEq, MentalCalculations_CheckFunction_GetRelativeNetDistanceEq)
{
  // Create required classes for test
  DataForGetRelativeNetDistanceEq data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  auto mentalCalculations = std::make_unique<TestMentalCalculations>(fakeFeatureExtractor);

  mentalCalculations->Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  ON_CALL(fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(15.0_mps));

  // Set up test
  ON_CALL(fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(data.input_VTarget));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(data.input_Aoi)).WillByDefault(Return(data.input_NetDistance));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(data.input_VEgo));
  ON_CALL(fakeMentalModel, GetIsMergeRegulate(data.input_Aoi, _)).WillByDefault(Return(data.input_IsMergeRegulate));
  ON_CALL(fakeMentalModel, DetermineVelocityReasonRightOvertakingProhibition()).WillByDefault(Return(data.mock_GetVReasonRightOvertakingProhibition));
  ON_CALL(*mentalCalculations, GetEqDistance(_, _)).WillByDefault(Return(100._m));
  ON_CALL(*mentalCalculations, GetMinDistance(_, _, _, _)).WillByDefault(Return(80._m));

  // Call test
  auto result = mentalCalculations->TestGetRelativeNetDistanceEq(data.input_Aoi);

  // Evaluate results
  ASSERT_EQ(data.result_AEqFollowing, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetRelativeNetDistanceEq,
    testing::Values(
        // Not merging, not passing on right and above jam velocity
        DataForGetRelativeNetDistanceEq{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_VTarget = 25._mps,
            .input_VEgo = 20._mps,
            .input_IsMergeRegulate = false,
            .input_NetDistance = 150._m,
            .mock_GetVReasonRightOvertakingProhibition = 30._mps,
            .result_AEqFollowing = 50._m},
        DataForGetRelativeNetDistanceEq{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_VTarget = 25._mps,
            .input_VEgo = 30._mps,
            .input_IsMergeRegulate = false,
            .input_NetDistance = 150._m,
            .mock_GetVReasonRightOvertakingProhibition = 30._mps,
            .result_AEqFollowing = 50._m},
        // Merging, not passing on right and above jam velocity
        DataForGetRelativeNetDistanceEq{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_VTarget = 25._mps,
            .input_VEgo = 20._mps,
            .input_IsMergeRegulate = true,
            .input_NetDistance = 150._m,
            .mock_GetVReasonRightOvertakingProhibition = 30._mps,
            .result_AEqFollowing = 70._m},
        // Not Merging, passing on right and above jam velocity
        DataForGetRelativeNetDistanceEq{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_VTarget = 25._mps,
            .input_VEgo = 20._mps,
            .input_IsMergeRegulate = false,
            .input_NetDistance = 150._m,
            .mock_GetVReasonRightOvertakingProhibition = 25._mps,
            .result_AEqFollowing = 125._m},
        // Not Merging, not passing on right and below jam velocity should be the same as Test/0
        DataForGetRelativeNetDistanceEq{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_VTarget = 25._mps,
            .input_VEgo = 10._mps,
            .input_IsMergeRegulate = false,
            .input_NetDistance = 150._m,
            .mock_GetVReasonRightOvertakingProhibition = 30._mps,
            .result_AEqFollowing = 50._m}));

/*****************************
 * CHECK GetLaneMarkingAtAoi *
 *****************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetLaneMarkingAtAoi
{
  AreaOfInterest input_Aoi;
  units::length::meter_t input_RelativeNetDistance;
  std::vector<scm::LaneMarking::Type> input_LaneMarkingsTypes;  // Position in vector must correspond with relative distance
  std::vector<units::length::meter_t> input_LaneMarkingsRelativeDistance;  // Position in vector must correspond with type
  scm::LaneMarking::Type result_Type;
  units::length::meter_t result_RelativeDistance;
};

class MentalCalculations_GetLaneMarkingSection : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_GetLaneMarkingAtAoi>
{
};

TEST_P(MentalCalculations_GetLaneMarkingSection, MentalCalculations_CheckFunction_GetLaneMarkingSection)
{
  // Get resources for testing
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set up tests
  DataFor_GetLaneMarkingAtAoi data = GetParam();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.input_Aoi, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(data.input_Aoi)).WillByDefault(Return(data.input_RelativeNetDistance));

  ASSERT_EQ(data.input_LaneMarkingsTypes.size(), data.input_LaneMarkingsRelativeDistance.size());

  std::vector<scm::LaneMarking::Entity> laneMarkings;
  for (auto ii = 0; ii < static_cast<int>(data.input_LaneMarkingsTypes.size()); ++ii)
  {
    scm::LaneMarking::Entity laneMarking;
    laneMarking.type = data.input_LaneMarkingsTypes.at(ii);
    laneMarking.relativeStartDistance = data.input_LaneMarkingsRelativeDistance.at(ii);
    laneMarkings.push_back(laneMarking);
  }

  TrafficRuleInformationScmExtended trafficRuleInformation;
  trafficRuleInformation.Close().laneMarkingsLeft = laneMarkings;
  trafficRuleInformation.Close().laneMarkingsRight = laneMarkings;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficRuleInformation()).WillByDefault(Return(&trafficRuleInformation));
  // Run test
  scm::LaneMarking::Entity result = TEST_HELPER.mentalCalculations.GetLaneMarkingAtAoi(data.input_Aoi);

  // Evaluate results
  ASSERT_EQ(data.result_Type, result.type);
  ASSERT_EQ(data.result_RelativeDistance, result.relativeStartDistance);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetLaneMarkingSection,
    testing::Values(
        DataFor_GetLaneMarkingAtAoi{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_RelativeNetDistance = 100._m,
            .input_LaneMarkingsTypes = {scm::LaneMarking::Type::Broken},
            .input_LaneMarkingsRelativeDistance = {0._m},
            .result_Type = scm::LaneMarking::Type::Broken,
            .result_RelativeDistance = 0._m},
        DataFor_GetLaneMarkingAtAoi{
            .input_Aoi = AreaOfInterest::LEFT_SIDE,
            .input_RelativeNetDistance = 0._m,
            .input_LaneMarkingsTypes = {scm::LaneMarking::Type::Solid, scm::LaneMarking::Type::Broken},
            .input_LaneMarkingsRelativeDistance = {0._m, 80._m},
            .result_Type = scm::LaneMarking::Type::Solid,
            .result_RelativeDistance = 0._m},
        DataFor_GetLaneMarkingAtAoi{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_RelativeNetDistance = 100._m,
            .input_LaneMarkingsTypes = {scm::LaneMarking::Type::Solid, scm::LaneMarking::Type::Broken, scm::LaneMarking::Type::Broken_Solid},
            .input_LaneMarkingsRelativeDistance = {180._m, 80._m, 110._m},
            .result_Type = scm::LaneMarking::Type::Broken,
            .result_RelativeDistance = 80._m},
        DataFor_GetLaneMarkingAtAoi{
            .input_Aoi = AreaOfInterest::RIGHT_FRONT,
            .input_RelativeNetDistance = 60._m,
            .input_LaneMarkingsTypes = {scm::LaneMarking::Type::Solid_Broken, scm::LaneMarking::Type::Broken, scm::LaneMarking::Type::Broken_Solid, scm::LaneMarking::Type::Solid_Solid},
            .input_LaneMarkingsRelativeDistance = {0._m, 80._m, 90._m, 200._m},
            .result_Type = scm::LaneMarking::Type::Solid_Broken,
            .result_RelativeDistance = 0._m},
        DataFor_GetLaneMarkingAtAoi{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_RelativeNetDistance = 100._m,
            .input_LaneMarkingsTypes = {},
            .input_LaneMarkingsRelativeDistance = {},
            .result_Type = scm::LaneMarking::Type::None,
            .result_RelativeDistance = -999._m}));

/**************************
 * CHECK CalculateVLegal *
 **************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_CalculateVLegal
{
  bool input_spawn;
  units::length::meter_t input_distanceToSignLeft;
  units::length::meter_t input_distanceToSignRight;
  std::vector<bool> expected_foundSign;
  std::vector<units::velocity::meters_per_second_t> expected_calculatedVLegal;
};

class MentalCalculations_CalculateVLegal : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_CalculateVLegal>
{
};

TEST_P(MentalCalculations_CalculateVLegal, MentalCalculations_CheckFunction_CalculateVLegal)
{
  // Get resources for testing
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  DataFor_CalculateVLegal data = GetParam();

  // Set up test
  scm::CommonTrafficSign::Entity TrafficSignLeft;
  TrafficSignLeft.relativeDistance = data.input_distanceToSignLeft;
  TrafficSignLeft.value = 120.;
  TrafficSignLeft.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  std::vector<scm::CommonTrafficSign::Entity> TrafficSignsLeft;
  TrafficSignsLeft.push_back(TrafficSignLeft);

  scm::CommonTrafficSign::Entity TrafficSignEgo;
  TrafficSignLeft.relativeDistance = -999._m;
  TrafficSignLeft.value = INFINITY;
  TrafficSignLeft.type = scm::CommonTrafficSign::Type::Undefined;
  std::vector<scm::CommonTrafficSign::Entity> TrafficSignsEgo;
  TrafficSignsEgo.push_back(TrafficSignEgo);

  scm::CommonTrafficSign::Entity TrafficSignRight;
  TrafficSignRight.relativeDistance = data.input_distanceToSignRight;
  TrafficSignRight.value = 100.;
  TrafficSignRight.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  std::vector<scm::CommonTrafficSign::Entity> TrafficSignsRight;
  TrafficSignsRight.push_back(TrafficSignRight);

  TrafficRuleInformationScmExtended trafficRuleInformationScmExtended;
  trafficRuleInformationScmExtended.Left().trafficSigns = TrafficSignsLeft;
  trafficRuleInformationScmExtended.Close().trafficSigns = TrafficSignsEgo;
  trafficRuleInformationScmExtended.Right().trafficSigns = TrafficSignsRight;

  std::vector<std::vector<scm::CommonTrafficSign::Entity>> TrafficSigns;
  TrafficSigns.push_back(TrafficSignsLeft);
  TrafficSigns.push_back(TrafficSignsEgo);
  TrafficSigns.push_back(TrafficSignsRight);

  scm::signal::DriverInput driverInput;
  driverInput.driverParameters.previewDistance = 300.0_m;
  driverInput.driverParameters.desiredVelocity = 25.0_mps;
  driverInput.driverParameters.amountOfDesiredVelocityViolation = 4.0_mps;

  NiceMock<FakeScmDependencies> fakeScmDependencies;
  fakeScmDependencies.UpdateParametersScmSignal(driverInput);

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, GetTrafficRuleInformation()).WillByDefault(Return(&trafficRuleInformationScmExtended));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverInput.driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(25._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficSigns()).WillByDefault(Return(TrafficSigns));

  // Call test
  const auto& result_calculateVLegalFunction = TEST_HELPER.mentalCalculations.CalculateVLegal(data.input_spawn);
  std::vector<bool> result_foundSign = {result_calculateVLegalFunction.at(0).signDetected, result_calculateVLegalFunction.at(1).signDetected, result_calculateVLegalFunction.at(2).signDetected};

  // Evaluate results
  ASSERT_EQ(result_foundSign, data.expected_foundSign);
  ASSERT_DOUBLE_EQ(data.expected_calculatedVLegal.at(0).value(), result_calculateVLegalFunction.at(0).speedLimit.value());
  ASSERT_DOUBLE_EQ(data.expected_calculatedVLegal.at(1).value(), result_calculateVLegalFunction.at(1).speedLimit.value());
  ASSERT_DOUBLE_EQ(data.expected_calculatedVLegal.at(2).value(), result_calculateVLegalFunction.at(2).speedLimit.value());
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateVLegal,
    testing::Values(
        DataFor_CalculateVLegal{
            .input_spawn = true,
            .input_distanceToSignLeft = 50._m,
            .input_distanceToSignRight = -5._m,
            .expected_foundSign = {true, false, true},
            .expected_calculatedVLegal = {120._mps, ScmDefinitions::INF_VELOCITY, 100._mps}},
        DataFor_CalculateVLegal{
            .input_spawn = false,
            .input_distanceToSignLeft = 50._m,
            .input_distanceToSignRight = -5._m,
            .expected_foundSign = {true, false, false},
            .expected_calculatedVLegal = {120._mps, ScmDefinitions::INF_VELOCITY, ScmDefinitions::INF_VELOCITY}},  // As before but no spawn -> no recognition of signs in negative distance
        DataFor_CalculateVLegal{
            .input_spawn = false,
            .input_distanceToSignLeft = 50._m,
            .input_distanceToSignRight = 350._m,
            .expected_foundSign = {true, false, false},
            .expected_calculatedVLegal = {120._mps, ScmDefinitions::INF_VELOCITY, ScmDefinitions::INF_VELOCITY}},  // As before but right Sign outside previewDistance
        DataFor_CalculateVLegal{
            .input_spawn = false,
            .input_distanceToSignLeft = 80._m,
            .input_distanceToSignRight = 50._m,
            .expected_foundSign = {true, false, true},
            .expected_calculatedVLegal = {120._mps, ScmDefinitions::INF_VELOCITY, 100._mps}}));  // As before but both signs now in range

/*****************************************
 * CHECK CalculateAccelerationToDefuse *
 *****************************************/

struct DataFor_CalculateAccelerationToDefuse
{
  AreaOfInterest in_Aoi;
  units::acceleration::meters_per_second_squared_t in_accelerationLimit;
  units::acceleration::meters_per_second_squared_t in_accelerationObject;
  units::time::second_t in_ttc;
  units::velocity::meters_per_second_t in_velocityEgo;
  double in_velocityObject;
  double in_estimatedVelocityObject;
  units::length::meter_t in_relativeNetDistance;
  units::acceleration::meters_per_second_squared_t out_accelerationToDefuse;
};

class MentalCalculations_CalculateAccelerationToDefuse : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_CalculateAccelerationToDefuse>
{
};

TEST_P(MentalCalculations_CalculateAccelerationToDefuse, MentalCalculations_CheckFunction_CalculateAccelerationToDefuse)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculateAccelerationToDefuse data = GetParam();
  // Set up tests
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  OwnVehicleInformationScmExtended ego{};
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));

  NiceMock<FakeSurroundingVehicle> fakeObserved;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeObserved));
  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, _, _, _, _, _, _)).WillByDefault(Return(80.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(_, _)).WillByDefault(Return(data.in_ttc));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(data.in_velocityEgo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration(_, _)).WillByDefault(Return(data.in_accelerationObject));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(data.in_Aoi)).WillByDefault(Return(data.in_relativeNetDistance));

  DriverParameters driverParameters{};
  driverParameters.maximumLongitudinalAcceleration = 4.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  auto result = TEST_HELPER.mentalCalculations.CalculateAccelerationToDefuse(data.in_Aoi, data.in_accelerationLimit);

  ASSERT_EQ(result, data.out_accelerationToDefuse);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateAccelerationToDefuse,
    testing::Values(
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_REAR,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 5.0_mps_sq,
            .in_ttc = 9.0_s,
            .in_velocityEgo = 7.0_mps,
            .in_estimatedVelocityObject = 8.0,
            .in_relativeNetDistance = 12.0_m,
            .out_accelerationToDefuse = 4.0_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_REAR,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 0.0_mps_sq,
            .in_ttc = 5.0_s,
            .in_velocityEgo = 5.0_mps,
            .in_estimatedVelocityObject = 5.0,
            .in_relativeNetDistance = 90.0_m,
            .out_accelerationToDefuse = 1.0_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_REAR,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 10.0_mps_sq,
            .in_ttc = -5.0_s,
            .in_velocityEgo = 5.0_mps,
            .in_estimatedVelocityObject = 5.0,
            .in_relativeNetDistance = 90.0_m,
            .out_accelerationToDefuse = 1.0_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_FRONT,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 10.0_mps_sq,
            .in_ttc = 5.0_s,
            .in_velocityEgo = 0.0_mps,
            .in_estimatedVelocityObject = 5.0,
            .in_relativeNetDistance = 90.0_m,
            .out_accelerationToDefuse = 10.0_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_FRONT,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 1.0_mps_sq,
            .in_ttc = 9.0_s,
            .in_velocityEgo = 7.0_mps,
            .in_velocityObject = 5.0,
            .in_estimatedVelocityObject = 8.0,
            .in_relativeNetDistance = 12.0_m,
            .out_accelerationToDefuse = 3.3_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_FRONT,
            .in_accelerationLimit = 3.3_mps_sq,
            .in_accelerationObject = 1.0_mps_sq,
            .in_ttc = 9.0_s,
            .in_velocityEgo = 7.0_mps,
            .in_velocityObject = 5.0,
            .in_estimatedVelocityObject = 8.0,
            .in_relativeNetDistance = 12.0_m,
            .out_accelerationToDefuse = 3.3_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_FRONT,
            .in_accelerationLimit = 1.0_mps_sq,
            .in_accelerationObject = 3.0_mps_sq,
            .in_ttc = 9.0_s,
            .in_velocityEgo = 7.0_mps,
            .in_velocityObject = 5.0,
            .in_estimatedVelocityObject = 9.0,
            .in_relativeNetDistance = 12.0_m,
            .out_accelerationToDefuse = 1.0_mps_sq},
        DataFor_CalculateAccelerationToDefuse{
            .in_Aoi = AreaOfInterest::EGO_FRONT,
            .in_accelerationLimit = 2.5_mps_sq,
            .in_accelerationObject = 0.0_mps_sq,
            .in_ttc = 4.0_s,
            .in_velocityEgo = 10.0_mps,
            .in_estimatedVelocityObject = 8.0,
            .in_relativeNetDistance = 15.0_m,
            .out_accelerationToDefuse = -2.5_mps_sq}));

/**********************************************
 * CHECK CalculateVelocityAtCurrentParameters *
 **********************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_CalculateVelocityAtCurrentParameters
{
  units::velocity::meters_per_second_t in_velocity;
  units::acceleration::meters_per_second_squared_t in_acceleration;
  units::time::second_t in_ttc;
  units::velocity::meters_per_second_t out_velocity;
};

class MentalCalculations_CalculateVelocityAtCurrentParameters : public ::testing::Test,
                                                                public ::testing::WithParamInterface<DataFor_CalculateVelocityAtCurrentParameters>
{
};

TEST_P(MentalCalculations_CalculateVelocityAtCurrentParameters, MentalCalculations_CheckFunction_CalculateVelocityAtCurrentParameters)
{
  // Create required classes for test
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_CalculateVelocityAtCurrentParameters data = GetParam();

  const AreaOfInterest aoi = AreaOfInterest::EGO_FRONT;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(aoi, _)).WillByDefault(Return(data.in_velocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration(aoi, _)).WillByDefault(Return(data.in_acceleration));

  const auto result = TEST_HELPER.mentalCalculations.CalculateVelocityAtCurrentParameters(aoi, data.in_ttc);
  ASSERT_EQ(data.out_velocity, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateVelocityAtCurrentParameters,
    testing::Values(
        DataFor_CalculateVelocityAtCurrentParameters{
            .in_velocity = 10._mps,
            .in_acceleration = 0._mps_sq,
            .in_ttc = 3._s,
            .out_velocity = 10._mps},  // Constant velocity
        DataFor_CalculateVelocityAtCurrentParameters{
            .in_velocity = 10._mps,
            .in_acceleration = 1._mps_sq,
            .in_ttc = 3._s,
            .out_velocity = 13._mps},  // Accelerating
        DataFor_CalculateVelocityAtCurrentParameters{
            .in_velocity = 10._mps,
            .in_acceleration = -2._mps_sq,
            .in_ttc = 3._s,
            .out_velocity = 4._mps},  // Decelerating
        DataFor_CalculateVelocityAtCurrentParameters{
            .in_velocity = 10._mps,
            .in_acceleration = -5._mps_sq,
            .in_ttc = 3._s,
            .out_velocity = 0._mps}));  // Stop at standstill

/********************************
 * CHECK GetRelativeNetDistance *
 ********************************/
class MentalCalculations_GetRelativeNetDistanceSideLaneTests : public ::testing::Test,
                                                               public ::testing::WithParamInterface<std::tuple<SurroundingLane, AreaOfInterest, AreaOfInterest>>
{
};

TEST_P(MentalCalculations_GetRelativeNetDistanceSideLaneTests, VehicleVisibleOnSide_ReturnZero)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  const auto param = GetParam();

  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};

  const bool input_vehicleVisibleOnSide{true};
  const auto expected_relativeNetDistance{0._m};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillOnce(Return(input_vehicleVisibleOnSide));

  ASSERT_EQ(TEST_HELPER.mentalCalculations.GetRelativeNetDistance(targetLane), expected_relativeNetDistance);
}

TEST_P(MentalCalculations_GetRelativeNetDistanceSideLaneTests, VehicleVisibleSideFront_ReturnRelativeNetDistanceToAoi)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  const auto param = GetParam();

  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};
  const AreaOfInterest sideFrontAoi{std::get<2>(param)};

  const bool input_vehicleVisibleOnSide{false};
  const bool input_vehicleVisibleSideFront{true};
  const auto relativeNetDist{10._m};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillOnce(Return(input_vehicleVisibleOnSide));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideFrontAoi, _)).WillOnce(Return(input_vehicleVisibleSideFront));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(sideFrontAoi)).WillOnce(Return(relativeNetDist));

  ASSERT_EQ(TEST_HELPER.mentalCalculations.GetRelativeNetDistance(targetLane), relativeNetDist);
}

TEST_P(MentalCalculations_GetRelativeNetDistanceSideLaneTests, NoVehicleVisible_ReturnInfinity)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  const auto param = GetParam();

  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};
  const AreaOfInterest sideFrontAoi{std::get<2>(param)};

  const bool input_vehicleVisibleOnSide{false};
  const bool input_vehicleVisibleSideFront{false};
  const auto expected_relativeNetDistance{ScmDefinitions::INF_DISTANCE};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillOnce(Return(input_vehicleVisibleOnSide));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideFrontAoi, _)).WillOnce(Return(input_vehicleVisibleSideFront));
  
  ASSERT_EQ(TEST_HELPER.mentalCalculations.GetRelativeNetDistance(targetLane).value(), expected_relativeNetDistance.value());
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetRelativeNetDistanceSideLaneTests,
    testing::Values(
        std::make_tuple(SurroundingLane::LEFT, AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_FRONT),
        std::make_tuple(SurroundingLane::RIGHT, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_FRONT)));

class MentalCalculations_GetRelativeNetDistanceEgoLaneTests : public ::testing::Test
{
};

TEST_F(MentalCalculations_GetRelativeNetDistanceEgoLaneTests, VehicleVisible_ReturnRelativeNetDistance)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  const SurroundingLane targetLane{SurroundingLane::EGO};
  const bool input_vehicleVisible{true};
  const auto expected_relativeNetDistance{10._m};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillOnce(Return(input_vehicleVisible));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillOnce(Return(expected_relativeNetDistance));

  ASSERT_EQ(TEST_HELPER.mentalCalculations.GetRelativeNetDistance(targetLane), expected_relativeNetDistance);
}

TEST_F(MentalCalculations_GetRelativeNetDistanceEgoLaneTests, NoVehicleVisible_ReturnInfinity)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  const SurroundingLane targetLane{SurroundingLane::EGO};
  const bool input_vehicleVisible{false};
  const auto expected_relativeNetDistance{ScmDefinitions::INF_DISTANCE};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillOnce(Return(input_vehicleVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(10._m));

  ASSERT_EQ(TEST_HELPER.mentalCalculations.GetRelativeNetDistance(targetLane).value(), expected_relativeNetDistance.value());
}

/*****************************************
 * CHECK InterpreteBrokenBoldLaneMarking *
 *****************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_InterpreteBrokenBoldLaneMarking
{
  Side side;
  int input_LaneIdForJunctionIngoing;
  scm::LaneMarking::Type result_LaneMarkingType;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_InterpreteBrokenBoldLaneMarking& obj)
  {
    return os
           << "| side (Side): " << static_cast<int>(obj.side);
  }
};

class MentalCalculations_InterpreteBrokenBoldLaneMarking : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_InterpreteBrokenBoldLaneMarking>
{
};

TEST_P(MentalCalculations_InterpreteBrokenBoldLaneMarking, MentalCalculations_CheckFunction_InterpreteBrokenBoldLaneMarking)
{
  // Create required classes for test
  DataFor_InterpreteBrokenBoldLaneMarking data = GetParam();
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set up test
  scm::LaneMarking::Entity laneMarking;
  laneMarking.type = scm::LaneMarking::Type::Broken;
  laneMarking.color = scm::LaneMarking::Color::White;
  laneMarking.relativeStartDistance = 0._m;
  laneMarking.width = .30_m;
  std::vector<scm::LaneMarking::Entity> laneMarkings{laneMarking};

  TrafficRuleInformationScmExtended trafficRuleInformation;
  trafficRuleInformation.Close().laneMarkingsLeft = laneMarkings;
  trafficRuleInformation.Close().laneMarkingsRight = laneMarkings;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficRuleInformation()).WillByDefault(Return(&trafficRuleInformation));
  TEST_HELPER.mentalCalculations.GetLaneMarkingAtDistance(0._m, data.side);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetClosestRelativeLaneIdForJunctionIngoing()).WillByDefault(Return(data.input_LaneIdForJunctionIngoing));

  // Call test
  const scm::LaneMarking::Entity result{TEST_HELPER.mentalCalculations.InterpreteBrokenBoldLaneMarking(0._m, data.side)};

  // Evaluate results
  ASSERT_EQ(result.type, data.result_LaneMarkingType);
  ASSERT_EQ(result.width, .15_m);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_InterpreteBrokenBoldLaneMarking,
    testing::Values(
        DataFor_InterpreteBrokenBoldLaneMarking{
            .side = Side::Right,
            .input_LaneIdForJunctionIngoing = -1,
            .result_LaneMarkingType = scm::LaneMarking::Type::Broken},
        DataFor_InterpreteBrokenBoldLaneMarking{
            .side = Side::Right,
            .input_LaneIdForJunctionIngoing = -999,
            .result_LaneMarkingType = scm::LaneMarking::Type::Solid},
        DataFor_InterpreteBrokenBoldLaneMarking{
            .side = Side::Left,
            .input_LaneIdForJunctionIngoing = 0,
            .result_LaneMarkingType = scm::LaneMarking::Type::Solid},
        DataFor_InterpreteBrokenBoldLaneMarking{
            .side = Side::Left,
            .input_LaneIdForJunctionIngoing = -999,
            .result_LaneMarkingType = scm::LaneMarking::Type::Broken}));

/*******************************************
 * CHECK DetermineUrgencyFactorForNextExit *
 *******************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_DetermineUrgencyFactorForNextExit
{
  units::length::meter_t input_DistanceToStartOfExit;
  units::length::meter_t input_DistanceToEndOfExit;
  int input_EgoLaneId;
  std::vector<int> input_LaneIdForJunction;
  LateralAction input_CurrentLateralAction;
  double result_Urgency;
};

class MentalCalculations_DetermineUrgencyFactorForNextExit : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_DetermineUrgencyFactorForNextExit>
{
};

TEST_P(MentalCalculations_DetermineUrgencyFactorForNextExit, MentalCalculations_CheckFunction_DetermineUrgencyFactorForNextExit)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_DetermineUrgencyFactorForNextExit data = GetParam();
  NiceMock<FakeLaneChangeDimension> laneChangeDimension;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;

  // Set up test

  GeometryInformationSCM geometryInformation{};
  geometryInformation.relativeLaneIdForJunctionIngoing = data.input_LaneIdForJunction;

  OwnVehicleInformationScmExtended ownVehicleInfo{};
  ownVehicleInfo.mainLaneId = data.input_EgoLaneId;
  ownVehicleInfo.lateralAction = data.input_CurrentLateralAction;

  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformation));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfNextExit()).WillByDefault(Return(data.input_DistanceToEndOfExit));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToStartOfNextExit()).WillByDefault(Return(data.input_DistanceToStartOfExit));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetClosestRelativeLaneIdForJunctionIngoing()).WillByDefault(Return(data.input_LaneIdForJunction.at(0)));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.input_CurrentLateralAction));
  ON_CALL(laneChangeDimension, EstimateLaneChangeLength()).WillByDefault(Return(60._m));
  // Call test
  const double result = TEST_HELPER.mentalCalculations.DetermineUrgencyFactorForNextExit(false, laneChangeDimension);

  // Evaluate results
  ASSERT_NEAR(result, data.result_Urgency, 0.01);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_DetermineUrgencyFactorForNextExit,
    testing::Values(
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 500._m,
            .input_DistanceToEndOfExit = 2000._m,
            .input_EgoLaneId = -3,
            .input_LaneIdForJunction = {-1},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .2},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 500._m,
            .input_DistanceToEndOfExit = 2000._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .6},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 400._m,
            .input_DistanceToEndOfExit = 500._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .6},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 400._m,
            .input_DistanceToEndOfExit = 700._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .6},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 300._m,
            .input_DistanceToEndOfExit = 600._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .6},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 200._m,
            .input_DistanceToEndOfExit = 500._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .86},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 100._m,
            .input_DistanceToEndOfExit = 400._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = 1.},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 200._m,
            .input_DistanceToEndOfExit = 500._m,
            .input_EgoLaneId = -2,
            .input_LaneIdForJunction = {-2},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .66},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = -50._m,
            .input_DistanceToEndOfExit = 800._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = 1.},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = -50._m,
            .input_DistanceToEndOfExit = 150._m,
            .input_EgoLaneId = -1,
            .input_LaneIdForJunction = {-3},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = 0.},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 500._m,
            .input_DistanceToEndOfExit = -50._m,
            .input_EgoLaneId = -3,
            .input_LaneIdForJunction = {-1},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = 0.},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 500._m,
            .input_DistanceToEndOfExit = 550._m,
            .input_EgoLaneId = -3,
            .input_LaneIdForJunction = {-1},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .result_Urgency = .2},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 5._m,
            .input_DistanceToEndOfExit = 55._m,
            .input_EgoLaneId = -3,
            .input_LaneIdForJunction = {-1},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = .98},  // Could be further improved to be "0." if module checks wether distanceToEndOfExit > lengthRequiredForLaneChange except if LANE_CHANGING_RIGHT
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 5._m,
            .input_DistanceToEndOfExit = 55._m,
            .input_EgoLaneId = -3,
            .input_LaneIdForJunction = {-1},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .result_Urgency = .98},
        DataFor_DetermineUrgencyFactorForNextExit{
            .input_DistanceToStartOfExit = 100._m,
            .input_DistanceToEndOfExit = 300._m,
            .input_EgoLaneId = -4,
            .input_LaneIdForJunction = {0},
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result_Urgency = 0.}));

/*********************************************
 * CHECK GetIntensityForSlowerLeadingVehicle *
 *********************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataForGetIntensityForSlowerLeadingVehicle
{
  AreaOfInterest input_Aoi;
  AreaOfInterest input_AoiFar;
  scm::common::VehicleClass input_VehicleTypeAoi;
  scm::common::VehicleClass input_VehicleTypeAoiFar;
  double input_Urgency;
  bool input_IsApproaching;
  bool input_IsVehicleVisible;
  double result_IntensitySlowerLeadingVehicle;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataForGetIntensityForSlowerLeadingVehicle& obj)
  {
    return os
           << "input_Aoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_Aoi)
           << " | input_AoiFar (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_AoiFar)
           << " | input_vehicleTypeAoi (scm::common::VehicleClass): " << static_cast<size_t>(obj.input_VehicleTypeAoi)
           << " | input_vehicleTypeAoiFar (scm::common::VehicleClass): " << static_cast<size_t>(obj.input_VehicleTypeAoiFar)
           << " | input_Urgency (double): " << obj.input_Urgency
           << " | input_IsApproaching (bool): " << obj.input_IsApproaching
           << " | input_IsVehicleVisible (bool): " << obj.input_IsVehicleVisible
           << " | result_IntensitySlowerLeadingVehicle (double): " << obj.result_IntensitySlowerLeadingVehicle;
  }
};

class MentalCalculations_GetIntensityForSlowerLeadingVehicle : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataForGetIntensityForSlowerLeadingVehicle>
{
};

TEST_P(MentalCalculations_GetIntensityForSlowerLeadingVehicle, MentalCalculations_CheckFunction_GetIntensityForSlowerLeadingVehicle)
{
  DataForGetIntensityForSlowerLeadingVehicle data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TestMentalCalculations3 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);
  // Set up test

  ON_CALL(fakeMentalModel, GetVehicleClassification(data.input_Aoi, _)).WillByDefault(Return(data.input_VehicleTypeAoi));
  ON_CALL(fakeMentalModel, GetVehicleClassification(data.input_AoiFar, _)).WillByDefault(Return(data.input_VehicleTypeAoiFar));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.input_IsVehicleVisible));
  ON_CALL(testMentalCalculations, IsApproaching(_, _)).WillByDefault(Return(data.input_IsApproaching));
  ON_CALL(testMentalCalculations, GetUrgencyBetweenMinAndEq(_, _)).WillByDefault(Return(data.input_Urgency));

  // Call test
  double result = testMentalCalculations.GetIntensityForSlowerLeadingVehicle(data.input_Aoi);

  // Evaluate results
  ASSERT_EQ(data.result_IntensitySlowerLeadingVehicle, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetIntensityForSlowerLeadingVehicle,
    testing::Values(
        // Intensity = 0 because vehicle is not approaching
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kMedium_car,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kMedium_car,
            .input_Urgency = 10.0,
            .input_IsApproaching = false,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 0.0},
        // Intensity = 0 because vehicle is not visible
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kMedium_car,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kMedium_car,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = false,
            .result_IntensitySlowerLeadingVehicle = 0.0},
        // Intensity without factor because VehicleTypes don't fit
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kOther,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kOther,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 10.0},
        // Intensity with factor 0.75 because Car after Car
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kMedium_car,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kMedium_car,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 7.5},
        // Intensity with factor 1.0 because Car after Truck
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kMedium_car,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kHeavy_truck,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 10.0},
        // Intensity with factor 1.0 because Truck after Car
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kHeavy_truck,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kMedium_car,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 0.0},
        // Intensity with factor 0.5 because Truck after Truck
        DataForGetIntensityForSlowerLeadingVehicle{
            .input_Aoi = AreaOfInterest::LEFT_FRONT,
            .input_AoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_VehicleTypeAoi = scm::common::VehicleClass::kHeavy_truck,
            .input_VehicleTypeAoiFar = scm::common::VehicleClass::kHeavy_truck,
            .input_Urgency = 10.0,
            .input_IsApproaching = true,
            .input_IsVehicleVisible = true,
            .result_IntensitySlowerLeadingVehicle = 5.0}));

/********************************
 * CHECK CalculateLateralOffset *
 ********************************/

/// \brief Data table for definition of individual test cases for CalculateLateralOffset
struct DataForCalculateLateralOffset
{
  MesoscopicSituation input_CurrentSituation;
  units::velocity::meters_per_second_t input_VEgo;
  units::velocity::meters_per_second_t input_JamVelocity;
  units::velocity::meters_per_second_t input_RescueLaneVelocity;
  units::length::meter_t input_LateralOffset;
  units::length::meter_t input_LateralOffsetRescue;
  bool input_DriveToLeftSide;
  bool input_persistentCooperativeBehavior;
  units::length::meter_t input_LaneWidth;
  units::length::meter_t input_VehicleWidth;
  double result_lateralOffsetScaled;
};

class MentalCalculations_CalculateLateralOffset : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataForCalculateLateralOffset>
{
};

TEST_P(MentalCalculations_CalculateLateralOffset, MentalCalculations_CheckFunction_CalculateLateralOffset)
{
  // Create required classes for test
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  DataForCalculateLateralOffset data = GetParam();

  // Set up test suite

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;

  GeometryInformationSCM geometryInformationSCM{};
  geometryInformationSCM.Close().width = data.input_LaneWidth;

  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformationSCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(!data.input_DriveToLeftSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_DriveToLeftSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.input_VEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleWidth()).WillByDefault(Return(data.input_VehicleWidth));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(data.input_JamVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetFormRescueLaneVelocityThreshold()).WillByDefault(Return(data.input_RescueLaneVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralOffsetNeutralPosition()).WillByDefault(Return(data.input_LateralOffset));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralOffsetNeutralPositionRescueLane()).WillByDefault(Return(data.input_LateralOffsetRescue));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.input_CurrentSituation)).WillByDefault(Return(true));

  TEST_HELPER.mentalCalculations.SET_PERSITENT_COOPERATIVE_BEHAVIOR(data.input_persistentCooperativeBehavior);

  // Call test
  auto result = TEST_HELPER.mentalCalculations.CalculateLateralOffsetNeutralPositionScaled();

  // Evaluate results
  EXPECT_NEAR(data.result_lateralOffsetScaled, result.value(), 1e-3);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateLateralOffset,
    testing::Values(
        // Tests for QUEUED_TRAFFIC velocity
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::FREE_DRIVING,
            .input_VEgo = units::velocity::meters_per_second_t(90_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        // Tests for ego velocity
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(90_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(50_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.25},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.625},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.75},
        // No Rescue Lane according to cooperative behavior
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = false,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = false,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        // Tests for direction of offset
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.125},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.75},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.125},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.75},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.625},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.75},
        // tests for changing lateraloffsetrescuelane to lateraloffset
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.3_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = -0.5_m,
            .input_LateralOffsetRescue = 0.3_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.3_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = 0.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.3_m,
            .input_DriveToLeftSide = false,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 4.0_m,
            .input_VehicleWidth = 2.0_m,
            .result_lateralOffsetScaled = -0.5},
        // Tests for different lane/vehicle width
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(90_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 8.0_m,
            .input_VehicleWidth = 4.0_m,
            .result_lateralOffsetScaled = 0.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(50_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 8.0_m,
            .input_VehicleWidth = 4.0_m,
            .result_lateralOffsetScaled = 0.5},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(30_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 8.0_m,
            .input_VehicleWidth = 4.0_m,
            .result_lateralOffsetScaled = 1.0},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(15_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 8.0_m,
            .input_VehicleWidth = 4.0_m,
            .result_lateralOffsetScaled = 1.25},
        DataForCalculateLateralOffset{
            .input_CurrentSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .input_VEgo = units::velocity::meters_per_second_t(5_kph),
            .input_JamVelocity = units::velocity::meters_per_second_t(60_kph),
            .input_RescueLaneVelocity = units::velocity::meters_per_second_t(10_kph),
            .input_LateralOffset = 0.5_m,
            .input_LateralOffsetRescue = 0.75_m,
            .input_DriveToLeftSide = true,
            .input_persistentCooperativeBehavior = true,
            .input_LaneWidth = 8.0_m,
            .input_VehicleWidth = 4.0_m,
            .result_lateralOffsetScaled = 1.5}));

/********************************************
 * CHECK CheckVelocityDifferenceIsPlatoon *
 ********************************************/

/// \brief Data table
struct TestData_CheckVelocityDifferenceIsPlatoon
{
  Side input_sideToCheck;
  units::velocity::meters_per_second_t input_velocityLeftFront;
  units::velocity::meters_per_second_t input_velocityLeftFrontFar;
  units::velocity::meters_per_second_t input_velocityRightFront;
  units::velocity::meters_per_second_t input_velocityRightFrontFar;
  bool result_isPlatoon;
};

class MentalCalculations_CheckVelocityDifferenceIsPlatoon : public ::testing::Test,
                                                            public ::testing::WithParamInterface<TestData_CheckVelocityDifferenceIsPlatoon>
{
};

TEST_P(MentalCalculations_CheckVelocityDifferenceIsPlatoon, MentalCalculations_CheckFunction_CheckVelocityDifferenceIsPlatoon)
{
  // Get resources
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  TestData_CheckVelocityDifferenceIsPlatoon param = GetParam();

  // Set data
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(param.input_velocityLeftFront));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT_FAR, _)).WillByDefault(Return(param.input_velocityLeftFrontFar));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(param.input_velocityRightFront));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT_FAR, _)).WillByDefault(Return(param.input_velocityRightFrontFar));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.CheckVelocityDifferenceIsPlatoon(param.input_sideToCheck);

  // Evaluate results
  ASSERT_EQ(result, param.result_isPlatoon);
}

namespace
{
constexpr units::velocity::meters_per_second_t PLATOON_MAX_VELOCITY_DIFFERENCE = 3.0_mps;
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CheckVelocityDifferenceIsPlatoon,
    testing::Values(
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Left,
            .input_velocityLeftFront = 60._mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .input_velocityLeftFrontFar = 60._mps,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 0._mps,
            .result_isPlatoon = true},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Left,
            .input_velocityLeftFront = 60.1_mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .input_velocityLeftFrontFar = 60_mps,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 0._mps,
            .result_isPlatoon = false},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Left,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 60._mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 0._mps,
            .result_isPlatoon = true},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Left,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 0._mps,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 60._mps,
            .result_isPlatoon = false},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Right,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 60._mps,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 0._mps,
            .result_isPlatoon = false},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Right,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 0._mps,
            .input_velocityRightFront = 60._mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .input_velocityRightFrontFar = 60._mps,
            .result_isPlatoon = true},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Right,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 0._mps,
            .input_velocityRightFront = 60.1_mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .input_velocityRightFrontFar = 60._mps,
            .result_isPlatoon = false},
        TestData_CheckVelocityDifferenceIsPlatoon{
            .input_sideToCheck = Side::Right,
            .input_velocityLeftFront = 60._mps,
            .input_velocityLeftFrontFar = 0._mps,
            .input_velocityRightFront = 60._mps,
            .input_velocityRightFrontFar = 60._mps + PLATOON_MAX_VELOCITY_DIFFERENCE,
            .result_isPlatoon = true}));

/****************************************
 * CHECK CheckRelativeDistanceIsPlatoon *
 ****************************************/

/// \brief Data table
struct TestData_CheckRelativeDistanceIsPlatoon
{
  units::length::meter_t input_relativeDistanceFront;
  units::length::meter_t input_relativeDistanceFrontFar;
  units::length::meter_t input_vehicleLengthFront;
  units::velocity::meters_per_second_t input_velocity;
  bool result_isPlatoon;
};

class MentalCalculations_CheckRelativeDistanceIsPlatoon : public ::testing::Test,
                                                          public ::testing::WithParamInterface<TestData_CheckRelativeDistanceIsPlatoon>
{
};

TEST_P(MentalCalculations_CheckRelativeDistanceIsPlatoon, MentalCalculations_CheckFunction_CheckRelativeDistanceIsPlatoonOnRight)
{
  // Get resources
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  TestData_CheckRelativeDistanceIsPlatoon param = GetParam();

  // Set data
  constexpr double DC = 99.9;
  constexpr AreaOfInterest Aoi_Front = AreaOfInterest::RIGHT_FRONT;
  constexpr AreaOfInterest Aoi_FrontFar = AreaOfInterest::RIGHT_FRONT_FAR;
  constexpr Side Side = Side::Right;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(units::make_unit<units::length::meter_t>(DC)));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(Aoi_Front)).WillByDefault(Return(param.input_relativeDistanceFront));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(Aoi_FrontFar)).WillByDefault(Return(param.input_relativeDistanceFrontFar));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength(_, -1)).WillByDefault(Return(0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength(_, _)).WillByDefault(Return(param.input_vehicleLengthFront));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(Aoi_Front, _)).WillByDefault(Return(units::make_unit<units::velocity::meters_per_second_t>(DC)));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(param.input_velocity));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.CheckRelativeDistanceIsPlatoon(Side);

  // Evaluate results
  ASSERT_EQ(result, param.result_isPlatoon);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CheckRelativeDistanceIsPlatoon,
    testing::Values(
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1._m,
            .input_relativeDistanceFrontFar = 2._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 2.0_mps,
            .result_isPlatoon = true},
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1._m,
            .input_relativeDistanceFrontFar = 2._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 2.0_mps,
            .result_isPlatoon = true},
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1.5_m,
            .input_relativeDistanceFrontFar = 10._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 2.0_mps,
            .result_isPlatoon = false},
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1.5_m,
            .input_relativeDistanceFrontFar = 10._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 2.0_mps,
            .result_isPlatoon = false},
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1.5_m,
            .input_relativeDistanceFrontFar = 3._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 1.0_mps,
            .result_isPlatoon = false},
        TestData_CheckRelativeDistanceIsPlatoon{
            .input_relativeDistanceFront = 1.5_m,
            .input_relativeDistanceFrontFar = 3._m,
            .input_vehicleLengthFront = 0.5_m,
            .input_velocity = 1.0_mps,
            .result_isPlatoon = false}));

/***************************************
 * CHECK AnticipatedLaneChangerPresent *
 ***************************************/

/// \brief Data table
struct TestData_AnticipatedLaneChangerPresent
{
  Side input_sideToCheck;
  bool input_isAnticipating;
  Situation input_currentSituation;
  Risk input_situationRisk;
  bool input_situationAnticipated;
  bool result_isPlatoon;

  // clang-format off
    /// \brief This stream will be shown in case the test fails
    friend std::ostream& operator<<(std::ostream& os, const TestData_AnticipatedLaneChangerPresent& obj)
    {
        return os
                << " | input_sideToCheck (Side): " << ((obj.input_sideToCheck ==  Side::Left) ? "Left" : "Right")
                << " | input_isAnticipating (bool): " << ScmCommons::BooleanToString(obj.input_isAnticipating)
                << " | input_currentSituation (Situation): " << ScmCommons::SituationToString(obj.input_currentSituation)
                << " | situationRisk (Risk): " << static_cast<int>(obj.input_situationRisk)
                << " | input_situationAnticipated (bool): " << ScmCommons::BooleanToString(obj.input_situationAnticipated)
                << " | result_isPlatoon (bool): " << ScmCommons::BooleanToString(obj.result_isPlatoon);

    }
  // clang-format on
};

class MentalCalculations_AnticipatedLaneChangerPresent : public ::testing::Test,
                                                         public ::testing::WithParamInterface<TestData_AnticipatedLaneChangerPresent>
{
};

TEST_P(MentalCalculations_AnticipatedLaneChangerPresent, MentalCalculations_CheckFunction_AnticipatedLaneChangerPresent)
{
  // Get resources
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  TestData_AnticipatedLaneChangerPresent param = GetParam();
  // Set data
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsAnticipating()).WillByDefault(Return(param.input_isAnticipating));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(param.input_currentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetSituationRisk(param.input_currentSituation)).WillByDefault(Return(param.input_situationRisk));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSituationAnticipated(param.input_currentSituation)).WillByDefault(Return(param.input_situationAnticipated));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.AnticipatedLaneChangerPresent(param.input_sideToCheck);

  // Evaluate results
  ASSERT_EQ(result, param.result_isPlatoon);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_AnticipatedLaneChangerPresent,
    testing::Values(
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Left,
            .input_isAnticipating = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_situationRisk = Risk::LOW,
            .input_situationAnticipated = true,
            .result_isPlatoon = true},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Left,
            .input_isAnticipating = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_situationRisk = Risk::HIGH,
            .input_situationAnticipated = true,
            .result_isPlatoon = false},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Left,
            .input_isAnticipating = false,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_situationRisk = Risk::MEDIUM,
            .input_situationAnticipated = true,
            .result_isPlatoon = false},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Right,
            .input_isAnticipating = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_situationRisk = Risk::MEDIUM,
            .input_situationAnticipated = true,
            .result_isPlatoon = false},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Right,
            .input_isAnticipating = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .input_situationRisk = Risk::MEDIUM,
            .input_situationAnticipated = true,
            .result_isPlatoon = true},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Right,
            .input_isAnticipating = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .input_situationRisk = Risk::LOW,
            .input_situationAnticipated = false,
            .result_isPlatoon = false},
        TestData_AnticipatedLaneChangerPresent{
            .input_sideToCheck = Side::Right,
            .input_isAnticipating = false,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .input_situationRisk = Risk::LOW,
            .input_situationAnticipated = true,
            .result_isPlatoon = false}));

/******************************
 * CHECK PlatoonBelowJamSpeed *
 ******************************/

TEST(MentalCalculations_PlatoonBelowJamSpeed, MentalCalculations_CheckFunction_PlatoonBelowJamSpeed_LeftFrontBelowJamSpeed)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr units::velocity::meters_per_second_t JamSpeed = 10._mps;
  constexpr auto AoiSpeed = 5._mps;
  constexpr Side Side = Side::Left;
  constexpr AreaOfInterest Aoi = AreaOfInterest::LEFT_FRONT;
  constexpr bool Expected = true;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(Aoi, _)).WillByDefault(Return(AoiSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));
  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PlatoonBelowJamSpeed(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

TEST(MentalCalculations_PlatoonBelowJamSpeed, MentalCalculations_CheckFunction_PlatoonBelowJamSpeed_LeftFrontEqualJamSpeed)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr auto DC = 99._mps;
  constexpr auto JamSpeed = 10._mps;
  constexpr auto AoiSpeed = 10._mps;
  constexpr Side Side = Side::Left;
  constexpr AreaOfInterest Aoi = AreaOfInterest::LEFT_FRONT;
  constexpr bool Expected = false;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(DC));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(Aoi, _)).WillByDefault(Return(AoiSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PlatoonBelowJamSpeed(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

TEST(MentalCalculations_PlatoonBelowJamSpeed, MentalCalculations_CheckFunction_PlatoonBelowJamSpeed_RightFrontBelowJamSpeed)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr auto DC = 99._mps;
  constexpr units::velocity::meters_per_second_t JamSpeed = 10._mps;
  constexpr auto AoiSpeed = 10._mps;
  constexpr Side Side = Side::Right;
  constexpr AreaOfInterest Aoi = AreaOfInterest::RIGHT_FRONT;
  constexpr bool Expected = false;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(DC));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(Aoi, _)).WillByDefault(Return(AoiSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PlatoonBelowJamSpeed(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

/************************************
 * CHECK PossiblePlatoonCarsVisible *
 ************************************/

TEST(MentalCalculations_PossiblePlatoonCarsVisible, MentalCalculations_CheckFunction_PossiblePlatoonCarsVisibleLeft)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr Side Side = Side::Left;
  constexpr AreaOfInterest AoiFar = AreaOfInterest::LEFT_FRONT_FAR;
  constexpr AreaOfInterest Aoi = AreaOfInterest::LEFT_FRONT;
  constexpr bool Expected = true;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AoiFar, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(Aoi, _)).WillByDefault(Return(true));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PossiblePlatoonCarsVisible(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

TEST(MentalCalculations_PossiblePlatoonCarsVisible, MentalCalculations_CheckFunction_PossiblePlatoonCarsVisibleRight)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr Side Side = Side::Right;
  constexpr AreaOfInterest AoiFar = AreaOfInterest::RIGHT_FRONT_FAR;
  constexpr AreaOfInterest Aoi = AreaOfInterest::RIGHT_FRONT;
  constexpr bool Expected = true;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AoiFar, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(Aoi, _)).WillByDefault(Return(true));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PossiblePlatoonCarsVisible(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

TEST(MentalCalculations_PossiblePlatoonCarsVisible, MentalCalculations_CheckFunction_PossiblePlatoonCarsVisibleOnlyFarVisible)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr Side Side = Side::Right;
  constexpr AreaOfInterest AoiFar = AreaOfInterest::RIGHT_FRONT_FAR;
  constexpr AreaOfInterest Aoi = AreaOfInterest::RIGHT_FRONT;
  constexpr bool Expected = false;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AoiFar, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(Aoi, _)).WillByDefault(Return(false));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PossiblePlatoonCarsVisible(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

TEST(MentalCalculations_PossiblePlatoonCarsVisible, MentalCalculations_CheckFunction_PossiblePlatoonCarsVisibleOnlyNearVisible)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr Side Side = Side::Right;
  constexpr AreaOfInterest AoiFar = AreaOfInterest::RIGHT_FRONT_FAR;
  constexpr AreaOfInterest Aoi = AreaOfInterest::RIGHT_FRONT;
  constexpr bool Expected = false;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AoiFar, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(Aoi, _)).WillByDefault(Return(true));

  // Call test
  const bool result = TEST_HELPER.mentalCalculations.PossiblePlatoonCarsVisible(Side);

  // Evaluate results
  ASSERT_EQ(result, Expected);
}

/***********************************
 * CHECK VReasonPassingSlowPlatoon *
 ***********************************/

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonEgoBelowJamSpeed)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  // Set data
  constexpr auto Expected = ScmDefinitions::INF_DISTANCE;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(5.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(6.0_mps));

  // Call test
  const auto result = TEST_HELPER.mentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_TRUE(units::math::isinf(result));
}

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonNoPassablePlatoonPresent)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set data
  constexpr bool EgoBelowJamSpeed = false;
  constexpr bool PlatoonPresentLeft = false;
  constexpr bool PlatoonPresentRight = false;
  constexpr auto Expected = ScmDefinitions::INF_VELOCITY;

  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Left)).WillByDefault(Return(PlatoonPresentLeft));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Right)).WillByDefault(Return(PlatoonPresentRight));

  // Call test
  const auto result = testMentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_TRUE(units::math::isinf(result));
}

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonPlatoonPresentRight)
{
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Define parameters
  constexpr AreaOfInterest AoiFront = AreaOfInterest::RIGHT_FRONT;
  constexpr AreaOfInterest AoiFrontFar = AreaOfInterest::RIGHT_FRONT_FAR;

  constexpr bool EgoBelowJamSpeed = false;
  constexpr bool PlatoonPresentLeft = false;
  constexpr bool PlatoonPresentRight = true;

  constexpr auto VelocitySideFront = 10._mps;
  constexpr auto VelocitySideFrontFar = 5._mps;

  constexpr auto JamSpeed = 1._mps;

  constexpr double SuperelevationFactorLeft = 5.;
  constexpr double SuperelevationFactorRight = 4.;

  constexpr auto VLimitLeft = 10._mps;
  constexpr auto VLimitRight = 20._mps;

  // Calculate expected Result
  const auto VMin = units::math::min(VelocitySideFront, VelocitySideFrontFar);
  const units::velocity::meters_per_second_t Result = SuperelevationFactorRight * VMin;

  // Set data
  ON_CALL(fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(ScmDefinitions::INF_VELOCITY));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFront, _)).WillByDefault(Return(VelocitySideFront));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFrontFar, _)).WillByDefault(Return(VelocitySideFrontFar));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Left)).WillByDefault(Return(PlatoonPresentLeft));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Right)).WillByDefault(Return(PlatoonPresentRight));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingVelocityLimits(_)).WillByDefault(Return(std::make_tuple(VLimitLeft, VLimitRight)));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingSuperevelationFactors(_)).WillByDefault(Return(std::make_tuple(SuperelevationFactorLeft, SuperelevationFactorRight)));

  // Call test
  const auto result = testMentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_EQ(result, Result);
}

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonPlatoonFasterThanPassingLimit)
{
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Define parameters
  constexpr AreaOfInterest AoiFront = AreaOfInterest::RIGHT_FRONT;
  constexpr AreaOfInterest AoiFrontFar = AreaOfInterest::RIGHT_FRONT_FAR;

  constexpr bool EgoBelowJamSpeed = false;
  constexpr bool PlatoonPresentLeft = false;
  constexpr bool PlatoonPresentRight = true;

  constexpr auto VelocitySideFront = 20._mps;
  constexpr auto VelocitySideFrontFar = 10._mps;

  constexpr units::velocity::meters_per_second_t JamSpeed = 1._mps;

  constexpr double SuperelevationFactorLeft = 5.;
  constexpr double SuperelevationFactorRight = 4.;

  constexpr auto VLimitLeft = 10._mps;
  constexpr auto VLimitRight = 5._mps;

  // Calculate expected Result
  constexpr auto Result = ScmDefinitions::INF_VELOCITY;

  // Set data
  ON_CALL(fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(ScmDefinitions::INF_VELOCITY));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFront, _)).WillByDefault(Return(VelocitySideFront));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFrontFar, _)).WillByDefault(Return(VelocitySideFrontFar));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Left)).WillByDefault(Return(PlatoonPresentLeft));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Right)).WillByDefault(Return(PlatoonPresentRight));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingVelocityLimits(_)).WillByDefault(Return(std::make_tuple(VLimitLeft, VLimitRight)));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingSuperevelationFactors(_)).WillByDefault(Return(std::make_tuple(SuperelevationFactorLeft, SuperelevationFactorRight)));

  // Call test
  const auto result = testMentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_TRUE(units::math::isinf(result));
}

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonPlatoonSlowerThanJamVelocity)
{
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Define parameters
  constexpr AreaOfInterest AoiFront = AreaOfInterest::RIGHT_FRONT;
  constexpr AreaOfInterest AoiFrontFar = AreaOfInterest::RIGHT_FRONT_FAR;

  constexpr bool EgoBelowJamSpeed = false;
  constexpr bool PlatoonPresentLeft = false;
  constexpr bool PlatoonPresentRight = true;

  constexpr auto VelocitySideFront = 10._mps;
  constexpr auto VelocitySideFrontFar = 20._mps;

  constexpr double SuperelevationFactorLeft = 5.;
  constexpr double SuperelevationFactorRight = 4.;

  constexpr auto VLimitLeft = 10._mps;
  constexpr auto VLimitRight = 20._mps;

  // Calculate expected Result
  constexpr auto JamSpeed = 10._mps + (SuperelevationFactorRight * VelocitySideFrontFar);
  constexpr units::velocity::meters_per_second_t Result = JamSpeed;

  // Set data
  ON_CALL(fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(ScmDefinitions::INF_VELOCITY));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFront, _)).WillByDefault(Return(VelocitySideFront));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AoiFrontFar, _)).WillByDefault(Return(VelocitySideFrontFar));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Left)).WillByDefault(Return(PlatoonPresentLeft));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Right)).WillByDefault(Return(PlatoonPresentRight));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingVelocityLimits(_)).WillByDefault(Return(std::make_tuple(VLimitLeft, VLimitRight)));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingSuperevelationFactors(_)).WillByDefault(Return(std::make_tuple(SuperelevationFactorLeft, SuperelevationFactorRight)));

  // Call test
  const auto result = testMentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_THAT(result, Result);
}

TEST(MentalCalculations_VReasonPassingSlowPlatoon, MentalCalculations_CheckFunction_VReasonPassingSlowPlatoonLeftPlatoonSlowerThanRight)
{
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Define parameters
  constexpr bool EgoBelowJamSpeed = false;
  constexpr bool PlatoonPresentLeft = true;
  constexpr bool PlatoonPresentRight = true;

  constexpr units::velocity::meters_per_second_t JamSpeed = 5._mps;
  constexpr auto VelocityFrontLeft = 10._mps;
  constexpr auto VelocityFrontLeftFar = 20._mps;
  constexpr auto VelocityFrontRight = 20._mps;
  constexpr auto VelocityFrontRightFar = 30._mps;

  constexpr double SuperelevationFactorLeft = 5.;
  constexpr double SuperelevationFactorRight = 4.;

  constexpr auto VLimitLeft = 10._mps;
  constexpr auto VLimitRight = 20._mps;

  // Calculate expected Result
  constexpr auto Result = SuperelevationFactorLeft * VelocityFrontLeft;

  // Set data
  ON_CALL(fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(JamSpeed));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(ScmDefinitions::INF_VELOCITY));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(VelocityFrontLeft));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT_FAR, _)).WillByDefault(Return(VelocityFrontLeftFar));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(VelocityFrontRight));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT_FAR, _)).WillByDefault(Return(VelocityFrontRightFar));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(EgoBelowJamSpeed));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Left)).WillByDefault(Return(PlatoonPresentLeft));
  ON_CALL(testMentalCalculations, PassablePlatoonPresent(Side::Right)).WillByDefault(Return(PlatoonPresentRight));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingVelocityLimits(_)).WillByDefault(Return(std::make_tuple(VLimitLeft, VLimitRight)));
  ON_CALL(testMentalCalculations, CalculatePlatoonPassingSuperevelationFactors(_)).WillByDefault(Return(std::make_tuple(SuperelevationFactorLeft, SuperelevationFactorRight)));

  // Call test
  const auto result = testMentalCalculations.VReasonPassingSlowPlatoon();

  // Evaluate results
  ASSERT_THAT(result, Result);
}

/***********************
 * CHECK IsApproaching *
 ***********************/

/// \brief Data table for definition of individual test cases
struct DataFor_IsApproaching
{
  units::velocity::meters_per_second_t input_ObservedVelocity;
  units::velocity::meters_per_second_t input_FrontObservedVelocity;
  bool expected_Result;
};

class FeatureExtractor_IsApproaching : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsApproaching>
{
};

TEST_P(FeatureExtractor_IsApproaching, FeatureExtractor_CheckFunction_IsApproaching)
{
  // Get resources for testing
  DataFor_IsApproaching data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  TestMentalCalculations testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(data.input_ObservedVelocity));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT_FAR, _)).WillByDefault(Return(data.input_FrontObservedVelocity));

  // Run Test
  bool result = testMentalCalculations.IsApproaching(AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR);

  // Evaluate results
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsApproaching,
    testing::Values(
        DataFor_IsApproaching{
            .input_ObservedVelocity = 20._mps,
            .input_FrontObservedVelocity = 18._mps,
            .expected_Result = true},
        DataFor_IsApproaching{
            .input_ObservedVelocity = 20._mps,
            .input_FrontObservedVelocity = 20._mps,
            .expected_Result = false},
        DataFor_IsApproaching{
            .input_ObservedVelocity = 20._mps,
            .input_FrontObservedVelocity = 23._mps,
            .expected_Result = false}));

/********************************
 * CHECK CalculateUrgencyFactor *
 ********************************/

/// \brief Data table for definition of individual test cases for CalculateUrgencyFactor
struct DataFor_CalculateUrgencyFactor
{
  double input_riskObstacle;
  double inout_riskEndOfLane;

  units::length::meter_t mock_distanceToEndOfLane;
  units::length::meter_t mock_influencingDistanceEndOfLane;

  double expected_riskEndOfLane;
  double expected_urgencyFactor;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CalculateUrgencyFactor& obj)
  {
    return os
           << "input_riskObstacle (double): " << obj.input_riskObstacle
           << " | inout_riskEndOfLane (double): " << obj.inout_riskEndOfLane
           << " | mock_distanceToEndOfLane (double): " << obj.mock_distanceToEndOfLane
           << " | mock_influencingDistanceEndOfLane (double): " << obj.mock_influencingDistanceEndOfLane
           << " | expected_riskEndOfLane (double): " << obj.expected_riskEndOfLane
           << " | expected_urgencyFactor (double): " << obj.expected_urgencyFactor;
  }
};

class MentalCalculations_CalculateUrgencyFactor : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_CalculateUrgencyFactor>
{
};

TEST_P(MentalCalculations_CalculateUrgencyFactor, MentalCalculations_CheckFunction_CalculateUrgencyFactor)
{
  // Create required classes for test
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_CalculateUrgencyFactor data = GetParam();

  // Set up test
  const units::velocity::meters_per_second_t vEgo = 10._mps;
  const units::acceleration::meters_per_second_squared_t aComf = 5.5_mps_sq;

  GeometryInformationSCM geometryInformationSCM{};
  geometryInformationSCM.Close().distanceToEndOfLane = data.mock_distanceToEndOfLane;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformationSCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(data.mock_influencingDistanceEndOfLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(vEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(aComf));

  // Call test
  double actual_urgencyFactor = TEST_HELPER.mentalCalculations.CalculateUrgencyFactor(data.input_riskObstacle, data.inout_riskEndOfLane);

  // Evaluate results
  ASSERT_DOUBLE_EQ(data.expected_urgencyFactor, actual_urgencyFactor);
  ASSERT_DOUBLE_EQ(data.expected_riskEndOfLane, data.inout_riskEndOfLane);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateUrgencyFactor,
    testing::Values(
        DataFor_CalculateUrgencyFactor{
            .input_riskObstacle = 0.1,
            .inout_riskEndOfLane = 0.2,
            .mock_distanceToEndOfLane = 4._m,
            .mock_influencingDistanceEndOfLane = 3._m,
            .expected_riskEndOfLane = 0.2,
            .expected_urgencyFactor = 0.2},
        DataFor_CalculateUrgencyFactor{
            .input_riskObstacle = 0.1,
            .inout_riskEndOfLane = 0.2,
            .mock_distanceToEndOfLane = 3._m,
            .mock_influencingDistanceEndOfLane = 4._m,
            .expected_riskEndOfLane = (4. - 3.) / (4. - 3. + (3. - (10. * 10. / 2. / 5.5))),
            .expected_urgencyFactor = 0.1}));

/****************************************************
 * CHECK CalculateUrgencyFactorSituationNoCollision *
 ***************************************************/

/// \brief Data table for definition of individual test cases for CalculateUrgencyFactorSituationNoCollision
struct DataFor_CalculateUrgencyFactorSituationNoCollision
{
  double input_riskObstacle;
  double inout_riskEndOfLane;
  bool input_isCurrentSituationAndNotHighRisk;
  Situation input_currentSituation;
  double expected_urgencyFactor;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CalculateUrgencyFactorSituationNoCollision& obj)
  {
    return os
           << "input_riskObstacle (double): " << obj.input_riskObstacle
           << " | inout_riskEndOfLane (double): " << obj.inout_riskEndOfLane
           << " | input_isCurrentSituationAndNotHighRisk (bool): " << ScmCommons::BooleanToString(obj.input_isCurrentSituationAndNotHighRisk)
           << " | input_currentSituation (Situation): " << ScmCommons::SituationToString(obj.input_currentSituation)
           << " | expected_urgencyFactor (double): " << obj.expected_urgencyFactor;
  }
};

class MentalCalculations_CalculateUrgencyFactorSituationNoCollision : public ::testing::Test,
                                                                      public ::testing::WithParamInterface<DataFor_CalculateUrgencyFactorSituationNoCollision>
{
};

TEST_P(MentalCalculations_CalculateUrgencyFactorSituationNoCollision, MentalCalculations_CheckFunction_CalculateUrgencyFactorSituationNoCollision)
{
  // Create required classes for test
  DataFor_CalculateUrgencyFactorSituationNoCollision data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  TestMentalCalculations4 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up test
  const auto distanceToObstacle = 30._m;
  const auto distanceToObstacleFar = 60._m;
  const auto distanceToEndOfLane = 90._m;

  GeometryInformationSCM geometryInformationSCM{};
  geometryInformationSCM.Close().distanceToEndOfLane = distanceToEndOfLane;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformationSCM));
  ON_CALL(fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(testMentalCalculations, IsCurrentSituationAndNotHighRisk(data.input_currentSituation)).WillByDefault(Return(data.input_isCurrentSituationAndNotHighRisk));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT)).WillByDefault(Return(1._m));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT_FAR)).WillByDefault(Return(2._m));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT)).WillByDefault(Return(3._m));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT_FAR)).WillByDefault(Return(4._m));

  ON_CALL(testMentalCalculations, CalculateRiskObstacle(_, _, distanceToObstacle, distanceToObstacleFar, data.input_riskObstacle)).WillByDefault(Return(data.input_riskObstacle));

  // Call test
  double actual_urgencyFactor = testMentalCalculations.TestCalculateUrgencyFactorSituationNoCollision(distanceToObstacle, distanceToObstacleFar, data.input_riskObstacle, data.inout_riskEndOfLane);

  // Evaluate results
  ASSERT_DOUBLE_EQ(data.expected_urgencyFactor, actual_urgencyFactor);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateUrgencyFactorSituationNoCollision,
    testing::Values(
        DataFor_CalculateUrgencyFactorSituationNoCollision{
            .input_riskObstacle = 0.1,
            .inout_riskEndOfLane = 0.2,
            .input_isCurrentSituationAndNotHighRisk = true,
            .input_currentSituation = Situation::FOLLOWING_DRIVING,
            .expected_urgencyFactor = 0.},
        DataFor_CalculateUrgencyFactorSituationNoCollision{
            .input_riskObstacle = 0.1,
            .inout_riskEndOfLane = 0.2,
            .input_isCurrentSituationAndNotHighRisk = true,
            .input_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .expected_urgencyFactor = 0.},
        DataFor_CalculateUrgencyFactorSituationNoCollision{
            .input_riskObstacle = 0.1,
            .inout_riskEndOfLane = 0.2,
            .input_isCurrentSituationAndNotHighRisk = false,
            .input_currentSituation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .expected_urgencyFactor = 0.2},
        DataFor_CalculateUrgencyFactorSituationNoCollision{
            .input_riskObstacle = 0.3,
            .inout_riskEndOfLane = 0.2,
            .input_isCurrentSituationAndNotHighRisk = false,
            .input_currentSituation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .expected_urgencyFactor = 0.3}));

/*****************************************
 * CHECK CalculateUrgencyFactorSituation *
 ****************************************/

/// \brief Data table for definition of individual test cases for CalculateUrgencyFactorSituation
struct DataFor_CalculateUrgencyFactorSituation
{
  Situation mock_currentSituation;
  units::time::second_t input_ttc;
  double expected_urgencyFactor;
};

class MentalCalculations_CalculateUrgencyFactorSituation : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_CalculateUrgencyFactorSituation>
{
};

TEST_P(MentalCalculations_CalculateUrgencyFactorSituation, MentalCalculations_CheckFunction_CalculateUrgencyFactorSituation)
{
  // Create required classes for test
  DataFor_CalculateUrgencyFactorSituation data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  TestMentalCalculations7 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up test
  const double riskObstacle = 0.1;
  const double riskEndOfLane = 0.3;
  const auto distanceToObstacle = 30._m;
  const auto distanceToObstacleFar = 60._m;

  ON_CALL(fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(4.0_mps_sq));
  ON_CALL(fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.mock_currentSituation));
  ON_CALL(testMentalCalculations, GetTtcForUrgencyFactorSituationCollision(data.mock_currentSituation)).WillByDefault(Return(data.input_ttc));
  ON_CALL(testMentalCalculations, CalculateUrgencyFactorSituationNoCollision(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane)).WillByDefault(Return(data.expected_urgencyFactor));
  ON_CALL(testMentalCalculations, CalculateUrgencyFactorSituationCollision(data.input_ttc)).WillByDefault(Return(data.expected_urgencyFactor));

  // Call test
  double actual_urgencyFactor = testMentalCalculations.TestCalculateUrgencyFactorSituation(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane);

  // Evaluate results
  ASSERT_DOUBLE_EQ(data.expected_urgencyFactor, actual_urgencyFactor);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateUrgencyFactorSituation,
    testing::Values(
        DataFor_CalculateUrgencyFactorSituation{
            .mock_currentSituation = Situation::FOLLOWING_DRIVING,
            .input_ttc = 100._s,
            .expected_urgencyFactor = 0.},
        DataFor_CalculateUrgencyFactorSituation{
            .mock_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_ttc = 100._s,
            .expected_urgencyFactor = 0.},
        DataFor_CalculateUrgencyFactorSituation{
            .mock_currentSituation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .input_ttc = 100._s,
            .expected_urgencyFactor = 0.9},
        DataFor_CalculateUrgencyFactorSituation{
            .mock_currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .input_ttc = 4.3_s,
            .expected_urgencyFactor = 0.2},
        DataFor_CalculateUrgencyFactorSituation{
            .mock_currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .input_ttc = 1._s,
            .expected_urgencyFactor = 1.}));

/********************************************************
 * CHECK AdjustDesiredFollowingDistanceDueToCooperation *
 ********************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AdjustDesiredFollowingDistanceDueToCooperation
{
  units::length::meter_t desiredDistance;
  bool isCooperative;
  Situation currentSituation;
  AreaOfInterest aoiRegulate;
  AreaOfInterest causingVehicleAoi;
  units::velocity::meters_per_second_t absoluteVelocityEgo;
  units::velocity::meters_per_second_t trafficJamVelocityThreshold;
  units::length::meter_t result_modifiedDesiredDistance;
};

class MentalCalculations_AdjustDesiredFollowingDistanceDueToCooperation : public ::testing::Test,
                                                                          public ::testing::WithParamInterface<DataFor_AdjustDesiredFollowingDistanceDueToCooperation>
{
};

TEST_P(MentalCalculations_AdjustDesiredFollowingDistanceDueToCooperation, MentalCalculations_CheckAdjustDesiredFollowingDistanceDueToCooperation)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  DataFor_AdjustDesiredFollowingDistanceDueToCooperation data = GetParam();

  // TODO Shared ptr only a temporary workaround, change when SurroundingVehiclesContainer is integrated in MentalModel
  NiceMock<FakeSurroundingVehicle> leadingVehicle;
  ON_CALL(leadingVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoiRegulate));

  /* Set up mocks */

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(data.absoluteVelocityEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(data.trafficJamVelocityThreshold));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(data.isCooperative));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.currentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationSideCluster()).WillByDefault(Return(AreaOfInterest::NumberOfAreaOfInterests));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationSideCluster()).WillByDefault(Return(data.causingVehicleAoi));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicle()).WillByDefault(Return(&leadingVehicle));

  const auto result = TEST_HELPER.mentalCalculations.AdjustDesiredFollowingDistanceDueToCooperation(data.desiredDistance);

  ASSERT_EQ(result, data.result_modifiedDesiredDistance);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_AdjustDesiredFollowingDistanceDueToCooperation,
    testing::Values(
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust desired distance wish */
            .desiredDistance = 10._m,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = 12._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust desired distance wish */
            .desiredDistance = 10._m,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = 12._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Desired distance negative -> return same value */
            .desiredDistance = -10._m,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = -10._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Agent not below jam speed -> return same value */
            .desiredDistance = 10._m,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 10.0_mps,
            .trafficJamVelocityThreshold = 5.0_mps,
            .result_modifiedDesiredDistance = 10._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Agent not cooperative -> return same value */
            .desiredDistance = 10._m,
            .isCooperative = false,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = 10._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Not a lane change situation -> return same value */
            .desiredDistance = 10._m,
            .isCooperative = true,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = 10._m},
        DataFor_AdjustDesiredFollowingDistanceDueToCooperation{
            /* Causing vehicle is not aoi regulate -> return same value */
            .desiredDistance = 10._m,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::LEFT_FRONT,
            .absoluteVelocityEgo = 5.0_mps,
            .trafficJamVelocityThreshold = 10.0_mps,
            .result_modifiedDesiredDistance = 10._m}));

/**************************************************
 * CHECK EvaluateUrgencyFactorMesoscopicSituation *
 **************************************************/

/// \brief Data table for definition of individual test cases for EvaluateUrgencyFactorMesoscopicSituation
struct DataFor_EvaluateUrgencyFactorMesoscopicSituation
{
  MesoscopicSituation input_CurrentSituation;
  bool input_isObstacle;

  double expected_riskObstacle;
  double expected_urgencyFactor;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_EvaluateUrgencyFactorMesoscopicSituation& obj)
  {
    return os
           << " | input_isObstacle (bool): " << obj.input_isObstacle
           << " | expected_riskObstacle (double): " << obj.expected_riskObstacle
           << " | expected_urgencyFactor (double): " << obj.expected_urgencyFactor;
  }
};

class MentalCalculations_EvaluateUrgencyFactorMesoscopicSituation : public ::testing::Test,
                                                                    public ::testing::WithParamInterface<DataFor_EvaluateUrgencyFactorMesoscopicSituation>
{
};

TEST_P(MentalCalculations_EvaluateUrgencyFactorMesoscopicSituation, MentalCalculations_CheckFunction_EvaluateUrgencyFactorMesoscopicSituations)
{
  // Create required classes for test
  DataFor_EvaluateUrgencyFactorMesoscopicSituation data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  TestMentalCalculations6 testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up test
  double inout_riskObstacle = 0.1;
  double inout_riskEndOfLane = 0.2;
  auto distanceToObstacle = 30._m;

  ON_CALL(fakeMentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(fakeMentalModel, IsMesoscopicSituationActive(data.input_CurrentSituation)).WillByDefault(Return(true));
  ON_CALL(fakeFeatureExtractor, IsObstacle(&fakeVehicle)).WillByDefault(Return(data.input_isObstacle));
  ON_CALL(testMentalCalculations, CalculateRiskObstacle(distanceToObstacle)).WillByDefault(Return(inout_riskObstacle));
  ON_CALL(testMentalCalculations, CalculateUrgencyFactor(inout_riskObstacle, inout_riskEndOfLane)).WillByDefault(Return(data.expected_urgencyFactor));

  // Call test
  double result = testMentalCalculations.TestEvaluateUrgencyFactorMesoscopicSituation(distanceToObstacle, inout_riskObstacle, inout_riskEndOfLane);

  // Evaluate results
  ASSERT_EQ(data.expected_urgencyFactor, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_EvaluateUrgencyFactorMesoscopicSituation,
    testing::Values(
        DataFor_EvaluateUrgencyFactorMesoscopicSituation{
            .input_CurrentSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,
            .input_isObstacle = true,
            .expected_riskObstacle = 0.9,
            .expected_urgencyFactor = 0.9},
        DataFor_EvaluateUrgencyFactorMesoscopicSituation{
            .input_CurrentSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,
            .input_isObstacle = false,
            .expected_riskObstacle = 0.1,
            .expected_urgencyFactor = 0.1},
        DataFor_EvaluateUrgencyFactorMesoscopicSituation{
            .input_CurrentSituation = MesoscopicSituation{},
            .input_isObstacle = true,
            .expected_riskObstacle = 0.1,
            .expected_urgencyFactor = 0.}));

/**********************************
 * CHECK EvaluateAOIsAndDistances *
 **********************************/

/// \brief Data table for definition of individual test cases for EvaluateAOIsAndDistances
struct DataFor_EvaluateAOIsAndDistances
{
  AreaOfInterest inout_aoi;
  AreaOfInterest inout_aoiFar;
  units::length::meter_t inout_distanceToObstacle;
  units::length::meter_t inout_distanceToObstacleFar;

  Situation mock_currentSituation;

  AreaOfInterest expected_aoi;
  AreaOfInterest expected_aoiFar;
  units::length::meter_t expected_distanceToObstacle;
  units::length::meter_t expected_distanceToObstacleFar;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_EvaluateAOIsAndDistances& obj)
  {
    return os
           << "inout_aoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.inout_aoi)
           << " | inout_aoiFar (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.inout_aoiFar)
           << " | inout_distanceToObstacle (double): " << obj.inout_distanceToObstacle
           << " | inout_distanceToObstacleFar (double): " << obj.inout_distanceToObstacleFar
           << " | mock_currentSituation (Situation): " << ScmCommons::SituationToString(obj.mock_currentSituation)
           << " | expected_aoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.expected_aoi)
           << " | expected_aoiFar (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.expected_aoiFar)
           << " | expected_distanceToObstacle (double): " << obj.expected_distanceToObstacle
           << " | expected_distanceToObstacle (double): " << obj.expected_distanceToObstacleFar;
  }
};

class MentalCalculations_EvaluateAOIsAndDistances : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_EvaluateAOIsAndDistances>
{
};

TEST_P(MentalCalculations_EvaluateAOIsAndDistances, MentalCalculations_CheckFunction_EvaluateAOIsAndDistances)
{
  // Create required classes for test
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  DataFor_EvaluateAOIsAndDistances data = GetParam();

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT)).WillByDefault(Return(1._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::LEFT_FRONT_FAR)).WillByDefault(Return(2._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT)).WillByDefault(Return(3._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT_FAR)).WillByDefault(Return(4._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.mock_currentSituation));

  // Call test
  TEST_HELPER.mentalCalculations.EvaluateAOIsAndDistances(data.inout_aoi, data.inout_aoiFar, data.inout_distanceToObstacle, data.inout_distanceToObstacleFar);

  // Evaluate results
  ASSERT_EQ(data.expected_aoi, data.inout_aoi);
  ASSERT_EQ(data.expected_aoiFar, data.inout_aoiFar);
  ASSERT_EQ(data.expected_distanceToObstacle, data.inout_distanceToObstacle);
  ASSERT_EQ(data.expected_distanceToObstacleFar, data.inout_distanceToObstacleFar);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_EvaluateAOIsAndDistances,
    testing::Values(
        DataFor_EvaluateAOIsAndDistances{
            .inout_aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_aoiFar = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_distanceToObstacle = 999._m,
            .inout_distanceToObstacleFar = 999._m,
            .mock_currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .expected_aoi = AreaOfInterest::LEFT_FRONT,
            .expected_aoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .expected_distanceToObstacle = 1._m,
            .expected_distanceToObstacleFar = 2._m},
        DataFor_EvaluateAOIsAndDistances{
            .inout_aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_aoiFar = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_distanceToObstacle = 999._m,
            .inout_distanceToObstacleFar = 999._m,
            .mock_currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .expected_aoi = AreaOfInterest::RIGHT_FRONT,
            .expected_aoiFar = AreaOfInterest::RIGHT_FRONT_FAR,
            .expected_distanceToObstacle = 3._m,
            .expected_distanceToObstacleFar = 4._m},
        DataFor_EvaluateAOIsAndDistances{
            .inout_aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_aoiFar = AreaOfInterest::NumberOfAreaOfInterests,
            .inout_distanceToObstacle = 999._m,
            .inout_distanceToObstacleFar = 999._m,
            .mock_currentSituation = Situation::COLLISION,
            .expected_aoi = AreaOfInterest::EGO_FRONT,
            .expected_aoiFar = AreaOfInterest::EGO_FRONT_FAR,
            .expected_distanceToObstacle = 999._m,
            .expected_distanceToObstacleFar = 999._m}));

/*******************************
 * CHECK CalculateRiskObstacle *
 *******************************/

/// \brief Data table for definition of individual test cases for CalculateRiskObstacle
struct DataFor_CalculateRiskObstacle
{
  AreaOfInterest input_aoi;
  AreaOfInterest input_aoiFar;
  units::length::meter_t input_distanceToObstacle;
  units::length::meter_t input_distanceToObstacleFar;
  double input_riskObstacle;

  bool mock_isObstacle;
  bool mock_isOverlapping;

  double expected_riskObstacle;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CalculateRiskObstacle& obj)
  {
    return os
           << "input_aoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoi)
           << " | input_aoiFar (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoiFar)
           << " | input_distanceToObstacle (double): " << obj.input_distanceToObstacle
           << " | input_distanceToObstacleFar (double): " << obj.input_distanceToObstacle
           << " | input_riskObstacle (double): " << obj.input_riskObstacle
           << " | mock_isObstacle (bool): " << obj.mock_isObstacle
           << " | mock_isOverlapping (bool): " << obj.mock_isOverlapping
           << " | expected_riskObstacle (double): " << obj.expected_riskObstacle;
  }
};

class MentalCalculations_CalculateRiskObstacle : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_CalculateRiskObstacle>
{
};

TEST_P(MentalCalculations_CalculateRiskObstacle, MentalCalculations_CheckFunction_CalculateRiskObstacle)
{
  // Create required classes for test
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  DataFor_CalculateRiskObstacle data = GetParam();
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  // Set up test
  ObstructionScm obstruction{0_m, 0_m, 0_m};
  obstruction.isOverlapping = data.mock_isOverlapping;

  DriverParameters driverParameters;
  driverParameters.previewDistance = 5_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(data.input_aoi, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(data.input_aoi, _)).WillByDefault(Return(obstruction));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsObstacle(&fakeVehicle)).WillByDefault(Return(data.mock_isObstacle));

  // Call test
  double actual_riskObstacle = TEST_HELPER.mentalCalculations.CalculateRiskObstacle(data.input_aoi, data.input_aoiFar, data.input_distanceToObstacle, data.input_distanceToObstacleFar, data.input_riskObstacle);

  // Evaluate results
  ASSERT_EQ(data.expected_riskObstacle, actual_riskObstacle);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateRiskObstacle,
    testing::Values(
        DataFor_CalculateRiskObstacle{
            .input_aoi = AreaOfInterest::EGO_FRONT,
            .input_aoiFar = AreaOfInterest::EGO_FRONT_FAR,
            .input_distanceToObstacle = 999._m,
            .input_distanceToObstacleFar = 999._m,
            .input_riskObstacle = 0.7,
            .mock_isObstacle = false,
            .mock_isOverlapping = true,
            .expected_riskObstacle = 0.7},
        DataFor_CalculateRiskObstacle{
            .input_aoi = AreaOfInterest::EGO_FRONT,
            .input_aoiFar = AreaOfInterest::EGO_FRONT_FAR,
            .input_distanceToObstacle = 0._m,
            .input_distanceToObstacleFar = 2._m,
            .input_riskObstacle = 0.7,
            .mock_isObstacle = true,
            .mock_isOverlapping = false,
            .expected_riskObstacle = 1.0},
        DataFor_CalculateRiskObstacle{
            .input_aoi = AreaOfInterest::LEFT_FRONT,
            .input_aoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_distanceToObstacle = 0._m,
            .input_distanceToObstacleFar = 0._m,
            .input_riskObstacle = 0.7,
            .mock_isObstacle = true,
            .mock_isOverlapping = true,
            .expected_riskObstacle = 1.0},
        DataFor_CalculateRiskObstacle{
            .input_aoi = AreaOfInterest::LEFT_FRONT,
            .input_aoiFar = AreaOfInterest::LEFT_FRONT_FAR,
            .input_distanceToObstacle = 4._m,
            .input_distanceToObstacleFar = 4.5_m,
            .input_riskObstacle = 0.7,
            .mock_isObstacle = true,
            .mock_isOverlapping = true,
            .expected_riskObstacle = 0.2}));

/************************************************
 * CHECK DetermineLateralOffsetDueToCooperation *
 ************************************************/

TEST(MentalCalculations_DetermineLateralOffsetDueToCooperation, GivenNonCooperativeBehaviorThrowsRuntimeError)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(false));

  // Expect exception
  EXPECT_THROW(TEST_HELPER.mentalCalculations.DetermineLateralOffsetDueToCooperation(), std::runtime_error);
}

TEST(MentalCalculations_DetermineLateralOffsetDueToCooperation, GivenLateralActionNotIntentToChangeLanes)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(LateralAction(LateralAction::State::LANE_KEEPING)));
  // Expect exception
  EXPECT_EQ(TEST_HELPER.mentalCalculations.DetermineLateralOffsetDueToCooperation(), 0_m);
}

// /// \brief Data table for definition of individual test cases
struct DataFor_DetermineLateralOffsetDueToCooperation
{
  bool isCooperative;
  LateralAction lateralAction;
  units::length::meter_t distanceToLaneBoundaryLeft;
  units::length::meter_t distanceToLaneBoundaryRight;
  units::length::meter_t lateralPositionInLane;
  double cooperationFactor;
  units::length::meter_t result_lateralPosition;
};

class MentalCalculations_DetermineLateralOffsetDueToCooperation : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<DataFor_DetermineLateralOffsetDueToCooperation>
{
};

TEST_P(MentalCalculations_DetermineLateralOffsetDueToCooperation, MentalCalculations_CheckDetermineLateralOffsetDueToCooperation)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_DetermineLateralOffsetDueToCooperation data = GetParam();

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(data.isCooperative));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToBoundaryLeft()).WillByDefault(Return(data.distanceToLaneBoundaryLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToBoundaryRight()).WillByDefault(Return(data.distanceToLaneBoundaryRight));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(data.lateralPositionInLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor()).WillByDefault(Return(data.cooperationFactor));

  const auto result = TEST_HELPER.mentalCalculations.DetermineLateralOffsetDueToCooperation();

  ASSERT_EQ(result, data.result_lateralPosition);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_DetermineLateralOffsetDueToCooperation,
    testing::Values(
        DataFor_DetermineLateralOffsetDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust lateral position */
            .isCooperative = true,
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .distanceToLaneBoundaryLeft = 1._m,
            .distanceToLaneBoundaryRight = 2._m,
            .lateralPositionInLane = 10._m,
            .cooperationFactor = 3.,
            .result_lateralPosition = 10._m + 1._m * 3.},
        DataFor_DetermineLateralOffsetDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust lateral position */
            .isCooperative = true,
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .distanceToLaneBoundaryLeft = 1._m,
            .distanceToLaneBoundaryRight = 2._m,
            .lateralPositionInLane = 10._m,
            .cooperationFactor = 3.,
            .result_lateralPosition = 10._m - 2._m * 3.},
        DataFor_DetermineLateralOffsetDueToCooperation{
            /* Distance to left lane boundary smaller 0 while changing to left -> do not adjust, is already leaving lane */
            .isCooperative = true,
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .distanceToLaneBoundaryLeft = -1._m,
            .distanceToLaneBoundaryRight = 0._m,
            .lateralPositionInLane = 10._m,
            .cooperationFactor = 3.,
            .result_lateralPosition = 10._m},
        DataFor_DetermineLateralOffsetDueToCooperation{
            /* Distance to right lane boundary larger 0 while changing to right -> do not adjust, is already leaving lane */
            .isCooperative = true,
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .distanceToLaneBoundaryLeft = 0._m,
            .distanceToLaneBoundaryRight = -1._m,
            .lateralPositionInLane = 10._m,
            .cooperationFactor = 3.,
            .result_lateralPosition = 10._m}));

/**********************************
 * CHECK EstimatedTimeToLeaveLane *
 **********************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_EstimatedTimeToLeaveLane
{
  units::velocity::meters_per_second_t input_lateralVelocity;
  units::length::meter_t input_DistanceToLaneBoundaryLeft;
  units::length::meter_t input_DistanceToLaneBoundaryRight;
  units::length::meter_t input_carWidth;
  units::time::second_t expected_Result;
};

class MentalCalculations_EstimatedTimeToLeaveLane : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_EstimatedTimeToLeaveLane>
{
};

TEST_P(MentalCalculations_EstimatedTimeToLeaveLane, MentalCalculations_CheckEstimatedTimeToLeaveLane)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_EstimatedTimeToLeaveLane data = GetParam();

  // Set data
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  OwnVehicleInformationScmExtended ownVehicleInfo{};
  ownVehicleInfo.distanceToLaneBoundaryLeft = data.input_DistanceToLaneBoundaryLeft;
  ownVehicleInfo.distanceToLaneBoundaryRight = data.input_DistanceToLaneBoundaryRight;

  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  vehicleParameters.bounding_box.dimension.width = data.input_carWidth;

  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(data.input_lateralVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  // Call test
  auto result = TEST_HELPER.mentalCalculations.EstimatedTimeToLeaveLane();

  // Evaluate results

  if (units::math::isinf(data.expected_Result))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expected_Result);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_EstimatedTimeToLeaveLane,
    testing::Values(
        DataFor_EstimatedTimeToLeaveLane{
            .input_lateralVelocity = 0._mps,
            .input_DistanceToLaneBoundaryLeft = 1._m,
            .input_DistanceToLaneBoundaryRight = 2._m,
            .input_carWidth = 2._m,
            .expected_Result = ScmDefinitions::INF_TIME},
        DataFor_EstimatedTimeToLeaveLane{
            .input_lateralVelocity = 1._mps,
            .input_DistanceToLaneBoundaryLeft = 1._m,
            .input_DistanceToLaneBoundaryRight = 2._m,
            .input_carWidth = 2._m,
            .expected_Result = 3_s},
        DataFor_EstimatedTimeToLeaveLane{
            .input_lateralVelocity = -1._mps,
            .input_DistanceToLaneBoundaryLeft = -1._m,
            .input_DistanceToLaneBoundaryRight = 0._m,
            .input_carWidth = 2._m,
            .expected_Result = -2_s}));

/***********************************
 * CHECK GetUrgencyBetweenMinAndEq *
 ***********************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetUrgencyBetweenMinAndEq
{
  bool mock_IsNearEnoughForFollowing;
  bool mock_IsMinimumFollowingDistanceViolated;
  units::length::meter_t mock_GetEqDistance;
  units::length::meter_t mock_GetMinDistance;
  double expected_Result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GetUrgencyBetweenMinAndEq& obj)
  {
    return os
           << "  mock_IsNearEnoughForFollowing (bool): " << ScmCommons::BooleanToString(obj.mock_IsNearEnoughForFollowing)
           << "| mock_IsMinimumFollowingDistanceViolated (bool): " << ScmCommons::BooleanToString(obj.mock_IsMinimumFollowingDistanceViolated)
           << "| mock_GetEqDistance (double): " << obj.mock_GetEqDistance
           << "| mock_GetMinDistance (double): " << obj.mock_GetMinDistance
           << "| expected_Result (double): " << obj.expected_Result;
  }
};

class MentalCalculations_GetUrgencyBetweenMinAndEq : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_GetUrgencyBetweenMinAndEq>
{
};

TEST_P(MentalCalculations_GetUrgencyBetweenMinAndEq, FeatureExtractor_CheckFunction_GetUrgencyBetweenMinAndEq)
{
  // Get resources for testing
  DataFor_GetUrgencyBetweenMinAndEq data = GetParam();
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  TestMentalCalculations testMentalCalculations(fakeFeatureExtractor);

  testMentalCalculations.Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up tests
  EXPECT_CALL(fakeFeatureExtractor, IsNearEnoughForFollowing(_, _)).Times(1).WillRepeatedly(Return(data.mock_IsNearEnoughForFollowing));
  EXPECT_CALL(fakeFeatureExtractor, IsMinimumFollowingDistanceViolated(_, _)).Times(static_cast<int>(data.mock_IsNearEnoughForFollowing)).WillRepeatedly(Return(data.mock_IsMinimumFollowingDistanceViolated));  // Will only be called when IsNearEnoughForFollowing is true
  EXPECT_CALL(testMentalCalculations, GetEqDistance(_, _)).Times(static_cast<int>(data.mock_IsNearEnoughForFollowing && !data.mock_IsMinimumFollowingDistanceViolated) * 2).WillRepeatedly(Return(data.mock_GetEqDistance));
  EXPECT_CALL(testMentalCalculations, GetMinDistance(_, _, _, _)).Times(static_cast<int>(data.mock_IsNearEnoughForFollowing && !data.mock_IsMinimumFollowingDistanceViolated)).WillRepeatedly(Return(data.mock_GetMinDistance));

  // Run Test
  double result = testMentalCalculations.TestGetUrgencyBetweenMinAndEq(10._m, AreaOfInterest::EGO_FRONT);

  // Evaluate results
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetUrgencyBetweenMinAndEq,
    testing::Values(
        DataFor_GetUrgencyBetweenMinAndEq{
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsMinimumFollowingDistanceViolated = false,
            .mock_GetEqDistance = -99._m,
            .mock_GetMinDistance = -99._m,
            .expected_Result = 0.},
        DataFor_GetUrgencyBetweenMinAndEq{
            .mock_IsNearEnoughForFollowing = true,
            .mock_IsMinimumFollowingDistanceViolated = true,
            .mock_GetEqDistance = -99._m,
            .mock_GetMinDistance = -99._m,
            .expected_Result = 1.},
        DataFor_GetUrgencyBetweenMinAndEq{
            .mock_IsNearEnoughForFollowing = true,
            .mock_IsMinimumFollowingDistanceViolated = false,
            .mock_GetEqDistance = 20._m,
            .mock_GetMinDistance = 5._m,
            .expected_Result = 2. / 3.}));

/************************
 * CHECK URGENCY FACTOR *
 ************************/

/// \brief Data table for definition of individual test cases for different situations
struct DataFor_UrgencyFactor
{
  double expectedUrgency;
  units::length::meter_t extrapolatedDeltaSEgoFront;
  units::length::meter_t extrapolatedDeltaSRightFront;
  Situation currentSituation;
  MesoscopicSituation currentMesoscopicSituation;
  units::length::meter_t distanceToEndOfLaneEgoFront;
  units::time::second_t ttc;
  bool isObstacle;
};

class MentalCalculations_CheckUrgencyFactor : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_UrgencyFactor>
{
};

TEST_P(MentalCalculations_CheckUrgencyFactor, CheckUrgencyFactor_ForCurrentSituation)
{
  // Get test data
  DataFor_UrgencyFactor data = GetParam();

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  // Get resources

  auto mentalCalculations = std::make_unique<TestMentalCalculations>(fakeFeatureExtractor);
  mentalCalculations->Initialize(&fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up test
  DriverParameters driverParameters;
  driverParameters.previewDistance = 250_m;
  driverParameters.comfortLongitudinalDeceleration = 1.0_mps_sq;
  driverParameters.maximumLongitudinalDeceleration = 2.0_mps_sq;

  GeometryInformationSCM geometryInformation;
  geometryInformation.Close().distanceToEndOfLane = data.distanceToEndOfLaneEgoFront;
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformation));
  ON_CALL(fakeFeatureExtractor, IsNearEnoughForFollowing(_, _)).WillByDefault(Return(true));
  ON_CALL(fakeFeatureExtractor, IsMinimumFollowingDistanceViolated(_, _)).WillByDefault(Return(false));
  ON_CALL(fakeMentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(fakeFeatureExtractor, IsObstacle(_)).WillByDefault(Return(data.isObstacle));

  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(fakeMentalModel, GetIsStaticObject(_, _)).WillByDefault(Return(data.isObstacle));
  ON_CALL(fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(15.0_mps));
  ON_CALL(fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.currentSituation));
  ON_CALL(fakeMentalModel, IsMesoscopicSituationActive(data.currentMesoscopicSituation)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(data.ttc));
  ON_CALL(fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(data.extrapolatedDeltaSEgoFront));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::RIGHT_FRONT)).WillByDefault(Return(data.extrapolatedDeltaSRightFront));

  EXPECT_CALL(*mentalCalculations, GetMinDistance(AreaOfInterest::LEFT_FRONT, _, _, _)).WillRepeatedly(Return(10.5_m));
  EXPECT_CALL(*mentalCalculations, GetEqDistance(AreaOfInterest::LEFT_FRONT, _)).WillRepeatedly(Return(24._m));
  EXPECT_CALL(*mentalCalculations, GetMinDistance(AreaOfInterest::RIGHT_FRONT, _, _, _)).WillRepeatedly(Return(7.5_m));
  EXPECT_CALL(*mentalCalculations, GetEqDistance(AreaOfInterest::RIGHT_FRONT, _)).WillRepeatedly(Return(21._m));

  ON_CALL(*mentalCalculations, GetSideTtc(_)).WillByDefault(Return(data.ttc));
  ON_CALL(*mentalCalculations, GetMinDistance(_, _, _, _)).WillByDefault(Return(0._m));
  ON_CALL(*mentalCalculations, GetEqDistance(_, _)).WillByDefault(Return(2._m));

  // Call test
  double urgencyFactor = mentalCalculations->GetUrgencyFactorForLaneChange();

  double minThresholdUrgencyFactor = .0;
  double maxThresholdUrgencyFactor = .9;
  // Evaluate result
  ASSERT_TRUE(urgencyFactor >= minThresholdUrgencyFactor);
  ASSERT_TRUE(urgencyFactor <= maxThresholdUrgencyFactor);
  ASSERT_NEAR(urgencyFactor, data.expectedUrgency, 0.001);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CheckUrgencyFactor,
    testing::Values(
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 5.5_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 5._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.5,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 3.25_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.9,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 1.5_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.9,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 1.4_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 5.5_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 5._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.5,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 3.25_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.9,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 1.5_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.9,
            .extrapolatedDeltaSEgoFront = 0._m,
            .extrapolatedDeltaSRightFront = 0._m,
            .currentSituation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 1000._m,
            .ttc = 1.4_s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 40._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 60._m,
            .ttc = 99._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 30._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 120._m,
            .ttc = 99._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.444,
            .extrapolatedDeltaSEgoFront = 100._m,
            .extrapolatedDeltaSRightFront = 15._m,
            .currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 200._m,
            .ttc = 99._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 100._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FREE_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .distanceToEndOfLaneEgoFront = 180._m,
            .ttc = 99._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 60._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FREE_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::MANDATORY_EXIT,
            .distanceToEndOfLaneEgoFront = 180._m,
            .ttc = 99._s,
            .isObstacle = false},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.76,
            .extrapolatedDeltaSEgoFront = 60._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,
            .distanceToEndOfLaneEgoFront = 180._m,
            .ttc = 99._s,
            .isObstacle = true},
        DataFor_UrgencyFactor{
            .expectedUrgency = 0.,
            .extrapolatedDeltaSEgoFront = 60._m,
            .extrapolatedDeltaSRightFront = 22.5_m,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .currentMesoscopicSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,
            .distanceToEndOfLaneEgoFront = 180._m,
            .ttc = 99._s,
            .isObstacle = false}));

/**************************
 * CHECK GET_MIN_DISTANCE *
 **************************/

TEST(MentalCalculations, GetEqMinDistance)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, _, _, _, _, _, _)).WillByDefault(Return(10_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  OwnVehicleInformationScmExtended ego{};
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));
  const SurroundingVehicleInterface* fakeSurroundingVehicle{&fakeVehicle};

  auto result = TEST_HELPER.mentalCalculations.GetMinDistance(fakeSurroundingVehicle, MinThwPerspective::NORM, units::velocity::meters_per_second_t(ScmDefinitions::DEFAULT_VALUE));

  ASSERT_EQ(result, 10_m);
}

/**************************
 * CHECK GET_EQ_DISTANCE  *
 **************************/

/// \brief Data table for definition of individual test cases for different situations
struct DataFor_GetEqDistance
{
  bool toTheSideOfEgo;
  bool behindEgo;
  units::length::meter_t result;
};

class MentalCalculations_GetEqDistance : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_GetEqDistance>
{
};

TEST_P(MentalCalculations_GetEqDistance, GetEqDistanceWithEarlyReturn)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  DataFor_GetEqDistance data = GetParam();
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(data.toTheSideOfEgo));
  ON_CALL(fakeVehicle, BehindEgo()).WillByDefault(Return(data.behindEgo));
  ON_CALL(fakeVehicle, GetLongitudinalVelocity()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(1.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(1.0_mps));
  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, _, _, _, _, _, _)).WillByDefault(Return(10_m));
  OwnVehicleInformationScmExtended ego{};
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));
  const SurroundingVehicleInterface* fakeSurroundingVehicle{&fakeVehicle};
  MinThwPerspective perspective = MinThwPerspective::NORM;

  auto result = TEST_HELPER.mentalCalculations.GetEqDistance(fakeSurroundingVehicle);

  ASSERT_EQ(result, data.result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetEqDistance,
    testing::Values(
        DataFor_GetEqDistance{
            .toTheSideOfEgo = true,
            .behindEgo = false,
            .result = 0.0_m},
        DataFor_GetEqDistance{
            .toTheSideOfEgo = false,
            .behindEgo = false,
            .result = 10.0_m}));

/*************************************************
 * CHECK GetTimeHeadwayBetweenTwoAreasOfInterest *
 *************************************************/
struct DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest
{
  AreaOfInterest aoi_first;
  AreaOfInterest aoi_second;
  units::length::meter_t relativeNetDistanceAoi_first;
  units::length::meter_t relativeNetDistanceAoi_second;
  bool isVehicleVisible;
  units::time::second_t expectedResult;
};

class MentalCalculations_GetTimeHeadwayBetweenTwoAreasOfInterest : public ::testing::Test,
                                                                   public ::testing::WithParamInterface<DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest>
{
};

TEST_P(MentalCalculations_GetTimeHeadwayBetweenTwoAreasOfInterest, Check_GetTimeHeadwayBetweenTwoAreasOfInterest)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest data = GetParam();

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.isVehicleVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPreviewDistance()).WillByDefault(Return(100_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(10_mps));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(data.aoi_first)).WillByDefault(Return(data.relativeNetDistanceAoi_first));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(data.aoi_second)).WillByDefault(Return(data.relativeNetDistanceAoi_second));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength(_, _)).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocity(_, _)).WillByDefault(Return(20.0_mps));

  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));

  auto result = TEST_HELPER.mentalCalculations.GetTimeHeadwayBetweenTwoAreasOfInterest(data.aoi_first, data.aoi_second);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetTimeHeadwayBetweenTwoAreasOfInterest,
    testing::Values(
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = false,
            .expectedResult = 10.0_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = true,
            .expectedResult = 2.5_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 50.0_m,
            .isVehicleVisible = true,
            .expectedResult = 2.5_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::EGO_FRONT,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::LEFT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5.25_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::EGO_FRONT,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5.25_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::EGO_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5.25_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 5_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = true,
            .expectedResult = 2.75_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 50.0_m,
            .isVehicleVisible = true,
            .expectedResult = 2.75_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0_s},
        DataFor_GetTimeHeadwayBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::RIGHT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0_s}));

/*********************
 * CHECK GetTauDotEq *
 *********************/
struct DataFor_GetTauDotEq
{
  units::velocity::meters_per_second_t longitudinalVelocityDelta;
  double expectedResult;
};

class MentalCalculations_GetTauDotEq : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_GetTauDotEq>
{
};

TEST_P(MentalCalculations_GetTauDotEq, Check_GetTauDotEq)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetTauDotEq data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsMergeRegulate(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineVelocityReasonRightOvertakingProhibition()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(100.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAccelerationDelta(_, _)).WillByDefault(Return(5.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityDelta(_, _)).WillByDefault(Return(data.longitudinalVelocityDelta));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetTauDotEq(AreaOfInterest::EGO_FRONT);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetTauDotEq,
    testing::Values(
        DataFor_GetTauDotEq{
            .longitudinalVelocityDelta = -1.0_mps,
            .expectedResult = 0.0},
        DataFor_GetTauDotEq{
            .longitudinalVelocityDelta = 5.0_mps,
            .expectedResult = 19.0}));

/****************************************
 * CHECK CalculateFollowingAcceleration *
 ****************************************/

struct DataFor_CalculateFollowingAcceleration
{
  double longitudinalVelocityDelta;
  units::length::meter_t carRestartDistance;
  units::velocity::meters_per_second_t absoluteVelocityEgo;
  units::length::meter_t relativeNetDistance;
  AreaOfInterest assignedAoi;
  units::length::meter_t deltaDistanceForAdjustment;
  units::length::meter_t rearDistance;
  units::acceleration::meters_per_second_squared_t expectedResult;
};

class MentalCalculations_CalculateFollowingAcceleration : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataFor_CalculateFollowingAcceleration>
{
};

TEST_P(MentalCalculations_CalculateFollowingAcceleration, Check_CalculateFollowingAcceleration)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculateFollowingAcceleration data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(1));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength()).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCarRestartDistance()).WillByDefault(Return(data.carRestartDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(data.absoluteVelocityEgo));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(true));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  auto fakeAccelerationCalculationsQuery = std::make_shared<NiceMock<FakeAccelerationCalculationsQuery>>();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalActionState()).WillByDefault(Return(LongitudinalActionState::ACTIVE_ZIP_MERGING));
  ON_CALL(*fakeAccelerationCalculationsQuery, GetMentalModel()).WillByDefault(ReturnRef(TEST_HELPER.fakeMentalModel));
  ON_CALL(*fakeAccelerationCalculationsQuery, GetMinDistance(_)).WillByDefault(Return(5.0_m));
  ON_CALL(*fakeAccelerationCalculationsQuery, IsEgoNearStandstill()).WillByDefault(Return(true));
  ON_CALL(*fakeAccelerationCalculationsQuery, GetCarQueuingDistance()).WillByDefault(Return(20.0_m));
  ON_CALL(*fakeAccelerationCalculationsQuery, GetEgoVelocity()).WillByDefault(Return(30.0_mps));
  TEST_HELPER.mentalCalculations.SET_ACCELERATION_CALCULATION_QUERY(fakeAccelerationCalculationsQuery);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.assignedAoi));
  ON_CALL(fakeVehicle, RearDistance()).WillByDefault(Return(data.rearDistance));
  ON_CALL(fakeVehicle, GetLongitudinalVelocity()).WillByDefault(Return(35.0_mps));
  ON_CALL(fakeVehicle, GetLongitudinalAcceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(fakeVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(100.0_m));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(data.relativeNetDistance));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(8_s));

  auto result = TEST_HELPER.mentalCalculations.CalculateFollowingAcceleration(fakeVehicle, 5.0_mps_sq, data.deltaDistanceForAdjustment);

  if (data.deltaDistanceForAdjustment == 400_m)
  {
    ASSERT_NEAR(result.value(), data.expectedResult.value(), 0.001);
  }
  else
  {
    ASSERT_EQ(result, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateFollowingAcceleration,
    testing::Values(
        DataFor_CalculateFollowingAcceleration{
            .longitudinalVelocityDelta = -1.0,
            .carRestartDistance = 105.0_m,
            .absoluteVelocityEgo = 0.01_mps,
            .relativeNetDistance = 45.0_m,
            .assignedAoi = AreaOfInterest::EGO_FRONT_FAR,
            .deltaDistanceForAdjustment = 95_m,
            .rearDistance = 20_m,
            .expectedResult = -1_mps_sq},
        DataFor_CalculateFollowingAcceleration{
            .longitudinalVelocityDelta = -1.0,
            .carRestartDistance = 100.0_m,
            .absoluteVelocityEgo = 20_mps,
            .relativeNetDistance = 100.0_m,
            .assignedAoi = AreaOfInterest::EGO_FRONT,
            .deltaDistanceForAdjustment = 95_m,
            .rearDistance = 20_m,
            .expectedResult = -2.375_mps_sq},
        DataFor_CalculateFollowingAcceleration{
            .longitudinalVelocityDelta = 1.5,
            .carRestartDistance = 10.0_m,
            .absoluteVelocityEgo = 25_mps,
            .relativeNetDistance = 150.0_m,
            .assignedAoi = AreaOfInterest::EGO_FRONT,
            .deltaDistanceForAdjustment = 400_m,
            .rearDistance = 500_m,
            .expectedResult = 1.512_mps_sq}));

/**********************************************
 * CHECK CalculateAccelerationToDefuseLateral *
 **********************************************/

struct DataFor_CalculateAccelerationToDefuseLateral
{
  AreaOfInterest aoi;
  ObstructionScm obstruction;
  units::velocity::meters_per_second_t velocityDeltaLateral;
  units::acceleration::meters_per_second_squared_t expectedResult;
};

class MentalCalculations_CalculateAccelerationToDefuseLateral : public ::testing::Test,
                                                                public ::testing::WithParamInterface<DataFor_CalculateAccelerationToDefuseLateral>
{
};

TEST_P(MentalCalculations_CalculateAccelerationToDefuseLateral, Check_CalculateAccelerationToDefuseLateral)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculateAccelerationToDefuseLateral data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(_, _)).WillByDefault(Return(data.obstruction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityDelta(_, _)).WillByDefault(Return(data.velocityDeltaLateral));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  auto result = TEST_HELPER.mentalCalculations.CalculateAccelerationToDefuseLateral(data.aoi);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateAccelerationToDefuseLateral,
    testing::Values(
        DataFor_CalculateAccelerationToDefuseLateral{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .obstruction = ObstructionScm(-1.0_m, 1.0_m, 1.0_m),
            .velocityDeltaLateral = 1.0_mps,
            .expectedResult = 0.0_mps_sq},
        DataFor_CalculateAccelerationToDefuseLateral{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .obstruction = ObstructionScm(1.0_m, -1.0_m, 1.0_m),
            .velocityDeltaLateral = -1.0_mps,
            .expectedResult = 1.0_mps_sq},
        DataFor_CalculateAccelerationToDefuseLateral{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .obstruction = ObstructionScm(-1.0_m, 1.0_m, 1.0_m),
            .velocityDeltaLateral = -1.0_mps,
            .expectedResult = 1.0_mps_sq},
        DataFor_CalculateAccelerationToDefuseLateral{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .obstruction = ObstructionScm(-1.0_m, 1.0_m, 1.0_m),
            .velocityDeltaLateral = -1.0_mps,
            .expectedResult = 0.0_mps_sq}));

/************************************************************************************
 * CHECK CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion *
 ************************************************************************************/

struct DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion
{
  units::acceleration::meters_per_second_squared_t acceleration;
  units::velocity::meters_per_second_t changeInVelocity;
  units::time::second_t expectedResult;
};

class MentalCalculations_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion : public ::testing::Test,
                                                                                                      public ::testing::WithParamInterface<DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion>
{
};

TEST_P(MentalCalculations_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion, Check_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion data = GetParam();

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  auto result = TEST_HELPER.mentalCalculations.CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion(data.acceleration, data.changeInVelocity);

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion,
    testing::Values(
        DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion{
            .acceleration = 0_mps_sq,
            .changeInVelocity = -5_mps,
            .expectedResult = ScmDefinitions::INF_TIME},
        DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion{
            .acceleration = 5_mps_sq,
            .changeInVelocity = -5_mps,
            .expectedResult = ScmDefinitions::INF_TIME},
        DataFor_CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion{
            .acceleration = 5_mps_sq,
            .changeInVelocity = 5_mps,
            .expectedResult = 1_s}));

/************************************************************************************
 * CHECK CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion *
 ************************************************************************************/

struct DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion
{
  units::acceleration::meters_per_second_squared_t acceleration;
  units::velocity::meters_per_second_t startingVelocity;
  units::length::meter_t changeInDistance;
  units::time::second_t expectedResult;
};

class MentalCalculations_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion : public ::testing::Test,
                                                                                                      public ::testing::WithParamInterface<DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion>
{
};

TEST_P(MentalCalculations_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion, Check_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion data = GetParam();

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  auto result = TEST_HELPER.mentalCalculations.CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion(data.acceleration, data.startingVelocity, data.changeInDistance);

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    EXPECT_NEAR(result.value(), data.expectedResult.value(), 0.01);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion,
    testing::Values(
        DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion{
            .acceleration = 0_mps_sq,
            .startingVelocity = 5_mps,
            .changeInDistance = 5.0_m,
            .expectedResult = 1_s},
        DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion{
            .acceleration = 0_mps_sq,
            .startingVelocity = 5_mps,
            .changeInDistance = -5.0_m,
            .expectedResult = ScmDefinitions::INF_TIME},
        DataFor_CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion{
            .acceleration = 5_mps_sq,
            .startingVelocity = 10_mps,
            .changeInDistance = 5.0_m,
            .expectedResult = 0.44_s}));

/***************************************************************
 * CHECK CalculateReachedDistanceForUniformlyAcceleratedMotion *
 ***************************************************************/

TEST(MentalCalculations_CalculateReachedDistanceForUniformlyAcceleratedMotion, Check_CalculateReachedDistanceForUniformlyAcceleratedMotion)
{
  MentalCalculationsTester TEST_HELPER;
  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);

  auto result = TEST_HELPER.mentalCalculations.CalculateReachedDistanceForUniformlyAcceleratedMotion(10_s, 20_mps_sq, 5_mps, 100_m);

  ASSERT_EQ(result, 1150_m);
}

/********************
 * CHECK GetSideTtc *
 ********************/

struct DataFor_GetSideTtc
{
  AreaOfInterest aoi;
  units::length::meter_t obstructionRight;
  units::time::second_t expectedResult;
};

class MentalCalculations_GetSideTtc : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_GetSideTtc>
{
};

TEST_P(MentalCalculations_GetSideTtc, Check_GetSideTtc)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetSideTtc data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.aoi, _)).WillByDefault(Return(true));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  ObjectInformationScmExtended object{};
  object.obstruction.right = data.obstructionRight;
  object.obstruction.left = 5_m;
  std::vector<ObjectInformationScmExtended> objectInformations{};

  if (data.aoi == AreaOfInterest::LEFT_SIDE)
  {
    objectInformations.push_back(object);
  }

  ON_CALL(fakeMicroscopicCharacteristics, GetSideObjectVector(_)).WillByDefault(Return(&objectInformations));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(data.aoi, _)).WillByDefault(Return(object.obstruction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityDelta(data.aoi, _)).WillByDefault(Return(2.0_mps));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetSideTtc(data.aoi);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetSideTtc,
    testing::Values(
        DataFor_GetSideTtc{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .obstructionRight = 4_m,
            .expectedResult = 0.0_s},
        DataFor_GetSideTtc{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .obstructionRight = -4_m,
            .expectedResult = -2.0_s},
        DataFor_GetSideTtc{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .obstructionRight = -4_m,
            .expectedResult = 99.0_s},
        DataFor_GetSideTtc{
            .aoi = AreaOfInterest::EGO_FRONT,
            .obstructionRight = -4_m,
            .expectedResult = 99.0_s}));

/*****************************************
 * CHECK GetUrgencyFactorForAcceleration *
 *****************************************/

struct DataFor_GetUrgencyFactorForAcceleration
{
  bool isVisible;
  AreaOfInterest aoi;
  double expectedResult;
};

class MentalCalculations_GetUrgencyFactorForAcceleration : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_GetUrgencyFactorForAcceleration>
{
};

TEST_P(MentalCalculations_GetUrgencyFactorForAcceleration, Check_GetUrgencyFactorForAcceleration)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetUrgencyFactorForAcceleration data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeLeadingVehicle;
  ON_CALL(fakeLeadingVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicle()).WillByDefault(Return(&fakeLeadingVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.isVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(5.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(50.0_m));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  GeometryInformationSCM geometryInformation{};
  geometryInformation.at(data.aoi)->distanceToEndOfLane = 100_m;
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformation));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(150.0_m));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetUrgencyFactorForAcceleration();

  EXPECT_NEAR(result, data.expectedResult, 0.001);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetUrgencyFactorForAcceleration,
    testing::Values(
        DataFor_GetUrgencyFactorForAcceleration{
            .isVisible = false,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = 0.0},
        DataFor_GetUrgencyFactorForAcceleration{
            .isVisible = true,
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = 0.666}));

/*****************************************
 * CHECK GetDistanceForRegulatingVelocity *
 *****************************************/

struct DataFor_GetDistanceForRegulatingVelocity
{
  units::velocity::meters_per_second_t targetVelocity;
  units::length::meter_t expectedResult;
};

class MentalCalculations_GetDistanceForRegulatingVelocity : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_GetDistanceForRegulatingVelocity>
{
};

TEST_P(MentalCalculations_GetDistanceForRegulatingVelocity, Check_GetDistanceForRegulatingVelocity)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetDistanceForRegulatingVelocity data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(100_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(3.0_mps_sq));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;

  std::map<std::string, std::string> powertrainDrag{{"DecelerationFromPowertrainDrag", "10"}};
  vehicleModelParameters.properties = powertrainDrag;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  DriverParameters driverParameters;
  driverParameters.pedalChangeTimeMean = 1.0_s;
  driverParameters.reactionBaseTimeMean = 2.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetDistanceForRegulatingVelocity(data.targetVelocity);

  EXPECT_NEAR(result.value(), data.expectedResult.value(), 0.0000001);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetDistanceForRegulatingVelocity,
    testing::Values(
        DataFor_GetDistanceForRegulatingVelocity{
            .targetVelocity = 50_mps,
            .expectedResult = 2100.0_m},
        DataFor_GetDistanceForRegulatingVelocity{
            .targetVelocity = 110_mps,
            .expectedResult = 665.0_m}));

/***********************************************************
 * CHECK GetDistanceToPointOfNoReturnForBrakingToEndOfExit *
 ***********************************************************/

TEST(MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfExit, Check_GetDistanceToPointOfNoReturnForBrakingToEndOfExit)
{
  MentalCalculationsTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(12_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.0_mps_sq));

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalDeceleration = 4.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  GeometryInformationSCM geometryInformation{};
  geometryInformation.distanceToEndOfNextExit = 100.0_m;
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInformation));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetDistanceToPointOfNoReturnForBrakingToEndOfExit();

  ASSERT_EQ(result, 76.0_m);
}

/***********************************************************
 * CHECK GetDistanceToPointOfNoReturnForBrakingToEndOfLane *
 ***********************************************************/

struct DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane
{
  RelativeLane lane;
  units::length::meter_t expectedResult;
};

class MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane : public ::testing::Test,
                                                                                             public ::testing::WithParamInterface<DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane>
{
};

TEST_P(MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane, Check_GetUrgencyFactorForAcceleration)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane data = GetParam();

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalDeceleration = 4.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  LaneInformationGeometrySCM laneInfo{};
  laneInfo.distanceToEndOfLaneDuringEmergency = 100.0_m;

  GeometryInformationSCM geometryInfo{};
  geometryInfo.Close().distanceToEndOfLaneDuringEmergency = 120_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, GetLaneInformationGeometry(data.lane)).WillByDefault(ReturnRef(laneInfo));
  ON_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetDistanceToPointOfNoReturnForBrakingToEndOfLane(12_mps, data.lane);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane,
    testing::Values(
        DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane{
            .lane = RelativeLane::LEFT,
            .expectedResult = 76.0_m},
        DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane{
            .lane = RelativeLane::RIGHT,
            .expectedResult = 76.0_m},
        DataFor_GetDistanceToPointOfNoReturnForBrakingToEndOfLaneWithRelativeLane{
            .lane = RelativeLane::EGO,
            .expectedResult = 96.0_m}));

/********************************
 * CHECK GetInfluencingDistance *
 ********************************/

struct DataFor_GetInfluencingDistance
{
  bool isBehind;
  units::velocity::meters_per_second_t velocityDelta;
  units::length::meter_t expectedResult;
};

class MentalCalculations_GetInfluencingDistance : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_GetInfluencingDistance>
{
};

TEST_P(MentalCalculations_GetInfluencingDistance, Check_GetInfluencingDistance)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetInfluencingDistance data = GetParam();

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateVelocityDelta(_, _)).WillByDefault(Return(data.velocityDelta));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(4.0_mps_sq));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  std::map<std::string, std::string> powertrainDrag{{"DecelerationFromPowertrainDrag", "10"}};
  vehicleModelParameters.properties = powertrainDrag;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  OwnVehicleInformationScmExtended ego;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));

  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, 0.0_mps, _, _, _, _)).WillByDefault(Return(10.0_m));
  DriverParameters driverParameters;
  driverParameters.carQueuingDistance = 4.0_m;
  driverParameters.carRestartDistance = 5.0_m;
  driverParameters.reactionBaseTimeMean = 2.0_s;
  driverParameters.pedalChangeTimeMean = 0.5_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, BehindEgo()).WillByDefault(Return(data.isBehind));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(data.isBehind));
  ON_CALL(fakeVehicle, GetLongitudinalVelocity()).WillByDefault(Return(5.0_mps));
  ON_CALL(fakeVehicle, RearDistance()).WillByDefault(Return(5.0_m));

  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, _, _, _, _, _, _)).WillByDefault(Return(5.0_m));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength()).WillByDefault(Return(5.0_m));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetInfluencingDistance(&fakeVehicle);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetInfluencingDistance,
    testing::Values(
        DataFor_GetInfluencingDistance{
            .isBehind = true,
            .velocityDelta = -8_mps,
            .expectedResult = 0.0_m},
        DataFor_GetInfluencingDistance{
            .isBehind = false,
            .velocityDelta = -8_mps,
            .expectedResult = 50.5_m},
        DataFor_GetInfluencingDistance{
            .isBehind = false,
            .velocityDelta = 5_mps,
            .expectedResult = 47.5_m}));

/************************
 * CHECK GetInfDistance *
 ************************/

struct DataFor_GetInfDistance
{
  AreaOfInterest aoi;
  AreaOfInterest aoiReference;
  bool isVisible;
  units::length::meter_t expectedResult;
};

class MentalCalculations_GetInfDistance : public ::testing::Test,
                                          public ::testing::WithParamInterface<DataFor_GetInfDistance>
{
};

TEST_P(MentalCalculations_GetInfDistance, Check_GetInfDistance)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_GetInfDistance data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(data.aoi, _, _)).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(data.aoiReference, _, _)).WillByDefault(Return(0));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.isVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInSideArea(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInRearArea(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInSurroundingArea(_, _, _)).WillByDefault(Return(true));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  OwnVehicleInformationScmExtended ego;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ego));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));

  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateVelocityDelta(_, _)).WillByDefault(Return(-8.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocity(data.aoiReference, _)).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeLongitudinalCalculations, CalculateSecureNetDistance(_, _, _, _, _, _, _, _, _)).WillByDefault(Return(10.0_m));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  std::map<std::string, std::string> powertrainDrag{{"DecelerationFromPowertrainDrag", "10"}};
  vehicleModelParameters.properties = powertrainDrag;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  DriverParameters driverParameters;
  driverParameters.carQueuingDistance = 4.0_m;
  driverParameters.carRestartDistance = 10.0_m;
  driverParameters.reactionBaseTimeMean = 2.0_s;
  driverParameters.pedalChangeTimeMean = 0.5_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength(data.aoiReference, _)).WillByDefault(Return(7.0_m));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.GetInfDistance(data.aoi, data.aoiReference);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_GetInfDistance,
    testing::Values(
        DataFor_GetInfDistance{
            .aoi = AreaOfInterest::EGO_FRONT,
            .aoiReference = AreaOfInterest::LEFT_FRONT,
            .isVisible = false,
            .expectedResult = 0.0_m},
        DataFor_GetInfDistance{
            .aoi = AreaOfInterest::EGO_FRONT,
            .aoiReference = AreaOfInterest::LEFT_FRONT,
            .isVisible = true,
            .expectedResult = 51.0_m}));

/***********************************************
 * CHECK CalculatePlatoonPassingVelocityLimits *
 ***********************************************/

struct DataFor_CalculatePlatoonPassingVelocityLimits
{
  units::velocity::meters_per_second_t speedThreshold;
  std::tuple<units::velocity::meters_per_second_t, units::velocity::meters_per_second_t> expectedResult;
};

class MentalCalculations_CalculatePlatoonPassingVelocityLimits : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataFor_CalculatePlatoonPassingVelocityLimits>
{
};

TEST_P(MentalCalculations_CalculatePlatoonPassingVelocityLimits, Check_CalculatePlatoonPassingVelocityLimits)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_CalculatePlatoonPassingVelocityLimits data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(20.0_mps));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  auto result = TEST_HELPER.mentalCalculations.CalculatePlatoonPassingVelocityLimits(data.speedThreshold);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_CalculatePlatoonPassingVelocityLimits,
    testing::Values(
        DataFor_CalculatePlatoonPassingVelocityLimits{
            .speedThreshold = 21.0_mps,
            .expectedResult = {10.0_mps, 9.0_mps}},
        DataFor_CalculatePlatoonPassingVelocityLimits{
            .speedThreshold = 10.0_mps,
            .expectedResult = {18.75_mps, 17.75_mps}}));

/***********************************
 * CHECK TransitionSurroundingData *
 **********************************/

struct DataFor_TransitionSurroundingData
{
  bool isLeftSide;
};

class MentalCalculations_TransitionSurroundingData : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_TransitionSurroundingData>
{
};

TEST_P(MentalCalculations_TransitionSurroundingData, Check_TransitionSurroundingData)
{
  MentalCalculationsTester TEST_HELPER;
  DataFor_TransitionSurroundingData data = GetParam();

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  LaneInformationGeometrySCM laneInfo{};
  laneInfo.distanceToEndOfLaneDuringEmergency = 100.0_m;
  EXPECT_CALL(fakeInfrastructureCharacteristics, GetLaneInformationGeometry(_)).Times(4).WillRepeatedly(ReturnRef(laneInfo));
  EXPECT_CALL(fakeInfrastructureCharacteristics, UpdateLaneInformationGeometry(_)).Times(5).WillRepeatedly(Return(&laneInfo));

  LaneInformationTrafficRulesScmExtended laneTrafficRules{};
  EXPECT_CALL(fakeInfrastructureCharacteristics, GetLaneInformationTrafficRules(_)).Times(4).WillRepeatedly(ReturnRef(laneTrafficRules));
  EXPECT_CALL(fakeInfrastructureCharacteristics, UpdateLaneInformationTrafficRules(Matcher<RelativeLane>(_))).Times(5).WillRepeatedly(Return(&laneTrafficRules));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTime()).WillByDefault(Return(1200_ms));

  EXPECT_CALL(fakeInfrastructureCharacteristics, SetLaneMarkingTypeLeftLast(scm::LaneMarking::Type::None)).Times(4);
  EXPECT_CALL(fakeInfrastructureCharacteristics, SetLaneMarkingTypeRightLast(scm::LaneMarking::Type::None)).Times(4);

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  std::vector<ObjectInformationScmExtended> sideAgents{};
  ON_CALL(fakeMicroscopicCharacteristics, UpdateSideObjectsInformation(_)).WillByDefault(Return(&sideAgents));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  TEST_HELPER.mentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &TEST_HELPER.fakeSurroundingVehicleQueryFactory, &TEST_HELPER.fakeLongitudinalCalculations);
  TEST_HELPER.mentalCalculations.TransitionSurroundingData(data.isLeftSide);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalCalculations_TransitionSurroundingData,
    testing::Values(
        DataFor_TransitionSurroundingData{
            .isLeftSide = true},
        DataFor_TransitionSurroundingData{
            .isLeftSide = false}));