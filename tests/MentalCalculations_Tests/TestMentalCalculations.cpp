/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestMentalCalculations.h"

#include "module/driver/src/MentalCalculationsInterface.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

TestMentalCalculations::TestMentalCalculations(const FeatureExtractorInterface& featureExtractor)
    : MentalCalculations(featureExtractor)
{
}

void TestMentalCalculations::SetLaneChangeBehavior(std::shared_ptr<LaneChangeBehaviorInterface> lcb)
{
  MentalCalculations::_laneChangeBehavior = lcb;
}

double TestMentalCalculations::GetUrgencyFactorForLaneChange() const
{
  return MentalCalculations::GetUrgencyFactorForLaneChange();
}

void TestMentalCalculations::SetTimeLaneChangeNorm(units::time::second_t time)
{
  _timeLaneChangeNorm = time;
}

void TestMentalCalculations::SetTimeLaneChangeUrgent(units::time::second_t time)
{
  _timeLaneChangeUrgent = time;
}

void TestMentalCalculations::UpdateRangeBetweenNormUrgent()
{
  _timeRangeBetweenNormUrgent = _timeLaneChangeNorm - _timeLaneChangeUrgent;
}

units::length::meter_t TestMentalCalculations::TestGetMinDistance(const SurroundingVehicleInterface* vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t vGap) const
{
  return MentalCalculations::GetMinDistance(vehicle, perspective, vGap);
}

units::length::meter_t TestMentalCalculations::TestGetEqDistance(const SurroundingVehicleInterface* vehicle) const
{
  return MentalCalculations::GetEqDistance(vehicle);
}

ObjectInformationScmExtended TestMentalCalculations::CreateSurroundingObject(double multiplier) const
{
  ObjectInformationScmExtended surroundingObject;
  surroundingObject.acceleration = 1._mps_sq * multiplier;
  surroundingObject.brakeLightsActive = true;
  surroundingObject.collision = true;
  surroundingObject.distanceToLaneBoundaryLeft = 1._m * multiplier;
  surroundingObject.distanceToLaneBoundaryRight = 1._m * multiplier;
  surroundingObject.exist = true;
  surroundingObject.gap = 1._s * multiplier;
  surroundingObject.gapDot = 1. * multiplier;
  surroundingObject.heading = 1. * multiplier;
  surroundingObject.height = 1._m * multiplier;
  surroundingObject.id = 1 * static_cast<int>(multiplier);
  surroundingObject.indicatorState = scm::LightState::Indicator::Warn;
  surroundingObject.isStatic = true;
  surroundingObject.lateralPositionInLane = 1._m * multiplier;
  surroundingObject.lateralVelocity = 1._mps * multiplier;
  surroundingObject.length = 1._m * multiplier;
  surroundingObject.longitudinalVelocity = 1._mps * multiplier;
  surroundingObject.obstruction = ObstructionScm(1._m * multiplier, 1._m * multiplier, 1._m * multiplier);
  surroundingObject.opticalRequest = {InformationRequest()};
  surroundingObject.relativeLateralDistance = 1._m * multiplier;
  surroundingObject.relativeLongitudinalDistance = 1._m * multiplier;
  surroundingObject.reliabilityMap = {{FieldOfViewAssignment::FOVEA, 1000_ms * static_cast<int>(multiplier)},
                                      {FieldOfViewAssignment::UFOV, 1000_ms * static_cast<int>(multiplier)},
                                      {FieldOfViewAssignment::PERIPHERY, 1000_ms * static_cast<int>(multiplier)}};
  surroundingObject.tauDot = 1. * multiplier;
  surroundingObject.ttc = 1._s * multiplier;
  surroundingObject.absoluteVelocity = 1._mps * multiplier;
  surroundingObject.width = 1._m * multiplier;

  return surroundingObject;
}

LaneInformationGeometrySCM TestMentalCalculations::CreateGeometryLane(double multiplier) const
{
  LaneInformationGeometrySCM geometryObject;

  geometryObject.curvature = 1.0_i_m * multiplier;
  geometryObject.curvatureInPreviewDistance = 1._i_m * multiplier;
  geometryObject.distanceToEndOfLane = 1._m * multiplier;
  geometryObject.exists = true;
  geometryObject.laneType = scm::LaneType::Driving;
  geometryObject.width = 1.0_m * multiplier;

  return geometryObject;
}

LaneInformationTrafficRulesScmExtended TestMentalCalculations::CreateTrafficRuleLane(double multiplier) const
{
  LaneInformationTrafficRulesScmExtended trafficRuleObject;
  scm::LaneMarking::Entity laneMarking;
  scm::CommonTrafficSign::Entity trafficSign;

  laneMarking.relativeStartDistance = 1._m * multiplier;
  trafficSign.relativeDistance = 1._m * multiplier;

  trafficRuleObject.laneMarkingsLeft = {laneMarking};
  trafficRuleObject.laneMarkingsRight = {laneMarking};
  trafficRuleObject.trafficSigns = {trafficSign};
  trafficRuleObject.speedLimit = 1._mps * multiplier;
  trafficRuleObject.previousRelevantSpeedLimit = 1._mps * multiplier;

  return trafficRuleObject;
}

units::length::meter_t TestMentalCalculations::TestGetRelativeNetDistanceEq(AreaOfInterest aoi)
{
  return MentalCalculations::GetRelativeNetDistanceEq(aoi);
}

double TestMentalCalculations::TestGetUrgencyBetweenMinAndEq(units::length::meter_t relativeNetDistanceNetBetweenVehicles, AreaOfInterest aoi) const
{
  return MentalCalculations::GetUrgencyBetweenMinAndEq(relativeNetDistanceNetBetweenVehicles, aoi);
}

double TestMentalCalculations::TestCalculateUrgencyFactor(double riskObstacle, double& riskEndOfLane) const
{
  return MentalCalculations::CalculateUrgencyFactor(riskObstacle, riskEndOfLane);
}

void TestMentalCalculations::TestEvaluateAOIsAndDistances(AreaOfInterest& aoi, AreaOfInterest& aoiFar, units::length::meter_t& distanceToObstacle, units::length::meter_t& distanceToObstacleFar) const
{
  return MentalCalculations::EvaluateAOIsAndDistances(aoi, aoiFar, distanceToObstacle, distanceToObstacleFar);
}

units::length::meter_t TestMentalCalculations::TestAdjustDesiredFollowingDistanceDueToCooperation(units::length::meter_t desiredDistance)
{
  return MentalCalculations::AdjustDesiredFollowingDistanceDueToCooperation(desiredDistance);
}

units::acceleration::meters_per_second_squared_t TestMentalCalculations::TestCalculateAccelerationToDefuse(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t accelerationLimit)
{
  return MentalCalculations::CalculateAccelerationToDefuse(aoi, accelerationLimit);
}
//*****************************************************
//---------------TestMentalCalculations3---------------
//*****************************************************

TestMentalCalculations3::TestMentalCalculations3(const FeatureExtractorInterface& featureExtractor)
    : MentalCalculations(featureExtractor)
{
}

units::length::meter_t TestMentalCalculations3::TestGetRelativeNetDistance(SurroundingLane lane)
{
  return MentalCalculations::GetRelativeNetDistance(lane);
}

units::time::second_t TestMentalCalculations3::TestEstimatedTimeToLeaveLane()
{
  return MentalCalculations::EstimatedTimeToLeaveLane();
}

//*****************************************************
//---------------TestMentalCalculations4---------------
//*****************************************************

TestMentalCalculations4::TestMentalCalculations4(const FeatureExtractorInterface& featureExtractor)
    : MentalCalculations(featureExtractor)
{
}

void TestMentalCalculations4::SetPersistentCooperativeBehavior(bool coop)
{
  _persistentCooperativeBehavior = coop;
}

double TestMentalCalculations4::TestCalculateUrgencyFactorSituationNoCollision(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const
{
  return MentalCalculations::CalculateUrgencyFactorSituationNoCollision(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane);
}

//*****************************************************
//---------------TestMentalCalculations6---------------
//*****************************************************

TestMentalCalculations6::TestMentalCalculations6(const FeatureExtractorInterface& featureExtractor)
    : MentalCalculations(featureExtractor)
{
}

bool TestMentalCalculations6::TestPassablePlatoonPresent(const Side side) const
{
  return MentalCalculations::PassablePlatoonPresent(side);
}

double TestMentalCalculations6::TestEvaluateUrgencyFactorMesoscopicSituation(units::length::meter_t& distanceToObstacle, double& riskObstacle, double& riskEndOfLane) const
{
  return MentalCalculations::EvaluateUrgencyFactorMesoscopicSituation(distanceToObstacle, riskObstacle, riskEndOfLane);
}

//*****************************************************
//---------------TestMentalCalculations7--------------
//*****************************************************

TestMentalCalculations7::TestMentalCalculations7(const FeatureExtractorInterface& featureExtractor)
    : MentalCalculations(featureExtractor)
{
}

double TestMentalCalculations7::TestCalculateUrgencyFactorSituation(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const
{
  return MentalCalculations::CalculateUrgencyFactorSituation(distanceToObstacle, distanceToObstacleFar, riskObstacle, riskEndOfLane);
}