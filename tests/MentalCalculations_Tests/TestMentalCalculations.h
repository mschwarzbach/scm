/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../MentalModel_Tests/TestMentalModel.h"
#include "ScmDriver.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "module/driver/src/MentalCalculations.h"
#include "module/driver/src/MentalModelInterface.h"
#include "module/driver/src/ScmDefinitions.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

class TestMentalModel;
class TestMentalModel2;
class TestMentalModel5;

class TestMentalCalculations : public MentalCalculations
{
public:
  TestMentalCalculations(const FeatureExtractorInterface& featureExtractor);

  void SetLaneChangeBehavior(std::shared_ptr<LaneChangeBehaviorInterface>);

  MOCK_CONST_METHOD2(GetEqDistance, units::length::meter_t(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD4(GetMinDistance, units::length::meter_t(AreaOfInterest, MinThwPerspective, AreaOfInterest, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD2(GetInfDistance, units::length::meter_t(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD2(CalculateVelocityAtCurrentParameters, double(AreaOfInterest aoi, double ttc));

  MOCK_CONST_METHOD1(GetSideTtc, units::time::second_t(AreaOfInterest aoi));
  MOCK_CONST_METHOD2(GetDistanceForRegulatingVelocity, double(double acceleration, double targetvelocity));
  MOCK_METHOD0(DistanceToEndOfExitDestination, double());
  MOCK_CONST_METHOD0(EgoBelowJamSpeed, bool());
  MOCK_CONST_METHOD2(DetermineTimeToSwervePast, int(AreaOfInterest, int));
  MOCK_CONST_METHOD2(GetLaneMarkingAtDistance, scm::LaneMarking::Entity(double, Side));

  double TestGetPassingTime(AreaOfInterest aoi, double deltaVelocity);
  double TestAnticipatedTimeToLaneCrossing(AreaOfInterest aoi, int sideAoiIndex);
  units::length::meter_t TestGetRelativeNetDistanceEq(AreaOfInterest aoi);
  double TestGetUrgencyBetweenMinAndEq(units::length::meter_t relativeNetDistanceNetBetweenVehicles, AreaOfInterest aoi) const;
  double TestCalculateUrgencyFactor(double riskObstacle, double& riskEndOfLane) const;
  void TestEvaluateAOIsAndDistances(AreaOfInterest& aoi, AreaOfInterest& aoiFar, units::length::meter_t& distanceToObstacle, units::length::meter_t& distanceToObstacleFar) const;

  units::length::meter_t TestAdjustDesiredFollowingDistanceDueToCooperation(units::length::meter_t desiredDistance);
  units::acceleration::meters_per_second_squared_t TestCalculateAccelerationToDefuse(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t accelerationLimit);
  units::length::meter_t TestGetMinDistance(const SurroundingVehicleInterface* vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t vGap) const;
  units::length::meter_t TestGetEqDistance(const SurroundingVehicleInterface* vehicle) const;
  double GetUrgencyFactorForLaneChange() const;

  void SetTimeLaneChangeNorm(units::time::second_t time);
  void SetTimeLaneChangeUrgent(units::time::second_t time);

  void UpdateRangeBetweenNormUrgent();

  ObjectInformationScmExtended CreateSurroundingObject(double multiplier = 1.) const;
  LaneInformationGeometrySCM CreateGeometryLane(double multiplier = 1.) const;
  LaneInformationTrafficRulesScmExtended CreateTrafficRuleLane(double multiplier = 1.) const;
};

//*****************************************************
//---------------TestMentalCalculations3---------------
//*****************************************************

class TestMentalCalculations3 : public MentalCalculations
{
public:
  TestMentalCalculations3(const FeatureExtractorInterface& featureExtractor);

  MOCK_CONST_METHOD2(GetUrgencyBetweenMinAndEq, double(units::length::meter_t netDistanceBetweenVehicles, AreaOfInterest aoi));
  MOCK_CONST_METHOD2(IsApproaching, bool(AreaOfInterest aoi, AreaOfInterest aoiFar));

  units::length::meter_t TestGetRelativeNetDistance(SurroundingLane lane);
  void SetAgentGeneralInfluencingDistanceToEndOfLane(double dist);

  units::time::second_t TestEstimatedTimeToLeaveLane();
};

//*****************************************************
//---------------TestMentalCalculations4---------------
//*****************************************************

class TestMentalCalculations4 : public MentalCalculations
{
public:
  TestMentalCalculations4(const FeatureExtractorInterface& featureExtractor);

  MOCK_CONST_METHOD1(GetRelativeNetDistance, units::length::meter_t(SurroundingLane targetLane));
  MOCK_CONST_METHOD1(IsCurrentSituationAndNotHighRisk, bool(Situation situation));
  MOCK_CONST_METHOD5(CalculateRiskObstacle, double(AreaOfInterest aoi, AreaOfInterest aoiFar, units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle));

  void SetPersistentCooperativeBehavior(bool coop);
  double TestCalculateUrgencyFactorSituationNoCollision(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const;
};
//*****************************************************
//---------------TestMentalCalculations6---------------
//*****************************************************

class TestMentalCalculations6 : public MentalCalculations
{
public:
  TestMentalCalculations6(const FeatureExtractorInterface& featureExtractor);
  bool TestPassablePlatoonPresent(const Side side) const;
  double TestEvaluateUrgencyFactorMesoscopicSituation(units::length::meter_t& distanceToObstacle, double& riskObstacle, double& riskEndOfLane) const;

  MOCK_CONST_METHOD1(CalculateHalfSpeedometerDistanceForAoi, units::length::meter_t(AreaOfInterest aoi));
  MOCK_CONST_METHOD1(CheckVelocityDifferenceIsPlatoon, bool(Side));
  MOCK_CONST_METHOD1(CheckRelativeDistanceIsPlatoon, bool(Side));
  MOCK_CONST_METHOD1(AnticipatedLaneChangerPresent, bool(Side));
  MOCK_CONST_METHOD1(PlatoonBelowJamSpeed, bool(Side));
  MOCK_CONST_METHOD1(PossiblePlatoonCarsVisible, bool(Side));
  MOCK_CONST_METHOD2(CalculateUrgencyFactor, double(double, double&));
  MOCK_CONST_METHOD5(CalculateRiskObstacle, double(AreaOfInterest aoi, AreaOfInterest aoiFar, units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle));
  MOCK_CONST_METHOD1(CalculateRiskObstacle, double(units::length::meter_t distanceToObstacle));
};

//*****************************************************
//---------------TestMentalCalculations7---------------
//*****************************************************

class TestMentalCalculations7 : public MentalCalculations
{
public:
  TestMentalCalculations7(const FeatureExtractorInterface& featureExtractor);

  MOCK_CONST_METHOD0(EgoBelowJamSpeed, bool());
  MOCK_CONST_METHOD1(PassablePlatoonPresent, bool(Side));
  MOCK_CONST_METHOD1(CalculatePlatoonPassingVelocityLimits, std::tuple<units::velocity::meters_per_second_t, units::velocity::meters_per_second_t>(units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD1(CalculatePlatoonPassingSuperevelationFactors, std::tuple<double, double>(units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD1(GetTtcForUrgencyFactorSituationCollision, units::time::second_t(Situation currentSituation));
  MOCK_CONST_METHOD0(EstimatedTimeToLeaveLane, units::time::second_t());
  MOCK_CONST_METHOD1(CalculateUrgencyFactorSituationCollision, double(units::time::second_t));
  MOCK_CONST_METHOD4(CalculateUrgencyFactorSituationNoCollision, double(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane));

  double TestCalculateUrgencyFactorSituation(units::length::meter_t distanceToObstacle, units::length::meter_t distanceToObstacleFar, double riskObstacle, double riskEndOfLane) const;
};
