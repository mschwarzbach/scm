/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gtest/gtest.h>

#include <memory>
#include <utility>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeScmComponents.h"
#include "Fakes/FakeGazeControl.h"
#include "Fakes/FakeMicroscopicCharacteristics.h"
#include "Fakes/FakeStochastics.h"
#include "ParametersHaf.h"
#include "module/driver/src/ScmDefinitions.h"
#include "module/driver/src/ScmDriver.h"

using ::testing::NiceMock;
using ::testing::Return;
struct ScmDriverTester
{
  class ScmDriverUnderTest : scm::ScmDriver
  {
  public:
    template <typename... Args>
    ScmDriverUnderTest(Args&&... args)
        : scm::ScmDriver{std::forward<Args>(args)...} {};

    using scm::ScmDriver::_externalControlState;
    using scm::ScmDriver::_externalControlStateLastStep;
    using scm::ScmDriver::_scmComponents;
    using scm::ScmDriver::HasExternalControlStateChanged;
    using scm::ScmDriver::SetExternalControlFlags;

    void SET_IS_EGO_VIEW_IN_INSTRUMENT_CLUSTER(bool value)
    {
      _isEgoViewInInstrumentCluster = value;
    }
  };

  ScmDriverTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeScmComponents{},
        scmdriver(&fakeStochastics,
                  100_ms,
                  nullptr)
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeScmComponents> fakeScmComponents;

  ScmDriverUnderTest scmdriver;
};

/****************************
 * CHECK SetExternalControl *
 ****************************/

struct DataFor_SetExternalControlFlagsHaf
{
  bool isHafActive;
  bool isHafDeactive;
  bool isEgoViewInInstrumentCluster;
  bool result_externalControlStateLateralActive;
  bool result_externalControlStateLongitudinalActive;
};
class ScmDriver_SetExternalControl : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_SetExternalControlFlagsHaf>
{
};

TEST_P(ScmDriver_SetExternalControl, SetExternalControlFlagsWithHAFParameters)
{
  DataFor_SetExternalControlFlagsHaf data = GetParam();
  ScmDriverTester TEST_HELPER;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeGazeControl> fakeGazeControl;

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.fovea = AreaOfInterest::EGO_FRONT;

  auto components = std::make_unique<testing::NiceMock<FakeScmComponents>>();

  ON_CALL(fakeGazeControl, GetHafSignalProcessed()).WillByDefault(Return(true));
  ON_CALL(*components, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  ON_CALL(*components, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmdriver._scmComponents = std::move(components);

  TEST_HELPER.scmdriver.SET_IS_EGO_VIEW_IN_INSTRUMENT_CLUSTER(data.isEgoViewInInstrumentCluster);
  const HafParameters haf{data.isHafActive, data.isHafDeactive};
  TEST_HELPER.scmdriver.SetExternalControlFlags(haf);

  ASSERT_EQ(TEST_HELPER.scmdriver._externalControlState.lateralActive, data.result_externalControlStateLateralActive);
  ASSERT_EQ(TEST_HELPER.scmdriver._externalControlState.longitudinalActive, data.result_externalControlStateLongitudinalActive);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmDriver_SetExternalControl,
    testing::Values(
        DataFor_SetExternalControlFlagsHaf{
            .isHafActive = true,
            .isHafDeactive = false,
            .isEgoViewInInstrumentCluster = false,
            .result_externalControlStateLateralActive = true,
            .result_externalControlStateLongitudinalActive = true},
        DataFor_SetExternalControlFlagsHaf{
            .isHafActive = false,
            .isHafDeactive = true,
            .isEgoViewInInstrumentCluster = true,
            .result_externalControlStateLateralActive = false,
            .result_externalControlStateLongitudinalActive = false},
        DataFor_SetExternalControlFlagsHaf{
            .isHafActive = false,
            .isHafDeactive = false,
            .isEgoViewInInstrumentCluster = false,
            .result_externalControlStateLateralActive = false,
            .result_externalControlStateLongitudinalActive = false}));

/****************************************
 * CHECK HasExternalControlStateChanged *
 ****************************************/

struct DataFor_HasExternalControlStateChanged
{
  bool externalControlStateLateralActive;
  bool externalControlStateLastStepLateralActive;
  bool result;
};

class Scm_Driver_HasExternalControlStateChanged : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_HasExternalControlStateChanged>
{
};

TEST_P(Scm_Driver_HasExternalControlStateChanged, HasExternalControlStateChanged)
{
  DataFor_HasExternalControlStateChanged data = GetParam();
  ScmDriverTester TEST_HELPER;

  auto components = std::make_unique<testing::NiceMock<FakeScmComponents>>();
  ON_CALL(*components, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmdriver._scmComponents = std::move(components);

  TEST_HELPER.scmdriver._externalControlState.lateralActive = data.externalControlStateLateralActive;
  TEST_HELPER.scmdriver._externalControlStateLastStep.lateralActive = data.externalControlStateLastStepLateralActive;

  bool result = TEST_HELPER.scmdriver.HasExternalControlStateChanged();

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    Scm_Driver_HasExternalControlStateChanged,
    testing::Values(
        DataFor_HasExternalControlStateChanged{
            .externalControlStateLateralActive = true,
            .externalControlStateLastStepLateralActive = false,
            .result = true},
        DataFor_HasExternalControlStateChanged{
            .externalControlStateLateralActive = false,
            .externalControlStateLastStepLateralActive = true,
            .result = true},
        DataFor_HasExternalControlStateChanged{
            .externalControlStateLateralActive = true,
            .externalControlStateLastStepLateralActive = true,
            .result = false}));