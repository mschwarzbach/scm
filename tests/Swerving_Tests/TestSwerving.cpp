/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSwerving.h"
#include "MentalModel.h"
#include "ScmCommons.h"
#include "Swerving/Swerving.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/***********************************
 * CHECK DetermineTimeToSwervePast *
 ***********************************/

TEST(Swerving_DetermineTimeToSwervePast, ExecutedWithSideAoi_Throws)
{
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);

  EXPECT_THROW({
    try
    {
      swerving.DetermineTimeToSwervePast(AreaOfInterest::LEFT_SIDE);
    }
    catch (const std::runtime_error& e)
    {
      EXPECT_STREQ("MentalCalculations - DetermineTimeToSwervePast: Not a front area!", e.what());
      throw;
    }
  },
               std::runtime_error);
}

TEST(Swerving_DetermineTimeToSwervePast, GivenLongitudinalValues_CalculatesCorrectTime)
{
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);

  ON_CALL(mentalModel, GetLongitudinalObstruction(_, _)).WillByDefault(Return(ObstructionLongitudinal{12_m, 0_m, 0_m}));
  ON_CALL(mentalModel, GetLongitudinalVelocityDelta(_, _)).WillByDefault(Return(-24_mps));

  ASSERT_EQ(swerving.DetermineTimeToSwervePast(AreaOfInterest::EGO_FRONT), 0.5_s);
}

/**************************************
 * CHECK SetSwervingEndTime Exception *
 **************************************/

TEST(Swerving_SetSwervingEndTime, Swerving_CheckException_SetSwervingEndTime)
{
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);

  ON_CALL(mentalModel, GetTime()).WillByDefault(Return(2_ms));

  // Call test expecting an exception because time <= mentalModel->GetTime()
  EXPECT_THROW({
    try
    {
      swerving.SetSwervingEndTime(1_ms);
    }
    catch (const std::runtime_error& e)
    {
      EXPECT_STREQ("Swerving::SetSwervingEndTime - End time must be in the future!", e.what());
      throw;
    }
  },
               std::runtime_error);
}

/****************************
 * CHECK SetSwervingEndTime *
 ****************************/

TEST(Swerving_SetSwervingEndTime, Swerving_Check_SetSwervingEndTime)
{
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);
  ON_CALL(mentalModel, GetTime()).WillByDefault(Return(2_ms));

  // Call test
  swerving.SetSwervingEndTime(3_ms);

  ASSERT_EQ(swerving.GetSwervingEndTime(), 3_ms);
}

/************************************************
 * CHECK DetermineLateralDistanceToEvadeComfort *
 ***********************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_DetermineLateralDistanceToEvadeComfort
{
  ObstructionScm obstruction;
  units::velocity::meters_per_second_t egoVelocity;
  units::length::meter_t result_LateralDistanceToEvade;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineLateralDistanceToEvadeComfort& obj)
  {
    return os
           << "obstruction (ObstructionScm): "
           << " | result_LateralDistanceToEvade (double): " << obj.result_LateralDistanceToEvade;
  }
};

class Swerving_DetermineLateralDistanceToEvadeComfort : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_DetermineLateralDistanceToEvadeComfort>
{
};

TEST_P(Swerving_DetermineLateralDistanceToEvadeComfort, Swerving_CheckFunction_DetermineLateralDistanceToEvadeComfort)
{
  // Create required classes for test
  DataFor_DetermineLateralDistanceToEvadeComfort data = GetParam();
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);
  ON_CALL(mentalModel, GetObstruction(_, _)).WillByDefault(Return(data.obstruction));
  ON_CALL(mentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.egoVelocity));

  auto result = swerving.DetermineLateralDistanceToEvadeComfort(AreaOfInterest::EGO_FRONT, Side::Left);

  EXPECT_EQ(result, data.result_LateralDistanceToEvade);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    Swerving_DetermineLateralDistanceToEvadeComfort,
    testing::Values(
        DataFor_DetermineLateralDistanceToEvadeComfort{
            .obstruction = ObstructionScm{0.0_m, 0.0_m, 0.0_m},
            .egoVelocity = 2.0_mps,
            .result_LateralDistanceToEvade = 0.2_m},
        DataFor_DetermineLateralDistanceToEvadeComfort{
            .obstruction = ObstructionScm{0.0_m, 0.0_m, 0.0_m},
            .egoVelocity = 20.0_mps,
            .result_LateralDistanceToEvade = 0.576_m},
        DataFor_DetermineLateralDistanceToEvadeComfort{
            .obstruction = ObstructionScm{1.0_m, 0.0_m, 0.0_m},
            .egoVelocity = 20.0_mps,
            .result_LateralDistanceToEvade = 1.576_m},
        DataFor_DetermineLateralDistanceToEvadeComfort{
            .obstruction = ObstructionScm{0.5_m, 0.0_m, 0.0_m},
            .egoVelocity = 40.0_mps,
            .result_LateralDistanceToEvade = 1.3_m},
        DataFor_DetermineLateralDistanceToEvadeComfort{
            .obstruction = ObstructionScm{1.0_m, 0.0_m, 0.0_m},
            .egoVelocity = 40.0_mps,
            .result_LateralDistanceToEvade = 1.8_m}));

/*****************************************
 * CHECK DetermineLateralDistanceToEvade *
 ****************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_DetermineLateralDistanceToEvade
{
  ObstructionScm obstruction;
  units::length::meter_t result_LateralDistanceToEvade;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineLateralDistanceToEvade& obj)
  {
    return os
           << "obstruction (ObstructionScm): "
           << " | result_LateralDistanceToEvade (double): " << obj.result_LateralDistanceToEvade;
  }
};

class Swerving_DetermineLateralDistanceToEvade : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_DetermineLateralDistanceToEvade>
{
};

TEST_P(Swerving_DetermineLateralDistanceToEvade, Swerving_CheckFunction_DetermineLateralDistanceToEvade)
{
  // Create required classes for test
  DataFor_DetermineLateralDistanceToEvade data = GetParam();
  NiceMock<FakeMentalModel> mentalModel;
  Swerving swerving(mentalModel);

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  DriverParameters driverParameters;
  driverParameters.lateralSafetyDistanceForEvading = 0.4_m;
  ON_CALL(mentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, GetLateralObstruction).WillByDefault(Return(data.obstruction));
  ON_CALL(mentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&vehicle));
  auto result = swerving.DetermineLateralDistanceToEvade(AreaOfInterest::EGO_FRONT, Side::Left, true, 0);

  EXPECT_EQ(result, data.result_LateralDistanceToEvade);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    Swerving_DetermineLateralDistanceToEvade,
    testing::Values(
        DataFor_DetermineLateralDistanceToEvade{
            .obstruction = ObstructionScm{0.0_m, 0.0_m, 0.0_m},
            .result_LateralDistanceToEvade = 0.4_m},
        DataFor_DetermineLateralDistanceToEvade{
            .obstruction = ObstructionScm{-0.8_m, 0.0_m, 0.0_m},
            .result_LateralDistanceToEvade = 0.4_m},
        DataFor_DetermineLateralDistanceToEvade{
            .obstruction = ObstructionScm{0.3_m, 0.0_m, 5.0_m},
            .result_LateralDistanceToEvade = 0.7_m},
        DataFor_DetermineLateralDistanceToEvade{
            .obstruction = ObstructionScm{0.123_m, -0.5_m, 0.0_m},
            .result_LateralDistanceToEvade = 0.523_m},
        DataFor_DetermineLateralDistanceToEvade{
            .obstruction = ObstructionScm{999.0_m, 0.0_m, 0.0_m},
            .result_LateralDistanceToEvade = 999.4_m}));

/********************************
 * DetermineSwervingIntensities *
 ********************************/

// struct DataFor_DetermineSwervingIntensities
//{
//     double input_swervingTimeLeft;
//     double input_swervingTimeRight;
//     bool input_isUrgentSwerving;
//     double result_IntensityLaneKeeping;
//     double result_IntensitySwervingLeft;
//     double result_IntensitySwervingRight;

//    /// \brief This stream will be shown in case the test fails
//    friend std::ostream &operator<<(std::ostream &os, const DataFor_DetermineSwervingIntensities &rhs) {
//        os << "input_swervingTimeLeft: " << rhs.input_swervingTimeLeft
//           << " input_swervingTimeRight: " << rhs.input_swervingTimeRight
//           << " input_isUrgentSwerving: " << rhs.input_isUrgentSwerving
//           << " result_IntensityLaneKeeping: " << rhs.result_IntensityLaneKeeping
//           << " result_IntensitySwervingLeft: " << rhs.result_IntensitySwervingLeft
//           << " result_IntensitySwervingRight: " << rhs.result_IntensitySwervingRight;
//        return os;
//    }
//  };

// class ActionManager_DetermineSwervingIntensities :
//                             public ::testing::Test,
//                             public ::testing::WithParamInterface<DataFor_DetermineSwervingIntensities>
//{
// };

// TEST_P(ActionManager_DetermineSwervingIntensities, CheckFunction_DetermineSwervingIntensities)
//{
//     // Get resources for testing
//     DataFor_DetermineSwervingIntensities data = GetParam();

//    std::unique_ptr<FeatureExtractorInterface> featureExtractor = std::make_unique<FeatureExtractor>();
//    FakeMentalCalculations mentalCalculations;
//    FakeMentalModel mentalModel;
//    Swerving swerving(mentalModel);
//    std::unique_ptr<TestActionManager> stubActionManager = std::make_unique<TestActionManager>(mentalModel, *featureExtractor.get(), mentalCalculations);

//    // Set up test
//    std::map<LateralActionState, double> swervingTimes = {{LateralActionState::URGENT_SWERVING_LEFT, -999.},
//                                                         {LateralActionState::URGENT_SWERVING_RIGHT, -999.},
//                                                         {LateralActionState::COMFORT_SWERVING_LEFT, -999.},
//                                                         {LateralActionState::COMFORT_SWERVING_RIGHT, -999.}};

//    stubActionManager->SetLateralActionPatternIntensity(LateralActionState::LANE_KEEPING, -999.);
//    stubActionManager->SetLateralActionPatternIntensity(LateralActionState::URGENT_SWERVING_LEFT, -999.);
//    stubActionManager->SetLateralActionPatternIntensity(LateralActionState::URGENT_SWERVING_RIGHT, -999.);
//    stubActionManager->SetLateralActionPatternIntensity(LateralActionState::COMFORT_SWERVING_LEFT, -999.);
//    stubActionManager->SetLateralActionPatternIntensity(LateralActionState::COMFORT_SWERVING_RIGHT, -999.);

//    if(data.input_isUrgentSwerving)
//    {
//        swervingTimes.at(LateralActionState::URGENT_SWERVING_LEFT) = data.input_swervingTimeLeft;
//        swervingTimes.at(LateralActionState::URGENT_SWERVING_RIGHT) = data.input_swervingTimeRight;
//    }
//    else
//    {
//        swervingTimes.at(LateralActionState::COMFORT_SWERVING_LEFT) = data.input_swervingTimeLeft;
//        swervingTimes.at(LateralActionState::COMFORT_SWERVING_RIGHT) = data.input_swervingTimeRight;
//    }

////    ON_CALL(*mentalCalculations, GetSwervingTimes(_, _)).WillByDefault(Return(swervingTimes));

//    // Run test
//    auto actionPattern = stubActionManager->GetActionPatternIntensities();
//    swerving.DetermineSwervingIntensities(AreaOfInterest::RIGHT_FRONT, data.input_isUrgentSwerving, actionPattern);

//    // Evaluate results
//    // TODO USE ASSERT_DOUBLE_EQ
//    ASSERT_TRUE(stubActionManager->GetActionPatternIntensities().at(LateralActionState::LANE_KEEPING) -
//                data.result_IntensityLaneKeeping <= std::numeric_limits<double>::epsilon());
//    ASSERT_TRUE(stubActionManager->GetActionPatternIntensities().at(LateralActionState::URGENT_SWERVING_LEFT) -
//                data.result_IntensitySwervingLeft <= std::numeric_limits<double>::epsilon());
//    ASSERT_TRUE(stubActionManager->GetActionPatternIntensities().at(LateralActionState::URGENT_SWERVING_RIGHT) -
//                data.result_IntensitySwervingRight <= std::numeric_limits<double>::epsilon());
//};

// INSTANTIATE_TEST_CASE_P(Default, ActionManager_DetermineSwervingIntensities,testing::Values
//(
//     /*
//         int input_testcase;  // 1: Only swerving left, 2: Only swerving right, 3: Swerving left and right, 4: No swerving
//     */
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 1.3,
//         .input_swervingTimeRight =  0.,
//         .input_isUrgentSwerving = false,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 1.,
//         .result_IntensitySwervingRight = 0.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 0,
//         .input_swervingTimeRight =  1.3,
//         .input_isUrgentSwerving = false,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 0.,
//         .result_IntensitySwervingRight = 1.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 2.,
//         .input_swervingTimeRight =  4.,
//         .input_isUrgentSwerving = false,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 2. / 3.,
//         .result_IntensitySwervingRight = 1. / 3.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 0.,
//         .input_swervingTimeRight =  0.,
//         .input_isUrgentSwerving = false,
//         .result_IntensityLaneKeeping = 1.,
//         .result_IntensitySwervingLeft = 0.,
//         .result_IntensitySwervingRight = 0.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 1.3,
//         .input_swervingTimeRight =  0.,
//         .input_isUrgentSwerving = true,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 1.,
//         .result_IntensitySwervingRight = 0.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 0,
//         .input_swervingTimeRight =  1.3,
//         .input_isUrgentSwerving = true,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 0.,
//         .result_IntensitySwervingRight = 1.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 2.,
//         .input_swervingTimeRight =  4.,
//         .input_isUrgentSwerving = true,
//         .result_IntensityLaneKeeping = 0.,
//         .result_IntensitySwervingLeft = 2. / 3.,
//         .result_IntensitySwervingRight = 1. / 3.
//     },
//     DataFor_DetermineSwervingIntensities
//     {
//         .input_swervingTimeLeft = 0.,
//         .input_swervingTimeRight =  0.,
//         .input_isUrgentSwerving = true,
//         .result_IntensityLaneKeeping = 1.,
//         .result_IntensitySwervingLeft = 0.,
//         .result_IntensitySwervingRight = 0.
//     }
//)
//);

///**********************************************
// * CHECK DetermineSwervingReturnParameters    *
// *********************************************/

///// \brief Data table for definition of individual test cases
// struct DataFor_DetermineSwervingReturnParameters
//{
//     double swervingReturnDistance;
//     bool isVehicleVisible;
//     AreaOfInterest swervingAoi;
//     double ttc;
//     double gap;
//     double relativeNetDistance;

//    double result_widthLaneChange;
//    bool result_isVehicleVisible;
//    double result_ttc;
//    double result_thw;
//    double result_relativeNetDistance;

//    /// \brief This stream will be shown in case the test fails
//    friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineSwervingReturnParameters& obj)
//    {
//        return os
//        << "  swervingReturnDistance (double): " << obj.swervingReturnDistance
//        << "| isVehicleVisible (bool): "         << ScmCommons::BooleanToString(obj.isVehicleVisible)
//        << "| swervingAoi (AreaOfInterest): "    << ScmCommons::AreaOfInterestToString(obj.swervingAoi)
//        << "| ttc (double): "                    << obj.ttc
//        << "| gap (double): "                    << obj.gap;
//    }
//};

// class Swerving_DetermineSwervingReturnParameters :
//                             public ::testing::Test,
//                             public ::testing::WithParamInterface<DataFor_DetermineSwervingReturnParameters>
//{
// };

// TEST_P(Swerving_DetermineSwervingReturnParameters, ActionImplementation_DetermineSwervingReturnParameters)
//{
//     DataFor_DetermineSwervingReturnParameters data = GetParam();
//     NiceMock<FakeMentalModel> fakeMentalModel;
//     Swerving swerving(fakeMentalModel);

//    ON_CALL(*stubActionImplementation, DetermineSwervingReturnDistance()).WillByDefault(Return(data.swervingReturnDistance));
//    ON_CALL(*stubActionImplementation, DetermineAoiForSwervingReturnParameters(_)).WillByDefault(Return(data.swervingAoi));

//    ON_CALL(fakeMentalModel, GetIsVehicleVisible(_, -1)).WillByDefault(Return(data.isVehicleVisible));

//    ON_CALL(fakeMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(data.relativeNetDistance));
//    ON_CALL(fakeMentalModel, GetGap(_, _)).WillByDefault(Return(data.gap));
//    ON_CALL(fakeMentalModel, GetTtc(_, _)).WillByDefault(Return(data.ttc));

//}

///**********************************************************/
//// The test data (must be defined below test)
///**********************************************************/

// INSTANTIATE_TEST_SUITE_P(Default, Swerving_DetermineSwervingReturnParameters,testing::Values
//(
//  DataFor_DetermineSwervingReturnParameters{
//      .swervingReturnDistance = 3.0,
//      .isVehicleVisible = true,
//      .swervingAoi = AreaOfInterest::EGO_FRONT,
//      .ttc = 5.0,
//      .gap = 1.0,
//      .relativeNetDistance = 12,
//      .result_widthLaneChange = 3.0,
//      .result_ttc = 5.0,
//      .result_thw = 1.0,
//      .result_relativeNetDistance = 12
//  },
//  DataFor_DetermineSwervingReturnParameters{
//      .swervingReturnDistance = 3.0,
//      .isVehicleVisible = false,
//      .swervingAoi = AreaOfInterest::EGO_FRONT,
//      .ttc = 5.0,
//      .gap = 1.0,
//      .relativeNetDistance = 12,
//      .result_widthLaneChange = 3.0,
//      .result_ttc = INFINITY,
//      .result_thw = INFINITY,
//      .result_relativeNetDistance = INFINITY
//  }
//)
//);

/*****************************************
 * CHECK DetermineSwervingReturnDistance *
 *****************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_DetermineSwervingReturnDistance
{
  bool input_hasLaneChangedWhileSwerving;
  bool input_isSwervingLeft;
  bool input_isStartLaneSafe;
  bool input_isSideLaneSafe;
  units::time::second_t input_ttcStartLaneFront;
  units::time::second_t input_ttcSideLaneFront;
  bool input_isSideVehiclePresentInSideLane;
  bool input_hasLateralOffset;
  units::length::meter_t result_distanceToReturn;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineSwervingReturnDistance& obj)
  {
    return os
           << "  input_hasLaneChangedWhileSwerving (bool): " << ScmCommons::BooleanToString(obj.input_hasLaneChangedWhileSwerving)
           << "| input_isSwervingLeft (bool): " << ScmCommons::BooleanToString(obj.input_isSwervingLeft)
           << "| input_isStartLaneSafe (bool): " << ScmCommons::BooleanToString(obj.input_isStartLaneSafe)
           << "| input_isSideLaneSafe (bool): " << ScmCommons::BooleanToString(obj.input_isSideLaneSafe)
           << "| input_ttcStartLaneFront (double): " << obj.input_ttcStartLaneFront
           << "| input_ttcSideLaneFront (double): " << obj.input_ttcSideLaneFront
           << "| input_isSideVehiclePresentInSideLane (bool): " << ScmCommons::BooleanToString(obj.input_isSideVehiclePresentInSideLane)
           << "| input_hasLateralOffset (bool): " << ScmCommons::BooleanToString(obj.input_hasLateralOffset)
           << "| result_distanceToReturn (double): " << obj.result_distanceToReturn;
  }
};

class Swerving_DetermineSwervingReturnDistance : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_DetermineSwervingReturnDistance>
{
};

TEST_P(Swerving_DetermineSwervingReturnDistance, Swerving_DetermineSwervingReturnDistance)
{
  // Get resources for testing
  DataFor_DetermineSwervingReturnDistance data = GetParam();

  FakeFeatureExtractor featureExtractor;
  FakeMentalModel mentalModel;
  FakeMentalCalculations mentalCalculations;
  Swerving swerving(mentalModel);

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  DriverParameters driverParameters;
  MicroscopicCharacteristics micro;
  micro.Initialize(0.0_mps);
  micro.Initialize(0.0_mps);

  InfrastructureCharacteristics infrastructure;
  // default situation conditions - 3 motorway lanes and swerving started in middle lane
  infrastructure.UpdateGeometryInformation()->Close().width = 3.75_m;
  infrastructure.UpdateGeometryInformation()->Left().width = 3.75_m;
  infrastructure.UpdateGeometryInformation()->Right().width = 3.75_m;

  ON_CALL(mentalModel, GetMicroscopicData()).WillByDefault(Return(&micro));
  ON_CALL(mentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructure));
  ON_CALL(mentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(units::velocity::meters_per_second_t(20_kph)));
  ON_CALL(mentalModel, GetVehicleWidth()).WillByDefault(Return(2.0_m));
  ON_CALL(mentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(units::velocity::meters_per_second_t(60_kph)));
  ON_CALL(mentalModel, GetFormRescueLaneVelocityThreshold()).WillByDefault(Return(units::velocity::meters_per_second_t(10_kph)));
  ON_CALL(mentalModel, GetLaneExistence(_, _)).WillByDefault(Return(true));

  swerving.SetLaneIdAtStartOfSwervingManeuver(-2);

  // apply lateral offset
  if (data.input_hasLateralOffset)
  {
    ON_CALL(mentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillByDefault(Return(0.5_m));
    ON_CALL(mentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(true));
  }
  else
  {
    ON_CALL(mentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillByDefault(Return(0.0_m));
  }

  // apply test conditions on default values (what was the driver been doing?)
  if (data.input_isSwervingLeft)
  {
    ON_CALL(mentalModel, GetLateralAction()).WillByDefault(Return(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)));

    if (data.input_hasLaneChangedWhileSwerving)
    {
      ON_CALL(mentalModel, GetLaneId()).WillByDefault(Return(-1));             // driver reached left lane while swerving
      ON_CALL(mentalModel, GetLateralPosition()).WillByDefault(Return(-1.5_m));  // lateral position on lane -1
    }
    else  // lane has not changed
    {
      ON_CALL(mentalModel, GetLaneId()).WillByDefault(Return(-2));            // driver never left center lane
      ON_CALL(mentalModel, GetLateralPosition()).WillByDefault(Return(1.5_m));  // lateral position on lane -2
    }
  }
  else  // swerving right
  {
    ON_CALL(mentalModel, GetLateralAction()).WillByDefault(Return(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)));
    if (data.input_hasLaneChangedWhileSwerving)
    {
      ON_CALL(mentalModel, GetLaneId()).WillByDefault(Return(-3));            // driver reached right lane while swerving
      ON_CALL(mentalModel, GetLateralPosition()).WillByDefault(Return(1.5_m));  // lateral position on lane -3
    }
    else  // lane has not changed
    {
      ON_CALL(mentalModel, GetLaneId()).WillByDefault(Return(-2));             // driver never left center lane
      ON_CALL(mentalModel, GetLateralPosition()).WillByDefault(Return(-1.5_m));  // lateral position on lane -2
    }
  }

  // apply test conditions on expected assessment logic of the DetermineSwervingReturnDistance
  if (!data.input_hasLaneChangedWhileSwerving)
  {
    NiceMock<FakeSurroundingVehicle> vehicle;
    ON_CALL(mentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&vehicle));

    ON_CALL(mentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(data.input_ttcStartLaneFront));
    ON_CALL(featureExtractor, IsCollisionCourseDetected(&vehicle)).WillByDefault(Return(!data.input_isStartLaneSafe));
    ON_CALL(featureExtractor, HasSideLaneNoCollisionCourses(_)).WillByDefault(Return(data.input_isSideLaneSafe));

    ON_CALL(mentalModel, GetTtc(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(data.input_ttcSideLaneFront));
    ON_CALL(mentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(data.input_isSideVehiclePresentInSideLane));
    ON_CALL(mentalModel, GetTtc(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(data.input_ttcSideLaneFront));
    ON_CALL(mentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, _)).WillByDefault(Return(data.input_isSideVehiclePresentInSideLane));
  }
  else  // driver has changed lanes while swerving
  {
    NiceMock<FakeSurroundingVehicle> vehicle;
    ON_CALL(mentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&vehicle));
    ON_CALL(featureExtractor, IsCollisionCourseDetected(&vehicle)).WillByDefault(Return(!data.input_isSideLaneSafe));
    ON_CALL(mentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(data.input_ttcSideLaneFront));

    NiceMock<FakeSurroundingVehicle> vehicleRight;
    ON_CALL(mentalModel, GetVehicle(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(&vehicleRight));
    ON_CALL(featureExtractor, IsCollisionCourseDetected(&vehicleRight)).WillByDefault(Return(!data.input_isSideLaneSafe));

    NiceMock<FakeSurroundingVehicle> vehicleLeft;
    ON_CALL(mentalModel, GetVehicle(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(&vehicleLeft));
    ON_CALL(featureExtractor, IsCollisionCourseDetected(&vehicleLeft)).WillByDefault(Return(!data.input_isStartLaneSafe));

    ON_CALL(mentalModel, GetTtc(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(data.input_ttcStartLaneFront));
    ON_CALL(mentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, _)).WillByDefault(Return(data.input_isSideVehiclePresentInSideLane));
    ON_CALL(mentalModel, GetTtc(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(data.input_ttcStartLaneFront));
    ON_CALL(mentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(data.input_isSideVehiclePresentInSideLane));
  }

  // Run Test
  auto result = swerving.DetermineSwervingReturnDistance(featureExtractor, mentalCalculations);

  // Evaluate Results
  ASSERT_EQ(data.result_distanceToReturn, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_CASE_P(
    Default,
    Swerving_DetermineSwervingReturnDistance,
    testing::Values(
        // Use lateralOffset = (3.75/2 - 2/2) * 0.5 = 0.4375 for DetermineSwervingReturnDistance
        // lane has not changed and swerving left
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = (3.75_m / 2) + (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = true,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = (3.75_m / 2) + (3.75_m / 2 - 1.5_m)},
        // lane has not changed and swerving right
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -(3.75_m / 2) - (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = true,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = false,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -(3.75_m / 2) - (3.75_m / 2 - 1.5_m)},
        // lane has changed and swerving left
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -(3.75_m / 2) - (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -(3.75_m / 2) - (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = true,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = true,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = 1.5_m},
        // lane has changed and swerving right
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = (3.75_m / 2) + (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = true,
            .input_isSideLaneSafe = true,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 99._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = (3.75_m / 2) + (3.75_m / 2 - 1.5_m)},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = true,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m},
        DataFor_DetermineSwervingReturnDistance{
            .input_hasLaneChangedWhileSwerving = true,
            .input_isSwervingLeft = false,
            .input_isStartLaneSafe = false,
            .input_isSideLaneSafe = false,
            .input_ttcStartLaneFront = 10._s,
            .input_ttcSideLaneFront = 99._s,
            .input_isSideVehiclePresentInSideLane = false,
            .input_hasLateralOffset = false,
            .result_distanceToReturn = -1.5_m}  //     // lateral offset is set to 0.5
        //     DataFor_DetermineSwervingReturnDistance{false, true, false, true, 99., 99., false, true, (3.75 / 2) + (3.75 / 2 - 1.5) + 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{false, true, false, false, 10., 99., false, true, (3.75 / 2) + (3.75 / 2 - 1.5) + 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{false, false, false, true, 99., 99., false, true, -(3.75 / 2) - (3.75 / 2 - 1.5) - 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{false, false, false, false, 10., 99., false, true, -(3.75 / 2) - (3.75 / 2 - 1.5) - 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{true, true, true, false, 99., 99., false, true, -(3.75 / 2) - (3.75 / 2 - 1.5) - 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{true, true, false, false, 99., 99., false, true, -(3.75 / 2) - (3.75 / 2 - 1.5) - 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{true, false, true, false, 99., 99., false, true, (3.75 / 2) + (3.75 / 2 - 1.5) + 0.4375},
        //     DataFor_DetermineSwervingReturnDistance{true, false, false, false, 99., 99., false, true, (3.75 / 2) + (3.75 / 2 - 1.5) + 0.4375}
        ));

/*************************************************
 * CHECK DetermineAoiForSwervingReturnParameters *
 ************************************************/

///// \brief Data table for definition of individual test cases
// struct DataFor_DetermineAoiForSwervingReturnParameters
//{
//     double widthLaneChange;
//     double distanceToBoundaryLeft;
//     double distanceToBoundaryRight;
//     bool isVehicleVisible;
//     AreaOfInterest result_aoi;

//    /// \brief This stream will be shown in case the test fails
//    friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineAoiForSwervingReturnParameters& obj)
//    {
//        return os
//        << "| isVehicleVisible (bool): "         << ScmCommons::BooleanToString(obj.isVehicleVisible);
//    }
//};

// class ActionImplementation_DetermineAoiForSwervingReturnParameters :
//                             public ::testing::Test,
//                             public ::testing::WithParamInterface<DataFor_DetermineAoiForSwervingReturnParameters>
//{
// };

// TEST_P(ActionImplementation_DetermineAoiForSwervingReturnParameters, ActionImplementation_DetermineAoiForSwervingReturnParameters)
//{
//     DataFor_DetermineAoiForSwervingReturnParameters data = GetParam();
//     TestResourceManager resourceManager;
//     ScmDependencies* scmDependencies = resourceManager._scmDependencies.get();
//     auto tmpFeatureExtractor = resourceManager.CreateFeatureExtractor();
//     auto stubFeatureExtractor = tmpFeatureExtractor.get();
//     auto mentalCalculations = resourceManager.CreateMentalCalculations(*tmpFeatureExtractor);
//     auto stubMentalModel = resourceManager.CreateMentalModel3(*stubFeatureExtractor, *mentalCalculations, *scmDependencies);
//     mentalCalculations->Initialize(stubMentalModel.get(), resourceManager.fakeStochastics.get());
//     auto stubActionImplementation = resourceManager.CreateActionImplementation3(stubMentalModel.get(), *stubFeatureExtractor, *mentalCalculations);

//    tmpFeatureExtractor->SetMentalModel(*stubMentalModel);
//    tmpFeatureExtractor->SetMentalCalculations(*mentalCalculations);

//    ON_CALL(*stubMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT, -1)).WillByDefault(Return(data.isVehicleVisible));
//    ON_CALL(*stubMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT, -1)).WillByDefault(Return(data.isVehicleVisible));

//    ON_CALL(*stubMentalModel, GetVehicleWidth()).WillByDefault(Return(2.3));
//    ON_CALL(*stubMentalModel, GetDistanceToBoundaryLeft()).WillByDefault(Return(data.distanceToBoundaryLeft));
//    ON_CALL(*stubMentalModel, GetDistanceToBoundaryRight()).WillByDefault(Return(data.distanceToBoundaryRight));

//    ASSERT_EQ(aoi, data.result_aoi);
//}

///**********************************************************/
//// The test data (must be defined below test)
///**********************************************************/

// INSTANTIATE_TEST_SUITE_P(Default, ActionImplementation_DetermineAoiForSwervingReturnParameters,testing::Values
//(
//  DataFor_DetermineAoiForSwervingReturnParameters{
//      .widthLaneChange = 3.0,
//      .distanceToBoundaryLeft = 1.4,
//      .distanceToBoundaryRight = 1.4,
//      .isVehicleVisible = true,
//      .result_aoi = AreaOfInterest::LEFT_FRONT
//  },
//  DataFor_DetermineAoiForSwervingReturnParameters{
//      .widthLaneChange = 3.0,
//      .distanceToBoundaryLeft = 2.0,
//      .distanceToBoundaryRight = 1.4,
//      .isVehicleVisible = false,
//      .result_aoi = AreaOfInterest::EGO_FRONT
//  },
//  DataFor_DetermineAoiForSwervingReturnParameters{
//      .widthLaneChange = -3.0,
//      .distanceToBoundaryLeft = 2.0,
//      .distanceToBoundaryRight = 1.4,
//      .isVehicleVisible = true,
//      .result_aoi = AreaOfInterest::RIGHT_FRONT
//  },
//  DataFor_DetermineAoiForSwervingReturnParameters{
//      .widthLaneChange = -3.0,
//      .distanceToBoundaryLeft = 2.0,
//      .distanceToBoundaryRight = 1.4,
//      .isVehicleVisible = false,
//      .result_aoi = AreaOfInterest::EGO_FRONT
//  }
//)
//);

///**************************
// * CHECK GetSwervingTimes *
// **************************/

///// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
// struct DataFor_GetSwervingTimes
//{
//     AreaOfInterest input_aoi;
//     bool input_isUrgentSwerving;
//     double input_lateralDistanceToEvadeLeft;
//     double input_lateralDistanceToEvadeRight;
//     bool input_IsSwervingSafeLeft;
//     bool input_IsSwervingSafeRight;
//     double input_Ttc;
//     double result_TimeSwervingLeft;
//     double result_TimeSwervingRight;

//    /// \brief This stream will be shown in case the test fails
//    friend std::ostream& operator<<(std::ostream& os, const DataFor_GetSwervingTimes& obj)
//    {
//        return os
//            << "input_aoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoi)
//            << " | input_isUrgentSwerving (bool): " << ScmCommons::BooleanToString(obj.input_isUrgentSwerving)
//            << " | input_lateralDistanceToEvadeLeft (double): " << obj.input_lateralDistanceToEvadeLeft
//            << " | input_lateralDistanceToEvadeRight (double): " << obj.input_lateralDistanceToEvadeRight
//            << " | input_IsSwervingSafeLeft (bool): " << ScmCommons::BooleanToString(obj.input_IsSwervingSafeLeft)
//            << " | input_IsSwervingSafeRight (bool): " << ScmCommons::BooleanToString(obj.input_IsSwervingSafeRight)
//            << " | input_Ttc (double): " << obj.input_Ttc
//            << " | result_TimeSwervingLeft (double): " << obj.result_TimeSwervingLeft
//            << " | result_TimeSwervingRight (double): " << obj.result_TimeSwervingRight;
//     }
//};

// class MentalCalculations_GetSwervingTimes : public ::testing::Test,
//                             public ::testing::WithParamInterface<DataFor_GetSwervingTimes>
//{
// };

// TEST_P(MentalCalculations_GetSwervingTimes, MentalCalculations_CheckFunction_GetSwervingTimes)
//{
//     // Create required classes for test
//     DataFor_GetSwervingTimes data = GetParam();

//    TestResourceManager resourceManager;
//    ScmDependencies* scmDependencies = resourceManager._scmDependencies.get();
//    auto tmpFeatureExtractor = resourceManager.CreateFeatureExtractor();
//    auto stubFeatureExtractor = tmpFeatureExtractor.get();
//    auto mentalCalculations = resourceManager.CreateMentalCalculations3(*tmpFeatureExtractor);
//    auto stubMentalModel = resourceManager.CreateMentalModel2(*stubFeatureExtractor, *mentalCalculations, *scmDependencies);
//    mentalCalculations->Initialize(stubMentalModel.get(), resourceManager.fakeStochastics.get());
//    auto tmpMicroscopicCharacteristics = resourceManager.CreateMicroscopicCharacteristics();
//    auto stubMicroscopicCharacteristics = tmpMicroscopicCharacteristics.get();

//    stubMentalModel->SetJamVelocity(15.);
//    stubMentalModel->UpdateMentalModel(std::move(tmpMicroscopicCharacteristics), nullptr, nullptr);
//    stubFeatureExtractor->SetMentalModel(*stubMentalModel);

//    // Set up test
//    ON_CALL(*stubMentalModel, GetMicroscopicData()).WillByDefault(Return(stubMicroscopicCharacteristics));
//    stubMicroscopicCharacteristics->UpdateObjectInformation(data.input_aoi)->obstruction.isOverlapping = true;

//    DriverParameters driverParameters;
//    driverParameters.comfortLateralAcceleration = 0.8;
//    driverParameters.maximumLateralAcceleration = 1.0;
//    ParametersScmSignal parametersScmSignal(driverParameters);
//    scmDependencies->UpdateParametersScmSignal(parametersScmSignal);

//    ON_CALL(*mentalCalculations, DetermineLateralDistanceToEvade(_, Side::Left, data.input_isUrgentSwerving, _)).WillByDefault(Return(data.input_lateralDistanceToEvadeLeft));
//    ON_CALL(*mentalCalculations, DetermineLateralDistanceToEvade(_, Side::Right, data.input_isUrgentSwerving, _)).WillByDefault(Return(data.input_lateralDistanceToEvadeRight));

//    ON_CALL(*stubFeatureExtractor, IsSwervingSafe(_,Side::Left,_)).WillByDefault(Return(data.input_IsSwervingSafeLeft));
//    ON_CALL(*stubFeatureExtractor, IsSwervingSafe(_,Side::Right,_)).WillByDefault(Return(data.input_IsSwervingSafeRight));

//    stubMicroscopicCharacteristics->UpdateObjectInformation(data.input_aoi, 0)->ttc = data.input_Ttc;

//    // Call test
//    std::map<LateralActionState, double> result = mentalCalculations->GetSwervingTimes(data.input_aoi,
//                                                                                           data.input_isUrgentSwerving);

//    // Evaluate results
//    if (data.input_isUrgentSwerving)
//    {
//        ASSERT_NEAR(result.at(LateralActionState::URGENT_SWERVING_LEFT), data.result_TimeSwervingLeft,
//                    std::numeric_limits<double>::epsilon());
//        ASSERT_NEAR(result.at(LateralActionState::URGENT_SWERVING_RIGHT), data.result_TimeSwervingRight,
//                    std::numeric_limits<double>::epsilon());
//    }
//    else
//    {
//        ASSERT_NEAR(result.at(LateralActionState::COMFORT_SWERVING_LEFT), data.result_TimeSwervingLeft,
//                    std::numeric_limits<double>::epsilon());
//        ASSERT_NEAR(result.at(LateralActionState::COMFORT_SWERVING_RIGHT), data.result_TimeSwervingRight,
//                    std::numeric_limits<double>::epsilon());
//    }
//}

///**********************************************************/
//// The test data (must be defined below test)
///**********************************************************/

// INSTANTIATE_TEST_CASE_P(Default, MentalCalculations_GetSwervingTimes,testing::Values
//(
//    /*
//         AreaOfInterest input_aoi;
//         bool input_isUrgentSwerving;
//         double input_relativeLateralDistance;
//         bool input_IsEvadingSafe;
//         double input_Ttc;
//         double result_TimeSwervingLeft;
//         double result_TimeSwervingRight;
//    */

//    // IsUrgentSwerving (change ttc and IsSwervingSafe)
//    DataFor_GetSwervingTimes{AreaOfInterest::RIGHT_FRONT,  true, 2.5,  2.,  true,  true, 1.5,         -999.,         -999.},  // 0 No evading necessary (ttc)
//    DataFor_GetSwervingTimes{AreaOfInterest::RIGHT_FRONT,  true, 2.5,  3.,  true,  true, 2.3, std::sqrt(5.),         -999.},  // 1 Evading only left necessary and possible
//    DataFor_GetSwervingTimes{AreaOfInterest::RIGHT_FRONT,  true, 2.5, 2.5,  true,  true, 3.0, std::sqrt(5.), std::sqrt(5.)},  // 2 Evading left and right necessary and possible
//    DataFor_GetSwervingTimes{AreaOfInterest::RIGHT_FRONT,  true, 2.5,  3., false,  true, 2.3,         -999.,         -999.},  // 3 Evading left necessary but not possible
//    DataFor_GetSwervingTimes{AreaOfInterest::RIGHT_FRONT,  true, 2.5, 2.5,  true, false, 3.0, std::sqrt(5.),         -999.},  // 4 Evading left and right necessary but only left is possible
//    // Change aoi
//    DataFor_GetSwervingTimes{ AreaOfInterest::LEFT_FRONT,  true, 4.5,  2.,  true,  true, 2.5,  -999.,    2.},                 // 5 No evading necessary (ttc)
//    DataFor_GetSwervingTimes{  AreaOfInterest::EGO_FRONT,  true, 4.5,  3.,  true, false, 3.5,     3., -999.},                 // 6 Directly behind target, evading both sides possible
//    // IsComfortSwerving (change of acc)
//    DataFor_GetSwervingTimes{  AreaOfInterest::EGO_FRONT, false, 1.5, 1.0,  true,  true, 1.8,           -999., std::sqrt(2.5)},  // 7 No evading necessary (ttc)
//    DataFor_GetSwervingTimes{  AreaOfInterest::EGO_FRONT, false, 1.5, 1.0,  true,  true, 3.0, std::sqrt(3.75), std::sqrt(2.5)}   // 8 Evading left and right necessary and possible
//)
//);

/**********************************
 * CHECK EstimateLaneChangeLength *
 **********************************/

///// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
// struct DataFor_EstimateLaneChangeLength
//{
//     double input_AbsoluteVelocity;
//     double input_RelativeNetDistance;
//     double input_DistanceToEndOfLane;
//     double result_LaneChangeLength;

//    /// \brief This stream will be shown in case the test fails
//    friend std::ostream& operator<<(std::ostream& os, const DataFor_EstimateLaneChangeLength& obj)
//     {
//       return os
//         << "input_AbsoluteVelocity (double): " << obj.input_AbsoluteVelocity
//         << " | input_RelativeNetDistance (double): " << obj.input_RelativeNetDistance
//         << " | input_DistanceToEndOfLane (double): " << obj.input_DistanceToEndOfLane
//         << " | result_LaneChangeLength (double): " << obj.result_LaneChangeLength;
//     }
//};

// class MentalCalculations_EstimateLaneChangeLength : public ::testing::Test,
//                             public ::testing::WithParamInterface<DataFor_EstimateLaneChangeLength>
//{
// };

// TEST_P(MentalCalculations_EstimateLaneChangeLength, MentalCalculations_CheckFunction_EstimateLaneChangeLength)
//{
//     // Create required classes for test
//     DataFor_EstimateLaneChangeLength data = GetParam();

//    TestResourceManager resourceManager;
//    ScmDependencies* scmDependencies = resourceManager._scmDependencies.get();

//    auto tmpFeatureExtractor = resourceManager.CreateFeatureExtractor();
//    auto stubFeatureExtractor = tmpFeatureExtractor.get();
//    auto mentalCalculations = resourceManager.CreateMentalCalculations(*tmpFeatureExtractor);
//    auto stubMentalModel = resourceManager.CreateMentalModel(*stubFeatureExtractor, *mentalCalculations, *scmDependencies);
//    mentalCalculations->Initialize(stubMentalModel.get(), resourceManager.fakeStochastics.get());
//    auto tmpMicroscopicCharacteristics = resourceManager.CreateMicroscopicCharacteristics();
//    auto stubMicroscopicCharacteristics = tmpMicroscopicCharacteristics.get();
//    auto tmpInfrastructureCharacteristics = resourceManager.CreateInfrastructureCharacteristics();
//    auto stubInfrastructure = tmpInfrastructureCharacteristics.get();

//    stubMentalModel->UpdateMentalModel(std::move(tmpMicroscopicCharacteristics), nullptr, std::move(tmpInfrastructureCharacteristics));
//    stubFeatureExtractor->SetMentalModel(*stubMentalModel);

//    // Set up test
//    ON_CALL(*stubMentalModel, GetMicroscopicData).WillByDefault(Return(stubMicroscopicCharacteristics));

//    DriverParameters driverParameters;
//    driverParameters.comfortLateralAcceleration = 1.;
//    ParametersScmSignal parametersScmSignal(driverParameters);
//    scmDependencies->UpdateParametersScmSignal(parametersScmSignal);

//    stubInfrastructure->UpdateGeometryInformation()->laneEgo.distanceToEndOfLane = data.input_DistanceToEndOfLane;
//    stubMentalModel->GetInfrastructureCharacteristics()->UpdateGeometryInformation()->laneEgo.width = 3.75;
//    ON_CALL(*stubMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(data.input_RelativeNetDistance));
//    ON_CALL(*stubMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.input_AbsoluteVelocity));

//    // Call test
//    const double result = mentalCalculations->EstimateLaneChangeLength();

//    // Evaluate results
//    ASSERT_TRUE(std::fabs(result - data.result_LaneChangeLength) <= std::numeric_limits<double>::epsilon());
//}

///**********************************************************/
//// The test data (must be defined below test)
///**********************************************************/

// INSTANTIATE_TEST_CASE_P(Default, MentalCalculations_EstimateLaneChangeLength,testing::Values
//(
//    /*
//         double input_AbsoluteVelocity;
//         double input_RelativeNetDistance;
//         double input_DistanceToEndOfLane;
//         double result_LaneChangeLength;
//    */

//    DataFor_EstimateLaneChangeLength{50., 100., 30., 232.65121477552492 * 1.2},
//    DataFor_EstimateLaneChangeLength{ 0., 100., 30.,                30.},
//    DataFor_EstimateLaneChangeLength{ 0.,  20., 30.,                20.},
//    DataFor_EstimateLaneChangeLength{ 0.,  10., 10.,                15.}
//)
//);