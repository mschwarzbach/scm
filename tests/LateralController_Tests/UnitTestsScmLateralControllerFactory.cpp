/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/lateralController/include/ScmLateralControllerFactory.h"
#include "Fakes/FakeStochastics.h"

using ::testing::NotNull;

/****************
 * CHECK Create *
 ****************/

TEST(ScmLateralControllerFactory_Create, Check_Create)
{
  auto result = scm::lateral_controller::factory::Create();

  ASSERT_THAT(result, NotNull());
}