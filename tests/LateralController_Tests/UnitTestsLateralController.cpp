/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../include/signal/lateralControllerInput.h"
#include "../module/lateralController/src/LateralController.h"
#include "ScmCommons.h"
struct LateralControllerTester
{
  class LateralControllerUnderTest : scm::LateralController
  {
  public:
    template <typename... Args>
    LateralControllerUnderTest(Args&&... args)
        : LateralController{std::forward<Args>(args)...} {};

    using scm::LateralController::Trigger;
  };

  LateralControllerTester()
      : lateralController()
  {
  }
  LateralControllerUnderTest lateralController;
};

/*****************
 * CHECK Trigger *
 *****************/

struct DataFor_AlgorithmLateralDriverImplementation_Trigger
{
  units::velocity::meters_per_second_t input_LongitudinalVelocity;
  units::length::meter_t input_LateralDeviation;
  units::angle::radian_t input_HeadingError;
  units::angle::radian_t input_LastSteeringWheelAngle;
  std::vector<units::curvature::inverse_meter_t> input_CurvatureSegmentsNear;
  std::vector<units::curvature::inverse_meter_t> input_CurvatureSegmentsFar;
  units::curvature::inverse_meter_t input_KappaRoad;
  units::curvature::inverse_meter_t input_KappaManeuver;
  units::angle::degree_t result_SteeringWheelAngle;
};

class LateralDriverTrigger : public ::testing::Test,
                             public ::testing::WithParamInterface<DataFor_AlgorithmLateralDriverImplementation_Trigger>
{
};

TEST_P(LateralDriverTrigger, LateralDriver_CheckTriggerFunction)
{
  // Get Resources for testing
  LateralControllerTester TEST_HELPER;
  DataFor_AlgorithmLateralDriverImplementation_Trigger data = GetParam();

  scm::LateralController controller;

  scm::signal::LateralControllerInput signal;

  signal.velocity = data.input_LongitudinalVelocity;
  signal.lateralDeviation = data.input_LateralDeviation;
  signal.headingError = data.input_HeadingError;
  signal.steeringWheelAngle = data.input_LastSteeringWheelAngle;
  signal.curvatureOfSegmentsToNearPoint = data.input_CurvatureSegmentsNear;
  signal.curvatureOfSegmentsToFarPoint = data.input_CurvatureSegmentsFar;
  signal.kappaRoad = data.input_KappaRoad;
  signal.kappaManoeuvre = data.input_KappaManeuver;
  signal.gainHeadingError = 10_Hz;
  signal.gainLateralDeviation = 20_rad_per_s_sq;
  signal.steeringRatio = 10.0;
  signal.frontAxleMaxSteering = 2 * M_PI;
  signal.wheelBase = 3.0_m;

  auto result = controller.Trigger(signal, 0_s);

  // Results must be within 1% of analytical results (since excact matches can't be guaranteed)
  bool resultLegit = units::math::abs(data.result_SteeringWheelAngle - result.desiredSteeringWheelAngle) <= .01 * units::math::abs(data.result_SteeringWheelAngle);

  // Evaluate result
  ASSERT_TRUE(resultLegit);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/
INSTANTIATE_TEST_CASE_P(
    Default,
    LateralDriverTrigger,
    testing::Values(
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            .input_LongitudinalVelocity = 50._mps,
            .input_LateralDeviation = 0._m,
            .input_HeadingError = 0._rad,
            .input_LastSteeringWheelAngle = 0._rad,
            .input_CurvatureSegmentsNear = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_CurvatureSegmentsFar = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_KappaRoad = 0._i_m,
            .input_KappaManeuver = 0._i_m,
            .result_SteeringWheelAngle = 000.000000_deg},  // Driving straight
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            .input_LongitudinalVelocity = 50._mps,
            .input_LateralDeviation = 1._m,
            .input_HeadingError = 0._rad,
            .input_LastSteeringWheelAngle = 0._rad,
            .input_CurvatureSegmentsNear = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_CurvatureSegmentsFar = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_KappaRoad = 0._i_m,
            .input_KappaManeuver = 0._i_m,
            .result_SteeringWheelAngle = 013.750987_deg},  // Lateral deviation from trajectory
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            .input_LongitudinalVelocity = 50._mps,
            .input_LateralDeviation = 0._m,
            .input_HeadingError = 1._rad,
            .input_LastSteeringWheelAngle = 300._rad,
            .input_CurvatureSegmentsNear = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_CurvatureSegmentsFar = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_KappaRoad = 0._i_m,
            .input_KappaManeuver = 0._i_m,
            .result_SteeringWheelAngle = 360.000000_deg},  // Lateral deviation from trajectory with non central steering wheel capped to 320°/s
                                                           // (actual 343.77467)
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            .input_LongitudinalVelocity = 50._mps,
            .input_LateralDeviation = 0._m,
            .input_HeadingError = 2._rad,
            .input_LastSteeringWheelAngle = 350._rad,
            .input_CurvatureSegmentsNear = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_CurvatureSegmentsFar = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_KappaRoad = 0._i_m,
            .input_KappaManeuver = 0._i_m,
            .result_SteeringWheelAngle = 360.000000_deg},  // Curvature of trajectory, 687.54935° capped at 360°
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            .input_LongitudinalVelocity = 50._mps,
            .input_LateralDeviation = 2._m,
            .input_HeadingError = 1._rad,
            .input_LastSteeringWheelAngle = 350._rad,
            .input_CurvatureSegmentsNear = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_CurvatureSegmentsFar = {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            .input_KappaRoad = 0._i_m,
            .input_KappaManeuver = 0._i_m,
            .result_SteeringWheelAngle = 360.000000_deg}));
// Total steering wheel angle, 371.27665° capped at 360°
