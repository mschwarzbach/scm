/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock-nice-strict.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <ostream>
#include <vector>

#include "../Fakes/FakeActionImplementation.h"
#include "../Fakes/FakeActionManager.h"
#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeLaneChangeBehavior.h"
#include "../Fakes/FakeLaneChangeTrajectoryCalculations.h"
#include "../Fakes/FakeLaneKeepingCalculations.h"
#include "../Fakes/FakeLongitudinalCalculations.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeSideLaneSafety.h"
#include "../Fakes/FakeSideLaneSafetyQuery.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "../Fakes/FakeSwerving.h"
#include "../Fakes/FakeTrajectoryPlanner.h"
#include "../Fakes/FakeZipMerging.h"
#include "AoiAssigner.h"
#include "LateralAction.h"
#include "ScmCommons.h"
#include "ScmComponents.h"
#include "ScmDriver.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "module/driver/src/ActionManager.h"
#include "module/driver/src/ScmDefinitions.h"
#include "module/parameterParser/src/TrafficRules/TrafficRules.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct ActionManagerTester
{
  class ActionManagerUnderTest : ActionManager
  {
  public:
    template <typename... Args>
    ActionManagerUnderTest(Args&&... args)
        : ActionManager{std::forward<Args>(args)...} {};

    using ActionManager::CalculateCriticalActionIntensitiesParameters;
    using ActionManager::CalculateLateralActionIntensityValuesForEndingLane;
    using ActionManager::CalculateTTCActionLimits;
    using ActionManager::CheckIntensitiesForChangingLeft;
    using ActionManager::CheckIntensitiesForChangingRight;
    using ActionManager::ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement;
    using ActionManager::ChooseLateralActionIntensitiesForCollisionCase;
    using ActionManager::ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement;
    using ActionManager::DetermineIsLaneChangeModifiedOrAborted;
    using ActionManager::DetermineLongitudinalActionState;
    using ActionManager::EvaluateActionIntensities;
    using ActionManager::GenerateCriticalActionIntensityVector;
    using ActionManager::GenerateIntensityAndSampleLateralAction;
    using ActionManager::GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations;
    using ActionManager::GenerateLaneChangeIntensitiesForZipMerging;
    using ActionManager::GenerateLateralActionPatternIntensities;
    using ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger;
    using ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft;
    using ActionManager::GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight;
    using ActionManager::GenerateLateralActionPatternIntensitiesForDetectedLaneChanger;
    using ActionManager::GenerateLateralActionPatternIntensitiesForFollowingDriving;
    using ActionManager::GenerateLateralActionPatternIntensitiesForFreeDriving;
    using ActionManager::GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane;
    using ActionManager::GenerateLateralActionPatternIntensitiesForSideCollisionRisk;
    using ActionManager::GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane;
    using ActionManager::GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane;
    using ActionManager::GenerateLateralActionPatternIntensitiesForTooClose;
    using ActionManager::GetActionPatternIntensities;
    using ActionManager::GetAvailableLanesForCollisionCase;
    using ActionManager::IsBraking;
    using ActionManager::IsLaneKeepingMostAppropriate;
    using ActionManager::SetIntensitiesWhileLaneChangeBlocked;
    using ActionManager::SetLateralIntensitiesForNoCollisionThreatSituation;
    using ActionManager::SetLateralMove;

    using ActionManager::_lateralActionLastTick;
    using ActionManager::_sideLaneSafety;
    using ActionManager::_ttc;
    using ActionManager::_ttcActionLimits;
    // Test functions

    void SET_LATERAL_ACTION_LAST_TICK(LateralAction lateralActionLastTick)
    {
      _lateralActionLastTick = lateralActionLastTick;
    }

    units::time::second_t GET_TTC()
    {
      return _ttc;
    }

    TTCActionLimits GET_TTC_ACTION_LIMITS()
    {
      return _ttcActionLimits;
    }

    void SET_CRITICAL_ACTION_INTENSITIES_PARAMETERS(CriticalActionIntensitiesParameters parameters)
    {
      _criticalActionIntensitiesParameters = parameters;
    }

    CriticalActionIntensitiesParameters GET_CRITICAL_ACTION_INTENSITIES_PARAMETERS()
    {
      return _criticalActionIntensitiesParameters;
    }

    void RESET_LATERAL_ACTION_PATTERN_INTENSITIES()
    {
      _actionPatternIntensities.clear();
    }

    LateralAction GET_MOST_INTENSE_LATERAL_ACTION(ActionPatternIntensities intensityMap,
                                                  LateralAction returnForNoResult)
    {
      LateralAction mostIntenseLateralAction = returnForNoResult;
      double maxIntensity = 0.;

      for (auto i = intensityMap.cbegin(); i != intensityMap.cend(); ++i)
      {
        if (i->second > maxIntensity)
        {
          maxIntensity = i->second;
          mostIntenseLateralAction = i->first;
        }
      }

      return mostIntenseLateralAction;
    }

    void SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction state, double intensity)
    {
      _actionPatternIntensities[state] = intensity;
    }

    void SET_LAST_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction state, double intensity)
    {
      _lastActionPatternIntensities[state] = intensity;
    }

    void SET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX(bool value)
    {
      pastPointOfLaneChangeIntensityMax = value;
    }
    bool GET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX()
    {
      return pastPointOfLaneChangeIntensityMax;
    }

    void SET_ACTION_STATE_LAST_TICK(LateralAction actionState)
    {
      _lateralActionLastTick = actionState;
    }

    ActionPatternIntensities GET_LATERAL_ACTION_PATTERN_INTENSITIES()
    {
      return _actionPatternIntensities;
    }

    void SET_IS_CHANGING_LANE(bool state, AreaOfInterest side)
    {
      switch (side)
      {
        case AreaOfInterest::LEFT_FRONT:
        case AreaOfInterest::LEFT_FRONT_FAR:
        case AreaOfInterest::LEFT_REAR:
        case AreaOfInterest::LEFT_SIDE:
          isChangingLaneLeft = state;
          break;
        case AreaOfInterest::RIGHT_FRONT:
        case AreaOfInterest::RIGHT_FRONT_FAR:
        case AreaOfInterest::RIGHT_REAR:
        case AreaOfInterest::RIGHT_SIDE:
          isChangingLaneRight = state;
          break;
        default:
          break;
      }
    }

    double GET_ACTION_PATTERN_INTENSITIES(LateralAction state)
    {
      return _actionPatternIntensities.at(state);
    }

    void SET_LANE_KEEPING_FLAG(bool state)
    {
      laneKeeping = state;
    }

    void SET_TTC(units::time::second_t ttc)
    {
      _ttc = ttc;
    }

    void SET_TTC_ACTION_LIMITS(TTCActionLimits ttcActionLimits)
    {
      _ttcActionLimits = ttcActionLimits;
    }

    void SET_SIDELANESAFETY(std::shared_ptr<FakeSideLaneSafety>& sideLaneSafety)
    {
      _sideLaneSafety = sideLaneSafety;
    }

    ActionPatternIntensities GET_ACTION_PATTERN_INTENSITIES_LAST()
    {
      return _lastActionPatternIntensities;
    }
  };

  template <typename T>
  T& WITH_CYCLETIME(T& fakeMentalModel, units::time::millisecond_t cycleTime)
  {
    ON_CALL(fakeMentalModel, GetCycleTime()).WillByDefault(Return(cycleTime));
    return fakeMentalModel;
  }

  TrafficRulesScm GENERATE_TRAFFIC_RULES_SCM(bool SET_ZIP_MERGE_ACTIVE)
  {
    TrafficRulesScm traffic;
    traffic.motorway.ZipperMerge = Rule<bool>{traffic.motorway.ZipperMerge.applicable, SET_ZIP_MERGE_ACTIVE};
    return traffic;
  }

  ActionManagerTester(bool SET_ZIP_MERGE_ACTIVE = false)
      : fakeStochastics{},
        fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeSwerving{},
        fakeSurroundingVehicleQueryFactory{},
        fakeTrajectoryPlanner{},
        trafficRulesScm{GENERATE_TRAFFIC_RULES_SCM(SET_ZIP_MERGE_ACTIVE)},
        actionManager(&fakeStochastics,
                      WITH_CYCLETIME(fakeMentalModel, 100_ms),
                      fakeFeatureExtractor,
                      fakeMentalCalculations,
                      fakeSwerving,
                      fakeSurroundingVehicleQueryFactory,
                      trafficRulesScm,
                      fakeTrajectoryPlanner,
                      fakeZipMerging)
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  NiceMock<FakeTrajectoryPlanner> fakeTrajectoryPlanner;
  TrafficRulesScm trafficRulesScm;
  NiceMock<FakeZipMerging> fakeZipMerging;
  ActionManagerUnderTest actionManager;
};

/************************************
 * CHECK LONGITUDINAL ACTION STATES *
 * **********************************/

static std::vector<ObjectInformationScmExtended> FILL_SIDE_VEHICLE_VECTOR(std::vector<int> ids)
{
  std::vector<ObjectInformationScmExtended> resultVector;
  for (auto id : ids)
  {
    ObjectInformationScmExtended newObject;
    newObject.id = id;
    newObject.exist = false;
    resultVector.push_back(newObject);
  }
  return resultVector;
}

/// \brief Data table for definition of individual test cases
struct DataFor_Longitudinal_ActionStates
{
  Situation input_CurrentSituation;
  MesoscopicSituation input_CurrentMesoscopicSituation;
  bool input_IsVehicleVisible;
  units::velocity::meters_per_second_t input_ExtrapolatedDeltaV;
  units::time::second_t input_ExtrapolatedTTC;
  bool input_IsInfluencingDistanceViolated;
  bool input_IsMergeRegulateAndAction;
  bool mock_IsEvadingEndOfLane;
  bool mock_IsCollisionCourseDetected;
  bool mock_MinFollowingDistanceViolated;
  bool mock_IsCooperatingByAccelerating;
  bool mock_IsNearEnoughForFollowing;
  int mock_IsDataForAoiReliableCalled;
  LongitudinalActionState expected_LongitudinalActionState;
};

class ActionManager_LongitudinalActionState : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_Longitudinal_ActionStates>
{
};

TEST_P(ActionManager_LongitudinalActionState, ActionManager_DetermineLongitudinalActionState)
{
  // Get resources for testing
  ActionManagerTester TEST_HELPER;
  DataFor_Longitudinal_ActionStates data = GetParam();

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  const auto fakeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(TEST_HELPER.fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeSurroundingVehicleQuery));
  ON_CALL(*fakeSurroundingVehicleQuery, GetLongitudinalVelocityDelta()).WillByDefault(Return(data.input_ExtrapolatedDeltaV));

  // TODO SharedPtr is only temporary workaround, change when SurroundingVehiclesContainer is integrated in MentalModel
  NiceMock<FakeSurroundingVehicle> leadingVehicle;
  ON_CALL(leadingVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));
  ON_CALL(leadingVehicle, GetTtc()).WillByDefault(Return(data.input_ExtrapolatedTTC));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicle()).WillByDefault(Return(&leadingVehicle));
  auto vehicle = TEST_HELPER.fakeMentalModel.GetLeadingVehicle();
  // Set up test
  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.absoluteVelocity = 30_mps;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  GeometryInformationSCM fakeGeometryInfo{};
  fakeGeometryInfo.Close().distanceToEndOfLaneDuringEmergency = -999.0_m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&fakeGeometryInfo));
  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLaneDuringEmergency = -999.0_m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetDistanceToPointOfNoReturnForBrakingToEndOfLane(_, -999_m)).WillByDefault(Return(-1_m));

  DriverParameters param{};
  param.comfortLongitudinalDeceleration = 2.0_mps_sq;
  param.maximumLongitudinalDeceleration = 9.0_mps_sq;

  ObjectInformationScmExtended mocker{};
  ON_CALL(fakeMicroscopicCharacteristics, GetObjectInformation(_, _)).WillByDefault(Return(&mocker));
  std::vector<ObjectInformationScmExtended> _objectSideLeft = FILL_SIDE_VEHICLE_VECTOR({5, 6, 7});

  ON_CALL(fakeMicroscopicCharacteristics, GetSideObjectsInformation(_)).WillByDefault(Return(&_objectSideLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(data.input_IsVehicleVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(param));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(LateralAction()));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::EGO, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsOnEntryOrExitLane()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsEvadingEndOfLane()).WillByDefault(Return(data.mock_IsEvadingEndOfLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsMinimumFollowingDistanceViolated(vehicle, _)).WillByDefault(Return(data.mock_MinFollowingDistanceViolated));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsAbleToCooperateByAccelerating()).WillByDefault(Return(data.mock_IsCooperatingByAccelerating));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsNearEnoughForFollowing(_, _)).WillByDefault(Return(data.mock_IsNearEnoughForFollowing));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(data.mock_IsCollisionCourseDetected));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  // By default the function 'IsInfluencingDistanceViolated' will return false
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsInfluencingDistanceViolated(vehicle)).WillByDefault(Return(data.input_IsInfluencingDistanceViolated));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasDrivableSuccessor(_, _)).WillByDefault(Return(false));

  auto infDist{40._m};
  double currentDist{50.};

  if (!data.mock_MinFollowingDistanceViolated && !data.mock_IsNearEnoughForFollowing &&
      data.input_CurrentSituation != Situation::COLLISION && data.input_CurrentMesoscopicSituation != MesoscopicSituation::CURRENT_LANE_BLOCKED)
  {
    if (data.input_IsInfluencingDistanceViolated)
    {
      infDist = 50._m;
      currentDist = 40.;
    }
    else
    {
      infDist = 40._m;
      currentDist = 50.;
    }

    ON_CALL(TEST_HELPER.fakeMentalCalculations, GetInfDistance(_, _)).WillByDefault(Return(infDist));
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.input_CurrentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.input_CurrentMesoscopicSituation)).WillByDefault(Return(true));

  if (data.input_IsMergeRegulateAndAction)
  {
    NiceMock<FakeSurroundingVehicle> vehicle;
    Merge::Gap mergeGap(&vehicle, nullptr);
    mergeGap.selectedMergePhases.push_back({MergeAction::CONSTANT_DRIVING, 0.0_s});
    ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));
  }
  else
  {
    Merge::Gap mergeGap(nullptr, nullptr);
    ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));
  }

  // Evaluate results
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLongitudinalActionState(data.expected_LongitudinalActionState)).Times(1);
  // Call test
  TEST_HELPER.actionManager.DetermineLongitudinalActionState();
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_LongitudinalActionState,
    testing::Values(
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::COLLISION,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 0
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 0._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 0,
            .expected_LongitudinalActionState = LongitudinalActionState::STOPPED},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,  // 1
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 0._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::BRAKING_TO_END_OF_LANE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,  // 2
            .input_IsVehicleVisible = true,
            .input_ExtrapolatedDeltaV = 0._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::BRAKING_TO_END_OF_LANE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 3
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 1._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = true,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 4
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -1._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 5 same as 4, but not FALL_BACK_TO_DESIRED_DISTANCE due to merge preparation
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -1._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = true,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 0,
            .expected_LongitudinalActionState = LongitudinalActionState::PREPARING_TO_MERGE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 6
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -6._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 7 SPEED_ADJUSTMENT even though MinFolloWingDistance is violated since leading vehicle is significantly faster
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -4._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 8 FALL_BACK_TO_DESIRED_DISTANCE since MinFolloWingDistance is violated and leading vehicle only slightly faster (limit 2m/s (7.2km/h)
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 1._mps,
            .input_ExtrapolatedTTC = 2._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = true,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 9
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 0._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = true,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 10 APPROACHING since infDistance is violated and ego is faster than target
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 5._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = true,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::APPROACHING},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 11 same as 10, but not APPROACHUNG due to merge preparation
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -5._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = true,
            .input_IsMergeRegulateAndAction = true,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 0,
            .expected_LongitudinalActionState = LongitudinalActionState::PREPARING_TO_MERGE},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 12 SPEED_ADJUSTMENT even though influencingDist is violated since leading vehicle is faster than ego
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = -1._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = true,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT},
        DataFor_Longitudinal_ActionStates{
            .input_CurrentSituation = Situation::FOLLOWING_DRIVING,
            .input_CurrentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,  // 13
            .input_IsVehicleVisible = false,
            .input_ExtrapolatedDeltaV = 0._mps,
            .input_ExtrapolatedTTC = 3._s,
            .input_IsInfluencingDistanceViolated = false,
            .input_IsMergeRegulateAndAction = false,
            .mock_IsEvadingEndOfLane = false,
            .mock_IsCollisionCourseDetected = false,
            .mock_MinFollowingDistanceViolated = false,
            .mock_IsCooperatingByAccelerating = false,
            .mock_IsNearEnoughForFollowing = false,
            .mock_IsDataForAoiReliableCalled = 1,
            .expected_LongitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT}));

/**************************************************
 * CHECK LATERAL ACTION STATES FORCED LANE CHANGE *
 **************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_Lateral_ActionStates_Forced
{
  LateralAction expectedLateralAction;
  LaneChangeAction inputLaneChangeForced;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_Lateral_ActionStates_Forced& obj)
  {
    return os
           << "expectedLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expectedLateralAction)
           << " | inputLaneChangeForced (LaneChangeAction): " << ScmCommons::LaneChangeActionToString(obj.inputLaneChangeForced);
  }
};

class ActionManager_LateralAction_Forced : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_Lateral_ActionStates_Forced>
{
};

TEST_P(ActionManager_LateralAction_Forced, ActionManager_DetermineForcedLateralAction)
{
  // Get resources for testing
  ActionManagerTester TEST_HELPER;
  DataFor_Lateral_ActionStates_Forced data = GetParam();

  NiceMock<FakeActionManager> fakeActionManger;
  NiceMock<FakeActionImplementation> fakeActionImplementation;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  TEST_HELPER.fakeMentalCalculations.Initialize(&TEST_HELPER.fakeMentalModel, &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);

  // Set up test
  auto stubAlgorithmSceneryCar = std::make_unique<AlgorithmSceneryCar>(TEST_HELPER.fakeMentalModel);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(0.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetHeading()).WillByDefault(Return(0.0_rad));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLateralAction(data.expectedLateralAction)).Times(1);

  // Call test
  stubAlgorithmSceneryCar->ForcedLaneChange(&fakeActionManger,
                                            &fakeActionImplementation,
                                            data.inputLaneChangeForced);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_LateralAction_Forced,
    testing::Values(
        DataFor_Lateral_ActionStates_Forced{
            .expectedLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .inputLaneChangeForced = LaneChangeAction::ForcePositive},
        DataFor_Lateral_ActionStates_Forced{
            .expectedLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .inputLaneChangeForced = LaneChangeAction::ForceNegative}));

/************************************************************
 * CHECK SetLateralIntensitiesForNoCollisionThreatSituation *
 ************************************************************/

struct DataFor_SetLateralIntensitiesForNoCollisionThreatSituation
{
  bool input_RemainInLaneChangeState;
  bool input_RemainInSwervingState;
  LateralAction input_PreviousLateralAction;
  bool mock_LaneChangeRightIntensities;
  bool mock_LaneChangeLeftIntensities;
  bool result_ExpectResult;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_SetLateralIntensitiesForNoCollisionThreatSituation& obj)
  {
    return os
           << "input_RemainInLaneChangeState (bool): " << ScmCommons::BooleanToString(obj.input_RemainInLaneChangeState)
           << " | input_RemainInSwervingState (bool): " << ScmCommons::BooleanToString(obj.input_RemainInSwervingState)
           << " | input_PreviousLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.input_PreviousLateralAction)
           << " | mock_LaneChangeRightIntensities (bool): " << ScmCommons::BooleanToString(obj.mock_LaneChangeRightIntensities)
           << " | mock_LaneChangeLeftIntensities (bool): " << ScmCommons::BooleanToString(obj.mock_LaneChangeLeftIntensities)
           << " | result_ExpectResult (bool): " << ScmCommons::BooleanToString(obj.result_ExpectResult);
  }
};

class ActionManager_SetLateralIntensitiesForNoCollisionThreatSituation : public ::testing::Test,
                                                                         public ::testing::WithParamInterface<DataFor_SetLateralIntensitiesForNoCollisionThreatSituation>
{
};

TEST_P(ActionManager_SetLateralIntensitiesForNoCollisionThreatSituation, CheckFunction_SetLateralIntensitiesForNoCollisionThreatSituation)
{
  // Get resources for testing
  ActionManagerTester TEST_HELPER;
  DataFor_SetLateralIntensitiesForNoCollisionThreatSituation data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.input_PreviousLateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInLaneChangeState()).WillByDefault(Return(data.input_RemainInLaneChangeState));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInSwervingState()).WillByDefault(Return(data.input_RemainInSwervingState));

  if (!(data.input_RemainInLaneChangeState || data.input_RemainInSwervingState))
  {
    if (data.mock_LaneChangeRightIntensities)
    {
      TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
    }

    if (!data.mock_LaneChangeRightIntensities && data.mock_LaneChangeLeftIntensities)
    {
      TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();
    }
  }
  // Run test
  TEST_HELPER.actionManager.SetLateralIntensitiesForNoCollisionThreatSituation();

  // Evaluate results
  if (data.result_ExpectResult)
  {
    if (data.input_RemainInLaneChangeState || data.input_RemainInSwervingState)
    {
      ASSERT_EQ(TEST_HELPER.actionManager.GET_ACTION_PATTERN_INTENSITIES(data.input_PreviousLateralAction), 1.);
    }
    else
    {
      ASSERT_EQ(TEST_HELPER.actionManager.GET_ACTION_PATTERN_INTENSITIES(LateralAction(LateralAction::State::LANE_KEEPING)), 1.);
    }
  }
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_SetLateralIntensitiesForNoCollisionThreatSituation,
    testing::Values(
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .mock_LaneChangeRightIntensities = false,
            .mock_LaneChangeLeftIntensities = false,
            .result_ExpectResult = true},  // No active lane change, no lane change wish
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .mock_LaneChangeRightIntensities = false,
            .mock_LaneChangeLeftIntensities = false,
            .result_ExpectResult = true},  // No active lane change, no lane change wish
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .mock_LaneChangeRightIntensities = false,
            .mock_LaneChangeLeftIntensities = false,
            .result_ExpectResult = true},  // Lane change just finished, no lane change wish
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = true,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .mock_LaneChangeRightIntensities = true,
            .mock_LaneChangeLeftIntensities = true,
            .result_ExpectResult = true},  // Lane change left, lane change wish
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT),
            .mock_LaneChangeRightIntensities = true,
            .mock_LaneChangeLeftIntensities = true,
            .result_ExpectResult = true},  // Swerving right, lane change wish
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .mock_LaneChangeRightIntensities = true,
            .mock_LaneChangeLeftIntensities = false,
            .result_ExpectResult = false},  // No active lane change, lane change wish right
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .mock_LaneChangeRightIntensities = false,
            .mock_LaneChangeLeftIntensities = true,
            .result_ExpectResult = false},  // No active lane change, no lane change wish left
        DataFor_SetLateralIntensitiesForNoCollisionThreatSituation{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_PreviousLateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .mock_LaneChangeRightIntensities = true,
            .mock_LaneChangeLeftIntensities = true,
            .result_ExpectResult = false}));  // No active lane change, no lane change wish both sides

/********************************************************
 * CHECK ChooseLateralActionIntensitiesForCollisionCase *
 ********************************************************/

TEST(ChooseLateralActionIntensitiesForCollisionCase, NoReturn)
{
  ActionManagerTester TEST_HELPER;
  TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::EGO_REAR, false);
}

TEST(ChooseLateralActionIntensitiesForCollisionCase, NoReturn2)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::EGO_FRONT, false);

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT));

  ASSERT_EQ(LateralAction(LateralAction::State::LANE_KEEPING), result);
}

/*************************************************
 * CHECK GenerateLateralActionPatternIntensities *
 *************************************************/

struct DataFor_GenerateLateralActionPatternIntensities
{
  Situation input_situation;  // current situation choosen by the driver
  MesoscopicSituation input_mesoscopicSituation;
  Risk situationRisk;
  bool isAnticipated;

  // \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensities& obj)
  {
    return os
           << "input_situation (Situation): " << ScmCommons::SituationToString(obj.input_situation)
           << " situationRisk (Risk): " << static_cast<int>(obj.situationRisk)
           << " isAnticipated (bool): " << ScmCommons::BooleanToString(obj.isAnticipated);
  }
};

class ActionManager_GenerateLateralActionPatternIntensities : public ::testing::Test,
                                                              public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensities>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensities, CheckFunction_GenerateLateralActionPatternIntensities)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensities data = GetParam();
  ActionManagerTester TEST_HELPER;

  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));

  // Set up test
  Situation situation = data.input_situation;

  switch (situation)
  {
    case Situation::FREE_DRIVING:
    case Situation::FOLLOWING_DRIVING:
    case Situation::OBSTACLE_ON_CURRENT_LANE:
    case Situation::LANE_CHANGER_FROM_LEFT:
    case Situation::LANE_CHANGER_FROM_RIGHT:
    case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
    case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
    case Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE:
    case Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE:
      ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(situation));
      ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
      break;

    default:
      ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::UNDEFINED));
      ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
      break;
  }
  if (data.input_mesoscopicSituation == MesoscopicSituation::CURRENT_LANE_BLOCKED || data.input_mesoscopicSituation == MesoscopicSituation::MANDATORY_EXIT || data.input_mesoscopicSituation == MesoscopicSituation::QUEUED_TRAFFIC)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::UNDEFINED));
    ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.input_mesoscopicSituation)).WillByDefault(Return(true));
  }

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetSituationRisk(situation)).WillByDefault(Return(data.situationRisk));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSituationAnticipated(situation)).WillByDefault(Return(data.isAnticipated));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(LateralAction(LateralAction::State::LANE_KEEPING)));

  // Define test conditions
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  GeometryInformationSCM fakeGeometryInfo{};
  fakeGeometryInfo.Close().distanceToEndOfLane = -999._m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&fakeGeometryInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  TEST_HELPER.actionManager.GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations();

  switch (situation)
  {
    case Situation::FREE_DRIVING:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFreeDriving();
      break;

    case Situation::FOLLOWING_DRIVING:
      if (data.situationRisk == Risk::HIGH)
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForTooClose();
      }
      else
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFollowingDriving();
      }
      break;

    case Situation::OBSTACLE_ON_CURRENT_LANE:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
      break;

    case Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane();
      break;

    case Situation::SIDE_COLLISION_RISK_FROM_LEFT:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest::LEFT_SIDE);
      break;

    case Situation::SIDE_COLLISION_RISK_FROM_RIGHT:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest::RIGHT_SIDE);
      break;

    case Situation::LANE_CHANGER_FROM_LEFT:
      if (data.situationRisk == Risk::HIGH)
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();
      }
      else if (data.situationRisk != Risk::HIGH && !data.isAnticipated)
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side::Left);
      }
      else
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft();
      }
      break;

    case Situation::LANE_CHANGER_FROM_RIGHT:
      if (data.situationRisk == Risk::HIGH)
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();
      }
      else if (data.situationRisk != Risk::HIGH && !data.isAnticipated)
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side::Right);
      }
      else
      {
        TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight();
      }
      break;

    case Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE:
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane();
      break;

    default:
      break;
  }

  // Run test
  if (data.input_mesoscopicSituation == MesoscopicSituation::CURRENT_LANE_BLOCKED || data.input_mesoscopicSituation == MesoscopicSituation::MANDATORY_EXIT || data.input_mesoscopicSituation == MesoscopicSituation::QUEUED_TRAFFIC)
  {
    EXPECT_THROW(TEST_HELPER.actionManager.GenerateLateralActionPatternIntensities(), std::runtime_error);
  }
  else
  {
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensities();
  }
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensities,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FREE_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::CURRENT_LANE_BLOCKED,
            .situationRisk = Risk::HIGH,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FREE_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::MANDATORY_EXIT,
            .situationRisk = Risk::MEDIUM,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FREE_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .situationRisk = Risk::MEDIUM,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FREE_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FOLLOWING_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FOLLOWING_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::MEDIUM,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::FOLLOWING_DRIVING,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::HIGH,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::HIGH,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::MEDIUM,
            .isAnticipated = true},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::LANE_CHANGER_FROM_LEFT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::HIGH,
            .isAnticipated = true},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::MEDIUM,
            .isAnticipated = true},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::HIGH,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false},
        DataFor_GenerateLateralActionPatternIntensities{
            .input_situation = Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE,
            .input_mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .situationRisk = Risk::LOW,
            .isAnticipated = false}));

/***************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForFreeDriving *
 ***************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving
{
  int input_laneChangeWish;                         // -1: Lane change wish right, 0: No lane change wish, 1: Lane change wish left
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving& obj)
  {
    return os
           << "input_laneChangeWish (int): " << obj.input_laneChangeWish
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForFreeDriving : public ::testing::Test,
                                                                            public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForFreeDriving, CheckFunction_GenerateLateralActionPatternIntensitiesForFreeDriving)
{
  // Get resources for testing
  ActionManagerTester TEST_HELPER;
  DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving data = GetParam();

  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  // Set up test
  if (data.input_laneChangeWish == -1)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(true));
    NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
    ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
    ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::RIGHT));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 1.);

    std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
    TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);

    ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::FREE_DRIVING));

    DriverParameters driverParameter;
    driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
    ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

    LaneInformationGeometrySCM laneInformationGeometrySCM;
    laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

    NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
    ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

    TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();

    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFreeDriving();
  }
  else if (data.input_laneChangeWish == 1)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(true));
    NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
    ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
    ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::LEFT));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.);
    TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
  }
  else
  {
    TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();

    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFreeDriving();
  }

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction =
      TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForFreeDriving,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving{
            .input_laneChangeWish = -1,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving{
            .input_laneChangeWish = 0,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForFreeDriving{
            .input_laneChangeWish = 1,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)}));

/********************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForFollowingDriving *
 ********************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving
{
  int input_laneChangeWish;                         // -1: Lane change wish right, 0: No lane change wish, 1: Lane change wish left
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving& obj)
  {
    return os
           << "input_laneChangeWish (int): " << obj.input_laneChangeWish
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForFollowingDriving : public ::testing::Test,
                                                                                 public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForFollowingDriving, CheckFunction_GenerateLateralActionPatternIntensitiesForFollowingDriving)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving data = GetParam();
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  // Set up test
  if (data.input_laneChangeWish == -1)
  {
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 1.);
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(true));
    NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
    ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
    ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::RIGHT));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));

    std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
    TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
    ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(true));

    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 1.);
    TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
    TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();
    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFollowingDriving();
  }
  else if (data.input_laneChangeWish == 1)
  {
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.);
    TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(true));
    NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
    ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
    ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::LEFT));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));

    std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
    TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);

    ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(100.0));
    TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();

    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFollowingDriving();
  }
  else
  {
    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForFollowingDriving();
  }

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForFollowingDriving,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving{
            .input_laneChangeWish = -1,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving{
            .input_laneChangeWish = 0,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForFollowingDriving{
            .input_laneChangeWish = 1,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)}));

/************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForTooClose *
 ************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForTooClose
{
  bool input_hasCausingVehicle;  // Is a causing vehicle saved in the MentalModel?
  bool input_collisionCourse;    // Is a collision course detected to another vehicle?
  bool input_laneKeeping;        // Is the driver currently performing a lane keeping action?

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForTooClose& obj)
  {
    return os
           << "input_collisionCourse (bool): " << ScmCommons::BooleanToString(obj.input_collisionCourse)
           << " | input_laneKeeping (bool): " << ScmCommons::BooleanToString(obj.input_laneKeeping);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForTooClose : public ::testing::Test,
                                                                         public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForTooClose>
{
};

// TODO Note Thomas: Disabled for now as this test does not actually test anything. Only one EXPECT_CALL?
TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForTooClose, CheckFunction_GenerateLateralActionPatternIntensitiesForTooClose)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForTooClose data = GetParam();
  ActionManagerTester TEST_HELPER;
  FakeSurroundingVehicle causingVehicle;
  AreaOfInterest causingAoi{AreaOfInterest::EGO_FRONT};

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationFrontCluster()).WillByDefault(Return(causingAoi));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(causingAoi, _)).WillByDefault(Return(&causingVehicle));

  EXPECT_CALL(causingVehicle, GetReliability(_)).Times(1);
  TEST_HELPER.actionManager.SET_LANE_KEEPING_FLAG(data.input_laneKeeping);

  if (!data.input_collisionCourse)
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(false));
    TEST_HELPER.actionManager.SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(true));
    if (data.input_laneKeeping)
    {
      TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::LEFT_FRONT, false);
    }
    else
    {
      TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest::EGO_FRONT, false);
    }
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForTooClose();
};

INSTANTIATE_TEST_SUITE_P(
    DISABLED_Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForTooClose,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForTooClose{
            .input_hasCausingVehicle = false,
            .input_collisionCourse = false,
            .input_laneKeeping = true},
        DataFor_GenerateLateralActionPatternIntensitiesForTooClose{
            .input_hasCausingVehicle = true,
            .input_collisionCourse = true,
            .input_laneKeeping = true},
        DataFor_GenerateLateralActionPatternIntensitiesForTooClose{
            .input_hasCausingVehicle = true,
            .input_collisionCourse = true,
            .input_laneKeeping = false}));

/*************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane *
 *************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane
{
  bool input_collisionCourse;                       // Is a collision course detected to the obstacle?
  bool input_isLaneChangeRightPossible;             // Is a lane change to the right lane possible?
  bool input_isLaneChangeLeftPossible;              // Is a lane change to the left lane possible?
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane& obj)
  {
    return os
           << "input_collisionCourse (bool): " << ScmCommons::BooleanToString(obj.input_collisionCourse)
           << " | input_isLaneChangeRightPossible (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeRightPossible)
           << " | input_isLaneChangeLeftPossible (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeLeftPossible)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane : public ::testing::Test,
                                                                                      public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane, CheckFunction_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane data = GetParam();
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  // Set up test
  FakeSurroundingVehicle causingVehicle;
  AreaOfInterest causingAoi{AreaOfInterest::EGO_FRONT};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationFrontCluster()).WillByDefault(Return(causingAoi));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(causingAoi, _)).WillByDefault(Return(&causingVehicle));

  ON_CALL(causingVehicle, GetAssignedAoi).WillByDefault(Return(causingAoi));
  EXPECT_CALL(causingVehicle, IsReliable(DataQuality::HIGH, ParameterChangeRate::FAST)).Times(1).WillRepeatedly(Return(true));

  auto fakeVehicleQuery = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(TEST_HELPER.fakeSurroundingVehicleQueryFactory, GetQuery(::testing::Ref(causingVehicle))).WillByDefault(Return(fakeVehicleQuery));
  ON_CALL(*fakeVehicleQuery, HasCollisionCourse(_)).WillByDefault(Return(data.input_collisionCourse));

  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  if (!data.input_collisionCourse)
  {
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_KEEPING), 1.);
    if (data.input_isLaneChangeRightPossible)
    {
      NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
      ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
      ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::RIGHT));
      ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
      ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));

      std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
      TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
      ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, RelativeLane::RIGHT)).WillByDefault(Return(true));

      TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
      TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 1.);

      // Run test
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
    }
    else if (data.input_isLaneChangeLeftPossible)
    {
      TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
      ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(true));
      NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
      ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
      ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(SurroundingLane::LEFT));
      ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
      ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));
      TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();
      TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.);

      // Run test
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
    }
    else
    {
      TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();
      TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();
      TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_KEEPING), 1.);

      // Run test
      TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
    }
  }
  else
  {
    TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::LEFT_FRONT, false);
    TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_KEEPING), 1.);

    // Run test
    TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();
  }

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane{
            .input_collisionCourse = false,
            .input_isLaneChangeRightPossible = false,
            .input_isLaneChangeLeftPossible = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane{
            .input_collisionCourse = true,
            .input_isLaneChangeRightPossible = false,
            .input_isLaneChangeLeftPossible = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane{
            .input_collisionCourse = false,
            .input_isLaneChangeRightPossible = true,
            .input_isLaneChangeLeftPossible = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane{
            .input_collisionCourse = false,
            .input_isLaneChangeRightPossible = false,
            .input_isLaneChangeLeftPossible = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)}));

/***********************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForDetectedLaneChanger *
 ***********************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger
{
  bool input_hasSideLane;             // Is there a lane to the right of the driver?
  bool input_isLaneChangerFavorable;  // Could the lane changer be accepted as new leading vehicle?
  bool input_isLaneChangeSideSafe;    // Is a lane change to the right lane possible?
  Side side;
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger& obj)
  {
    return os
           << "input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasSideLane)
           << " | input_isLaneChangerFavorable (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangerFavorable)
           << " | input_isLaneChangeSideSafe (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeSideSafe)
           << " | side (Side): " << static_cast<int>(obj.side)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger : public ::testing::Test,
                                                                                    public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger, CheckFunction_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger data = GetParam();
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));

  // Set up test
  if (data.input_hasSideLane)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(true));
  }
  else
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(false));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(false));
  }

  if (data.input_isLaneChangerFavorable)
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsUnfavorable(_)).WillByDefault(Return(false));
  }
  else
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsUnfavorable(_)).WillByDefault(Return(true));
  }

  if (data.input_isLaneChangeSideSafe)
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));
  }
  else
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(false));
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(data.side);

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = false,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = false,
            .side = Side::Left,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = true,
            .input_isLaneChangeSideSafe = true,
            .side = Side::Left,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = true,
            .side = Side::Left,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = false,
            .side = Side::Left,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = false,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = false,
            .side = Side::Right,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = true,
            .input_isLaneChangeSideSafe = true,
            .side = Side::Right,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = true,
            .side = Side::Right,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)},
        DataFor_GenerateLateralActionPatternIntensitiesForDetectedLaneChanger{
            .input_hasSideLane = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeSideSafe = false,
            .side = Side::Right,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)}));

/************************************************************************
 * CHECK ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement *
 ************************************************************************/

TEST(ActionManager_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement, CallWithNoLaneChanging_ResultsInRuntimeError)
{
  ActionManagerTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(LateralAction(LateralAction::State::LANE_KEEPING)));

  // Run Test
  EXPECT_THROW(
      {
        try
        {
          TEST_HELPER.actionManager.ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();
        }
        catch (std::runtime_error& e)
        {
          EXPECT_STREQ("ActionManager::ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement: This function is currently only valid while changing lanes!", e.what());
          throw;
        }
      },
      std::runtime_error);
}

struct DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement
{
  LateralAction lateralAction;
  bool isLaneKeepingMostAppropriate;
  LateralAction expect_mostIntenseLateralAction;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement& obj)
  {
    return os
           << " | lateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralAction)
           << " | isLaneKeepingMostAppropriate (bool): " << ScmCommons::BooleanToString(obj.isLaneKeepingMostAppropriate)
           << " | expect_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expect_mostIntenseLateralAction);
  }
};

class ActionManager_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement : public ::testing::Test,
                                                                                     public ::testing::WithParamInterface<DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement>
{
};

TEST_P(ActionManager_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement, CheckFunction_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement)
{
  // Get resources for testing
  DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  MicroscopicCharacteristics microscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&microscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));

  NiceMock<FakeLaneKeepingCalculations> fakeLaneCalculations;

  if (data.isLaneKeepingMostAppropriate)
  {
    TEST_HELPER.actionManager.IsLaneKeepingMostAppropriate(fakeLaneCalculations);
    TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

    // Run Test
    TEST_HELPER.actionManager.ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();
  }
  else
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, true)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, true)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(false));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(5.0_s));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(5.0_s));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(true));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(5.0_s));

    TEST_HELPER.actionManager.IsLaneKeepingMostAppropriate(fakeLaneCalculations);

    // Run Test
    TEST_HELPER.actionManager.ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();
  }

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));
  ASSERT_EQ(data.expect_mostIntenseLateralAction, result);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement,
    testing::Values(
        DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .isLaneKeepingMostAppropriate = true,
            .expect_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .isLaneKeepingMostAppropriate = false,
            .expect_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)},
        DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneKeepingMostAppropriate = true,
            .expect_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneKeepingMostAppropriate = false,
            .expect_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)}));

/**************************************
 * CHECK IsLaneKeepingMostAppropriate *
 **************************************/

struct DataFor_IsLaneKeepingMostAppropriate
{
  LateralAction lateralAction;
  std::pair<AreaOfInterest, AreaOfInterest> frontRearAoi;
  bool changingRightAppropriate;
  bool changingLeftAppropriate;
  bool isAtEndOfLateralMovement;
  bool isObjectInTargetSide;
  bool egoFasterThanDeltaSide;
  bool result_isLaneKeepingMostAppropriate;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_IsLaneKeepingMostAppropriate& obj)
  {
    return os
           << " | lateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralAction)
           << " | result_isLaneKeepingMostAppropriate (bool): " << ScmCommons::BooleanToString(obj.result_isLaneKeepingMostAppropriate);
  }
};

class ActionManager_IsLaneKeepingMostAppropriate : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_IsLaneKeepingMostAppropriate>
{
};

TEST_P(ActionManager_IsLaneKeepingMostAppropriate, CheckFunction_IsLaneKeepingMostAppropriate)
{
  // Get resources for testing
  DataFor_IsLaneKeepingMostAppropriate data = GetParam();
  ActionManagerTester TEST_HELPER;
  NiceMock<FakeLaneKeepingCalculations> fakeLaneCalculations;

  // Setup Test
  ON_CALL(fakeLaneCalculations, ChangingRightAppropriate()).WillByDefault(Return(data.changingRightAppropriate));
  ON_CALL(fakeLaneCalculations, ChangingLeftAppropriate()).WillByDefault(Return(data.changingLeftAppropriate));
  ON_CALL(fakeLaneCalculations, IsAtEndOfLateralMovement()).WillByDefault(Return(data.isAtEndOfLateralMovement));
  ON_CALL(fakeLaneCalculations, IsObjectInTargetSide()).WillByDefault(Return(data.isObjectInTargetSide));
  ON_CALL(fakeLaneCalculations, EgoFasterThanDeltaSide()).WillByDefault(Return(data.egoFasterThanDeltaSide));

  // Run Test
  bool result_isLaneKeepingMostAppropriate = TEST_HELPER.actionManager.IsLaneKeepingMostAppropriate(fakeLaneCalculations);
  ASSERT_EQ(result_isLaneKeepingMostAppropriate, data.result_isLaneKeepingMostAppropriate);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_IsLaneKeepingMostAppropriate,
    testing::Values(
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = false,
            .changingLeftAppropriate = false,
            .isAtEndOfLateralMovement = false,
            .isObjectInTargetSide = false,
            .egoFasterThanDeltaSide = false,
            .result_isLaneKeepingMostAppropriate = false},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = true,
            .changingLeftAppropriate = false,
            .isAtEndOfLateralMovement = false,
            .isObjectInTargetSide = false,
            .egoFasterThanDeltaSide = false,
            .result_isLaneKeepingMostAppropriate = true},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = false,
            .changingLeftAppropriate = true,
            .isAtEndOfLateralMovement = false,
            .isObjectInTargetSide = false,
            .egoFasterThanDeltaSide = false,
            .result_isLaneKeepingMostAppropriate = true},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = false,
            .changingLeftAppropriate = false,
            .isAtEndOfLateralMovement = true,
            .isObjectInTargetSide = false,
            .egoFasterThanDeltaSide = false,
            .result_isLaneKeepingMostAppropriate = true},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = false,
            .changingLeftAppropriate = false,
            .isAtEndOfLateralMovement = false,
            .isObjectInTargetSide = true,
            .egoFasterThanDeltaSide = false,
            .result_isLaneKeepingMostAppropriate = true},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = true,
            .changingLeftAppropriate = false,
            .isAtEndOfLateralMovement = false,
            .isObjectInTargetSide = false,
            .egoFasterThanDeltaSide = true,
            .result_isLaneKeepingMostAppropriate = true},
        DataFor_IsLaneKeepingMostAppropriate{
            .changingRightAppropriate = true,
            .changingLeftAppropriate = true,
            .isAtEndOfLateralMovement = true,
            .isObjectInTargetSide = true,
            .egoFasterThanDeltaSide = true,
            .result_isLaneKeepingMostAppropriate = true}));

/****************************************************************************
 * CHECK ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement *
 ****************************************************************************/

struct DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement
{
  bool input_urgent;
  bool input_hasLeftLane;
  bool input_hasRightLane;
  bool input_leftLaneSafe;
  bool input_rightLaneSafe;
  bool input_vehicleOnTargetLaneDueToLateralMovement;
  SurroundingLane input_laneChangeWish;
  bool mock_isChangingLaneLeft;
  bool mock_isChangingLaneRight;
  LateralAction expected_mostIntenseLateralAction;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement& obj)
  {
    return os
           << "input_urgent (bool): " << ScmCommons::BooleanToString(obj.input_urgent)
           << " | input_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.input_hasLeftLane)
           << " | input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasRightLane)
           << " | input_leftLaneSafe (bool): " << ScmCommons::BooleanToString(obj.input_leftLaneSafe)
           << " | input_rightLaneSafe (bool): " << ScmCommons::BooleanToString(obj.input_rightLaneSafe)
           << " | input_vehicleOnTargetLaneDueToLateralMovement (bool): " << ScmCommons::BooleanToString(obj.input_vehicleOnTargetLaneDueToLateralMovement)
           << " | input_laneChangeWish (SurroundingLane): " << ScmCommons::SurroundingLaneToString(obj.input_laneChangeWish)
           << " | mock_isChangingLaneLeft (bool): " << ScmCommons::BooleanToString(obj.mock_isChangingLaneLeft)
           << " | mock_isChangingLaneRight (bool): " << ScmCommons::BooleanToString(obj.mock_isChangingLaneRight)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement : public ::testing::Test,
                                                                                         public ::testing::WithParamInterface<DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement>
{
};

TEST_P(ActionManager_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement, CheckFunction_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement)
{
  // Get resources for testing
  DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(data.input_vehicleOnTargetLaneDueToLateralMovement));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsSideLaneSafe(Side::Right)).WillByDefault(Return(data.input_rightLaneSafe));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsSideLaneSafe(Side::Left)).WillByDefault(Return(data.input_leftLaneSafe));
  ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(data.input_laneChangeWish));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.lateralAction = LateralAction(LateralAction::State::LANE_KEEPING);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));

  if (data.mock_isChangingLaneLeft)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_hasRightLane));
    if (data.input_vehicleOnTargetLaneDueToLateralMovement)
    {
      if (data.input_hasRightLane && data.input_rightLaneSafe)
      {
        TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 1.0);
      }
    }
    else if (!data.input_vehicleOnTargetLaneDueToLateralMovement)
    {
      if (data.input_leftLaneSafe)
      {
        TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.0);
      }
    }
  }
  else if (data.mock_isChangingLaneRight)
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_hasLeftLane));
    if (data.input_vehicleOnTargetLaneDueToLateralMovement)
    {
      if (data.input_hasLeftLane && data.input_leftLaneSafe)
      {
        TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.0);
      }
    }
  }
  TEST_HELPER.actionManager.SET_IS_CHANGING_LANE(data.mock_isChangingLaneLeft, AreaOfInterest::LEFT_SIDE);
  TEST_HELPER.actionManager.SET_IS_CHANGING_LANE(data.mock_isChangingLaneRight, AreaOfInterest::RIGHT_SIDE);

  // Run Test
  TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest::EGO_FRONT, data.input_urgent);

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement,
    testing::Values(
        DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement{
            .input_urgent = false,
            .input_hasLeftLane = false,
            .input_hasRightLane = true,
            .input_leftLaneSafe = false,
            .input_rightLaneSafe = true,
            .input_vehicleOnTargetLaneDueToLateralMovement = true,
            .input_laneChangeWish = SurroundingLane::RIGHT,
            .mock_isChangingLaneLeft = true,
            .mock_isChangingLaneRight = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement{
            .input_urgent = false,
            .input_hasLeftLane = true,
            .input_hasRightLane = false,
            .input_leftLaneSafe = true,
            .input_rightLaneSafe = false,
            .input_vehicleOnTargetLaneDueToLateralMovement = true,
            .input_laneChangeWish = SurroundingLane::LEFT,
            .mock_isChangingLaneLeft = false,
            .mock_isChangingLaneRight = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)}));

/******************************************
 * CHECK CheckIntensitiesForChangingRight *
 ******************************************/

struct DataFor_CheckIntensitiesForChangingRight
{
  bool input_hasRightLane;
  bool input_laneChangePermittedDueToLaneMarkings;
  bool input_laneChangeRightSafe;
  bool input_isMergeRightSafe;
  SurroundingLane input_laneChangeWish;
  bool expected_Result;
  LateralAction expected_mostIntenseLateralAction;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CheckIntensitiesForChangingRight& obj)
  {
    return os
           << "input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasRightLane)
           << " | input_laneChangePermittedDueToLaneMarkings (bool): " << ScmCommons::BooleanToString(obj.input_laneChangePermittedDueToLaneMarkings)
           << " | input_laneChangeRightSafe (bool): " << ScmCommons::BooleanToString(obj.input_laneChangeRightSafe)
           << " | input_isMergeRightSafe (bool): " << ScmCommons::BooleanToString(obj.input_isMergeRightSafe)
           << " | input_laneChangeWish (SurroundingLane): " << ScmCommons::SurroundingLaneToString(obj.input_laneChangeWish)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction)
           << " | expected_Result (bool): " << ScmCommons::BooleanToString(obj.expected_Result);
  }
};

class ActionManager_CheckIntensitiesForChangingRight : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_CheckIntensitiesForChangingRight>
{
};

TEST_P(ActionManager_CheckIntensitiesForChangingRight, CheckFunction_CheckIntensitiesForChangingRight)
{
  // Get resources for testing
  DataFor_CheckIntensitiesForChangingRight data = GetParam();
  ActionManagerTester TEST_HELPER;

  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));

  // Set up test
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_hasRightLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(data.input_laneChangePermittedDueToLaneMarkings));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.input_laneChangeRightSafe));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSideLaneSafeWithMergePreparation(_)).WillByDefault(Return(data.input_isMergeRightSafe));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(data.input_laneChangeWish));

  std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
  TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
  ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, RelativeLane::RIGHT)).WillByDefault(Return(true));

  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  // Run Test
  bool result_bool = TEST_HELPER.actionManager.CheckIntensitiesForChangingRight();

  // Evaluate Results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result_LateralActioState = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_LateralActioState);
  ASSERT_EQ(data.expected_Result, result_bool);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_CheckIntensitiesForChangingRight,
    testing::Values(
        DataFor_CheckIntensitiesForChangingRight{
            .input_hasRightLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeRightSafe = true,
            .input_isMergeRightSafe = true,
            .input_laneChangeWish = SurroundingLane::RIGHT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_CheckIntensitiesForChangingRight{
            .input_hasRightLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeRightSafe = false,
            .input_isMergeRightSafe = true,
            .input_laneChangeWish = SurroundingLane::RIGHT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT)},
        DataFor_CheckIntensitiesForChangingRight{
            .input_hasRightLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeRightSafe = false,
            .input_isMergeRightSafe = false,
            .input_laneChangeWish = SurroundingLane::RIGHT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)},
        DataFor_CheckIntensitiesForChangingRight{
            .input_hasRightLane = false,
            .input_laneChangePermittedDueToLaneMarkings = false,
            .input_laneChangeRightSafe = false,
            .input_isMergeRightSafe = false,
            .input_laneChangeWish = SurroundingLane::EGO,
            .expected_Result = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT)},
        DataFor_CheckIntensitiesForChangingRight{
            .input_hasRightLane = true,
            .input_laneChangePermittedDueToLaneMarkings = false,
            .input_laneChangeRightSafe = true,
            .input_isMergeRightSafe = true,
            .input_laneChangeWish = SurroundingLane::RIGHT,
            .expected_Result = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT)}));

/*****************************************
 * CHECK CheckIntensitiesForChangingLeft *
 *****************************************/

struct DataFor_CheckIntensitiesForChangingLeft
{
  bool input_hasLeftLane;
  bool input_laneChangePermittedDueToLaneMarkings;
  bool input_laneChangeLeftSafe;
  bool input_isMergeLeftSafe;
  SurroundingLane input_laneChangeWish;
  bool expected_Result;
  LateralAction expected_mostIntenseLateralAction;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CheckIntensitiesForChangingLeft& obj)
  {
    return os
           << "input_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.input_hasLeftLane)
           << " | input_laneChangePermittedDueToLaneMarkings (bool): " << ScmCommons::BooleanToString(obj.input_laneChangePermittedDueToLaneMarkings)
           << " | input_laneChangeLeftSafe (bool): " << ScmCommons::BooleanToString(obj.input_laneChangeLeftSafe)
           << " | input_isMergeLeftSafe (bool): " << ScmCommons::BooleanToString(obj.input_isMergeLeftSafe)
           << " | input_laneChangeWish (SurroundingLane): " << ScmCommons::SurroundingLaneToString(obj.input_laneChangeWish)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction)
           << " | expected_Result (bool): " << ScmCommons::BooleanToString(obj.expected_Result);
  }
};

class ActionManager_CheckIntensitiesForChangingLeft : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_CheckIntensitiesForChangingLeft>
{
};

TEST_P(ActionManager_CheckIntensitiesForChangingLeft, CheckFunction_CheckIntensitiesForChangingLeft)
{
  // Get resources for testing
  DataFor_CheckIntensitiesForChangingLeft data = GetParam();
  ActionManagerTester TEST_HELPER;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));

  std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
  TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
  ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, RelativeLane::LEFT)).WillByDefault(Return(true));

  //  Set up test
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_hasLeftLane));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(data.input_laneChangePermittedDueToLaneMarkings));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.input_laneChangeLeftSafe));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSideLaneSafeWithMergePreparation(_)).WillByDefault(Return(data.input_isMergeLeftSafe));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeLaneChangeBehavior, GetLaneChangeWish()).WillByDefault(Return(data.input_laneChangeWish));
  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  // Run Test
  bool result_bool = TEST_HELPER.actionManager.CheckIntensitiesForChangingLeft();

  // Evaluate Results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result_LateralActioState = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_LateralActioState);
  ASSERT_EQ(data.expected_Result, result_bool);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_CheckIntensitiesForChangingLeft,
    testing::Values(
        DataFor_CheckIntensitiesForChangingLeft{
            .input_hasLeftLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeLeftSafe = true,
            .input_isMergeLeftSafe = true,
            .input_laneChangeWish = SurroundingLane::LEFT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)},
        DataFor_CheckIntensitiesForChangingLeft{
            .input_hasLeftLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeLeftSafe = false,
            .input_isMergeLeftSafe = true,
            .input_laneChangeWish = SurroundingLane::LEFT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)},
        DataFor_CheckIntensitiesForChangingLeft{
            .input_hasLeftLane = true,
            .input_laneChangePermittedDueToLaneMarkings = true,
            .input_laneChangeLeftSafe = false,
            .input_isMergeLeftSafe = false,
            .input_laneChangeWish = SurroundingLane::LEFT,
            .expected_Result = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)},
        DataFor_CheckIntensitiesForChangingLeft{
            .input_hasLeftLane = false,
            .input_laneChangePermittedDueToLaneMarkings = false,
            .input_laneChangeLeftSafe = false,
            .input_isMergeLeftSafe = false,
            .input_laneChangeWish = SurroundingLane::EGO,
            .expected_Result = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT)},
        DataFor_CheckIntensitiesForChangingLeft{
            .input_hasLeftLane = true,
            .input_laneChangePermittedDueToLaneMarkings = false,
            .input_laneChangeLeftSafe = true,
            .input_isMergeLeftSafe = true,
            .input_laneChangeWish = SurroundingLane::LEFT,
            .expected_Result = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT)}));

/**********************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft *
 **********************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft
{
  bool input_hasRightLane;                          // Is there a lane to the right of the driver?
  bool input_isDriverCooperative;                   // Is the driver cooperative with the possible lane changer?
  bool input_isLaneChangerFavorable;                // Could the possible lane changer be accepted as new leading vehicle?
  bool input_isLaneChangeRightSafe;                 // Is a lane change to the right lane possible?
  scm::common::VehicleClass input_EgoVehicleType;   // Type of Ego vehicle
  LateralAction expected_mostIntenseLateralAction;  // Expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft& obj)
  {
    return os
           << "input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasRightLane)
           << " | input_isDriverCooperative (bool): " << ScmCommons::BooleanToString(obj.input_isDriverCooperative)
           << " | input_isLaneChangerFavorable (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangerFavorable)
           << " | input_isLaneChangeRightSafe (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeRightSafe)
           << " | input_EgoVehicleType (scm::common::VehicleClass): " << static_cast<int>(obj.input_EgoVehicleType)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft : public ::testing::Test,
                                                                                               public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft, CheckFunction_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(true));
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(data.input_isDriverCooperative));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_hasRightLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsUnfavorable(_)).WillByDefault(Return(!data.input_isLaneChangerFavorable));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.input_isLaneChangeRightSafe));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleClassification()).WillByDefault(Return(data.input_EgoVehicleType));

  std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
  TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
  ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(100.0));

  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft();

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = false,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = false,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = false,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = true,
            .input_isLaneChangeRightSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = false,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kHeavy_truck,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft{
            .input_hasRightLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeRightSafe = false,
            .input_EgoVehicleType = scm::common::VehicleClass::kHeavy_truck,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)}));

/***********************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight *
 ***********************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight
{
  bool input_hasLeftLane;                           // Is there a lane to the left of the driver?
  bool input_isDriverCooperative;                   // Is the driver cooperative with the possible lane changer?
  bool input_isLaneChangerFavorable;                // Could the possible lane changer be accepted as new leading vehicle?
  bool input_isLaneChangeLeftSafe;                  // Is a lane change to the left lane possible?
  scm::common::VehicleClass input_EgoVehicleType;   // Type of Ego vehicle
  LateralAction expected_mostIntenseLateralAction;  // Expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight& obj)
  {
    return os
           << "input_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.input_hasLeftLane)
           << " | input_isDriverCooperative (bool): " << ScmCommons::BooleanToString(obj.input_isDriverCooperative)
           << " | input_isLaneChangerFavorable (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangerFavorable)
           << " | input_isLaneChangeLeftSafe (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeLeftSafe)
           << " | input_EgoVehicleType (scm::common::VehicleClass): " << static_cast<int>(obj.input_EgoVehicleType)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight : public ::testing::Test,
                                                                                                public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight, CheckFunction_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(data.input_isDriverCooperative));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_hasLeftLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsUnfavorable(_)).WillByDefault(Return(!data.input_isLaneChangerFavorable));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.input_isLaneChangeLeftSafe));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(Side::Left)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleClassification()).WillByDefault(Return(data.input_EgoVehicleType));
  std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
  TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
  ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(100.0));
  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight();

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = false,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = false,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = false,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = true,
            .input_isLaneChangeLeftSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = false,
            .input_EgoVehicleType = scm::common::VehicleClass::kMedium_car,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = true,
            .input_EgoVehicleType = scm::common::VehicleClass::kHeavy_truck,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight{
            .input_hasLeftLane = true,
            .input_isDriverCooperative = true,
            .input_isLaneChangerFavorable = false,
            .input_isLaneChangeLeftSafe = false,
            .input_EgoVehicleType =scm::common::VehicleClass::kHeavy_truck,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)}));

/*******************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane *
 *******************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane
{
  bool input_hasRightLane;                          // Is there a lane to the right of the driver?
  bool input_isDriverAvoidingLane;                  // Is the driver cooperative with the possible lane changer?
  bool input_isLaneChangeAllowedByLaneMarkings;     // Is a lane change to the right lane possible?
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane& obj)
  {
    return os
           << "input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasRightLane)
           << " | input_isDriverAvoidingLane (bool): " << ScmCommons::BooleanToString(obj.input_isDriverAvoidingLane)
           << " | input_isLaneChangeAllowedByLaneMarkings (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeAllowedByLaneMarkings)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane : public ::testing::Test,
                                                                                            public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane, CheckFunction_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_hasRightLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(data.input_isLaneChangeAllowedByLaneMarkings));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsAvoidingLaneNextToSuspiciousSideVehicles()).WillByDefault(Return(data.input_isDriverAvoidingLane));
  bool laneChangeInitiated = false;

  if (data.input_hasRightLane && data.input_isDriverAvoidingLane && data.input_isLaneChangeAllowedByLaneMarkings)
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsAvoidingLaneNextToSuspiciousSideVehicles()).WillByDefault(Return(false));
    // EXPECT_CALL(*stubActionManager, CheckIntensitiesForChangingRight()).Times(1).WillRepeatedly(Return(true));
    // EXPECT_CALL(*stubActionManager, CheckIntensitiesForChangingLeft()).Times(0);
    laneChangeInitiated = true;
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane();

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));
  if (!laneChangeInitiated)
  {
    ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
  }
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane{
            .input_hasRightLane = false,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane{
            .input_hasRightLane = true,
            .input_isDriverAvoidingLane = false,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane{
            .input_hasRightLane = true,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane{
            .input_hasRightLane = true,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)})); // impossible LateralAction State since AssertEq shouldn't be called

/********************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane *
 ********************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane
{
  bool input_hasLeftLane;                           // Is there a lane to the left of the driver?
  bool input_isDriverAvoidingLane;                  // Is the driver cooperative with the possible lane changer?
  bool input_isLaneChangeAllowedByLaneMarkings;     // Is a lane change to the left lane possible?
  LateralAction expected_mostIntenseLateralAction;  // expected LateralAction with the highest intensity

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane& obj)
  {
    return os
           << "input_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.input_hasLeftLane)
           << " | input_isDriverAvoidingLane (bool): " << ScmCommons::BooleanToString(obj.input_isDriverAvoidingLane)
           << " | input_isLaneChangeAllowedByLaneMarkings (bool): " << ScmCommons::BooleanToString(obj.input_isLaneChangeAllowedByLaneMarkings)
           << " | expected_mostIntenseLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.expected_mostIntenseLateralAction);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane : public ::testing::Test,
                                                                                             public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane>
{
};

TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane, CheckFunction_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  TEST_HELPER.actionManager.RESET_LATERAL_ACTION_PATTERN_INTENSITIES();
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_hasLeftLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(_)).WillByDefault(Return(data.input_isLaneChangeAllowedByLaneMarkings));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsAvoidingLaneNextToSuspiciousSideVehicles()).WillByDefault(Return(data.input_isDriverAvoidingLane));
  bool laneChangeInitiated = false;

  if (data.input_hasLeftLane && data.input_isDriverAvoidingLane && data.input_isLaneChangeAllowedByLaneMarkings)
  {
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsAvoidingLaneNextToSuspiciousSideVehicles()).WillByDefault(Return(false));
    laneChangeInitiated = true;
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane();

  // Evaluate results
  auto intensityMap = TEST_HELPER.actionManager.GET_LATERAL_ACTION_PATTERN_INTENSITIES();
  LateralAction result_mostIntenseLateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  if (!laneChangeInitiated)
  {
    ASSERT_EQ(data.expected_mostIntenseLateralAction, result_mostIntenseLateralAction);
  }
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane{
            .input_hasLeftLane = false,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane{
            .input_hasLeftLane = true,
            .input_isDriverAvoidingLane = false,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane{
            .input_hasLeftLane = true,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = false,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::LANE_KEEPING)},
        DataFor_GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane{
            .input_hasLeftLane = true,
            .input_isDriverAvoidingLane = true,
            .input_isLaneChangeAllowedByLaneMarkings = true,
            .expected_mostIntenseLateralAction = LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)}));  // impossible LateralAction State since AssertEq shouldn't be called

/*********************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForSideCollisionRisk *
 *********************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk
{
  bool input_laneCrossingConflict;  // Is an actual lane crossing conflict detected with the athor vehicle?
  bool input_laneKeeping;           // Is the driver currently performing a lane keeping action?
  AreaOfInterest sideAoi;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk& obj)
  {
    return os
           << "input_laneCrossingConflict (bool): " << ScmCommons::BooleanToString(obj.input_laneCrossingConflict)
           << " | input_laneKeeping (bool): " << ScmCommons::BooleanToString(obj.input_laneKeeping)
           << " | input_laneKeeping (bool): " << ScmCommons::AreaOfInterestToString(obj.sideAoi);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForSideCollisionRisk : public ::testing::Test,
                                                                                  public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk>
{
};

// TODO Thomas Test disabled as I cannot see what is tested here, except an EXPECT_CALL?
TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForSideCollisionRisk, CheckFunction_GenerateLateralActionPatternIntensitiesForSideCollisionRisk)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ObjectInformationScmExtended mocker{};
  ON_CALL(fakeMicroscopicCharacteristics, GetObjectInformation(_, _)).WillByDefault(Return(&mocker));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasActualLaneCrossingConflict(_)).WillByDefault(Return(data.input_laneCrossingConflict));
  TEST_HELPER.actionManager.SET_LANE_KEEPING_FLAG(data.input_laneKeeping);

  if (!data.input_laneCrossingConflict)
  {
    TEST_HELPER.actionManager.SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else
  {
    if (!data.input_laneKeeping)
    {
      TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest::LEFT_FRONT, false);
    }
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForSideCollisionRisk(data.sideAoi);
};

INSTANTIATE_TEST_SUITE_P(
    DISABLED_Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForSideCollisionRisk,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .sideAoi = AreaOfInterest::LEFT_SIDE},
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = true,
            .input_laneKeeping = false,
            .sideAoi = AreaOfInterest::LEFT_SIDE},
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .sideAoi = AreaOfInterest::LEFT_SIDE},
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .sideAoi = AreaOfInterest::RIGHT_SIDE},
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .sideAoi = AreaOfInterest::RIGHT_SIDE},
        DataFor_GenerateLateralActionPatternIntensitiesForSideCollisionRisk{
            .input_laneCrossingConflict = true,
            .input_laneKeeping = false,
            .sideAoi = AreaOfInterest::RIGHT_SIDE}));

/*************************************************************************************
 * CHECK GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger *
 *************************************************************************************/

struct DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger
{
  bool input_collisionCourse;       // Is collision course detected with the athor vehicle?
  bool input_laneCrossingConflict;  // Is an actual lane crossing conflict detected with the athor vehicle?
  bool input_laneKeeping;           // Is the driver currently performing a lane keeping action?
  AreaOfInterest input_aoi;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger& obj)
  {
    return os
           << "input_collisionCourse (bool): " << ScmCommons::BooleanToString(obj.input_collisionCourse)
           << " | input_laneCrossingConflict (bool): " << ScmCommons::BooleanToString(obj.input_laneCrossingConflict)
           << " | input_laneKeeping (bool): " << ScmCommons::BooleanToString(obj.input_laneKeeping)
           << " | input_laneKeeping (bool): " << ScmCommons::AreaOfInterestToString(obj.input_aoi);
  }
};

class ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger : public ::testing::Test,
                                                                                                  public ::testing::WithParamInterface<DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger>
{
};

// TODO Thomas What is being tested here?
TEST_P(ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger, CheckFunction_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger)
{
  // Get resources for testing
  DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationSideCluster()).WillByDefault(Return(data.input_aoi));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(data.input_collisionCourse));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasActualLaneCrossingConflict(_)).WillByDefault(Return(data.input_laneCrossingConflict));
  TEST_HELPER.actionManager.SET_LANE_KEEPING_FLAG(data.input_laneKeeping);

  if (!data.input_laneCrossingConflict && !data.input_collisionCourse)
  {
    TEST_HELPER.actionManager.SetLateralIntensitiesForNoCollisionThreatSituation();
  }
  else
  {
    if (data.input_laneKeeping)
    {
      TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::LEFT_FRONT, false);
    }
    else
    {
      TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest::LEFT_FRONT, false);
    }
  }

  // Run test
  TEST_HELPER.actionManager.GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger,
    testing::Values(
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = false,
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = false,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = false,
            .input_aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = false,
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = false,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = false,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = true,
            .input_aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger{
            .input_collisionCourse = true,
            .input_laneCrossingConflict = true,
            .input_laneKeeping = false,
            .input_aoi = AreaOfInterest::RIGHT_FRONT}));

/************************************************************
 * CHECK CalculateLateralActionIntensityValuesForEndingLane *
 ************************************************************/

struct DataFor_CalculateLateralActionIntensityValuesForEndingLane
{
  units::length::meter_t input_distanceToEndOfLaneEgo;  // Distance to the agent's current lane in m
  bool expected_flagPastPointOfLaneChangeIntensityMax;  // Expected value of the flag pastPointOfLaneChangeIntensityMax
  double expected_laneChangeIntensity;                  // Expected intensity for lane changing actions
  double expected_laneKeepingIntensity;                 // Expected intensity for lane keeping actions
};

class ActionManager_CalculateLateralActionIntensityValuesForEndingLane : public ::testing::Test,
                                                                         public ::testing::WithParamInterface<DataFor_CalculateLateralActionIntensityValuesForEndingLane>
{
};

TEST_P(ActionManager_CalculateLateralActionIntensityValuesForEndingLane, CheckFunction_CalculateLateralActionIntensityValuesForEndingLane)
{
  // Get resources for testing
  DataFor_CalculateLateralActionIntensityValuesForEndingLane data = GetParam();
  ActionManagerTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(15._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.5_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(10._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(100._m));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  TEST_HELPER.actionManager.SET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX(false);

  // Run Test
  std::pair<double, double> result_intensityValues =
      TEST_HELPER.actionManager.CalculateLateralActionIntensityValuesForEndingLane(0., 1., data.input_distanceToEndOfLaneEgo, 80._m);

  // Evaluate Results
  ASSERT_EQ(data.expected_laneChangeIntensity, result_intensityValues.first);
  ASSERT_EQ(data.expected_laneKeepingIntensity, result_intensityValues.second);
  ASSERT_EQ(data.expected_flagPastPointOfLaneChangeIntensityMax, TEST_HELPER.actionManager.GET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX());
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_CalculateLateralActionIntensityValuesForEndingLane,
    testing::Values(
        DataFor_CalculateLateralActionIntensityValuesForEndingLane{
            .input_distanceToEndOfLaneEgo = 20._m,
            .expected_flagPastPointOfLaneChangeIntensityMax = true,
            .expected_laneChangeIntensity = 1.,
            .expected_laneKeepingIntensity = 0.},
        DataFor_CalculateLateralActionIntensityValuesForEndingLane{
            .input_distanceToEndOfLaneEgo = 70._m,
            .expected_flagPastPointOfLaneChangeIntensityMax = true,
            .expected_laneChangeIntensity = 1.,
            .expected_laneKeepingIntensity = 0.},
        DataFor_CalculateLateralActionIntensityValuesForEndingLane{
            .input_distanceToEndOfLaneEgo = 90._m,
            .expected_flagPastPointOfLaneChangeIntensityMax = false,
            .expected_laneChangeIntensity = (100. - 90.) / (100. - 90. + (90. - 10. * 10. / 2. / 0.75 / 2.5)),
            .expected_laneKeepingIntensity = 1 - ((100. - 90.) / (100. - 90. + (90. - 10. * 10. / 2. / 0.75 / 2.5)))},
        DataFor_CalculateLateralActionIntensityValuesForEndingLane{
            .input_distanceToEndOfLaneEgo = 110._m,
            .expected_flagPastPointOfLaneChangeIntensityMax = false,
            .expected_laneChangeIntensity = 0.,
            .expected_laneKeepingIntensity = 1.}));

/*******************************************
 * CHECK GetAvailableLanesForCollisionCase *
 *******************************************/

struct DataFor_GetAvailableLanesForCollisionCase
{
  AreaOfInterest input_aoiCollision;  // Distance to the agent's current lane in m
  bool input_leftLaneExistance;       // Distance to the agent's current lane in m
  bool input_rightLaneExistance;      // Distance to the agent's current lane in m
  bool expected_hasLeftLane;          // Expected value of the flag pastPointOfLaneChangeIntensityMax
  bool expected_checkLeftLane;        // Expected intensity for lane changing actions
  bool expected_hasRightLane;         // Expected value of the flag pastPointOfLaneChangeIntensityMax
  bool expected_checkRightLane;       // Expected intensity for lane changing actions
  bool expected_noAlternative;        // Expected intensity for lane keeping actions

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GetAvailableLanesForCollisionCase& obj)
  {
    return os
           << "input_aoiCollision (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoiCollision)
           << " | input_leftLaneExistance (bool): " << ScmCommons::BooleanToString(obj.input_leftLaneExistance)
           << " | input_rightLaneExistance (bool): " << ScmCommons::BooleanToString(obj.input_rightLaneExistance)
           << " | expected_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.expected_hasLeftLane)
           << " | expected_checkLeftLane (bool): " << ScmCommons::BooleanToString(obj.expected_checkLeftLane)
           << " | expected_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.expected_hasRightLane)
           << " | expected_checkRightLane (bool): " << ScmCommons::BooleanToString(obj.expected_checkRightLane)
           << " | expected_noAlternative (bool): " << ScmCommons::BooleanToString(obj.expected_noAlternative);
  }
};

class ActionManager_GetAvailableLanesForCollisionCase : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_GetAvailableLanesForCollisionCase>
{
};

TEST_P(ActionManager_GetAvailableLanesForCollisionCase, CheckFunction_GetAvailableLanesForCollisionCase)
{
  // Get resources for testing
  DataFor_GetAvailableLanesForCollisionCase data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_leftLaneExistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_rightLaneExistance));

  // Run Test
  const auto [hasLeftLane, checkLeftLane, hasRightLane, checkRightLane, noAlternativeLane] = TEST_HELPER.actionManager.GetAvailableLanesForCollisionCase(data.input_aoiCollision);

  // Evaluate Results
  ASSERT_EQ(data.expected_hasLeftLane, hasLeftLane);
  ASSERT_EQ(data.expected_checkLeftLane, checkLeftLane);
  ASSERT_EQ(data.expected_hasRightLane, hasRightLane);
  ASSERT_EQ(data.expected_checkRightLane, checkRightLane);
  ASSERT_EQ(data.expected_noAlternative, noAlternativeLane);
};

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GetAvailableLanesForCollisionCase,
    testing::Values(
        // Aoi is not in Front:
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_REAR,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = true,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_REAR,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        // Aoi EgoFront
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = true,
            .expected_hasRightLane = true,
            .expected_checkRightLane = true,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = true,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = true,
            .expected_checkRightLane = true,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::EGO_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        // Aoi LeftFront
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::LEFT_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = true,
            .expected_checkRightLane = true,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::LEFT_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::LEFT_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = true,
            .expected_checkRightLane = true,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::LEFT_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        // Aoi RightFront
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::RIGHT_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = true,
            .expected_hasRightLane = true,
            .expected_checkRightLane = false,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::RIGHT_FRONT,
            .input_leftLaneExistance = true,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = true,
            .expected_checkLeftLane = true,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = false},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::RIGHT_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = true,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = true,
            .expected_checkRightLane = false,
            .expected_noAlternative = true},
        DataFor_GetAvailableLanesForCollisionCase{
            .input_aoiCollision = AreaOfInterest::RIGHT_FRONT,
            .input_leftLaneExistance = false,
            .input_rightLaneExistance = false,
            .expected_hasLeftLane = false,
            .expected_checkLeftLane = false,
            .expected_hasRightLane = false,
            .expected_checkRightLane = false,
            .expected_noAlternative = true}));

/****************************************************
 * CHECK CalculateCriticalActionIntensityParameters *
 ****************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_CalculateCriticalActionIntensitiesParameters
{
  units::length::meter_t input_lateralPositionOwnVehicle;
  units::velocity::meters_per_second_t input_lateralVelocityOwnVehicle;

  CriticalActionIntensitiesParameters expected_criticalActionIntensitiesParameters;
};

class ActionManager_CalculateCriticalActionIntensitiesParameters : public ::testing::Test,
                                                                   public ::testing::WithParamInterface<DataFor_CalculateCriticalActionIntensitiesParameters>
{
};

TEST_P(ActionManager_CalculateCriticalActionIntensitiesParameters, CheckFunction_CalculateParameters)
{
  // Get resources for testing
  DataFor_CalculateCriticalActionIntensitiesParameters data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up tests
  AreaOfInterest aoiTest = AreaOfInterest::EGO_FRONT;
  DriverParameters driverParameters{};
  driverParameters.maximumLateralAcceleration = 7._mps_sq;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(data.input_lateralPositionOwnVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(data.input_lateralVelocityOwnVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(aoiTest, _, _)).WillByDefault(Return(0));

  // Run Test
  TEST_HELPER.actionManager.CalculateCriticalActionIntensitiesParameters(aoiTest);

  // Evaluate results
  CriticalActionIntensitiesParameters actual_criticalActionIntensitiesParameters = TEST_HELPER.actionManager.GET_CRITICAL_ACTION_INTENSITIES_PARAMETERS();

  ASSERT_EQ(data.expected_criticalActionIntensitiesParameters, actual_criticalActionIntensitiesParameters);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_CalculateCriticalActionIntensitiesParameters,
    testing::Values(
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = 1.5_m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{0_mps, 5.6_mps_sq, 8.4_mps_sq, 1.5_m, 0, 1.5_m, 1.5_m, false, false}},
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = -1.5_m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{0_mps, 5.6_mps_sq, 8.4_mps_sq, -1.5_m, 0, -1.5_m, -1.5_m, false, false}},
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 4._mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{4._mps, 5.6_mps_sq, 8.4_mps_sq, 0_m, 0, 0_m, 0_m, true, true}},
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = -4.0_mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{-4._mps, 5.6_mps_sq, 8.4_mps_sq, 0_m, 0, 0_m, 0_m, true, true}},
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = 1.5_m,
            .input_lateralVelocityOwnVehicle = 1._mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{1._mps, 5.6_mps_sq, 8.4_mps_sq, 1.5_m, 0, 1.5_m, 1.5_m, true, true}},
        DataFor_CalculateCriticalActionIntensitiesParameters{
            .input_lateralPositionOwnVehicle = -1.5_m,
            .input_lateralVelocityOwnVehicle = -1.0_mps,
            .expected_criticalActionIntensitiesParameters = CriticalActionIntensitiesParameters{-1._mps, 5.6_mps_sq, 8.4_mps_sq, -1.5_m, 0, -1.5_m, -1.5_m, true, true}}));

/**********************************
 * CHECK CalculateTTCActionLimits *
 **********************************/

/// \brief Data table for definition of individual test cases
struct DataFor_CalculateTTCActionLimits
{
  bool input_evadeTargetOnItsLeft;

  units::time::second_t expected_ttc;
  TTCActionLimits expected_ttcActionLimits;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CalculateTTCActionLimits& obj)
  {
    return os
           << " | input_evadeTargetOnItsLeft (bool): " << obj.input_evadeTargetOnItsLeft
           << " | expected_ttc (double): " << obj.expected_ttc
           << " | expected_ttcActionLimits (TTCActionLimits): " << obj.expected_ttcActionLimits;
  }
};

class ActionManager_CalculateTTCActionLimits : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_CalculateTTCActionLimits>
{
};

TEST_P(ActionManager_CalculateTTCActionLimits, CheckFunction_CalculateTTCActionLimits)
{
  // Get resources for testing
  DataFor_CalculateTTCActionLimits data = GetParam();
  ActionManagerTester TEST_HELPER;

  // Set up tests

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  DriverParameters driverParameters;
  driverParameters.lateralSafetyDistanceForEvading = 0_m;
  driverParameters.maximumLateralAcceleration = 4.5_mps_sq;
  driverParameters.maximumLongitudinalDeceleration = 7_mps_sq;

  OwnVehicleInformationScmExtended mockInfo{};
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(1.0_s));

  TEST_HELPER.actionManager.SET_CRITICAL_ACTION_INTENSITIES_PARAMETERS(CriticalActionIntensitiesParameters{1._mps, 5.6_mps_sq, 8.4_mps_sq, 1.5_m, 0, 1.5_m, 1.5_m, true, true});

  ObjectInformationScmExtended fakeMock{};
  ON_CALL(fakeMicroscopicCharacteristics, UpdateObjectInformation(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&fakeMock));

  // Run Test
  TEST_HELPER.actionManager.CalculateTTCActionLimits(AreaOfInterest::EGO_FRONT, data.input_evadeTargetOnItsLeft);

  // Evaluate results
  auto actual_ttc = TEST_HELPER.actionManager.GET_TTC();
  TTCActionLimits actual_ttcActionLimits = TEST_HELPER.actionManager.GET_TTC_ACTION_LIMITS();

  EXPECT_EQ(data.expected_ttc, actual_ttc);
  ASSERT_EQ(data.expected_ttcActionLimits, actual_ttcActionLimits);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_CalculateTTCActionLimits,
    testing::Values(
        DataFor_CalculateTTCActionLimits{
            .input_evadeTargetOnItsLeft = false,
            .expected_ttc = 1._s,
            .expected_ttcActionLimits = TTCActionLimits{-0._s, 0._s, 0.3571428571428572_s, 0.23809523809523808_s}},
        DataFor_CalculateTTCActionLimits{
            .input_evadeTargetOnItsLeft = true,
            .expected_ttc = 1._s,
            .expected_ttcActionLimits = TTCActionLimits{-0._s, 0._s, 2.7755575615628914e-17_s, 0._s}}));

/*************************************************************
 * CHECK EvaluateActionIntensities - IsCollisionUnavoidable  *
 *************************************************************/

TEST(ActionManager_EvaluateActionIntensities_IsCollisionUnavoidable, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 1_s;
  TEST_HELPER.actionManager._ttc = 0_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 6_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 1.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/************************************************
 * CHECK EvaluateActionIntensities - IsBraking  *
 ************************************************/

TEST(ActionManager_EvaluateActionIntensities_IsBraking, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 1_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 6_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 1.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/************************************************
 * CHECK EvaluateActionIntensities - IsEvading  *
 ************************************************/

TEST(ActionManager_EvaluateActionIntensities_IsEvading, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 1_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 1.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/******************************************************
 * CHECK EvaluateActionIntensities - IsNormalDriving  *
 ******************************************************/

TEST(ActionManager_EvaluateActionIntensities_IsNormalDriving, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;
  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 1_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 1.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/********************************************************************
 * CHECK EvaluateActionIntensities - CollisionUnavoidableOrBraking  *
 ********************************************************************/

TEST(ActionManager_EvaluateActionIntensities_CollisionUnavoidableOrBraking, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 3_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 1.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/********************************************************************
 * CHECK EvaluateActionIntensities - CollisionUnavoidableOrEvading  *
 ********************************************************************/

TEST(ActionManager_EvaluateActionIntensities_CollisionUnavoidableOrEvading, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.steerUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 1.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/*************************************************************
 * CHECK EvaluateActionIntensities - BrakingOrNormalDriving  *
 *************************************************************/

TEST(ActionManager_EvaluateActionIntensities_BrakingOrNormalDriving, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.steerUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 1.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/*************************************************************
 * CHECK EvaluateActionIntensities - NormalDrivingOrEvading  *
 *************************************************************/

TEST(ActionManager_EvaluateActionIntensities_NormalDrivingOrEvading, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerUpperLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 1.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/********************************************************************************************
 * CHECK EvaluateActionIntensities - NormalDrivingOrEvadingOrBrakingOrCollisionUnavoidable  *
 ********************************************************************************************/

TEST(ActionManager_EvaluateActionIntensities_NormalDrivingOrEvadingOrBrakingOrCollisionUnavoidable, CheckFunction_EvaluateActionIntensities)
{
  ActionManagerTester TEST_HELPER;

  TEST_HELPER.actionManager._ttcActionLimits.brakeUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttc = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.brakeLowerLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerUpperLimit = 2_s;
  TEST_HELPER.actionManager._ttcActionLimits.steerLowerLimit = 2_s;

  TEST_HELPER.actionManager._ttcActionLimits.steerUpperLimit = 1_s;

  std::map<CriticalActionState, double> actual_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                            {CriticalActionState::BRAKING_OR_EVADING, 0.},
                                                                            {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                            {CriticalActionState::EVADING_LEFT, 0.},
                                                                            {CriticalActionState::EVADING_RIGHT, 0}};

  // Run Test
  TEST_HELPER.actionManager.EvaluateActionIntensities(actual_criticalActionIntensities, true);

  std::map<CriticalActionState, double> expected_criticalActionIntensities = {{CriticalActionState::BRAKING, 0.},
                                                                              {CriticalActionState::BRAKING_OR_EVADING, 1.},
                                                                              {CriticalActionState::COLLISION_UNAVOIDABLE, 0.},
                                                                              {CriticalActionState::EVADING_LEFT, 0.},
                                                                              {CriticalActionState::EVADING_RIGHT, 0}};

  ASSERT_EQ(expected_criticalActionIntensities.size(), actual_criticalActionIntensities.size());

  for (std::map<CriticalActionState, double>::iterator it = actual_criticalActionIntensities.begin(); it != actual_criticalActionIntensities.end(); ++it)
  {
    EXPECT_DOUBLE_EQ(expected_criticalActionIntensities[it->first], actual_criticalActionIntensities[it->first]);
  }
}

/***********************************************
 * CHECK GenerateCriticalActionIntensityVector *
 ***********************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GenerateCriticalActionIntensityVector
{
  bool input_evadeTargetOnItsLeft;
  units::length::meter_t input_lateralPositionOwnVehicle;
  units::velocity::meters_per_second_t input_lateralVelocityOwnVehicle;
  double input_lateralPositionEgoFront;
  units::length::meter_t input_relativeLongitudinalNetDistance;
  units::velocity::meters_per_second_t input_relativeLongitudinalVelocity;
  ObstructionScm input_Obstruction;
  CriticalActionState expected_mostCriticalActionState;
};

class ActionManager_GenerateCriticalActionIntensityVector : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_GenerateCriticalActionIntensityVector>
{
};

TEST_P(ActionManager_GenerateCriticalActionIntensityVector, CheckFunction_GenerateCriticalActionIntensityVector)
{
  // Get resources for testing
  DataFor_GenerateCriticalActionIntensityVector data = GetParam();
  ActionManagerTester TEST_HELPER;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  // Set up tests
  DriverParameters driverParameters;
  driverParameters.lateralSafetyDistanceForEvading = 0_m;
  driverParameters.maximumLateralAcceleration = 4.5_mps_sq;
  driverParameters.maximumLongitudinalDeceleration = 7_mps_sq;

  OwnVehicleInformationScmExtended fakeInfo{};
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&fakeInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(data.input_lateralVelocityOwnVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(data.input_lateralPositionOwnVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeSwerving, DetermineLateralDistanceToEvade(_, Side::Left, _, _)).WillByDefault(Return(units::length::meter_t(data.input_Obstruction.left)));
  ON_CALL(TEST_HELPER.fakeSwerving, DetermineLateralDistanceToEvade(_, Side::Right, _, _)).WillByDefault(Return(units::length::meter_t(data.input_Obstruction.right)));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(_, _)).WillByDefault(Return(-data.input_relativeLongitudinalNetDistance / data.input_relativeLongitudinalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityDelta(_, _)).WillByDefault(Return(data.input_relativeLongitudinalVelocity));

  // Run Test
  auto intensityMap = TEST_HELPER.actionManager.GenerateCriticalActionIntensityVector(
      AreaOfInterest::EGO_FRONT,
      data.input_evadeTargetOnItsLeft);

  // Evaluate results
  CriticalActionState result;
  double maxIntensity = 0.;
  for (auto i = intensityMap.cbegin(); i != intensityMap.cend(); ++i)
  {
    if (i->second > maxIntensity)
    {
      maxIntensity = i->second;
      result = i->first;
    }
  }

  ASSERT_EQ(data.expected_mostCriticalActionState, result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateCriticalActionIntensityVector,
    testing::Values(
        // Test cases for straight approach
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 15._m,
            .input_relativeLongitudinalVelocity = -20._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::COLLISION_UNAVOIDABLE},  // collision is unavoidable
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 8.5_m,
            .input_relativeLongitudinalVelocity = -10._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING},  // only braking will prevent a collision
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 45._m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::EVADING_LEFT},  // only evading (left) will prevent a collision
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = false,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 45._m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::EVADING_RIGHT},  // only evading (right) will prevent a collision
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 15._m,
            .input_relativeLongitudinalVelocity = -10._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING_OR_EVADING},  // collision can be prevented by either braking or evading
        // Test cases for passing without change in current action
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 1.5_m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = -1.5,
            .input_relativeLongitudinalNetDistance = 7.5_m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(-1._m, 5._m, 3._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING_OR_EVADING},  // passing left because lateral distance is already sufficient
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = false,
            .input_lateralPositionOwnVehicle = -1.5_m,
            .input_lateralVelocityOwnVehicle = 0._mps,
            .input_lateralPositionEgoFront = 1.5,
            .input_relativeLongitudinalNetDistance = 7.5_m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(5._m, -1._m, -3._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING_OR_EVADING},  // passing right because lateral distance is already sufficient
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = 4._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 7.5_m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING_OR_EVADING},  // passing left because lateral velocity is already sufficient
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = false,
            .input_lateralPositionOwnVehicle = 0._m,
            .input_lateralVelocityOwnVehicle = -4._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 7.5_m,
            .input_relativeLongitudinalVelocity = -30._mps,
            .input_Obstruction = ObstructionScm(2._m, 2._m, 0._m),
            .expected_mostCriticalActionState = CriticalActionState::BRAKING_OR_EVADING},  // passing right because lateral velocity is already sufficient
        // Test for different assessment while lane change
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = true,
            .input_lateralPositionOwnVehicle = 1.5_m,
            .input_lateralVelocityOwnVehicle = 1._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 15._m,
            .input_relativeLongitudinalVelocity = -20._mps,
            .input_Obstruction = ObstructionScm(.5_m, 3.5_m, 1.5_m),
            .expected_mostCriticalActionState = CriticalActionState::EVADING_LEFT},  // collision is not unavoidable because already lane changing left
        DataFor_GenerateCriticalActionIntensityVector{
            .input_evadeTargetOnItsLeft = false,
            .input_lateralPositionOwnVehicle = -1.5_m,
            .input_lateralVelocityOwnVehicle = -1._mps,
            .input_lateralPositionEgoFront = 0.,
            .input_relativeLongitudinalNetDistance = 15._m,
            .input_relativeLongitudinalVelocity = -20._mps,
            .input_Obstruction = ObstructionScm(3.5_m, .5_m, -1.5_m),
            .expected_mostCriticalActionState = CriticalActionState::EVADING_RIGHT}));  // collision is not unavoidable because already lane changing right

/***********************************************
 * CHECK DetermineIsLaneChangeModifiedOrAborted *
 ***********************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_DetermineIsLaneChangeModifiedOrAborted
{
  bool unsafeLateralMovement;
  LateralAction lateralAction;
  LateralAction lateralActionLastTick;
  bool isLaneChangePastTransition;

  bool expected_modifyLaneChange;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineIsLaneChangeModifiedOrAborted& obj)
  {
    return os
           << " | unsafeLateralMovement (bool): " << obj.unsafeLateralMovement
           << " | lateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralAction)
           << " | lateralActionLastTick (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralActionLastTick)
           << " | isLaneChangePastTransition (bool): " << ScmCommons::BooleanToString(obj.isLaneChangePastTransition)
           << " | expected_modifyLaneChange (bool): " << ScmCommons::BooleanToString(obj.expected_modifyLaneChange);
  }
};

class ActionManager_DetermineIsLaneChangeModifiedOrAborted : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_DetermineIsLaneChangeModifiedOrAborted>
{
};

TEST_P(ActionManager_DetermineIsLaneChangeModifiedOrAborted, CheckFunction_DetermineIsLaneChangeModifiedOrAborted)
{
  // Get resources for testing
  ActionManagerTester TEST_HELPER;
  DataFor_DetermineIsLaneChangeModifiedOrAborted data = GetParam();

  // Set up tests
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(data.isLaneChangePastTransition));

  TEST_HELPER.actionManager._lateralActionLastTick = data.lateralActionLastTick;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  OwnVehicleInformationScmExtended mockInfo;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));

  // Run Test
  TEST_HELPER.actionManager.DetermineIsLaneChangeModifiedOrAborted(data.unsafeLateralMovement, data.lateralAction);

  bool actual_modifyLaneChange = fakeMicroscopicCharacteristics.GetOwnVehicleInformation()->modifyLaneChange;
  EXPECT_EQ(data.expected_modifyLaneChange, actual_modifyLaneChange);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_DetermineIsLaneChangeModifiedOrAborted,
    testing::Values(
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = false,
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_KEEPING),
            .isLaneChangePastTransition = false,
            .expected_modifyLaneChange = false},
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = true,
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_KEEPING),
            .isLaneChangePastTransition = false,
            .expected_modifyLaneChange = true},
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = true,
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = true,
            .expected_modifyLaneChange = true},
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = true,
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_KEEPING),
            .isLaneChangePastTransition = false,
            .expected_modifyLaneChange = false},
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = true,
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = true,
            .expected_modifyLaneChange = true},
        DataFor_DetermineIsLaneChangeModifiedOrAborted{
            .unsafeLateralMovement = false,
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = true,
            .expected_modifyLaneChange = false}));

/********************************************************
 * CHECK ChooseLateralActionIntensitiesForCollisionCase *
 ********************************************************/

struct DataFor_ChooseLateralActionIntensitiesForCollisionCase
{
  units::length::meter_t widthLaneInformation;
  LateralAction expectedResult;
  bool laneExistenceLeft;
  bool laneExistenceRight;
  units::length::meter_t vehicleWidth;
  bool laneChange;

  // bool expected_modifyLaneChange;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChooseLateralActionIntensitiesForCollisionCase& obj)
  {
    return os
           << " | width (double): " << obj.widthLaneInformation
           << " | expectedResult(LateralAction): " << ScmCommons::LateralActionToString(obj.expectedResult)
           << " | laneExistenceLeft (bool): " << obj.laneExistenceLeft
           << " | laneExistenceRight (bool): " << obj.laneExistenceRight
           << " | vehicle width (double): " << obj.vehicleWidth
           << " | laneChange (bool): " << obj.laneChange;
  }
};

class ActionManager_ChooseLateralActionIntensitiesForCollisionCase : public ::testing::Test,
                                                                     public ::testing::WithParamInterface<DataFor_ChooseLateralActionIntensitiesForCollisionCase>
{
};

TEST_P(ActionManager_ChooseLateralActionIntensitiesForCollisionCase, ChooseLateralActionIntensitiesForCollisionCase)
{
  ActionManagerTester TEST_HELPER;
  DataFor_ChooseLateralActionIntensitiesForCollisionCase data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.laneExistenceLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.laneExistenceRight));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleWidth()).WillByDefault(Return(data.vehicleWidth));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.laneChange));

  LaneInformationGeometrySCM fakeLaneInformationGeometry;
  fakeLaneInformationGeometry.width = data.widthLaneInformation;

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(fakeLaneInformationGeometry));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  // Run test
  TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::EGO_FRONT, false);

  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT));

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_ChooseLateralActionIntensitiesForCollisionCase,
    testing::Values(
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .vehicleWidth = 3._m,
            .laneChange = true},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .vehicleWidth = 3._m,
            .laneChange = true},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .vehicleWidth = 5._m,
            .laneChange = false},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .vehicleWidth = 5._m,
            .laneChange = false},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .vehicleWidth = 5._m,
            .laneChange = true},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .vehicleWidth = 5._m,
            .laneChange = true},
        DataFor_ChooseLateralActionIntensitiesForCollisionCase{
            .widthLaneInformation = 5.0_m,
            .expectedResult = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .laneExistenceLeft = false,
            .laneExistenceRight = false,
            .vehicleWidth = 5._m,
            .laneChange = true}));

/*****************************************************************************************************************************
 * CHECK ChooseLateralActionIntensitiesForCollisionCase - ChooseLateralActionIntensitiesForCollisionCaseCollisionUnavoidable *
 *****************************************************************************************************************************/

TEST(ActionManager_ChooseLateralActionIntensitiesForCollisionCase, ChooseLateralActionIntensitiesForCollisionCaseCollisionUnavoidable)
{
  ActionManagerTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleWidth()).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(true));

  LaneInformationGeometrySCM fakeLaneInformationGeometry;
  fakeLaneInformationGeometry.width = 5.0_m;

  DriverParameters param{};
  param.maximumLongitudinalDeceleration = 1._mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(param));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(fakeLaneInformationGeometry));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityDelta(_, _)).WillByDefault(Return(-2_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(_, _)).WillByDefault(Return(-5_s));

  // Run test
  TEST_HELPER.actionManager.ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest::EGO_FRONT, false);

  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT));

  LateralAction expectedResult{LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE};
  ASSERT_EQ(result, expectedResult);
}

/******************************************************************
 * CHECK GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations *
 ******************************************************************/

struct DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations
{
  LateralAction expectedResult;
  bool laneExistenceLeft;
  bool laneExistenceRight;
  bool sideLaneSafeWithMergePreparation;
  bool laneChange;
  bool sideLaneSafeLeft;
  bool sideLaneSafeRight;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations& obj)
  {
    return os
           << " | expectedResult(LateralAction): " << ScmCommons::LateralActionToString(obj.expectedResult)
           << " | laneExistenceLeft (bool): " << obj.laneExistenceLeft
           << " | laneExistenceRight (bool): " << obj.laneExistenceRight
           << " | sideLaneSafeWithMergePreparation (bool): " << obj.sideLaneSafeWithMergePreparation
           << " | laneChange (bool): " << obj.laneChange
           << " | SideLaneSageLeft (bool): " << obj.sideLaneSafeLeft
           << " | SideLaneSageRight (bool): " << obj.sideLaneSafeRight;
  }
};

class ActionManager_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations : public ::testing::Test,
                                                                               public ::testing::WithParamInterface<DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations>
{
};

TEST_P(ActionManager_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations, GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations)
{
  ActionManagerTester TEST_HELPER;
  DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations data = GetParam();

  // Define test conditions
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  GeometryInformationSCM fakeGeometryInfo{};
  fakeGeometryInfo.Close().distanceToEndOfLane = 100_m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&fakeGeometryInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(20_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.laneExistenceLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.laneExistenceRight));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20_mps));
  std::shared_ptr<FakeSideLaneSafety> fakeSideLaneSafety = std::make_shared<NiceMock<FakeSideLaneSafety>>();
  TEST_HELPER.actionManager.SET_SIDELANESAFETY(fakeSideLaneSafety);
  ON_CALL(*fakeSideLaneSafety, CanFinishLaneChangeSafely(_, _)).WillByDefault(Return(100.0));

  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  TEST_HELPER.actionManager.SET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX(true);

  LateralAction action;
  action.state = LateralAction::State::INTENT_TO_CHANGE_LANES;
  action.direction = LateralAction::Direction::LEFT;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(action));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, CheckLaneChange(_, _)).WillByDefault(Return(data.laneChange));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsSideLaneSafe(Side::Right)).WillByDefault(Return(data.sideLaneSafeRight));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(Side::Right)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSideLaneSafeWithMergePreparation(_)).WillByDefault(Return(data.sideLaneSafeWithMergePreparation));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsSideLaneSafe(Side::Left)).WillByDefault(Return(data.sideLaneSafeLeft));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangePermittedDueToLaneMarkings(Side::Left)).WillByDefault(Return(true));
  // Run Test
  TEST_HELPER.actionManager.GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations();

  // Evaluate Results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result_LateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));

  ASSERT_EQ(data.expectedResult, result_LateralAction);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations,
    testing::Values(
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .laneExistenceLeft = false,
            .laneExistenceRight = false,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = true,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = true,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = true,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .sideLaneSafeWithMergePreparation = true,
            .laneChange = false,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .laneExistenceLeft = false,
            .laneExistenceRight = true,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = false,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .sideLaneSafeWithMergePreparation = true,
            .laneChange = false,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .laneExistenceLeft = true,
            .laneExistenceRight = false,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = false,
            .sideLaneSafeLeft = false,
            .sideLaneSafeRight = false},
        DataFor_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations{
            .expectedResult = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .laneExistenceLeft = true,
            .laneExistenceRight = true,
            .sideLaneSafeWithMergePreparation = false,
            .laneChange = false,
            .sideLaneSafeLeft = true,
            .sideLaneSafeRight = true}));

/**************************************************************************
 * CHECK GenerateLaneChangeIntensitiesForOnlyMesoscopicSituationsZipMerge *
 **************************************************************************/

TEST(ActionManager_GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations, GenerateLaneChangeIntensitiesForOnlyMesoscopicSituationsZipMerge)
{
  ActionManagerTester TEST_HELPER(true);

  // Define test conditions
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  GeometryInformationSCM fakeGeometryInfo{};
  fakeGeometryInfo.Close().distanceToEndOfLane = 100_m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&fakeGeometryInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(20_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocityLaneLeft()).WillByDefault(Return(15_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20_mps));
  DriverParameters driverParameter;
  driverParameter.timeHeadwayFreeLaneChange = 10.0_s;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15.0_mps));

  LaneInformationGeometrySCM laneInformationGeometrySCM;
  laneInformationGeometrySCM.distanceToEndOfLane = 100.0_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInformationGeometrySCM));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  TEST_HELPER.actionManager.SET_FLAG_PAST_POINT_OF_LANE_CHANGE_INTENSITY_MAX(true);

  // Run Test
  TEST_HELPER.actionManager.GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations();

  // Evaluate Results
  auto intensityMap = TEST_HELPER.actionManager.GetActionPatternIntensities();
  LateralAction result_LateralAction = TEST_HELPER.actionManager.GET_MOST_INTENSE_LATERAL_ACTION(intensityMap, LateralAction(LateralAction::State::EXIT, LateralAction::Direction::RIGHT));
  LateralAction expectedResult;
  expectedResult.state = LateralAction::State::LANE_KEEPING;
  ASSERT_EQ(expectedResult, result_LateralAction);
}

/********************
 * CHECK GetUrgency *
 ********************/

struct DataFor_CheckDirection
{
  LateralAction action;
  int expectedDirection;
};

class ActionManager_SetLateralMove : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckDirection>
{
};

TEST_P(ActionManager_SetLateralMove, CheckDirection)
{
  DataFor_CheckDirection data = GetParam();
  ActionManagerTester TEST_HELPER(true);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetDirection(data.expectedDirection)).Times(1);
  TEST_HELPER.actionManager.SetLateralMove(data.action);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_SetLateralMove,
    ::testing::Values(
        DataFor_CheckDirection{
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .expectedDirection = 1},
        DataFor_CheckDirection{
            .action = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::LEFT),
            .expectedDirection = 1},
        DataFor_CheckDirection{
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .expectedDirection = -1},
        DataFor_CheckDirection{
            .action = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::RIGHT),
            .expectedDirection = -1},
        DataFor_CheckDirection{
            .action = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .expectedDirection = 0}));

/*************************************************
 * CHECK GenerateIntensityAndSampleLateralAction *
 * ***********************************************/

TEST(ActionManager_GenerateIntensityAndSampleLateralAction, CheckActionPatternIntensities)
{
  ActionManagerTester TEST_HELPER(true);

  LateralAction lateralAction;
  lateralAction.state = LateralAction::State::INTENT_TO_CHANGE_LANES;
  lateralAction.direction = LateralAction::Direction::LEFT;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(lateralAction));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::FREE_DRIVING));

  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
  ON_CALL(fakeLaneChangeBehavior, CalculateLaneIntensities(_)).WillByDefault(Return());

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(false));

  // unsafe lateral Movement true
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsLateralActionForced()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInSwervingState()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalActionState()).WillByDefault(Return(LongitudinalActionState::APPROACHING));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsLateralMovementStillSafe()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePrevention()).WillByDefault(Return(true));

  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE), 1.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT), 1.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 2.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT), 3.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT), 4.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 5.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT), 6.);
  // has situation chanced true
  TEST_HELPER.actionManager.SET_LAST_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 1.);

  // hasLaneChangeBlockChanged true
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePrevention()).WillByDefault(Return(true));

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.modifyLaneChange = false;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));

  TEST_HELPER.actionManager.GenerateIntensityAndSampleLateralAction();
  auto result = TEST_HELPER.actionManager.GET_ACTION_PATTERN_INTENSITIES_LAST();

  ASSERT_EQ(result.at(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE)), 1);
}

/**********************************************
 * CHECK SetIntensitiesWhileLaneChangeBlocked *
 * ********************************************/

TEST(ActionManager_SetIntensitiesWhileLaneChangeBlocked, CheckActionPatternIntensities)
{
  ActionManagerTester TEST_HELPER(true);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePrevention()).WillByDefault(Return(true));

  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT), 1.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 2.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT), 3.);

  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT), 4.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT), 5.);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT), 6.);

  TEST_HELPER.actionManager.SetIntensitiesWhileLaneChangeBlocked();
  auto result = TEST_HELPER.actionManager.GetActionPatternIntensities();

  ASSERT_EQ(result.at(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)), 3);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::LEFT)), 0);

  ASSERT_EQ(result.at(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT)), 6);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT)), 0);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::URGENT_SWERVING, LateralAction::Direction::RIGHT)), 0);
}

/********************
 * CHECK GetUrgency *
 ********************/

struct DataFor_CheckActionPatternIntensities
{
  bool isPasiveZipMerge;
  bool isCurrentLaneBlocked;
  units::length::meter_t estimateMergePoint;
  double expectedResultLaneKeeping;
  double expectedResultLaneChanging;
  double expectedIntentToLaneChanges;
};

class ActionManager_GenerateLaneChangeIntensitiesForZipMerging : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckActionPatternIntensities>
{
};

TEST_P(ActionManager_GenerateLaneChangeIntensitiesForZipMerging, CheckActionPatternIntensities)
{
  DataFor_CheckActionPatternIntensities data = GetParam();
  ActionManagerTester TEST_HELPER(true);

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING)).WillByDefault(Return(data.isPasiveZipMerge));

  ZipMergeParameters zipParameters;
  zipParameters.relativeLane = RelativeLane::LEFT;
  ON_CALL(TEST_HELPER.fakeZipMerging, GetZipMergeParameters()).WillByDefault(Return(zipParameters));
  ON_CALL(TEST_HELPER.fakeZipMerging, EstimateMergePoint(_)).WillByDefault(Return(data.estimateMergePoint));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneDriveablePerceived()).WillByDefault(Return(data.isCurrentLaneBlocked));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(nullptr));

  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE), 0.0);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT), 0.0);
  TEST_HELPER.actionManager.SET_LATERAL_ACTION_PATTERN_INTENSITY(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT), 0.0);

  TEST_HELPER.actionManager.GenerateLaneChangeIntensitiesForZipMerging();
  auto result = TEST_HELPER.actionManager.GetActionPatternIntensities();
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE)), data.expectedResultLaneKeeping);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT)), data.expectedResultLaneChanging);
  ASSERT_EQ(result.at(LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT)), data.expectedIntentToLaneChanges);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionManager_GenerateLaneChangeIntensitiesForZipMerging,
    ::testing::Values(
        DataFor_CheckActionPatternIntensities{
            .isPasiveZipMerge = true,
            .isCurrentLaneBlocked = true,
            .estimateMergePoint = -1_m,
            .expectedResultLaneKeeping = 1.0,
            .expectedResultLaneChanging = 0.0,
            .expectedIntentToLaneChanges = 0.0},
        DataFor_CheckActionPatternIntensities{
            .isPasiveZipMerge = false,
            .isCurrentLaneBlocked = true,
            .estimateMergePoint = -1_m,
            .expectedResultLaneKeeping = 0.0,
            .expectedResultLaneChanging = 1.0,
            .expectedIntentToLaneChanges = 0.0},
        DataFor_CheckActionPatternIntensities{
            .isPasiveZipMerge = false,
            .isCurrentLaneBlocked = false,
            .estimateMergePoint = 1_m,
            .expectedResultLaneKeeping = 0.0,
            .expectedResultLaneChanging = 0.0,
            .expectedIntentToLaneChanges = 1.0},
        DataFor_CheckActionPatternIntensities{
            .isPasiveZipMerge = false,
            .isCurrentLaneBlocked = true,
            .estimateMergePoint = 1_m,
            .expectedResultLaneKeeping = 1.0,
            .expectedResultLaneChanging = 0.0,
            .expectedIntentToLaneChanges = 0.0}));