/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestMicroscopicCharacteristics.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

TestMicroscopicCharacteristics2::TestMicroscopicCharacteristics2()
    : MicroscopicCharacteristics()
{
  Initialize(0._mps);
}

void TestMicroscopicCharacteristics2::SetSurroundingObjectsData(SurroundingObjectsScmExtended* data)
{
  *_surroundingObjectInformation = *data;
}

TestMicroscopicCharacteristics::TestMicroscopicCharacteristics()
    : MicroscopicCharacteristics()
{
  Initialize(0._mps);
}

void TestMicroscopicCharacteristics::SetExtrapolatedRelativeNetDistance(AreaOfInterest aoi, units::length::meter_t distance)
{
  UpdateObjectInformation(aoi, 0)->relativeLongitudinalDistance = distance;
}

void TestMicroscopicCharacteristics::SetVisible(AreaOfInterest aoi, bool visible)
{
  UpdateObjectInformation(aoi, 0)->exist = visible;
}
void TestMicroscopicCharacteristics::SetExtrapolatedGap(AreaOfInterest aoi, units::time::second_t gap)
{
  UpdateObjectInformation(aoi, 0)->gap = gap;
}

void TestMicroscopicCharacteristics::SetAbsoluteVelocity(AreaOfInterest aoi, units::velocity::meters_per_second_t velocity)
{
  UpdateObjectInformation(aoi, 0)->absoluteVelocity = velocity;
}

void TestMicroscopicCharacteristics::SetLongitudinalVelocity(AreaOfInterest aoi, units::velocity::meters_per_second_t velocity)
{
  UpdateObjectInformation(aoi, 0)->longitudinalVelocity = velocity;
}

void TestMicroscopicCharacteristics::SetExtrapolatedA(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t acceleration)
{
  UpdateObjectInformation(aoi, 0)->acceleration = acceleration;
}

void TestMicroscopicCharacteristics::SetVTargetEgo(units::velocity::meters_per_second_t velocity)
{
  ownVehicleInformation->absoluteVelocityTargeted = velocity;
}

void TestMicroscopicCharacteristics::SetCurrentSituation(Situation situation)
{
  ownVehicleInformation->situation = situation;
}

void TestMicroscopicCharacteristics::SetAccelerationEgo(units::acceleration::meters_per_second_squared_t acc)
{
  ownVehicleInformation->acceleration = acc;
}

void TestMicroscopicCharacteristics::SetLongitudinalVelocityEgo(units::velocity::meters_per_second_t vel)
{
  ownVehicleInformation->longitudinalVelocity = static_cast<units::velocity::meters_per_second_t>(vel);
}

void TestMicroscopicCharacteristics::SetOwnVehicleData(OwnVehicleInformationScmExtended* data)
{
  *ownVehicleInformation = *data;
}

void TestMicroscopicCharacteristics::SetSurroundingObjectsData(SurroundingObjectsScmExtended* data)
{
  *_surroundingObjectInformation = *data;
}

void TestMicroscopicCharacteristics::SetEgoVelocity(units::velocity::meters_per_second_t velocity, bool isEstimatedValue)
{
  if (isEstimatedValue)
  {
    ownVehicleInformation->absoluteVelocity = velocity;
  }
  else
  {
    ownVehicleInformation->absoluteVelocityReal = velocity;
  }
}

SurroundingObjectsScmExtended* TestMicroscopicCharacteristics::GetSurroundingObjectTest()
{
  return _surroundingObjectInformation.get();
}

OwnVehicleInformationSCM* TestMicroscopicCharacteristics::GetOwnVehicleInfoTest()
{
  return ownVehicleInformation.get();
}

OwnVehicleInformationScmExtended* TestMicroscopicCharacteristics::GetOwnVehicleInfoSCMTest()
{
  return ownVehicleInformation.get();
}