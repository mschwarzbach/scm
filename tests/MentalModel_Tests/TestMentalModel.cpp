/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestMentalModel.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeStochastics.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/LaneQueryHelper.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

TestMentalModel::TestMentalModel(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies)
    : MentalModel(100_ms, featureExtractor, mentalCalculations, scmDependencies)
{
  Initialize(driverParameters, vehicleModelParameters, 0_mps, testStochastics);
}

void TestMentalModel::UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics,
                                        std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics)
{
  if (microscopicCharacteristics)
  {
    _microscopicData = std::move(microscopicCharacteristics);
  }

  if (infrastructureCharacteristics)
  {
    _infrastructureCollection = std::move(infrastructureCharacteristics);
  }
}

bool TestMentalModel::GetEndOfLateralMovement()
{
  return _endOfLateralMovement;
}

AreaOfInterest TestMentalModel::TestGetMergeRegulate()
{
  return MentalModel::GetMergeRegulate();
}

void TestMentalModel::SetReactionBaseTime(units::time::second_t time)
{
  _reactionBaseTime = time;
}

void TestMentalModel::SetCooperativeBehaviour(bool cooperative)
{
  _cooperativeBehavior = cooperative;
}

units::time::millisecond_t TestMentalModel::GetUrgentUpdateThreshold()
{
  return _urgentUpdateThreshold;
}

void TestMentalModel::SetDriverParameters(const scm::signal::DriverInput& driverInput)
{
  _scmDependencies.UpdateParametersScmSignal(driverInput);
}

void TestMentalModel::SetAccelerationWishLongitudinalLast(units::acceleration::meters_per_second_squared_t acceleration)
{
  _accelerationWishLongitudinalLast = acceleration;
}

void TestMentalModel::SetLaneChangeWidthTraveled(units::length::meter_t dist)
{
  _laneChangeWidthTraveled = dist;
}

bool TestMentalModel::TestGetIsLaneChangePastTransition()
{
  return _isLaneChangePastTransition;
}

//******************************************************//
//-------------------TestMentalModel2-------------------//
//******************************************************//

TestMentalModel2::TestMentalModel2(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies)
    : MentalModel(100_ms, featureExtractor, mentalCalculations, scmDependencies)
{
  Initialize(driverParameters, vehicleModelParameters, 0_mps, testStochastics);
}

void TestMentalModel2::UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics,
                                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics)
{
  if (microscopicCharacteristics)
  {
    _microscopicData = std::move(microscopicCharacteristics);
  }

  if (infrastructureCharacteristics)
  {
    _infrastructureCollection = std::move(infrastructureCharacteristics);
  }
}

bool TestMentalModel2::GetIsLateralMovementStillSafe()
{
  return _isLateralMovementStillSafe;
}

void TestMentalModel2::SetDriverParameters(const scm::signal::DriverInput& driverInput)
{
  _scmDependencies.UpdateParametersScmSignal(driverInput);
}

//******************************************************//
//-------------------TestMentalModel3-------------------//
//******************************************************//

TestMentalModel3::TestMentalModel3(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies)
    : MentalModel(100_ms, featureExtractor, mentalCalculations, scmDependencies)
{
  Initialize(driverParameters, vehicleModelParameters, 0_mps, testStochastics);
}

void TestMentalModel3::UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics,
                                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics)
{
  if (microscopicCharacteristics)
  {
    _microscopicData = std::move(microscopicCharacteristics);
  }

  if (infrastructureCharacteristics)
  {
    _infrastructureCollection = std::move(infrastructureCharacteristics);
  }
}

void TestMentalModel3::SetDriverParameters(const scm::signal::DriverInput& driverInput)
{
  _scmDependencies.UpdateParametersScmSignal(driverInput);
}

bool TestMentalModel3::TestIsObstacleCloserThanEndOfLane(double laneChangeSafetyFactor, units::velocity::meters_per_second_t velocityEgoAbsolute, units::length::meter_t distanceForFreeLaneChange, units::length::meter_t& distanceToEndOfMergeConsideringObstacle) const
{
  return MentalModel::IsObstacleCloserThanEndOfLane(laneChangeSafetyFactor, velocityEgoAbsolute, distanceForFreeLaneChange, distanceToEndOfMergeConsideringObstacle);
}

//******************************************************//
//-------------------TestMentalModel5-------------------//
//******************************************************//

TestMentalModel5::TestMentalModel5(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies)
    : MentalModel(100_ms, featureExtractor, mentalCalculations, scmDependencies)
{
  Initialize(driverParameters, vehicleModelParameters, 0_mps, testStochastics);
}

void TestMentalModel5::UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristics> microscopicCharacteristics,
                                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics)
{
  if (microscopicCharacteristics)
  {
    _microscopicData = std::move(microscopicCharacteristics);
  }

  if (infrastructureCharacteristics)
  {
    _infrastructureCollection = std::move(infrastructureCharacteristics);
  }
}

void TestMentalModel5::SetAgentGeneralInfluencingDistanceToEndOfLane(units::length::meter_t dist)
{
  _agentsGeneralInfluencingDistanceToEndOfLane = dist;
}