/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeStochastics.h"
#include "../MentalCalculations_Tests/TestMentalCalculations.h"
#include "MicroscopicCharacteristicsInterface.h"
#include "ScmDriver.h"
#include "module/driver/src/MentalCalculationsInterface.h"
#include "module/driver/src/MentalModel.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::Matcher;

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

class TestMentalModel;

// bool operator==(Obstruction const &a, Obstruction const &b);
bool operator==(ObjectInformationScmExtended const &a, ObjectInformationScmExtended const &b);
bool operator==(SurroundingObjectsScmExtended const &a, SurroundingObjectsScmExtended const &b);
bool operator==(GeometryInformationSCM const &a, GeometryInformationSCM const &b);
bool operator==(InformationRequest const &a, InformationRequest const &b);
bool operator==(TrafficRuleInformationScmExtended const &a, TrafficRuleInformationScmExtended const &b);  // Only data required by test is included here!
bool CheckLaneMarkingRelativeDistance(const std::vector<scm::LaneMarking::Entity> &laneMarkings1, const std::vector<scm::LaneMarking::Entity> &laneMarkings2);
bool CheckTrafficSignsRelativeDistance(const std::vector<scm::CommonTrafficSign::Entity> &trafficSigns1, const std::vector<scm::CommonTrafficSign::Entity> &trafficSigns2);

class TestMentalModel : public MentalModel
{
public:
  TestMentalModel(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies);

  virtual ~TestMentalModel() = default;

  MOCK_CONST_METHOD0(GetAbsoluteVelocityTargeted, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD2(GetLaneExistence, bool(RelativeLane lane, bool isEmergency));
  MOCK_CONST_METHOD0(GetVehicleClassification, scm::common::VehicleClass());
  MOCK_CONST_METHOD0(GetIsLaneChangePastTransition, bool());
  MOCK_CONST_METHOD0(IsEndOfLateralMovement, bool());
  MOCK_CONST_METHOD0(GetLaneChangeWish, int());
  MOCK_CONST_METHOD0(GetMeanVelocityLaneLeft, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetMeanVelocityLaneRight, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetInteriorHudExistence, bool());
  MOCK_CONST_METHOD0(GetInteriorInfotainmentExistence, bool());
  MOCK_CONST_METHOD0(GetEgoInfo, Merge::EgoInfo());
  MOCK_CONST_METHOD0(IsAbleToCooperateByAccelerating, bool());
  MOCK_CONST_METHOD2(GetObstruction, ObstructionScm(AreaOfInterest, int));
  MOCK_METHOD4(IsDataForAoiReliable, bool(AreaOfInterest aoi, FieldOfViewAssignment requirement, double priority, InformationRequestTrigger requester));

  MOCK_CONST_METHOD0(GetMicroscopicData, MicroscopicCharacteristicsInterface *());
  MOCK_METHOD1(IsSideLaneSafeWithMergePreparation, bool(AreaOfInterest aoi));
  MOCK_METHOD0(CheckSituationCooperativeBehavior, void());
  MOCK_CONST_METHOD0(GetTrafficJamVelocityThreshold, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetFormRescueLaneVelocityThreshold, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetVehicleWidth, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLateralOffsetNeutralPosition, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLateralOffsetNeutralPositionRescueLane, units::length::meter_t());
  MOCK_CONST_METHOD0(GetVelocityMeanLaneLeft, double());
  MOCK_CONST_METHOD0(GetVelocityMeanLaneRight, double());
  MOCK_CONST_METHOD0(GetVelocityLegalLeft, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetVelocityLegalRight, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD1(GetAbsoluteVelocityEgo, units::velocity::meters_per_second_t(bool isEstimatedValue));
  MOCK_CONST_METHOD0(GetTrafficSigns, std::vector<std::vector<scm::CommonTrafficSign::Entity>>());
  MOCK_CONST_METHOD0(GetMergeRegulate, AreaOfInterest());
  MOCK_CONST_METHOD0(IsMergePreparationActive, bool());
  MOCK_CONST_METHOD1(GetRelativeNetDistance, units::length::meter_t(AreaOfInterest aoi));
  MOCK_METHOD1(CalculateMicroscopicData, void(bool));
  MOCK_CONST_METHOD8(DetermineTransitionTargetAoi, AreaOfInterest(AreaOfInterest aoiOrigin, bool forward, bool backward, bool left, bool right, const SurroundingObjectsSCM *surroundingObjects_GroundTruth, int indexSideAoi, int realIndex));
  MOCK_CONST_METHOD5(AoiTransition, void(AreaOfInterest aoiOrigin, AreaOfInterest aoiTarget, bool transitionForward, bool transitionBackward, int indexSideAoiOrigin));
  MOCK_CONST_METHOD1(GetSituationRisk, Risk(Situation situation));
  MOCK_CONST_METHOD1(IsSituationAnticipated, bool(Situation situation));
  MOCK_CONST_METHOD0(GetCausingVehicleOfSituationFrontCluster, AreaOfInterest());
  MOCK_CONST_METHOD0(GetCausingVehicleOfSituationSideCluster, AreaOfInterest());
  MOCK_CONST_METHOD0(GetIsEndOfLateralMovement, bool());
  MOCK_CONST_METHOD0(GetInfluencingDistanceToEndOfLane, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLateralAction, LateralAction());
  MOCK_CONST_METHOD1(GetMeanVelocity, units::velocity::meters_per_second_t(SurroundingLane));
  MOCK_CONST_METHOD0(GetAccelerationLimitForMerging, units::acceleration::meters_per_second_squared_t());

  void UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics = nullptr,
                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristicsInterface = nullptr);

  void SetIntruder(AreaOfInterest intruder);
  void SetReactionBaseTime(units::time::second_t time);
  void SetCooperativeBehaviour(bool cooperative);
  void SetDriverParameters(const scm::signal::DriverInput &driverInput);
  void SetAccelerationWishLongitudinalLast(units::acceleration::meters_per_second_squared_t acceleration);
  void SetLaneChangeWidthTraveled(units::length::meter_t dist);

  units::time::millisecond_t GetUrgentUpdateThreshold();
  bool GetEndOfLateralMovement();

  void TestSetInits(std::vector<double> initVectorAoi, std::vector<double> initVectorMeso);
  AreaOfInterest TestGetMergeRegulate();
  void TestCorrectAoiDataDueToLongitudinalTransitionFromSideArea(AreaOfInterest aoi, bool transitionToFront, int sideAoiIndex) const;
  bool TestGetIsLaneChangePastTransition();
};

class TestMentalModel2 : public MentalModel
{
public:
  TestMentalModel2(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies);

  virtual ~TestMentalModel2() = default;

  void UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics = nullptr,
                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics = nullptr);

  MOCK_CONST_METHOD0(DetermineVelocityReasonRightOvertakingProhibition, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD4(AddInformationRequest, std::vector<double>(AreaOfInterest, FieldOfViewAssignment, double, InformationRequestTrigger));
  MOCK_CONST_METHOD0(GetMergeRegulate, AreaOfInterest());
  MOCK_CONST_METHOD2(GetLaneExistence, bool(RelativeLane, bool));
  MOCK_CONST_METHOD1(IsDataForLaneChangeReliable, bool(Side));
  MOCK_CONST_METHOD0(GetMicroscopicData, MicroscopicCharacteristics *());
  MOCK_CONST_METHOD0(GetInfluencingDistanceToEndOfLane, units::length::meter_t());
  MOCK_CONST_METHOD0(GetTrafficJamVelocityThreshold, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD1(IsInconsistent, bool(AreaOfInterest));

  bool GetIsLateralMovementStillSafe();
  void SetDriverParameters(const scm::signal::DriverInput &driverInput);
};

class TestMentalModel3 : public MentalModel
{
public:
  TestMentalModel3(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies);

  virtual ~TestMentalModel3() = default;

  void UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics = nullptr,
                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics = nullptr);

  MOCK_CONST_METHOD2(GetIsVehicleVisible, bool(AreaOfInterest, int));
  MOCK_CONST_METHOD0(GetMicroscopicData, MicroscopicCharacteristicsInterface *());
  MOCK_CONST_METHOD1(GetRelativeNetDistance, units::length::meter_t(AreaOfInterest aoi));
  MOCK_CONST_METHOD2(GetGap, units::time::second_t(AreaOfInterest aoi, int sideAoiIndex));
  MOCK_CONST_METHOD2(GetTtc, units::time::second_t(AreaOfInterest aoi, int sideAoiIndex));
  MOCK_CONST_METHOD0(GetVehicleWidth, units::length::meter_t());
  MOCK_CONST_METHOD0(GetDistanceToBoundaryRight, units::length::meter_t());
  MOCK_CONST_METHOD0(GetDistanceToBoundaryLeft, units::length::meter_t());
  MOCK_CONST_METHOD2(GetTauDot, double(AreaOfInterest, int));
  MOCK_CONST_METHOD0(IsAlreadyPreparingToMerge, bool());
  MOCK_CONST_METHOD0(GetLateralVelocity, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetLateralPosition, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLongitudinalVelocityEgo, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetCausingVehicleOfSituationFrontCluster, AreaOfInterest());
  MOCK_CONST_METHOD0(GetCurrentSituation, Situation());
  MOCK_CONST_METHOD1(IsMesoscopicSituationActive, bool(MesoscopicSituation));

  void SetDriverParameters(const scm::signal::DriverInput &driverInput);
  bool TestIsObstacleCloserThanEndOfLane(double laneChangeSafetyFactor, units::velocity::meters_per_second_t velocityEgoAbsolute, units::length::meter_t distanceForFreeLaneChange, units::length::meter_t& distanceToEndOfMergeConsideringObstacle) const;
};

class TestMentalModel5 : public MentalModel
{
public:
  TestMentalModel5(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleModelParameters, FakeStochastics* testStochastics, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies);

  virtual ~TestMentalModel5() = default;

  void UpdateMentalModel(std::unique_ptr<MicroscopicCharacteristics> microscopicCharacteristics = nullptr,
                         std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristicsInterface = nullptr);

  MOCK_CONST_METHOD2(GetVehicle, const SurroundingVehicleInterface *(AreaOfInterest, int));
  MOCK_CONST_METHOD2(GetObstruction, ObstructionScm(AreaOfInterest, int));
  MOCK_CONST_METHOD1(GetAbsoluteVelocityEgo, units::velocity::meters_per_second_t(bool isEstimatedValue));

  void SetAgentGeneralInfluencingDistanceToEndOfLane(units::length::meter_t dist);
};