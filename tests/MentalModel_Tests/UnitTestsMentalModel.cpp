/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <limits>
#include <memory>
#include <ostream>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeScmDependencies.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeTrafficFlow.h"
#include "Fakes/FakeActionImplementation.h"
#include "Fakes/FakeHighCognitive.h"
#include "Fakes/FakeMerging.h"
#include "Fakes/FakeSurroundingVehicleQuery.h"
#include "MentalModel.h"
#include "MicroscopicCharacteristics.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactory.h"
#include "SurroundingVehicles_Tests/TestSurroundingVehicles.h"
#include "TestMentalModel.h"
#include "TestMicroscopicCharacteristics.h"
#include "gmock/gmock.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/LaneQueryHelper.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct MentalModelTester
{
  class MentalModelUnderTest : MentalModel
  {
  public:
    template <typename... Args>
    MentalModelUnderTest(Args&&... args)
        : MentalModel{std::forward<Args>(args)...} {};

    using MentalModel::AddInformationRequest;
    using MentalModel::AdjustMinDistanceDueToCooperation;
    using MentalModel::AdvanceAdjustmentBaseTime;
    using MentalModel::AdvanceReactionBaseTime;
    using MentalModel::CalculateAdjustmentBaseTime;
    using MentalModel::CheckForEgoLaneTransition;
    using MentalModel::CheckForEndOfLaneChange;
    using MentalModel::CheckIsLaneChangeProhibitionIgnored;
    using MentalModel::CheckSituationCooperativeBehavior;
    using MentalModel::ClearInconsistency;
    using MentalModel::CompareInformationRequest;
    using MentalModel::DetermineDistanceTraveledLeft;
    using MentalModel::DetermineDistanceTraveledRight;
    using MentalModel::DetermineIndexOfSideObject;
    using MentalModel::DetermineVelocityReasonCurvature;
    using MentalModel::DetermineVelocityReasonForDetectedTrafficJam;
    using MentalModel::DetermineVelocityReasonLaneWidth;
    using MentalModel::DetermineVelocityReasonPassingSlowPlatoon;
    using MentalModel::DetermineVelocityReasonRightOvertakingProhibition;
    using MentalModel::DetermineVelocityReasonSight;
    using MentalModel::DrawShoulderLaneUsage;
    using MentalModel::GenerateTopDownAoiScoring;
    using MentalModel::GetAbsoluteVelocity;
    using MentalModel::GetAbsoluteVelocityEgo;
    using MentalModel::GetAbsoluteVelocityTargeted;
    using MentalModel::GetAcceleration;
    using MentalModel::GetAccelerationDelta;
    using MentalModel::GetAccelerationLimitForMerging;
    using MentalModel::GetActiveMesoscopicSituations;
    using MentalModel::GetAdjustmentBaseTime;
    using MentalModel::GetAgentCooperationFactor;
    using MentalModel::GetAgentCooperationFactorForSuspiciousBehaviourEvasion;
    using MentalModel::GetAgentId;
    using MentalModel::GetCarQueuingDistance;
    using MentalModel::GetCarRestartDistance;
    using MentalModel::GetCausingVehicleOfSituationFrontCluster;
    using MentalModel::GetCausingVehicleOfSituationSideCluster;
    using MentalModel::GetClosestRelativeLaneIdForJunctionIngoing;
    using MentalModel::GetCollisionState;
    using MentalModel::GetCombinedAcousticOpticSignal;
    using MentalModel::GetComfortLongitudinalAcceleration;
    using MentalModel::GetComfortLongitudinalDeceleration;
    using MentalModel::GetCooperativeBehavior;
    using MentalModel::GetCurrentDirection;
    using MentalModel::GetCurrentSituation;
    using MentalModel::GetCurvatureInPreviewDistance;
    using MentalModel::GetCurvatureOnCurrentPosition;
    using MentalModel::GetCycleTime;
    using MentalModel::GetDecelerationLimitForMerging;
    using MentalModel::GetDesiredVelocity;
    using MentalModel::GetDistanceToBoundaryLeft;
    using MentalModel::GetDistanceToBoundaryRight;
    using MentalModel::GetDistanceToEndOfLane;
    using MentalModel::GetDistanceToEndOfNextExit;
    using MentalModel::GetDistanceToStartOfNextExit;
    using MentalModel::GetDistanceTowardsEgoLane;
    using MentalModel::GetDurationCurrentSituation;
    using MentalModel::GetEgoLaneKeepingQuotaFulfilled;
    using MentalModel::GetEgoLaneWidth;
    using MentalModel::GetFlasherActiviationProbability;
    using MentalModel::GetFormRescueLaneVelocityThreshold;
    using MentalModel::GetFovea;
    using MentalModel::GetGap;
    using MentalModel::GetHasMesoscopicSituationChanged;
    using MentalModel::GetHasSituationChanged;
    using MentalModel::GetHeading;
    using MentalModel::GetHighCognitiveLaneChangeProbability;
    using MentalModel::GetHorizontalGazeAngle;
    using MentalModel::GetInconsistentAois;
    using MentalModel::GetIndicatorActiviationProbability;
    using MentalModel::GetIndicatorState;
    using MentalModel::GetIsAnticipating;
    using MentalModel::GetIsEndOfLateralMovement;
    using MentalModel::GetIsLaneChangePastTransition;
    using MentalModel::GetIsLateralActionForced;
    using MentalModel::GetIsLateralMovementStillSafe;
    using MentalModel::GetIsMergeRegulate;
    using MentalModel::GetIsNewEgoLane;
    using MentalModel::GetIsStaticObject;
    using MentalModel::GetLaneChangePrevention;
    using MentalModel::GetLaneChangePreventionAtSpawn;
    using MentalModel::GetLaneChangePreventionExternalControl;
    using MentalModel::GetLaneChangeWidthTraveled;
    using MentalModel::GetLaneExistence;
    using MentalModel::GetLastLateralActionState;
    using MentalModel::GetLateralAction;
    using MentalModel::GetLateralOffsetNeutralPosition;
    using MentalModel::GetLateralOffsetNeutralPositionRescueLane;
    using MentalModel::GetLateralPositionFrontAxle;
    using MentalModel::GetLateralVelocity;
    using MentalModel::GetLateralVelocityDelta;
    using MentalModel::GetLateralVelocityFrontAxle;
    using MentalModel::GetLateralVelocityTowardsEgoLane;
    using MentalModel::GetLeadingVehicleId;
    using MentalModel::GetLegalVelocity;
    using MentalModel::GetLongitudinalActionState;
    using MentalModel::GetLongitudinalObstruction;
    using MentalModel::GetLongitudinalVelocity;
    using MentalModel::GetLongitudinalVelocityDelta;
    using MentalModel::GetLongitudinalVelocityEgo;
    using MentalModel::GetMeanVelocity;
    using MentalModel::GetMergeRegulateId;
    using MentalModel::GetMicroscopicData;
    using MentalModel::GetObjectToEvade;
    using MentalModel::GetObstruction;
    using MentalModel::GetOuterKeepingQuotaFulfilled;
    using MentalModel::GetPeriphery;
    using MentalModel::GetPlannedLaneChangeDimensions;
    using MentalModel::GetPossibleRegulateIds;
    using MentalModel::GetPreviewDistance;
    using MentalModel::GetPrioritySinceLastUpdate;
    using MentalModel::GetProjectedDimensions;
    using MentalModel::GetProjectedVehicleWidth;
    using MentalModel::GetProjectedVehicleWidthLeft;
    using MentalModel::GetProjectedVehicleWidthRight;
    using MentalModel::GetReactionBaseTime;
    using MentalModel::GetReductionFactorUnderUrgency;
    using MentalModel::GetRelativeNetDistance;
    using MentalModel::GetReliabilityMap;
    using MentalModel::GetRemainInLaneChangeState;
    using MentalModel::GetRemainInRealignState;
    using MentalModel::GetRequesterPriority;
    using MentalModel::GetSituationRisk;
    using MentalModel::GetTauDot;
    using MentalModel::GetThresholdLooming;
    using MentalModel::GetTime;
    using MentalModel::GetTimeSinceLastUpdate;
    using MentalModel::GetTrafficJamVelocityThreshold;
    using MentalModel::GetTrafficSigns;
    using MentalModel::GetTtc;
    using MentalModel::GetUfov;
    using MentalModel::GetUrgencyReductionFactor;
    using MentalModel::GetUrgentUpdateThreshold;
    using MentalModel::GetVehicleClassification;
    using MentalModel::GetVehicleHeight;
    using MentalModel::GetVehicleLength;
    using MentalModel::GetVehicleWidth;
    using MentalModel::GetVelocityViolationDelta;
    using MentalModel::GetVisibilityDistance;
    using MentalModel::GetVisibilityFactorForHighwayExit;
    using MentalModel::GetZipMerge;
    using MentalModel::HasEgoLateralDistanceReached;
    using MentalModel::HasLeadingVehicleChanged;
    using MentalModel::ImplementStochasticActivation;
    using MentalModel::IsAdjustmentBaseTimeExpired;
    using MentalModel::IsEarlyWithUpdate;
    using MentalModel::IsFovea;
    using MentalModel::IsFrontSituation;
    using MentalModel::IsMergePreparationActive;
    using MentalModel::IsObjectInRearArea;
    using MentalModel::IsObjectInSideArea;
    using MentalModel::IsObjectInSideOrRearArea;
    using MentalModel::IsObjectInSurroundingArea;
    using MentalModel::IsObstacleCloserThanEndOfLane;
    using MentalModel::IsPointVisible;
    using MentalModel::IsRightHandTraffic;
    using MentalModel::IsShoulderLaneUsageLegit;
    using MentalModel::IsSideLaneSafeWithMergePreparation;
    using MentalModel::IsSideSideLaneObjectChangingIntoSide;
    using MentalModel::IsSideSituation;
    using MentalModel::IsSituationAnticipated;
    using MentalModel::IsUrgentlyLateWithUpdate;
    using MentalModel::MarkInconsistent;
    using MentalModel::ReevaluateCurrentLaneChange;
    using MentalModel::ReevaluateMergeGap;
    using MentalModel::ResetInformationRequests;
    using MentalModel::ResetLaneChangeWidthTraveled;
    using MentalModel::ResetReactionBaseTime;
    using MentalModel::ResetSpeedLimitSign;
    using MentalModel::ResetTransitionState;
    using MentalModel::ScaleTopDownRequestMap;
    using MentalModel::SetAbsoluteVelocityTargeted;
    using MentalModel::SetAdjustmentBaseTime;
    using MentalModel::SetAgentId;
    using MentalModel::SetAoiMapping;
    using MentalModel::SetCausingVehicleOfSituationFrontCluster;
    using MentalModel::SetCausingVehicleOfSituationSideCluster;
    using MentalModel::SetCombinedAcousticOpticSignal;
    using MentalModel::SetCurrentSituation;
    using MentalModel::SetDurationCurrentSituation;
    using MentalModel::SetHorizontalGazeAngle;
    using MentalModel::SetLaneChangeWidthTraveled;
    using MentalModel::SetLastLateralAction;
    using MentalModel::SetLateralAction;
    using MentalModel::SetLeadingVehicleId;
    using MentalModel::SetLongitudinalActionState;
    using MentalModel::SetMergeGap;
    using MentalModel::SetMergeRegulate;
    using MentalModel::SetMergeRegulateId;
    using MentalModel::SetPlannedLaneChangeDimensions;
    using MentalModel::SetPossibleRegulateIds;
    using MentalModel::SetReactionBaseTime;
    using MentalModel::SetRemainInLaneChangeState;
    using MentalModel::SetRemainInRealignState;
    using MentalModel::SetRemainInSwervingState;
    using MentalModel::SetTime;
    using MentalModel::SetUrgentUpdateThreshold;
    using MentalModel::SetZipMerge;
    using MentalModel::Transition;
    using MentalModel::UpdateReliabilityMap;
    using MentalModel::UpdateSurroundingVehicles;

    using MentalModel::_infrastructureCollection;
    using MentalModel::_isLaneChangePastTransition;
    using MentalModel::_mesoscopicSituations;
    using MentalModel::_microscopicData;
    using MentalModel::_updateThresholdFovea;
    using MentalModel::_updateThresholdPeriphery;
    using MentalModel::_updateThresholdUfov;

    void SET_MESOSCOPIC_SITUATION(MesoscopicSituation situation, bool value)
    {
      _mesoscopicSituations.at(situation) = value;
    }
    void SET_MESOSCOPIC_SITUATION_ACTIVE(MesoscopicSituation situation)
    {
      _mesoscopicSituations[situation] = true;
    }
    void SET_TRAFFIC_FLOW(std::shared_ptr<FakeTrafficFlow> trafficFlow)
    {
      _trafficFlow = trafficFlow;
    }

    void SET_MERGE_REGULATE_ID(int mergeRegulateId)
    {
      _mergeRegulateId = mergeRegulateId;
    }

    void SET_IS_NEW_EGOLANE(bool isNewEgoLane)
    {
      _isNewEgoLane = isNewEgoLane;
    }

    void UPDATE_MENTALMODEL(std::unique_ptr<MicroscopicCharacteristicsInterface> microscopicCharacteristics,
                            std::unique_ptr<InfrastructureCharacteristicsInterface> infrastructureCharacteristics)
    {
      if (microscopicCharacteristics)
      {
        _microscopicData = std::move(microscopicCharacteristics);
      }

      if (infrastructureCharacteristics)
      {
        _infrastructureCollection = std::move(infrastructureCharacteristics);
      }
    }

    void SET_THRESHOLD(double fovea, double ufov, double periphery)
    {
      _updateThresholdFovea = fovea;
      _updateThresholdUfov = ufov;
      _updateThresholdPeriphery = periphery;
    }

    bool GET_IS_LANE_CHANGE_PAST_TRANSITION()
    {
      return _isLaneChangePastTransition;
    }

    bool TEST_CONSISTENT(AreaOfInterest aoi)
    {
      return !IsInconsistent(aoi);
    }

    bool TEST_CONSISTENT(const std::vector<AreaOfInterest>& aois)
    {
      return std::none_of(begin(aois), end(aois), [this](const AreaOfInterest aoi)
                          { return IsInconsistent(aoi); });
    }

    bool TEST_INCONSISTENT(AreaOfInterest aoi)
    {
      return IsInconsistent(aoi);
    }

    bool TEST_INCONSISTENT(const std::vector<AreaOfInterest>& aois)
    {
      return std::all_of(begin(aois), end(aois), [this](const AreaOfInterest aoi)
                         { return IsInconsistent(aoi); });
    }

    void SET_CYCLE_TIME(units::time::millisecond_t time)
    {
      _cycleTime = time;
    }

    void SET_VEHICLE_QUERY_FACTORY(std::unique_ptr<SurroundingVehicleQueryFactoryInterface> vehicleQueryFactory)
    {
      _vehicleQueryFactory = std::move(vehicleQueryFactory);
    }

    void SET_STOCHASTICS(StochasticsInterface* stochastics)
    {
      _stochastics = stochastics;
    }

    void SET_LEADING_VEHICLE_CHANGED(bool isLeadingVehicleChanged)
    {
      _leadingVehicleChanged = isLeadingVehicleChanged;
    }

    void SET_LANE_CHANGE_PAST_TRANSITION(bool value)
    {
      _isLaneChangePastTransition = value;
    }

    void SET_MERGE_GAP(Merge::Gap gap)
    {
      _mergeGap = gap;
    }

    void SET_MERGING(std::unique_ptr<MergingInterface> merging)
    {
      _merging = std::move(merging);
    }

    void SET_PROB_INDICATOR_ACTIVATION(double probIndicatorActivation)
    {
      _probIndicatorActivation = probIndicatorActivation;
    }

    void SET_PROB_FLASHER_ACTIVATION(double probIndicatorActivation)
    {
      _probFlasherActivation = probIndicatorActivation;
    }

    void SET_ADJUSTMENT_BASE_TIME(units::time::second_t adjustmentBaseTime)
    {
      _adjustmentBaseTime = adjustmentBaseTime;
    }

    units::acceleration::meters_per_second_squared_t GET_ACCELERATION_WISH_LONGITUDINAL_LAST()
    {
      return _accelerationWishLongitudinalLast;
    }

    void SET_HIGH_COGNITIVE(std::unique_ptr<HighCognitiveInterface> highCognitive)
    {
      _highCognitive = std::move(highCognitive);
    }

    void SET_COOPERATIVE_BEHAVIOR(bool cooperativeBehavior)
    {
      _cooperativeBehavior = cooperativeBehavior;
    }
  };

  template <typename T>
  T& WITH_SCM_DEPENDENCIES(T& fakeScmDependencies)
  {
    DriverParameters driverParameters;
    driverParameters.desiredVelocity = 123.4_mps;
    ON_CALL(fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));
    ON_CALL(fakeScmDependencies, GetCycleTime()).WillByDefault(Return(100_ms));
    return fakeScmDependencies;
  }

  MentalModelTester()
      : fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeScmDependencies{},
        mentalModel(100_ms,
                    fakeFeatureExtractor,
                    fakeMentalCalculations,
                    WITH_SCM_DEPENDENCIES(fakeScmDependencies))
  {
  }

  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeScmDependencies> fakeScmDependencies;

  MentalModelUnderTest mentalModel;
};

/*************************************
 * CHECK ReevaluateCurrentLaneChange *
 *************************************/

/// \brief Data table
struct DataFor_ReevaluateCurrentLaneChange
{
  bool input_RemainInLaneChangeState;
  bool input_RemainInSwervingState;
  bool input_IsLaneChangeSafe;
  bool result_IsLateralMovementStillSave;
};

class MentalModel_ReevaluateCurrentLaneChange : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_ReevaluateCurrentLaneChange>
{
};

TEST_P(MentalModel_ReevaluateCurrentLaneChange, MentalModel_CheckFunction_ReevaluateCurrentLaneChange)
{
  // Create required classes for test
  MentalModelTester TEST_HELPER;
  DataFor_ReevaluateCurrentLaneChange data = GetParam();

  // Set up test
  TEST_HELPER.mentalModel.SetRemainInLaneChangeState(data.input_RemainInLaneChangeState);
  TEST_HELPER.mentalModel.SetRemainInSwervingState(data.input_RemainInSwervingState);

  auto tmpMicroscopicCharacteristics = std::make_unique<TestMicroscopicCharacteristics2>();
  auto stubMicroscopicCharacteristics = tmpMicroscopicCharacteristics.get();

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(tmpMicroscopicCharacteristics), nullptr);
  stubMicroscopicCharacteristics->UpdateOwnVehicleData()->direction = 0;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetDistanceToPointOfNoReturnForBrakingToEndOfLane(_, RelativeLane::EGO)).WillByDefault(Return(1.0_m));

  bool latMovement = data.input_RemainInLaneChangeState || data.input_RemainInSwervingState;
  EXPECT_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneChangeSafe(_)).Times(static_cast<int>(latMovement)).WillRepeatedly(Return(data.input_IsLaneChangeSafe));

  // Call test
  TEST_HELPER.mentalModel.ReevaluateCurrentLaneChange();

  // Evaluate results
  ASSERT_EQ(TEST_HELPER.mentalModel.GetIsLateralMovementStillSafe(), data.result_IsLateralMovementStillSave);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_ReevaluateCurrentLaneChange,
    testing::Values(
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_IsLaneChangeSafe = false,
            .result_IsLateralMovementStillSave = true},
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_IsLaneChangeSafe = false,
            .result_IsLateralMovementStillSave = false},
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = true,
            .input_IsLaneChangeSafe = false,
            .result_IsLateralMovementStillSave = false},
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_IsLaneChangeSafe = true,
            .result_IsLateralMovementStillSave = true},
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_IsLaneChangeSafe = false,
            .result_IsLateralMovementStillSave = true},
        DataFor_ReevaluateCurrentLaneChange{
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = true,
            .input_IsLaneChangeSafe = true,
            .result_IsLateralMovementStillSave = true}));

/*********************************
 * CHECK CheckForEndOfLaneChange *
 *********************************/

/// \brief Data table
struct DataFor_CheckForEndOfLaneChange
{
  units::length::meter_t input_LaneChangeWidth;
  units::velocity::meters_per_second_t input_LateralVelocity;
  units::length::meter_t input_LaneChangeWidthTraveled;
  bool input_TrajectoryPlanningCompleted;
  units::length::meter_t lateralPositionFrontAxle;
  LateralAction action;
  bool result_EndOfLateralMovement;
  units::time::second_t result_ReactionBaseTime;
  bool result_RemainInLaneChangeState;
};

class MentalModel_CheckForEndOfLaneChange : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_CheckForEndOfLaneChange>
{
};

TEST_P(MentalModel_CheckForEndOfLaneChange, MentalModel_CheckFunction_CheckForEndOfLaneChange)
{
  MentalModelTester TEST_HELPER;
  // Create required classes for test
  DataFor_CheckForEndOfLaneChange data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  // Set up test
  TEST_HELPER.mentalModel.SetLaneChangeWidthTraveled(data.input_LaneChangeWidthTraveled);
  TEST_HELPER.mentalModel.SetReactionBaseTime(-99._s);
  TEST_HELPER.mentalModel.SetMergeRegulate(AreaOfInterest::RIGHT_FRONT);
  TEST_HELPER.mentalModel.SetRemainInLaneChangeState(true);

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralAction = data.action;
  ownVehicleInfo.lateralPositionFrontAxle = data.lateralPositionFrontAxle;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  ON_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  // Call test
  TEST_HELPER.mentalModel.CheckForEndOfLaneChange(data.input_LaneChangeWidth, data.input_LateralVelocity, data.input_TrajectoryPlanningCompleted);

  // Evaluate results
  ASSERT_EQ(TEST_HELPER.mentalModel.GetIsEndOfLateralMovement(), data.result_EndOfLateralMovement);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetReactionBaseTime(), data.result_ReactionBaseTime);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetRemainInLaneChangeState(), data.result_RemainInLaneChangeState);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CheckForEndOfLaneChange,
    testing::Values(
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = 0._m,
            .input_LateralVelocity = 0._mps,
            .input_LaneChangeWidthTraveled = 0._m,
            .input_TrajectoryPlanningCompleted = true,
            .lateralPositionFrontAxle = 0.0_m,
            .action = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .result_EndOfLateralMovement = false,
            .result_ReactionBaseTime = -99._s,
            .result_RemainInLaneChangeState = true},  // No lane change
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = 3.75_m,
            .input_LateralVelocity = 1._mps,
            .input_LaneChangeWidthTraveled = 0._m,
            .input_TrajectoryPlanningCompleted = true,
            .lateralPositionFrontAxle = -1.0_m,
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .result_EndOfLateralMovement = false,
            .result_ReactionBaseTime = -99._s,
            .result_RemainInLaneChangeState = true},  // Lane change left just starts
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = 3.75_m,
            .input_LateralVelocity = -1._mps,
            .input_LaneChangeWidthTraveled = -1.8_m,
            .input_TrajectoryPlanningCompleted = true,
            .lateralPositionFrontAxle = 1.0_m,
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .result_EndOfLateralMovement = false,
            .result_ReactionBaseTime = -99._s,
            .result_RemainInLaneChangeState = true},  // During lane change right
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = 3.75_m,
            .input_LateralVelocity = 1._mps,
            .input_LaneChangeWidthTraveled = 3.75_m,
            .input_TrajectoryPlanningCompleted = true,
            .lateralPositionFrontAxle = 3.75_m,
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .result_EndOfLateralMovement = true,
            .result_ReactionBaseTime = 0._s,
            .result_RemainInLaneChangeState = false},  // Lane change left has ended
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = -3.75_m,
            .input_LateralVelocity = -1._mps,
            .input_LaneChangeWidthTraveled = -4._m,
            .input_TrajectoryPlanningCompleted = true,
            .lateralPositionFrontAxle = -3.75_m,
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .result_EndOfLateralMovement = true,
            .result_ReactionBaseTime = 0._s,
            .result_RemainInLaneChangeState = false},  // Lane change right past the end
        DataFor_CheckForEndOfLaneChange{
            .input_LaneChangeWidth = 3.75_m,
            .input_LateralVelocity = 1._mps,
            .input_LaneChangeWidthTraveled = 3.75_m,
            .input_TrajectoryPlanningCompleted = false,
            .lateralPositionFrontAxle = 2.75_m,
            .action = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .result_EndOfLateralMovement = false,
            .result_ReactionBaseTime = -99._s,
            .result_RemainInLaneChangeState = true}));  // Lane change left has ended regarding values but still continous since trajectory planning hasn't ended yet (vehicle was decelerating while lane changing)

/*********************************
 * CHECK Adjustments due to Exit *
 *********************************/

/// \brief Data table
struct DataFor_AdjustmentDueToExit
{
  units::length::meter_t input_DistanceToEndOfExit;
  units::velocity::meters_per_second_t input_DesiredVelocity;
  double mock_UrgencyIntensity;
  units::velocity::meters_per_second_t result_DesiredVelocity;
};

class MentalModel_AdjustmentDueToExit : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_AdjustmentDueToExit>
{
};

TEST_P(MentalModel_AdjustmentDueToExit, MentalModel_CheckFeature_AdjustmentDueToExit)
{
  // Get resources
  MentalModelTester TEST_HELPER;
  DataFor_AdjustmentDueToExit data = GetParam();

  DriverParameters driverParameters;
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  NiceMock<FakeStochastics> fakeStochastics;
  auto stubMentalModel = std::make_unique<TestMentalModel>(driverParameters, vehicleParameters, &fakeStochastics, TEST_HELPER.fakeFeatureExtractor, TEST_HELPER.fakeMentalCalculations, TEST_HELPER.fakeScmDependencies);

  auto tmpMicroscopicCharacteristics = std::make_unique<FakeMicroscopicCharacteristics>();
  stubMentalModel->UpdateMentalModel(std::move(tmpMicroscopicCharacteristics), nullptr);

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalCalculations, DetermineUrgencyFactorForNextExit(_, _)).WillByDefault(Return(data.mock_UrgencyIntensity));
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(*stubMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  stubMentalModel->GetInfrastructureCharacteristics()->UpdateGeometryInformation()->distanceToEndOfNextExit = data.input_DistanceToEndOfExit;

  scm::signal::DriverInput driverInput;
  driverInput.driverParameters.desiredVelocity = data.input_DesiredVelocity;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverInput.driverParameters));

  // Expand for lateral adjustments once implemented!

  // Call test
  const auto result = stubMentalModel->GetDesiredVelocity();

  // Evaluate results
  ASSERT_TRUE(units::math::fabs(result - data.result_DesiredVelocity) <= units::make_unit<units::velocity::meters_per_second_t>(std::numeric_limits<double>::epsilon()));
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_AdjustmentDueToExit,
    testing::Values(
        DataFor_AdjustmentDueToExit{
            .input_DistanceToEndOfExit = -999._m,
            .input_DesiredVelocity = 50._mps,
            .mock_UrgencyIntensity = 0.,
            .result_DesiredVelocity = 50._mps},
        DataFor_AdjustmentDueToExit{
            .input_DistanceToEndOfExit = 2000._m,
            .input_DesiredVelocity = 50._mps,
            .mock_UrgencyIntensity = 0.,
            .result_DesiredVelocity = 50._mps},
        DataFor_AdjustmentDueToExit{
            .input_DistanceToEndOfExit = -999._m,
            .input_DesiredVelocity = 50._mps,
            .mock_UrgencyIntensity = .3,
            .result_DesiredVelocity = 50._mps},
        DataFor_AdjustmentDueToExit{
            .input_DistanceToEndOfExit = 2000._m,
            .input_DesiredVelocity = 50._mps,
            .mock_UrgencyIntensity = .3,
            .result_DesiredVelocity = 130_kph},
        DataFor_AdjustmentDueToExit{
            .input_DistanceToEndOfExit = 2000._m,
            .input_DesiredVelocity = 20._mps,
            .mock_UrgencyIntensity = .3,
            .result_DesiredVelocity = 20._mps}));

/************************************
 * CHECK DetermineIndexOfSideObject *
 ************************************/

struct DataFor_DetermineIndexOfSideObject
{
  AreaOfInterest aoi;
  SideAoiCriteria criterion;
  std::function<double(const SurroundingVehicleInterface*)> parameter;
  int expectedIndex;
};

class MentalModel_DetermineIndexOfSideObject : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_DetermineIndexOfSideObject>
{
};

TEST_P(MentalModel_DetermineIndexOfSideObject, MentalModel_CheckFunction_DetermineIndexOfSideObject)
{
  const auto data = GetParam();
  MentalModelTester TEST_HELPER;

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> leftVehicle1;
  ON_CALL(leftVehicle1, GetLongitudinalVelocity).WillByDefault(Return(10.0_mps));
  ON_CALL(leftVehicle1, GetRelativeLateralPosition).WillByDefault(Return(5.0_m));
  aoiMapping[AreaOfInterest::LEFT_SIDE] = {&leftVehicle1};

  NiceMock<FakeSurroundingVehicle> leftVehicle2;
  ON_CALL(leftVehicle2, GetLongitudinalVelocity).WillByDefault(Return(20.0_mps));
  ON_CALL(leftVehicle2, GetRelativeLateralPosition).WillByDefault(Return(2.0_m));
  aoiMapping[AreaOfInterest::LEFT_SIDE].push_back(&leftVehicle2);

  NiceMock<FakeSurroundingVehicle> leftVehicle3;
  ON_CALL(leftVehicle3, GetLongitudinalVelocity).WillByDefault(Return(30.0_mps));
  ON_CALL(leftVehicle3, GetRelativeLateralPosition).WillByDefault(Return(3.0_m));
  aoiMapping[AreaOfInterest::LEFT_SIDE].push_back(&leftVehicle3);

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  const auto result = TEST_HELPER.mentalModel.DetermineIndexOfSideObject(data.aoi, data.criterion, data.parameter);

  ASSERT_EQ(result, data.expectedIndex);
}
// clang-format off
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_DetermineIndexOfSideObject,
    testing::Values(
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::EGO_FRONT,
        .criterion = SideAoiCriteria::NONE,
        .parameter = nullptr,
        .expectedIndex = 0},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::FRONT,
        .parameter = nullptr,
        .expectedIndex = 0},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::REAR,
        .parameter = nullptr,
        .expectedIndex = 2},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::MIN,
        .parameter = [](const SurroundingVehicleInterface* vehicle){
          return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value());
        },
        .expectedIndex = 1},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::MAX,
        .parameter = [](const SurroundingVehicleInterface* vehicle){
          return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value());
        },
        .expectedIndex = 0},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::MAX,
        .parameter = [](const SurroundingVehicleInterface* vehicle){
          return vehicle->GetLongitudinalVelocity().GetValue().value();
        },
        .expectedIndex = 2},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::MIN,
        .parameter = [](const SurroundingVehicleInterface* vehicle){
          return vehicle->GetLongitudinalVelocity().GetValue().value();
        },
        .expectedIndex = 0},
      DataFor_DetermineIndexOfSideObject{
        .aoi = AreaOfInterest::LEFT_SIDE,
        .criterion = SideAoiCriteria::MAX,
        .parameter = [](const SurroundingVehicleInterface* vehicle){
          return vehicle->GetLongitudinalVelocity().GetValue().value();
        },
        .expectedIndex = 2}));

// clang-format on
/*********************************
 * CHECK IsSideOrRearArea *
 *********************************/

/// \brief Data table
struct DataFor_IsSideOrRearArea
{
  AreaOfInterest input_aoiReference;
  AreaOfInterest input_aoiObserved;
  bool input_isSideArea;
  bool input_isReferenceVisible;
  bool input_isObservedVisible;
  units::length::meter_t input_mLLReference;
  units::length::meter_t input_mLLObserved;
  bool result_expectedResult;
};

class MentalModel_IsSideOrRearArea : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_IsSideOrRearArea>
{
};

TEST_P(MentalModel_IsSideOrRearArea, MentalModel_CheckFunction_IsSideOrRearArea)
{
  // Get resources
  MentalModelTester TEST_HELPER;
  DataFor_IsSideOrRearArea data = GetParam();

  DriverParameters testDriverParameters;
  scm::common::vehicle::properties::EntityProperties testVehicleParameters;
  NiceMock<FakeStochastics> fakeStochastics;
  auto stubMentalModel = std::make_unique<TestMentalModel5>(testDriverParameters, testVehicleParameters, &fakeStochastics, TEST_HELPER.fakeFeatureExtractor, TEST_HELPER.fakeMentalCalculations, TEST_HELPER.fakeScmDependencies);
  stubMentalModel->Initialize(testDriverParameters, testVehicleParameters, 0_mps, &fakeStochastics);

  // Set data
  NiceMock<FakeSurroundingVehicle> referenceVehicle;
  // Set Reference data
  if (data.input_aoiReference != AreaOfInterest::NumberOfAreaOfInterests)
  {
    ObstructionLongitudinal obstructionReference;
    obstructionReference.mainLaneLocator = data.input_mLLReference;
    ON_CALL(referenceVehicle, GetLongitudinalObstruction).WillByDefault(Return(obstructionReference));
    ON_CALL(referenceVehicle, GetLength).WillByDefault(Return(4.0_m));
  }

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  // Set Observed data
  ObstructionLongitudinal obstructionObserved;
  obstructionObserved.mainLaneLocator = data.input_mLLObserved;
  ON_CALL(observedVehicle, GetLongitudinalObstruction).WillByDefault(Return(obstructionObserved));
  ON_CALL(observedVehicle, GetLength).WillByDefault(Return(4.0_m));

  ON_CALL(*stubMentalModel, GetVehicle(data.input_aoiReference, _)).WillByDefault(Return(data.input_isReferenceVisible ? &referenceVehicle : nullptr));
  ON_CALL(*stubMentalModel, GetVehicle(data.input_aoiObserved, _)).WillByDefault(Return(data.input_isObservedVisible ? &observedVehicle : nullptr));
  if (!data.input_isReferenceVisible && data.input_aoiReference != AreaOfInterest::NumberOfAreaOfInterests)
  {
    // Call test and expect exception
    EXPECT_THROW(stubMentalModel->IsObjectInSideOrRearArea(data.input_aoiReference, data.input_aoiObserved, data.input_isSideArea), std::runtime_error);
  }
  else
  {
    // Call test
    const double result = stubMentalModel->IsObjectInSideOrRearArea(data.input_aoiReference, data.input_aoiObserved, data.input_isSideArea);

    // Evaluate results
    ASSERT_EQ(result, data.result_expectedResult);
  }
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsSideOrRearArea,
    testing::Values(
        // aoiReference is not visible
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_isSideArea = true,
            .input_isReferenceVisible = false,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 0
        // aoiObserved is not visible
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = false,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 1
        // aoiReference is Ego
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 2
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = true},  // 3
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 4
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 5
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 6
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 10.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = true},  // 7
        // IsSideArea()
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = 10.0_m,
            .result_expectedResult = false},  // 8
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = 0.0_m,
            .result_expectedResult = true},  // 9
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_REAR,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = -8.0_m,
            .result_expectedResult = false},  // 10
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_REAR,
            .input_isSideArea = true,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = -2.0_m,
            .result_expectedResult = true},  // 11
        // IsRearArea()
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHT_FRONT_FAR,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = 20.0_m,
            .result_expectedResult = false},  // 12
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = -2.0_m,
            .result_expectedResult = false},  // 13
        DataFor_IsSideOrRearArea{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::EGO_REAR,
            .input_isSideArea = false,
            .input_isReferenceVisible = true,
            .input_isObservedVisible = true,
            .input_mLLReference = 0.0_m,
            .input_mLLObserved = -8.0_m,
            .result_expectedResult = true}));  // 14

/***************************
 * CHECK IsSurroundingArea *
 ***************************/

/// \brief Data table
struct DataFor_IsSurroundingArea
{
  AreaOfInterest input_aoiReference;
  AreaOfInterest input_aoiObserved;
  bool input_includeSideSideLanes;
  bool result_expectedResult;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_IsSurroundingArea& obj)
  {
    return os
           << " input_aoiReference (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoiReference)
           << " | input_aoiObserved (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoiObserved)
           << " | input_includeSideSideLanes (bool): " << obj.input_includeSideSideLanes
           << " | result_expectedResult (bool): " << obj.result_expectedResult;
  }
};

class MentalModel_IsSurroundingArea : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_IsSurroundingArea>
{
};

TEST_P(MentalModel_IsSurroundingArea, MentalModel_CheckFunction_IsSurroundingArea)
{
  // Get resources
  MentalModelTester TEST_HELPER;
  DataFor_IsSurroundingArea data = GetParam();

  if (data.input_aoiReference == data.input_aoiObserved || data.input_aoiObserved == AreaOfInterest::NumberOfAreaOfInterests)
  {
    // Call test and expect exception
    EXPECT_THROW(TEST_HELPER.mentalModel.IsObjectInSurroundingArea(data.input_aoiReference, data.input_aoiObserved, data.input_includeSideSideLanes), std::runtime_error);
  }
  else
  {
    // Call test
    const double result = TEST_HELPER.mentalModel.IsObjectInSurroundingArea(data.input_aoiReference, data.input_aoiObserved, data.input_includeSideSideLanes);

    // Evaluate results
    ASSERT_EQ(result, data.result_expectedResult);
  }
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsSurroundingArea,
    testing::Values(
        // aoiReference and/or aoiObserved are outside vehicle
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::HUD,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 0
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::INFOTAINMENT,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 1
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::INFOTAINMENT,
            .input_aoiObserved = AreaOfInterest::HUD,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 2
        // aoiReference == aoiObserved -> runtime error
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 3
        // aoiObserved == Ego -> runtime error
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::NumberOfAreaOfInterests,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 4
        // aoiReference is Ego
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_includeSideSideLanes = false,
            .result_expectedResult = true},  // 5
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 6
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 7
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_includeSideSideLanes = false,
            .result_expectedResult = false},  // 8
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 9
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 10
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::NumberOfAreaOfInterests,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_includeSideSideLanes = false,
            .result_expectedResult = false},  // 11
        // With SideSideLanes()
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_FRONT,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 12
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT_FAR,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 13
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_includeSideSideLanes = true,
            .result_expectedResult = true},  // 14
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 15
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_includeSideSideLanes = true,
            .result_expectedResult = false},  // 16
        // Without SideSideLanes()
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::RIGHT_FRONT,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_REAR,
            .input_includeSideSideLanes = false,
            .result_expectedResult = true},  // 17
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::RIGHT_FRONT,
            .input_aoiObserved = AreaOfInterest::RIGHT_FRONT_FAR,
            .input_includeSideSideLanes = false,
            .result_expectedResult = true},  // 18
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::RIGHT_FRONT,
            .input_aoiObserved = AreaOfInterest::EGO_REAR,
            .input_includeSideSideLanes = false,
            .result_expectedResult = true},  // 19
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::RIGHT_FRONT,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT,
            .input_includeSideSideLanes = false,
            .result_expectedResult = false},  // 20
        DataFor_IsSurroundingArea{
            .input_aoiReference = AreaOfInterest::RIGHT_FRONT,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_includeSideSideLanes = false,
            .result_expectedResult = false}));  // 21

/******************************************************
 * CHECK DetermineVelocityReasonForDetectedTrafficJam *
 ******************************************************/

/// \brief Data table
struct DataFor_DetermineVelocityReasonForDetectedTrafficJam
{
  std::string description;
  MesoscopicSituation currentMesoscopicSituation;
  units::velocity::meters_per_second_t recentMeanVelocityEgoLane;
  bool isInfluencingDistanceViolated;
  units::velocity::meters_per_second_t expected_velocityReason;
};

class MentalModel_DetermineVelocityReasonForDetectedTrafficJam : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataFor_DetermineVelocityReasonForDetectedTrafficJam>
{
};

TEST_P(MentalModel_DetermineVelocityReasonForDetectedTrafficJam, MentalModel_CheckFunction_DetermineVelocityReasonForDetectedTrafficJam)
{
  DataFor_DetermineVelocityReasonForDetectedTrafficJam data = GetParam();

  MentalModelTester TEST_HELPER;
  std::shared_ptr<FakeTrafficFlow> fakeTrafficFlow = std::make_shared<FakeTrafficFlow>();
  TEST_HELPER.mentalModel.SET_TRAFFIC_FLOW(fakeTrafficFlow);
  TEST_HELPER.mentalModel.SET_MESOSCOPIC_SITUATION_ACTIVE(data.currentMesoscopicSituation);
  ON_CALL(*fakeTrafficFlow, GetMeanVelocity(_)).WillByDefault(Return(data.recentMeanVelocityEgoLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsInfluencingDistanceViolated(_)).WillByDefault(Return(data.isInfluencingDistanceViolated));
  auto result = TEST_HELPER.mentalModel.DetermineVelocityReasonForDetectedTrafficJam();

  if (units::math::isinf(data.expected_velocityReason))
  {
    ASSERT_TRUE(result);
  }
  else
  {
    ASSERT_EQ(result, data.expected_velocityReason);
  }
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_DetermineVelocityReasonForDetectedTrafficJam,
    testing::Values(
        DataFor_DetermineVelocityReasonForDetectedTrafficJam{
            .description = "As the agent is in FREE_DRIVING, there is currently no traffic jam detected so no need to adjust the velocity to it.",
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .recentMeanVelocityEgoLane = 1.0_mps,
            .isInfluencingDistanceViolated = true,
            .expected_velocityReason = ScmDefinitions::INF_VELOCITY},
        DataFor_DetermineVelocityReasonForDetectedTrafficJam{
            .description = "Traffic jam detected but influencing distance is not violated so no need to adjust the velocity to it.",
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .recentMeanVelocityEgoLane = 1.0_mps,
            .isInfluencingDistanceViolated = false,
            .expected_velocityReason = ScmDefinitions::INF_VELOCITY},
        DataFor_DetermineVelocityReasonForDetectedTrafficJam{
            .description = "Traffic jam detected and very low mean velocities leads to minimum approaching velocity."
                           " This shall prevent agents from driving very slow to the end of the traffic jam or even brake to standstill.",
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .recentMeanVelocityEgoLane = 1.0_mps,
            .isInfluencingDistanceViolated = true,
            .expected_velocityReason = ScmDefinitions::MIN_TRAFFIC_JAM_APROACHING_VELOCITY},
        DataFor_DetermineVelocityReasonForDetectedTrafficJam{
            .description = "Traffic jam detected and reasonable velocity equals recent mean velocitiy of ego lane",
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .recentMeanVelocityEgoLane = 70_kph,
            .isInfluencingDistanceViolated = true,
            .expected_velocityReason = 70.0_kph}));

/***********************************
 * CHECK InconsistentAoiManagement *
 **********************************/

TEST(MentalModel_InconsistentAoiManagement, CheckMarkInconsistent)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::LEFT_FRONT);
  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::LEFT_REAR);
  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::EGO_FRONT);

  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_INCONSISTENT({AreaOfInterest::LEFT_FRONT,
                                                         AreaOfInterest::LEFT_REAR,
                                                         AreaOfInterest::EGO_FRONT}));

  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_CONSISTENT({AreaOfInterest::RIGHT_FRONT,
                                                       AreaOfInterest::RIGHT_SIDE,
                                                       AreaOfInterest::EGO_REAR,
                                                       AreaOfInterest::LEFT_SIDE}));
}

TEST(MentalModel_InconsistentAoiManagement, CheckClearInconsistency)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::LEFT_FRONT);
  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::LEFT_REAR);
  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::EGO_FRONT);

  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_INCONSISTENT({AreaOfInterest::LEFT_FRONT,
                                                         AreaOfInterest::LEFT_REAR,
                                                         AreaOfInterest::EGO_FRONT}));

  TEST_HELPER.mentalModel.ClearInconsistency(AreaOfInterest::LEFT_REAR);
  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_CONSISTENT(AreaOfInterest::LEFT_REAR));
  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_INCONSISTENT({AreaOfInterest::LEFT_FRONT,
                                                         AreaOfInterest::EGO_FRONT}));

  TEST_HELPER.mentalModel.ClearInconsistency(AreaOfInterest::LEFT_FRONT);
  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_CONSISTENT({AreaOfInterest::LEFT_REAR,
                                                       AreaOfInterest::LEFT_FRONT}));
  ASSERT_TRUE(TEST_HELPER.mentalModel.TEST_INCONSISTENT(AreaOfInterest::EGO_FRONT));
}

/*********************************************************
 * CHECK CheckForEgoLaneTransition (& TriggerNewEgoLane) *
 *********************************************************/

/// \brief Data table
struct DataFor_CheckForEgoLaneTransition
{
  bool input_RemainInActiveLateralState;
  LateralAction input_CurrentLateralAction;
  int input_LastLaneId;
  int input_CurrentLaneId;
  units::length::meter_t input_LastLateralPosition;
  units::length::meter_t input_CurrentLateralPosition;
  bool result_IsNewEgoLane;
  bool result_TransitionFlag;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CheckForEgoLaneTransition& obj)
  {
    return os
           << "input_RemainInActiveLateralState (bool): " << ScmCommons::BooleanToString(obj.input_RemainInActiveLateralState)
           << " | input_CurrentLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.input_CurrentLateralAction)
           << " | input_LastLaneId (int): " << obj.input_LastLaneId
           << " | input_CurrentLaneId (int): " << obj.input_CurrentLaneId
           << " | input_LastLateralPosition (double): " << obj.input_LastLateralPosition
           << " | input_CurrentLateralPosition (double): " << obj.input_CurrentLateralPosition
           << " | result_IsNewEgoLane (bool): " << ScmCommons::BooleanToString(obj.result_IsNewEgoLane)
           << " | result_TransitionFlag (bool): " << ScmCommons::BooleanToString(obj.result_TransitionFlag);
  }
};

class MentalModel_CheckForEgoLaneTransition : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_CheckForEgoLaneTransition>
{
};

TEST_P(MentalModel_CheckForEgoLaneTransition, MentalModel_CheckFunction_CheckForEgoLaneTransition)
{
  // Create required classes for test
  MentalModelTester TEST_HELPER;

  DataFor_CheckForEgoLaneTransition data = GetParam();

  auto tmpMicroscopicCharacteristics = std::make_unique<TestMicroscopicCharacteristics>();
  auto stubMicroscopicCharacteristics = tmpMicroscopicCharacteristics.get();

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(tmpMicroscopicCharacteristics), nullptr);

  OwnVehicleInformationSCM ownVehicleInformationSCM;
  ownVehicleInformationSCM.mainLaneId = data.input_CurrentLaneId;
  ownVehicleInformationSCM.lateralPosition = data.input_CurrentLateralPosition;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetOwnVehicleInformationScm()).WillByDefault(Return(&ownVehicleInformationSCM));

  TEST_HELPER.mentalModel.SetRemainInLaneChangeState(data.input_RemainInActiveLateralState);
  stubMicroscopicCharacteristics->UpdateOwnVehicleData()->mainLaneId = data.input_LastLaneId;
  stubMicroscopicCharacteristics->UpdateOwnVehicleData()->lateralPosition = data.input_LastLateralPosition;
  stubMicroscopicCharacteristics->UpdateOwnVehicleData()->lateralAction = data.input_CurrentLateralAction;
  TEST_HELPER.mentalModel.ResetTransitionState();

  // Call test
  const bool result_NewEgoLane = TEST_HELPER.mentalModel.CheckForEgoLaneTransition();
  const bool result_TransitionFlag = TEST_HELPER.mentalModel.GET_IS_LANE_CHANGE_PAST_TRANSITION();

  // Evaluate results
  ASSERT_EQ(result_NewEgoLane, data.result_IsNewEgoLane);
  ASSERT_EQ(result_TransitionFlag, data.result_TransitionFlag);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CheckForEgoLaneTransition,
    testing::Values(
        // Not changing lanes
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = false,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .input_LastLaneId = -1,
            .input_CurrentLaneId = -1,
            .input_LastLateralPosition = 0_m,
            .input_CurrentLateralPosition = 0_m,
            .result_IsNewEgoLane = false,
            .result_TransitionFlag = false},
        // Changing lanes left with transition
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = true,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .input_LastLaneId = -2,
            .input_CurrentLaneId = -1,
            .input_LastLateralPosition = 1.49_m,
            .input_CurrentLateralPosition = -1.49_m,
            .result_IsNewEgoLane = true,
            .result_TransitionFlag = true},
        // Changing lanes right with transition
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = true,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .input_LastLaneId = -2,
            .input_CurrentLaneId = -3,
            .input_LastLateralPosition = -1.49_m,
            .input_CurrentLateralPosition = 1.49_m,
            .result_IsNewEgoLane = true,
            .result_TransitionFlag = true},
        // Changing lanes left without transition
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = true,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .input_LastLaneId = -1,
            .input_CurrentLaneId = -1,
            .input_LastLateralPosition = 1.49_m,
            .input_CurrentLateralPosition = 1.49_m,
            .result_IsNewEgoLane = false,
            .result_TransitionFlag = false},
        // Changing lanes right without
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = true,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .input_LastLaneId = -1,
            .input_CurrentLaneId = -1,
            .input_LastLateralPosition = -1.49_m,
            .input_CurrentLateralPosition = -1.49_m,
            .result_IsNewEgoLane = false,
            .result_TransitionFlag = false},
        // New lane id without lane change
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = false,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .input_LastLaneId = -1,
            .input_CurrentLaneId = -2,
            .input_LastLateralPosition = 0_m,
            .input_CurrentLateralPosition = 0_m,
            .result_IsNewEgoLane = false,
            .result_TransitionFlag = false},
        // Changing lanes right with transition but end of lane change
        DataFor_CheckForEgoLaneTransition{
            .input_RemainInActiveLateralState = false,
            .input_CurrentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .input_LastLaneId = -2,
            .input_CurrentLaneId = -3,
            .input_LastLateralPosition = -1.49_m,
            .input_CurrentLateralPosition = 1.49_m,
            .result_IsNewEgoLane = true,
            .result_TransitionFlag = false}));

/*******************************************
 * CHECK GetInfluencingDistanceToEndOfLane *
 *******************************************/

/// \brief Data table
struct DataFor_GetInfluencingDistanceToEndOfLane
{
  units::velocity::meters_per_second_t input_VelocityEgo;
  units::length::meter_t result_ExpectedInfluencingDistanceToEndOfLane;
};

class MentalModel_GetInfluencingDistanceToEndOfLane : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_GetInfluencingDistanceToEndOfLane>
{
};

TEST_P(MentalModel_GetInfluencingDistanceToEndOfLane, MentalModel_CheckFunction_GetInfluencingDistanceToEndOfLane)
{
  // Create required classes for test
  DataFor_GetInfluencingDistanceToEndOfLane data = GetParam();

  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeScmDependencies> fakeScmDependencies;
  NiceMock<FakeStochastics> fakeStochastics;

  auto testVehicleParameters = std::make_unique<scm::common::vehicle::properties::EntityProperties>();
  DriverParameters testDriverParameters;
  testDriverParameters.desiredVelocity = 123.4_mps;
  ON_CALL(fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(testDriverParameters));
  ON_CALL(fakeScmDependencies, GetCycleTime()).WillByDefault(Return(123_ms));
  auto stubMentalModel = std::make_unique<TestMentalModel5>(testDriverParameters, *testVehicleParameters.get(), &fakeStochastics, fakeFeatureExtractor, fakeMentalCalculations, fakeScmDependencies);

  // Set up test
  stubMentalModel->SetAgentGeneralInfluencingDistanceToEndOfLane(500._m);
  ON_CALL(*stubMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.input_VelocityEgo));

  // Call test
  auto result = stubMentalModel->GetInfluencingDistanceToEndOfLane();

  // Evaluate results
  ASSERT_NEAR(result.value(), data.result_ExpectedInfluencingDistanceToEndOfLane.value(), .00001);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetInfluencingDistanceToEndOfLane,
    testing::Values(
        DataFor_GetInfluencingDistanceToEndOfLane{
            .input_VelocityEgo = 0._mps,
            .result_ExpectedInfluencingDistanceToEndOfLane = (500._m / 13.)},  // Below 10 km/h
        DataFor_GetInfluencingDistanceToEndOfLane{
            .input_VelocityEgo = units::velocity::meters_per_second_t(65_kph),
            .result_ExpectedInfluencingDistanceToEndOfLane = 250._m}));  // Above 10 km/h;

/****************************
 * CHECK GetMergeRegulateId *
 ****************************/
TEST(MentalModel_GetMergeRegulateId, Check_GetMergeRegulateId)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_MERGE_REGULATE_ID(5);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetMergeRegulateId(), 5);
}

/********************
 * CHECK Transition *
 ********************/
TEST(MentalModel_Transition, Check_Transition)
{
  MentalModelTester TEST_HELPER;
  EXPECT_CALL(TEST_HELPER.fakeMentalCalculations, TransitionOfEgoVehicle(_, _)).Times(1);

  TEST_HELPER.mentalModel.Transition();
}

/*****************
 * CHECK IsFovea *
 *****************/
struct DataFor_IsFovea
{
  AreaOfInterest aoi;
  bool expectedResult;
};

class MentalModel_IsFovea : public ::testing::Test,
                            public ::testing::WithParamInterface<DataFor_IsFovea>
{
};

TEST_P(MentalModel_IsFovea, Check_IsFovea)
{
  DataFor_IsFovea data = GetParam();
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->fovea = AreaOfInterest::LEFT_FRONT;

  auto result = TEST_HELPER.mentalModel.IsFovea(data.aoi);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsFovea,
    testing::Values(
        DataFor_IsFovea{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = true},
        DataFor_IsFovea{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = false}));

/*****************
 * CHECK GetUfov *
 *****************/

TEST(MentalModel_GetUfov, Check_GetUfov)
{
  MentalModelTester TEST_HELPER;

  std::vector<AreaOfInterest> ufov{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT};
  TEST_HELPER.mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->ufov = ufov;

  auto result = TEST_HELPER.mentalModel.GetUfov();

  ASSERT_EQ(result, ufov);
}

/**********************
 * CHECK GetPeriphery *
 **********************/

TEST(MentalModel_GetPeriphery, Check_GetPeriphery)
{
  MentalModelTester TEST_HELPER;

  std::vector<AreaOfInterest> periphery{AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_FRONT_FAR};
  TEST_HELPER.mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->periphery = periphery;

  auto result = TEST_HELPER.mentalModel.GetPeriphery();

  ASSERT_EQ(result, periphery);
}

/*************************
 * CHECK GetIsNewEgoLane *
 *************************/

TEST(MentalModel_GetIsNewEgoLane, Check_GetIsNewEgoLane)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_IS_NEW_EGOLANE(true);

  ASSERT_TRUE(TEST_HELPER.mentalModel.GetIsNewEgoLane());
}

/***************************
 * CHECK GetCollisionState *
 ***************************/
TEST(MentalModel_GetCollisionState, Check_GetCollisionState)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.GetMicroscopicData()->UpdateOwnVehicleData()->collision = true;

  auto result = TEST_HELPER.mentalModel.GetCollisionState();

  ASSERT_TRUE(TEST_HELPER.mentalModel.GetCollisionState());
}

/****************************
 * CHECK IsObjectInRearArea *
 ****************************/
struct DataFor_IsObjectInRearArea
{
  units::length::meter_t mainLaneLocatorReference;
  units::length::meter_t mainLaneLocatorObserved;
  bool expectedResult;
};

class MentalModel_IsObjectInRearArea : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsObjectInRearArea>
{
};
TEST_P(MentalModel_IsObjectInRearArea, Check_IsObjectInRearArea)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsObjectInRearArea data = GetParam();

  AreaOfInterest aoiReference = AreaOfInterest::EGO_REAR;
  AreaOfInterest aoiObserved = AreaOfInterest::EGO_FRONT;

  VehicleParameter<ObstructionLongitudinal> obstructionReference{{0_m, 0_m, data.mainLaneLocatorReference}};
  VehicleParameter<ObstructionLongitudinal> obstructionObserved{{0_m, 0_m, data.mainLaneLocatorObserved}};

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> referenceVehicle;
  ON_CALL(referenceVehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstructionReference));
  ON_CALL(referenceVehicle, GetLength()).WillByDefault(Return(5.0_m));
  aoiMapping[aoiReference] = {&referenceVehicle};

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  ON_CALL(observedVehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstructionObserved));
  aoiMapping[aoiObserved].push_back(&observedVehicle);

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.IsObjectInRearArea(aoiReference, aoiObserved);

  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsObjectInRearArea,
    testing::Values(
        DataFor_IsObjectInRearArea{
            .mainLaneLocatorReference = 30.0_m,
            .mainLaneLocatorObserved = 20.0_m,
            .expectedResult = true},
        DataFor_IsObjectInRearArea{
            .mainLaneLocatorReference = 20.0_m,
            .mainLaneLocatorObserved = 20.0_m,
            .expectedResult = false}));

/****************************
 * CHECK IsObjectInSideArea *
 ****************************/

struct DataFor_IsObjectInSideArea
{
  units::length::meter_t mainLaneLocatorReference;
  units::length::meter_t mainLaneLocatorObserved;
  bool expectedResult;
};

class MentalModel_IsObjectInSideArea : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsObjectInSideArea>
{
};
TEST_P(MentalModel_IsObjectInSideArea, Check_IsObjectInSideArea)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsObjectInSideArea data = GetParam();

  AreaOfInterest aoiReference = AreaOfInterest::EGO_FRONT;
  AreaOfInterest aoiObserved = AreaOfInterest::LEFT_SIDE;

  VehicleParameter<ObstructionLongitudinal> obstructionReference{{0_m, 0_m, data.mainLaneLocatorReference}};
  VehicleParameter<ObstructionLongitudinal> obstructionObserved{{0_m, 0_m, data.mainLaneLocatorObserved}};

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> referenceVehicle;
  ON_CALL(referenceVehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstructionReference));
  ON_CALL(referenceVehicle, GetLength()).WillByDefault(Return(5.0_m));
  aoiMapping[aoiReference] = {&referenceVehicle};

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  ON_CALL(observedVehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstructionObserved));
  ON_CALL(observedVehicle, GetLength()).WillByDefault(Return(5.0_m));
  aoiMapping[aoiObserved].push_back(&observedVehicle);

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.IsObjectInSideArea(aoiReference, aoiObserved);

  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsObjectInSideArea,
    testing::Values(
        DataFor_IsObjectInSideArea{
            .mainLaneLocatorReference = 30.0_m,
            .mainLaneLocatorObserved = 20.0_m,
            .expectedResult = false},
        DataFor_IsObjectInSideArea{
            .mainLaneLocatorReference = 20.0_m,
            .mainLaneLocatorObserved = 20.0_m,
            .expectedResult = true}));

/**********************************
 * CHECK ResetInformationRequests *
 **********************************/

TEST(MentalModel_ResetInformationRequests, Check_ResetInformationRequests)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.id = 0;
  agentEgo.fovea = AreaOfInterest::EGO_FRONT;
  agentEgo.ufov = {AreaOfInterest::LEFT_FRONT};
  agentEgo.periphery = {AreaOfInterest::LEFT_SIDE};

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&agentEgo));

  SurroundingObjectsScmExtended surroundingObjects;
  ObjectInformationScmExtended one, two, three;

  InformationRequest info;
  info.Timestamp = 1700_ms;
  info.RequesterPriority = 1.0;
  info.Priority = 2.0;
  info.Request = FieldOfViewAssignment::FOVEA;

  std::vector<InformationRequest> opticalRequest;
  opticalRequest.push_back(info);

  one.id = 1;
  one.opticalRequest = opticalRequest;
  two.id = 2;
  two.opticalRequest = opticalRequest;
  three.id = 3;
  three.opticalRequest = opticalRequest;

  surroundingObjects.Ahead().Left().push_back(one);
  surroundingObjects.Close().Left().push_back(two);
  surroundingObjects.Ahead().Close().push_back(three);

  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&one));
  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(&two));
  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(&three));

  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::EGO_FRONT, _)).Times(2);
  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::LEFT_FRONT, _)).Times(2);
  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::LEFT_SIDE, _)).Times(2);

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  TEST_HELPER.mentalModel.ResetInformationRequests();
}

/**********************
 * CHECK Set/ GetTime *
 **********************/

TEST(MentalModel_SetGetTime, Check_SetGetTime)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetTime(100_ms);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetTime(), 100_ms);
}

/******************************
 * CHECK GetRequesterPriority *
 ******************************/

struct DataFor_GetRequesterPriority
{
  InformationRequestTrigger trigger;
  double expectedResult;
};

class MentalModel_GetRequesterPriority : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_GetRequesterPriority>
{
};

TEST_P(MentalModel_GetRequesterPriority, Check_GetRequesterPriority)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetRequesterPriority data = GetParam();

  auto result = TEST_HELPER.mentalModel.GetRequesterPriority(data.trigger);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetRequesterPriority,
    testing::Values(
        DataFor_GetRequesterPriority{
            .trigger = InformationRequestTrigger::LONGITUDINAL_ACTION,
            .expectedResult = 0.4},
        DataFor_GetRequesterPriority{
            .trigger = InformationRequestTrigger::UNDEFINED,
            .expectedResult = 0.0}));

/*******************************
 * CHECK AddInformationRequest *
 *******************************/

struct DataFor_AddInformationRequest
{
  AreaOfInterest aoi;
  InformationRequestTrigger trigger;
  std::vector<double> expectedResult;
};

class MentalModel_AddInformationRequest : public ::testing::Test,
                                          public ::testing::WithParamInterface<DataFor_AddInformationRequest>
{
};

TEST_P(MentalModel_AddInformationRequest, Check_AddInformationRequest)
{
  MentalModelTester TEST_HELPER;
  DataFor_AddInformationRequest data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  TEST_HELPER.mentalModel.SetTime(100_ms);

  SurroundingObjectsScmExtended surroundingObjects;
  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;

  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.AddInformationRequest(data.aoi, FieldOfViewAssignment::FOVEA, 1.0, data.trigger);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_AddInformationRequest,
    testing::Values(
        DataFor_AddInformationRequest{
            .aoi = AreaOfInterest::EGO_FRONT,
            .trigger = InformationRequestTrigger::LONGITUDINAL_ACTION,
            .expectedResult{1, 0, 1}},
        DataFor_AddInformationRequest{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .trigger = InformationRequestTrigger::LATERAL_ACTION,
            .expectedResult{0, 0, 1}}));

/********************************
 * CHECK ScaleTopDownRequestMap *
 ********************************/

struct DataFor_ScaleTopDownRequestMap
{
  std::map<AreaOfInterest, double> map;
  std::map<AreaOfInterest, double> expectedResult;
};

class MentalModel_ScaleTopDownRequestMap : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_ScaleTopDownRequestMap>
{
};

TEST_P(MentalModel_ScaleTopDownRequestMap, Check_ScaleTopDownRequestMap)
{
  MentalModelTester TEST_HELPER;
  DataFor_ScaleTopDownRequestMap data = GetParam();

  auto result = TEST_HELPER.mentalModel.ScaleTopDownRequestMap(data.map);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_ScaleTopDownRequestMap,
    testing::Values(
        DataFor_ScaleTopDownRequestMap{
            .map{},
            .expectedResult{}},
        DataFor_ScaleTopDownRequestMap{
            .map = {{AreaOfInterest::EGO_FRONT, 2.5},
                    {AreaOfInterest::LEFT_FRONT, 10.0},
                    {AreaOfInterest::RIGHT_FRONT, 5.0}},
            .expectedResult{{AreaOfInterest::LEFT_FRONT, 1.0},
                            {AreaOfInterest::RIGHT_FRONT, 0.5},
                            {AreaOfInterest::EGO_FRONT, 0.25}}}));

/***********************************
 * CHECK CompareInformationRequest *
 ***********************************/

struct DataFor_CompareInformationRequest
{
  units::time::millisecond_t timeStampA;
  double requesterPriorityA;
  double priorityA;
  FieldOfViewAssignment requestA;

  units::time::millisecond_t timeStampB;
  double requesterPriorityB;
  double priorityB;
  FieldOfViewAssignment requestB;

  bool expectedResult;
};

class MentalModel_CompareInformationRequest : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_CompareInformationRequest>
{
};

TEST_P(MentalModel_CompareInformationRequest, Check_CompareInformationRequest)
{
  MentalModelTester TEST_HELPER;
  DataFor_CompareInformationRequest data = GetParam();

  InformationRequest a;
  a.Timestamp = data.timeStampA;
  a.RequesterPriority = data.requesterPriorityA;
  a.Priority = data.priorityA;
  a.Request = data.requestA;

  InformationRequest b;
  b.Timestamp = data.timeStampB;
  b.RequesterPriority = data.requesterPriorityB;
  b.Priority = data.priorityB;
  b.Request = data.requestB;

  auto result = TEST_HELPER.mentalModel.CompareInformationRequest(a, b);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CompareInformationRequest,
    testing::Values(
        DataFor_CompareInformationRequest{
            .timeStampA = 100_ms,
            .requesterPriorityA = 1,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::FOVEA,
            .timeStampB = 100_ms,
            .requesterPriorityB = 1,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = true},
        DataFor_CompareInformationRequest{
            .timeStampA = 100_ms,
            .requesterPriorityA = 1,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::UFOV,
            .timeStampB = 100_ms,
            .requesterPriorityB = 1,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = false},
        DataFor_CompareInformationRequest{
            .timeStampA = 100_ms,
            .requesterPriorityA = 2,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::UFOV,
            .timeStampB = 100_ms,
            .requesterPriorityB = 1,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = true},
        DataFor_CompareInformationRequest{
            .timeStampA = 100_ms,
            .requesterPriorityA = 1,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::UFOV,
            .timeStampB = 100_ms,
            .requesterPriorityB = 2,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = false},
        DataFor_CompareInformationRequest{
            .timeStampA = 100_ms,
            .requesterPriorityA = 1,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::UFOV,
            .timeStampB = 200_ms,
            .requesterPriorityB = 2,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = false},
        DataFor_CompareInformationRequest{
            .timeStampA = 200_ms,
            .requesterPriorityA = 1,
            .priorityA = 1,
            .requestA = FieldOfViewAssignment::UFOV,
            .timeStampB = 100_ms,
            .requesterPriorityB = 2,
            .priorityB = 0.5,
            .requestB = FieldOfViewAssignment::FOVEA,
            .expectedResult = true}));

/***********************************
 * CHECK GenerateTopDownAoiScoring *
 ***********************************/

TEST(MentalModel_GenerateTopDownAoiScoring, Check_GenerateTopDownAoiScoring)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  InformationRequest infoA;
  infoA.Timestamp = 1700_ms;
  infoA.RequesterPriority = 1.0;
  infoA.Priority = 2.0;
  infoA.Request = FieldOfViewAssignment::FOVEA;

  InformationRequest infoB;
  infoB.Timestamp = 1000_ms;
  infoB.RequesterPriority = 3.0;
  infoB.Priority = 1.0;
  infoB.Request = FieldOfViewAssignment::UFOV;

  std::vector<InformationRequest> opticalRequestA;
  opticalRequestA.push_back(infoA);

  std::vector<InformationRequest> opticalRequestB;
  opticalRequestB.push_back(infoB);

  ObjectInformationScmExtended objectInfoA;
  objectInfoA.id = 1;
  objectInfoA.opticalRequest = opticalRequestA;

  ObjectInformationScmExtended objectInfoB;
  objectInfoB.id = 2;
  objectInfoB.opticalRequest = opticalRequestB;

  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&objectInfoA));
  ON_CALL(*fakeMicroscopicData, UpdateObjectInformation(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(&objectInfoB));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.GenerateTopDownAoiScoring(false);

  ASSERT_EQ(result.at(AreaOfInterest::LEFT_FRONT), 0.75);
  ASSERT_EQ(result.at(AreaOfInterest::EGO_FRONT), 1.0);
}

/**********************
 * CHECK GetCycleTime *
 **********************/

TEST(MentalModel_GetCycleTime, Check_GetCycleTime)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_CYCLE_TIME(250_ms);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetCycleTime(), 250_ms);
}

/************************************
 * CHECK GetLongitudinalActionState *
 ************************************/

TEST(MentalModel_GetLongitudinalActionState, Check_GetLongitudinalActionState)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo{};

  EXPECT_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).Times(1).WillOnce(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  TEST_HELPER.mentalModel.GetLongitudinalActionState();
}

/************************************
 * CHECK SetLongitudinalActionState *
 ************************************/

TEST(MentalModel_SetLongitudinalActionState, Check_SetLongitudinalActionState)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo{};

  EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(1).WillOnce(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  TEST_HELPER.mentalModel.SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);
}

/************************************
 * CHECK SetLongitudinalActionState *
 ************************************/

struct DataFor_GetIsStaticObject
{
  AreaOfInterest aoi;
  bool expectedResult;
};

class MentalModel_GetIsStaticObject : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_GetIsStaticObject>
{
};

TEST_P(MentalModel_GetIsStaticObject, Check_GetIsStaticObject)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetIsStaticObject data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, IsStatic()).WillByDefault(Return(true));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeRightVehicle;
  ON_CALL(fakeRightVehicle, IsStatic()).WillByDefault(Return(false));
  aoiMapping[AreaOfInterest::RIGHT_FRONT].push_back(&fakeRightVehicle);

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.GetIsStaticObject(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetIsStaticObject,
    testing::Values(
        DataFor_GetIsStaticObject{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = true},
        DataFor_GetIsStaticObject{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = false},
        DataFor_GetIsStaticObject{
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = false}));

/****************
 * CHECK GetTtc *
 ****************/

struct DataFor_GetTtc
{
  AreaOfInterest aoi;
  units::time::second_t expectedResult;
};

class MentalModel_GetTtc : public ::testing::Test,
                           public ::testing::WithParamInterface<DataFor_GetTtc>
{
};

TEST_P(MentalModel_GetTtc, Check_GetTtc)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetTtc data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetTtc()).WillByDefault(Return(10.0_s));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.GetTtc(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetTtc,
    testing::Values(
        DataFor_GetTtc{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 10.0_s},
        DataFor_GetTtc{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = ScmDefinitions::TTC_LIMIT}));

/****************
 * CHECK GetGap *
 ****************/

struct DataFor_GetGap
{
  AreaOfInterest aoi;
  units::time::second_t expectedResult;
};

class MentalModel_GetGap : public ::testing::Test,
                           public ::testing::WithParamInterface<DataFor_GetGap>
{
};

TEST_P(MentalModel_GetGap, Check_GetGap)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetGap data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetThw()).WillByDefault(Return(10.0_s));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.GetGap(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetGap,
    testing::Values(
        DataFor_GetGap{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 10.0_s},
        DataFor_GetGap{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = 99._s}));

/********************************
 * CHECK GetRelativeNetDistance *
 ********************************/

struct DataFor_GetRelativeNetDistance
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetRelativeNetDistance : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetRelativeNetDistance>
{
};

TEST_P(MentalModel_GetRelativeNetDistance, Check_GetRelativeNetDistance)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetRelativeNetDistance data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetRelativeNetDistance()).WillByDefault(Return(10.0_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.GetRelativeNetDistance(data.aoi);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetRelativeNetDistance,
    testing::Values(
        DataFor_GetRelativeNetDistance{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 10.0_m},
        DataFor_GetRelativeNetDistance{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)}));

/******************************
 * CHECK GetAccelerationDelta *
 ******************************/

struct DataFor_GetAccelerationDelta
{
  AreaOfInterest aoi;
  bool isBehind;
  units::acceleration::meters_per_second_squared_t expectedResult;
};

class MentalModel_GetAccelerationDelta : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_GetAccelerationDelta>
{
};

TEST_P(MentalModel_GetAccelerationDelta, Check_GetAccelerationDelta)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetAccelerationDelta data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, ToTheSideOfEgo()).WillByDefault(Return(true));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeLeftFrontVehicle;
  ON_CALL(fakeLeftFrontVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeLeftFrontVehicle, BehindEgo()).WillByDefault(Return(data.isBehind));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeLeftFrontVehicle};

  auto fakeSurroundingVehicleQueryFactory = std::make_unique<NiceMock<FakeSurroundingVehicleQueryFactory>>();
  auto fakeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*fakeVehicleQuery, GetLongitudinalAccelerationDelta()).WillByDefault(Return(100_mps_sq));
  ON_CALL(*fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));

  TEST_HELPER.mentalModel.SET_VEHICLE_QUERY_FACTORY(std::move(fakeSurroundingVehicleQueryFactory));

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetAccelerationDelta(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetAccelerationDelta,
    testing::Values(
        DataFor_GetAccelerationDelta{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .isBehind = false,
            .expectedResult = units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::DEFAULT_VALUE)},
        DataFor_GetAccelerationDelta{
            .aoi = AreaOfInterest::EGO_FRONT,
            .isBehind = false,
            .expectedResult = 0.0_mps_sq},
        DataFor_GetAccelerationDelta{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .isBehind = true,
            .expectedResult = 100.0_mps_sq},
        DataFor_GetAccelerationDelta{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .isBehind = false,
            .expectedResult = -100.0_mps_sq}));

/**************************************
 * CHECK GetLongitudinalVelocityDelta *
 **************************************/

struct DataFor_GetLongitudinalVelocityDelta
{
  AreaOfInterest aoi;
  bool isBehind;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLongitudinalVelocityDelta : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_GetLongitudinalVelocityDelta>
{
};

TEST_P(MentalModel_GetLongitudinalVelocityDelta, Check_GetLongitudinalVelocityDelta)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLongitudinalVelocityDelta data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, ToTheSideOfEgo()).WillByDefault(Return(true));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeLeftFrontVehicle;
  ON_CALL(fakeLeftFrontVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeLeftFrontVehicle, BehindEgo()).WillByDefault(Return(data.isBehind));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeLeftFrontVehicle};

  auto fakeSurroundingVehicleQueryFactory = std::make_unique<NiceMock<FakeSurroundingVehicleQueryFactory>>();
  auto fakeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*fakeVehicleQuery, GetLongitudinalVelocityDelta()).WillByDefault(Return(100_mps));
  ON_CALL(*fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));

  TEST_HELPER.mentalModel.SET_VEHICLE_QUERY_FACTORY(std::move(fakeSurroundingVehicleQueryFactory));

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetLongitudinalVelocityDelta(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLongitudinalVelocityDelta,
    testing::Values(
        DataFor_GetLongitudinalVelocityDelta{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .isBehind = false,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)},
        DataFor_GetLongitudinalVelocityDelta{
            .aoi = AreaOfInterest::EGO_FRONT,
            .isBehind = false,
            .expectedResult = 0.0_mps},
        DataFor_GetLongitudinalVelocityDelta{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .isBehind = true,
            .expectedResult = 100.0_mps},
        DataFor_GetLongitudinalVelocityDelta{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .isBehind = false,
            .expectedResult = -100.0_mps}));

/*********************************
 * CHECK GetLateralVelocityDelta *
 *********************************/

struct DataFor_GetLateralVelocityDelta
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLateralVelocityDelta : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_GetLateralVelocityDelta>
{
};

TEST_P(MentalModel_GetLateralVelocityDelta, Check_GetLateralVelocityDelta)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLateralVelocityDelta data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, ToTheSideOfEgo()).WillByDefault(Return(true));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  auto fakeSurroundingVehicleQueryFactory = std::make_unique<NiceMock<FakeSurroundingVehicleQueryFactory>>();
  auto fakeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*fakeVehicleQuery, GetLateralVelocityDelta()).WillByDefault(Return(100_mps));
  ON_CALL(*fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));

  TEST_HELPER.mentalModel.SET_VEHICLE_QUERY_FACTORY(std::move(fakeSurroundingVehicleQueryFactory));

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetLateralVelocityDelta(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLateralVelocityDelta,
    testing::Values(
        DataFor_GetLateralVelocityDelta{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)},
        DataFor_GetLateralVelocityDelta{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = -100.0_mps}));

/*****************************
 * CHECK GetAbsoluteVelocity *
 *****************************/

struct DataFor_GetAbsoluteVelocity
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetAbsoluteVelocity : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_GetAbsoluteVelocity>
{
};

TEST_P(MentalModel_GetAbsoluteVelocity, Check_GetAbsoluteVelocity)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetAbsoluteVelocity data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetAbsoluteVelocity()).WillByDefault(Return(15.0_mps));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetAbsoluteVelocity(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetAbsoluteVelocity,
    testing::Values(
        DataFor_GetAbsoluteVelocity{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 15.0_mps},
        DataFor_GetAbsoluteVelocity{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)}));

/*************************
 * CHECK GetAcceleration *
 *************************/

struct DataFor_GetAcceleration
{
  AreaOfInterest aoi;
  units::acceleration::meters_per_second_squared_t expectedResult;
};

class MentalModel_GetAcceleration : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_GetAcceleration>
{
};

TEST_P(MentalModel_GetAcceleration, Check_GetAcceleration)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetAcceleration data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetLongitudinalAcceleration()).WillByDefault(Return(5.0_mps_sq));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetAcceleration(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetAcceleration,
    testing::Values(
        DataFor_GetAcceleration{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 5.0_mps_sq},
        DataFor_GetAcceleration{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::DEFAULT_VALUE)}));

/****************************
 * CHECK GetLateralVelocity *
 ****************************/

struct DataFor_GetLateralVelocity
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLateralVelocity : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_GetLateralVelocity>
{
};

TEST_P(MentalModel_GetLateralVelocity, Check_GetLateralVelocity)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLateralVelocity data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetLateralVelocity()).WillByDefault(Return(1.0_mps));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetLateralVelocity(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLateralVelocity,
    testing::Values(
        DataFor_GetLateralVelocity{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 1.0_mps},
        DataFor_GetLateralVelocity{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)}));

/***********************************
 * CHECK GetDistanceToBoundaryLeft *
 ***********************************/

struct DataFor_GetDistanceToBoundaryLeft
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetDistanceToBoundaryLeft : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_GetDistanceToBoundaryLeft>
{
};

TEST_P(MentalModel_GetDistanceToBoundaryLeft, Check_GetDistanceToBoundaryLeft)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetDistanceToBoundaryLeft data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetDistanceToLaneBoundary(Side::Left)).WillByDefault(Return(0.5_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetDistanceToBoundaryLeft(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetDistanceToBoundaryLeft,
    testing::Values(
        DataFor_GetDistanceToBoundaryLeft{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 0.5_m},
        DataFor_GetDistanceToBoundaryLeft{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)}));

TEST(MentalModel_GetDistanceToBoundaryLeft_Without_Parameters, Check_GetDistanceToBoundaryLeft)
{
  MentalModelTester TEST_HELPER;

  OwnVehicleInformationScmExtended ownVehicleInformationSCM;
  ownVehicleInformationSCM.distanceToLaneBoundaryLeft = 1.0_m;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInformationSCM));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceToBoundaryLeft(), 1.0_m);
}

/************************************
 * CHECK GetDistanceToBoundaryRight *
 ************************************/

struct DataFor_GetDistanceToBoundaryRight
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetDistanceToBoundaryRight : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_GetDistanceToBoundaryRight>
{
};

TEST_P(MentalModel_GetDistanceToBoundaryRight, Check_GetDistanceToBoundaryLeft)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetDistanceToBoundaryRight data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetDistanceToLaneBoundary(Side::Right)).WillByDefault(Return(0.5_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetDistanceToBoundaryRight(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetDistanceToBoundaryRight,
    testing::Values(
        DataFor_GetDistanceToBoundaryRight{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 0.5_m},
        DataFor_GetDistanceToBoundaryRight{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)}));

TEST(MentalModel_GetDistanceToBoundaryRight_Without_Parameters, Check_GetDistanceToBoundaryRight)
{
  MentalModelTester TEST_HELPER;

  OwnVehicleInformationScmExtended ownVehicleInformationSCM;
  ownVehicleInformationSCM.distanceToLaneBoundaryRight = 1.0_m;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInformationSCM));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceToBoundaryRight(), 1.0_m);
}

/**************************
 * CHECK GetVehicleLength *
 **************************/

struct DataFor_GetVehicleLength
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetVehicleLength : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GetVehicleLength>
{
};

TEST_P(MentalModel_GetVehicleLength, Check_GetDistanceToBoundaryLeft)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetVehicleLength data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetLength()).WillByDefault(Return(5.5_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetVehicleLength(data.aoi, 0);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetVehicleLength,
    testing::Values(
        DataFor_GetVehicleLength{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expectedResult = 5.0_m},
        DataFor_GetVehicleLength{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 5.5_m},
        DataFor_GetVehicleLength{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::length::meter_t(ScmDefinitions::DEFAULT_VALUE)}));

/*************************
 * CHECK GetVehicleWidth *
 *************************/

TEST(MentalModel_GetVehicleWidth_Without_Parameters, Check_GetVehicleWidth)
{
  MentalModelTester TEST_HELPER;

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.width = 3.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetVehicleWidth(), 3.0_m);
}

struct DataFor_GetVehicleWidth
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetVehicleWidth : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_GetVehicleWidth>
{
};

TEST_P(MentalModel_GetVehicleWidth, Check_GetVehicleWidth)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetVehicleWidth data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetWidth()).WillByDefault(Return(5.5_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetVehicleWidth(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetVehicleWidth,
    testing::Values(
        DataFor_GetVehicleWidth{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 5.5_m},
        DataFor_GetVehicleWidth{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)}));

/**********************************
 * CHECK GetVehicleClassification *
 **********************************/

TEST(MentalModel_GetVehicleClassification_Without_Parameters, Check_GetVehicleClassification)
{
  MentalModelTester TEST_HELPER;

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.classification = scm::common::VehicleClass::kLuxury_car;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetVehicleClassification(), scm::common::VehicleClass::kLuxury_car);
}

struct DataFor_GetVehicleClassification
{
  AreaOfInterest aoi;
  scm::common::VehicleClass expectedResult;
};

class MentalModel_GetVehicleClassification : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_GetVehicleClassification>
{
};

TEST_P(MentalModel_GetVehicleClassification, Check_GetVehicleClassification)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetVehicleClassification data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetVehicleClassification()).WillByDefault(Return(scm::common::VehicleClass::kLuxury_car));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetVehicleClassification(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetVehicleClassification,
    testing::Values(
        DataFor_GetVehicleClassification{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = scm::common::VehicleClass::kLuxury_car},
        DataFor_GetVehicleClassification{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = scm::common::VehicleClass::kInvalid}));

/**************************
 * CHECK GetVehicleHeight *
 **************************/

struct DataFor_GetVehicleHeight
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetVehicleHeight : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GetVehicleHeight>
{
};

TEST_P(MentalModel_GetVehicleHeight, Check_GetVehicleHeight)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetVehicleHeight data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetHeight()).WillByDefault(Return(1.5_m));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetVehicleHeight(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetVehicleHeight,
    testing::Values(
        DataFor_GetVehicleHeight{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 1.5_m},
        DataFor_GetVehicleHeight{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)}));

/*************************
 * CHECK Get/Set AgentId *
 *************************/

TEST(MentalModel_GetSetAgentId_Without_Parameters, Check_GetSetAgentId)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.id = 1;

  EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(1).WillOnce(Return(&ownVehicleInfo));
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  TEST_HELPER.mentalModel.SetAgentId(1);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAgentId(), 1);
}

struct DataFor_GetAgentId
{
  AreaOfInterest aoi;
  double expectedResult;
};

class MentalModel_GetAgentId : public ::testing::Test,
                               public ::testing::WithParamInterface<DataFor_GetAgentId>
{
};

TEST_P(MentalModel_GetAgentId, Check_GetAgentId)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetAgentId data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetId()).WillByDefault(Return(1));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetAgentId(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetAgentId,
    testing::Values(
        DataFor_GetAgentId{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 1},
        DataFor_GetAgentId{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = -1}));

/*******************************
 * CHECK ResetReactionBaseTime *
 *******************************/

TEST(MentalModel_ResetReactionBaseTime, Check_ResetReactionBaseTime)
{
  MentalModelTester TEST_HELPER;

  NiceMock<FakeStochastics> fakeStochastics;

  DriverParameters driverParameters;
  driverParameters.reactionBaseTimeMean = 3.5_s;
  driverParameters.reactionBaseTimeStandardDeviation = 2.0_s;
  driverParameters.reactionBaseTimeMinimum = 3.0_s;
  driverParameters.reactionBaseTimeMaximum = 4.0_s;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(2));

  ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(2));

  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);
  TEST_HELPER.mentalModel.SET_CYCLE_TIME(100_ms);
  TEST_HELPER.mentalModel.ResetReactionBaseTime();
  ASSERT_EQ(TEST_HELPER.mentalModel.GetReactionBaseTime(), 3.1_s);
}

/*********************************
 * CHECK AdvanceReactionBaseTime *
 *********************************/

TEST(MentalModel_AdvanceReactionBaseTime, Check_AdvanceReactionBaseTime)
{
  MentalModelTester TEST_HELPER;
  TEST_HELPER.mentalModel.SET_CYCLE_TIME(100_ms);
  TEST_HELPER.mentalModel.AdvanceReactionBaseTime();

  ASSERT_EQ(TEST_HELPER.mentalModel.GetReactionBaseTime(), -0.1_s);
}

/*******************************
 * CHECK GetCarQueuingDistance *
 *******************************/

TEST(MentalModel_GetCarQueuingDistance, Check_GetCarQueuingDistance)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameters;
  driverParameters.carQueuingDistance = 10.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetCarQueuingDistance(), 10.0_m);
}

/*******************************
 * CHECK GetCarRestartDistance *
 *******************************/

TEST(MentalModel_GetCarRestartDistance, Check_GetCarRestartDistance)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameters;
  driverParameters.carRestartDistance = 11.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetCarRestartDistance(), 11.0_m);
}

/*****************************************
 * CHECK GetLateralOffsetNeutralPosition *
 *****************************************/

TEST(MentalModel_GetLateralOffsetNeutralPosition, Check_GetLateralOffsetNeutralPosition)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameters;
  driverParameters.lateralOffsetNeutralPosition = 12.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralOffsetNeutralPosition(), 12.0_m);
}

/***************************************************
 * CHECK GetLateralOffsetNeutralPositionRescueLane *
 ***************************************************/

TEST(MentalModel_GetLateralOffsetNeutralPositionRescueLane, Check_GetLateralOffsetNeutralPositionRescueLane)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameters;
  driverParameters.lateralOffsetNeutralPositionRescueLane = 13.0_m;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralOffsetNeutralPositionRescueLane(), 13.0_m);
}

/******************************************
 * CHECK GetLateralVelocityTowardsEgoLane *
 ******************************************/

struct DataFor_GetLateralVelocityTowardsEgoLane
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLateralVelocityTowardsEgoLane : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_GetLateralVelocityTowardsEgoLane>
{
};

TEST_P(MentalModel_GetLateralVelocityTowardsEgoLane, Check_GetLateralVelocityTowardsEgoLane)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLateralVelocityTowardsEgoLane data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetRelativeLateralPosition()).WillByDefault(Return(1_m));
  ON_CALL(fakeEgoFrontVehicle, GetLateralVelocity()).WillByDefault(Return(1_mps));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeLeftFrontVehicle;
  ON_CALL(fakeLeftFrontVehicle, GetRelativeLateralPosition()).WillByDefault(Return(-1_m));
  ON_CALL(fakeLeftFrontVehicle, GetLateralVelocity()).WillByDefault(Return(2_mps));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeLeftFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeRightFrontVehicle;
  ON_CALL(fakeRightFrontVehicle, GetRelativeLateralPosition()).WillByDefault(Return(0_m));
  ON_CALL(fakeRightFrontVehicle, GetLateralVelocity()).WillByDefault(Return(3_mps));
  aoiMapping[AreaOfInterest::RIGHT_FRONT] = {&fakeRightFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralVelocityTowardsEgoLane(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLateralVelocityTowardsEgoLane,
    testing::Values(
        DataFor_GetLateralVelocityTowardsEgoLane{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = -1_mps},
        DataFor_GetLateralVelocityTowardsEgoLane{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .expectedResult = 0.0_mps},
        DataFor_GetLateralVelocityTowardsEgoLane{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = 2.0_mps},
        DataFor_GetLateralVelocityTowardsEgoLane{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = 0.0_mps}));

/***********************************
 * CHECK GetDistanceTowardsEgoLane *
 ***********************************/

struct DataFor_GetDistanceTowardsEgoLane
{
  AreaOfInterest aoi;
  units::length::meter_t expectedResult;
};

class MentalModel_GetDistanceTowardsEgoLane : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_GetDistanceTowardsEgoLane>
{
};

TEST_P(MentalModel_GetDistanceTowardsEgoLane, Check_GetDistanceTowardsEgoLane)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetDistanceTowardsEgoLane data = GetParam();

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  LaneInformationGeometrySCM laneInfo;
  laneInfo.width = 4.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  AoiVehicleMapping aoiMapping;

  NiceMock<FakeSurroundingVehicle> fakeRightFrontVehicle;
  ON_CALL(fakeRightFrontVehicle, GetDistanceToLaneBoundary(Side::Left)).WillByDefault(Return(0.5_m));
  aoiMapping[AreaOfInterest::RIGHT_FRONT] = {&fakeRightFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeLeftFrontVehicle;
  ON_CALL(fakeLeftFrontVehicle, GetDistanceToLaneBoundary(Side::Right)).WillByDefault(Return(0.6_m));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeLeftFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeLeftLeftFrontVehicle;
  ON_CALL(fakeLeftLeftFrontVehicle, GetDistanceToLaneBoundary(Side::Right)).WillByDefault(Return(0.6_m));
  aoiMapping[AreaOfInterest::LEFTLEFT_FRONT] = {&fakeLeftLeftFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeRightRightFrontVehicle;
  ON_CALL(fakeRightRightFrontVehicle, GetDistanceToLaneBoundary(Side::Left)).WillByDefault(Return(0.5_m));
  aoiMapping[AreaOfInterest::RIGHTRIGHT_FRONT] = {&fakeRightRightFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  aoiMapping[AreaOfInterest::NumberOfAreaOfInterests] = {&fakeVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));

  if (data.aoi != AreaOfInterest::DISTRACTION)
  {
    ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceTowardsEgoLane(data.aoi, 0), data.expectedResult);
  }
  else
  {
    EXPECT_THROW(TEST_HELPER.mentalModel.GetDistanceTowardsEgoLane(data.aoi, 0), std::runtime_error);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetDistanceTowardsEgoLane,
    testing::Values(
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = 0.5_m},
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = 0.6_m},
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::LEFTLEFT_FRONT,
            .expectedResult = 4.6_m},
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::RIGHTRIGHT_FRONT,
            .expectedResult = 4.5_m},
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expectedResult = units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)},
        DataFor_GetDistanceTowardsEgoLane{
            .aoi = AreaOfInterest::DISTRACTION,
            .expectedResult = 0_m}));

/***************************
 * CHECK GetIndicatorState *
 ***************************/

struct DataFor_GetIndicatorState
{
  AreaOfInterest aoi;
  scm::LightState::Indicator expectedResult;
};

class MentalModel_GetIndicatorState : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_GetIndicatorState>
{
};

TEST_P(MentalModel_GetIndicatorState, Check_GetIndicatorState)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetIndicatorState data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetIndicatorState()).WillByDefault(Return(scm::LightState::Indicator::Warn));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetIndicatorState(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetIndicatorState,
    testing::Values(
        DataFor_GetIndicatorState{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = scm::LightState::Indicator::Warn},
        DataFor_GetIndicatorState{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .expectedResult = scm::LightState::Indicator::Off}));

/***************************************
 * CHECK ImplementStochasticActivation *
 ***************************************/

struct DataFor_ImplementStochasticActivation
{
  units::time::millisecond_t durationSinceInitiation;
  bool hasStateChanged;
  double probActivation;
  double maxProb;
  double minProb;
  bool activationState;
  bool expectedResult;
};

class MentalModel_ImplementStochasticActivation : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_ImplementStochasticActivation>
{
};

TEST_P(MentalModel_ImplementStochasticActivation, Check_ImplementStochasticActivation)
{
  MentalModelTester TEST_HELPER;
  DataFor_ImplementStochasticActivation data = GetParam();

  auto startTime = 10.0_ms;
  auto endTime = 20.0_ms;

  TEST_HELPER.mentalModel.SET_CYCLE_TIME(100_ms);

  auto result = TEST_HELPER.mentalModel.ImplementStochasticActivation(startTime,
                                                                      endTime,
                                                                      data.durationSinceInitiation,
                                                                      data.hasStateChanged,
                                                                      data.probActivation,
                                                                      data.maxProb,
                                                                      data.minProb,
                                                                      data.activationState);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_ImplementStochasticActivation,
    testing::Values(
        DataFor_ImplementStochasticActivation{
            .durationSinceInitiation = 1.0_ms,
            .hasStateChanged = false,
            .probActivation = 10.0,
            .maxProb = 1.0,
            .minProb = 1.0,
            .activationState = true,
            .expectedResult = true},
        DataFor_ImplementStochasticActivation{
            .durationSinceInitiation = 15.0_ms,
            .hasStateChanged = true,
            .probActivation = 1.4,
            .maxProb = 2.0,
            .minProb = 1.0,
            .activationState = true,
            .expectedResult = true},
        DataFor_ImplementStochasticActivation{
            .durationSinceInitiation = 15.0_ms,
            .hasStateChanged = true,
            .probActivation = 1.6,
            .maxProb = 2.0,
            .minProb = 1.0,
            .activationState = true,
            .expectedResult = false},
        DataFor_ImplementStochasticActivation{
            .durationSinceInitiation = 40.0_ms,
            .hasStateChanged = true,
            .probActivation = 1.6,
            .maxProb = 2.0,
            .minProb = 1.0,
            .activationState = true,
            .expectedResult = false}));

/********************************
 * CHECK GetTimeSinceLastUpdate *
 ********************************/

struct DataFor_GetTimeSinceLastUpdate
{
  AreaOfInterest aoi;
  units::time::millisecond_t time;
  units::time::millisecond_t expectedResult;
};

class MentalModel_GetTimeSinceLastUpdate : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetTimeSinceLastUpdate>
{
};

TEST_P(MentalModel_GetTimeSinceLastUpdate, Check_GetTimeSinceLastUpdate)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetTimeSinceLastUpdate data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(data.aoi)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(data.aoi, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetTimeSinceLastUpdate(data.aoi), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetTimeSinceLastUpdate,
    testing::Values(
        DataFor_GetTimeSinceLastUpdate{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .time = 100_ms,
            .expectedResult = 87_ms},
        DataFor_GetTimeSinceLastUpdate{
            .aoi = AreaOfInterest::EGO_FRONT,
            .time = 200_ms,
            .expectedResult = 187_ms}));

/**************************
 * CHECK GetLaneExistence *
 **************************/

struct DataFor_GetLaneExistence
{
  bool isEmergency;
  bool exists;
  scm::LaneType laneType;
  bool expectedResult;
};

class MentalModel_GetLaneExistence : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GetLaneExistence>
{
};

TEST_P(MentalModel_GetLaneExistence, Check_GetLaneExistence)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLaneExistence data = GetParam();

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  LaneInformationGeometrySCM laneInfo;
  laneInfo.exists = data.exists;
  laneInfo.laneType = data.laneType;
  ON_CALL(*fakeInfrastructureCollection, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLaneExistence(RelativeLane::EGO, data.isEmergency), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLaneExistence,
    testing::Values(
        DataFor_GetLaneExistence{
            .isEmergency = true,
            .exists = true,
            .laneType = scm::LaneType::Undefined,
            .expectedResult = true},
        DataFor_GetLaneExistence{
            .isEmergency = true,
            .exists = false,
            .laneType = scm::LaneType::Undefined,
            .expectedResult = false},
        DataFor_GetLaneExistence{
            .isEmergency = false,
            .exists = false,
            .laneType = scm::LaneType::Undefined,
            .expectedResult = false},
        DataFor_GetLaneExistence{
            .isEmergency = false,
            .exists = true,
            .laneType = scm::LaneType::Undefined,
            .expectedResult = false},
        DataFor_GetLaneExistence{
            .isEmergency = false,
            .exists = true,
            .laneType = scm::LaneType::Driving,
            .expectedResult = true}));

/***************************
 * CHECK GetReliabilityMap *
 **************************/

TEST(MentalModel_GetReliabilityMap, Check_GetReliabilityMap)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 10_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 12_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  ON_CALL(*fakeMicroscopicData, GetObjectInformation(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.GetReliabilityMap(AreaOfInterest::EGO_FRONT, 0);

  ASSERT_EQ(result.at(FieldOfViewAssignment::FOVEA), 10_ms);
  ASSERT_EQ(result.at(FieldOfViewAssignment::UFOV), 11_ms);
  ASSERT_EQ(result.at(FieldOfViewAssignment::PERIPHERY), 12_ms);
}

/*******************
 * CHECK GetTauDot *
 *******************/

struct DataFor_GetTauDot
{
  AreaOfInterest aoi;
  double expectedResult;
};

class MentalModel_GetTauDot : public ::testing::Test,
                              public ::testing::WithParamInterface<DataFor_GetTauDot>
{
};

TEST_P(MentalModel_GetTauDot, Check_GetTauDot)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetTauDot data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetTauDot()).WillByDefault(Return(1));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetTauDot(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetTauDot,
    testing::Values(
        DataFor_GetTauDot{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 1},
        DataFor_GetTauDot{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = ScmDefinitions::DEFAULT_VALUE}));

/*****************************
 * CHECK GetCurrentSituation *
 *****************************/

TEST(MentalModel_GetCurrentSituation, Check_GetCurrentSituation)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = Situation::LANE_CHANGER_FROM_LEFT;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCurrentSituation(), Situation::LANE_CHANGER_FROM_LEFT);
}

/***********************************
 * CHECK GetAgentCooperationFactor *
 ***********************************/

TEST(MentalModel_GetAgentCooperationFactor, Check_GetAgentCooperationFactor)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameter;
  driverParameter.agentCooperationFactor = 0.8;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAgentCooperationFactor(), 0.8);
}

/****************************************************************
 * CHECK GetAgentCooperationFactorForSuspiciousBehaviourEvasion *
 ****************************************************************/

TEST(MentalModel_GetAgentCooperationFactorForSuspiciousBehaviourEvasion, Check_GetAgentCooperationFactorForSuspiciousBehaviourEvasion)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameter;
  driverParameter.agentSuspiciousBehaviourEvasionFactor = 0.6;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAgentCooperationFactorForSuspiciousBehaviourEvasion(), 0.6);
}

/*******************************************
 * CHECK CheckSituationCooperativeBehavior *
 *******************************************/

struct DataFor_CheckSituationCooperativeBehavior
{
  double agentCooperationFactor;
  bool expectedResult;
};

class MentalModel_CheckSituationCooperativeBehavior : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_CheckSituationCooperativeBehavior>
{
};

TEST_P(MentalModel_CheckSituationCooperativeBehavior, Check_CheckSituationCooperativeBehavior)
{
  MentalModelTester TEST_HELPER;
  DataFor_CheckSituationCooperativeBehavior data = GetParam();
  NiceMock<FakeStochastics> fakeStochastics;

  ON_CALL(fakeStochastics, GetUniformDistributed(0, 1)).WillByDefault(Return(0.5));

  DriverParameters driverParameter;
  driverParameter.agentCooperationFactor = data.agentCooperationFactor;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);
  TEST_HELPER.mentalModel.CheckSituationCooperativeBehavior();
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCooperativeBehavior(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CheckSituationCooperativeBehavior,
    testing::Values(
        DataFor_CheckSituationCooperativeBehavior{
            .agentCooperationFactor = 0.6,
            .expectedResult = true},
        DataFor_CheckSituationCooperativeBehavior{
            .agentCooperationFactor = 0.4,
            .expectedResult = false}));

/*****************************
 * CHECK SetCurrentSituation *
 *****************************/

struct DataFor_SetCurrentSituation
{
  Situation currentSituation;
};

class MentalModel_SetCurrentSituation : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_SetCurrentSituation>
{
};

TEST_P(MentalModel_SetCurrentSituation, Check_SetCurrentSituation)
{
  MentalModelTester TEST_HELPER;
  DataFor_SetCurrentSituation data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = Situation::LANE_CHANGER_FROM_LEFT;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData).Times(2).WillRepeatedly(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  TEST_HELPER.mentalModel.SetCurrentSituation(Situation::FREE_DRIVING);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_SetCurrentSituation,
    testing::Values(
        DataFor_SetCurrentSituation{
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT},
        DataFor_SetCurrentSituation{
            .currentSituation = Situation::FREE_DRIVING}));

/********************************
 * CHECK GetHasSituationChanged *
 ********************************/

struct DataFor_GetHasSituationChanged
{
  bool hasSituationChanged;
  bool expectedResult;
};

class MentalModel_GetHasSituationChanged : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetHasSituationChanged>
{
};

TEST_P(MentalModel_GetHasSituationChanged, Check_GetHasSituationChanged)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetHasSituationChanged data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.hasSituationChanged = data.hasSituationChanged;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.GetHasSituationChanged();
  ASSERT_EQ(data.expectedResult, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetHasSituationChanged,
    testing::Values(
        DataFor_GetHasSituationChanged{
            .hasSituationChanged = false,
            .expectedResult = false},
        DataFor_GetHasSituationChanged{
            .hasSituationChanged = true,
            .expectedResult = true}));

/******************************************
 * CHECK GetHasMesoscopicSituationChanged *
 ******************************************/

struct DataFor_GetHasMesoscopicSituationChanged
{
  bool hasMesoscopicSituationChanged;
  bool expectedResult;
};

class MentalModel_GetHasMesoscopicSituationChanged : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_GetHasMesoscopicSituationChanged>
{
};

TEST_P(MentalModel_GetHasMesoscopicSituationChanged, Check_GetHasMesoscopicSituationChanged)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetHasMesoscopicSituationChanged data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.hasMesoscopicSituationChanged = data.hasMesoscopicSituationChanged;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.GetHasMesoscopicSituationChanged();
  ASSERT_EQ(data.expectedResult, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetHasMesoscopicSituationChanged,
    testing::Values(
        DataFor_GetHasMesoscopicSituationChanged{
            .hasMesoscopicSituationChanged = false,
            .expectedResult = false},
        DataFor_GetHasMesoscopicSituationChanged{
            .hasMesoscopicSituationChanged = true,
            .expectedResult = true}));

/**********************************
 * CHECK HasLeadingVehicleChanged *
 **********************************/

TEST(MentalModel_HasLeadingVehicleChanged, Check_HasLeadingVehicleChanged)
{
  MentalModelTester TEST_HELPER;
  TEST_HELPER.mentalModel.SET_LEADING_VEHICLE_CHANGED(true);

  ASSERT_TRUE(TEST_HELPER.mentalModel.HasLeadingVehicleChanged());
}

/**********************************
 * CHECK GetIsLateralActionForced *
 **********************************/
struct DataFor_GetIsLateralActionForced
{
  LaneChangeAction laneChangeAction;
  bool expectedResult;
};

class MentalModel_GetIsLateralActionForced : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_GetIsLateralActionForced>
{
};

TEST_P(MentalModel_GetIsLateralActionForced, Check_GetIsLateralActionForced)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetIsLateralActionForced data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.currentLaneChangeForced = data.laneChangeAction;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetIsLateralActionForced(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetIsLateralActionForced,
    testing::Values(
        DataFor_GetIsLateralActionForced{
            .laneChangeAction = LaneChangeAction::ForcePositive,
            .expectedResult = true},
        DataFor_GetIsLateralActionForced{
            .laneChangeAction = LaneChangeAction::IntentPositive,
            .expectedResult = false}));

/***************************************
 * CHECK GetIsLaneChangePastTransition *
 ***************************************/

TEST(MentalModel_GetIsLaneChangePastTransition, Check_GetIsLaneChangePastTransition)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_LANE_CHANGE_PAST_TRANSITION(true);

  ASSERT_TRUE(TEST_HELPER.mentalModel.GetIsLaneChangePastTransition());
}

/****************************
 * CHECK GetIsMergeRegulate *
 ****************************/

struct DataFor_GetIsMergeRegulate
{
  AreaOfInterest aoi;
  double mergeRegulateId;
  bool expectedResult;
};

class MentalModel_GetIsMergeRegulate : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_GetIsMergeRegulate>
{
};

TEST_P(MentalModel_GetIsMergeRegulate, Check_GetIsMergeRegulate)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetIsMergeRegulate data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetId()).WillByDefault(Return(1));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeEgoFrontVehicle};

  TEST_HELPER.mentalModel.SetMergeRegulateId(data.mergeRegulateId);

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetIsMergeRegulate(data.aoi, 0), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetIsMergeRegulate,
    testing::Values(
        DataFor_GetIsMergeRegulate{
            .aoi = AreaOfInterest::EGO_FRONT,
            .mergeRegulateId = 1,
            .expectedResult = true},
        DataFor_GetIsMergeRegulate{
            .aoi = AreaOfInterest::EGO_FRONT,
            .mergeRegulateId = 2,
            .expectedResult = false},
        DataFor_GetIsMergeRegulate{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = false}));

/***************************
 * CHECK GetIsAnticipating *
 ***************************/

TEST(MentalModel_GetIsAnticipating, Check_GetIsAnticipating)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.isAnticipating = true;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_TRUE(TEST_HELPER.mentalModel.GetIsAnticipating());
}

/**************************
 * CHECK GetLateralAction *
 **************************/

TEST(MentalModel_GetLateralAction, Check_GetLateralAction)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  LateralAction action;
  action.direction = LateralAction::Direction::LEFT;
  action.state = LateralAction::State::LANE_CHANGING;

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralAction = action;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  auto result = TEST_HELPER.mentalModel.GetLateralAction();

  ASSERT_EQ(result.direction, action.direction);
  ASSERT_EQ(result.state, action.state);
}

/*****************************
 * CHECK GetCurrentDirection *
 *****************************/

struct DataFor_GetCurrentDirection
{
  int direction;
  SurroundingLane expectedResult;
};

class MentalModel_GetCurrentDirection : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_GetCurrentDirection>
{
};

TEST_P(MentalModel_GetCurrentDirection, Check_GetCurrentDirection)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetCurrentDirection data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.direction = data.direction;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCurrentDirection(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetCurrentDirection,
    testing::Values(
        DataFor_GetCurrentDirection{
            .direction = 0,
            .expectedResult = SurroundingLane::EGO},
        DataFor_GetCurrentDirection{
            .direction = 1,
            .expectedResult = SurroundingLane::LEFT},
        DataFor_GetCurrentDirection{
            .direction = 2,
            .expectedResult = SurroundingLane::RIGHT}));

/******************************************
 * CHECK Get Set DurationCurrentSituation *
 ******************************************/

TEST(MentalModel_GetSetDurationCurrentSituation, Check_GetSetDurationCurrentSituation)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetDurationCurrentSituation(10.0_ms);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetDurationCurrentSituation(), 10_ms);
}

/********************************************
 * CHECK GetIndicatorActiviationProbability *
 ********************************************/

TEST(MentalModel_GetIndicatorActiviationProbability, Check_GetIndicatorActiviationProbability)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_PROB_INDICATOR_ACTIVATION(10.0);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetIndicatorActiviationProbability(), 10);
}

/******************************************
 * CHECK GetFlasherActiviationProbability *
 ******************************************/

TEST(MentalModel_GetFlasherActiviationProbability, Check_GetFlasherActiviationProbability)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_PROB_FLASHER_ACTIVATION(10.0);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetFlasherActiviationProbability(), 10);
}

/**********************************
 * CHECK IsMergePreparationActive *
 **********************************/

struct DataFor_IsMergePreparationActive
{
  bool expectedResult;
};

class MentalModel_IsMergePreparationActive : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsMergePreparationActive>
{
};

TEST_P(MentalModel_IsMergePreparationActive, Check_IsMergePreparationActive)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsMergePreparationActive data = GetParam();

  NiceMock<FakeSurroundingVehicle> leader;
  ON_CALL(leader, GetId()).WillByDefault(Return(2));

  Merge::Phase phase;
  phase.distance = 100_m;
  phase.time = 100_s;
  phase.mergeAction = MergeAction::ACCELERATING;

  Merge::Gap gap(&leader, nullptr);

  if (data.expectedResult)
  {
    gap.selectedMergePhases = {phase};
  }
  else
  {
    gap.selectedMergePhases = {};
  }

  TEST_HELPER.mentalModel.SET_MERGE_GAP(gap);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsMergePreparationActive(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsMergePreparationActive,
    testing::Values(
        DataFor_IsMergePreparationActive{
            .expectedResult = true},
        DataFor_IsMergePreparationActive{
            .expectedResult = false}));

/********************************************
 * CHECK IsSideLaneSafeWithMergePreparation *
 ********************************************/

struct DataFor_IsSideLaneSafeWithMergePreparation
{
  bool isLaneDriveablePerceived;
  bool laneExists;
  units::time::millisecond_t time;
  bool expectedResult;
};

class MentalModel_IsSideLaneSafeWithMergePreparation : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_IsSideLaneSafeWithMergePreparation>
{
};

TEST_P(MentalModel_IsSideLaneSafeWithMergePreparation, Check_IsSideLaneSafeWithMergePreparation)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsSideLaneSafeWithMergePreparation data = GetParam();

  LateralAction action;
  action.direction = LateralAction::Direction::LEFT;
  action.state = LateralAction::State::LANE_KEEPING;

  auto fakeMicroscopicData = std::make_unique<NiceMock<FakeMicroscopicCharacteristics>>();
  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralAction = action;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();
  LaneInformationGeometrySCM laneInfo;
  laneInfo.exists = data.laneExists;
  laneInfo.laneType = scm::LaneType::Driving;
  ON_CALL(*fakeInfrastructureCollection, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  NiceMock<FakeSurroundingVehicle> leader;
  ON_CALL(leader, GetId()).WillByDefault(Return(2));

  Merge::Phase phase;
  phase.distance = 100_m;
  phase.time = 100_s;
  phase.mergeAction = MergeAction::ACCELERATING;

  Merge::Gap gap(&leader, nullptr);
  gap.selectedMergePhases = {phase};
  gap.timestep = 100_ms;

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneDriveablePerceived(_)).WillByDefault(Return(data.isLaneDriveablePerceived));

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeEgoFrontVehicle;
  ON_CALL(fakeEgoFrontVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeEgoFrontVehicle, IsReliable(_, _)).WillByDefault(Return(true));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeEgoFrontVehicle};

  auto fakeMerging = std::make_unique<NiceMock<FakeMerging>>();

  NiceMock<FakeSurroundingVehicle> bestLeader;
  NiceMock<FakeSurroundingVehicle> bestFollower;

  ON_CALL(bestLeader, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));
  ON_CALL(bestLeader, GetId()).WillByDefault(Return(2));

  ObstructionLongitudinal obstructionLeader;
  obstructionLeader.mainLaneLocator = 2.0_m;
  ON_CALL(bestLeader, GetLongitudinalObstruction()).WillByDefault(Return(obstructionLeader));
  ON_CALL(bestLeader, GetLength()).WillByDefault(Return(5.0_m));

  ObstructionLongitudinal obstructionFollower;
  obstructionFollower.mainLaneLocator = 2.0_m;
  ON_CALL(bestFollower, GetLongitudinalObstruction()).WillByDefault(Return(obstructionFollower));
  Merge::Gap bestGap(&bestLeader, &bestFollower);

  if (!data.expectedResult)
  {
    bestGap.selectedMergePhases = {};
  }
  else
  {
    bestGap.selectedMergePhases = {phase};
  }

  ON_CALL(*fakeMerging, RetrieveMergeGap(_, _)).WillByDefault(Return(bestGap));

  TEST_HELPER.mentalModel.SET_MERGING(std::move(fakeMerging));
  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.SET_MERGE_GAP(gap);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.IsSideLaneSafeWithMergePreparation(AreaOfInterest::LEFT_FRONT), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsSideLaneSafeWithMergePreparation,
    testing::Values(
        DataFor_IsSideLaneSafeWithMergePreparation{
            .isLaneDriveablePerceived = false,
            .laneExists = true,
            .time = 2500_ms,
            .expectedResult = false},
        DataFor_IsSideLaneSafeWithMergePreparation{
            .isLaneDriveablePerceived = true,
            .laneExists = true,
            .time = 2500_ms,
            .expectedResult = false},
        DataFor_IsSideLaneSafeWithMergePreparation{
            .isLaneDriveablePerceived = true,
            .laneExists = true,
            .time = 2500_ms,
            .expectedResult = true},
        DataFor_IsSideLaneSafeWithMergePreparation{
            .isLaneDriveablePerceived = false,
            .laneExists = false,
            .time = 0_ms,
            .expectedResult = true}));

/****************************
 * CHECK GetPreviewDistance *
 ****************************/

TEST(MentalModel_GetPreviewDistance, Check_GetPreviewDistance)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameter;
  driverParameter.previewDistance = 100_m;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetPreviewDistance(), 100_m);
}

/*************************
 * CHECK GetMeanVelocity *
 *************************/

struct DataFor_GetMeanVelocity
{
  SurroundingLane lane;
  units::velocity::meters_per_second_t meanVelocity;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetMeanVelocity : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_GetMeanVelocity>
{
};

TEST_P(MentalModel_GetMeanVelocity, Check_GetMeanVelocity)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetMeanVelocity data = GetParam();

  auto fakeTrafficFlow = std::make_shared<FakeTrafficFlow>();
  ON_CALL(*fakeTrafficFlow, GetMeanVelocity(_)).WillByDefault(Return(data.meanVelocity));

  TEST_HELPER.mentalModel.SET_TRAFFIC_FLOW(fakeTrafficFlow);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetMeanVelocity(data.lane), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetMeanVelocity,
    testing::Values(
        DataFor_GetMeanVelocity{
            .lane = SurroundingLane::LEFT,
            .meanVelocity = 50.0_mps,
            .expectedResult = 50.0_mps},
        DataFor_GetMeanVelocity{
            .lane = SurroundingLane::RIGHT,
            .meanVelocity = 60.0_mps,
            .expectedResult = 60.0_mps},
        DataFor_GetMeanVelocity{
            .lane = SurroundingLane::EGO,
            .meanVelocity = 70.0_mps,
            .expectedResult = 70.0_mps}));

/***********************************
 * CHECK IsUrgentlyLateWithUpdate  *
 ***********************************/

struct DataFor_IsUrgentlyLateWithUpdate
{
  GazeState gs;
  units::time::millisecond_t time;
  bool expectedResult;
};

class MentalModel_IsUrgentlyLateWithUpdate : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsUrgentlyLateWithUpdate>
{
};

TEST_P(MentalModel_IsUrgentlyLateWithUpdate, Check_IsUrgentlyLateWithUpdate)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsUrgentlyLateWithUpdate data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsUrgentlyLateWithUpdate(data.gs), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsUrgentlyLateWithUpdate,
    testing::Values(
        DataFor_IsUrgentlyLateWithUpdate{
            .gs = GazeState::LEFT_SIDE,
            .time = 100_ms,
            .expectedResult = false},
        DataFor_IsUrgentlyLateWithUpdate{
            .gs = GazeState::EGO_FRONT,
            .time = 1100_ms,
            .expectedResult = true}));

/******************************************
 * CHECK IsUrgentlyLateWithUpdateWithAoi  *
 ******************************************/

struct DataFor_IsUrgentlyLateWithUpdateWithAoi
{
  AreaOfInterest aoi;
  units::time::millisecond_t time;
  bool expectedResult;
};

class MentalModel_IsUrgentlyLateWithUpdateWithAoi : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_IsUrgentlyLateWithUpdateWithAoi>
{
};

TEST_P(MentalModel_IsUrgentlyLateWithUpdateWithAoi, Check_IsUrgentlyLateWithUpdateWithAoi)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsUrgentlyLateWithUpdateWithAoi data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsUrgentlyLateWithUpdate(data.aoi), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsUrgentlyLateWithUpdateWithAoi,
    testing::Values(
        DataFor_IsUrgentlyLateWithUpdateWithAoi{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .time = 100_ms,
            .expectedResult = false},
        DataFor_IsUrgentlyLateWithUpdateWithAoi{
            .aoi = AreaOfInterest::EGO_FRONT,
            .time = 1100_ms,
            .expectedResult = true}));

/****************************************
 * CHECK Get Set UrgentUpdateThreshold  *
 ****************************************/

TEST(MenatlModel_Get_Set_UrgentUpdateThreshold, Check_Get_Set_UrgentUpdateThreshold)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetUrgentUpdateThreshold(1200_ms);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetUrgentUpdateThreshold(), 1200_ms);
}

/******************************
 * CHECK GetInconsistentAois  *
 ******************************/

TEST(MentalModel_GetInconsistentAois, Check_GetInconsistentAois)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.MarkInconsistent(AreaOfInterest::LEFT_FRONT);

  auto result = TEST_HELPER.mentalModel.GetInconsistentAois();

  ASSERT_EQ(result.at(0), AreaOfInterest::LEFT_FRONT);
}

/****************************
 * CHECK IsEarlyWithUpdate  *
 ****************************/

struct DataFor_IsEarlyWithUpdateWithGazeState
{
  GazeState gs;
  units::time::millisecond_t time;
  bool expectedResult;
};

class MentalModel_IsEarlyWithUpdateWithGazeState : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_IsEarlyWithUpdateWithGazeState>
{
};

TEST_P(MentalModel_IsEarlyWithUpdateWithGazeState, Check_IsEarlyWithUpdateWithGazeState)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsEarlyWithUpdateWithGazeState data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsEarlyWithUpdate(data.gs), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsEarlyWithUpdateWithGazeState,
    testing::Values(
        DataFor_IsEarlyWithUpdateWithGazeState{
            .gs = GazeState::LEFT_SIDE,
            .time = 100_ms,
            .expectedResult = true},
        DataFor_IsEarlyWithUpdateWithGazeState{
            .gs = GazeState::EGO_FRONT,
            .time = 2200_ms,
            .expectedResult = false}));

/***********************************
 * CHECK IsEarlyWithUpdateWithAoi  *
 ***********************************/

struct DataFor_IsEarlyWithUpdateWithAoi
{
  AreaOfInterest aoi;
  units::time::millisecond_t time;
  bool expectedResult;
};

class MentalModel_IsEarlyWithUpdateWithAoi : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsEarlyWithUpdateWithAoi>
{
};

TEST_P(MentalModel_IsEarlyWithUpdateWithAoi, Check_IsEarlyWithUpdateWithAoi)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsEarlyWithUpdateWithAoi data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(data.time);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsEarlyWithUpdate(data.aoi), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsEarlyWithUpdateWithAoi,
    testing::Values(
        DataFor_IsEarlyWithUpdateWithAoi{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .time = 100_ms,
            .expectedResult = true},
        DataFor_IsEarlyWithUpdateWithAoi{
            .aoi = AreaOfInterest::EGO_FRONT,
            .time = 2200_ms,
            .expectedResult = false}));

/*************************************
 * CHECK GetPrioritySinceLastUpdate  *
 *************************************/

TEST(MentalModel_GetPrioritySinceLastUpdate, Check_GetPrioritySinceLastUpdate)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::vector<ObjectInformationScmExtended> objects;

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended objectInfo;
  objectInfo.id = 1;
  objectInfo.reliabilityMap = reliabilityMap;

  objects.push_back(objectInfo);

  ON_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).WillByDefault(Return(&objects));
  ON_CALL(*fakeMicroscopicData, GetObjectInformation(_, _)).WillByDefault(Return(&objectInfo));

  TEST_HELPER.mentalModel.SetTime(2013_ms);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetPrioritySinceLastUpdate(GazeState::LEFT_SIDE), 2.0);
}

/*******************************
 * CHECK UpdateReliabilityMap  *
 *******************************/

struct DataFor_UpdateReliabilityMap
{
  AreaOfInterest fovea;
  int sideVectorTimes;
  int foveaTimes;
  AreaOfInterest ufov;
  int ufovTimes;
  int time;
  AreaOfInterest periphery;
  int peripheryTimes;
  bool idealPerception;
  int idealPerceptionTimes;
};

class MentalModel_UpdateReliabilityMap : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_UpdateReliabilityMap>
{
};
TEST_P(MentalModel_UpdateReliabilityMap, Check_UpdateReliabilityMap)
{
  MentalModelTester TEST_HELPER;
  DataFor_UpdateReliabilityMap data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.fovea = data.fovea;
  ownVehicleInfo.ufov = {data.ufov};
  ownVehicleInfo.periphery = {data.periphery};

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  std::map<FieldOfViewAssignment, units::time::millisecond_t> reliabilityMap;
  reliabilityMap[FieldOfViewAssignment::FOVEA] = 11_ms;
  reliabilityMap[FieldOfViewAssignment::UFOV] = 12_ms;
  reliabilityMap[FieldOfViewAssignment::PERIPHERY] = 13_ms;

  ObjectInformationScmExtended vehicle;
  vehicle.reliabilityMap = reliabilityMap;
  std::vector<ObjectInformationScmExtended> objects{vehicle};
  EXPECT_CALL(*fakeMicroscopicData, GetSideObjectVector(_)).Times(data.sideVectorTimes).WillRepeatedly(Return(&objects));

  DriverParameters driverParameter;
  driverParameter.idealPerception = data.idealPerception;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(data.fovea, _)).Times(data.foveaTimes).WillOnce(Return(&vehicle));
  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(data.ufov, _)).Times(data.ufovTimes).WillOnce(Return(&vehicle));
  EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(data.periphery, _)).Times(data.peripheryTimes).WillOnce(Return(&vehicle));

  if (data.idealPerception)
  {
    EXPECT_CALL(*fakeMicroscopicData, UpdateObjectInformation(_, _)).Times(data.idealPerceptionTimes).WillRepeatedly(Return(&vehicle));
  }

  TEST_HELPER.mentalModel.SetTime(400_ms);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  TEST_HELPER.mentalModel.UpdateReliabilityMap();
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_UpdateReliabilityMap,
    testing::Values(
        DataFor_UpdateReliabilityMap{
            .fovea = AreaOfInterest::LEFT_SIDE,
            .sideVectorTimes = 6,
            .foveaTimes = 1,
            .ufov = AreaOfInterest::LEFTLEFT_SIDE,
            .ufovTimes = 1,
            .periphery = AreaOfInterest::RIGHT_SIDE,
            .peripheryTimes = 1,
            .idealPerception = false,
            .idealPerceptionTimes = 0},
        DataFor_UpdateReliabilityMap{
            .fovea = AreaOfInterest::EGO_FRONT,
            .sideVectorTimes = 0,
            .foveaTimes = 1,
            .ufov = AreaOfInterest::EGO_FRONT_FAR,
            .ufovTimes = 1,
            .periphery = AreaOfInterest::EGO_REAR,
            .peripheryTimes = 1,
            .idealPerception = false,
            .idealPerceptionTimes = 0},
        DataFor_UpdateReliabilityMap{
            .fovea = AreaOfInterest::NumberOfAreaOfInterests,
            .sideVectorTimes = 8,
            .foveaTimes = 0,
            .ufov = AreaOfInterest::NumberOfAreaOfInterests,
            .ufovTimes = 0,
            .periphery = AreaOfInterest::NumberOfAreaOfInterests,
            .peripheryTimes = 0,
            .idealPerception = true,
            .idealPerceptionTimes = 23}));

/***********************************************
 * CHECK IsSideSideLaneObjectChangingIntoSide  *
 ***********************************************/

struct DataFor_IsSideSideLaneObjectChangingIntoSide
{
  RelativeLane lane;
  bool merge;
  bool isLaneExistence;
  bool isExceedingLateralMotionThresholdTowardsEgoLane;
  bool isMinimumFollowingDistanceViolated;
  bool expectedResult;
};

class MentalModel_IsSideSideLaneObjectChangingIntoSide : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_IsSideSideLaneObjectChangingIntoSide>
{
};

TEST_P(MentalModel_IsSideSideLaneObjectChangingIntoSide, Check_IsSideSideLaneObjectChangingIntoSide)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsSideSideLaneObjectChangingIntoSide data = GetParam();

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();
  ON_CALL(*fakeInfrastructureCollection, CheckLaneExistence(_, _)).WillByDefault(Return(data.isLaneExistence));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsExceedingLateralMotionThresholdTowardsEgoLane(_)).WillByDefault(Return(data.isExceedingLateralMotionThresholdTowardsEgoLane));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsMinimumFollowingDistanceViolated(_, _)).WillByDefault(Return(data.isMinimumFollowingDistanceViolated));

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeRightRigthFrontVehicle;
  ON_CALL(fakeRightRigthFrontVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeRightRigthFrontVehicle, GetIndicatorState()).WillByDefault(Return(scm::LightState::Indicator::Left));
  aoiMapping[AreaOfInterest::RIGHTRIGHT_FRONT] = {&fakeRightRigthFrontVehicle};

  NiceMock<FakeSurroundingVehicle> fakeRightRigthSideVehicle;
  ON_CALL(fakeRightRigthSideVehicle, GetId()).WillByDefault(Return(2));
  ON_CALL(fakeRightRigthSideVehicle, GetIndicatorState()).WillByDefault(Return(scm::LightState::Indicator::Left));
  aoiMapping[AreaOfInterest::RIGHTRIGHT_FRONT] = {&fakeRightRigthSideVehicle};

  NiceMock<FakeSurroundingVehicle> fakeRightRigthRearVehicle;
  ON_CALL(fakeRightRigthRearVehicle, GetId()).WillByDefault(Return(3));
  ON_CALL(fakeRightRigthRearVehicle, GetIndicatorState()).WillByDefault(Return(scm::LightState::Indicator::Left));
  aoiMapping[AreaOfInterest::RIGHTRIGHT_FRONT] = {&fakeRightRigthRearVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(1.0));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetIntensityForNeedToChangeLane(_)).WillByDefault(Return(2.0));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  auto result = TEST_HELPER.mentalModel.IsSideSideLaneObjectChangingIntoSide(data.lane, data.merge);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsSideSideLaneObjectChangingIntoSide,
    testing::Values(
        DataFor_IsSideSideLaneObjectChangingIntoSide{
            .lane = RelativeLane::LEFT,
            .merge = true,
            .isLaneExistence = false,
            .isExceedingLateralMotionThresholdTowardsEgoLane = false,
            .isMinimumFollowingDistanceViolated = false,
            .expectedResult = false},
        DataFor_IsSideSideLaneObjectChangingIntoSide{
            .lane = RelativeLane::RIGHT,
            .merge = true,
            .isLaneExistence = false,
            .isExceedingLateralMotionThresholdTowardsEgoLane = false,
            .isMinimumFollowingDistanceViolated = false,
            .expectedResult = false},
        DataFor_IsSideSideLaneObjectChangingIntoSide{
            .lane = RelativeLane::RIGHT,
            .merge = true,
            .isLaneExistence = true,
            .isExceedingLateralMotionThresholdTowardsEgoLane = true,
            .isMinimumFollowingDistanceViolated = true,
            .expectedResult = true},
        DataFor_IsSideSideLaneObjectChangingIntoSide{
            .lane = RelativeLane::RIGHT,
            .merge = false,
            .isLaneExistence = true,
            .isExceedingLateralMotionThresholdTowardsEgoLane = false,
            .isMinimumFollowingDistanceViolated = true,
            .expectedResult = true},
        DataFor_IsSideSideLaneObjectChangingIntoSide{
            .lane = RelativeLane::RIGHT,
            .merge = false,
            .isLaneExistence = true,
            .isExceedingLateralMotionThresholdTowardsEgoLane = false,
            .isMinimumFollowingDistanceViolated = false,
            .expectedResult = false}));

/**************************************
 * CHECK GetLateralPositionFrontAxle  *
 **************************************/

TEST(MentalModel_GetLateralPositionFrontAxle, Check_GetLateralPositionFrontAxle)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralPositionFrontAxle = 0.5_m;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralPositionFrontAxle(), 0.5_m);
}

/*********************************
 * CHECK GetAbsoluteVelocityEgo  *
 *********************************/

struct DataFor_GetAbsoluteVelocityEgo
{
  bool isEstimatedValue;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetAbsoluteVelocityEgo : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetAbsoluteVelocityEgo>
{
};
TEST_P(MentalModel_GetAbsoluteVelocityEgo, Check_GetAbsoluteVelocityEgo)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetAbsoluteVelocityEgo data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.absoluteVelocity = 10.0_mps;
  ownVehicleInfo.absoluteVelocityReal = 15.0_mps;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAbsoluteVelocityEgo(data.isEstimatedValue), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetAbsoluteVelocityEgo,
    testing::Values(
        DataFor_GetAbsoluteVelocityEgo{
            .isEstimatedValue = true,
            .expectedResult = 10.0_mps},
        DataFor_GetAbsoluteVelocityEgo{
            .isEstimatedValue = false,
            .expectedResult = 15.0_mps}));

/*****************************
 * CHECK GetLateralVelocity  *
 *****************************/

TEST(MentalModel_GetLateralVelocityOverMicroscopicData, Check_GetLateralVelocity)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralVelocity = 0.5_mps;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralVelocity(), 0.5_mps);
}

/**************************************
 * CHECK GetLateralVelocityFrontAxle  *
 **************************************/

TEST(MentalModel_GetLateralVelocityFrontAxle, Check_GetLateralVelocityFrontAxle)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralVelocityFrontAxle = 0.5_mps;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLateralVelocityFrontAxle(), 0.5_mps);
}

/**************************************
 * CHECK GetAbsoluteVelocityTargeted  *
 **************************************/

TEST(MentalModel_GetAbsoluteVelocityTargeted, Check_GetAbsoluteVelocityTargeted)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.absoluteVelocityTargeted = 10.5_mps;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAbsoluteVelocityTargeted(), 10.5_mps);
}

/*************************
 * CHECK GetAcceleration *
 *************************/

TEST(MentalModel_GetAccelerationOverMicroscopicData, Check_GetAcceleration)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.acceleration = 1.5_mps_sq;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetAcceleration(), 1.5_mps_sq);
}

/**************************
 * CHECK GetLegalVelocity *
 **************************/

struct DataFor_GetLegalVelocity
{
  bool signDetected;
  units::length::meter_t relativeDistanceToTrafficsign;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLegalVelocity : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GetLegalVelocity>
{
};
TEST_P(MentalModel_GetLegalVelocity, Check_GetLegalVelocity)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLegalVelocity data = GetParam();

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  LaneInformationTrafficRulesScmExtended laneInfoTrafficRules;
  laneInfoTrafficRules.signDetected = data.signDetected;
  laneInfoTrafficRules.speedLimit = 50.0_mps;
  laneInfoTrafficRules.relativeDistanceToTrafficsign = data.relativeDistanceToTrafficsign;
  laneInfoTrafficRules.previousRelevantSpeedLimit = ScmDefinitions::INF_VELOCITY;
  ON_CALL(*fakeInfrastructureCollection, GetLaneInformationTrafficRules(_)).WillByDefault(ReturnRef(laneInfoTrafficRules));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateVLegalInfluencigDistance(_, _)).WillByDefault(Return(100_m));

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.absoluteVelocity = 20.0_mps;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&agentEgo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(fakeInfrastructureCollection));

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(TEST_HELPER.mentalModel.GetLegalVelocity(SurroundingLane::LEFT));
  }
  else
  {
    ASSERT_EQ(TEST_HELPER.mentalModel.GetLegalVelocity(SurroundingLane::LEFT), data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLegalVelocity,
    testing::Values(
        DataFor_GetLegalVelocity{
            .signDetected = false,
            .relativeDistanceToTrafficsign = 10.0_m,
            .expectedResult = 50.0_mps},
        DataFor_GetLegalVelocity{
            .signDetected = true,
            .relativeDistanceToTrafficsign = 90.0_m,
            .expectedResult = 50.0_mps},
        DataFor_GetLegalVelocity{
            .signDetected = true,
            .relativeDistanceToTrafficsign = 110.0_m,
            .expectedResult = ScmDefinitions::INF_VELOCITY}));

/************************************
 * CHECK GetLongitudinalVelocityEgo *
 ************************************/

TEST(MentalModel_GetLongitudinalVelocityEgo, Check_GetLongitudinalVelocityEgo)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.longitudinalVelocity = 10.0_mps;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLongitudinalVelocityEgo(), 10.0_mps);
}

/*********************************
 * CHECK GetLongitudinalVelocity *
 *********************************/

struct DataFor_GetLongitudinalVelocity
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_GetLongitudinalVelocity : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_GetLongitudinalVelocity>
{
};
TEST_P(MentalModel_GetLongitudinalVelocity, Check_GetLongitudinalVelocity)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLongitudinalVelocity data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.longitudinalVelocity = 20.0_mps;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> rigthVehicle;
  ON_CALL(rigthVehicle, GetLongitudinalVelocity).WillByDefault(Return(10.0_mps));
  aoiMapping[AreaOfInterest::RIGHT_FRONT] = {&rigthVehicle};
  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLongitudinalVelocity(data.aoi), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLongitudinalVelocity,
    testing::Values(
        DataFor_GetLongitudinalVelocity{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expectedResult = 20.0_mps},
        DataFor_GetLongitudinalVelocity{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)},
        DataFor_GetLongitudinalVelocity{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = 10.0_mps}));

/********************
 * CHECK GetHeading *
 ********************/

TEST(MentalModel_GetHeading, Check_GetHeading)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.heading = 0.5_rad;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetHeading(), 0.5_rad);
}

/*************************
 * CHECK GetEgoLaneWidth *
 *************************/

TEST(MentalModel_GetEgoLaneWidth, Check_GetEgoLaneWidth)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.Close().width = 3.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetEgoLaneWidth(), 3.0_m);
}

/***************************************
 * CHECK GetCurvatureOnCurrentPosition *
 ***************************************/

TEST(MentalModel_GetCurvatureOnCurrentPosition, Check_GetCurvatureOnCurrentPosition)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.Close().curvature = 1.5_i_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCurvatureOnCurrentPosition(), 1.5_i_m);
}

/***************************************
 * CHECK GetCurvatureInPreviewDistance *
 ***************************************/

TEST(MentalModel_GetCurvatureInPreviewDistance, Check_GetCurvatureInPreviewDistance)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.Close().curvatureInPreviewDistance = 2.5_i_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCurvatureInPreviewDistance(), 2.5_i_m);
}

/********************************
 * CHECK GetDistanceToEndOfLane *
 ********************************/

struct DataFor_GetDistanceToEndOfLane
{
  RelativeLane lane;
  bool isEmergency;
  units::length::meter_t expectedResult;
};

class MentalModel_GetDistanceToEndOfLane : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetDistanceToEndOfLane>
{
};
TEST_P(MentalModel_GetDistanceToEndOfLane, Check_GetDistanceToEndOfLane)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetDistanceToEndOfLane data = GetParam();
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.Close().distanceToEndOfLaneDuringEmergency = 100.0_m;
  geoInfo.Close().distanceToEndOfLane = 150.0_m;

  geoInfo.Left().distanceToEndOfLaneDuringEmergency = 110.0_m;
  geoInfo.Left().distanceToEndOfLane = 160.0_m;

  geoInfo.Right().distanceToEndOfLaneDuringEmergency = 120.0_m;
  geoInfo.Right().distanceToEndOfLane = 170.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(units::math::isinf(TEST_HELPER.mentalModel.GetDistanceToEndOfLane(data.lane, data.isEmergency)));
  }
  else
  {
    ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceToEndOfLane(data.lane, data.isEmergency), data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetDistanceToEndOfLane,
    testing::Values(
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::EGO,
            .isEmergency = true,
            .expectedResult = 100.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::EGO,
            .isEmergency = false,
            .expectedResult = 150.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::LEFT,
            .isEmergency = true,
            .expectedResult = 110.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::LEFT,
            .isEmergency = false,
            .expectedResult = 160.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::RIGHT,
            .isEmergency = true,
            .expectedResult = 120.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::RIGHT,
            .isEmergency = false,
            .expectedResult = 170.0_m},
        DataFor_GetDistanceToEndOfLane{
            .lane = RelativeLane::RIGHTRIGHT,
            .isEmergency = false,
            .expectedResult = ScmDefinitions::INF_DISTANCE}));

/************************************
 * CHECK GetDistanceToEndOfNextExit *
 ************************************/

TEST(MentalModel_GetDistanceToEndOfNextExit, Check_GetDistanceToEndOfNextExit)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.distanceToEndOfNextExit = 150.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceToEndOfNextExit(), 150.0_m);
}

/**************************************
 * CHECK GetDistanceToStartOfNextExit *
 **************************************/

TEST(MentalModel_GetDistanceToStartOfNextExit, Check_GetDistanceToStartOfNextExit)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.distanceToStartOfNextExit = 170.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetDistanceToStartOfNextExit(), 170.0_m);
}

/*****************************
 * CHECK ResetSpeedLimitSign *
 *****************************/

TEST(MentalModel_ResetSpeedLimitSign, Check_ResetSpeedLimitSign)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.absoluteVelocity = 33.0_mps;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  auto result = TEST_HELPER.mentalModel.ResetSpeedLimitSign();

  ASSERT_TRUE(units::math::isinf(result.speedLimit));
  ASSERT_EQ(result.relativeDistanceToTrafficsign, 999.0_m);
  ASSERT_EQ(result.timeOfLastDetection, 0.0_ms);
  ASSERT_EQ(result.speedWhenDetected, 33.0_mps);
  ASSERT_TRUE(units::math::isinf(result.previousRelevantSpeedLimit));
  ASSERT_FALSE(result.signDetected);
}

/***********************************
 * CHECK GetVelocityViolationDelta *
 ***********************************/

TEST(MentalModel_GetVelocityViolationDelta, Check_GetVelocityViolationDelta)
{
  MentalModelTester TEST_HELPER;

  DriverParameters driverParameter;
  driverParameter.amountOfSpeedLimitViolation = 10.0_mps;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetVelocityViolationDelta(), 10.0_mps);
}

/**************************************
 * CHECK DetermineVelocityReasonSight *
 **************************************/

TEST(MentalModel_DetermineVelocityReasonSight, Check_DetermineVelocityReasonSight)
{
  MentalModelTester TEST_HELPER;

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.visibilityDistance = 300.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  DriverParameters driverParameter;
  driverParameter.maximumLongitudinalDeceleration = 1.5_mps_sq;
  driverParameter.carQueuingDistance = 10.0_m;
  driverParameter.reactionBaseTimeMinimum = 1.0_s;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetNormalDistributed(1, .1)).WillByDefault(Return(0.5));

  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));

  auto result = TEST_HELPER.mentalModel.DetermineVelocityReasonSight();
  EXPECT_NEAR(result.value(), 28.03, 0.01);
}

/******************************************
 * CHECK DetermineVelocityReasonLaneWidth *
 ******************************************/

TEST(MentalModel_DetermineVelocityReasonLaneWidth, Check_DetermineVelocityReasonLaneWidth)
{
  MentalModelTester TEST_HELPER;

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.Close().width = 5.0_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.width = 4.0_m;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));

  auto result = TEST_HELPER.mentalModel.DetermineVelocityReasonLaneWidth();
  EXPECT_NEAR(result.value(), 48.57, 0.01);
}

/******************************************
 * CHECK DetermineVelocityReasonCurvature *
 ******************************************/

TEST(MentalModel_DetermineVelocityReasonCurvature, Check_DetermineVelocityReasonCurvature)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  DriverParameters driverParameter;
  driverParameter.comfortLateralAcceleration = 2.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsOnEntryLane()).WillByDefault(Return(true));

  GeometryInformationSCM geoInfo;
  geoInfo.Close().curvatureInPreviewDistance = 2.0_i_m;
  geoInfo.Close().curvature = 0.5_i_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  auto result = TEST_HELPER.mentalModel.DetermineVelocityReasonCurvature();

  EXPECT_NEAR(result.value(), 1.41, 0.01);
}

/***********************************************************
 * CHECK DetermineVelocityReasonRightOvertakingProhibition *
 ***********************************************************/

struct DataFor_DetermineVelocityReasonRightOvertakingProhibition
{
  bool isDoesOuterLaneOvertakingProhibitionApply;
  bool isRightHandTraffic;
  units::velocity::meters_per_second_t expectedResult;
};

class MentalModel_DetermineVelocityReasonRightOvertakingProhibition : public ::testing::Test,
                                                                      public ::testing::WithParamInterface<DataFor_DetermineVelocityReasonRightOvertakingProhibition>
{
};
TEST_P(MentalModel_DetermineVelocityReasonRightOvertakingProhibition, Check_DetermineVelocityReasonRightOvertakingProhibition)
{
  MentalModelTester TEST_HELPER;
  DataFor_DetermineVelocityReasonRightOvertakingProhibition data = GetParam();

  TrafficRulesScm trafficRules;
  trafficRules.common.rightHandTraffic.value = data.isRightHandTraffic;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetTrafficRulesScm()).WillByDefault(Return(&trafficRules));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, DoesOuterLaneOvertakingProhibitionApply(_, false)).WillByDefault(Return(data.isDoesOuterLaneOvertakingProhibitionApply));

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAbsoluteVelocity).WillByDefault(Return(10.0_mps));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeVehicle};
  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.DetermineVelocityReasonRightOvertakingProhibition();

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_DetermineVelocityReasonRightOvertakingProhibition,
    testing::Values(
        DataFor_DetermineVelocityReasonRightOvertakingProhibition{
            .isDoesOuterLaneOvertakingProhibitionApply = false,
            .isRightHandTraffic = true,
            .expectedResult = ScmDefinitions::INF_VELOCITY},
        DataFor_DetermineVelocityReasonRightOvertakingProhibition{
            .isDoesOuterLaneOvertakingProhibitionApply = true,
            .isRightHandTraffic = true,
            .expectedResult = 10.0_mps},
        DataFor_DetermineVelocityReasonRightOvertakingProhibition{
            .isDoesOuterLaneOvertakingProhibitionApply = true,
            .isRightHandTraffic = false,
            .expectedResult = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)}));

/***************************************************
 * CHECK DetermineVelocityReasonPassingSlowPlatoon *
 ***************************************************/

TEST(MentalModel_DetermineVelocityReasonPassingSlowPlatoon, Check_DetermineVelocityReasonPassingSlowPlatoon)
{
  MentalModelTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalCalculations, VReasonPassingSlowPlatoon()).WillByDefault(Return(10.0_mps));

  ASSERT_EQ(TEST_HELPER.mentalModel.DetermineVelocityReasonPassingSlowPlatoon(), 10.0_mps);
}

/*************************
 * CHECK GetTrafficSigns *
 *************************/

TEST(MentalModel_GetTrafficSigns, Check_GetTrafficSigns)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  scm::CommonTrafficSign::Entity trafficSignLeft;
  trafficSignLeft.relativeDistance = 100_m;
  trafficSignLeft.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
  std::vector<scm::CommonTrafficSign::Entity> trafficSignsLeft{trafficSignLeft};

  scm::CommonTrafficSign::Entity trafficSignEgo;
  trafficSignEgo.relativeDistance = 150_m;
  trafficSignEgo.type = scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd;
  std::vector<scm::CommonTrafficSign::Entity> trafficSignsEgo{trafficSignEgo};

  scm::CommonTrafficSign::Entity trafficSignRight;
  trafficSignRight.relativeDistance = 200_m;
  trafficSignRight.type = scm::CommonTrafficSign::Type::EnvironmentalZoneBegin;
  std::vector<scm::CommonTrafficSign::Entity> trafficSignsRight{trafficSignRight};

  TrafficRuleInformationScmExtended trafficRuleInfo;
  trafficRuleInfo.Left().trafficSigns = trafficSignsLeft;
  trafficRuleInfo.Close().trafficSigns = trafficSignsEgo;
  trafficRuleInfo.Right().trafficSigns = trafficSignsRight;

  ON_CALL(*fakeInfrastructureCollection, GetTrafficRuleInformation()).WillByDefault(Return(&trafficRuleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  auto result = TEST_HELPER.mentalModel.GetTrafficSigns();

  ASSERT_EQ(result.at(0).at(0).relativeDistance, 100_m);
  ASSERT_EQ(result.at(0).at(0).type, scm::CommonTrafficSign::Type::MaximumSpeedLimit);

  ASSERT_EQ(result.at(1).at(0).relativeDistance, 150_m);
  ASSERT_EQ(result.at(1).at(0).type, scm::CommonTrafficSign::Type::AnnounceLeftLaneEnd);

  ASSERT_EQ(result.at(2).at(0).relativeDistance, 200_m);
  ASSERT_EQ(result.at(2).at(0).type, scm::CommonTrafficSign::Type::EnvironmentalZoneBegin);
}

/****************************************
 * CHECK GetTrafficJamVelocityThreshold *
 ****************************************/

TEST(MentalModel_GetTrafficJamVelocityThreshold, Check_GetTrafficJamVelocityThreshold)
{
  MentalModelTester TEST_HELPER;
  ASSERT_EQ(TEST_HELPER.mentalModel.GetTrafficJamVelocityThreshold(), ScmDefinitions::TRAFFIC_JAM_VELOCITY_THRESHOLD);
}

/********************************************
 * CHECK GetFormRescueLaneVelocityThreshold *
 ********************************************/

TEST(MentalModel_GetFormRescueLaneVelocityThreshold, Check_GetFormRescueLaneVelocityThreshold)
{
  MentalModelTester TEST_HELPER;
  ASSERT_EQ(TEST_HELPER.mentalModel.GetFormRescueLaneVelocityThreshold(), ScmDefinitions::FORM_RESCUELANE_VELOCITY_THRESHOLD);
}

/*******************************
 * CHECK GetAdjustmentBaseTime *
 *******************************/

TEST(MentalModel_GetAdjustmentBaseTime, Check_GetAdjustmentBaseTime)
{
  MentalModelTester TEST_HELPER;
  TEST_HELPER.mentalModel.SET_ADJUSTMENT_BASE_TIME(0.5_s);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetAdjustmentBaseTime(), 0.5_s);
}

/*************************************
 * CHECK CalculateAdjustmentBaseTime *
 *************************************/

struct DataFor_CalculateAdjustmentBaseTime
{
  units::acceleration::meters_per_second_squared_t longitudinalAccelerationWish;
  units::time::second_t expectedResultAdjustmentBaseTime;
  units::acceleration::meters_per_second_squared_t expectedResultAccelerationWishLongitudinalLast;
};

class MentalModel_CalculateAdjustmentBaseTime : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_CalculateAdjustmentBaseTime>
{
};
TEST_P(MentalModel_CalculateAdjustmentBaseTime, Check_CalculateAdjustmentBaseTime)
{
  MentalModelTester TEST_HELPER;
  DataFor_CalculateAdjustmentBaseTime data = GetParam();
  NiceMock<FakeActionImplementation> fakeActionImplementation;

  ON_CALL(fakeActionImplementation, GetLongitudinalAccelerationWish()).WillByDefault(Return(data.longitudinalAccelerationWish));

  DriverParameters driverParameter;
  driverParameter.adjustmentBaseTimeMinimum = 2.0_s;
  driverParameter.adjustmentBaseTimeMean = 3.0_s;
  driverParameter.adjustmentBaseTimeStandardDeviation = 4.0_s;
  driverParameter.adjustmentBaseTimeMaximum = 5.0_s;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(3.5));

  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);
  TEST_HELPER.mentalModel.CalculateAdjustmentBaseTime(&fakeActionImplementation);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetAdjustmentBaseTime(), data.expectedResultAdjustmentBaseTime);
  ASSERT_EQ(TEST_HELPER.mentalModel.GET_ACCELERATION_WISH_LONGITUDINAL_LAST(), data.expectedResultAccelerationWishLongitudinalLast);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CalculateAdjustmentBaseTime,
    testing::Values(
        DataFor_CalculateAdjustmentBaseTime{
            .longitudinalAccelerationWish = -99.0_mps_sq,
            .expectedResultAdjustmentBaseTime = 0.1_s,
            .expectedResultAccelerationWishLongitudinalLast = -99.0_mps_sq},
        DataFor_CalculateAdjustmentBaseTime{
            .longitudinalAccelerationWish = 10.0_mps_sq,
            .expectedResultAdjustmentBaseTime = 3.6_s,
            .expectedResultAccelerationWishLongitudinalLast = 10.0_mps_sq}));

/***********************************
 * CHECK AdvanceAdjustmentBaseTime *
 ***********************************/

TEST(MentalModel_AdvanceAdjustmentBaseTime, Check_AdvanceAdjustmentBaseTime)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetAdjustmentBaseTime(1000_ms);
  TEST_HELPER.mentalModel.SET_CYCLE_TIME(100_ms);
  TEST_HELPER.mentalModel.AdvanceAdjustmentBaseTime();
  ASSERT_EQ(TEST_HELPER.mentalModel.GetAdjustmentBaseTime(), 0.9_s);
}

/*************************************
 * CHECK IsAdjustmentBaseTimeExpired *
 *************************************/

struct DataFor_IsAdjustmentBaseTimeExpired
{
  units::time::second_t adjustmentBaseTime;
  bool expectedResult;
};

class MentalModel_IsAdjustmentBaseTimeExpired : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_IsAdjustmentBaseTimeExpired>
{
};
TEST_P(MentalModel_IsAdjustmentBaseTimeExpired, Check_IsAdjustmentBaseTimeExpired)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsAdjustmentBaseTimeExpired data = GetParam();

  TEST_HELPER.mentalModel.SetAdjustmentBaseTime(data.adjustmentBaseTime);

  ASSERT_EQ(TEST_HELPER.mentalModel.IsAdjustmentBaseTimeExpired(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsAdjustmentBaseTimeExpired,
    testing::Values(
        DataFor_IsAdjustmentBaseTimeExpired{
            .adjustmentBaseTime = -10.0_s,
            .expectedResult = true},
        DataFor_IsAdjustmentBaseTimeExpired{
            .adjustmentBaseTime = 5.0_s,
            .expectedResult = false}));

/***********************************************
 * CHECK GetHighCognitiveLaneChangeProbability *
 ***********************************************/

TEST(MentalModel_GetHighCognitiveLaneChangeProbability, Check_GetHighCognitiveLaneChangeProbability)
{
  MentalModelTester TEST_HELPER;
  auto fakeHighCognitive = std::make_unique<FakeHighCognitive>();

  ON_CALL(*fakeHighCognitive, AnticipateLaneChangeProbability(_)).WillByDefault(Return(10.0));

  TEST_HELPER.mentalModel.SET_HIGH_COGNITIVE(std::move(fakeHighCognitive));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetHighCognitiveLaneChangeProbability(), 10.0);
}

/****************************************************
 * CHECK GetClosestRelativeLaneIdForJunctionIngoing *
 ****************************************************/

struct DataFor_GetClosestRelativeLaneIdForJunctionIngoing
{
  std::vector<int> relativeLaneIdForJunctionIngoing;
  int expectedResult;
};

class MentalModel_GetClosestRelativeLaneIdForJunctionIngoing : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_GetClosestRelativeLaneIdForJunctionIngoing>
{
};
TEST_P(MentalModel_GetClosestRelativeLaneIdForJunctionIngoing, Check_GetClosestRelativeLaneIdForJunctionIngoing)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetClosestRelativeLaneIdForJunctionIngoing data = GetParam();
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM info;
  info.relativeLaneIdForJunctionIngoing = data.relativeLaneIdForJunctionIngoing;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&info));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetClosestRelativeLaneIdForJunctionIngoing(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetClosestRelativeLaneIdForJunctionIngoing,
    testing::Values(
        DataFor_GetClosestRelativeLaneIdForJunctionIngoing{
            .relativeLaneIdForJunctionIngoing = {0},
            .expectedResult = 0},
        DataFor_GetClosestRelativeLaneIdForJunctionIngoing{
            .relativeLaneIdForJunctionIngoing = {1, 2},
            .expectedResult = 1},
        DataFor_GetClosestRelativeLaneIdForJunctionIngoing{
            .relativeLaneIdForJunctionIngoing = {},
            .expectedResult = -999}));

/*********************************
 * CHECK GetLaneChangePrevention *
 *********************************/

struct DataFor_GetLaneChangePrevention
{
  bool preventLaneChangeDueToSpawn;
  bool preventLaneChangeDueExternalLateralControl;
  bool expectedResult;
};

class MentalModel_GetLaneChangePrevention : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_GetLaneChangePrevention>
{
};

TEST_P(MentalModel_GetLaneChangePrevention, Check_GetLaneChangePrevention)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLaneChangePrevention data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.preventLaneChangeDueToSpawn = data.preventLaneChangeDueToSpawn;
  ownVehicleInfo.preventLaneChangeDueExternalLateralControl = data.preventLaneChangeDueExternalLateralControl;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLaneChangePrevention(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLaneChangePrevention,
    testing::Values(
        DataFor_GetLaneChangePrevention{
            .preventLaneChangeDueToSpawn = false,
            .preventLaneChangeDueExternalLateralControl = false,
            .expectedResult = false},
        DataFor_GetLaneChangePrevention{
            .preventLaneChangeDueToSpawn = true,
            .preventLaneChangeDueExternalLateralControl = false,
            .expectedResult = true},
        DataFor_GetLaneChangePrevention{
            .preventLaneChangeDueToSpawn = false,
            .preventLaneChangeDueExternalLateralControl = true,
            .expectedResult = true},
        DataFor_GetLaneChangePrevention{
            .preventLaneChangeDueToSpawn = true,
            .preventLaneChangeDueExternalLateralControl = true,
            .expectedResult = true}));

/****************************************
 * CHECK GetLaneChangePreventionAtSpawn *
 ****************************************/

TEST(MentalModel_GetLaneChangePreventionAtSpawn, Check_GetLaneChangePreventionAtSpawn)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.preventLaneChangeDueToSpawn = true;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  EXPECT_TRUE(TEST_HELPER.mentalModel.GetLaneChangePreventionAtSpawn());
}

/************************************************
 * CHECK GetLaneChangePreventionExternalControl *
 ************************************************/

TEST(MentalModel_GetLaneChangePreventionExternalControl, Check_GetLaneChangePreventionExternalControl)
{
  MentalModelTester TEST_HELPER;

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.preventLaneChangeDueExternalLateralControl = true;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  EXPECT_TRUE(TEST_HELPER.mentalModel.GetLaneChangePreventionExternalControl());
}

/**************************************
 * CHECK ResetLaneChangeWidthTraveled *
 **************************************/

TEST(MentalModel_ResetLaneChangeWidthTraveled, Check_ResetLaneChangeWidthTraveled)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetLaneChangeWidthTraveled(10.0_m);
  TEST_HELPER.mentalModel.ResetLaneChangeWidthTraveled();
  ASSERT_EQ(TEST_HELPER.mentalModel.GetLaneChangeWidthTraveled(), 0.0_m);
}

/************************
 * CHECK GetObstruction *
 ************************/

struct DataFor_GetObstruction
{
  AreaOfInterest aoi;
  ObstructionScm expectedResult;
};

class MentalModel_GetObstruction : public ::testing::Test,
                                   public ::testing::WithParamInterface<DataFor_GetObstruction>
{
};

TEST_P(MentalModel_GetObstruction, Check_GetObstruction)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetObstruction data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ObstructionScm obstruction{1.0_m,
                             1.0_m,
                             0.0_m};
  ON_CALL(fakeVehicle, GetLateralObstruction()).WillByDefault(Return(obstruction));

  AoiVehicleMapping aoiMapping;
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetObstruction(data.aoi, -1);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetObstruction,
    testing::Values(
        DataFor_GetObstruction{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = {1.0_m, 1.0_m, 0.0_m}},
        DataFor_GetObstruction{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = {0.0_m, 0.0_m, 0.0_m}}));

/**************************************************
 * CHECK GetCausingVehicleOfSituationFrontCluster *
 **************************************************/

struct DataFor_GetCausingVehicleOfSituationFrontCluster
{
  Situation situation;
  AreaOfInterest expectedResult;
};

class MentalModel_GetCausingVehicleOfSituationFrontCluster : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_GetCausingVehicleOfSituationFrontCluster>
{
};

TEST_P(MentalModel_GetCausingVehicleOfSituationFrontCluster, Check_GetCausingVehicleOfSituationFrontCluster)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetCausingVehicleOfSituationFrontCluster data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationFrontCluster{{Situation::FOLLOWING_DRIVING, AreaOfInterest::LEFT_FRONT},
                                                                            {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationFrontCluster = causingVehicleOfSituationFrontCluster;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCausingVehicleOfSituationFrontCluster(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetCausingVehicleOfSituationFrontCluster,
    testing::Values(
        DataFor_GetCausingVehicleOfSituationFrontCluster{
            .situation = Situation::FOLLOWING_DRIVING,
            .expectedResult = AreaOfInterest::LEFT_FRONT},
        DataFor_GetCausingVehicleOfSituationFrontCluster{
            .situation = Situation::FREE_DRIVING,
            .expectedResult = AreaOfInterest::NumberOfAreaOfInterests}));

/**************************************************
 * CHECK SetCausingVehicleOfSituationFrontCluster *
 **************************************************/

struct DataFor_SetCausingVehicleOfSituationFrontCluster
{
  Situation situation;
  AreaOfInterest aoi;
};

class MentalModel_SetCausingVehicleOfSituationFrontCluster : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_SetCausingVehicleOfSituationFrontCluster>
{
};

TEST_P(MentalModel_SetCausingVehicleOfSituationFrontCluster, Check_SetCausingVehicleOfSituationFrontCluster)
{
  MentalModelTester TEST_HELPER;
  DataFor_SetCausingVehicleOfSituationFrontCluster data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationFrontCluster{{Situation::FOLLOWING_DRIVING, AreaOfInterest::LEFT_FRONT},
                                                                            {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationFrontCluster = causingVehicleOfSituationFrontCluster;
  ON_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).WillByDefault(Return(&ownVehicleInfo));

  if (data.situation == Situation::FREE_DRIVING)
  {
    EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(1);
    TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
    EXPECT_THROW(TEST_HELPER.mentalModel.SetCausingVehicleOfSituationFrontCluster(data.situation, data.aoi), std::runtime_error);
  }
  else
  {
    EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(2);
    TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
    TEST_HELPER.mentalModel.SetCausingVehicleOfSituationFrontCluster(data.situation, data.aoi);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_SetCausingVehicleOfSituationFrontCluster,
    testing::Values(
        DataFor_SetCausingVehicleOfSituationFrontCluster{
            .situation = Situation::FOLLOWING_DRIVING,
            .aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_SetCausingVehicleOfSituationFrontCluster{
            .situation = Situation::FREE_DRIVING,
            .aoi = AreaOfInterest::NumberOfAreaOfInterests}));

/**************************************************
 * CHECK GetCausingVehicleOfSituationSideCluster *
 **************************************************/

struct DataFor_GetCausingVehicleOfSituationSideCluster
{
  Situation situation;
  AreaOfInterest expectedResult;
};

class MentalModel_GetCausingVehicleOfSituationSideCluster : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_GetCausingVehicleOfSituationSideCluster>
{
};

TEST_P(MentalModel_GetCausingVehicleOfSituationSideCluster, Check_GetCausingVehicleOfSituationSideCluster)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetCausingVehicleOfSituationSideCluster data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationSideCluster{{Situation::LANE_CHANGER_FROM_RIGHT, AreaOfInterest::LEFT_FRONT},
                                                                           {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationSideCluster = causingVehicleOfSituationSideCluster;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetCausingVehicleOfSituationSideCluster(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetCausingVehicleOfSituationSideCluster,
    testing::Values(
        DataFor_GetCausingVehicleOfSituationSideCluster{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .expectedResult = AreaOfInterest::LEFT_FRONT},
        DataFor_GetCausingVehicleOfSituationSideCluster{
            .situation = Situation::FREE_DRIVING,
            .expectedResult = AreaOfInterest::NumberOfAreaOfInterests}));

/*************************************************
 * CHECK SetCausingVehicleOfSituationSideCluster *
 *************************************************/

struct DataFor_SetCausingVehicleOfSituationSideCluster
{
  Situation situation;
  AreaOfInterest aoi;
};

class MentalModel_SetCausingVehicleOfSituationSideCluster : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_SetCausingVehicleOfSituationSideCluster>
{
};

TEST_P(MentalModel_SetCausingVehicleOfSituationSideCluster, Check_SetCausingVehicleOfSituationSideCluster)
{
  MentalModelTester TEST_HELPER;
  DataFor_SetCausingVehicleOfSituationSideCluster data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationSideCluster{{Situation::LANE_CHANGER_FROM_RIGHT, AreaOfInterest::LEFT_FRONT},
                                                                           {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationFrontCluster = causingVehicleOfSituationSideCluster;
  ON_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).WillByDefault(Return(&ownVehicleInfo));

  if (data.situation == Situation::FREE_DRIVING)
  {
    EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(1);
    TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
    EXPECT_THROW(TEST_HELPER.mentalModel.SetCausingVehicleOfSituationSideCluster(data.situation, data.aoi), std::runtime_error);
  }
  else
  {
    EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(2);
    TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
    TEST_HELPER.mentalModel.SetCausingVehicleOfSituationSideCluster(data.situation, data.aoi);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_SetCausingVehicleOfSituationSideCluster,
    testing::Values(
        DataFor_SetCausingVehicleOfSituationSideCluster{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_SetCausingVehicleOfSituationSideCluster{
            .situation = Situation::FREE_DRIVING,
            .aoi = AreaOfInterest::NumberOfAreaOfInterests}));

/**************************
 * CHECK GetSituationRisk *
 **************************/

TEST(MentalModel_GetSituationRisk, Check_GetSituationRisk)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, Risk> situationRisk{
      {Situation::FOLLOWING_DRIVING, Risk::LOW}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situationRisk = situationRisk;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetSituationRisk(Situation::FOLLOWING_DRIVING), Risk::LOW);
}

/*************************
 * CHECK IsFrontSituation *
 **************************/

struct DataFor_IsFrontSituation
{
  Situation situation;
  bool expectedResult;
};

class MentalModel_IsFrontSituation : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_IsFrontSituation>
{
};

TEST_P(MentalModel_IsFrontSituation, Check_IsFrontSituation)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsFrontSituation data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsFrontSituation(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsFrontSituation,
    testing::Values(
        DataFor_IsFrontSituation{
            .situation = Situation::FOLLOWING_DRIVING,
            .expectedResult = true},
        DataFor_IsFrontSituation{
            .situation = Situation::FREE_DRIVING,
            .expectedResult = false}));

/********************************
 * CHECK IsSituationAnticipated *
 ********************************/

TEST(MentalModel_IsSituationAnticipated, Check_IsSituationAnticipated)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, bool> situationAnticipated{
      {Situation::FOLLOWING_DRIVING, true}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situationAnticipated = situationAnticipated;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_TRUE(TEST_HELPER.mentalModel.IsSituationAnticipated(Situation::FOLLOWING_DRIVING));
}

/**********************************
 * CHECK GetProjectedVehicleWidth *
 **********************************/

TEST(MentalModel_GetProjectedVehicleWidth, Check_GetProjectedVehicleWidth)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.projectedDimensions.widthProjected = 5.0_m;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetProjectedVehicleWidth(), 5.0_m);
}

/**************************************
 * CHECK GetProjectedVehicleWidthLeft *
 **************************************/

TEST(MentalModel_GetProjectedVehicleWidthLeft, Check_GetProjectedVehicleWidthLeft)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.projectedDimensions.widthProjectedLeft = 2.0_m;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetProjectedVehicleWidthLeft(), 2.0_m);
}

/***************************************
 * CHECK GetProjectedVehicleWidthRight *
 ***************************************/

TEST(MentalModel_GetProjectedVehicleWidthRight, Check_GetProjectedVehicleWidthRight)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.projectedDimensions.widthProjectedRight = 3.0_m;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetProjectedVehicleWidthRight(), 3.0_m);
}

/********************************
 * CHECK GetProjectedDimensions *
 ********************************/

struct DataFor_GetProjectedDimensions
{
  AreaOfInterest aoi;
  ProjectedSpacialDimensions expectedResult;
};

class MentalModel_GetProjectedDimensions : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetProjectedDimensions>
{
};

TEST_P(MentalModel_GetProjectedDimensions, Check_GetProjectedDimensions)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetProjectedDimensions data = GetParam();

  ProjectedSpacialDimensions spacialDimensions{};
  spacialDimensions.heightProjected = 3.5_m;
  spacialDimensions.widthProjected = 5.5_m;
  spacialDimensions.widthProjectedLeft = 2.75_m;
  spacialDimensions.widthProjectedRight = 2.75_m;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetProjectedDimensions()).WillByDefault(Return(spacialDimensions));

  AoiVehicleMapping aoiMapping;
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  auto result = TEST_HELPER.mentalModel.GetProjectedDimensions(data.aoi);

  ASSERT_EQ(result.heightProjected, data.expectedResult.heightProjected);
  ASSERT_EQ(result.widthProjected, data.expectedResult.widthProjected);
  ASSERT_EQ(result.widthProjectedLeft, data.expectedResult.widthProjectedLeft);
  ASSERT_EQ(result.widthProjectedRight, data.expectedResult.widthProjectedRight);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetProjectedDimensions,
    testing::Values(
        DataFor_GetProjectedDimensions{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = {3.5_m, 5.5_m, 2.75_m, 2.75_m}},
        DataFor_GetProjectedDimensions{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = {units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE), units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE), units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE), units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)}}));

/********************************************
 * CHECK GetComfortLongitudinalAcceleration *
 ********************************************/

TEST(MentalModel_GetComfortLongitudinalAcceleration, Check_GetComfortLongitudinalAcceleration)
{
  MentalModelTester TEST_HELPER;

  DriverParameters parameter;
  parameter.comfortLongitudinalAcceleration = 3.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(parameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetComfortLongitudinalAcceleration(), 3.0_mps_sq);
}

/********************************************
 * CHECK GetComfortLongitudinalDeceleration *
 ********************************************/

TEST(MentalModel_GetComfortLongitudinalDeceleration, Check_GetComfortLongitudinalDeceleration)
{
  MentalModelTester TEST_HELPER;

  DriverParameters parameter;
  parameter.comfortLongitudinalDeceleration = 1.5_mps_sq;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(parameter));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetComfortLongitudinalDeceleration(), 1.5_mps_sq);
}

/****************************************
 * CHECK GetDecelerationLimitForMerging *
 ****************************************/

TEST(MentalModel_GetDecelerationLimitForMerging, Check_GetDecelerationLimitForMerging)
{
  MentalModelTester TEST_HELPER;

  auto fakeMerging = std::make_unique<NiceMock<FakeMerging>>();
  ON_CALL(*fakeMerging, GetDecelerationLimitForMerging()).WillByDefault(Return(1.5_mps_sq));

  TEST_HELPER.mentalModel.SET_MERGING(std::move(fakeMerging));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetDecelerationLimitForMerging(), 1.5_mps_sq);
}

/****************************************
 * CHECK GetAccelerationLimitForMerging *
 ****************************************/

TEST(MentalModel_GetAccelerationLimitForMerging, Check_GetAccelerationLimitForMerging)
{
  MentalModelTester TEST_HELPER;

  auto fakeMerging = std::make_unique<NiceMock<FakeMerging>>();
  ON_CALL(*fakeMerging, GetAccelerationLimitForMerging()).WillByDefault(Return(2.5_mps_sq));

  TEST_HELPER.mentalModel.SET_MERGING(std::move(fakeMerging));
  ASSERT_EQ(TEST_HELPER.mentalModel.GetAccelerationLimitForMerging(), 2.5_mps_sq);
}

/***************************************
 * CHECK IsObstacleCloserThanEndOfLane *
 ***************************************/

struct DataFor_IsObstacleCloserThanEndOfLane
{
  Situation situation;
  units::time::second_t ttc;
  units::time::second_t thw;
  bool expectedResult;
};

class MentalModel_IsObstacleCloserThanEndOfLane : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_IsObstacleCloserThanEndOfLane>
{
};

TEST_P(MentalModel_IsObstacleCloserThanEndOfLane, Check_IsObstacleCloserThanEndOfLane)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsObstacleCloserThanEndOfLane data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();
  AoiVehicleMapping aoiMapping;

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationFrontCluster{{Situation::FOLLOWING_DRIVING, AreaOfInterest::LEFT_FRONT},
                                                                            {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationFrontCluster = causingVehicleOfSituationFrontCluster;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(100_m));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.ttc));
  ON_CALL(fakeVehicle, GetThw()).WillByDefault(Return(data.thw));
  aoiMapping[AreaOfInterest::RIGHT_FRONT] = {&fakeVehicle};

  GeometryInformationSCM info;
  info.Close().distanceToEndOfLane = 50_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&info));

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(fakeInfrastructureCollection));

  auto distanceToEndOfMergeConsideringObstacle = 100_m;
  auto result = TEST_HELPER.mentalModel.IsObstacleCloserThanEndOfLane(0.5, 100_mps, 100_m, distanceToEndOfMergeConsideringObstacle);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsObstacleCloserThanEndOfLane,
    testing::Values(
        DataFor_IsObstacleCloserThanEndOfLane{
            .situation = Situation::FOLLOWING_DRIVING,
            .ttc = 10_s,
            .thw = 20_s,
            .expectedResult = false},
        DataFor_IsObstacleCloserThanEndOfLane{
            .situation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .ttc = 10_s,
            .thw = 20_s,
            .expectedResult = true},
        DataFor_IsObstacleCloserThanEndOfLane{
            .situation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .ttc = -10_s,
            .thw = 20_s,
            .expectedResult = false},
        DataFor_IsObstacleCloserThanEndOfLane{
            .situation = Situation::OBSTACLE_ON_CURRENT_LANE,
            .ttc = 10_s,
            .thw = 9_s,
            .expectedResult = false}));

/*********************************************
 * CHECK CheckIsLaneChangeProhibitionIgnored *
 *********************************************/

struct DataFor_CheckIsLaneChangeProhibitionIgnored
{
  scm::LaneMarking::Type left;
  scm::LaneMarking::Type right;
  scm::LaneMarking::Type leftLast;
  scm::LaneMarking::Type rightLast;
  int times;
};

class MentalModel_CheckIsLaneChangeProhibitionIgnored : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_CheckIsLaneChangeProhibitionIgnored>
{
};

TEST_P(MentalModel_CheckIsLaneChangeProhibitionIgnored, Check_CheckIsLaneChangeProhibitionIgnored)
{
  MentalModelTester TEST_HELPER;
  DataFor_CheckIsLaneChangeProhibitionIgnored data = GetParam();

  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  scm::LaneMarking::Entity laneMarkingLeft;
  laneMarkingLeft.type = data.left;

  scm::LaneMarking::Entity laneMarkingRight;
  laneMarkingRight.type = data.right;

  LaneInformationTrafficRulesScmExtended laneInfoTrafficRules;

  if (data.left == scm::LaneMarking::Type::Curb)
  {
    laneInfoTrafficRules.laneMarkingsLeft = {};
    laneInfoTrafficRules.laneMarkingsRight = {};
  }
  else
  {
    laneInfoTrafficRules.laneMarkingsLeft = {laneMarkingLeft};
    laneInfoTrafficRules.laneMarkingsRight = {laneMarkingRight};
  }
  ON_CALL(*fakeInfrastructureCollection, GetLaneInformationTrafficRules(_)).WillByDefault(ReturnRef(laneInfoTrafficRules));
  ON_CALL(*fakeInfrastructureCollection, GetLaneMarkingTypeLeftLast()).WillByDefault(Return(data.leftLast));
  ON_CALL(*fakeInfrastructureCollection, GetLaneMarkingTypeRightLast()).WillByDefault(Return(data.rightLast));

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(0.5));

  DriverParameters driverParameters;
  driverParameters.useShoulderInTrafficJamQuota = 0.5;
  driverParameters.useShoulderWhenPeerPressuredQuota = 0.5;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  OwnVehicleInformationScmExtended ownVehicleInfo;
  EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(data.times).WillOnce(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(fakeInfrastructureCollection));
  TEST_HELPER.mentalModel.CheckIsLaneChangeProhibitionIgnored();
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_CheckIsLaneChangeProhibitionIgnored,
    testing::Values(
        DataFor_CheckIsLaneChangeProhibitionIgnored{
            .left = scm::LaneMarking::Type::Broken,
            .right = scm::LaneMarking::Type::Solid,
            .leftLast = scm::LaneMarking::Type::Solid,
            .rightLast = scm::LaneMarking::Type::Broken,
            .times = 1},
        DataFor_CheckIsLaneChangeProhibitionIgnored{
            .left = scm::LaneMarking::Type::Grass,
            .right = scm::LaneMarking::Type::Grass,
            .leftLast = scm::LaneMarking::Type::Solid,
            .rightLast = scm::LaneMarking::Type::Broken,
            .times = 1},
        DataFor_CheckIsLaneChangeProhibitionIgnored{
            .left = scm::LaneMarking::Type::Curb,
            .right = scm::LaneMarking::Type::Curb,
            .leftLast = scm::LaneMarking::Type::Solid,
            .rightLast = scm::LaneMarking::Type::Broken,
            .times = 0},
        DataFor_CheckIsLaneChangeProhibitionIgnored{
            .left = scm::LaneMarking::Type::Broken,
            .right = scm::LaneMarking::Type::Solid,
            .leftLast = scm::LaneMarking::Type::Broken,
            .rightLast = scm::LaneMarking::Type::Solid,
            .times = 0}));

/*******************************
 * CHECK DrawShoulderLaneUsage *
 *******************************/

TEST(MentalModel_DrawShoulderLaneUsage, Check_DrawShoulderLaneUsage)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(0.5));

  DriverParameters driverParameters;
  driverParameters.useShoulderInTrafficJamQuota = 0.5;
  driverParameters.useShoulderInTrafficJamToExitQuota = 0.5;
  driverParameters.useShoulderWhenPeerPressuredQuota = 0.5;
  ON_CALL(TEST_HELPER.fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  TEST_HELPER.mentalModel.SET_STOCHASTICS(&fakeStochastics);

  OwnVehicleInformationScmExtended ownVehicleInfo;
  EXPECT_CALL(*fakeMicroscopicData, UpdateOwnVehicleData()).Times(1).WillOnce(Return(&ownVehicleInfo));
  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  TEST_HELPER.mentalModel.DrawShoulderLaneUsage();
}

/*******************************************
 * CHECK AdjustMinDistanceDueToCooperation *
 *******************************************/

TEST(MentalModel_AdjustMinDistanceDueToCooperation, Check_AdjustMinDistanceDueToCooperation)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_COOPERATIVE_BEHAVIOR(true);

  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(true));

  ASSERT_EQ(TEST_HELPER.mentalModel.AdjustMinDistanceDueToCooperation(100_m), 80.0_m);
}

/*************************
 * CHECK IsSideSituation *
 *************************/

struct DataFor_IsSideSituation
{
  Situation situation;
  bool expectedResult;
};

class MentalModel_IsSideSituation : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_IsSideSituation>
{
};

TEST_P(MentalModel_IsSideSituation, Check_IsSideSituation)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsSideSituation data = GetParam();
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;

  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.IsSideSituation(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsSideSituation,
    testing::Values(
        DataFor_IsSideSituation{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .expectedResult = true},
        DataFor_IsSideSituation{
            .situation = Situation::FREE_DRIVING,
            .expectedResult = false}));

/************************************
 * CHECK GetLongitudinalObstruction *
 ************************************/

struct DataFor_GetLongitudinalObstruction
{
  AreaOfInterest aoi;
  ObstructionLongitudinal expectedResult;
};

class MentalModel_GetLongitudinalObstruction : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_GetLongitudinalObstruction>
{
};

TEST_P(MentalModel_GetLongitudinalObstruction, Check_GetLongitudinalObstruction)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetLongitudinalObstruction data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  ObstructionLongitudinal obstruction;
  obstruction.front = 0_m;
  obstruction.rear = 0_m;
  obstruction.mainLaneLocator = 2.0_m;
  ON_CALL(fakeVehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstruction));
  aoiMapping[AreaOfInterest::LEFT_FRONT] = {&fakeVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);
  auto result = TEST_HELPER.mentalModel.GetLongitudinalObstruction(data.aoi, -1);

  ASSERT_EQ(result.mainLaneLocator, data.expectedResult.mainLaneLocator);
  ASSERT_EQ(result.front, data.expectedResult.front);
  ASSERT_EQ(result.rear, data.expectedResult.rear);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetLongitudinalObstruction,
    testing::Values(
        DataFor_GetLongitudinalObstruction{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = {0.0_m, 0.0_m, 2.0_m}},
        DataFor_GetLongitudinalObstruction{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = {0.0_m, 0.0_m, 0.0_m}}));

/**************************
 * CHECK GetObjectToEvade *
 **************************/

struct DataFor_GetObjectToEvade
{
  Situation situation;
  AreaOfInterest expectedResult;
};

class MentalModel_GetObjectToEvade : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GetObjectToEvade>
{
};

TEST_P(MentalModel_GetObjectToEvade, Check_GetObjectToEvade)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetObjectToEvade data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  std::map<Situation, AreaOfInterest> causingVehicleOfSituationFrontCluster{{Situation::FOLLOWING_DRIVING, AreaOfInterest::LEFT_FRONT},
                                                                            {Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::RIGHT_FRONT}};

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.situation = data.situation;
  ownVehicleInfo.causingVehicleOfSituationFrontCluster = causingVehicleOfSituationFrontCluster;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetObjectToEvade(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetObjectToEvade,
    testing::Values(
        DataFor_GetObjectToEvade{
            .situation = Situation::FOLLOWING_DRIVING,
            .expectedResult = AreaOfInterest::LEFT_FRONT},
        DataFor_GetObjectToEvade{
            .situation = Situation::FREE_DRIVING,
            .expectedResult = AreaOfInterest::EGO_FRONT}));

/*******************************
 * CHECK GetVisibilityDistance *
 *******************************/

TEST(MentalModel_GetVisibilityDistance, Check_GetVisibilityDistance)
{
  MentalModelTester TEST_HELPER;
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.visibilityDistance = 300_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(nullptr, std::move(fakeInfrastructureCollection));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetVisibilityDistance(), 300_m);
}

/********************************
 * CHECK GetHorizontalGazeAngle *
 ********************************/

struct DataFor_GetHorizontalGazeAngle
{
  AreaOfInterest fovea;
  units::angle::radian_t expectedResult;
};

class MentalModel_GetHorizontalGazeAngle : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GetHorizontalGazeAngle>
{
};

TEST_P(MentalModel_GetHorizontalGazeAngle, Check_GetHorizontalGazeAngle)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetHorizontalGazeAngle data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.fovea = data.fovea;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.SetHorizontalGazeAngle(2.0_rad);

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);
  ASSERT_EQ(TEST_HELPER.mentalModel.GetHorizontalGazeAngle(), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetHorizontalGazeAngle,
    testing::Values(
        DataFor_GetHorizontalGazeAngle{
            .fovea = AreaOfInterest::EGO_REAR,
            .expectedResult = units::angle::radian_t(-M_PI)},
        DataFor_GetHorizontalGazeAngle{
            .fovea = AreaOfInterest::LEFT_FRONT,
            .expectedResult = 2.0_rad}));

/************************
 * CHECK IsPointVisible *
 ************************/

struct DataFor_IsPointVisible
{
  units::length::meter_t s;
  units::length::meter_t t;
  bool expectedResult;
};

class MentalModel_IsPointVisible : public ::testing::Test,
                                   public ::testing::WithParamInterface<DataFor_IsPointVisible>
{
};

TEST_P(MentalModel_IsPointVisible, Check_IsPointVisible)
{
  MentalModelTester TEST_HELPER;
  DataFor_IsPointVisible data = GetParam();

  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  auto fakeInfrastructureCollection = std::make_unique<FakeInfrastructureCharacteristics>();

  GeometryInformationSCM geoInfo;
  geoInfo.visibilityDistance = 300_m;
  ON_CALL(*fakeInfrastructureCollection, GetGeometryInformation()).WillByDefault(Return(&geoInfo));

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.fovea = AreaOfInterest::LEFT_FRONT;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.SetHorizontalGazeAngle(2.0_rad);

  Scm::Point point{data.s, data.t};

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(fakeInfrastructureCollection));
  ASSERT_EQ(TEST_HELPER.mentalModel.IsPointVisible(point), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_IsPointVisible,
    testing::Values(
        DataFor_IsPointVisible{
            .s = 2.0_m,
            .t = 3.0_m,
            .expectedResult = true},
        DataFor_IsPointVisible{
            .s = 310.0_m,
            .t = 3.0_m,
            .expectedResult = false}));

/********************************
 * CHECK GetPossibleRegulateIds *
 ********************************/

TEST(MentalModel_GetPossibleRegulateIds, Check_GetPossibleRegulateIds)
{
  MentalModelTester TEST_HELPER;

  std::vector<int> possibleRegulateIds{1, 2, 3};
  TEST_HELPER.mentalModel.SetPossibleRegulateIds(possibleRegulateIds);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetPossibleRegulateIds(), possibleRegulateIds);
}

/*****************************
 * CHECK GetLeadingVehicleId *
 *****************************/

TEST(MentalModel_GetLeadingVehicleId, Check_GetLeadingVehicleId)
{
  MentalModelTester TEST_HELPER;
  TEST_HELPER.mentalModel.SetLeadingVehicleId(2);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetLeadingVehicleId(), 2);
}

/***************************************
 * CHECK GetActiveMesoscopicSituations *
 ***************************************/

TEST(MentalModel_GetActiveMesoscopicSituations, Check_GetActiveMesoscopicSituations)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SET_MESOSCOPIC_SITUATION_ACTIVE(MesoscopicSituation::CURRENT_LANE_BLOCKED);
  TEST_HELPER.mentalModel.SET_MESOSCOPIC_SITUATION_ACTIVE(MesoscopicSituation::QUEUED_TRAFFIC);

  auto result = TEST_HELPER.mentalModel.GetActiveMesoscopicSituations();

  std::vector<MesoscopicSituation> expectedResult{MesoscopicSituation::CURRENT_LANE_BLOCKED, MesoscopicSituation::QUEUED_TRAFFIC};
  ASSERT_EQ(result, expectedResult);
}

/*********************
 * CHECK GetZipMerge *
 *********************/

TEST(MentalModel_GetZipMerge, Check_GetZipMerge)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetZipMerge(true);
  EXPECT_TRUE(TEST_HELPER.mentalModel.GetZipMerge());
}

/*********************************
 * CHECK GetRemainInRealignState *
 *********************************/

TEST(MentalModel_GetRemainInRealignState, Check_GetRemainInRealignState)
{
  MentalModelTester TEST_HELPER;

  TEST_HELPER.mentalModel.SetRemainInRealignState(true);
  EXPECT_TRUE(TEST_HELPER.mentalModel.GetRemainInRealignState());
}

/***********************************
 * CHECK GetLastLateralActionState *
 ***********************************/

TEST(MentalModel_GetLastLateralActionState, Check_GetLastLateralActionState)
{
  MentalModelTester TEST_HELPER;

  LateralAction action;
  action.direction = LateralAction::Direction::LEFT;
  action.state = LateralAction::State::LANE_KEEPING;

  TEST_HELPER.mentalModel.SetLastLateralAction(action);
  auto result = TEST_HELPER.mentalModel.GetLastLateralActionState();

  ASSERT_EQ(result.state, LateralAction::State::LANE_KEEPING);
  ASSERT_EQ(result.direction, LateralAction::Direction::LEFT);
}

/*****************************
 * CHECK GetThresholdLooming *
 *****************************/

struct DataFor_GetThresholdLooming
{
  AreaOfInterest aoi;
  double expectedResult;
};

class MentalModel_GetThresholdLooming : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_GetThresholdLooming>
{
};

TEST_P(MentalModel_GetThresholdLooming, Check_GetThresholdLooming)
{
  MentalModelTester TEST_HELPER;
  DataFor_GetThresholdLooming data = GetParam();

  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetTtcThresholdLooming()).WillByDefault(Return(5.5));
  aoiMapping[AreaOfInterest::EGO_FRONT] = {&fakeVehicle};

  TEST_HELPER.mentalModel.SetAoiMapping(aoiMapping);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetThresholdLooming(data.aoi, -1), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_GetThresholdLooming,
    testing::Values(
        DataFor_GetThresholdLooming{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 5.5},
        DataFor_GetThresholdLooming{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = ScmDefinitions::DEFAULT_VALUE}));

/*******************************************
 * CHECK GetVisibilityFactorForHighwayExit *
 *******************************************/

TEST(MentalModel_GetVisibilityFactorForHighwayExit, Check_GetVisibilityFactorForHighwayExit)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.distributionForHighwayExit = 0.3;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  ASSERT_EQ(TEST_HELPER.mentalModel.GetVisibilityFactorForHighwayExit(), 0.3);
}

/***************************************
 * CHECK GetOuterKeepingQuotaFulfilled *
 ***************************************/

TEST(MentalModel_GetOuterKeepingQuotaFulfilled, Check_GetOuterKeepingQuotaFulfilled)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.isOuterKeeping = true;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  EXPECT_TRUE(TEST_HELPER.mentalModel.GetOuterKeepingQuotaFulfilled());
}

/*****************************************
 * CHECK GetEgoLaneKeepingQuotaFulfilled *
 *****************************************/

TEST(MentalModel_GetEgoLaneKeepingQuotaFulfilled, Check_GetEgoLaneKeepingQuotaFulfilled)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.isEgoLaneKeeping = true;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), nullptr);

  EXPECT_TRUE(TEST_HELPER.mentalModel.GetEgoLaneKeepingQuotaFulfilled());
}

/****************************
 * CHECK IsRightHandTraffic *
 ****************************/

TEST(MentalModel_IsRightHandTraffic, Check_IsRightHandTraffic)
{
  MentalModelTester TEST_HELPER;

  TrafficRulesScm trafficRules;
  trafficRules.common.rightHandTraffic.value = true;

  ON_CALL(TEST_HELPER.fakeScmDependencies, GetTrafficRulesScm()).WillByDefault(Return(&trafficRules));

  EXPECT_TRUE(TEST_HELPER.mentalModel.IsRightHandTraffic());
}

/****************************************
 * CHECK GetReductionFactorUnderUrgency *
 ****************************************/

TEST(MentalModel_GetReductionFactorUnderUrgency, Check_GetReductionFactorUnderUrgency)
{
  MentalModelTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(0.6));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetReductionFactorUnderUrgency(), 0.4);
}

/****************************************
 * CHECK GetCombinedAcousticOpticSignal *
 ****************************************/

TEST(MentalModel_GetCombinedAcousticOpticSignal, Check_GetCombinedAcousticOpticSignal)
{
  MentalModelTester TEST_HELPER;

  AdasHmiSignal signal;
  signal.activity = true;
  signal.spotOfPresentation = "headUpDisplay";
  signal.sendingSystem = "HAF";

  TEST_HELPER.mentalModel.SetCombinedAcousticOpticSignal(signal);
  auto result = TEST_HELPER.mentalModel.GetCombinedAcousticOpticSignal();

  EXPECT_TRUE(result.activity);
  ASSERT_EQ(result.spotOfPresentation, "headUpDisplay");
  ASSERT_EQ(result.sendingSystem, "HAF");
}

/****************************************
 * CHECK GetPlannedLaneChangeDimensions *
 ****************************************/

TEST(MentalModel_GetPlannedLaneChangeDimensions, Check_GetPlannedLaneChangeDimensions)
{
  MentalModelTester TEST_HELPER;

  TrajectoryPlanning::TrajectoryDimensions dimensions;
  dimensions.length = 100_m;
  dimensions.width = 2.5_m;

  TEST_HELPER.mentalModel.SetPlannedLaneChangeDimensions(dimensions);
  auto result = TEST_HELPER.mentalModel.GetPlannedLaneChangeDimensions();

  ASSERT_EQ(result->length, 100_m);
  ASSERT_EQ(result->width, 2.5_m);
}

/****************************************
 * CHECK GetUrgencyReductionFactor *
 ****************************************/

TEST(MentalModel_GetUrgencyReductionFactor, Check_GetUrgencyReductionFactor)
{
  MentalModelTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(0.6));

  ASSERT_EQ(TEST_HELPER.mentalModel.GetUrgencyReductionFactor(), 0.4);
}

/***************************************
 * CHECK HasEgoLateralDistanceReached  *
 ***************************************/

struct DataFor_HasEgoLateralDistanceReached
{
  units::length::meter_t desiredLateralDisplacement;
  units::length::meter_t lateralPositionSet;
  bool expectedResult;
};

class MentalModel_HasEgoLateralDistanceReached : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_HasEgoLateralDistanceReached>
{
};

TEST_P(MentalModel_HasEgoLateralDistanceReached, Check_HasEgoLateralDistanceReached)
{
  MentalModelTester TEST_HELPER;
  DataFor_HasEgoLateralDistanceReached data = GetParam();

  ASSERT_EQ(TEST_HELPER.mentalModel.HasEgoLateralDistanceReached(data.desiredLateralDisplacement, data.lateralPositionSet), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_HasEgoLateralDistanceReached,
    testing::Values(
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = 3.75_m,
            .lateralPositionSet = 1.5_m,
            .expectedResult = false},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = 3.75_m,
            .lateralPositionSet = 3.75_m,
            .expectedResult = true},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = 3.75_m,
            .lateralPositionSet = 3.6_m,
            .expectedResult = true},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = -3.75_m,
            .lateralPositionSet = -1.5_m,
            .expectedResult = false},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = -3.75_m,
            .lateralPositionSet = -3.75_m,
            .expectedResult = true},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = -3.75_m,
            .lateralPositionSet = -3.6_m,
            .expectedResult = true},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = 2.00_m,
            .lateralPositionSet = 1.8_m,
            .expectedResult = false},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = 2.00_m,
            .lateralPositionSet = 1.9_m,
            .expectedResult = true},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = -2.00_m,
            .lateralPositionSet = -1.8_m,
            .expectedResult = false},
        DataFor_HasEgoLateralDistanceReached{
            .desiredLateralDisplacement = -2.00_m,
            .lateralPositionSet = -1.9_m,
            .expectedResult = true}));

/****************************************
 * CHECK DetermineDistanceTraveledLeft  *
 ****************************************/

struct DataFor_DetermineDistanceTraveledLeft
{
  bool isNewEgoLane;
  units::length::meter_t laneChangeWidth;
  units::length::meter_t lateralPositionFrontAxle;
  units::length::meter_t expectedResult;
};

class MentalModel_DetermineDistanceTraveledLeft : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_DetermineDistanceTraveledLeft>
{
};

TEST_P(MentalModel_DetermineDistanceTraveledLeft, Check_DetermineDistanceTraveledLeft)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  DataFor_DetermineDistanceTraveledLeft data = GetParam();

  TEST_HELPER.mentalModel.SET_IS_NEW_EGOLANE(data.isNewEgoLane);

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralPositionFrontAxle = data.lateralPositionFrontAxle;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(nullptr));
  ASSERT_EQ(TEST_HELPER.mentalModel.DetermineDistanceTraveledLeft(data.laneChangeWidth), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_DetermineDistanceTraveledLeft,
    testing::Values(
        DataFor_DetermineDistanceTraveledLeft{
            .isNewEgoLane = false,
            .laneChangeWidth = 3.75_m,
            .lateralPositionFrontAxle = 1.75_m,
            .expectedResult = 1.75_m},
        DataFor_DetermineDistanceTraveledLeft{
            .isNewEgoLane = true,
            .laneChangeWidth = 3.75_m,
            .lateralPositionFrontAxle = -0.75_m,
            .expectedResult = 3_m},
        DataFor_DetermineDistanceTraveledLeft{
            .isNewEgoLane = true,
            .laneChangeWidth = 2.75_m,
            .lateralPositionFrontAxle = -0.75_m,
            .expectedResult = 2_m},
        DataFor_DetermineDistanceTraveledLeft{
            .isNewEgoLane = false,
            .laneChangeWidth = 2.75_m,
            .lateralPositionFrontAxle = 0.50_m,
            .expectedResult = 0.5_m}));

/*****************************************
 * CHECK DetermineDistanceTraveledRight  *
 *****************************************/

struct DataFor_DetermineDistanceTraveledRight
{
  bool isNewEgoLane;
  units::length::meter_t laneChangeWidth;
  units::length::meter_t lateralPositionFrontAxle;
  units::length::meter_t expectedResult;
};

class MentalModel_DetermineDistanceTraveledRight : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_DetermineDistanceTraveledRight>
{
};

TEST_P(MentalModel_DetermineDistanceTraveledRight, Check_DetermineDistanceTraveledRight)
{
  MentalModelTester TEST_HELPER;
  auto fakeMicroscopicData = std::make_unique<FakeMicroscopicCharacteristics>();
  DataFor_DetermineDistanceTraveledRight data = GetParam();

  TEST_HELPER.mentalModel.SET_IS_NEW_EGOLANE(data.isNewEgoLane);

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.lateralPositionFrontAxle = data.lateralPositionFrontAxle;
  ON_CALL(*fakeMicroscopicData, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  TEST_HELPER.mentalModel.UPDATE_MENTALMODEL(std::move(fakeMicroscopicData), std::move(nullptr));
  ASSERT_EQ(TEST_HELPER.mentalModel.DetermineDistanceTraveledRight(data.laneChangeWidth), data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MentalModel_DetermineDistanceTraveledRight,
    testing::Values(
        DataFor_DetermineDistanceTraveledRight{
            .isNewEgoLane = false,
            .laneChangeWidth = -3.75_m,
            .lateralPositionFrontAxle = -0.75_m,
            .expectedResult = -0.75_m},
        DataFor_DetermineDistanceTraveledRight{
            .isNewEgoLane = true,
            .laneChangeWidth = -3.75_m,
            .lateralPositionFrontAxle = -0.75_m,
            .expectedResult = -3_m},
        DataFor_DetermineDistanceTraveledRight{
            .isNewEgoLane = true,
            .laneChangeWidth = -2.75_m,
            .lateralPositionFrontAxle = -0.75_m,
            .expectedResult = -2_m},
        DataFor_DetermineDistanceTraveledRight{
            .isNewEgoLane = false,
            .laneChangeWidth = -2.75_m,
            .lateralPositionFrontAxle = -0.5_m,
            .expectedResult = -0.5_m}));
// using MentalModel::;