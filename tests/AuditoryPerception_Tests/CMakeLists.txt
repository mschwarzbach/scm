################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME AuditoryPerception_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN
  LINKOSI

  SOURCES
  UnitTestsAuditoryPerception.cpp
  ${DRIVER_DIR}/src/AuditoryPerception.cpp

  HEADERS
  ${DRIVER_DIR}/src/AuditoryPerception.h
  ../Fakes/FakeInfrastructureCharacteristics.h

  INCDIRS
  ${DRIVER_DIR}/src
  ${DRIVER_DIR}/src/Signals

  LIBRARIES
  Stochastics::Stochastics
)