/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "AuditoryPerception.h"
#include "module/driver/src/SurroundingVehicles/SurroundingVehicleDefinitions.h"
#include "module/driver/src/SurroundingVehicles/SurroundingVehicles.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct AuditoryPerceptionTester
{
  class AuditoryPerceptionUnderTest : AuditoryPerception
  {
  public:
    template <typename... Args>
    AuditoryPerceptionUnderTest(Args&&... args)
        : AuditoryPerception{std::forward<Args>(args)...} {};

    using AuditoryPerception::GetAcousticStimulusDirection;
    using AuditoryPerception::IsAcousticStimulusDetected;
    using AuditoryPerception::IsAcousticStimulusProcessed;
    using AuditoryPerception::UpdateStimulusData;

    void SET_DURATION_OWN_VEHICLE_SIGNAL_ACTIVITY(units::time::millisecond_t duration)
    {
      _durationOwnVehicleSignalActivity = duration;
    }
  };

  template <typename T>
  T& WITH_AUDITORY_PROCESSING_TIME(T& fakeStochastics)
  {
    ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(100));
    return fakeStochastics;
  }

  AuditoryPerceptionTester()
      : fakeStochastics{},
        auditoryPerception(100_ms, &WITH_AUDITORY_PROCESSING_TIME(fakeStochastics))
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  TrafficRulesScm trafficRulesScm;

  AuditoryPerceptionUnderTest auditoryPerception;
};

/************************************
 * CHECK IsAcousticStimulusDetected *
 ************************************/

struct DataFor_CheckIsAcousticStimulusDetected
{
  bool isHmiSignal;
  double expectedResult;
};

class AuditoryPerception_IsAcousticStimulusDetected : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckIsAcousticStimulusDetected>
{
};

TEST_P(AuditoryPerception_IsAcousticStimulusDetected, AuditoryPerception_CheckIsAcousticStimulusDetected)
{
  AuditoryPerceptionTester TEST_HELPER;
  DataFor_CheckIsAcousticStimulusDetected data = GetParam();

  AdasHmiSignal signal;
  signal.activity = data.isHmiSignal;
  signal.sendingSystem = "HAF";
  signal.spotOfPresentation = "instrumentCluster";

  std::vector<AdasHmiSignal> acousticAdasSignals{signal};

  auto result = TEST_HELPER.auditoryPerception.IsAcousticStimulusDetected(acousticAdasSignals);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    AuditoryPerception_IsAcousticStimulusDetected,
    ::testing::Values(
        DataFor_CheckIsAcousticStimulusDetected{
            .isHmiSignal = true,
            .expectedResult = true},
        DataFor_CheckIsAcousticStimulusDetected{
            .isHmiSignal = false,
            .expectedResult = false}));

/****************************
 * CHECK UpdateStimulusData *
 ****************************/

struct DataFor_CheckUpdateStimulusData
{
  bool isHmiSignal;
  units::time::millisecond_t auditoryPerception;
  std::string spotOfPresentation;
  AcousticStimulusDirection expectedDirection;
  bool expectedStimulusProcessed;
};

class AuditoryPerception_UpdateStimulusData : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckUpdateStimulusData>
{
};

TEST_P(AuditoryPerception_UpdateStimulusData, AuditoryPerception_CheckUpdateStimulusData)
{
  AuditoryPerceptionTester TEST_HELPER;
  DataFor_CheckUpdateStimulusData data = GetParam();

  AdasHmiSignal signal;
  signal.activity = data.isHmiSignal;
  signal.sendingSystem = "HAF";
  signal.spotOfPresentation = data.spotOfPresentation;

  std::vector<AdasHmiSignal> acousticAdasSignals{signal};

  TEST_HELPER.auditoryPerception.SET_DURATION_OWN_VEHICLE_SIGNAL_ACTIVITY(data.auditoryPerception);

  TEST_HELPER.auditoryPerception.UpdateStimulusData(acousticAdasSignals);
  auto resultDirection = TEST_HELPER.auditoryPerception.GetAcousticStimulusDirection();
  auto resultStimulusProcessed = TEST_HELPER.auditoryPerception.IsAcousticStimulusProcessed();

  ASSERT_EQ(resultDirection, data.expectedDirection);
  ASSERT_EQ(resultStimulusProcessed, data.expectedStimulusProcessed);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    AuditoryPerception_UpdateStimulusData,
    ::testing::Values(
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = 100_ms,
            .spotOfPresentation = "front",
            .expectedDirection = AcousticStimulusDirection::FRONT,
            .expectedStimulusProcessed = true},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = 100_ms,
            .spotOfPresentation = "sideRight",
            .expectedDirection = AcousticStimulusDirection::RIGHT,
            .expectedStimulusProcessed = true},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = 100_ms,
            .spotOfPresentation = "rear",
            .expectedDirection = AcousticStimulusDirection::REAR,
            .expectedStimulusProcessed = true},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = 100_ms,
            .spotOfPresentation = "sideLeft",
            .expectedDirection = AcousticStimulusDirection::LEFT,
            .expectedStimulusProcessed = true},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = 100_ms,
            .spotOfPresentation = "instumentCluster",
            .expectedDirection = AcousticStimulusDirection::DEFAULT,
            .expectedStimulusProcessed = true},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = true,
            .auditoryPerception = -1_ms,
            .spotOfPresentation = "front",
            .expectedDirection = AcousticStimulusDirection::DEFAULT,
            .expectedStimulusProcessed = false},
        DataFor_CheckUpdateStimulusData{
            .isHmiSignal = false,
            .auditoryPerception = -1_ms,
            .spotOfPresentation = "front",
            .expectedDirection = AcousticStimulusDirection::DEFAULT,
            .expectedStimulusProcessed = false}));
