/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/dynamicsModel/src/ScmDynamicDriving.h"

/*****************
 * CHECK Trigger *
 *****************/
struct DataFor_Trigger
{
  std::string gearRatio;
  double accPedalPos;
  double brakePedalPos;
  double expectedResultAcceleration;
  double expectedResultVelocityX;
  double expectedResultVelocityY;
  double expectedResultPositionX;
  double expectedResultPositionY;
  double expectedResultYaw;
  double expectedResultYawRate;
  double expectedResultYawAcceleration;
  units::angle::radian_t expectedResultRoll;
  double expectedResultCentripetalAcceleration;
  double expectedResultTravelDistance;
};

class DynamicsDriving_Trigger : public ::testing::Test,
                                public ::testing::WithParamInterface<DataFor_Trigger>
{
};

TEST_P(DynamicsDriving_Trigger, Check_Trigger)
{
  DataFor_Trigger data = GetParam();

  osi3::GroundTruth groundTruth;
  osi3::MovingObject* vehicle = groundTruth.add_moving_object();
  vehicle->mutable_base()->mutable_orientation()->set_yaw(0.5);
  vehicle->mutable_base()->mutable_velocity()->set_x(20.0);
  vehicle->mutable_base()->mutable_position()->set_x(0);
  vehicle->mutable_base()->mutable_position()->set_y(0);
  vehicle->mutable_id()->set_value(1);
  vehicle->mutable_base()->mutable_dimension()->set_length(5.0);
  vehicle->mutable_base()->mutable_dimension()->set_width(2.20);

  groundTruth.mutable_host_vehicle_id()->set_value(1);
  osiql::Query query{groundTruth};
  scm::ScmDynamicDriving dynamicModul(100_ms, &query);

  std::map<std::string, std::string> properties{};
  properties["NumberOfGears"] = "1";
  properties["FrictionCoefficient"] = "1";
  properties["AxleRatio"] = "1";
  properties["GearRatio1"] = data.gearRatio;
  properties["MaximumEngineTorque"] = "400";
  properties["MaximumEngineSpeed"] = "8000";
  properties["MinimumEngineSpeed"] = "600";
  properties["AirDragCoefficient"] = "1";
  properties["FrontSurface"] = "2";
  properties["SteeringRatio"] = "1";

  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  vehicleParameters.properties = properties;
  vehicleParameters.rear_axle.wheel_diameter = 20_m;
  vehicleParameters.mass = 1500_kg;
  vehicleParameters.front_axle.max_steering = 2_rad;
  vehicleParameters.front_axle.bb_center_to_axle_center.x = 1;
  vehicleParameters.rear_axle.bb_center_to_axle_center.x = 1;

  scm::signal::DynamicsModelInput input;
  input.cycleTime = 100_ms;
  input.vehicleParameters = vehicleParameters;
  input.longitudinalControllerOutput.accPedalPos = data.accPedalPos;
  input.longitudinalControllerOutput.brakePedalPos = data.brakePedalPos;
  input.longitudinalControllerOutput.gear = 1;

  auto result = dynamicModul.Trigger(input);

  EXPECT_NEAR(result.dynamicsInformation.acceleration.value(), data.expectedResultAcceleration, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.velocityX.value(), data.expectedResultVelocityX, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.velocityY.value(), data.expectedResultVelocityY, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.positionX.value(), data.expectedResultPositionX, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.positionY.value(), data.expectedResultPositionY, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.yaw.value(), data.expectedResultYaw, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.yawRate.value(), data.expectedResultYawRate, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.yawAcceleration.value(), data.expectedResultYawAcceleration, 0.01);
  ASSERT_EQ(result.dynamicsInformation.roll, data.expectedResultRoll);
  EXPECT_NEAR(result.dynamicsInformation.centripetalAcceleration.value(), data.expectedResultCentripetalAcceleration, 0.01);
  EXPECT_NEAR(result.dynamicsInformation.travelDistance.value(), data.expectedResultTravelDistance, 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    DynamicsDriving_Trigger,
    testing::Values(
        DataFor_Trigger{
            .gearRatio = "1",
            .accPedalPos = 0.5,
            .brakePedalPos = 0.0,
            .expectedResultAcceleration = -0.44,
            .expectedResultVelocityX = 17.51,
            .expectedResultVelocityY = 9.56,
            .expectedResultPositionX = 1.75,
            .expectedResultPositionY = 0.95,
            .expectedResultYaw = 2.07,
            .expectedResultYawRate = 15.7,
            .expectedResultYawAcceleration = 157.07,
            .expectedResultRoll = 0_rad,
            .expectedResultCentripetalAcceleration = 313.46,
            .expectedResultTravelDistance = 1.99},
        DataFor_Trigger{
            .gearRatio = "400",
            .accPedalPos = 0.5,
            .brakePedalPos = 0.0,
            .expectedResultAcceleration = 4.04,
            .expectedResultVelocityX = 17.90,
            .expectedResultVelocityY = 9.78,
            .expectedResultPositionX = 1.79,
            .expectedResultPositionY = 0.97,
            .expectedResultYaw = 2.07,
            .expectedResultYawRate = 15.7,
            .expectedResultYawAcceleration = 157.07,
            .expectedResultRoll = 0_rad,
            .expectedResultCentripetalAcceleration = 320.50,
            .expectedResultTravelDistance = 2.04},
        DataFor_Trigger{
            .gearRatio = "1",
            .accPedalPos = 0.0,
            .brakePedalPos = 0.5,
            .expectedResultAcceleration = -5.35,
            .expectedResultVelocityX = 17.08,
            .expectedResultVelocityY = 9.33,
            .expectedResultPositionX = 1.70,
            .expectedResultPositionY = 0.93,
            .expectedResultYaw = 2.07,
            .expectedResultYawRate = 15.7,
            .expectedResultYawAcceleration = 157.07,
            .expectedResultRoll = 0_rad,
            .expectedResultCentripetalAcceleration = 305.74,
            .expectedResultTravelDistance = 1.94}));