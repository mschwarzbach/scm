/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <initializer_list>

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "module/driver/src/InformationAcquisition.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::An;
using ::testing::Return;
using ::testing::ReturnRef;

class TestSurroundingVehicles : public SurroundingVehicles
{
public:
  /**! 
     * @brief FOR TESTING ONLY: Makes sure, that objects are not
     *        deleted, when the class is released (@see SetObjects)
     */
  ~TestSurroundingVehicles() override
  {
    for (auto& ptr : _surroundingVehicles)
    {
      ptr.release();
    }
  }

  bool CANNOT_CALL_SET_SURROUNDING_VEHICLES_TWICE()
  {
    return _surroundingVehicles.empty();
  }
  /** 
     * @brief FOR TESTING ONLY: Allows to use stack objects
     *        within the normally memory managed objects vector.
     *
     * Pointers to objects are stored as unique pointers but 
     * not released within the destructor (@see ~TestSurroundingVehicles)
     */
  void SetSurroundingVehicles(std::initializer_list<SurroundingVehicleInterface*> surroundingVehicles);
};