/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQuery.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/ScmCommons.h"

using ::testing::NiceMock;
using ::testing::Return;

/***************************************
 * CHECK GetLateralObstructionDynamics *
 ****************************************/

struct DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t lateralVelocityEgo;
  units::velocity::meters_per_second_t lateralVelocityVehicle;
  ObstructionScm obstruction;
  bool isOverlapping;
  ObstructionDynamics expectedObstructionDynamics;
};

class SurroundingVehicleQuery_GetLateralObstructionDynamics : public ::testing::Test,
                                                              public ::testing::WithParamInterface<DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics>
{
};

TEST_P(SurroundingVehicleQuery_GetLateralObstructionDynamics, LateralObstructionDynamics)
{
  const auto data = GetParam();

  auto combinedObstructionData{data.obstruction};
  combinedObstructionData.isOverlapping = data.isOverlapping;

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, GetLateralVelocity()).WillByDefault(Return(data.lateralVelocityVehicle));
  ON_CALL(vehicle, GetLateralObstruction()).WillByDefault(Return(combinedObstructionData));
  ON_CALL(vehicle, LateralObstructionOverlapping()).WillByDefault(Return(data.isOverlapping));

  OwnVehicleInformationScmExtended egoInfo;
  egoInfo.lateralVelocity = data.lateralVelocityEgo;

  SurroundingVehicleQuery query(vehicle, egoInfo);

  auto result = query.GetLateralObstructionDynamics();

  if (units::math::isinf(data.expectedObstructionDynamics.timeToEnter))
  {
    ASSERT_TRUE(units::math::isinf(result.timeToEnter));
  }
  else
  {
    ASSERT_EQ(result.timeToEnter, data.expectedObstructionDynamics.timeToEnter);
  }

  if (units::math::isinf(data.expectedObstructionDynamics.timeToLeave))
  {
    ASSERT_TRUE(units::math::isinf(result.timeToLeave));
  }
  else
  {
    ASSERT_EQ(result.timeToLeave, data.expectedObstructionDynamics.timeToLeave);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicleQuery_GetLateralObstructionDynamics,
    testing::Values(
        // moving right, no ego movement
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .obstruction = ObstructionScm(2_m, -1_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {1.0_s, 2.0_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .obstruction = ObstructionScm(1.5_m, 0.5_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0_s, 1.5_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .obstruction = ObstructionScm(1.0_m, 1.0_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0._s, 1.0_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .obstruction = ObstructionScm(0.5_m, 1.5_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0._s, 0.5_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .obstruction = ObstructionScm(-1.0_m, 2.0_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {-2.0_s, -1.0_s}},
        // movingLeft, no ego movement
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .obstruction = ObstructionScm(2_m, -1_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {-2.0_s, -1.0_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .obstruction = ObstructionScm(1.5_m, 0.5_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0_s, 0.5_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .obstruction = ObstructionScm(1.0_m, 1.0_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0._s, 1.0_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .obstruction = ObstructionScm(0.5_m, 1.5_m, 0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0._s, 1.5_s}},
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .obstruction = ObstructionScm(-1.0_m, 2.0_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {1.0_s, 2.0_s}},
        // moving right, with ego movement
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.5_mps,
            .lateralVelocityVehicle = -0.5_mps,
            .obstruction = ObstructionScm(2_m, -1_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {1.0_s, 2.0_s}},
        // moving left, with ego movement
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = -0.5_mps,
            .lateralVelocityVehicle = 0.5_mps,
            .obstruction = ObstructionScm(2_m, -1_m, 0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {-2.0_s, -1.0_s}},
        // no lateral movement, not overlapping
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 0.0_mps,
            .obstruction = ObstructionScm(0.0_m, 0.0_m, 0.0_m),
            .isOverlapping = false,
            .expectedObstructionDynamics = {ScmDefinitions::INF_TIME, 0.0_s}},
        // no lateral movement, already overlapping
        DataFor_SurroundingVehicleQuery_GetLateralObstructionDynamics{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 0.0_mps,
            .obstruction = ObstructionScm(0.0_m, 0.0_m, 0.0_m),
            .isOverlapping = true,
            .expectedObstructionDynamics = {0.0_s, ScmDefinitions::INF_TIME}}));

/*********************************
 * CHECK GetLateralVelocityDelta *
 **********************************/

struct DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta
{
  units::velocity::meters_per_second_t lateralVelocityEgo;
  units::velocity::meters_per_second_t lateralVelocityVehicle;
  units::velocity::meters_per_second_t expected_lateralVelocityDelta;
};

class SurroundingVehicleQuery_GetLateralVelocityDelta : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta>
{
};

TEST_P(SurroundingVehicleQuery_GetLateralVelocityDelta, CalculateDeltaVLateral)
{
  const auto data = GetParam();

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, GetLateralVelocity()).WillByDefault(Return(data.lateralVelocityVehicle));

  OwnVehicleInformationScmExtended egoInfo;
  egoInfo.lateralVelocity = data.lateralVelocityEgo;

  const SurroundingVehicleQuery query{vehicle, egoInfo};
  const auto result{query.GetLateralVelocityDelta()};

  ASSERT_EQ(data.expected_lateralVelocityDelta, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicleQuery_GetLateralVelocityDelta,
    testing::Values(
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .expected_lateralVelocityDelta = 1_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = 0.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .expected_lateralVelocityDelta = -1_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = 1.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .expected_lateralVelocityDelta = 2_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = 1.0_mps,
            .lateralVelocityVehicle = 0.0_mps,
            .expected_lateralVelocityDelta = 1_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = 1.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .expected_lateralVelocityDelta = 0_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = -1.0_mps,
            .lateralVelocityVehicle = -1.0_mps,
            .expected_lateralVelocityDelta = 0_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = -1.0_mps,
            .lateralVelocityVehicle = 0.0_mps,
            .expected_lateralVelocityDelta = -1_mps},
        DataFor_SurroundingVehicleQuery_GetLateralVelocityDelta{
            .lateralVelocityEgo = -1.0_mps,
            .lateralVelocityVehicle = 1.0_mps,
            .expected_lateralVelocityDelta = -2_mps}));

/***********************************************************
 * CHECK CannotClearLongitudinalObstructionBeforeCollision *
 ************************************************************/

struct DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision
{
  units::acceleration::meters_per_second_squared_t longitudinalAccelerationDelta;
  units::velocity::meters_per_second_t longitudinalVelocityDelta;
  units::time::second_t timeToLateralImpact;
  units::length::meter_t distanceEgoFrontObjectRear;
  units::length::meter_t distanceEgoRearObjectFront;
  bool expected_hasCollisionCourse;
};

class SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision : public ::testing::Test,
                                                                                  public ::testing::WithParamInterface<DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision>
{
public:
  void SET_LONGITUDINAL_VELOCITIES(units::velocity::meters_per_second_t velocityDelta, units::acceleration::meters_per_second_squared_t accelerationDelta)
  {
    const auto egoVelocity{1._mps};
    const auto vehicleVelocity{egoVelocity - velocityDelta};  // delta = ego - vehicle -> vehicle = ego - delta
    const auto egoAcceleration{1._mps_sq};
    const auto vehicleAcceleration{egoAcceleration - accelerationDelta};

    ON_CALL(vehicle, GetLongitudinalVelocity()).WillByDefault(Return(vehicleVelocity));
    ON_CALL(vehicle, GetLongitudinalAcceleration()).WillByDefault(Return(vehicleAcceleration));

    egoInfo.longitudinalVelocity = egoVelocity;
    egoInfo.acceleration = egoAcceleration;
  }

  void SET_LATERAL_OBSTRUCTION_DATA(units::time::second_t timeToLateralImpact)
  {
    // Derive requried obstruction distance for given timeToEnterObstruction
    const units::velocity::meters_per_second_t deltaVLateral = 1._mps;
    const units::velocity::meters_per_second_t egoLateralVelocity = 1._mps;
    const auto vehicleLateralVelocity{egoLateralVelocity - deltaVLateral};
    const auto obstructionLeft = DONT_CARE;
    const auto obstructionRight = -timeToLateralImpact * deltaVLateral;
    const ObstructionScm lateralObstruction(obstructionLeft, obstructionRight, DONT_CARE);
    ON_CALL(vehicle, GetLateralVelocity()).WillByDefault(Return(vehicleLateralVelocity));
    ON_CALL(vehicle, GetLateralObstruction()).WillByDefault(Return(lateralObstruction));

    egoInfo.lateralVelocity = egoLateralVelocity;
  }

  void SET_LONGITUDINAL_OBSTRUCTION_DATA(units::length::meter_t distanceEgoFrontObjectRear, units::length::meter_t distanceEgoRearObjectFront)
  {
    ObstructionLongitudinal obstruction(distanceEgoRearObjectFront, -distanceEgoFrontObjectRear, DONT_CARE);
    ON_CALL(vehicle, GetLongitudinalObstruction()).WillByDefault(Return(obstruction));
  }

  NiceMock<FakeSurroundingVehicle> vehicle;
  OwnVehicleInformationScmExtended egoInfo;
  static constexpr auto DONT_CARE{0._m};
};

TEST_P(SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision, CollisionCourseIfLongitudinalObstructionIsNotClearedUntilImpact)
{
  const auto data = GetParam();
  SET_LONGITUDINAL_VELOCITIES(data.longitudinalVelocityDelta, data.longitudinalAccelerationDelta);
  SET_LATERAL_OBSTRUCTION_DATA(data.timeToLateralImpact);
  SET_LONGITUDINAL_OBSTRUCTION_DATA(data.distanceEgoFrontObjectRear, data.distanceEgoRearObjectFront);

  ON_CALL(vehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));

  const SurroundingVehicleQuery query{vehicle, egoInfo};
  const auto result{query.CannotClearLongitudinalObstructionBeforeCollision()};

  ASSERT_EQ(data.expected_hasCollisionCourse, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision,
    testing::Values(
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // No Overlap, behind vehicle
            .longitudinalAccelerationDelta = 0_mps_sq,
            .longitudinalVelocityDelta = 1_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = 0.1_m,
            .distanceEgoRearObjectFront = 1_m,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // No Overlap, behind vehicle, cant clear in time
            .longitudinalAccelerationDelta = 1_mps_sq,
            .longitudinalVelocityDelta = 0_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = 0.1_m,
            .distanceEgoRearObjectFront = 1_m,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // Already has Overlap, behind vehicle, cant clear in time
            .longitudinalAccelerationDelta = 0.1_mps_sq,
            .longitudinalVelocityDelta = 0_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = -0.1_m,
            .distanceEgoRearObjectFront = 1_m,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // Already has Overlap, behind vehicle, can clear in time
            .longitudinalAccelerationDelta = 0_mps_sq,
            .longitudinalVelocityDelta = 1_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = -0.1_m,
            .distanceEgoRearObjectFront = 1_m,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // No Overlap, in front of vehicle, can clear in time
            .longitudinalAccelerationDelta = 1_mps_sq,
            .longitudinalVelocityDelta = 1_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = -1_m,
            .distanceEgoRearObjectFront = -0.1_m,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // No Overlap, in front of vehicle, cannot clear in time
            .longitudinalAccelerationDelta = -1_mps_sq,
            .longitudinalVelocityDelta = 0_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = -1_m,
            .distanceEgoRearObjectFront = -0.1_m,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_CannotClearLongitudinalObstructionBeforeCollision{
            // Overlap, in front of vehicle, cannot clear in time
            .longitudinalAccelerationDelta = 0_mps_sq,
            .longitudinalVelocityDelta = -1_mps,
            .timeToLateralImpact = 1_s,
            .distanceEgoFrontObjectRear = -1_m,
            .distanceEgoRearObjectFront = 0.1_m,
            .expected_hasCollisionCourse = true}));

/***************************
 * CHECK HasCollisionCourse *
 ****************************/

struct DataFor_SurroundingVehicleQuery_HasCollisionCourse
{
  std::string testcase;
  bool toTheSideOfEgo;
  units::time::second_t ttc;
  double tauDot;
  ObstructionScm lateralObstruction;
  ObstructionLongitudinal longitudinalObstruction;
  bool canClearObstructionLongitudinal;
  units::velocity::meters_per_second_t deltaVLateral;
  units::velocity::meters_per_second_t deltaVLongitudinal;
  bool willEnterMinDistanceBeforeClearingObstruction;
  bool expected_hasCollisionCourse;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_SurroundingVehicleQuery_HasCollisionCourse& obj)
  {
    os << "Testcase: " << obj.testcase
       << "\n toTheSideOfEgo: " << ScmCommons::BooleanToString(obj.toTheSideOfEgo)
       << "\n ttc: " << obj.ttc
       << "\n tauDot: " << obj.tauDot
       << "\n hasLateralOverlap with ego: " << ScmCommons::BooleanToString(obj.lateralObstruction.isOverlapping)
       << "\n longitudinalObstruction.front: " << obj.longitudinalObstruction.front
       << "\n Can clear longitudinal obstruction in time: " << ScmCommons::BooleanToString(obj.canClearObstructionLongitudinal)
       << "\n Delta V Lateral: " << obj.deltaVLateral
       << "\n Will enter minimum followind distance before clearing lateral obstruction: " << obj.willEnterMinDistanceBeforeClearingObstruction
       << "\n EXPECTED Collision Course: " << ScmCommons::BooleanToString(obj.expected_hasCollisionCourse)
       << "\n";
    return os;
  }
};

class SurroundingVehicleQuery_HasCollisionCourse : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_SurroundingVehicleQuery_HasCollisionCourse>
{
};

TEST_P(SurroundingVehicleQuery_HasCollisionCourse, TestCollisionCourseDetection)
{
  constexpr auto MIN_DISTANCE{10._m};
  const auto data = GetParam();

  const auto EGO_LATERAL_VELOCITY{0.0_mps + data.deltaVLateral};
  const auto VEHICLE_LATERAL_VELOCITY{0.0_mps};
  const auto EGO_LONGITUDINAL_VELOCITY{data.deltaVLongitudinal};
  const auto VEHICLE_LONGITUDINAL_VELOCITY{0.0_mps};
  const auto ACCELERATION{0.0_mps_sq};

  const auto VEHICLE_REAR_DISTANCE{data.willEnterMinDistanceBeforeClearingObstruction ? MIN_DISTANCE : 100.0_m};
  const auto VEHICLE_FRONT_DISTANCE{VEHICLE_REAR_DISTANCE + 3.0_m};

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, ToTheSideOfEgo).WillByDefault(Return(data.toTheSideOfEgo));
  ON_CALL(vehicle, GetTtc).WillByDefault(Return(data.ttc));
  ON_CALL(vehicle, GetTauDot).WillByDefault(Return(data.tauDot));
  ON_CALL(vehicle, GetLateralObstruction).WillByDefault(Return(data.lateralObstruction));
  ON_CALL(vehicle, GetLongitudinalObstruction).WillByDefault(Return(data.longitudinalObstruction));
  ON_CALL(vehicle, LateralObstructionOverlapping).WillByDefault(Return(data.lateralObstruction.isOverlapping));
  ON_CALL(vehicle, GetLateralVelocity).WillByDefault(Return(VEHICLE_LATERAL_VELOCITY));
  ON_CALL(vehicle, GetLongitudinalVelocity).WillByDefault(Return(VEHICLE_LONGITUDINAL_VELOCITY));
  ON_CALL(vehicle, FrontDistance).WillByDefault(Return(VEHICLE_FRONT_DISTANCE));
  ON_CALL(vehicle, RearDistance).WillByDefault(Return(VEHICLE_REAR_DISTANCE));
  ON_CALL(vehicle, GetLongitudinalAcceleration).WillByDefault(Return(ACCELERATION));
  ON_CALL(vehicle, GetRelativeNetDistance).WillByDefault(Return(50_m));

  OwnVehicleInformationScmExtended egoInfo;
  egoInfo.lateralVelocity = EGO_LATERAL_VELOCITY;
  egoInfo.longitudinalVelocity = EGO_LONGITUDINAL_VELOCITY;
  egoInfo.acceleration = ACCELERATION;
  SurroundingVehicleQuery testQuery(vehicle, egoInfo);

  auto a = testQuery.HasCollisionCourse(MIN_DISTANCE);
  ASSERT_EQ(data.expected_hasCollisionCourse, testQuery.HasCollisionCourse(MIN_DISTANCE));
}

namespace
{
constexpr bool DONT_CARE{false};

constexpr auto TTC_CRITICAL{1._s};
constexpr auto TTC_NON_CRITICAL_ABOVE_THRESHOLD{11._s};
constexpr auto TTC_NON_CRITICAL_NEGATIVE{-1._s};

constexpr double TAU_DOT_CRITICAL{-0.5};
constexpr double TAU_DOT_NON_CRITICAL{-0.4};

constexpr auto NO_LATERAL_VELOCITY_DIFFERENCE{0.0_mps};
constexpr auto AGENTS_MOVING_LATERAL{1.0_mps};
constexpr auto AGENTS_MOVING_LATERAL_NON_TTC_CRITICAL{0.5_mps};

constexpr auto LONGITUDINAL_VELOCITY_DIFFERENCE{1.0_mps};

const ObstructionScm LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO{1.0_m, 1.0_m, 0.0_m};
const ObstructionScm LATERAL_OBSTRUCTION_DIFFERENT_LANE{2.0_m, -5.0_m, 0.0_m};
const ObstructionLongitudinal LONGITUDINAL_OBSTRUCTION_CLEARABLE{4.0_m, -2.0_m, 0.0_m};
const ObstructionLongitudinal LONGITUDINAL_OBSTRUCTION_NOT_CLEARABLE{6.0_m, -1.0_m, 0.0_m};
const ObstructionLongitudinal LONGITUDINAL_OBSTRUCTION_DONT_CARE{1.0_m, -3.0_m, 0.0_m};

constexpr double SIDE_TTC_ABOVE_THRESHOLD{10.};
constexpr double SIDE_TTC_BELOW_THRESHOLD{5.};
constexpr double SIDE_TTC_MOVING_AWAY{-5.};
constexpr double SIDE_TTC_DONT_CARE{10.};
}  // namespace

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicleQuery_HasCollisionCourse,
    testing::Values(
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "On Collision Course, vehicle in front of ego",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = NO_LATERAL_VELOCITY_DIFFERENCE,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = true,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego, ttc above criticality, no collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_NON_CRITICAL_ABOVE_THRESHOLD,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = NO_LATERAL_VELOCITY_DIFFERENCE,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego, ttc negative, no collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_NON_CRITICAL_NEGATIVE,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = NO_LATERAL_VELOCITY_DIFFERENCE,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego, tau dot not critical, no collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_NON_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = NO_LATERAL_VELOCITY_DIFFERENCE,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego, lateral movement between agents, can clear before entering min distance, no collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_IN_FRONT_OF_EGO,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = false,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego, lateral movement between agents, cannot clear before entering min distance, collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = {2.0_m, 2.0_m, 0.0_m},
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_DONT_CARE,
            .canClearObstructionLongitudinal = DONT_CARE,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = true,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego on different Lane, can clear longitudinal obstruction before surrounding vehicle crashes into side, no collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_DIFFERENT_LANE,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_CLEARABLE,
            .canClearObstructionLongitudinal = true,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in front of ego on different Lane, cannot clear longitudinal obstruction before surrounding vehicle crashes into side, collision course",
            .toTheSideOfEgo = false,
            .ttc = TTC_CRITICAL,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_DIFFERENT_LANE,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_NOT_CLEARABLE,
            .canClearObstructionLongitudinal = false,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = true},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in SideArea, Side TTC above threshold, no collision course",
            .toTheSideOfEgo = true,
            .ttc = TTC_NON_CRITICAL_ABOVE_THRESHOLD,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_DIFFERENT_LANE,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_NOT_CLEARABLE,
            .deltaVLateral = AGENTS_MOVING_LATERAL_NON_TTC_CRITICAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in SideArea, Side TTC below threshold, can leave obstruction before crash from side, no collision course",
            .toTheSideOfEgo = true,
            .ttc = TTC_NON_CRITICAL_ABOVE_THRESHOLD,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_DIFFERENT_LANE,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_CLEARABLE,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = false},
        DataFor_SurroundingVehicleQuery_HasCollisionCourse{
            .testcase = "Vehicle in SideArea, Side TTC below threshold, cannot leave obstruction before crash from side, collision course",
            .toTheSideOfEgo = true,
            .ttc = TTC_NON_CRITICAL_ABOVE_THRESHOLD,
            .tauDot = TAU_DOT_CRITICAL,
            .lateralObstruction = LATERAL_OBSTRUCTION_DIFFERENT_LANE,
            .longitudinalObstruction = LONGITUDINAL_OBSTRUCTION_NOT_CLEARABLE,
            .deltaVLateral = AGENTS_MOVING_LATERAL,
            .deltaVLongitudinal = LONGITUDINAL_VELOCITY_DIFFERENCE,
            .willEnterMinDistanceBeforeClearingObstruction = DONT_CARE,
            .expected_hasCollisionCourse = true}));

/******************************************
 * CHECK IsSideSideVehicleChangingIntoSide *
 *******************************************/

struct DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide
{
  std::string testcase;
  RelativeLane laneOfPerception;
  units::velocity::meters_per_second_t lateralVelocity;
  units::length::meter_t lateralPositionInLane;
  bool isExceedingLateralMotionThreshold;
  bool expected_isChangingIntoSide;
};

class SurroundingVehicleQuery_IsSideSideVehicleChangingIntoSide : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide>
{
};

TEST_P(SurroundingVehicleQuery_IsSideSideVehicleChangingIntoSide, ReturnsTrueOnlyIfMotionIsPerceivedAndMovingTowardsSideLane)
{
  auto param = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetLateralVelocity).WillByDefault(Return(param.lateralVelocity));
  ON_CALL(fakeVehicle, GetLateralPositionInLane).WillByDefault(Return(param.lateralPositionInLane));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(param.laneOfPerception));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold).WillByDefault(Return(param.isExceedingLateralMotionThreshold));

  OwnVehicleInformationScmExtended egoInfo;
  SurroundingVehicleQuery vehicleQuery(fakeVehicle, egoInfo);

  const auto result{vehicleQuery.IsSideSideVehicleChangingIntoSide()};

  ASSERT_EQ(param.expected_isChangingIntoSide, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicleQuery_IsSideSideVehicleChangingIntoSide,
    ::testing::Values(
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnRightRightLane, MotionPerceivable, MovingWithPositiveLateralVelocity -> ReturnsTrue",
            .laneOfPerception = RelativeLane::RIGHTRIGHT,
            .lateralVelocity = 1.0_mps,
            .lateralPositionInLane = 1.0_m,
            .isExceedingLateralMotionThreshold = true,
            .expected_isChangingIntoSide = true},
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnRightRightLane, MotionNotPerceivable, MovingWithPositiveLateralVelocity -> ReturnsFalse",
            .laneOfPerception = RelativeLane::RIGHTRIGHT,
            .lateralVelocity = 1.0_mps,
            .lateralPositionInLane = 1.0_m,
            .isExceedingLateralMotionThreshold = false,
            .expected_isChangingIntoSide = false},
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnRightRightLane, MotionPerceivable, MovingWithNegativeLateralVelocity -> ReturnsFalse",
            .laneOfPerception = RelativeLane::RIGHTRIGHT,
            .lateralVelocity = -1.0_mps,
            .lateralPositionInLane = 1.0_m,
            .isExceedingLateralMotionThreshold = true,
            .expected_isChangingIntoSide = false},
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnRightRightLane, MotionPerceivable, MovingWithPositiveLateralVelocity but moving towards its lane center -> ReturnsFalse",
            .laneOfPerception = RelativeLane::RIGHTRIGHT,
            .lateralVelocity = 1.0_mps,
            .lateralPositionInLane = -1.0_m,
            .isExceedingLateralMotionThreshold = true,
            .expected_isChangingIntoSide = false},
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnLeftLeftLane, MotionPerceivable, MovingWithNegativeLateralVelocity -> ReturnsTrue",
            .laneOfPerception = RelativeLane::LEFTLEFT,
            .lateralVelocity = -1.0_mps,
            .lateralPositionInLane = -1.0_m,
            .isExceedingLateralMotionThreshold = true,
            .expected_isChangingIntoSide = true},
        DataFor_SurroundingVehicleQueryIsSideSideVehicleChangingIntoSide{
            .testcase = "VehicleOnLeftLeftLane, MotionPerceivable, MovingWithPositiveLateralVelocity -> ReturnsFalse",
            .laneOfPerception = RelativeLane::LEFTLEFT,
            .lateralVelocity = 1.0_mps,
            .lateralPositionInLane = -1.0_m,
            .isExceedingLateralMotionThreshold = true,
            .expected_isChangingIntoSide = false}));