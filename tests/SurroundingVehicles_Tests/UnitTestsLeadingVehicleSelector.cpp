/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>

#include "../Fakes/FakeLeadingVehicleFilter.h"
#include "../Fakes/FakeLeadingVehicleQuery.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "SurroundingVehicles/LeadingVehicleSelector.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"
#include "module/driver/src/ScmCommons.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/*****************************************
 * CHECK DetermineCollisionCourseVehicles *
 ******************************************/

TEST(LeadingVehicleSelector_DetermineCollisionCourseVehicles, ReturnCollisionCourseVehicles)
{
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  std::shared_ptr<NiceMock<FakeSurroundingVehicleQuery>> nonCollisionVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  std::shared_ptr<NiceMock<FakeSurroundingVehicleQuery>> collisionVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*collisionVehicleQuery, HasCollisionCourse(_)).WillByDefault(Return(true));
  ON_CALL(*nonCollisionVehicleQuery, HasCollisionCourse(_)).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> nonCollisionVehicle;
  ON_CALL(fakeVehicleQueryFactory, GetQuery(::testing::Ref(nonCollisionVehicle))).WillByDefault(Return(nonCollisionVehicleQuery));
  NiceMock<FakeSurroundingVehicle> collisionVehicle;
  ON_CALL(fakeVehicleQueryFactory, GetQuery(::testing::Ref(collisionVehicle))).WillByDefault(Return(collisionVehicleQuery));

  NiceMock<FakeLeadingVehicleFilter> filter;
  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, BelowJamVelocity()).WillByDefault(Return(false));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  surroundingVehicles.push_back(&nonCollisionVehicle);
  surroundingVehicles.push_back(&collisionVehicle);

  const auto leadingVehicleSelector{LeadingVehicleSelector(fakeVehicleQueryFactory, filter, leadingVehicleQuery)};
  const auto collisionCourseVehicles{leadingVehicleSelector.DetermineCollisionCourseVehicles(surroundingVehicles)};

  ASSERT_EQ(1, collisionCourseVehicles.size());
}

/***************************
 * CHECK FindLeadingVehicle *
 ****************************/

TEST(LeadingVehicleSelector_FindLeadingVehicle_MergeRegulate, IfMergeRegulateIsDominatingAndNoCollisionCourseDetected_ReturnMergeRegulateVehicle)
{
  NiceMock<FakeSurroundingVehicle> vehicle;
  NiceMock<FakeSurroundingVehicle> mergeRegulateVehicle;

  NiceMock<FakeLeadingVehicleFilter> filter;
  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;

  const auto fakeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeSurroundingVehicleQuery));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  surroundingVehicles.push_back(&vehicle);
  surroundingVehicles.push_back(&mergeRegulateVehicle);

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(mergeRegulateVehicle, GetId()).WillByDefault(Return(1));

  // Avoid detection of collision course
  ON_CALL(vehicle, GetTtc()).WillByDefault(Return(ScmDefinitions::TTC_LIMIT));
  ON_CALL(mergeRegulateVehicle, GetTtc()).WillByDefault(Return(ScmDefinitions::TTC_LIMIT));

  ON_CALL(vehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_REAR));
  ON_CALL(mergeRegulateVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(leadingVehicleQuery, GetMergeRegulateIfDominating(_)).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(filter, FilterVehiclesWithUnassignedAoi(_)).WillByDefault(Return(surroundingVehicles));

  const auto leadingVehicleSelector{LeadingVehicleSelector(fakeSurroundingVehicleQueryFactory, filter, leadingVehicleQuery)};
  const auto result{leadingVehicleSelector.FindLeadingVehicle(surroundingVehicles)};

  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result.value()->GetId(), 1);
}

TEST(LeadingVehicleSelector_FindLeadingVehicle_CollisionCourse, IfCollisionCourseDetected_ReturnCollisionCourseVehicle)
{
  NiceMock<FakeSurroundingVehicle> vehicle;
  NiceMock<FakeSurroundingVehicle> collisionCourseVehicle;

  NiceMock<FakeLeadingVehicleFilter> filter;
  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;

  const auto fakeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  const auto fakeSurroundingVehicleQueryCollision{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeSurroundingVehicleQuery));
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(::testing::Ref(collisionCourseVehicle))).WillByDefault(Return(fakeSurroundingVehicleQueryCollision));

  ON_CALL(*fakeSurroundingVehicleQuery, HasCollisionCourse(_)).WillByDefault(Return(false));
  ON_CALL(*fakeSurroundingVehicleQueryCollision, HasCollisionCourse(_)).WillByDefault(Return(true));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles{
      &vehicle, &collisionCourseVehicle};

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(collisionCourseVehicle, GetId()).WillByDefault(Return(1));

  ON_CALL(vehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_REAR));
  ON_CALL(collisionCourseVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(leadingVehicleQuery, GetMergeRegulateIfDominating(_)).WillByDefault(Return(std::nullopt));
  ON_CALL(filter, FilterVehiclesWithUnassignedAoi(_)).WillByDefault(Return(surroundingVehicles));
  ON_CALL(filter, FilterSideLaneVehicles(_)).WillByDefault(Return(surroundingVehicles));  //

  const auto leadingVehicleSelector{LeadingVehicleSelector(fakeSurroundingVehicleQueryFactory, filter, leadingVehicleQuery)};
  ON_CALL(filter, FilterSwervingTarget(_)).WillByDefault(Return(leadingVehicleSelector.DetermineCollisionCourseVehicles(surroundingVehicles)));
  const auto result{leadingVehicleSelector.FindLeadingVehicle(surroundingVehicles)};

  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result.value()->GetId(), 1);
}

TEST(LeadingVehicleSelector_FindLeadingVehicle_RegularFilterCase, ChooseVehicleWithSmallestThwIfAboveJamVelocity)
{
  NiceMock<FakeSurroundingVehicle> vehicle;
  NiceMock<FakeSurroundingVehicle> leadingVehicle;

  NiceMock<FakeLeadingVehicleFilter> filter;
  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, BelowJamVelocity).WillByDefault(Return(false));

  const auto fakeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeSurroundingVehicleQuery));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles{
      &vehicle, &leadingVehicle};

  EXPECT_CALL(filter, FilterRearVehicles(surroundingVehicles)).WillOnce(Return(surroundingVehicles));
  EXPECT_CALL(filter, FilterSideSideVehicles(surroundingVehicles)).WillOnce(Return(surroundingVehicles));

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(leadingVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(vehicle, GetThw()).WillByDefault(Return(10_s));
  ON_CALL(leadingVehicle, GetThw()).WillByDefault(Return(1_s));
  ON_CALL(vehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(10_m));
  ON_CALL(leadingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(100_m));
  const auto leadingVehicleSelector{LeadingVehicleSelector(fakeSurroundingVehicleQueryFactory, filter, leadingVehicleQuery)};

  ON_CALL(filter, FilterSwervingTarget(_)).WillByDefault(Return(leadingVehicleSelector.DetermineCollisionCourseVehicles(surroundingVehicles)));
  ON_CALL(filter, FilterSideLaneVehicles(_)).WillByDefault(Return(surroundingVehicles));
  const auto result{leadingVehicleSelector.FindLeadingVehicle(surroundingVehicles)};

  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result.value()->GetId(), 1);
}

TEST(LeadingVehicleSelector_FindLeadingVehicle_RegularFilterCase, ChooseVehicleWithSmallestNetDistanceIfBelowJamVelocity)
{
  NiceMock<FakeSurroundingVehicle> vehicle;
  NiceMock<FakeSurroundingVehicle> leadingVehicle;

  NiceMock<FakeLeadingVehicleFilter> filter;
  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, BelowJamVelocity).WillByDefault(Return(false));

  const auto fakeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeSurroundingVehicleQueryFactory;
  ON_CALL(fakeSurroundingVehicleQueryFactory, GetQuery(_)).WillByDefault(Return(fakeSurroundingVehicleQuery));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles{
      &vehicle, &leadingVehicle};

  EXPECT_CALL(filter, FilterRearVehicles(surroundingVehicles)).WillOnce(Return(surroundingVehicles));
  EXPECT_CALL(filter, FilterSideSideVehicles(surroundingVehicles)).WillOnce(Return(surroundingVehicles));

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(leadingVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(vehicle, GetThw()).WillByDefault(Return(10_s));
  ON_CALL(leadingVehicle, GetThw()).WillByDefault(Return(1_s));
  ON_CALL(vehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(10_m));
  ON_CALL(leadingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(100_m));

  const auto leadingVehicleSelector{LeadingVehicleSelector(fakeSurroundingVehicleQueryFactory, filter, leadingVehicleQuery)};
  ON_CALL(filter, FilterSwervingTarget(_)).WillByDefault(Return(leadingVehicleSelector.DetermineCollisionCourseVehicles(surroundingVehicles)));
  ON_CALL(filter, FilterSideLaneVehicles(_)).WillByDefault(Return(surroundingVehicles));
  const auto result{leadingVehicleSelector.FindLeadingVehicle(surroundingVehicles)};

  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result.value()->GetId(), 1);
}