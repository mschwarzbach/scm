/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-actions.h>
#include <gmock/gmock-spec-builders.h>
#include <gtest/gtest-param-test.h>
#include <gtest/gtest.h>

#include <ostream>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSwerving.h"
#include "LateralAction.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/LeadingVehicleQuery.h"
#include "include/common/ScmDefinitions.h"
#include "module/driver/src/ScmCommons.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/*************************************
 * CHECK GetMergeRegulateIfDominating *
 **************************************/

struct DataFor_LeadingVehicleQuery_GetMergeRegulateIfDominating
{
  std::string testcase;
  AreaOfInterest mergeRegulateAoi;
  bool hasEgoFrontVehicle;
  units::length::meter_t egoFrontDistance;
  units::length::meter_t minDistanceEgoFront;
  double urgencyFactorForLaneChange;
  std::optional<AreaOfInterest> expected_mergeRegulateAoi;
};

class LeadingVehicleQuery_GetMergeRegulateIfDominating : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_LeadingVehicleQuery_GetMergeRegulateIfDominating>
{
protected:
  LeadingVehicleQuery GetLeadingVehicleQuery()
  {
    return LeadingVehicleQuery(_mentalModel, _mentalCalculations, _featureExtractor, _swerving);
  }

  NiceMock<FakeMentalModel> _mentalModel;
  NiceMock<FakeMentalCalculations> _mentalCalculations;
  NiceMock<FakeFeatureExtractor> _featureExtractor;
  NiceMock<FakeSwerving> _swerving;
};

TEST_P(LeadingVehicleQuery_GetMergeRegulateIfDominating, ReturnsMergeRegulateAoiIfEgoFrontIsFarEnoughAway)
{
  auto data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle1;
  NiceMock<FakeSurroundingVehicle> fakeVehicle2;
  ON_CALL(fakeVehicle1, GetAssignedAoi).WillByDefault(Return(data.hasEgoFrontVehicle ? AreaOfInterest::EGO_FRONT : AreaOfInterest::EGO_REAR));
  ON_CALL(fakeVehicle2, GetAssignedAoi).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  if (data.hasEgoFrontVehicle)
  {
    ON_CALL(fakeVehicle1, GetRelativeLongitudinalPosition).WillByDefault(Return(data.egoFrontDistance));
    ON_CALL(_mentalCalculations, GetMinDistance(AreaOfInterest::EGO_FRONT, _, _, _)).WillByDefault(Return(data.minDistanceEgoFront));
    ON_CALL(_mentalCalculations, GetUrgencyFactorForLaneChange).WillByDefault(Return(data.urgencyFactorForLaneChange));
  }

  ON_CALL(_mentalModel, GetMergeRegulate).WillByDefault(Return(data.mergeRegulateAoi));

  LeadingVehicleQuery leadingVehicleQuery{GetLeadingVehicleQuery()};
  const auto result{leadingVehicleQuery.GetMergeRegulateIfDominating({&fakeVehicle1, &fakeVehicle2})};

  ASSERT_EQ(data.expected_mergeRegulateAoi, result);
}

namespace
{
static constexpr double DONT_CARE{10.0};
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleQuery_GetMergeRegulateIfDominating,
    ::testing::Values(
        DataFor_LeadingVehicleQuery_GetMergeRegulateIfDominating{
            .testcase = "MergeRegulateAoi in front of ego, EGO_FRONT vehicle distance is above min distance times urgency",
            .mergeRegulateAoi = AreaOfInterest::RIGHT_FRONT,
            .hasEgoFrontVehicle = true,
            .egoFrontDistance = 11.0_m,
            .minDistanceEgoFront = 20.0_m,
            .urgencyFactorForLaneChange = 0.5,
            .expected_mergeRegulateAoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_LeadingVehicleQuery_GetMergeRegulateIfDominating{
            .testcase = "MergeRegulateAoi in front of ego, no EGO_FRONT vehicle present",
            .mergeRegulateAoi = AreaOfInterest::RIGHT_FRONT,
            .hasEgoFrontVehicle = false,
            .egoFrontDistance = units::make_unit<units::length::meter_t>(DONT_CARE),
            .minDistanceEgoFront = 20.0_m,
            .urgencyFactorForLaneChange = 0.5,
            .expected_mergeRegulateAoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_LeadingVehicleQuery_GetMergeRegulateIfDominating{
            .testcase = "MergeRegulateAoi in front of ego, EGO_FRONT vehicle distance is equal to min distance times urgency",
            .mergeRegulateAoi = AreaOfInterest::RIGHT_FRONT,
            .hasEgoFrontVehicle = true,
            .egoFrontDistance = 10.0_m,
            .minDistanceEgoFront = 20.0_m,
            .urgencyFactorForLaneChange = 0.5,
            .expected_mergeRegulateAoi = std::nullopt}));

/**********************************
 * CHECK GetEgoLaneChangeDirection *
 ***********************************/

struct DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection
{
  SurroundingLane currentDirection;
  LateralAction currentLateralAction;
  bool isLaneChangePastTransition;
  std::optional<SurroundingLane> expected_laneChangeDirection;
};

class LeadingVehicleQuery_GetEgoLaneChangeDirection : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection>
{
protected:
  LeadingVehicleQuery GetLeadingVehicleQuery()
  {
    return LeadingVehicleQuery(_mentalModel, _mentalCalculations, _featureExtractor, _swerving);
  }

  NiceMock<FakeMentalModel> _mentalModel;
  NiceMock<FakeMentalCalculations> _mentalCalculations;
  NiceMock<FakeFeatureExtractor> _featureExtractor;
  NiceMock<FakeSwerving> _swerving;
};

TEST_P(LeadingVehicleQuery_GetEgoLaneChangeDirection, ReturnsRelativeLaneEgoIsChangingToIfEgoIsAboutToChangeLanes)
{
  auto data = GetParam();

  ON_CALL(_mentalModel, GetCurrentDirection).WillByDefault(Return(data.currentDirection));
  ON_CALL(_mentalModel, GetLateralAction).WillByDefault(Return(data.currentLateralAction));
  ON_CALL(_mentalModel, GetIsLaneChangePastTransition).WillByDefault(Return(data.isLaneChangePastTransition));

  LeadingVehicleQuery leadingVehicleQuery{GetLeadingVehicleQuery()};
  const auto result{leadingVehicleQuery.GetEgoLaneChangeDirection()};

  ASSERT_EQ(data.expected_laneChangeDirection, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleQuery_GetEgoLaneChangeDirection,
    ::testing::Values(
        DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection{
            .currentDirection = SurroundingLane::RIGHT,
            .currentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = false,
            .expected_laneChangeDirection = SurroundingLane::RIGHT},
        DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection{
            .currentDirection = SurroundingLane::RIGHT,
            .currentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = true,
            .expected_laneChangeDirection = std::nullopt},
        DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection{
            .currentDirection = SurroundingLane::EGO,
            .currentLateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = false,
            .expected_laneChangeDirection = std::nullopt},
        DataFor_LeadingVehicleQuery_GetEgoLaneChangeDirection{
            .currentDirection = SurroundingLane::RIGHT,
            .currentLateralAction = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::RIGHT),
            .isLaneChangePastTransition = false,
            .expected_laneChangeDirection = std::nullopt}));

/***************************************
 * CHECK DoesOvertakingProhibitionApply *
 ****************************************/

struct DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply
{
  bool isRighthandTraffic;
  AreaOfInterest vehicleAoi;
  units::velocity::meters_per_second_t vehicleVelocity;
  units::velocity::meters_per_second_t egoTargetVelocity;
  bool featureExtractorDoesOuterLaneProhibitionApply;
  bool expected_overtakingProhibitionApplies;
};

class LeadingVehicleQuery_DoesOvertakingProhibitionApply : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply>
{
protected:
  LeadingVehicleQuery GetLeadingVehicleQuery()
  {
    return LeadingVehicleQuery(_mentalModel, _mentalCalculations, _featureExtractor, _swerving);
  }

  NiceMock<FakeMentalModel> _mentalModel;
  NiceMock<FakeMentalCalculations> _mentalCalculations;
  NiceMock<FakeFeatureExtractor> _featureExtractor;
  NiceMock<FakeSwerving> _swerving;
};

TEST_P(LeadingVehicleQuery_DoesOvertakingProhibitionApply, ReturnsTrueIfVehicleIsInRelevantAoiAndOvertakingProhibitionAppliesAndEgoWantsToGoFasterOrSameSpeedAsVehicle)
{
  auto data = GetParam();

  ON_CALL(_mentalModel, IsRightHandTraffic).WillByDefault(Return(data.isRighthandTraffic));
  ON_CALL(_mentalModel, GetAbsoluteVelocityTargeted).WillByDefault(Return(data.egoTargetVelocity));

  ON_CALL(_featureExtractor, DoesOuterLaneOvertakingProhibitionApply(data.isRighthandTraffic ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT, data.isRighthandTraffic)).WillByDefault(Return(data.featureExtractorDoesOuterLaneProhibitionApply));

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, GetAssignedAoi).WillByDefault(Return(data.vehicleAoi));
  ON_CALL(vehicle, GetLongitudinalVelocity).WillByDefault(Return(data.vehicleVelocity));

  LeadingVehicleQuery leadingVehicleQuery{GetLeadingVehicleQuery()};
  const auto result{leadingVehicleQuery.DoesOvertakingProhibitionApply(vehicle)};

  ASSERT_EQ(data.expected_overtakingProhibitionApplies, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleQuery_DoesOvertakingProhibitionApply,
    ::testing::Values(
        DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply{
            .isRighthandTraffic = true,
            .vehicleAoi = AreaOfInterest::LEFT_FRONT,
            .vehicleVelocity = 10.0_mps,
            .egoTargetVelocity = 20.0_mps,
            .featureExtractorDoesOuterLaneProhibitionApply = true,
            .expected_overtakingProhibitionApplies = true},
        DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply{
            .isRighthandTraffic = false,
            .vehicleAoi = AreaOfInterest::RIGHT_FRONT,
            .vehicleVelocity = 10.0_mps,
            .egoTargetVelocity = 20.0_mps,
            .featureExtractorDoesOuterLaneProhibitionApply = true,
            .expected_overtakingProhibitionApplies = true},
        DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply{
            .isRighthandTraffic = true,
            .vehicleAoi = AreaOfInterest::RIGHT_FRONT,
            .vehicleVelocity = 10.0_mps,
            .egoTargetVelocity = 20.0_mps,
            .featureExtractorDoesOuterLaneProhibitionApply = true,
            .expected_overtakingProhibitionApplies = false},
        DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply{
            .isRighthandTraffic = true,
            .vehicleAoi = AreaOfInterest::LEFT_FRONT,
            .vehicleVelocity = 10.0_mps,
            .egoTargetVelocity = 9.0_mps,
            .featureExtractorDoesOuterLaneProhibitionApply = true,
            .expected_overtakingProhibitionApplies = false},
        DataFor_LeadingVehicleQuery_DoesOvertakingProhibitionApply{
            .isRighthandTraffic = true,
            .vehicleAoi = AreaOfInterest::LEFT_FRONT,
            .vehicleVelocity = 10.0_mps,
            .egoTargetVelocity = 20.0_mps,
            .featureExtractorDoesOuterLaneProhibitionApply = false,
            .expected_overtakingProhibitionApplies = false}));

/****************************
 * CHECK GetSwervingTargetId *
 *****************************/

struct DataFor_LeadingVehicleQuery_GetSwervingTargetId
{
  int swervingAgentId;
  SwervingState currentSwervingState;
  std::optional<int> expected_swervingTargetId;
};

class LeadingVehicleQuery_GetSwervingTargetId : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_LeadingVehicleQuery_GetSwervingTargetId>
{
protected:
  LeadingVehicleQuery GetLeadingVehicleQuery()
  {
    return LeadingVehicleQuery(_mentalModel, _mentalCalculations, _featureExtractor, _swerving);
  }

  NiceMock<FakeMentalModel> _mentalModel;
  NiceMock<FakeMentalCalculations> _mentalCalculations;
  NiceMock<FakeFeatureExtractor> _featureExtractor;
  NiceMock<FakeSwerving> _swerving;
};

TEST_P(LeadingVehicleQuery_GetSwervingTargetId, ReturnsRelativeLaneEgoIsChangingToIfEgoIsAboutToChangeLanes)
{
  auto data = GetParam();

  ON_CALL(_swerving, GetSwervingState).WillByDefault(Return(data.currentSwervingState));
  ON_CALL(_swerving, GetAgentIdOfSwervingTarget).WillByDefault(Return(data.swervingAgentId));

  LeadingVehicleQuery leadingVehicleQuery{GetLeadingVehicleQuery()};
  const auto result{leadingVehicleQuery.GetSwervingTargetId()};

  ASSERT_EQ(data.expected_swervingTargetId, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleQuery_GetSwervingTargetId,
    ::testing::Values(
        DataFor_LeadingVehicleQuery_GetSwervingTargetId{
            .swervingAgentId = 1,
            .currentSwervingState = SwervingState::EVADING,
            .expected_swervingTargetId = 1},
        DataFor_LeadingVehicleQuery_GetSwervingTargetId{
            .swervingAgentId = 1,
            .currentSwervingState = SwervingState::DEFUSING,
            .expected_swervingTargetId = 1},
        DataFor_LeadingVehicleQuery_GetSwervingTargetId{
            .swervingAgentId = 1,
            .currentSwervingState = SwervingState::NONE,
            .expected_swervingTargetId = std::nullopt},
        DataFor_LeadingVehicleQuery_GetSwervingTargetId{
            .swervingAgentId = 1,
            .currentSwervingState = SwervingState::RETURNING,
            .expected_swervingTargetId = std::nullopt}));
