/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TestSurroundingVehicles.h"

#include <algorithm>

#include "SurroundingVehicles/SurroundingVehicleInterface.h"

void TestSurroundingVehicles::SetSurroundingVehicles(std::initializer_list<SurroundingVehicleInterface*> surroundingVehicles)
{
  assert(CANNOT_CALL_SET_SURROUNDING_VEHICLES_TWICE());
  for (auto vehicle : surroundingVehicles)
  {
    _surroundingVehicles.emplace_back(std::unique_ptr<SurroundingVehicleInterface>(vehicle));
  }
}