################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME SurroundingVehicles_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)

file(GLOB 
     SRC_SUR_VEH
     CONFIGURE_DEPENDS
     FOLLOW_SYMLINKS false
     LIST_DIRECTORIES false
     "${DRIVER_DIR}/src/SurroundingVehicles/*.cpp")

file(GLOB 
     HEADER_SUR_VEH
     CONFIGURE_DEPENDS
     FOLLOW_SYMLINKS false
     LIST_DIRECTORIES false
     "${DRIVER_DIR}/src/SurroundingVehicles/*.h")

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN
  LINKOSI

  SOURCES
  ${SRC_SUR_VEH}
  TestSurroundingVehicles.cpp
  UnitTestsSurroundingVehicles.cpp
  UnitTestsSurroundingVehicleQuery.cpp
  UnitTestsLeadingVehicleSelector.cpp
  UnitTestsLeadingVehicleFilter.cpp
  UnitTestsLeadingVehicleQuery.cpp
  ${DRIVER_DIR}/src/GazeControl/GazeCone.cpp
  ${DRIVER_DIR}/src/Extrapolation.cpp
  ${DRIVER_DIR}/src/InfrastructureCharacteristics.cpp
  ${DRIVER_DIR}/src/MicroscopicCharacteristics.cpp
  ${DRIVER_DIR}/src/LaneQueryHelper.cpp
  ${DRIVER_DIR}/src/Reliability.cpp
  ${DRIVER_DIR}/src/ScmDependencies.cpp
  ${DRIVER_DIR}/src/ScmCommons.cpp

  HEADERS
  ${HEADER_SUR_VEH}
  TestSurroundingVehicles.h
  ../Fakes/FakeSurroundingVehicle.h
  ../Fakes/FakeLeadingVehicleQuery.h
  ../Fakes/FakeLeadingVehicleFilter.h
  ../Fakes/FakeMentalCalculations.h
  ../Fakes/FakeInformationAcquisition.h
  ../Fakes/FakeMentalModel.h
  ../Fakes/FakeMicroscopicCharacteristics.h
  ${DRIVER_DIR}/src/AoiAssignerInterface.h
  ${DRIVER_DIR}/src/Extrapolation.h
  ${DRIVER_DIR}/src/ExtrapolationInterface.h
  ${DRIVER_DIR}/src/InfrastructureCharacteristics.h
  ${DRIVER_DIR}/src/MentalModelDefinitions.h
  ${DRIVER_DIR}/src/MentalModelInterface.h
  ${DRIVER_DIR}/src/MicroscopicCharacteristics.h
  ${DRIVER_DIR}/src/MicroscopicCharacteristicsInterface.h
  ${DRIVER_DIR}/src/Reliability.h
  ${DRIVER_DIR}/src/ScmDependencies.h
  ${DRIVER_DIR}/src/ScmDefinitions.h
  ${DRIVER_DIR}/src/ScmCommons.h
  ${ROOT_DIR}/include/common/ScmEnums.h

  INCDIRS
  ${DRIVER_DIR}
  ${DRIVER_DIR}/src
  ${ROOT_DIR}/module/parameterParser/src
  ${ROOT_DIR}/module/parameterParser/src/Signals
  ${ROOT_DIR}/module/sensor/src/Signals

  LIBRARIES
  Stochastics::Stochastics
  OsiQueryLibrary
)

target_compile_definitions(${COMPONENT_TEST_NAME} PRIVATE
  SCM_TESTING
  TESTING_ENABLED
)
