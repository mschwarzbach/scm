/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "ScmCommons.h"

/**************************************
 * CHECK MapAreaOfInterestToGazeState *
 **************************************/

struct DataFor_MapAreaOfInterestToGazeState
{
  AreaOfInterest aoi;
  GazeState expectedResult;
};

class ScmCommons_MapAreaOfInterestToGazeState : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_MapAreaOfInterestToGazeState>
{
};

TEST_P(ScmCommons_MapAreaOfInterestToGazeState, Check_MapAreaOfInterestToGazeState)
{
  DataFor_MapAreaOfInterestToGazeState data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.MapAreaOfInterestToGazeState(data.aoi);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_MapAreaOfInterestToGazeState,
    testing::Values(
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = GazeState::LEFT_FRONT},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedResult = GazeState::LEFT_FRONT_FAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = GazeState::RIGHT_FRONT},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedResult = GazeState::RIGHT_FRONT_FAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::LEFT_REAR,
            .expectedResult = GazeState::LEFT_REAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .expectedResult = GazeState::RIGHT_REAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = GazeState::EGO_FRONT},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::EGO_FRONT_FAR,
            .expectedResult = GazeState::EGO_FRONT_FAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = GazeState::EGO_REAR},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = GazeState::LEFT_SIDE},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .expectedResult = GazeState::LEFTLEFT_SIDE},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .expectedResult = GazeState::RIGHT_SIDE},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .expectedResult = GazeState::RIGHTRIGHT_SIDE},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::INSTRUMENT_CLUSTER,
            .expectedResult = GazeState::INSTRUMENT_CLUSTER},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::INFOTAINMENT,
            .expectedResult = GazeState::INFOTAINMENT},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::HUD,
            .expectedResult = GazeState::HUD},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expectedResult = GazeState::NumberOfGazeStates},
        DataFor_MapAreaOfInterestToGazeState{
            .aoi = AreaOfInterest::DISTRACTION,
            .expectedResult = GazeState::DISTRACTION}));

/*********************************
 * CHECK ConvertFarAoiToCloseAoi *
 *********************************/

struct DataFor_ConvertFarAoiToCloseAoi
{
  AreaOfInterest aoi;
  AreaOfInterest expectedResult;
};

class ScmCommons_ConvertFarAoiToCloseAoi : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_ConvertFarAoiToCloseAoi>
{
};

TEST_P(ScmCommons_ConvertFarAoiToCloseAoi, Check_ConvertFarAoiToCloseAoi)
{
  DataFor_ConvertFarAoiToCloseAoi data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.ConvertFarAoiToCloseAoi(data.aoi);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_ConvertFarAoiToCloseAoi,
    testing::Values(
        DataFor_ConvertFarAoiToCloseAoi{
            .aoi = AreaOfInterest::EGO_FRONT_FAR,
            .expectedResult = AreaOfInterest::EGO_FRONT},
        DataFor_ConvertFarAoiToCloseAoi{
            .aoi = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedResult = AreaOfInterest::LEFT_FRONT},
        DataFor_ConvertFarAoiToCloseAoi{
            .aoi = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedResult = AreaOfInterest::RIGHT_FRONT},
        DataFor_ConvertFarAoiToCloseAoi{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = AreaOfInterest::NumberOfAreaOfInterests}));

/***************************************
 * CHECK FieldOfViewAssignmentToString *
 ***************************************/

struct DataFor_FieldOfViewAssignmentToString
{
  FieldOfViewAssignment qualityArea;
  std::string expectedResult;
};

class ScmCommons_FieldOfViewAssignmentToString : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_FieldOfViewAssignmentToString>
{
};

TEST_P(ScmCommons_FieldOfViewAssignmentToString, Check_FieldOfViewAssignmentToString)
{
  DataFor_FieldOfViewAssignmentToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.FieldOfViewAssignmentToString(data.qualityArea);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_FieldOfViewAssignmentToString,
    testing::Values(
        DataFor_FieldOfViewAssignmentToString{
            .qualityArea = FieldOfViewAssignment::FOVEA,
            .expectedResult = "FOVEA"},
        DataFor_FieldOfViewAssignmentToString{
            .qualityArea = FieldOfViewAssignment::UFOV,
            .expectedResult = "UFOV"},
        DataFor_FieldOfViewAssignmentToString{
            .qualityArea = FieldOfViewAssignment::PERIPHERY,
            .expectedResult = "PERIPHERY"},
        DataFor_FieldOfViewAssignmentToString{
            .qualityArea = FieldOfViewAssignment::NONE,
            .expectedResult = "NONE"}));

/***************************************
 * CHECK FieldOfViewAssignmentToString *
 ***************************************/

struct DataFor_GazeStateToString
{
  GazeState gazeState;
  std::string expectedResult;
};

class ScmCommons_GazeStateToString : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_GazeStateToString>
{
};

TEST_P(ScmCommons_GazeStateToString, Check_GazeStateToString)
{
  DataFor_GazeStateToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.GazeStateToString(data.gazeState);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_GazeStateToString,
    testing::Values(
        DataFor_GazeStateToString{
            .gazeState = GazeState::EGO_FRONT,
            .expectedResult = "EGO_FRONT"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::EGO_FRONT_FAR,
            .expectedResult = "EGO_FRONT_FAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::EGO_REAR,
            .expectedResult = "EGO_REAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::HUD,
            .expectedResult = "HUD"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::INFOTAINMENT,
            .expectedResult = "INFOTAINMENT"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::INSTRUMENT_CLUSTER,
            .expectedResult = "INSTRUMENT_CLUSTER"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::LEFT_FRONT,
            .expectedResult = "LEFT_FRONT"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::LEFT_FRONT_FAR,
            .expectedResult = "LEFT_FRONT_FAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::LEFT_REAR,
            .expectedResult = "LEFT_REAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::LEFT_SIDE,
            .expectedResult = "LEFT_SIDE"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::NumberOfGazeStates,
            .expectedResult = "NumberOfGazeStates"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::RIGHT_FRONT,
            .expectedResult = "RIGHT_FRONT"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::RIGHT_FRONT_FAR,
            .expectedResult = "RIGHT_FRONT_FAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::RIGHT_REAR,
            .expectedResult = "RIGHT_REAR"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::RIGHT_SIDE,
            .expectedResult = "RIGHT_SIDE"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::SACCADE,
            .expectedResult = "SACCADE"},
        DataFor_GazeStateToString{
            .gazeState = GazeState::UNDEFINED,
            .expectedResult = "UNDEFINED"}));

/***************************
 * CHECK IndicatorToString *
 ***************************/

struct DataFor_IndicatorToString
{
  scm::LightState::Indicator indicator;
  std::string expectedResult;
};

class ScmCommons_IndicatorToString : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_IndicatorToString>
{
};

TEST_P(ScmCommons_IndicatorToString, Check_IndicatorToString)
{
  DataFor_IndicatorToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.IndicatorToString(data.indicator);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_IndicatorToString,
    testing::Values(
        DataFor_IndicatorToString{
            .indicator = scm::LightState::Indicator::Off,
            .expectedResult = "IndicatorState_Off"},
        DataFor_IndicatorToString{
            .indicator = scm::LightState::Indicator::Warn,
            .expectedResult = "IndicatorState_Warn"},
        DataFor_IndicatorToString{
            .indicator = scm::LightState::Indicator::Left,
            .expectedResult = "IndicatorState_Left"},
        DataFor_IndicatorToString{
            .indicator = scm::LightState::Indicator::Right,
            .expectedResult = "IndicatorState_Right"}));

/**************************
 * CHECK LaneTypeToString *
 **************************/

struct DataFor_LaneTypeToString
{
  scm::LaneType type;
  std::string expectedResult;
};

class ScmCommons_LaneTypeToString : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_LaneTypeToString>
{
};

TEST_P(ScmCommons_LaneTypeToString, Check_LaneTypeToString)
{
  DataFor_LaneTypeToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.LaneTypeToString(data.type);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_LaneTypeToString,
    testing::Values(
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Shoulder,
            .expectedResult = "Shoulder"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Border,
            .expectedResult = "Border"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Driving,
            .expectedResult = "Driving"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Stop,
            .expectedResult = "Stop"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::None,
            .expectedResult = "None"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Restricted,
            .expectedResult = "Restricted"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Parking,
            .expectedResult = "Parking"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Median,
            .expectedResult = "Median"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Biking,
            .expectedResult = "Biking"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Sidewalk,
            .expectedResult = "Sidewalk"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Curb,
            .expectedResult = "Curb"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Exit,
            .expectedResult = "Exit"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::Entry,
            .expectedResult = "Entry"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::OnRamp,
            .expectedResult = "OnRamp"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::OffRamp,
            .expectedResult = "OffRamp"},
        DataFor_LaneTypeToString{
            .type = scm::LaneType::ConnectingRamp,
            .expectedResult = "ConnectingRamp"}));

/*******************************
 * CHECK SwervingStateToString *
 *******************************/

struct DataFor_SwervingStateToString
{
  SwervingState action;
  std::string expectedResult;
};

class ScmCommons_SwervingStateToString : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_SwervingStateToString>
{
};

TEST_P(ScmCommons_SwervingStateToString, Check_SwervingStateToString)
{
  DataFor_SwervingStateToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.SwervingStateToString(data.action);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_SwervingStateToString,
    testing::Values(
        DataFor_SwervingStateToString{
            .action = SwervingState::DEFUSING,
            .expectedResult = "DEFUSING"},
        DataFor_SwervingStateToString{
            .action = SwervingState::EVADING,
            .expectedResult = "EVADING"},
        DataFor_SwervingStateToString{
            .action = SwervingState::NONE,
            .expectedResult = "NONE"},
        DataFor_SwervingStateToString{
            .action = SwervingState::RETURNING,
            .expectedResult = "RETURNING"}));

/**********************************
 * CHECK LaneChangeActionToString *
 **********************************/

struct DataFor_LaneChangeActionToString
{
  LaneChangeAction action;
  std::string expectedResult;
};

class ScmCommons_LaneChangeActionToString : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_LaneChangeActionToString>
{
};

TEST_P(ScmCommons_LaneChangeActionToString, Check_LaneChangeActionToString)
{
  DataFor_LaneChangeActionToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.LaneChangeActionToString(data.action);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_LaneChangeActionToString,
    testing::Values(
        DataFor_LaneChangeActionToString{
            .action = LaneChangeAction::None,
            .expectedResult = "None"},
        DataFor_LaneChangeActionToString{
            .action = LaneChangeAction::IntentPositive,
            .expectedResult = "IntentPositive"},
        DataFor_LaneChangeActionToString{
            .action = LaneChangeAction::IntentNegative,
            .expectedResult = "IntentNegative"},
        DataFor_LaneChangeActionToString{
            .action = LaneChangeAction::ForcePositive,
            .expectedResult = "ForcePositive"},
        DataFor_LaneChangeActionToString{
            .action = LaneChangeAction::ForceNegative,
            .expectedResult = "ForceNegative"}));

/***********************************
 * CHECK MinThwPerspectiveToString *
 ***********************************/

struct DataFor_MinThwPerspectiveToString
{
  MinThwPerspective perspective;
  std::string expectedResult;
};

class ScmCommons_MinThwPerspectiveToString : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_MinThwPerspectiveToString>
{
};

TEST_P(ScmCommons_MinThwPerspectiveToString, Check_MinThwPerspectiveToString)
{
  DataFor_MinThwPerspectiveToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.MinThwPerspectiveToString(data.perspective);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_MinThwPerspectiveToString,
    testing::Values(
        DataFor_MinThwPerspectiveToString{
            .perspective = MinThwPerspective::NORM,
            .expectedResult = "NORM"},
        DataFor_MinThwPerspectiveToString{
            .perspective = MinThwPerspective::EGO_ANTICIPATED_FRONT,
            .expectedResult = "EGO_ANTICIPATED_FRONT"},
        DataFor_MinThwPerspectiveToString{
            .perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .expectedResult = "EGO_ANTICIPATED_REAR"}));

/*********************************
 * CHECK LaneChangeStateToString *
 *********************************/

struct DataFor_LaneChangeStateToString
{
  LaneChangeState state;
  std::string expectedResult;
};

class ScmCommons_LaneChangeStateToString : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_LaneChangeStateToString>
{
};

TEST_P(ScmCommons_LaneChangeStateToString, Check_LaneChangeStateToString)
{
  DataFor_LaneChangeStateToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.LaneChangeStateToString(data.state);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_LaneChangeStateToString,
    testing::Values(
        DataFor_LaneChangeStateToString{
            .state = LaneChangeState::LaneChangeLeft,
            .expectedResult = "LEFT"},
        DataFor_LaneChangeStateToString{
            .state = LaneChangeState::LaneChangeRight,
            .expectedResult = "RIGHT"},
        DataFor_LaneChangeStateToString{
            .state = LaneChangeState::NoLaneChange,
            .expectedResult = "NO CHANGE"}));

/**********************************************
 * CHECK RelativeLongitudinalPositionToString *
 **********************************************/

struct DataFor_RelativeLongitudinalPositionToString
{
  RelativeLongitudinalPosition lane;
  std::string expectedResult;
};

class ScmCommons_RelativeLongitudinalPositionToString : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_RelativeLongitudinalPositionToString>
{
};

TEST_P(ScmCommons_RelativeLongitudinalPositionToString, Check_RelativeLongitudinalPositionToString)
{
  DataFor_RelativeLongitudinalPositionToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.RelativeLongitudinalPositionToString(data.lane);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_RelativeLongitudinalPositionToString,
    testing::Values(
        DataFor_RelativeLongitudinalPositionToString{
            .lane = RelativeLongitudinalPosition::FRONT_FAR,
            .expectedResult = "FRONT_FAR"},
        DataFor_RelativeLongitudinalPositionToString{
            .lane = RelativeLongitudinalPosition::FRONT,
            .expectedResult = "FRONT"},
        DataFor_RelativeLongitudinalPositionToString{
            .lane = RelativeLongitudinalPosition::SIDE,
            .expectedResult = "SIDE"},
        DataFor_RelativeLongitudinalPositionToString{
            .lane = RelativeLongitudinalPosition::REAR,
            .expectedResult = "REAR"},
        DataFor_RelativeLongitudinalPositionToString{
            .lane = RelativeLongitudinalPosition::REAR_FAR,
            .expectedResult = "REAR_FAR"}));

/*************************
 * CHECK CalculateNetGap *
 *************************/

struct DataFor_CalculateNetGap
{
  units::length::meter_t relativeNetDistance;
  AreaOfInterest aoi;
  units::time::second_t expectedResult;
};

class ScmCommons_CalculateNetGap : public ::testing::Test,
                                   public ::testing::WithParamInterface<DataFor_CalculateNetGap>
{
};

TEST_P(ScmCommons_CalculateNetGap, Check_CalculateNetGap)
{
  DataFor_CalculateNetGap data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.CalculateNetGap(20.0_mps, 10.0_mps, data.relativeNetDistance, data.aoi);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_CalculateNetGap,
    testing::Values(
        DataFor_CalculateNetGap{
            .relativeNetDistance = -1.0_m,
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = 0.0_s},
        DataFor_CalculateNetGap{
            .relativeNetDistance = 20.0_m,
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = 2.0_s},
        DataFor_CalculateNetGap{
            .relativeNetDistance = -1.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 0.0_s},
        DataFor_CalculateNetGap{
            .relativeNetDistance = 40.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 2.0_s},
        DataFor_CalculateNetGap{
            .relativeNetDistance = 40.0_m,
            .aoi = AreaOfInterest::DISTRACTION,
            .expectedResult = 99.0_s}));

/****************************
 * CHECK CalculateNetGapDot *
 ****************************/

struct DataFor_CalculateNetGapDot
{
  units::length::meter_t relativeNetDistance;
  AreaOfInterest aoi;
  double expectedResult;
};

class ScmCommons_CalculateNetGapDot : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_CalculateNetGapDot>
{
};

TEST_P(ScmCommons_CalculateNetGapDot, Check_CalculateNetGapDot)
{
  DataFor_CalculateNetGapDot data = GetParam();
  ScmCommons testScmCommons;

  auto v_ego = 10.0_mps;
  auto v_observed = 10.0_mps;
  auto a_ego = 1.0_mps_sq;
  auto a_observed = 10.0_mps_sq;
  auto vDelta = 20.0_mps;

  auto result = testScmCommons.CalculateNetGapDot(v_ego, v_observed, a_ego, a_observed, data.relativeNetDistance, vDelta, data.aoi);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_CalculateNetGapDot,
    testing::Values(
        DataFor_CalculateNetGapDot{
            .relativeNetDistance = -1.0_m,
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = 0.0},
        DataFor_CalculateNetGapDot{
            .relativeNetDistance = 30.0_m,
            .aoi = AreaOfInterest::EGO_REAR,
            .expectedResult = -1.0},
        DataFor_CalculateNetGapDot{
            .relativeNetDistance = -1.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 0.0},
        DataFor_CalculateNetGapDot{
            .relativeNetDistance = 40.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 1.6},
        DataFor_CalculateNetGapDot{
            .relativeNetDistance = 40.0_m,
            .aoi = AreaOfInterest::DISTRACTION,
            .expectedResult = 0.0}));

/*******************************
 * CHECK LaneExistenceToString *
 *******************************/

struct DataFor_LaneExistenceToString
{
  LaneExistence laneExistence;
  std::string expectedResult;
};

class ScmCommons_LaneExistenceToString : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_LaneExistenceToString>
{
};

TEST_P(ScmCommons_LaneExistenceToString, Check_LaneExistenceToString)
{
  DataFor_LaneExistenceToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.LaneExistenceToString(data.laneExistence);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_LaneExistenceToString,
    testing::Values(
        DataFor_LaneExistenceToString{
            .laneExistence = LaneExistence::FALSE,
            .expectedResult = "FALSE"},
        DataFor_LaneExistenceToString{
            .laneExistence = LaneExistence::TRUE,
            .expectedResult = "TRUE"},
        DataFor_LaneExistenceToString{
            .laneExistence = LaneExistence::UNKNOWN,
            .expectedResult = "UNKNOWN"}));

/********************************
 * CHECK StringToAreaOfInterest *
 ********************************/

struct DataFor_StringToAreaOfInterest
{
  std::string string;
  AreaOfInterest expectedResult;
};

class ScmCommons_StringToAreaOfInterest : public ::testing::Test,
                                          public ::testing::WithParamInterface<DataFor_StringToAreaOfInterest>
{
};

TEST_P(ScmCommons_StringToAreaOfInterest, Check_StringToAreaOfInterest)
{
  DataFor_StringToAreaOfInterest data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.StringToAreaOfInterest(data.string);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_StringToAreaOfInterest,
    testing::Values(
        DataFor_StringToAreaOfInterest{
            .string = "EGO_FRONT",
            .expectedResult = AreaOfInterest::EGO_FRONT},
        DataFor_StringToAreaOfInterest{
            .string = "EGO_FRONT_FAR",
            .expectedResult = AreaOfInterest::EGO_FRONT_FAR},
        DataFor_StringToAreaOfInterest{
            .string = "EGO_REAR",
            .expectedResult = AreaOfInterest::EGO_REAR},
        DataFor_StringToAreaOfInterest{
            .string = "HUD",
            .expectedResult = AreaOfInterest::HUD},
        DataFor_StringToAreaOfInterest{
            .string = "INFOTAINMENT",
            .expectedResult = AreaOfInterest::INFOTAINMENT},
        DataFor_StringToAreaOfInterest{
            .string = "INSTRUMENT_CLUSTER",
            .expectedResult = AreaOfInterest::INSTRUMENT_CLUSTER},
        DataFor_StringToAreaOfInterest{
            .string = "LEFT_FRONT",
            .expectedResult = AreaOfInterest::LEFT_FRONT},
        DataFor_StringToAreaOfInterest{
            .string = "LEFT_FRONT_FAR",
            .expectedResult = AreaOfInterest::LEFT_FRONT_FAR},
        DataFor_StringToAreaOfInterest{
            .string = "LEFT_REAR",
            .expectedResult = AreaOfInterest::LEFT_REAR},
        DataFor_StringToAreaOfInterest{
            .string = "LEFT_SIDE",
            .expectedResult = AreaOfInterest::LEFT_SIDE},
        DataFor_StringToAreaOfInterest{
            .string = "RIGHT_FRONT",
            .expectedResult = AreaOfInterest::RIGHT_FRONT},
        DataFor_StringToAreaOfInterest{
            .string = "RIGHT_FRONT_FAR",
            .expectedResult = AreaOfInterest::RIGHT_FRONT_FAR},
        DataFor_StringToAreaOfInterest{
            .string = "RIGHT_REAR",
            .expectedResult = AreaOfInterest::RIGHT_REAR},
        DataFor_StringToAreaOfInterest{
            .string = "RIGHT_SIDE",
            .expectedResult = AreaOfInterest::RIGHT_SIDE}));

/*******************************************
 * CHECK IsSurroundingAreaOfInterestVector *
 *******************************************/

struct DataFor_IsSurroundingAreaOfInterestVector
{
  AreaOfInterest aoiToCheck;
  bool expectedResult;
};

class ScmCommons_IsSurroundingAreaOfInterestVector : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_IsSurroundingAreaOfInterestVector>
{
};

TEST_P(ScmCommons_IsSurroundingAreaOfInterestVector, Check_IsSurroundingAreaOfInterestVector)
{
  DataFor_IsSurroundingAreaOfInterestVector data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.IsSurroundingAreaOfInterestVector(data.aoiToCheck);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_IsSurroundingAreaOfInterestVector,
    testing::Values(
        DataFor_IsSurroundingAreaOfInterestVector{
            .aoiToCheck = AreaOfInterest::EGO_FRONT,
            .expectedResult = true},
        DataFor_IsSurroundingAreaOfInterestVector{
            .aoiToCheck = AreaOfInterest::DISTRACTION,
            .expectedResult = false}));

/**********************
 * CHECK AoisToString *
 **********************/

struct DataFor_AoisToString
{
  std::vector<AreaOfInterest> aois;
  std::string expectedResult;
};

class ScmCommons_AoisToString : public ::testing::Test,
                                public ::testing::WithParamInterface<DataFor_AoisToString>
{
};

TEST_P(ScmCommons_AoisToString, Check_AoisToString)
{
  DataFor_AoisToString data = GetParam();
  ScmCommons testScmCommons;

  auto result = testScmCommons.AoisToString(data.aois);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_AoisToString,
    testing::Values(
        DataFor_AoisToString{
            .aois = {},
            .expectedResult = ""},
        DataFor_AoisToString{
            .aois = {AreaOfInterest::EGO_FRONT},
            .expectedResult = "EGO_FRONT"}));

/****************************
 * CHECK RelativeLaneToSide *
 ****************************/

struct DataFor_RelativeLaneToSide
{
  RelativeLane relativeLane;
  Side expectedResult;
};

class ScmCommons_RelativeLaneToSide : public ::testing::Test,
                                      public ::testing::WithParamInterface<DataFor_RelativeLaneToSide>
{
};

TEST_P(ScmCommons_RelativeLaneToSide, Check_RelativeLaneToSide)
{
  DataFor_RelativeLaneToSide data = GetParam();
  ScmCommons testScmCommons;

  if (data.relativeLane == RelativeLane::EGO)
  {
    EXPECT_THROW(testScmCommons.RelativeLaneToSide(data.relativeLane), std::runtime_error);
  }
  else
  {
    auto result = testScmCommons.RelativeLaneToSide(data.relativeLane);
    ASSERT_EQ(result, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_RelativeLaneToSide,
    testing::Values(
        DataFor_RelativeLaneToSide{
            .relativeLane = RelativeLane::RIGHT,
            .expectedResult = Side::Right},
        DataFor_RelativeLaneToSide{
            .relativeLane = RelativeLane::LEFT,
            .expectedResult = Side::Left},
        DataFor_RelativeLaneToSide{
            .relativeLane = RelativeLane::EGO,
            .expectedResult = Side::Left}));

/*************************
 * CHECK ToIntegerString *
 *************************/

struct DataFor_ToIntegerString
{
  std::vector<MesoscopicSituation> mesoscopicSituations;
  std::string expectedResult;
};

class ScmCommons_ToIntegerString : public ::testing::Test,
                                   public ::testing::WithParamInterface<DataFor_ToIntegerString>
{
};

TEST_P(ScmCommons_ToIntegerString, Check_ToIntegerString)
{
  DataFor_ToIntegerString data = GetParam();
  auto result = ScmCommons::ToIntegerString(data.mesoscopicSituations);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmCommons_ToIntegerString,
    testing::Values(
        DataFor_ToIntegerString{
            .mesoscopicSituations = {MesoscopicSituation::FREE_DRIVING},
            .expectedResult = "0;"},
        DataFor_ToIntegerString{
            .mesoscopicSituations = {MesoscopicSituation::FREE_DRIVING, MesoscopicSituation::QUEUED_TRAFFIC},
            .expectedResult = "0;3;"}));