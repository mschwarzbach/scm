/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeLaneChangeBehavior.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "Fakes/FakeIgnoringOuterLaneOvertakingProhibition.h"
#include "FeatureExtractor.h"
#include "SurroundingVehicles/SurroundingVehicleQueryInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "module/driver/src/MentalModelInterface.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct FeatureExtractorTester
{
  class FeatureExtractorUnderTest : FeatureExtractor
  {
  public:
    template <typename... Args>
    FeatureExtractorUnderTest(Args&&... args)
        : FeatureExtractor{std::forward<Args>(args)...} {};

    using FeatureExtractor::AbleToPass;
    using FeatureExtractor::AnticipatedAbleToPass;
    using FeatureExtractor::AnticipatedTimeToLaneCrossing;
    using FeatureExtractor::CantPreventCollisionCourse;
    using FeatureExtractor::CanVehicleCauseCollisionCourseForNonSideAreas;
    using FeatureExtractor::CheckLaneChange;
    using FeatureExtractor::DoesOuterLaneOvertakingProhibitionApply;
    using FeatureExtractor::DoesVehicleSurroundingFit;
    using FeatureExtractor::GetPassingTime;
    using FeatureExtractor::GetTimeAtTargetSpeedInLane;
    using FeatureExtractor::HasActualLaneCrossingConflict;
    using FeatureExtractor::HasAnticipatedLaneCrossingConflict;
    using FeatureExtractor::HasDrivableSuccessor;
    using FeatureExtractor::HasIntentionalLaneCrossingConflict;
    using FeatureExtractor::HasJamVelocity;
    using FeatureExtractor::HasSuspiciousBehaviour;
    using FeatureExtractor::IsAoiRelevantForOuterLaneOvertaking;
    using FeatureExtractor::IsAvoidingLaneNextToSuspiciousSideVehicles;
    using FeatureExtractor::IsCollisionCourseDetected;
    using FeatureExtractor::IsCollisionCourseDetectedNonSideArea;
    using FeatureExtractor::IsCollisionCourseDetectedSideArea;
    using FeatureExtractor::IsDeceleratingDueToSuspiciousSideVehicles;
    using FeatureExtractor::IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition;
    using FeatureExtractor::IsEvadingEndOfLane;
    using FeatureExtractor::IsExceptionallySlow;
    using FeatureExtractor::IsInfluencingDistanceViolated;
    using FeatureExtractor::IsLaneChangePermittedDueToLaneMarkings;
    using FeatureExtractor::IsLaneChangePermittedDueToLaneMarkingsForAoi;
    using FeatureExtractor::IsLaneChangeSafe;
    using FeatureExtractor::IsLaneSafe;
    using FeatureExtractor::IsMinimumFollowingDistanceViolated;
    using FeatureExtractor::IsMinimumFollowingDistanceViolatedWhileCooperating;
    using FeatureExtractor::IsNearEnoughForFollowing;
    using FeatureExtractor::IsObstacle;
    using FeatureExtractor::IsOnEntryLane;
    using FeatureExtractor::IsOnEntryOrExitLane;
    using FeatureExtractor::IsSideLaneSafe;
    using FeatureExtractor::IsSideLaneSeparateRoadway;
    using FeatureExtractor::IsUnfavorable;
    using FeatureExtractor::IsVehicleInSideLane;
    using FeatureExtractor::SetMentalCalculations;
    using FeatureExtractor::SetMentalModel;
    using FeatureExtractor::SetOuterLaneOvertakingProhibitionQuota;
    using FeatureExtractor::SetSurroundingVehicleQueryFactory;
    using FeatureExtractor::WillEgoLeaveLaneBeforeCollision;
  };

  FeatureExtractorTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeMentalCalculations{},
        featureExtractor()
  {
    featureExtractor.SetMentalModel(fakeMentalModel);
    featureExtractor.SetMentalCalculations(fakeMentalCalculations);
    featureExtractor.SetSurroundingVehicleQueryFactory(&fakeQueryFactory);
    fakeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
    ON_CALL(fakeQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  FeatureExtractorUnderTest featureExtractor;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeQueryFactory;
  std::shared_ptr<NiceMock<FakeSurroundingVehicleQuery>> fakeVehicleQuery;
};

/*******************************************
 * CHECK FEATURE IsCollisionCourseDetected *
 *******************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_FeatureIsCollisionCourseDetected
{
  AreaOfInterest input_ObservedAoi;  // AOI to be observed
  units::time::second_t input_ObservedTtc;  // Extrapolated indicator state of the observed vehicle
  double input_ObservedTauDot;       // Extrapolated tau dot of the observed vehicle
  double mock_SpeedTowardsEgoLane;   // Return value for the mocked function 'GetExtrapolatedLateralSpeedTowardsEgoLane'
  bool expected_Result;              // Expecten return value of the function 'HasIntentionalLaneCrossingConflict'
};

class FeatureExtractor_FeatureIsCollisionCourseDetectedCheck : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_FeatureIsCollisionCourseDetected>
{
};

TEST_P(FeatureExtractor_FeatureIsCollisionCourseDetectedCheck, FeatureExtractor_Check_Feature_IsCollisionCourseDetected)
{
  // Get resources for testing
  FeatureExtractorTester TEST_HELPER;
  DataFor_FeatureIsCollisionCourseDetected data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_ObservedAoi));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.input_ObservedTtc));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(data.input_ObservedTauDot));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::EGO));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.input_ObservedAoi, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(data.input_ObservedAoi, _)).WillByDefault(Return(0._mps));  // For values >= 0 this will lead to the desired result of 'IsExceedingLateralMotionThresholdAwayFromEgoLane'. For values < 0 the previous mentioned data will need to be provided.
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  // Execute Test
  bool result = TEST_HELPER.featureExtractor.IsCollisionCourseDetected(&fakeVehicle);

  // Compare actual values with expected values
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureIsCollisionCourseDetectedCheck,
    testing::Values(
        // Testcase 1: Ego rear aoi; observed vehicle is approaching while lane keeping
        DataFor_FeatureIsCollisionCourseDetected{
            .input_ObservedAoi = AreaOfInterest::EGO_REAR,
            .input_ObservedTtc = 3._s,
            .input_ObservedTauDot = -0.8,
            .mock_SpeedTowardsEgoLane = 0.,
            .expected_Result = true},
        // Testcase 2: Right side aoi, observed vehicle is not aproaching while lane keeping
        DataFor_FeatureIsCollisionCourseDetected{
            .input_ObservedAoi = AreaOfInterest::EGO_REAR,
            .input_ObservedTtc = -1._s,
            .input_ObservedTauDot = 0.1,
            .mock_SpeedTowardsEgoLane = 0.,
            .expected_Result = false}));

/***************************************
 * CHECK HasActualLaneCrossingConflict *
 ***************************************/
class FeatureExtractor_HasActualLaneCrossingConflict : public ::testing::Test
{
};

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleNotToTheSide_IsObstacle_ReturnsObstructionOverlapping)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20.0_mps));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(_)).WillByDefault(Return(100._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20.0_mps));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, IsStatic).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetRelativeNetDistance).WillByDefault(Return(20.0_m));
  ON_CALL(fakeVehicle, GetLongitudinalVelocity).WillByDefault(Return(5.0_mps));
  ON_CALL(fakeVehicle, GetTtc).WillByDefault(Return(5.0_s));

  ObstructionScm obstruction{// is overlapping
                             1.0_m,
                             1.0_m,
                             0.0_m};

  EXPECT_CALL(fakeVehicle, GetLateralObstruction).WillOnce(Return(obstruction));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleNotToTheSide_IsNotChangingIntoEgoLane_ReturnsFalse)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20.0_mps));

  NiceMock<FakeSurroundingVehicleQueryFactory> fakeQueryFactory;
  auto fakeVehicleQuery = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(fakeQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));
  ON_CALL(*fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  ObstructionScm combinedObstructionData{0_m, 1_m, 2_m};
  combinedObstructionData.isOverlapping = false;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, IsStatic).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity).WillByDefault(Return(units::velocity::meters_per_second_t(100_kph)));  // avoid detection as obstacle
  ON_CALL(fakeVehicle, GetLateralObstruction).WillByDefault(Return(combinedObstructionData));  // avoid detection as obstacle

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_FALSE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleNotToTheSide_TtcBelowZero_ReturnsFalse)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(_)).WillByDefault(Return(1.0_m));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, IsStatic).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity).WillByDefault(Return(units::velocity::meters_per_second_t(100_kph)));  // avoid detection as obstacle
  ON_CALL(fakeVehicle, GetAssignedAoi).WillByDefault(Return(AreaOfInterest::EGO_FRONT));  // TODO Remove: This is only needed for IsNearEnoughForFollowing with aois
  ON_CALL(fakeVehicle, GetTtc).WillByDefault(Return(-10.0_s));
  ON_CALL(fakeVehicle, GetRelativeNetDistance).WillByDefault(Return(100.0_m));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_FALSE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleNotToTheSide_ChangingIntoEgoLane_NearEnoughForFollowing_TtcAboveZero_ReturnCannotClearObstructionBeforeCollision)
{
  FeatureExtractorTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(_)).WillByDefault(Return(1.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(20.0_mps));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, IsStatic).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity).WillByDefault(Return(units::velocity::meters_per_second_t(100_kph)));  // avoid detection as obstacle
  ON_CALL(fakeVehicle, GetAssignedAoi).WillByDefault(Return(AreaOfInterest::EGO_FRONT));  // TODO Remove: This is only needed for IsNearEnoughForFollowing with aois
  ON_CALL(fakeVehicle, GetTtc).WillByDefault(Return(10.0_s));
  ON_CALL(fakeVehicle, GetRelativeNetDistance).WillByDefault(Return(0.5_m));

  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(true));
  EXPECT_CALL(*TEST_HELPER.fakeVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision).WillOnce(Return(true));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_ChangingIntoEgoLane_ReturnTrue)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::LEFT));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_NotChangingIntoEgoLane_EgoChangingLanesToLeft_VehicleOnLeftLeftLane_ReturnIsSideSideVehicleChangingIntoSide)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection).WillByDefault(Return(SurroundingLane::LEFT));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::LEFTLEFT));

  EXPECT_CALL(*TEST_HELPER.fakeVehicleQuery, IsSideSideVehicleChangingIntoSide).WillOnce(Return(true));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_NotChangingIntoEgoLane_EgoChangingLanesToRight_VehicleOnRightRightLane_ReturnIsSideSideVehicleChangingIntoSide)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection).WillByDefault(Return(SurroundingLane::RIGHT));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::RIGHTRIGHT));

  EXPECT_CALL(*TEST_HELPER.fakeVehicleQuery, IsSideSideVehicleChangingIntoSide).WillOnce(Return(true));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_NotChangingIntoEgoLane_EgoChangingLanesToLeft_VehicleOnLeftLane_ReturnCannotClearLongObstruction)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection).WillByDefault(Return(SurroundingLane::LEFT));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::LEFT));

  EXPECT_CALL(*TEST_HELPER.fakeVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision).WillOnce(Return(true));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_NotChangingIntoEgoLane_EgoChangingLanesToRight_VehicleOnRightLane_ReturnCannotClearLongObstruction)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection).WillByDefault(Return(SurroundingLane::RIGHT));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::RIGHT));

  EXPECT_CALL(*TEST_HELPER.fakeVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision).WillOnce(Return(true));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_HasActualLaneCrossingConflict, VehicleToTheSide_NotChangingIntoEgoLane_EgoNotChangingLanes_ReturnFalse)
{
  FeatureExtractorTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection).WillByDefault(Return(SurroundingLane::EGO));
  ON_CALL(*TEST_HELPER.fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, ToTheSideOfEgo).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetLaneOfPerception).WillByDefault(Return(RelativeLane::RIGHT));

  const auto result{TEST_HELPER.featureExtractor.HasActualLaneCrossingConflict(fakeVehicle)};
  ASSERT_FALSE(result);
}

/********************************
 * CHECK FEATURE GetPassingTime *
 ********************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetPassingTime
{
  units::velocity::meters_per_second_t input_DeltaVelocity;
  units::time::second_t expected_Result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GetPassingTime& obj)
  {
    return os
           << "input_DeltaVelocity (double): " << obj.input_DeltaVelocity
           << " | expected_Result (double): " << obj.expected_Result;
  }
};

class FeatureExtractor_FeatureGetPassingTime : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_GetPassingTime>
{
};

TEST_P(FeatureExtractor_FeatureGetPassingTime, FeatureExtractor_Check_Feature_GetPassingTime)
{
  // Get resources for testing
  FeatureExtractorTester TEST_HELPER;
  DataFor_GetPassingTime data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;

  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(3.0_s));
  ON_CALL(fakeVehicle, GetLength()).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  // Execute Test
  auto result = TEST_HELPER.featureExtractor.GetPassingTime(&fakeVehicle, data.input_DeltaVelocity);

  // Compare actual values with expected values
  if (units::math::isinf(data.expected_Result))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expected_Result);
  }
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureGetPassingTime,
    testing::Values(
        DataFor_GetPassingTime{
            .input_DeltaVelocity = 0._mps,
            .expected_Result = ScmDefinitions::INF_TIME},  // Case 1: DeltaV = 0
        DataFor_GetPassingTime{
            .input_DeltaVelocity = 1._mps,
            .expected_Result = 13.5_s},  // Case 2: DeltaV > 0
        DataFor_GetPassingTime{
            .input_DeltaVelocity = -1._mps,
            .expected_Result = 13.5_s}));  // Case 2: DeltaV < 0

/****************************
 * CHECK FEATURE AbleToPass *
 ****************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AbleToPass
{
  AreaOfInterest input_AOI;
  bool mock_PerceivedTtlGreaterPassingTime;
  units::velocity::meters_per_second_t mock_VelocityTowardsEgoLane;
  units::length::meter_t mock_DistanceTowardsEgoLane;
  RelativeLane lane;
  bool expected_Result;
};

class FeatureExtractor_FeatureAbleToPass : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_AbleToPass>
{
};

TEST_P(FeatureExtractor_FeatureAbleToPass, FeatureExtractor_CheckFeature_AbleToPass)
{
  DataFor_AbleToPass data = GetParam();
  FeatureExtractorTester TEST_HELPER;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(3.0_s));
  ON_CALL(fakeVehicle, GetLength()).WillByDefault(Return(5.0_m));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_AOI));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(_, _)).WillByDefault(Return(data.mock_VelocityTowardsEgoLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceTowardsEgoLane(_, _)).WillByDefault(Return(data.mock_DistanceTowardsEgoLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  static auto constexpr deltaVelocity = 1.0_mps;

  // Execute Test
  bool result = TEST_HELPER.featureExtractor.AbleToPass(&fakeVehicle, deltaVelocity);

  // Compare actual values with expected values
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureAbleToPass,
    testing::Values(
        DataFor_AbleToPass{
            .input_AOI = AreaOfInterest::LEFT_FRONT,
            .mock_PerceivedTtlGreaterPassingTime = true,
            .mock_VelocityTowardsEgoLane = 1.0_mps,
            .mock_DistanceTowardsEgoLane = 15.0_m,
            .lane = RelativeLane::LEFT,
            .expected_Result = true},  // Case 1: Passing time smaller than anticipated time to lane change
        DataFor_AbleToPass{
            .input_AOI = AreaOfInterest::LEFT_FRONT,
            .mock_PerceivedTtlGreaterPassingTime = false,
            .mock_VelocityTowardsEgoLane = 10.0_mps,
            .mock_DistanceTowardsEgoLane = 10.0_m,
            .lane = RelativeLane::EGO,
            .expected_Result = false}));  // Case 2: Passing time greater than anticipated time to lane change

/*************************************************
 * CHECK DoesOuterLaneOvertakingProhibitionApply *
 *************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency
{
  double input_urgencyFactor;
  double mock_Gap;
  units::length::meter_t mock_GetMinDistance;
  bool isNonVehicle;
  int vehicleId;
  int mergeRegulateId;
  bool expected_Result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency& obj)
  {
    return os
           << "input_urgencyFactor (double): " << obj.input_urgencyFactor
           << " | mock_Gap (double): " << obj.mock_Gap
           << " | mock_MinTimeHeadway (double): " << obj.mock_GetMinDistance
           << " | expected_Result (bool): " << ScmCommons::BooleanToString(obj.expected_Result);
  }
};

class FeatureExtractor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgencyCheck : public ::testing::Test,
                                                                                    public ::testing::WithParamInterface<DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency>
{
};

TEST_P(FeatureExtractor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgencyCheck, FeatureExtractor_Check_Feature_IsMinimumFollowingDistanceViolatedUnderUrgency)
{
  // Get resources for testing
  FeatureExtractorTester TEST_HELPER;
  DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency data = GetParam();
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  // Set up test
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(data.mock_GetMinDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(data.mergeRegulateId));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(100_m));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(data.vehicleId));

  // TODO also test the case that ego is not currently merging 
  Merge::Gap gap{&fakeVehicle, nullptr};
  gap.distance = 10.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(gap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(
    LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)
  ));

  // Run test
  bool result = false;
  if (data.isNonVehicle)
  {
    result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolated(nullptr, data.input_urgencyFactor);
  }
  else
  {
    result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolated(&fakeVehicle, data.input_urgencyFactor);
  }
  // Evaluate results
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgencyCheck,
    testing::Values(
        DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency{
            .input_urgencyFactor = 1.,
            .mock_Gap = 5.,
            .mock_GetMinDistance = 10._m,
            .isNonVehicle = true,
            .vehicleId = 2,
            .mergeRegulateId = -1,
            .expected_Result = false},
        DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency{
            .input_urgencyFactor = .4,
            .mock_Gap = 5.,
            .mock_GetMinDistance = 10._m,
            .isNonVehicle = true,
            .vehicleId = 2,
            .mergeRegulateId = -1,
            .expected_Result = false},
        DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency{
            .input_urgencyFactor = 1.,
            .mock_Gap = 10.,
            .mock_GetMinDistance = 200._m,
            .isNonVehicle = false,
            .vehicleId = 2,
            .mergeRegulateId = -1,
            .expected_Result = true},
        DataFor_FeatureIsMinimumFollowingDistanceViolatedUnderUrgency{
            .input_urgencyFactor = 1.,
            .mock_Gap = 10.,
            .mock_GetMinDistance = 200._m,
            .isNonVehicle = false,
            .vehicleId = 2,
            .mergeRegulateId = 2,
            .expected_Result = false}));

/*************************************
 * CHECK FEATURE IsExceptionallySlow *
 *************************************/

// \brief Data table for definition of individual test cases
struct DataFor_FeatureIsExceptionallySlow
{
  AreaOfInterest input_ObservedAoi;  // AOI to be observed
  units::velocity::meters_per_second_t input_vEgo;  // estimated ego velocity
  units::velocity::meters_per_second_t input_vAoi;  // estimated aoi agent velocity
  units::velocity::meters_per_second_t input_meanLaneVelocity;  // mean velocity of vehicles on aoi lane
  bool input_isFarVehiclePresent;    // are there vehicles in LEFT/RIGHT_FAR
  units::velocity::meters_per_second_t input_vAoiFar;  // estimated aoi_Far agent velocity
  units::velocity::meters_per_second_t input_legalVelocity;  // legal speed limit on aoi lane
  bool expected_Result;              // expected return value of the function 'IsExceptionallySlow'
};

class FeatureExtractor_FeatureIsExceptionallySlowCheck : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_FeatureIsExceptionallySlow>
{
};

TEST_P(FeatureExtractor_FeatureIsExceptionallySlowCheck, FeatureExtractor_Check_Feature_IsExceptionallySlow)
{
  // Get resources for testing
  FeatureExtractorTester TEST_HELPER;
  DataFor_FeatureIsExceptionallySlow data = GetParam();

  // Set conditions for test scenario

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_ObservedAoi));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(data.input_vAoi));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalLeft()).WillByDefault(Return(data.input_legalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalRight()).WillByDefault(Return(data.input_legalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(data.input_meanLaneVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocityLaneLeft()).WillByDefault(Return(data.input_meanLaneVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocityLaneRight()).WillByDefault(Return(data.input_meanLaneVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.input_vEgo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.input_ObservedAoi, _)).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, DetermineIndexOfSideObject(_, _, _)).WillByDefault(Return(0));

  AreaOfInterest farAoi = AreaOfInterest::EGO_FRONT_FAR;
  if (data.input_ObservedAoi == AreaOfInterest::LEFT_FRONT)
  {
    farAoi = AreaOfInterest::LEFT_FRONT_FAR;
  }
  else if (data.input_ObservedAoi == AreaOfInterest::RIGHT_FRONT)
  {
    farAoi = AreaOfInterest::RIGHT_FRONT_FAR;
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(farAoi, _)).WillByDefault(Return(data.input_isFarVehiclePresent));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(farAoi, _)).WillByDefault(Return(data.input_vAoiFar));
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  // Execute Test
  bool result = TEST_HELPER.featureExtractor.IsExceptionallySlow(&fakeVehicle);

  // Compare actual values with expected values
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureIsExceptionallySlowCheck,
    testing::Values(
        // Case 1: expected: true since vAoi << meanLaneVelocity
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_vEgo = 20._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = false,
            .input_vAoiFar = 0._mps,
            .input_legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_Result = true},
        // Case 2: expected: false almost equivalent to Case 1 but vEgo is slow too so no need to watch out for aoi agent
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_vEgo = 12._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = false,
            .input_vAoiFar = 0._mps,
            .input_legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_Result = false},
        // Case 3: expected: false equivalent to Case 1 but target is in Side so too late to react
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::LEFT_SIDE,
            .input_vEgo = 20._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = false,
            .input_vAoiFar = 0._mps,
            .input_legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_Result = false},
        // Case 4: expected: false almost equivalent to Case 1 but vAoi is jsut following traffic rules low vLegal on its lane
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_vEgo = 20._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = false,
            .input_vAoiFar = 0._mps,
            .input_legalVelocity = 40._kph,
            .expected_Result = false},
        // Case 5: expected: false almost equivalent to Case 1 but aoi has slow leading vehicle so it cant go faster even if it wanted too (traffic jam)
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_vEgo = 20._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = true,
            .input_vAoiFar = 9._mps,
            .input_legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_Result = false},
        // Case 6: expected: true almost equivalent to Case 1 but with fast vehicle in FAR_AOI
        DataFor_FeatureIsExceptionallySlow{
            .input_ObservedAoi = AreaOfInterest::RIGHT_FRONT,
            .input_vEgo = 20._mps,
            .input_vAoi = 8._mps,
            .input_meanLaneVelocity = 25._mps,
            .input_isFarVehiclePresent = true,
            .input_vAoiFar = 22._mps,
            .input_legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_Result = true}));

/****************************************************
 * CHECK FEATURE HasIntentionalLaneCrossingConflict *
 ****************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_FeatureHasIntentionalLaneCrossingConflict
{
  AreaOfInterest input_ObservedAoi;                         // AOI to be observed
  scm::LightState::Indicator input_ObservedIndicatorState;  // Extrapolated indicator state of the observed vehicle
  bool expected_Result;                                     // Expecten return value of the function 'HasIntentionalLaneCrossingConflict'

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_FeatureHasIntentionalLaneCrossingConflict& obj)
  {
    return os
           << "input_ObservedAoi (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_ObservedAoi)
           << " | input_ObservedIndicatorState (IndicatorState): " << ScmCommons::IndicatorToString(obj.input_ObservedIndicatorState)
           << " | expected_Result (bool): " << ScmCommons::BooleanToString(obj.expected_Result);
  }
};

class FeatureExtractor_FeatureHasIntentionalLaneCrossingConflictCheck : public ::testing::Test,
                                                                        public ::testing::WithParamInterface<DataFor_FeatureHasIntentionalLaneCrossingConflict>
{
};

TEST_P(FeatureExtractor_FeatureHasIntentionalLaneCrossingConflictCheck, FeatureExtractor_Check_Feature_HasIntentionalLaneCrossingConflict)
{
  // Get resources for testing
  FeatureExtractorTester TEST_HELPER;
  DataFor_FeatureHasIntentionalLaneCrossingConflict data = GetParam();

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_ObservedAoi));
  ON_CALL(fakeVehicle, GetIndicatorState()).WillByDefault(Return(data.input_ObservedIndicatorState));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ObjectInformationScmExtended object;
  object.indicatorState = data.input_ObservedIndicatorState;
  std::vector<ObjectInformationScmExtended> objectInformations{object};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetSideObjectVector(_)).WillByDefault(Return(&objectInformations));
  // Set conditions for test scenario

  // Execute Test
  bool result = TEST_HELPER.featureExtractor.HasIntentionalLaneCrossingConflict(&fakeVehicle);  // fail 1,2,3 (nicht 0)

  // Compare actual values with expected values
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_FeatureHasIntentionalLaneCrossingConflictCheck,
    testing::Values(
        // Testcase 1: Left side aoi; observed right indicator
        DataFor_FeatureHasIntentionalLaneCrossingConflict{
            .input_ObservedAoi = AreaOfInterest::LEFT_SIDE,
            .input_ObservedIndicatorState = scm::LightState::Indicator::Right,
            .expected_Result = true},
        // Testcase 2: Right side aoi, observed left indicator
        DataFor_FeatureHasIntentionalLaneCrossingConflict{
            .input_ObservedAoi = AreaOfInterest::RIGHT_SIDE,
            .input_ObservedIndicatorState = scm::LightState::Indicator::Left,
            .expected_Result = true},
        // Testcase 3: Right side aoi, observed right indicator
        DataFor_FeatureHasIntentionalLaneCrossingConflict{
            .input_ObservedAoi = AreaOfInterest::RIGHT_SIDE,
            .input_ObservedIndicatorState = scm::LightState::Indicator::Right,
            .expected_Result = false},
        // Testcase 4: Right front aoi, observed left indicator
        DataFor_FeatureHasIntentionalLaneCrossingConflict{
            .input_ObservedAoi = AreaOfInterest::RIGHT_FRONT,
            .input_ObservedIndicatorState = scm::LightState::Indicator::Left,
            .expected_Result = true},
        // Testcase 5: Left front aoi, no observed indicator
        DataFor_FeatureHasIntentionalLaneCrossingConflict{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_ObservedIndicatorState = scm::LightState::Indicator::Off,
            .expected_Result = false}));

/************************************************
 * CHECK IsLaneChangePermittedDueToLaneMarkings *
 ************************************************/

// \brief Data table for definition of individual test cases
struct DataFor_IsLaneChangePermittedDueToLaneMarkings
{
  Side input_Side;
  bool input_isLaneChangeProhibitionIgnored;
  bool input_doesLaneMarkingBrokenBoldPreventLaneChange;
  std::vector<scm::LaneMarking::Type> input_LaneMarkingsType;  // # of entry corresponds with # of entry of vector below
  std::vector<units::length::meter_t> input_LaneMakingsRelativeDistance;  // # of entry corresponds with # of entry of vector above
  std::vector<units::length::meter_t> input_LaneMakingsWidth;             // # of entry corresponds with # of entry of vector above
  bool result_Permitted;
};

class FeatureExtractor_IsLaneChangePermittedDueToLaneMarkings : public ::testing::Test,
                                                                public ::testing::WithParamInterface<DataFor_IsLaneChangePermittedDueToLaneMarkings>
{
};

TEST_P(FeatureExtractor_IsLaneChangePermittedDueToLaneMarkings, FeatureExtractor_CheckFunction_IsLaneChangePermittedDueToLaneMarkings)
{
  // Get resources for testing
  DataFor_IsLaneChangePermittedDueToLaneMarkings data = GetParam();
  FeatureExtractorTester TEST_HELPER;

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  // Set up tests
  MicroscopicCharacteristics microscopicCharacteristics;
  microscopicCharacteristics.UpdateOwnVehicleData()->isLaneChangeProhibitionIgnored = data.input_isLaneChangeProhibitionIgnored;

  std::vector<scm::LaneMarking::Entity> laneMarkings{};
  for (std::vector<scm::LaneMarking::Type>::size_type it = 0; it < data.input_LaneMarkingsType.size(); ++it)
  {
    scm::LaneMarking::Entity laneMarking;
    laneMarking.type = data.input_LaneMarkingsType.at(it);
    laneMarking.relativeStartDistance = data.input_LaneMakingsRelativeDistance.at(it);
    laneMarking.width = data.input_LaneMakingsWidth.at(it);

    laneMarkings.push_back(laneMarking);
  }

  InfrastructureCharacteristics infrastructureCharacteristics;
  infrastructureCharacteristics.UpdateGeometryInformation()->Close().laneType = scm::LaneType::Driving;
  infrastructureCharacteristics.UpdateTrafficRuleInformation()->Close().laneMarkingsLeft = laneMarkings;
  infrastructureCharacteristics.UpdateTrafficRuleInformation()->Close().laneMarkingsRight = laneMarkings;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&microscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructureCharacteristics));

  // Run test
  bool result = TEST_HELPER.featureExtractor.IsLaneChangePermittedDueToLaneMarkings(data.input_Side);

  // Evaluate results
  ASSERT_EQ(data.result_Permitted, result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsLaneChangePermittedDueToLaneMarkings,
    testing::Values(
        DataFor_IsLaneChangePermittedDueToLaneMarkings{
            .input_Side = Side::Left,
            .input_isLaneChangeProhibitionIgnored = false,
            .input_doesLaneMarkingBrokenBoldPreventLaneChange = false,
            .input_LaneMarkingsType = {scm::LaneMarking::Type::Broken},
            .input_LaneMakingsRelativeDistance = {0._m},
            .input_LaneMakingsWidth = {.15_m},
            .result_Permitted = true},
        DataFor_IsLaneChangePermittedDueToLaneMarkings{
            .input_Side = Side::Left,
            .input_isLaneChangeProhibitionIgnored = false,
            .input_doesLaneMarkingBrokenBoldPreventLaneChange = false,
            .input_LaneMarkingsType = {scm::LaneMarking::Type::Solid, scm::LaneMarking::Type::Broken},
            .input_LaneMakingsRelativeDistance = {0._m, 200._m},
            .input_LaneMakingsWidth = {.15_m, .15_m},
            .result_Permitted = false},
        DataFor_IsLaneChangePermittedDueToLaneMarkings{
            .input_Side = Side::Right,
            .input_isLaneChangeProhibitionIgnored = false,
            .input_doesLaneMarkingBrokenBoldPreventLaneChange = true,
            .input_LaneMarkingsType = {scm::LaneMarking::Type::Solid},
            .input_LaneMakingsRelativeDistance = {0._m},
            .input_LaneMakingsWidth = {.30_m},
            .result_Permitted = false}));

/*******************************************
 * DataFor_WillEgoLeaveLaneBeforeCollision *
 *******************************************/

// \brief Data table for definition of individual test cases
struct DataFor_WillEgoLeaveLaneBeforeCollision
{
  AreaOfInterest input_ObservedAoi;  // AOI to be observed
  units::time::second_t input_timeToCollision;     // Time to collision
  units::time::second_t input_timeToLeaveEgoLane;  // Time to leave ego lane
  RelativeLane lane;
  bool changingIntoEgoLane;
  bool expected_Result;  // Expecten return value of the function 'WillEgoLeaveLaneBeforeCollision'
};

class FeatureExtractor_WillEgoLeaveLaneBeforeCollision : public ::testing::Test,
                                                         public ::testing::WithParamInterface<DataFor_WillEgoLeaveLaneBeforeCollision>
{
};

TEST_P(FeatureExtractor_WillEgoLeaveLaneBeforeCollision, FeatureExtractor_CheckFunction_FeatureExtractor_WillEgoLeaveLaneBeforeCollision)
{
  DataFor_WillEgoLeaveLaneBeforeCollision data = GetParam();
  FeatureExtractorTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  ObstructionDynamics obstructionDynamics;
  obstructionDynamics.timeToLeave = data.input_timeToLeaveEgoLane;

  NiceMock<FakeSurroundingVehicleQueryFactory> fakeQueryFactory;
  auto fakeVehicleQuery = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(fakeQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));
  ON_CALL(*fakeVehicleQuery, GetLateralObstructionDynamics()).WillByDefault(Return(obstructionDynamics));
  ON_CALL(*fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(data.changingIntoEgoLane));

  TEST_HELPER.featureExtractor.SetSurroundingVehicleQueryFactory(&fakeQueryFactory);
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.input_timeToCollision));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_ObservedAoi));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EstimatedTimeToLeaveLane()).WillByDefault(Return(data.input_timeToLeaveEgoLane));

  auto result = TEST_HELPER.featureExtractor.WillEgoLeaveLaneBeforeCollision(&fakeVehicle);
  ASSERT_EQ(result, data.expected_Result);
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_WillEgoLeaveLaneBeforeCollision,
    testing::Values(
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_FRONT_FAR,
            .input_timeToCollision = 1.0_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::EGO,
            .changingIntoEgoLane = true,
            .expected_Result = true},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_FRONT_FAR,
            .input_timeToCollision = 1.0_s,
            .input_timeToLeaveEgoLane = 1.25_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = true,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_FRONT,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::EGO,
            .changingIntoEgoLane = true,
            .expected_Result = true},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_FRONT,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.5_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = true,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_REAR,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::EGO,
            .changingIntoEgoLane = true,
            .expected_Result = true},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::EGO_REAR,
            .input_timeToCollision = 0.0_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::LEFT_REAR,
            .input_timeToCollision = 1.0_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::LEFT_FRONT,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::LEFT_SIDE,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::RIGHT_REAR,
            .input_timeToCollision = 1.0_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::RIGHT_FRONT,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = false,
            .expected_Result = false},
        DataFor_WillEgoLeaveLaneBeforeCollision{
            .input_ObservedAoi = AreaOfInterest::RIGHT_SIDE,
            .input_timeToCollision = 1.25_s,
            .input_timeToLeaveEgoLane = 1.0_s,
            .lane = RelativeLane::LEFT,
            .changingIntoEgoLane = false,
            .expected_Result = false}));

/***********************************
 * CHECK DoesVehicleSurroundingFit *
 ***********************************/

/// \brief Data table
struct DataFor_DoesVehicleSurroundingFit
{
  AreaOfInterest input_aoiToEvaluate;
  bool result_expectedReturn;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DoesVehicleSurroundingFit& obj)
  {
    return os
           << "input_aoiToEvaluate (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.input_aoiToEvaluate)
           << " | result_expectedReturn (bool): " << ScmCommons::BooleanToString(obj.result_expectedReturn);
  }
};

class FeatureExtractor_DoesVehicleSurroundingFit : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_DoesVehicleSurroundingFit>
{
};

TEST_P(FeatureExtractor_DoesVehicleSurroundingFit, FeatureExtractor_CheckFunction_DoesVehicleSurroundingFit)
{
  // Create required classes for test
  FeatureExtractorTester TEST_HELPER;
  DataFor_DoesVehicleSurroundingFit data = GetParam();

  FakeLaneChangeBehavior laneChangeBehavior;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(laneChangeBehavior));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  // aoiToEvaluate specific inputs
  switch (data.input_aoiToEvaluate)
  {
    case AreaOfInterest::EGO_FRONT:
      ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(false));
      ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT_FAR, _)).WillByDefault(Return(false));
      ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, _)).WillByDefault(Return(false));
      ON_CALL(laneChangeBehavior, CalculateBaseLaneConvenience(SurroundingLane::RIGHT)).WillByDefault(Return(1.0));
      ON_CALL(laneChangeBehavior, CalculateBaseLaneConvenience(SurroundingLane::EGO)).WillByDefault(Return(0.8));
      break;

    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::RIGHT_FRONT:
      EXPECT_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).Times(1).WillRepeatedly(Return(true));

    default:
      break;
  }

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  // Call test
  double result = TEST_HELPER.featureExtractor.DoesVehicleSurroundingFit(data.input_aoiToEvaluate);

  // Evaluate results
  ASSERT_EQ(data.result_expectedReturn, result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_DoesVehicleSurroundingFit,
    testing::Values(
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::RIGHT_FRONT,
            .result_expectedReturn = true},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::RIGHT_FRONT_FAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::LEFT_FRONT_FAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::EGO_FRONT_FAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::EGO_REAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::LEFT_REAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::RIGHT_REAR,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::RIGHTRIGHT_SIDE,
            .result_expectedReturn = false},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::LEFT_FRONT,
            .result_expectedReturn = true},
        DataFor_DoesVehicleSurroundingFit{
            .input_aoiToEvaluate = AreaOfInterest::EGO_FRONT,
            .result_expectedReturn = true}));

/*******************************************************
 * CHECK CanVehicleCauseCollisionCourseForNonSideAreas *
 *******************************************************/
struct DataFor_CanVehicleCauseCollisionCourse
{
  units::time::second_t ttc;
  double tauDot;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CanVehicleCauseCollisionCourse& obj)
  {
    return os
           << "ttc (double): " << obj.ttc
           << "tauDot (double): " << obj.tauDot
           << " result (bool): " << ScmCommons::BooleanToString(obj.result);
  }
};

class FeatureExtractor_CanVehicleCauseCollisionCourse : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_CanVehicleCauseCollisionCourse>
{
};

TEST_P(FeatureExtractor_CanVehicleCauseCollisionCourse, FeatureExtractor_CheckFunction_CanVehicleCauseCollisionCourse)
{
  DataFor_CanVehicleCauseCollisionCourse data = GetParam();
  FeatureExtractorTester TEST_HELPER;
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.ttc));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(data.tauDot));

  bool result = TEST_HELPER.featureExtractor.CanVehicleCauseCollisionCourseForNonSideAreas(&fakeVehicle);
  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_CanVehicleCauseCollisionCourse,
    testing::Values(
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = 10.0_s,
            .tauDot = 1.0,
            .result = false},
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = 99.0_s,
            .tauDot = 1.0,
            .result = false},
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = -99.0_s,
            .tauDot = 1.0,
            .result = false},
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = 10.0_s,
            .tauDot = 1.0,
            .result = false},
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = 5.0_s,
            .tauDot = 1.0,
            .result = false},
        DataFor_CanVehicleCauseCollisionCourse{
            .ttc = 5.0_s,
            .tauDot = -1.0,
            .result = true}));

struct DataFor_IsCollisionCourseDetectedNonSideArea
{
  double tauDot;
  units::length::meter_t widthProjected;
  AreaOfInterest aoi;
  bool result;
};

/**********************************************
 * CHECK IsCollisionCourseDetectedNonSideArea *
 **********************************************/

class FeatureExtractor_IsCollisionCourseDetectedNonSideArea : public ::testing::Test,
                                                              public ::testing::WithParamInterface<DataFor_IsCollisionCourseDetectedNonSideArea>
{
};

TEST_P(FeatureExtractor_IsCollisionCourseDetectedNonSideArea, FeatureExtractor_CheckFunction_FeatureExtractor_IsCollisionCourseDetectedNonSideArea)
{
  DataFor_IsCollisionCourseDetectedNonSideArea data = GetParam();
  FeatureExtractorTester TEST_HELPER;
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(10.0_s));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(data.tauDot));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(-5.0_m));
  ON_CALL(fakeVehicle, GetTtcThresholdLooming()).WillByDefault(Return(0.1));

  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::EGO));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(true));

  ProjectedSpacialDimensions projectedSpacialDimensions;
  projectedSpacialDimensions.widthProjected = data.widthProjected;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetProjectedDimensions(_, _)).WillByDefault(Return(projectedSpacialDimensions));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(data.aoi, _)).WillByDefault(Return(-1.0_mps));

  bool result = TEST_HELPER.featureExtractor.IsCollisionCourseDetectedNonSideArea(&fakeVehicle);
  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsCollisionCourseDetectedNonSideArea,
    testing::Values(
        DataFor_IsCollisionCourseDetectedNonSideArea{
            .tauDot = 1.0,
            .widthProjected = 5.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .result = false},
        DataFor_IsCollisionCourseDetectedNonSideArea{
            .tauDot = -1.0,
            .widthProjected = 15.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .result = true},
        DataFor_IsCollisionCourseDetectedNonSideArea{
            .tauDot = 1.0,
            .widthProjected = 15.0_m,
            .aoi = AreaOfInterest::EGO_FRONT,
            .result = false},
        DataFor_IsCollisionCourseDetectedNonSideArea{
            .tauDot = -1.0,
            .widthProjected = 15.0_m,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .result = false}));

/************************************
 * CHECK CantPreventCollisionCourse *
 ************************************/
struct DataFor_CantPreventCollisionCourse
{
  units::time::second_t ttc;
  RelativeLane lane;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CantPreventCollisionCourse& obj)
  {
    return os
           << " ttc (double): " << obj.ttc
           << " result (bool): " << ScmCommons::BooleanToString(obj.result);
  }
};

class FeatureExtractor_CantPreventCollisionCourse : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_CantPreventCollisionCourse>
{
};

TEST_P(FeatureExtractor_CantPreventCollisionCourse, FeatureExtractor_CheckFunction_FeatureExtractor_CantPreventCollisionCourse)
{
  FeatureExtractorTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  AreaOfInterest aoi = AreaOfInterest::EGO_FRONT;  // will have no effect, because currently all used methods are mocked
  int sideAoiIndex = 0;                            // will have no effect, because currently all used methods are mocked

  ProjectedSpacialDimensions projectedSpacialDimensions;
  projectedSpacialDimensions.widthProjected = 5.0_m;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetProjectedDimensions(_, _)).WillByDefault(Return(projectedSpacialDimensions));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(_, _)).WillByDefault(Return(-1.0_mps));

  DataFor_CantPreventCollisionCourse data = GetParam();

  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.ttc));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  bool result = TEST_HELPER.featureExtractor.CantPreventCollisionCourse(&fakeVehicle);
  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_CantPreventCollisionCourse,
    testing::Values(
        DataFor_CantPreventCollisionCourse{
            .ttc = 6.0_s,
            .lane = RelativeLane::EGO,
            .result = false},
        DataFor_CantPreventCollisionCourse{
            .ttc = 1.9_s,
            .lane = RelativeLane::LEFT,
            .result = true}));

/*******************************************
 * CHECK IsCollisionCourseDetectedSideArea *
 *******************************************/
struct DataFor_IsCollisionCourseDetectedSideArea
{
  AreaOfInterest aoi;
  units::velocity::meters_per_second_t lateralVelocityTowardsEgoLane;
  RelativeLane lane;
  bool result;
};

class FeatureExtractor_IsCollisionCourseDetectedSideArea : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_IsCollisionCourseDetectedSideArea>
{
};

TEST_P(FeatureExtractor_IsCollisionCourseDetectedSideArea, FeatureExtractor_CheckFunction_FeatureExtractor_IsCollisionCourseDetectedSideArea)
{
  DataFor_IsCollisionCourseDetectedSideArea data = GetParam();
  FeatureExtractorTester TEST_HELPER;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  ObjectInformationScmExtended agent1;
  agent1.ResetToDefault();
  agent1.id = 1;
  agent1.exist = true;
  agent1.obstruction.right = 5_m;
  agent1.obstruction.left = -4_m;

  std::vector<ObjectInformationScmExtended> sideObjects;
  sideObjects.push_back(agent1);
  ON_CALL(fakeMicroscopicCharacteristics, GetSideObjectVector(data.aoi)).WillByDefault(Return(&sideObjects));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetLateralVelocity()).WillByDefault(Return(1_mps));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(true));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(2_s));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(data.aoi, _)).WillByDefault(Return(agent1.obstruction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(2_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(data.aoi, _)).WillByDefault(Return(data.lateralVelocityTowardsEgoLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(data.aoi, _)).WillByDefault(Return(&fakeVehicle));

  ProjectedSpacialDimensions projectedSpacialDimensions;
  projectedSpacialDimensions.widthProjected = 5.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetProjectedDimensions(_, _)).WillByDefault(Return(projectedSpacialDimensions));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  bool result = TEST_HELPER.featureExtractor.IsCollisionCourseDetectedSideArea(&fakeVehicle);
  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsCollisionCourseDetectedSideArea,
    testing::Values(
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .lateralVelocityTowardsEgoLane = 1.0_mps,
            .lane = RelativeLane::EGO,
            .result = true},
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .lateralVelocityTowardsEgoLane = 1.0_mps,
            .lane = RelativeLane::EGO,
            .result = true},
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .lateralVelocityTowardsEgoLane = 0.0_mps,
            .lane = RelativeLane::LEFT,
            .result = false},
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .lateralVelocityTowardsEgoLane = 0.0_mps,
            .lane = RelativeLane::LEFT,
            .result = false},
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .lateralVelocityTowardsEgoLane = 0.0_mps,
            .lane = RelativeLane::LEFT,
            .result = false},
        DataFor_IsCollisionCourseDetectedSideArea{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .lateralVelocityTowardsEgoLane = 0.0_mps,
            .lane = RelativeLane::LEFT,
            .result = false}));

/****************************
 * CHECK IsEvadingEndOfLane *
 ****************************/
struct DataFor_IsEvadingEndOfLane
{
  units::velocity::meters_per_second_t lateralVelocity;
  units::length::meter_t lateralPosition;
  units::length::meter_t relevantVehicleWidth;
  LateralAction lateralAction;
  bool result;
};

class FeatureExtractor_IsEvadingEndOfLane : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_IsEvadingEndOfLane>
{
};

TEST_P(FeatureExtractor_IsEvadingEndOfLane, FeatureExtractor_CheckFunction_IsEvadingEndOfLane)
{
  DataFor_IsEvadingEndOfLane data = GetParam();
  FeatureExtractorTester TEST_HELPER;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  GeometryInformationSCM geoInfo;
  geoInfo.Close().width = 4.0_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&geoInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(data.lateralVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(data.lateralPosition));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetProjectedVehicleWidthRight()).WillByDefault(Return(data.relevantVehicleWidth));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  bool result = TEST_HELPER.featureExtractor.IsEvadingEndOfLane();
  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsEvadingEndOfLane,
    testing::Values(
        DataFor_IsEvadingEndOfLane{
            .lateralVelocity = 1.1_mps,
            .lateralPosition = 0.0_m,
            .relevantVehicleWidth = 3.0_m,
            .lateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT),
            .result = true},
        DataFor_IsEvadingEndOfLane{
            .lateralVelocity = 1.1_mps,
            .lateralPosition = 0.4_m,
            .relevantVehicleWidth = 3.0_m,
            .lateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT),
            .result = true},
        DataFor_IsEvadingEndOfLane{
            .lateralVelocity = 0.0_mps,
            .lateralPosition = -0.4_m,
            .relevantVehicleWidth = 2.0_m,
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .result = false},
        DataFor_IsEvadingEndOfLane{
            .lateralVelocity = 0.0_mps,
            .lateralPosition = 0.0_m,
            .relevantVehicleWidth = 3.0_m,
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result = false},
        DataFor_IsEvadingEndOfLane{
            .lateralVelocity = 0.5_mps,
            .lateralPosition = 0.0_m,
            .relevantVehicleWidth = 2.5_m,
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .result = true}));

/************************************************************
 * CHECK IsMinimumFollowingDistanceViolatedWhileCooperating *
 ************************************************************/

TEST(FeatureExtractor_IsMinimumFollowingDistanceViolatedWhileCooperating, GivenEgoBelowJamSpeedTtcAboveThreshold_UseReductionFactor)
{
  FeatureExtractorTester TEST_HELPER;

  /* Algorithm constants */
  const auto ttcThresold{3._s};

  /* Call parameters */
  const bool egoBelowJamSpeed{true};
  const AreaOfInterest aoi{AreaOfInterest::LEFT_FRONT};
  const auto ttc{ttcThresold + 0.1_s};
  const double expectedReductionFactor = 0.8;

  /* Setup mocks */
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(egoBelowJamSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(0));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(ttc));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);
  /* Call function */
  auto result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolatedWhileCooperating(&fakeVehicle);
  ASSERT_FALSE(result);
}

TEST(FeatureExtractor_IsMinimumFollowingDistanceViolatedWhileCooperating, GivenEgoBelowJamSpeedTtcEqualToThreshold_NoReductionFactor)
{
  FeatureExtractorTester TEST_HELPER;

  /* Algorithm constants */
  const auto ttcThresold{3._s};

  /* Call parameters */
  const bool egoBelowJamSpeed{true};
  const AreaOfInterest aoi{AreaOfInterest::LEFT_FRONT};
  const auto ttc{ttcThresold};
  const double expectedReductionFactor = 1.;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  /* Setup mocks */
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(egoBelowJamSpeed));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(&fakeVehicle, _, _)).WillByDefault(Return(20.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(-1));

  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(ttc));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  /* Call function */
  auto result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolatedWhileCooperating(&fakeVehicle);
  ASSERT_TRUE(result);
}

TEST(FeatureExtractor_IsMinimumFollowingDistanceViolatedWhileCooperating, GivenEgoNotBelowJamSpeedTtcAboveThreshold_NoReductionFactor)
{
  FeatureExtractorTester TEST_HELPER;

  /* Algorithm constants */
  const auto ttcThresold{3._s};

  /* Call parameters */
  const bool egoBelowJamSpeed{false};
  const AreaOfInterest aoi{AreaOfInterest::LEFT_FRONT};
  const auto ttc{ttcThresold + 0.1_s};
  const double expectedReductionFactor = 1.;

  /* Setup mocks */
  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(egoBelowJamSpeed));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(&fakeVehicle, _, _)).WillByDefault(Return(20.0_m));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(ttc));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(-1));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);

  /* Call function */
  auto result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolatedWhileCooperating(&fakeVehicle);
  ASSERT_TRUE(result);
}

/********************
 * CHECK IsObstacle *
 ********************/
struct DataFor_IsObstacle
{
  std::string description;
  MesoscopicSituation currentMesoscopicSituation;
  bool isStaticObject;
  units::velocity::meters_per_second_t absoluteVelocity;
  units::time::second_t ttc;
  bool expected_isObstacle;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_IsObstacle& obj)
  {
    return os
           << "description (string): " << obj.description
           << " isStaticObject (bool): " << obj.isStaticObject
           << " absoluteVelocity (double): " << obj.absoluteVelocity
           << " ttc (double): " << obj.ttc
           << " expected_isObstacle (bool): " << ScmCommons::BooleanToString(obj.expected_isObstacle);
  }
};

class FeatureExtractor_IsObstacle : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_IsObstacle>
{
};

TEST_P(FeatureExtractor_IsObstacle, FeatureExtractor_CheckFunction_IsObstacle)
{
  DataFor_IsObstacle data = GetParam();
  FeatureExtractorTester TEST_HELPER;

  AreaOfInterest aoi = AreaOfInterest::EGO_FRONT;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.currentMesoscopicSituation)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(units::velocity::meters_per_second_t(60.0_kph)));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.ttc));
  ON_CALL(fakeVehicle, IsStatic).WillByDefault(Return(data.isStaticObject));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(data.absoluteVelocity));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  bool result = TEST_HELPER.featureExtractor.IsObstacle(&fakeVehicle);
  ASSERT_EQ(result, data.expected_isObstacle);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsObstacle,
    testing::Values(
        DataFor_IsObstacle{
            .description = "Slow non static vehicles are not obstacles when there is no valid ttc below TTC_LIMIT",
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isStaticObject = false,
            .absoluteVelocity = 1.0_mps,
            .ttc = ScmDefinitions::TTC_LIMIT,
            .expected_isObstacle = false},
        DataFor_IsObstacle{
            .description = "Fast non static vehicles are no obstacles even with minimal ttcs",
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isStaticObject = false,
            .absoluteVelocity = 99.0_mps,
            .ttc = 1.0_s,
            .expected_isObstacle = false},
        DataFor_IsObstacle{
            .description = "Slow non static vehicles are obstacles in when not in a traffic jam",
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isStaticObject = false,
            .absoluteVelocity = 1.0_mps,
            .ttc = 1.0_s,
            .expected_isObstacle = true},
        DataFor_IsObstacle{
            .description = "Static objects always counts as obstacles, no matter how fast and how long the ttc is (FREE_DRIVING)",
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isStaticObject = true,
            .absoluteVelocity = 99.0_mps,
            .ttc = ScmDefinitions::TTC_LIMIT,
            .expected_isObstacle = true},
        DataFor_IsObstacle{
            .description = "Static objects always counts as obstacles, no matter how fast and how long the ttc is (QUEUED_TRAFFIC)",
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .isStaticObject = true,
            .absoluteVelocity = 0.0_mps,
            .ttc = ScmDefinitions::TTC_LIMIT,
            .expected_isObstacle = true},
        DataFor_IsObstacle{
            .description = "Slow non static vehicles are no obstacles in a traffic jam",
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .isStaticObject = false,
            .absoluteVelocity = 1.0_mps,
            .ttc = 1.0_s,
            .expected_isObstacle = false}));

/*****************************
 * CHECK IsExceptionallySlow *
 *****************************/
struct DataFor_IsExceptionallySlow
{
  std::string description;
  AreaOfInterest aoi;
  MesoscopicSituation currentMesoscopicSituation;
  bool isVehicleVisible;
  units::velocity::meters_per_second_t absoluteVelocity;
  units::velocity::meters_per_second_t absoluteVelocityEgo;
  units::velocity::meters_per_second_t meanVelocity;
  units::velocity::meters_per_second_t legalVelocity;
  bool expected_IsExceptionallySlow;
};

class FeatureExtractor_IsExceptionallySlow : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsExceptionallySlow>
{
};

TEST_P(FeatureExtractor_IsExceptionallySlow, FeatureExtractor_CheckFunction_IsExceptionallySlow)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsExceptionallySlow data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.currentMesoscopicSituation)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(ScmDefinitions::TRAFFIC_JAM_VELOCITY_THRESHOLD));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(data.absoluteVelocityEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(data.meanVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalRight()).WillByDefault(Return(data.legalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalLeft()).WillByDefault(Return(data.legalVelocity));

  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(data.absoluteVelocity));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));

  bool result = TEST_HELPER.featureExtractor.IsExceptionallySlow(&fakeVehicle);
  ASSERT_EQ(result, data.expected_IsExceptionallySlow);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsExceptionallySlow,
    testing::Values(
        DataFor_IsExceptionallySlow{
            .description = "When the vehicle is not visible we do not know anything so it can't be exceptionally slow",
            .aoi = AreaOfInterest::EGO_FRONT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isVehicleVisible = false,
            .absoluteVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .absoluteVelocityEgo = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .meanVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .legalVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .expected_IsExceptionallySlow = false},
        DataFor_IsExceptionallySlow{
            .description = "In traffic jam situations and ego velocity < jam velocity, no vehicle counts as exceptionally slow",
            .aoi = AreaOfInterest::EGO_FRONT,
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .isVehicleVisible = true,
            .absoluteVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .absoluteVelocityEgo = ScmDefinitions::TRAFFIC_JAM_VELOCITY_THRESHOLD - 0.1_mps,
            .meanVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .legalVelocity = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE),
            .expected_IsExceptionallySlow = false},
        DataFor_IsExceptionallySlow{
            .description = "Ego fast, other very slow, but no vehicle counts as exceptionally slow in a traffic jam",
            .aoi = AreaOfInterest::EGO_FRONT,
            .currentMesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .isVehicleVisible = true,
            .absoluteVelocity = 0.0_mps,
            .absoluteVelocityEgo = units::velocity::meters_per_second_t(250.0_kph),
            .meanVelocity = units::velocity::meters_per_second_t(130.0_kph),
            .legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_IsExceptionallySlow = false},
        DataFor_IsExceptionallySlow{
            .description = "Ego fast, other very slow, vehicle counts as exceptionally slow (I)",
            .aoi = AreaOfInterest::LEFT_FRONT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isVehicleVisible = true,
            .absoluteVelocity = 0.0_mps,
            .absoluteVelocityEgo = ScmDefinitions::INF_VELOCITY,
            .meanVelocity = units::velocity::meters_per_second_t(130.0_kph),
            .legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_IsExceptionallySlow = true},
        DataFor_IsExceptionallySlow{
            .description = "Ego fast, other very slow, vehicle counts as exceptionally slow (II)",
            .aoi = AreaOfInterest::LEFT_FRONT,
            .currentMesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .isVehicleVisible = true,
            .absoluteVelocity = 0.0_mps,
            .absoluteVelocityEgo = ScmDefinitions::INF_VELOCITY,
            .meanVelocity = ScmDefinitions::INF_VELOCITY,
            .legalVelocity = ScmDefinitions::INF_VELOCITY,
            .expected_IsExceptionallySlow = true}));

/**********************************************
 * CHECK IsNearEnoughForFollowingUnderUrgency *
 **********************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_IsNearEnoughForFollowing
{
  units::velocity::meters_per_second_t velocityWish;
  units::length::meter_t eqDistance;
  units::velocity::meters_per_second_t longitudinalVelocityEgo;
  units::length::meter_t relativeNetDistance;
  bool result;
};

class FeatureExtractor_IsNearEnoughForFollowing : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_IsNearEnoughForFollowing>
{
};
TEST_P(FeatureExtractor_IsNearEnoughForFollowing, CHECK_IsNearEnoughForFollowing)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsNearEnoughForFollowing data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(0));

  const SurroundingVehicleInterface* vehicle{&fakeVehicle};
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateDeltaVelocityWish()).WillByDefault(Return(data.velocityWish));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(vehicle)).WillByDefault(Return(data.eqDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(data.longitudinalVelocityEgo));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(data.relativeNetDistance));
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  auto result = TEST_HELPER.featureExtractor.IsNearEnoughForFollowing(*vehicle, 1.0);

  ASSERT_EQ(result, data.result);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsNearEnoughForFollowing,
    testing::Values(
        DataFor_IsNearEnoughForFollowing{
            .velocityWish = 10_mps,
            .eqDistance = 10_m,
            .longitudinalVelocityEgo = 1_mps,
            .relativeNetDistance = 10_m,
            .result = true},
        DataFor_IsNearEnoughForFollowing{
            .velocityWish = 10_mps,
            .eqDistance = 10_m,
            .longitudinalVelocityEgo = 1_mps,
            .relativeNetDistance = 20_m,
            .result = false}));

/********************************************************
 * CHECK FeatureExtractor_IsInfluencingDistanceViolated *
 ********************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_IsInfluencingDistanceViolated
{
  units::length::meter_t influencingDistance;
  units::length::meter_t relativeNetDistance;
  bool result;
};

class FeatureExtractor_IsInfluencingDistanceViolated : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_IsInfluencingDistanceViolated>
{
};

TEST_P(FeatureExtractor_IsInfluencingDistanceViolated, CHECK_IsInfluencingDistanceViolated)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsInfluencingDistanceViolated data = GetParam();

  NiceMock<FakeSurroundingVehicle> surroundingVehicle;
  ON_CALL(surroundingVehicle, GetId()).WillByDefault(Return(0));
  const SurroundingVehicleInterface* vehicle{&surroundingVehicle};
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetInfluencingDistance(_)).WillByDefault(Return(data.influencingDistance));
  ON_CALL(surroundingVehicle, GetRelativeNetDistance()).WillByDefault(Return(data.relativeNetDistance));
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);
  auto result = TEST_HELPER.featureExtractor.IsInfluencingDistanceViolated(vehicle);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsInfluencingDistanceViolated,
    testing::Values(
        DataFor_IsInfluencingDistanceViolated{
            .influencingDistance = 20_m,
            .relativeNetDistance = 10_m,
            .result = true},
        DataFor_IsInfluencingDistanceViolated{
            .influencingDistance = 10_m,
            .relativeNetDistance = 20_m,
            .result = false}));

/*************************************************************************
 * CHECK FeatureExtractor_IsMinimumFollowingDistanceViolatedUnderUrgency *
 *************************************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_IsMinimumFollowingDistanceViolatedUnderUrgency
{
  units::length::meter_t minDistance;
  units::length::meter_t relativeNetDistance;
  bool result;
};

class FeatureExtractor_IsMinimumFollowingDistanceViolatedUnderUrgency : public ::testing::Test,
                                                                        public ::testing::WithParamInterface<DataFor_IsMinimumFollowingDistanceViolatedUnderUrgency>
{
};
TEST_P(FeatureExtractor_IsMinimumFollowingDistanceViolatedUnderUrgency, IsMinimumFollowingDistanceViolatedUnderUrgency)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsMinimumFollowingDistanceViolatedUnderUrgency data = GetParam();

  NiceMock<FakeSurroundingVehicle> surroundingVehicle;
  ON_CALL(surroundingVehicle, GetId()).WillByDefault(Return(0));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(data.minDistance));
  ON_CALL(surroundingVehicle, GetRelativeNetDistance()).WillByDefault(Return(data.relativeNetDistance));
  TEST_HELPER.featureExtractor.SetMentalCalculations(TEST_HELPER.fakeMentalCalculations);
  TEST_HELPER.featureExtractor.SetMentalModel(TEST_HELPER.fakeMentalModel);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(-1));
  auto result = TEST_HELPER.featureExtractor.IsMinimumFollowingDistanceViolated(&surroundingVehicle, 1.0);

  ASSERT_EQ(result, data.result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsMinimumFollowingDistanceViolatedUnderUrgency,
    testing::Values(
        DataFor_IsMinimumFollowingDistanceViolatedUnderUrgency{
            .minDistance = 20_m,
            .relativeNetDistance = 10_m,
            .result = true},
        DataFor_IsMinimumFollowingDistanceViolatedUnderUrgency{
            .minDistance = 10_m,
            .relativeNetDistance = 20_m,
            .result = false}));

/**********************************************
 * CHECK FeatureExtractor_IsVehicleInSideLane *
 **********************************************/
struct DataFor_IsVehicleInSideLane
{
  RelativeLane lane;
  bool expectedResult;
};

class FeatureExtractor_IsVehicleInSideLane : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsVehicleInSideLane>
{
};

TEST_P(FeatureExtractor_IsVehicleInSideLane, Check_IsVehicleInSideLane)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsVehicleInSideLane data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));

  bool result;
  if (data.lane == RelativeLane::EGO || data.lane == RelativeLane::LEFT)
  {
    result = TEST_HELPER.featureExtractor.IsVehicleInSideLane(&fakeVehicle);
  }
  else
  {
    result = TEST_HELPER.featureExtractor.IsVehicleInSideLane(nullptr);
  }

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsVehicleInSideLane,
    testing::Values(
        DataFor_IsVehicleInSideLane{
            .lane = RelativeLane::EGO,
            .expectedResult = false},
        DataFor_IsVehicleInSideLane{
            .lane = RelativeLane::LEFT,
            .expectedResult = true},
        DataFor_IsVehicleInSideLane{
            .lane = RelativeLane::LEFTLEFT,
            .expectedResult = false}));

/*************************************************
 * CHECK FeatureExtractor_HasSuspiciousBehaviour *
 *************************************************/

struct DataFor_HasSuspiciousBehaviour
{
  bool isQueuedTraffic;
  bool isStatic;
  units::velocity::meters_per_second_t trafficJamVelocityThreshold;
  bool isCollided;
  bool expectedResult;
};

class FeatureExtractor_HasSuspiciousBehaviour : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_HasSuspiciousBehaviour>
{
};

TEST_P(FeatureExtractor_HasSuspiciousBehaviour, Check_HasSuspiciousBehaviour)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_HasSuspiciousBehaviour data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, IsStatic()).WillByDefault(Return(data.isStatic));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(4.0_mps));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(5.0_s));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));
  ON_CALL(fakeVehicle, IsCollided()).WillByDefault(Return(data.isCollided));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::EGO));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(data.isQueuedTraffic));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(data.trafficJamVelocityThreshold));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(4.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalRight()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalLeft()).WillByDefault(Return(10.0_mps));

  auto result = TEST_HELPER.featureExtractor.HasSuspiciousBehaviour(&fakeVehicle);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_HasSuspiciousBehaviour,
    testing::Values(
        DataFor_HasSuspiciousBehaviour{
            .isQueuedTraffic = true,
            .isStatic = true,
            .trafficJamVelocityThreshold = 10.0_mps,
            .isCollided = false,
            .expectedResult = true},
        DataFor_HasSuspiciousBehaviour{
            .isQueuedTraffic = false,
            .isStatic = false,
            .trafficJamVelocityThreshold = 4.0_mps,
            .isCollided = false,
            .expectedResult = false}));

/****************************************
 * CHECK FeatureExtractor_IsUnfavorable *
 ****************************************/

struct DataFor_IsUnfavorable
{
  bool isBehindEgo;
  bool isReliable;
  units::time::second_t desiredTimeAtTargetSpeed;
  bool expectedResult;
};

class FeatureExtractor_IsUnfavorable : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsUnfavorable>
{
};

TEST_P(FeatureExtractor_IsUnfavorable, Check_IsUnfavorable)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsUnfavorable data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, BehindEgo()).WillByDefault(Return(data.isBehindEgo));
  ON_CALL(fakeVehicle, IsReliable(DataQuality::MEDIUM, ParameterChangeRate::MEDIUM)).WillByDefault(Return(data.isReliable));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));

  DriverParameters driverParameters;
  driverParameters.desiredTimeAtTargetSpeed = data.desiredTimeAtTargetSpeed;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(false));

  if (data.isReliable)
  {
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, AddInformationRequest(_, FieldOfViewAssignment::UFOV, 0.5, InformationRequestTrigger::LATERAL_ACTION)).Times(0);
  }
  else
  {
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, AddInformationRequest(_, FieldOfViewAssignment::UFOV, 0.5, InformationRequestTrigger::LATERAL_ACTION)).Times(1);
  }

  auto result = TEST_HELPER.featureExtractor.IsUnfavorable(&fakeVehicle);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsUnfavorable,
    testing::Values(
        DataFor_IsUnfavorable{
            .isBehindEgo = true,
            .isReliable = true,
            .desiredTimeAtTargetSpeed = 10_s,
            .expectedResult = true},
        DataFor_IsUnfavorable{
            .isBehindEgo = false,
            .isReliable = true,
            .desiredTimeAtTargetSpeed = -10_s,
            .expectedResult = false},
        DataFor_IsUnfavorable{
            .isBehindEgo = false,
            .isReliable = false,
            .desiredTimeAtTargetSpeed = -10_s,
            .expectedResult = true}));

/*****************************************
 * CHECK FeatureExtractor_HasJamVelocity *
 *****************************************/
struct DataFor_HasJamVelocity
{
  units::velocity::meters_per_second_t absoluteVelocity;
  units::velocity::meters_per_second_t jamVelocity;
  bool expectedResult;
};

class FeatureExtractor_HasJamVelocity : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_HasJamVelocity>
{
};

TEST_P(FeatureExtractor_HasJamVelocity, Check_HasJamVelocity)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_HasJamVelocity data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(data.absoluteVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(data.jamVelocity));

  auto result = TEST_HELPER.featureExtractor.HasJamVelocity(&fakeVehicle);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_HasJamVelocity,
    testing::Values(
        DataFor_HasJamVelocity{
            .absoluteVelocity = 10.0_mps,
            .jamVelocity = 20_mps,
            .expectedResult = true},
        DataFor_HasJamVelocity{
            .absoluteVelocity = 20.0_mps,
            .jamVelocity = 10_mps,
            .expectedResult = false}));

/********************************************************************
 * CHECK FeatureExtractor_IsDeceleratingDueToSuspiciousSideVehicles *
 ********************************************************************/

struct DataFor_IsDeceleratingDueToSuspiciousSideVehicles
{
  double cooperationFactor;
  bool expectedResult;
};

class FeatureExtractor_IsDeceleratingDueToSuspiciousSideVehicles : public ::testing::Test,
                                                                   public ::testing::WithParamInterface<DataFor_IsDeceleratingDueToSuspiciousSideVehicles>
{
};

TEST_P(FeatureExtractor_IsDeceleratingDueToSuspiciousSideVehicles, Check_IsDeceleratingDueToSuspiciousSideVehicles)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsDeceleratingDueToSuspiciousSideVehicles data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactorForSuspiciousBehaviourEvasion()).WillByDefault(Return(data.cooperationFactor));

  auto result = TEST_HELPER.featureExtractor.IsDeceleratingDueToSuspiciousSideVehicles();
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsDeceleratingDueToSuspiciousSideVehicles,
    testing::Values(
        DataFor_IsDeceleratingDueToSuspiciousSideVehicles{
            .cooperationFactor = 0.6,
            .expectedResult = true},
        DataFor_IsDeceleratingDueToSuspiciousSideVehicles{
            .cooperationFactor = 1.0,
            .expectedResult = false}));

/********************************************************************
 * CHECK FeatureExtractor_IsDeceleratingDueToSuspiciousSideVehicles *
 ********************************************************************/

struct DataFor_IsAvoidingLaneNextToSuspiciousSideVehicles
{
  double cooperationFactor;
  bool expectedResult;
};

class FeatureExtractor_IsAvoidingLaneNextToSuspiciousSideVehicles : public ::testing::Test,
                                                                    public ::testing::WithParamInterface<DataFor_IsAvoidingLaneNextToSuspiciousSideVehicles>
{
};

TEST_P(FeatureExtractor_IsAvoidingLaneNextToSuspiciousSideVehicles, Check_IsAvoidingLaneNextToSuspiciousSideVehicles)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsAvoidingLaneNextToSuspiciousSideVehicles data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactorForSuspiciousBehaviourEvasion()).WillByDefault(Return(data.cooperationFactor));

  auto result = TEST_HELPER.featureExtractor.IsAvoidingLaneNextToSuspiciousSideVehicles();
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsAvoidingLaneNextToSuspiciousSideVehicles,
    testing::Values(
        DataFor_IsAvoidingLaneNextToSuspiciousSideVehicles{
            .cooperationFactor = 1.0,
            .expectedResult = true},
        DataFor_IsAvoidingLaneNextToSuspiciousSideVehicles{
            .cooperationFactor = 0.6,
            .expectedResult = false}));

/************************************************
 * CHECK FeatureExtractor_AnticipatedAbleToPass *
 ************************************************/

struct DataFor_AnticipatedAbleToPass
{
  units::length::meter_t distanceTowardsEgoLane;
  bool expectedResult;
};

class FeatureExtractor_AnticipatedAbleToPass : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_AnticipatedAbleToPass>
{
};

TEST_P(FeatureExtractor_AnticipatedAbleToPass, Check_AnticipatedAbleToPass)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_AnticipatedAbleToPass data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(50.0_s));
  ON_CALL(fakeVehicle, GetLength()).WillByDefault(Return(5.0_m));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(true));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(_, _)).WillByDefault(Return(-1.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceTowardsEgoLane(_, _)).WillByDefault(Return(data.distanceTowardsEgoLane));

  auto result = TEST_HELPER.featureExtractor.AnticipatedAbleToPass(&fakeVehicle, 10.0_mps);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_AnticipatedAbleToPass,
    testing::Values(
        DataFor_AnticipatedAbleToPass{
            .distanceTowardsEgoLane = 100.0_m,
            .expectedResult = true},
        DataFor_AnticipatedAbleToPass{
            .distanceTowardsEgoLane = 4.0_m,
            .expectedResult = false}));

/**********************************************
 * CHECK FeatureExtractor_IsOnEntryOrExitLane *
 **********************************************/

struct DataFor_IsOnEntryOrExitLane
{
  scm::LaneType laneType;
  bool expectedResult;
};

class FeatureExtractor_IsOnEntryOrExitLane : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_IsOnEntryOrExitLane>
{
};

TEST_P(FeatureExtractor_IsOnEntryOrExitLane, Check_IsOnEntryOrExitLane)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsOnEntryOrExitLane data = GetParam();

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  GeometryInformationSCM geoInfo;
  geoInfo.Close().laneType = data.laneType;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&geoInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  auto result = TEST_HELPER.featureExtractor.IsOnEntryOrExitLane();
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsOnEntryOrExitLane,
    testing::Values(
        DataFor_IsOnEntryOrExitLane{
            .laneType = scm::LaneType::OffRamp,
            .expectedResult = true},
        DataFor_IsOnEntryOrExitLane{
            .laneType = scm::LaneType::OnRamp,
            .expectedResult = true},
        DataFor_IsOnEntryOrExitLane{
            .laneType = scm::LaneType::Exit,
            .expectedResult = true},
        DataFor_IsOnEntryOrExitLane{
            .laneType = scm::LaneType::Entry,
            .expectedResult = true},
        DataFor_IsOnEntryOrExitLane{
            .laneType = scm::LaneType::Driving,
            .expectedResult = false}));

/****************************************
 * CHECK FeatureExtractor_IsOnEntryLane *
 ****************************************/

struct DataFor_IsOnEntryLane
{
  scm::LaneType laneType;
  bool expectedResult;
};

class FeatureExtractor_IsOnEntryLane : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_IsOnEntryLane>
{
};

TEST_P(FeatureExtractor_IsOnEntryLane, Check_IsOnEntryLane)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsOnEntryLane data = GetParam();

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  GeometryInformationSCM geoInfo;
  geoInfo.Close().laneType = data.laneType;
  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&geoInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  auto result = TEST_HELPER.featureExtractor.IsOnEntryLane();
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsOnEntryLane,
    testing::Values(
        DataFor_IsOnEntryLane{
            .laneType = scm::LaneType::OnRamp,
            .expectedResult = true},
        DataFor_IsOnEntryLane{
            .laneType = scm::LaneType::Entry,
            .expectedResult = true},
        DataFor_IsOnEntryLane{
            .laneType = scm::LaneType::Exit,
            .expectedResult = false}));

/***********************************************************************
 * CHECK FeatureExtractor_IsLaneChangePermittedDueToLaneMarkingsForAoi *
 ***********************************************************************/

struct DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi
{
  scm::LaneType laneType;
  bool isShoulderLaneUsage;
  AreaOfInterest aoi;
  scm::LaneMarking::Type laneMarkingsAoi;
  bool expectedResult;
};

class FeatureExtractor_IsLaneChangePermittedDueToLaneMarkingsForAoi : public ::testing::Test,
                                                                      public ::testing::WithParamInterface<DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi>
{
};

TEST_P(FeatureExtractor_IsLaneChangePermittedDueToLaneMarkingsForAoi, Check_IsLaneChangePermittedDueToLaneMarkingsForAoi)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));

  LaneInformationGeometrySCM laneInfo;
  laneInfo.laneType = data.laneType;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsShoulderLaneUsageLegit(_)).WillByDefault(Return(data.isShoulderLaneUsage));

  scm::LaneMarking::Entity entity;

  entity.type = data.laneMarkingsAoi;

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneMarkingAtAoi(_)).WillByDefault(Return(entity));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, InterpreteBrokenBoldLaneMarking(_, _)).WillByDefault(Return(entity));

  auto result = TEST_HELPER.featureExtractor.IsLaneChangePermittedDueToLaneMarkingsForAoi(&fakeVehicle);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsLaneChangePermittedDueToLaneMarkingsForAoi,
    testing::Values(
        DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi{
            .laneType = scm::LaneType::Sidewalk,
            .isShoulderLaneUsage = false,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .laneMarkingsAoi = scm::LaneMarking::Type::Solid_Broken,
            .expectedResult = true},
        DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi{
            .laneType = scm::LaneType::Driving,
            .isShoulderLaneUsage = true,
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .laneMarkingsAoi = scm::LaneMarking::Type::Solid_Broken,
            .expectedResult = true},
        DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi{
            .laneType = scm::LaneType::Driving,
            .isShoulderLaneUsage = true,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .laneMarkingsAoi = scm::LaneMarking::Type::Solid_Broken,
            .expectedResult = false},
        DataFor_IsLaneChangePermittedDueToLaneMarkingsForAoi{
            .laneType = scm::LaneType::Driving,
            .isShoulderLaneUsage = true,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .laneMarkingsAoi = scm::LaneMarking::Type::Broken_Broken,
            .expectedResult = true}));

/******************************************
 * CHECK FeatureExtractor_CheckLaneChange *
 ******************************************/

struct DataFor_CheckLaneChange
{
  bool isEmergecy;
  Side side;
  AreaOfInterest aoi;
  bool expectedResult;
};

class FeatureExtractor_CheckLaneChange : public ::testing::Test,
                                         public ::testing::WithParamInterface<DataFor_CheckLaneChange>
{
};

TEST_P(FeatureExtractor_CheckLaneChange, CheckLaneChange)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_CheckLaneChange data = GetParam();

  std::vector<ObjectInformationSCM> virtualAgents{};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVirtualAgents()).WillByDefault(Return(virtualAgents));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsDataForLaneChangeReliable(data.side)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSideSideLaneObjectChangingIntoSide(_, false)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(4.0_s));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(-0.6));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));

  NiceMock<FakeSurroundingVehicle> fakeVehicleSide;
  std::vector<const SurroundingVehicleInterface*> vehiclesSide{};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleVector(_)).WillByDefault(Return(vehiclesSide));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToStartOfNextExit()).WillByDefault(Return(0.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfNextExit()).WillByDefault(Return(0.0_m));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  LaneInformationGeometrySCM laneInfo;
  laneInfo.distanceToEndOfLane = 6.0_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(5.0_m));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(1));

  // TODO also test the case that ego is not currently merging 
  Merge::Gap gap{&fakeVehicle, nullptr};
  gap.distance = 15.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(gap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(
    LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)
  ));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.isLaneChangeProhibitionIgnored = false;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  GeometryInformationSCM geoInfo;
  geoInfo.Close().laneType = scm::LaneType::Driving;

  TrafficRuleInformationScmExtended trafficInfo;
  scm::LaneMarking::Entity entity{};
  entity.relativeStartDistance = 1.0_m;
  trafficInfo.Close().laneMarkingsLeft = {entity};

  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&geoInfo));
  ON_CALL(fakeInfrastructureCharacteristsics, GetTrafficRuleInformation()).WillByDefault(Return(&trafficInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsShoulderLaneUsageLegit(_)).WillByDefault(Return(true));

  auto result = TEST_HELPER.featureExtractor.CheckLaneChange(data.side, data.isEmergecy);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_CheckLaneChange,
    testing::Values(
        DataFor_CheckLaneChange{
            .isEmergecy = true,
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = true},
        DataFor_CheckLaneChange{
            .isEmergecy = true,
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = false},
        DataFor_CheckLaneChange{
            .isEmergecy = false,
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = true},
        DataFor_CheckLaneChange{
            .isEmergecy = false,
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = false}));

/**********************************************************************
 * CHECK IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition *
 **********************************************************************/

struct DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition
{
  AreaOfInterest aoiToEvaluate;
  Situation currentSituation;
  bool isCurrentLaneBlocked;
  bool expectedResult;
};

class FeatureExtractor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition : public ::testing::Test,
                                                                                      public ::testing::WithParamInterface<DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition>
{
};

TEST_P(FeatureExtractor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition, Check_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.currentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED)).WillByDefault(Return(data.isCurrentLaneBlocked));

  auto result = TEST_HELPER.featureExtractor.IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition(data.aoiToEvaluate);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition,
    testing::Values(
        DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition{
            .aoiToEvaluate = AreaOfInterest::LEFT_FRONT,
            .currentSituation = Situation::COLLISION,
            .isCurrentLaneBlocked = false,
            .expectedResult = true},
        DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition{
            .aoiToEvaluate = AreaOfInterest::LEFT_FRONT,
            .currentSituation = Situation::FREE_DRIVING,
            .isCurrentLaneBlocked = false,
            .expectedResult = false},
        DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition{
            .aoiToEvaluate = AreaOfInterest::LEFT_SIDE,
            .currentSituation = Situation::COLLISION,
            .isCurrentLaneBlocked = true,
            .expectedResult = true},
        DataFor_IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition{
            .aoiToEvaluate = AreaOfInterest::LEFT_SIDE,
            .currentSituation = Situation::FREE_DRIVING,
            .isCurrentLaneBlocked = false,
            .expectedResult = false}));

/***********************************
 * CHECK IsSideLaneSeparateRoadway *
 ***********************************/

struct DataFor_IsSideLaneSeparateRoadway
{
  scm::LaneMarking::Type type;
  bool expectedResult;
};

class FeatureExtractor_IsSideLaneSeparateRoadway : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_IsSideLaneSeparateRoadway>
{
};

TEST_P(FeatureExtractor_IsSideLaneSeparateRoadway, Check_IsSideLaneSeparateRoadway)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsSideLaneSeparateRoadway data = GetParam();

  scm::LaneMarking::Entity entity{};
  entity.width = 0.3_m;
  entity.type = data.type;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneMarkingAtDistance(0._m, _)).WillByDefault(Return(entity));

  auto result = TEST_HELPER.featureExtractor.IsSideLaneSeparateRoadway(true);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsSideLaneSeparateRoadway,
    testing::Values(
        DataFor_IsSideLaneSeparateRoadway{
            .type = scm::LaneMarking::Type::Broken,
            .expectedResult = true},
        DataFor_IsSideLaneSeparateRoadway{
            .type = scm::LaneMarking::Type::Broken_Broken,
            .expectedResult = false}));

/*********************************************
 * CHECK IsAoiRelevantForOuterLaneOvertaking *
 *********************************************/

struct DataFor_IsAoiRelevantForOuterLaneOvertaking
{
  bool isRightHandTraffic;
  AreaOfInterest aoi;
  bool expectedResult;
};

class FeatureExtractor_IsAoiRelevantForOuterLaneOvertaking : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_IsAoiRelevantForOuterLaneOvertaking>
{
};

TEST_P(FeatureExtractor_IsAoiRelevantForOuterLaneOvertaking, Check_IsAoiRelevantForOuterLaneOvertaking)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsAoiRelevantForOuterLaneOvertaking data = GetParam();

  auto result = TEST_HELPER.featureExtractor.IsAoiRelevantForOuterLaneOvertaking(data.aoi, data.isRightHandTraffic);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsAoiRelevantForOuterLaneOvertaking,
    testing::Values(
        DataFor_IsAoiRelevantForOuterLaneOvertaking{
            .isRightHandTraffic = true,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = false},
        DataFor_IsAoiRelevantForOuterLaneOvertaking{
            .isRightHandTraffic = true,
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = true},
        DataFor_IsAoiRelevantForOuterLaneOvertaking{
            .isRightHandTraffic = false,
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedResult = false},
        DataFor_IsAoiRelevantForOuterLaneOvertaking{
            .isRightHandTraffic = false,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedResult = true}));

/*************************************************
 * CHECK DoesOuterLaneOvertakingProhibitionApply *
 *************************************************/

struct DataFor_DoesOuterLaneOvertakingProhibitionApply
{
  bool isRightHandTraffic;
  AreaOfInterest aoi;
  Situation currentSituation;
  bool isQueuedTraffic;
  bool isVisible;
  units::length::meter_t relativeNetDistance;
  bool isMergePreparation;
  bool isFulfilled;
  scm::LaneMarking::Type type;
  bool expectedResult;
};

class FeatureExtractor_DoesOuterLaneOvertakingProhibitionApply : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataFor_DoesOuterLaneOvertakingProhibitionApply>
{
};

TEST_P(FeatureExtractor_DoesOuterLaneOvertakingProhibitionApply, Check_DoesOuterLaneOvertakingProhibitionApply)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_DoesOuterLaneOvertakingProhibitionApply data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.currentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(data.isQueuedTraffic));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(data.aoi, _)).WillByDefault(Return(data.isVisible));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(data.relativeNetDistance));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetInfluencingDistance(_)).WillByDefault(Return(100_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(data.aoi, _)).WillByDefault(Return(&fakeVehicle));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMergePreparationActive()).WillByDefault(Return(data.isMergePreparation));

  NiceMock<FakeIgnoringOuterLaneOvertakingProhibition> fakeIgnoringOuterLaneOvertakingProhibition;
  ON_CALL(fakeIgnoringOuterLaneOvertakingProhibition, IsFulfilled(data.aoi)).WillByDefault(Return(data.isFulfilled));
  TEST_HELPER.featureExtractor.SetOuterLaneOvertakingProhibitionQuota(&fakeIgnoringOuterLaneOvertakingProhibition);

  scm::LaneMarking::Entity entity{};
  entity.width = 0.3_m;
  entity.type = data.type;
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneMarkingAtDistance(0._m, _)).WillByDefault(Return(entity));

  auto result = TEST_HELPER.featureExtractor.DoesOuterLaneOvertakingProhibitionApply(data.aoi, data.isRightHandTraffic);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_DoesOuterLaneOvertakingProhibitionApply,
    testing::Values(
        DataFor_DoesOuterLaneOvertakingProhibitionApply{
            .isRightHandTraffic = true,
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .currentSituation = Situation::COLLISION,
            .isQueuedTraffic = true,
            .isVisible = false,
            .relativeNetDistance = 150_m,
            .isMergePreparation = true,
            .isFulfilled = true,
            .type = scm::LaneMarking::Type::Broken,
            .expectedResult = false},
        DataFor_DoesOuterLaneOvertakingProhibitionApply{
            .isRightHandTraffic = true,
            .aoi = AreaOfInterest::LEFT_FRONT,
            .currentSituation = Situation::FREE_DRIVING,
            .isQueuedTraffic = false,
            .isVisible = true,
            .relativeNetDistance = 90_m,
            .isMergePreparation = false,
            .isFulfilled = false,
            .type = scm::LaneMarking::Type::Broken_Broken,
            .expectedResult = true}));

/********************************************
 * CHECK HasAnticipatedLaneCrossingConflict *
 ********************************************/

struct DataFor_HasAnticipatedLaneCrossingConflict
{
  bool isStatic;
  bool isChangingIntoEgoLane;
  bool isToTheSideOfEgo;
  units::time::second_t ttc;
  bool isExceedingLateralMotionThreshold;
  bool expectedResult;
};

class FeatureExtractor_HasAnticipatedLaneCrossingConflict : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_HasAnticipatedLaneCrossingConflict>
{
};

TEST_P(FeatureExtractor_HasAnticipatedLaneCrossingConflict, Check_HasAnticipatedLaneCrossingConflict)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_HasAnticipatedLaneCrossingConflict data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, IsStatic()).WillByDefault(Return(data.isStatic));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(200_m));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(data.isToTheSideOfEgo));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(data.ttc));
  ON_CALL(fakeVehicle, GetLength()).WillByDefault(Return(5.0_m));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(data.isExceedingLateralMotionThreshold));

  NiceMock<FakeSurroundingVehicleQueryFactory> fakeQueryFactory;
  auto fakeVehicleQuery = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(fakeQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));
  ON_CALL(*fakeVehicleQuery, IsChangingIntoEgoLane()).WillByDefault(Return(data.isChangingIntoEgoLane));
  ON_CALL(*fakeVehicleQuery, GetLongitudinalVelocityDelta()).WillByDefault(Return(1.0_mps));

  TEST_HELPER.featureExtractor.SetSurroundingVehicleQueryFactory(&fakeQueryFactory);

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(_)).WillByDefault(Return(100_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(100_mps));

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;
  vehicleModelParameters.bounding_box.dimension.length = 5.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(_, _)).WillByDefault(Return(-1.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceTowardsEgoLane(_, _)).WillByDefault(Return(1.0_m));

  auto result = TEST_HELPER.featureExtractor.HasAnticipatedLaneCrossingConflict(&fakeVehicle);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_HasAnticipatedLaneCrossingConflict,
    testing::Values(
        DataFor_HasAnticipatedLaneCrossingConflict{
            .isStatic = false,
            .isChangingIntoEgoLane = true,
            .isToTheSideOfEgo = false,
            .ttc = 10.0_s,
            .isExceedingLateralMotionThreshold = true,
            .expectedResult = true},
        DataFor_HasAnticipatedLaneCrossingConflict{
            .isStatic = false,
            .isChangingIntoEgoLane = true,
            .isToTheSideOfEgo = false,
            .ttc = 10.0_s,
            .isExceedingLateralMotionThreshold = false,
            .expectedResult = false},
        DataFor_HasAnticipatedLaneCrossingConflict{
            .isStatic = false,
            .isChangingIntoEgoLane = true,
            .isToTheSideOfEgo = false,
            .ttc = -10.0_s,
            .isExceedingLateralMotionThreshold = false,
            .expectedResult = false},
        DataFor_HasAnticipatedLaneCrossingConflict{
            .isStatic = false,
            .isChangingIntoEgoLane = false,
            .isToTheSideOfEgo = false,
            .ttc = -10.0_s,
            .isExceedingLateralMotionThreshold = false,
            .expectedResult = false}));

/************************
 * CHECK IsSideLaneSafe *
 ************************/

struct DataFor_IsSideLaneSafe
{
  bool laneExist;
  bool isDataReliable;
  bool isSideObjectChangingIntoSide;
  AreaOfInterest aoi;
  bool isVirtualAgents;
  scm::LaneType type;
  bool expectedResult;
};

class FeatureExtractor_IsSideLaneSafe : public ::testing::Test,
                                        public ::testing::WithParamInterface<DataFor_IsSideLaneSafe>
{
};

TEST_P(FeatureExtractor_IsSideLaneSafe, Check_IsSideLaneSafe)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsSideLaneSafe data = GetParam();

  std::vector<ObjectInformationSCM> virtualAgents{};
  if (data.isVirtualAgents)
  {
    ObjectInformationSCM virtualAgent;
    virtualAgent.relativeLongitudinalDistance = 10_m;
    virtualAgent.lanetype = data.type;
    virtualAgents.push_back(virtualAgent);
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVirtualAgents()).WillByDefault(Return(virtualAgents));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(data.laneExist));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsDataForLaneChangeReliable(_)).WillByDefault(Return(data.isDataReliable));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSideSideLaneObjectChangingIntoSide(_, _)).WillByDefault(Return(data.isSideObjectChangingIntoSide));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(4.0_s));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(data.aoi));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(-0.6));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));

  NiceMock<FakeSurroundingVehicle> fakeVehicleSide;
  std::vector<const SurroundingVehicleInterface*> vehiclesSide{};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleVector(_)).WillByDefault(Return(vehiclesSide));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToStartOfNextExit()).WillByDefault(Return(0.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfNextExit()).WillByDefault(Return(0.0_m));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  LaneInformationGeometrySCM laneInfo;
  laneInfo.distanceToEndOfLane = 6.0_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(5.0_m));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(0));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(1));

  Merge::Gap gap{&fakeVehicle, nullptr};
  gap.distance = 15.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(gap));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.isLaneChangeProhibitionIgnored = false;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  GeometryInformationSCM geoInfo;
  geoInfo.Close().laneType = scm::LaneType::Driving;

  TrafficRuleInformationScmExtended trafficInfo;
  scm::LaneMarking::Entity entity{};
  entity.relativeStartDistance = 1.0_m;
  trafficInfo.Close().laneMarkingsLeft = {entity};

  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&geoInfo));
  ON_CALL(fakeInfrastructureCharacteristsics, GetTrafficRuleInformation()).WillByDefault(Return(&trafficInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsShoulderLaneUsageLegit(_)).WillByDefault(Return(true));

  auto result = TEST_HELPER.featureExtractor.IsSideLaneSafe(Side::Left);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsSideLaneSafe,
    testing::Values(
        DataFor_IsSideLaneSafe{
            .laneExist = true,
            .isDataReliable = true,
            .isSideObjectChangingIntoSide = false,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = false,
            .type = scm::LaneType::OnRamp,
            .expectedResult = true},
        DataFor_IsSideLaneSafe{
            .laneExist = false,
            .isDataReliable = true,
            .isSideObjectChangingIntoSide = false,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = false,
            .type = scm::LaneType::OnRamp,
            .expectedResult = false},
        DataFor_IsSideLaneSafe{
            .laneExist = true,
            .isDataReliable = false,
            .isSideObjectChangingIntoSide = false,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = false,
            .type = scm::LaneType::OnRamp,
            .expectedResult = false},
        DataFor_IsSideLaneSafe{
            .laneExist = true,
            .isDataReliable = true,
            .isSideObjectChangingIntoSide = true,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = false,
            .type = scm::LaneType::OnRamp,
            .expectedResult = false},
        DataFor_IsSideLaneSafe{
            .laneExist = true,
            .isDataReliable = true,
            .isSideObjectChangingIntoSide = false,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = true,
            .type = scm::LaneType::Driving,
            .expectedResult = false},
        DataFor_IsSideLaneSafe{
            .laneExist = true,
            .isDataReliable = true,
            .isSideObjectChangingIntoSide = false,
            .aoi = AreaOfInterest::LEFT_SIDE,
            .isVirtualAgents = true,
            .type = scm::LaneType::OnRamp,
            .expectedResult = true}));

/********************
 * CHECK IsLaneSafe *
 ********************/

struct DataFor_IsLaneSafe
{
  Side side;
  bool isMandatoryExit;
  scm::LaneType type;
  double urgencyFactor;
  bool isSideVehicles;
  bool expectedResult;
};

class FeatureExtractor_IsLaneSafe : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_IsLaneSafe>
{
};

TEST_P(FeatureExtractor_IsLaneSafe, Check_IsLaneSafe)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsLaneSafe data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(4.0_s));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_SIDE));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(-0.6));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));

  NiceMock<FakeSurroundingVehicle> fakeVehicleSide;
  std::vector<const SurroundingVehicleInterface*> vehiclesSide{};
  if (data.isSideVehicles)
  {
    vehiclesSide.push_back(&fakeVehicleSide);
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleVector(_)).WillByDefault(Return(vehiclesSide));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT)).WillByDefault(Return(data.isMandatoryExit));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToStartOfNextExit()).WillByDefault(Return(1.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfNextExit()).WillByDefault(Return(1.0_m));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  LaneInformationGeometrySCM laneInfo;
  laneInfo.distanceToEndOfLane = 6.0_m;
  laneInfo.laneType = data.type;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(8.0_m));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForLaneChange()).WillByDefault(Return(data.urgencyFactor));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(5.0_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(1));

  // TODO also test the case that ego is not currently merging 
  Merge::Gap gap{&fakeVehicle, nullptr};
  gap.distance = 15.0_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(gap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(
    LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT)
  ));

  auto result = TEST_HELPER.featureExtractor.IsLaneSafe(data.side);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsLaneSafe,
    testing::Values(
        DataFor_IsLaneSafe{
            .side = Side::Left,
            .isMandatoryExit = false,
            .type = scm::LaneType::Shoulder,
            .urgencyFactor = 0,
            .isSideVehicles = false,
            .expectedResult = true},
        DataFor_IsLaneSafe{
            .side = Side::Left,
            .isMandatoryExit = false,
            .type = scm::LaneType::Shoulder,
            .urgencyFactor = 1,
            .isSideVehicles = false,
            .expectedResult = false},
        DataFor_IsLaneSafe{
            .side = Side::Left,
            .isMandatoryExit = false,
            .type = scm::LaneType::Driving,
            .urgencyFactor = 1,
            .isSideVehicles = false,
            .expectedResult = false},
        DataFor_IsLaneSafe{
            .side = Side::Left,
            .isMandatoryExit = true,
            .type = scm::LaneType::Driving,
            .urgencyFactor = 1,
            .isSideVehicles = false,
            .expectedResult = false},
        DataFor_IsLaneSafe{
            .side = Side::Left,
            .isMandatoryExit = true,
            .type = scm::LaneType::Driving,
            .urgencyFactor = 1,
            .isSideVehicles = true,
            .expectedResult = false},
        DataFor_IsLaneSafe{
            .side = Side::Right,
            .isMandatoryExit = false,
            .type = scm::LaneType::Shoulder,
            .urgencyFactor = 0,
            .isSideVehicles = false,
            .expectedResult = true}));

/**************************
 * CHECK IsLaneChangeSafe *
 **************************/

struct DataFor_IsLaneChangeSafe
{
  bool isTransitionTrigger;
  SurroundingLane direction;
  bool existLane;
  bool hasSurroundingVehicle;
  bool isCannotClearLongitudinalObstructionBeforeCollision;
  units::length::meter_t minDistance;
  bool expectedResult;
};

class FeatureExtractor_IsLaneChangeSafe : public ::testing::Test,
                                          public ::testing::WithParamInterface<DataFor_IsLaneChangeSafe>
{
};

TEST_P(FeatureExtractor_IsLaneChangeSafe, Check_IsLaneChangeSafe)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_IsLaneChangeSafe data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentDirection()).WillByDefault(Return(data.direction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, true)).WillByDefault(Return(data.existLane));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeVehicle, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, IsStatic()).WillByDefault(Return(false));
  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(1.0_s));
  ON_CALL(fakeVehicle, GetTauDot()).WillByDefault(Return(-0.6));
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicleQueryFactory> fakeQueryFactory;
  auto fakeVehicleQuery = std::make_shared<FakeSurroundingVehicleQuery>();
  ON_CALL(fakeQueryFactory, GetQuery(_)).WillByDefault(Return(fakeVehicleQuery));
  ON_CALL(*fakeVehicleQuery, IsChangingIntoEgoLane).WillByDefault(Return(true));
  ON_CALL(*fakeVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision()).WillByDefault(Return(data.isCannotClearLongitudinalObstructionBeforeCollision));

  ObstructionDynamics obstructionDynamics;
  obstructionDynamics.timeToLeave = 0.5_s;
  ON_CALL(*fakeVehicleQuery, GetLateralObstructionDynamics()).WillByDefault(Return(obstructionDynamics));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetEqDistance(_)).WillByDefault(Return(100._m));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetMinDistance(_, _, _)).WillByDefault(Return(data.minDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(2));

  TEST_HELPER.featureExtractor.SetSurroundingVehicleQueryFactory(&fakeQueryFactory);

  auto result = TEST_HELPER.featureExtractor.IsLaneChangeSafe(data.isTransitionTrigger);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_IsLaneChangeSafe,
    testing::Values(
        DataFor_IsLaneChangeSafe{
            .isTransitionTrigger = true,
            .direction = SurroundingLane::LEFT,
            .existLane = true,
            .hasSurroundingVehicle = true,
            .isCannotClearLongitudinalObstructionBeforeCollision = false,
            .minDistance = 100_m,
            .expectedResult = true},
        DataFor_IsLaneChangeSafe{
            .isTransitionTrigger = true,
            .direction = SurroundingLane::LEFT,
            .existLane = true,
            .hasSurroundingVehicle = true,
            .isCannotClearLongitudinalObstructionBeforeCollision = true,
            .minDistance = 100_m,
            .expectedResult = false},
        DataFor_IsLaneChangeSafe{
            .isTransitionTrigger = false,
            .direction = SurroundingLane::LEFT,
            .existLane = false,
            .hasSurroundingVehicle = true,
            .isCannotClearLongitudinalObstructionBeforeCollision = true,
            .minDistance = 100_m,
            .expectedResult = false},
        DataFor_IsLaneChangeSafe{
            .isTransitionTrigger = false,
            .direction = SurroundingLane::EGO,
            .existLane = false,
            .hasSurroundingVehicle = true,
            .isCannotClearLongitudinalObstructionBeforeCollision = true,
            .minDistance = 100_m,
            .expectedResult = false}));

/***************************************
 * CHECK AnticipatedTimeToLaneCrossing *
 ***************************************/

struct DataFor_AnticipatedTimeToLaneCrossing
{
  RelativeLane lane;
  bool isExceedingLateralMotionThreshold;
  units::velocity::meters_per_second_t velocityTowardsEgoLane;
  units::time::second_t expectedResult;
};

class FeatureExtractor_AnticipatedTimeToLaneCrossing : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_AnticipatedTimeToLaneCrossing>
{
};

TEST_P(FeatureExtractor_AnticipatedTimeToLaneCrossing, Check_AnticipatedTimeToLaneCrossing)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_AnticipatedTimeToLaneCrossing data = GetParam();

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetLaneOfPerception()).WillByDefault(Return(data.lane));
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(fakeVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(data.isExceedingLateralMotionThreshold));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityTowardsEgoLane(_, _)).WillByDefault(Return(data.velocityTowardsEgoLane));

  auto result = TEST_HELPER.featureExtractor.AnticipatedTimeToLaneCrossing(&fakeVehicle);

  if (units::math::isinf(data.expectedResult))
  {
    ASSERT_TRUE(units::math::isinf(result));
  }
  else
  {
    ASSERT_EQ(result, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_AnticipatedTimeToLaneCrossing,
    testing::Values(
        DataFor_AnticipatedTimeToLaneCrossing{
            .lane = RelativeLane::EGO,
            .isExceedingLateralMotionThreshold = false,
            .velocityTowardsEgoLane = 10.0_mps,
            .expectedResult = 0.0_s},
        DataFor_AnticipatedTimeToLaneCrossing{
            .lane = RelativeLane::LEFT,
            .isExceedingLateralMotionThreshold = false,
            .velocityTowardsEgoLane = 10.0_mps,
            .expectedResult = ScmDefinitions::INF_TIME},
        DataFor_AnticipatedTimeToLaneCrossing{
            .lane = RelativeLane::LEFT,
            .isExceedingLateralMotionThreshold = true,
            .velocityTowardsEgoLane = -10.0_mps,
            .expectedResult = 0.0_s}));

/************************************
 * CHECK GetTimeAtTargetSpeedInLane *
 ************************************/

struct DataFor_GetTimeAtTargetSpeedInLane
{
  SurroundingLane lane;
  bool isLaneExist;
  units::velocity::meters_per_second_t absoluteVelocity;
  units::time::second_t expectedResult;
};

class FeatureExtractor_GetTimeAtTargetSpeedInLane : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_GetTimeAtTargetSpeedInLane>
{
};

TEST_P(FeatureExtractor_GetTimeAtTargetSpeedInLane, Check_GetTimeAtTargetSpeedInLane)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_GetTimeAtTargetSpeedInLane data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(data.isLaneExist));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(10_m));
  ON_CALL(fakeVehicle, GetAbsoluteVelocity()).WillByDefault(Return(5_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(data.absoluteVelocity));

  auto result = TEST_HELPER.featureExtractor.GetTimeAtTargetSpeedInLane(data.lane);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_GetTimeAtTargetSpeedInLane,
    testing::Values(
        DataFor_GetTimeAtTargetSpeedInLane{
            .lane = SurroundingLane::LEFT,
            .isLaneExist = false,
            .absoluteVelocity = 10.0_mps,
            .expectedResult = 0.0_s},
        DataFor_GetTimeAtTargetSpeedInLane{
            .lane = SurroundingLane::LEFT,
            .isLaneExist = true,
            .absoluteVelocity = 3.0_mps,
            .expectedResult = 99.0_s},
        DataFor_GetTimeAtTargetSpeedInLane{
            .lane = SurroundingLane::RIGHT,
            .isLaneExist = true,
            .absoluteVelocity = 3.0_mps,
            .expectedResult = 99.0_s}));

/******************************
 * CHECK HasDrivableSuccessor *
 ******************************/

struct DataFor_HasDrivableSuccessor
{
  bool isEmergency;
  RelativeLane lane;
  bool hasSuccessor;
  scm::LaneType successorType;
  scm::LaneType laneType;
  bool expectedResult;
};

class FeatureExtractor_HasDrivableSuccessor : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_HasDrivableSuccessor>
{
};

TEST_P(FeatureExtractor_HasDrivableSuccessor, Check_HasDrivableSuccessor)
{
  FeatureExtractorTester TEST_HELPER;
  DataFor_HasDrivableSuccessor data = GetParam();

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;

  if (data.hasSuccessor)
  {
    LaneInformationGeometrySCM laneInfo;
    laneInfo.laneTypeSuccessor = data.successorType;
    laneInfo.laneType = data.laneType;
    ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  }
  else
  {
    LaneInformationGeometrySCM laneInfo;
    ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  auto result = TEST_HELPER.featureExtractor.HasDrivableSuccessor(data.isEmergency, data.lane);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    FeatureExtractor_HasDrivableSuccessor,
    testing::Values(
        DataFor_HasDrivableSuccessor{
            .isEmergency = true,
            .lane = RelativeLane::RIGHT,
            .hasSuccessor = false,
            .successorType = scm::LaneType::Shoulder,
            .laneType = scm::LaneType::Driving,
            .expectedResult = false},
        DataFor_HasDrivableSuccessor{
            .isEmergency = true,
            .lane = RelativeLane::RIGHT,
            .hasSuccessor = false,
            .successorType = scm::LaneType::Shoulder,
            .laneType = scm::LaneType::Driving,
            .expectedResult = false},
        DataFor_HasDrivableSuccessor{
            .isEmergency = true,
            .lane = RelativeLane::RIGHT,
            .hasSuccessor = true,
            .successorType = scm::LaneType::Shoulder,
            .laneType = scm::LaneType::Driving,
            .expectedResult = true},
        DataFor_HasDrivableSuccessor{
            .isEmergency = false,
            .lane = RelativeLane::RIGHT,
            .hasSuccessor = true,
            .successorType = scm::LaneType::Shoulder,
            .laneType = scm::LaneType::Driving,
            .expectedResult = true},
        DataFor_HasDrivableSuccessor{
            .isEmergency = false,
            .lane = RelativeLane::RIGHT,
            .hasSuccessor = true,
            .successorType = scm::LaneType::Shoulder,
            .laneType = scm::LaneType::Shoulder,
            .expectedResult = false}));