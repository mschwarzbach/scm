/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <ostream>
#include <vector>

#include "Fakes/FakeActionImplementation.h"
#include "Fakes/FakeActionManager.h"
#include "Fakes/FakeAlgorithmSceneryCar.h"
#include "Fakes/FakeAuditoryPerception.h"
#include "Fakes/FakeGazeControl.h"
#include "Fakes/FakeGazeFollower.h"
#include "Fakes/FakeIgnoringOuterLaneOvertakingProhibition.h"
#include "Fakes/FakeInfrastructureCharacteristics.h"
#include "Fakes/FakeLaneChangeBehavior.h"
#include "Fakes/FakeMentalCalculations.h"
#include "Fakes/FakeMentalModel.h"
#include "Fakes/FakeMicroscopicCharacteristics.h"
#include "Fakes/FakePeriodicLogger.h"
#include "Fakes/FakePublisher.h"
#include "Fakes/FakeScmComponents.h"
#include "Fakes/FakeScmDependencies.h"
#include "Fakes/FakeSituationManager.h"
#include "Fakes/FakeSurroundingVehicle.h"
#include "Fakes/FakeSwerving.h"
#include "PeriodicLogger.h"
#include "ScmLifetimeState.h"
#include "ScmSignalCollector.h"
#include "include/signal/driverOutput.h"

using ::testing::_;
using ::testing::Matcher;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct ScmLifetimeStateTester
{
  class ScmLifetimeStateUnderTest : ScmLifetimeState
  {
  public:
    template <typename... Args>
    ScmLifetimeStateUnderTest(Args&&... args)
        : ScmLifetimeState{std::forward<Args>(args)...} {};

    void DetermineAgentStates()
    {
    }

    using ScmLifetimeState::CheckForEndOfLateralMovement;
    using ScmLifetimeState::HandleExternalControl;
    using ScmLifetimeState::ImplementAgentStates;
    using ScmLifetimeState::ResetBaseTimes;
    using ScmLifetimeState::ResetLateralMovement;
    using ScmLifetimeState::SendDriverSimulationOutput;
    using ScmLifetimeState::SendScmPerceptionSimulationOutputInformation;
    using ScmLifetimeState::SetFinalParameters;
    using ScmLifetimeState::SetInitialParameters;
    using ScmLifetimeState::UpdateAgentInformation;

    void SET_NEW_EGO_LANE(bool isNewEgoLane)
    {
      _isNewEgoLane = isNewEgoLane;
    }

    void SET_EXTERNAL_CONTROL(ExternalControlState externalControlState)
    {
      _externalControlState = externalControlState;
    }
  };

  ScmLifetimeStateTester()
      : fakeScmComponents{},
        fakeMentalModel{},
        fakePeriodicLogger{},
        _externalControlState{},
        scmLifetimeState(componentName,
                         cycleTime,
                         fakePeriodicLogger,
                         fakeScmComponents,
                         _externalControlState)
  {
  }
  std::string componentName = "abc";
  units::time::millisecond_t cycleTime = 100_ms;
  ExternalControlState _externalControlState;
  ScmLifetimeStateUnderTest scmLifetimeState;
  NiceMock<FakePeriodicLogger>* fakePeriodicLogger;
  NiceMock<FakeScmComponents> fakeScmComponents;
  NiceMock<FakeMentalModel> fakeMentalModel;
};

/************************
 * SetInitialParameters *
 ************************/

struct DataFor_SetInitialParameters
{
  units::time::millisecond_t time;
  int LaneChangePreventionAtSpawnTrue;
  int LaneChangePreventionAtSpawnFalse;
  int urgentUpdateThreshold;
  int LaneChangePreventionExternalControlTrue;
  int LaneChangePreventionExternalControlFalse;
};

class ScmLifeTimeState_SetInitialParameters : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_SetInitialParameters>
{
};

TEST_P(ScmLifeTimeState_SetInitialParameters, Check_SetInitialParameters)
{
  ScmLifetimeStateTester TEST_HELPER;
  DataFor_SetInitialParameters data = GetParam();

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeScmDependencies> fakeScmDependencies;

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetTime(100_ms)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, AdvanceReactionBaseTime()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, AdvanceAdjustmentBaseTime()).Times(1);

  OwnVehicleInformationScmExtended ownVehicleData;
  EXPECT_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).Times(1).WillOnce(Return(&ownVehicleData));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTime()).WillByDefault(Return(data.time));
  ON_CALL(fakeScmDependencies, GetSpawnTime).WillByDefault(Return(0_ms));

  OwnVehicleInformationSCM ownVehicleInformation;
  ownVehicleInformation.longitudinalVelocity = 10.0_mps;
  ownVehicleInformation.longitudinalPosition = 100.0_m;
  ON_CALL(fakeScmDependencies, GetOwnVehicleInformationScm()).WillByDefault(Return(&ownVehicleInformation));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionAtSpawn(true)).Times(data.LaneChangePreventionExternalControlTrue);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionAtSpawn(false)).Times(data.LaneChangePreventionExternalControlFalse);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetUrgentUpdateThreshold(_)).Times(data.urgentUpdateThreshold);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePreventionAtSpawn()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePreventionExternalControl()).WillByDefault(Return(true));

  ExternalControlState externalControlState;
  externalControlState.lateralDeactivationTime = 10_ms;
  TEST_HELPER.scmLifetimeState.SET_EXTERNAL_CONTROL(externalControlState);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionExternalControl(true)).Times(data.LaneChangePreventionAtSpawnTrue);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionExternalControl(false)).Times(data.LaneChangePreventionAtSpawnFalse);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetScmDependencies()).WillByDefault(Return(&fakeScmDependencies));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.SetInitialParameters(100_ms);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmLifeTimeState_SetInitialParameters,
    testing::Values(
        DataFor_SetInitialParameters{
            .time = 100_ms,
            .LaneChangePreventionAtSpawnTrue = 1,
            .LaneChangePreventionAtSpawnFalse = 0,
            .urgentUpdateThreshold = 1,
            .LaneChangePreventionExternalControlTrue = 1,
            .LaneChangePreventionExternalControlFalse = 0},
        DataFor_SetInitialParameters{
            .time = 3200_ms,
            .LaneChangePreventionAtSpawnTrue = 0,
            .LaneChangePreventionAtSpawnFalse = 1,
            .urgentUpdateThreshold = 2,
            .LaneChangePreventionExternalControlTrue = 0,
            .LaneChangePreventionExternalControlFalse = 1}));

/**************************
 * UpdateAgentInformation *
 **************************/

TEST(ScmLifeTimeState_UpdateAgentInformation, Check_UpdateAgentInformation)
{
  ScmLifetimeStateTester TEST_HELPER;

  TEST_HELPER.scmLifetimeState.SET_NEW_EGO_LANE(true);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, Transition()).Times(1);

  NiceMock<FakeGazeFollower> fakeGazeFollower;
  ON_CALL(fakeGazeFollower, GetThrowWarning()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeFollower()).WillByDefault(Return(&fakeGazeFollower));

  NiceMock<FakeGazeControl> fakeGazeControl;
  EXPECT_CALL(fakeGazeControl, AdvanceGazeState(_)).Times(1);
  ON_CALL(fakeGazeControl, GetCurrentGazeState()).WillByDefault(Return(GazeState::DISTRACTION));
  ON_CALL(fakeGazeControl, IsNewInformationAcquisitionRequested()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  NiceMock<FakeScmDependencies> fakeScmDependencies;

  TrafficRuleInformationSCM trafficRule;
  ON_CALL(fakeScmDependencies, GetTrafficRuleInformationScm()).WillByDefault(Return(&trafficRule));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetScmDependencies()).WillByDefault(Return(&fakeScmDependencies));
  EXPECT_CALL(fakeInfrastructureCharacteristics, SetTrafficRuleInformation(&trafficRule)).Times(1);

  GeometryInformationSCM geoInfo;
  ON_CALL(fakeScmDependencies, GetGeometryInformationScm()).WillByDefault(Return(&geoInfo));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).Times(1).WillOnce(Return(10_mps));

  OwnVehicleInformationSCM ownVehicleInformation;
  ON_CALL(fakeScmDependencies, GetOwnVehicleInformationScm()).WillByDefault(Return(&ownVehicleInformation));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLaneId()).Times(1).WillOnce(Return(1));

  EXPECT_CALL(fakeInfrastructureCharacteristics, SetGeometryInformation(_, _, _, _)).Times(1);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, CalculateMicroscopicData(true)).Times(1);

  NiceMock<FakeAuditoryPerception> fakeAuditoryPerception;
  EXPECT_CALL(fakeAuditoryPerception, UpdateStimulusData(_)).Times(1);
  ON_CALL(TEST_HELPER.fakeScmComponents, GetAuditoryPerception()).WillByDefault(Return(&fakeAuditoryPerception));

  TrafficRulesScm trafficRules;
  trafficRules.common.rightHandTraffic = Rule<bool>{trafficRules.common.rightHandTraffic.applicable, true};
  ON_CALL(fakeScmDependencies, GetTrafficRulesScm()).WillByDefault(Return(&trafficRules));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  SurroundingObjectsScmExtended surroundingObjects;
  ObjectInformationScmExtended vehicle;
  vehicle.id = 1;
  std::vector<ObjectInformationScmExtended> vehicles{vehicle};
  surroundingObjects.Ahead().Left() = vehicles;
  surroundingObjects.Ahead().Close() = vehicles;
  ON_CALL(fakeMicroscopicCharacteristics, GetSurroundingVehicleInformation()).WillByDefault(Return(&surroundingObjects));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  NiceMock<FakeIgnoringOuterLaneOvertakingProhibition> fakeIgnoringOuterLaneOvertakingProhibition;
  EXPECT_CALL(fakeIgnoringOuterLaneOvertakingProhibition, Update(_, _)).Times(1);
  ON_CALL(TEST_HELPER.fakeScmComponents, GetIgnoringOuterLaneOvertakingProhibition()).WillByDefault(Return(&fakeIgnoringOuterLaneOvertakingProhibition));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetInformationRequests()).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.UpdateAgentInformation(100_ms);
}

/************************
 * ImplementAgentStates *
 ************************/

struct DataFor_ImplementAgentStates
{
  ExternalControlState state;
  int implementActionTimes;
  int updateInformationFromActionManagerTimes;
  int calculateAdjustmentBaseTimeTimes;
  int forceLaneChangeTimes;
  int resetReactionBaseTimeTimes;
};

class ScmLifeTimeState_ImplementAgentStates : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_ImplementAgentStates>
{
};

TEST_P(ScmLifeTimeState_ImplementAgentStates, Check_ImplementAgentStates)
{
  ScmLifetimeStateTester TEST_HELPER;
  DataFor_ImplementAgentStates data = GetParam();

  TEST_HELPER.scmLifetimeState.SET_EXTERNAL_CONTROL(data.state);

  NiceMock<FakeActionImplementation> fakeActionImplementation;
  EXPECT_CALL(fakeActionImplementation, ImplementAction()).Times(data.implementActionTimes);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, CalculateAdjustmentBaseTime(_)).Times(data.calculateAdjustmentBaseTimeTimes);

  NiceMock<FakeActionManager> fakeActionManager;
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionManager()).WillByDefault(Return(&fakeActionManager));

  NiceMock<FakeAlgorithmSceneryCar> fakeAlgorithmSceneryCar;
  EXPECT_CALL(fakeAlgorithmSceneryCar, ForcedLaneChange(_, _, _)).Times(data.forceLaneChangeTimes);
  ON_CALL(fakeAlgorithmSceneryCar, ResetReactionBaseTime()).WillByDefault(Return(true));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.forcedLaneChangeStartTrigger = LaneChangeAction::ForcePositive;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetReactionBaseTime()).Times(data.resetReactionBaseTimeTimes);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetAlgorithmSceneryCar()).WillByDefault(Return(&fakeAlgorithmSceneryCar));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionImplementation()).WillByDefault(Return(&fakeActionImplementation));

  TEST_HELPER.scmLifetimeState.ImplementAgentStates();
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmLifeTimeState_ImplementAgentStates,
    testing::Values(
        DataFor_ImplementAgentStates{
            .state{
                .longitudinalActive = true,
                .lateralActive = true,
                .lateralDeactivationTime = 100_ms},
            .implementActionTimes = 1,
            .updateInformationFromActionManagerTimes = 0,
            .calculateAdjustmentBaseTimeTimes = 0,
            .forceLaneChangeTimes = 0,
            .resetReactionBaseTimeTimes = 0},
        DataFor_ImplementAgentStates{
            .state{
                .longitudinalActive = false,
                .lateralActive = false,
                .lateralDeactivationTime = 100_ms},
            .implementActionTimes = 1,
            .updateInformationFromActionManagerTimes = 1,
            .calculateAdjustmentBaseTimeTimes = 1,
            .forceLaneChangeTimes = 1,
            .resetReactionBaseTimeTimes = 1}));

/**********************
 * SetFinalParameters *
 **********************/

TEST(ScmLifeTimeState_SetFinalParameters, Check_SetFinalParameters)
{
  ScmLifetimeStateTester TEST_HELPER;

  std::map<AreaOfInterest, double> aoiScoring{};
  ON_CALL(TEST_HELPER.fakeMentalModel, GenerateTopDownAoiScoring(_)).WillByDefault(Return(aoiScoring));

  LateralAction action{};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(action));
  DriverParameters driverParameter{};
  driverParameter.distractionPercentage = 1.0;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameter));

  NiceMock<FakeGazeControl> fakeGazeControl;
  NiceMock<FakeAuditoryPerception> fakeAuditoryPerception;
  EXPECT_CALL(fakeGazeControl, UpdateGazeRequests(_, _, _, _, _, _)).Times(1);

  NiceMock<FakeSituationManager> fakeSituationManager;
  Situation currentSituation;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(currentSituation));
  EXPECT_CALL(fakeSituationManager, SetSituationLastTick(_)).Times(1);

  NiceMock<FakeActionManager> fakeActionManager;
  LongitudinalActionState longitudinalActionState{};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalActionState()).WillByDefault(Return(longitudinalActionState));
  EXPECT_CALL(fakeActionManager, SetActionSubStateLastTick(_)).Times(1);

  NiceMock<FakeActionImplementation> fakeActionImplementation;
  EXPECT_CALL(fakeActionImplementation, GetNextIndicatorState()).Times(1).WillOnce(Return(scm::LightState::Indicator::Off));
  EXPECT_CALL(fakeActionImplementation, GetFlasherActiveNext()).Times(1).WillOnce(Return(false));
  EXPECT_CALL(fakeActionImplementation, GetLongitudinalAccelerationWish()).Times(1).WillOnce(Return(1.0_mps_sq));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  GeometryInformationSCM geoInfo;
  EXPECT_CALL(fakeInfrastructureCharacteristics, GetGeometryInformation()).Times(1).WillOnce(Return(&geoInfo));

  EXPECT_CALL(fakeActionImplementation, GetLateralDeviationGain()).Times(1).WillOnce(Return(2.0_rad_per_s_sq));
  EXPECT_CALL(fakeActionImplementation, GetLateralDeviationNext()).Times(1).WillOnce(Return(3.0_m));
  EXPECT_CALL(fakeActionImplementation, GetHeadingErrorGain()).Times(1).WillOnce(Return(4.0_Hz));
  EXPECT_CALL(fakeActionImplementation, GetHeadingErrorNext()).Times(1).WillOnce(Return(5.0_rad));
  EXPECT_CALL(fakeActionImplementation, GetKappaManoeuvre()).Times(1).WillOnce(Return(6.0_i_m));
  EXPECT_CALL(fakeActionImplementation, GetKappaRoad()).Times(1).WillOnce(Return(7.0_i_m));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended ownVehicleData{};
  EXPECT_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).Times(1).WillOnce(Return(&ownVehicleData));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCollisionState()).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionImplementation()).WillByDefault(Return(&fakeActionImplementation));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionManager()).WillByDefault(Return(&fakeActionManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetSituationManager()).WillByDefault(Return(&fakeSituationManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetAuditoryPerception()).WillByDefault(Return(&fakeAuditoryPerception));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetAuditoryPerception()).WillByDefault(Return(&fakeAuditoryPerception));
  TEST_HELPER.scmLifetimeState.SetFinalParameters();
}

/******************************
 * SendDriverSimulationOutput *
 *****************************/

TEST(ScmLifeTimeState_SendDriverSimulationOutput, Check_SendDriverSimulationOutput)
{
  ScmLifetimeStateTester TEST_HELPER;
  NiceMock<FakePublisher> fakePublisher;
  NiceMock<FakeGazeControl> fakeGazeControl;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  NiceMock<FakeActionImplementation> fakeActionImplementation;
  NiceMock<FakeLaneChangeBehavior> fakeLaneChangeBehavior;

  LateralAction lateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(lateralAction));

  EXPECT_CALL(fakePublisher, Publish("Driver.LateralActionState", _)).Times(1);

  NiceMock<FakeScmDependencies> fakeScmDependencies;
  DriverParameters driverParameter;
  driverParameter.previewDistance = 100.0_m;
  ON_CALL(fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameter));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetScmDependencies()).WillByDefault(Return(&fakeScmDependencies));
  EXPECT_CALL(fakePublisher, Publish("Driver.PreviewDistance", _)).Times(1);

  LongitudinalActionState longitudinalActionState;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalActionState()).WillByDefault(Return(longitudinalActionState));
  EXPECT_CALL(fakePublisher, Publish("Driver.LongitudinalActionState", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::LANE_CHANGER_FROM_LEFT));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsSituationAnticipated(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetSituationRisk(_)).WillByDefault(Return(Risk::LOW));
  EXPECT_CALL(fakePublisher, Publish("Driver.CurrentSituation", _)).Times(1);

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.fovea = AreaOfInterest::EGO_FRONT;
  ownVehicleInfo.lateralOffsetNeutralPositionScaled = 0.5_m;
  ownVehicleInfo.posX = 100_m;
  ownVehicleInfo.posY = 1.75_m;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));
  ON_CALL(fakeGazeControl, GetCurrentGazeState).WillByDefault(Return(GazeState::EGO_FRONT));
  EXPECT_CALL(fakePublisher, Publish("Driver.AOI", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GazeState", _)).Times(1);

  std::vector<MesoscopicSituation> mesoscopicSituations{MesoscopicSituation::FREE_DRIVING};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetActiveMesoscopicSituations()).WillByDefault(Return(mesoscopicSituations));
  EXPECT_CALL(fakePublisher, Publish("Driver.MesoscopicSituation", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicleId()).WillByDefault(Return(2));
  EXPECT_CALL(fakePublisher, Publish("Driver.SCMLeadingVehicle", _)).Times(1);

  ObjectInformationScmExtended vehicle;
  vehicle.id = 2;
  std::vector<ObjectInformationScmExtended> vehicles{vehicle};
  SurroundingObjectsScmExtended surroundingVehicles;
  surroundingVehicles.Ahead().Close() = vehicles;
  ON_CALL(fakeMicroscopicCharacteristics, GetSurroundingVehicleInformation()).WillByDefault(Return(&surroundingVehicles));
  EXPECT_CALL(fakePublisher, Publish("Driver.AgentInFront", _)).Times(1);

  std::map<std::string, std::string> debugValues;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDebugValues()).WillByDefault(Return(debugValues));

  EXPECT_CALL(fakePublisher, Publish("Driver.LateralOffset", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(0.4_m));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralPosition", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPositionFrontAxle()).WillByDefault(Return(1.5_m));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralPositionFrontAxle", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetReactionBaseTime()).WillByDefault(Return(1_s));
  EXPECT_CALL(fakePublisher, Publish("Driver.isDecisionPossible", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.ReactionBaseTime", _)).Times(1);

  LaneInformationGeometrySCM laneInfoSCM;
  laneInfoSCM.laneType = scm::LaneType::Driving;
  laneInfoSCM.width = 3.0_m;
  ON_CALL(fakeInfrastructureCharacteristics, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRef(laneInfoSCM));
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneType", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneId()).WillByDefault(Return(1));
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneId", _)).Times(1);

  EXPECT_CALL(fakePublisher, Publish("Driver.EgoLaneWidth", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(Matcher<int>(_), _)).WillByDefault(Return(2.0_m));
  EXPECT_CALL(fakePublisher, Publish("Driver.DistanceToLeftEOL", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.DistanceToEgoEOL", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.DistanceToRightEOL", _)).Times(1);

  ON_CALL(fakeActionImplementation, GetLateralDisplacementTraveled()).WillByDefault(Return(3.0_m));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralDisplacementTraveled", _)).Times(1);
  ON_CALL(fakeActionImplementation, GetDesiredLateralDisplacement()).WillByDefault(Return(4.0_m));
  EXPECT_CALL(fakePublisher, Publish("Driver.DesiredLateralDisplacement", _)).Times(1);
  ON_CALL(fakeActionImplementation, GetLateralDisplacementReached()).WillByDefault(Return(5.0));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralDisplacementReached", _)).Times(1);

  const std::optional<TrajectoryPlanning::TrajectoryDimensions> laneChangeDimensions;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPlannedLaneChangeDimensions()).WillByDefault(ReturnRef(laneChangeDimensions));
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneChangeLength", _)).Times(1);

  Merge::Gap mergeGap{nullptr, nullptr};
  mergeGap.approachingVelocity = 19_mps;
  mergeGap.fullMergeSpace = 20_m;
  mergeGap.velocity = 21_mps;
  mergeGap.distance = 22_m;
  mergeGap.leaderId = 23;
  mergeGap.followerId = 24;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulate()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  EXPECT_CALL(fakePublisher, Publish("Driver.GapMergeRegulate", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeRegulateId()).WillByDefault(Return(2));
  EXPECT_CALL(fakePublisher, Publish("Driver.GapMergeRegulateId", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapApproachingV", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapFullMergeSpace", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapDistanceToEndOfLane", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapVelocity", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapDistance", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapSafetyClearanceToLeader", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapSafetyClearanceToFollower", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapPosition", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapLeaderId", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.GapFollowerId", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetZipMerge()).WillByDefault(Return(false));
  EXPECT_CALL(fakePublisher, Publish("Driver.isZipMerge", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAdjustmentBaseTime()).WillByDefault(Return(1_s));
  EXPECT_CALL(fakePublisher, Publish("Driver.reevaluateLongitudinalRegulation", _)).Times(1);

  ON_CALL(fakeLaneChangeBehavior, GetLaneConvenience(_)).WillByDefault(Return(2));
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneConvenienceLeft", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneConvenienceRight", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.LaneConvenienceEgo", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(25_mps));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralVelocity", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityFrontAxle()).WillByDefault(Return(25_mps));
  EXPECT_CALL(fakePublisher, Publish("Driver.LateralVelocityFrontAxle", _)).Times(1);

  std::vector<AreaOfInterest> aois{AreaOfInterest::EGO_FRONT};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetUfov()).WillByDefault(Return(aois));
  EXPECT_CALL(fakePublisher, Publish("Driver.UFOV", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInconsistentAois()).WillByDefault(ReturnRef(aois));
  EXPECT_CALL(fakePublisher, Publish("Driver.InconsistentAois", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetHorizontalGazeAngle()).WillByDefault(Return(26_rad));
  EXPECT_CALL(fakePublisher, Publish("Driver.GazeAngle", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(27_mps));
  EXPECT_CALL(fakePublisher, Publish("Driver.VelocityMeanLeft", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.VelocityMeanRight", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.VelocityMeanEgo", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVisualReliability(_)).WillByDefault(Return(28));
  EXPECT_CALL(fakePublisher, Publish("Driver.Reliability FRONT", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Reliability REAR", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Reliability LEFT", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Reliability RIGHT", _)).Times(1);

  ON_CALL(fakeActionImplementation, GetLongitudinalAccelerationWish()).WillByDefault(Return(29_mps_sq));
  EXPECT_CALL(fakePublisher, Publish("Driver.AccelerationWish", _)).Times(1);

  std::vector<int> possibleRegulateIds{1};
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPossibleRegulateIds()).WillByDefault(Return(possibleRegulateIds));
  EXPECT_CALL(fakePublisher, Publish("Driver.PossibleRegulateVehicles", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, IsFrontSituation()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(30_s));
  EXPECT_CALL(fakePublisher, Publish("Driver.TTC", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalEgo()).WillByDefault(Return(31_mps));
  EXPECT_CALL(fakePublisher, Publish("Driver.velocityLegalEgo", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(32_mps));
  EXPECT_CALL(fakePublisher, Publish("Driver.velocityTargetEgo", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(true));
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleEgoFront", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleEgoFrontFar", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleEgoRear", _)).Times(1);

  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftRear", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftSide", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftFront", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftFrontFar", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftLeftRear", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftLeftSide", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleLeftLeftFront", _)).Times(1);

  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightRear", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightSide", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightFront", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightFrontFar", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightRightRear", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightRightSide", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.vehicleVisibleRightRightFront", _)).Times(1);

  ON_CALL(fakeMentalCalculations, GetLaneChangeBehavior()).WillByDefault(ReturnRef(fakeLaneChangeBehavior));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalCalculations()).WillByDefault(Return(&fakeMentalCalculations));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionImplementation()).WillByDefault(Return(&fakeActionImplementation));
  TEST_HELPER.scmLifetimeState.SendDriverSimulationOutput(&fakePublisher);
}

/************************************************
 * SendScmPerceptionSimulationOutputInformation *
 ***********************************************/

TEST(ScmLifeTimeState_SendScmPerceptionSimulationOutputInformation, Check_SendScmPerceptionSimulationOutputInformation)
{
  ScmLifetimeStateTester TEST_HELPER;
  NiceMock<FakePublisher> fakePublisher;

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ObjectInformationScmExtended fakeVehicleInfo;
  fakeVehicleInfo.acceleration = 10_mps_sq;
  fakeVehicleInfo.longitudinalVelocity = 11_mps;
  fakeVehicleInfo.gap = 12_s;
  fakeVehicleInfo.ttc = 13_s;
  fakeVehicleInfo.heading = 14;
  fakeVehicleInfo.tauDot = 15;
  fakeVehicleInfo.ttcthreshold = 16_s;
  fakeVehicleInfo.relativeLongitudinalDistance = 17_m;
  fakeVehicleInfo.lateralVelocity = 18_mps;
  OrientationRelativeToDriver orientationRelativeToDriver;
  fakeVehicleInfo.orientationRelativeToDriver = orientationRelativeToDriver;
  ObstructionLongitudinal obstructionLongitudinal;
  obstructionLongitudinal.mainLaneLocator = 20_m;
  fakeVehicleInfo.longitudinalObstruction = obstructionLongitudinal;
  fakeVehicleInfo.relativeLateralDistance = 19_m;
  ON_CALL(fakeVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT_FAR));
  ON_CALL(fakeVehicle, GetObjectInformation()).WillByDefault(Return(&fakeVehicleInfo));
  ON_CALL(fakeVehicle, GetId()).WillByDefault(Return(2));
  ON_CALL(fakeVehicle, GetReliability(_)).WillByDefault(Return(2));

  OwnVehicleInformationScmExtended ownVehicleInfo;
  ownVehicleInfo.posX = 1000_m;
  ownVehicleInfo.posY = 3.75_m;

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&ownVehicleInfo));

  std::vector<const SurroundingVehicleInterface*> fakeSurroundingVehicles{&fakeVehicle};
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:acceleration", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:velocity", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:gap", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:ttc", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:heading", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:tauDot", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:ttcthreshold", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:netDistance", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:lateralVelocity", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:orientationRelativeToDriver", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:relativeDistance", _)).Times(1);
  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#-1:PosLonLat", _)).Times(1);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetSurroundingVehicles()).WillByDefault(Return(fakeSurroundingVehicles));

  EXPECT_CALL(fakePublisher, Publish("Driver.Perception.7#2:reliability", _)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.SendScmPerceptionSimulationOutputInformation(&fakePublisher);
}

/************************
 * ResetLateralMovement *
 ************************/

TEST(ScmLifeTimeState_ResetLateralMovement, Check_ResetLateralMovement)
{
  ScmLifetimeStateTester TEST_HELPER;
  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeActionManager> fakeActionManager;

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInRealignState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInLaneChangeState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInSwervingState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetLaneChangeWidthTraveled()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetIsEndOfLateralMovement(true)).Times(1);
  EXPECT_CALL(fakeSwerving, SetSwervingState(SwervingState::NONE)).Times(1);
  EXPECT_CALL(fakeSwerving, ResetAgentIdOfSwervingTarget()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetMergeRegulate()).Times(1);
  EXPECT_CALL(fakeActionManager, DetermineLongitudinalActionState()).Times(1);
  EXPECT_CALL(fakeActionManager, RunLeadingVehicleSelector()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLateralAction(_)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetSwerving()).WillByDefault(Return(&fakeSwerving));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionManager()).WillByDefault(Return(&fakeActionManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.ResetLateralMovement();
}

/********************************
 * CheckForEndOfLateralMovement *
 ********************************/

struct DataFor_CheckForEndOfLateralMovement
{
  LateralAction::Direction direction;
  LateralAction::State state;
  int desiredLateralDisplacementTimes;
  int lateralVelocityTimes;
  int lateralDisplacementReachedTimes;
  int updateAndCheckForEndOfSwervingTimes;
  bool isPastSwervingEndTime;
  int addInformationRequestTimes;
  int setSwervingEndTimeTimes;
};

class ScmLifeTimeState_CheckForEndOfLateralMovement : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_CheckForEndOfLateralMovement>
{
};

TEST_P(ScmLifeTimeState_CheckForEndOfLateralMovement, Check_CheckForEndOfLateralMovement)
{
  ScmLifetimeStateTester TEST_HELPER;
  DataFor_CheckForEndOfLateralMovement data = GetParam();

  NiceMock<FakeActionImplementation> fakeActionImplementation;
  NiceMock<FakeSwerving> fakeServing;

  LateralAction lateralAction{};
  lateralAction.direction = data.direction;
  lateralAction.state = data.state;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(lateralAction));

  EXPECT_CALL(fakeActionImplementation, GetDesiredLateralDisplacement).Times(data.desiredLateralDisplacementTimes).WillOnce(Return(1.0_m));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).Times(data.lateralVelocityTimes).WillOnce(Return(2.0_mps));
  EXPECT_CALL(fakeActionImplementation, GetLateralDisplacementReached).Times(data.lateralDisplacementReachedTimes).WillOnce(Return(3.0));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(1.0_mps));
  EXPECT_CALL(fakeServing, UpdateAndCheckForEndOfSwerving(_, _, _)).Times(data.updateAndCheckForEndOfSwervingTimes);

  ON_CALL(fakeServing, IsPastSwervingEndTime()).WillByDefault(Return(data.isPastSwervingEndTime));
  std::pair<AreaOfInterest, bool> aoiToSwerve;
  aoiToSwerve.first = AreaOfInterest::LEFT_FRONT;
  ON_CALL(fakeServing, GetAoiToSwerve()).WillByDefault(Return(aoiToSwerve));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, AddInformationRequest(_, _, _, _)).Times(data.addInformationRequestTimes);
  EXPECT_CALL(fakeServing, SetSwervingEndTime(units::make_unit<units::time::millisecond_t>(std::numeric_limits<double>::max()))).Times(data.setSwervingEndTimeTimes);

  if (data.direction == LateralAction::Direction::NONE)
  {
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInRealignState(false)).Times(1);
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInLaneChangeState(false)).Times(1);
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInSwervingState(false)).Times(1);
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetLaneChangeWidthTraveled()).Times(1);
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetIsEndOfLateralMovement(false)).Times(1);
    EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetTransitionState()).Times(1);
  }

  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetSwerving()).WillByDefault(Return(&fakeServing));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionImplementation()).WillByDefault(Return(&fakeActionImplementation));
  TEST_HELPER.scmLifetimeState.CheckForEndOfLateralMovement();
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmLifeTimeState_CheckForEndOfLateralMovement,
    testing::Values(
        DataFor_CheckForEndOfLateralMovement{
            .direction = LateralAction::Direction::LEFT,
            .state = LateralAction::State::LANE_CHANGING,
            .desiredLateralDisplacementTimes = 1,
            .lateralVelocityTimes = 1,
            .lateralDisplacementReachedTimes = 1,
            .updateAndCheckForEndOfSwervingTimes = 0,
            .isPastSwervingEndTime = true,
            .addInformationRequestTimes = 0},
        DataFor_CheckForEndOfLateralMovement{
            .direction = LateralAction::Direction::LEFT,
            .state = LateralAction::State::COMFORT_SWERVING,
            .desiredLateralDisplacementTimes = 1,
            .lateralVelocityTimes = 1,
            .lateralDisplacementReachedTimes = 0,
            .updateAndCheckForEndOfSwervingTimes = 1,
            .isPastSwervingEndTime = true,
            .addInformationRequestTimes = 1,
            .setSwervingEndTimeTimes = 1},
        DataFor_CheckForEndOfLateralMovement{
            .direction = LateralAction::Direction::LEFT,
            .state = LateralAction::State::COMFORT_SWERVING,
            .desiredLateralDisplacementTimes = 1,
            .lateralVelocityTimes = 1,
            .lateralDisplacementReachedTimes = 0,
            .updateAndCheckForEndOfSwervingTimes = 1,
            .isPastSwervingEndTime = false,
            .addInformationRequestTimes = 0,
            .setSwervingEndTimeTimes = 0},
        DataFor_CheckForEndOfLateralMovement{
            .direction = LateralAction::Direction::NONE,
            .state = LateralAction::State::LANE_KEEPING,
            .desiredLateralDisplacementTimes = 0,
            .lateralVelocityTimes = 0,
            .lateralDisplacementReachedTimes = 0,
            .updateAndCheckForEndOfSwervingTimes = 0,
            .isPastSwervingEndTime = false,
            .addInformationRequestTimes = 0,
            .setSwervingEndTimeTimes = 0}));

/******************
 * ResetBaseTimes *
 ******************/

TEST(ScmLifeTimeState_ResetBaseTimes, Check_ResetBaseTimes)
{
  ScmLifetimeStateTester TEST_HELPER;

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetReactionBaseTime()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetAdjustmentBaseTime(0.0_s)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.ResetBaseTimes();
}

/*************************
 * HandleExternalControl *
 *************************/

TEST(ScmLifeTimeState_HandleExternalControl, Check_HandleExternalControl)
{
  ScmLifetimeStateTester TEST_HELPER;
  NiceMock<FakeActionManager> fakeActionManager;

  ExternalControlState externalControl{.longitudinalActive = true,
                                       .lateralActive = true,
                                       .lateralDeactivationTime = 100_ms};

  TEST_HELPER.scmLifetimeState.SET_EXTERNAL_CONTROL(externalControl);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLateralAction(LateralAction(LateralAction::State::LANE_KEEPING))).Times(1);
  EXPECT_CALL(fakeActionManager, GetLateralActionLastTick()).Times(1).WillOnce(Return(LateralAction(LateralAction::State::LANE_KEEPING)));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT)).Times(1);
  EXPECT_CALL(fakeActionManager, SetActionSubStateLastTick(LongitudinalActionState::SPEED_ADJUSTMENT)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionManager()).WillByDefault(Return(&fakeActionManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.scmLifetimeState.HandleExternalControl();
}