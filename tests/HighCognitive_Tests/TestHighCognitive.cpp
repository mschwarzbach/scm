/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestHighCognitive.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

TestHighCognitive::TestHighCognitive(MicroscopicCharacteristics* _microscopicData, MentalCalculationsInterface& _mentalCalculations, const MentalModelInterface& _mentalModel, bool isActive)
    : HighCognitive(_microscopicData, _mentalCalculations, _mentalModel, isActive) {}

void TestHighCognitive::TestAdvanceHighCognitive(int time, bool externalControlActive)
{
  AdvanceHighCognitive(time, externalControlActive);
}
