/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../MentalModel_Tests/TestMentalModel.h"
#include "HighCognitive.h"
#include "ScmDriver.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::An;
using ::testing::Expectation;
using ::testing::Invoke;
using ::testing::Matcher;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::WithArg;
using ::testing::WithArgs;

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/
class TestHighCognitive : public HighCognitive
{
public:
  TestHighCognitive(MicroscopicCharacteristics* _microscopicData, MentalCalculationsInterface& _mentalCalculations, const MentalModelInterface& _mentalModel, bool isActive);

  void TestAdvanceHighCognitive(int time, bool externalControlActive);

  MOCK_METHOD0(RightFrontAndRightFrontFarAvailable, bool());
  MOCK_METHOD0(DetermineCurrentSituationPattern, int());
};