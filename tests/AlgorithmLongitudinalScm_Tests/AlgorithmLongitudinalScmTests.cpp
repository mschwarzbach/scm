/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <limits>

#include "module/longitudinalController/src/AlgorithmLongitudinalScmCalculations.h"

using ::testing::DoubleEq;

/********************************************
 * CHECK ApplyFirstOrderTransientBehaviour  *
 ********************************************/

struct DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour
{
  double input_oldVal;
  double input_brakePedalPosition;
  double input_acceleratorPedalPosition;
  double result_brakePedalPosition;
  double result_acceleratorPedalPosition;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour& obj)
  {
    return os
           << "  input_oldVal (double): " << obj.input_oldVal
           << "| input_brakePedalPosition (double): " << obj.input_brakePedalPosition
           << "| input_acceleratorPedalPosition (double): " << obj.input_acceleratorPedalPosition
           << "|  result_brakePedalPosition (double): " << obj.result_brakePedalPosition
           << "| result_acceleratorPedalPosition (double): " << obj.result_acceleratorPedalPosition;
  }
};

class AlgorithmLongitudinalScmCalculationsApplyFirstOrderTransientBehaviour : public ::testing::Test,
                                                                              public ::testing::WithParamInterface<DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour>
{
};

TEST_P(AlgorithmLongitudinalScmCalculationsApplyFirstOrderTransientBehaviour, AlgorithmLongitudinalScmCalculations_CheckFunction_ApplyFirstOrderTransientBehaviour)
{
     // Get Resources for testing
     DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour data = GetParam();
     scm::common::vehicle::properties::EntityProperties vehicleParameters;
     AlgorithmLongitudinalScmCalculations calculations{0.0_mps,
                                                       0.0_mps_sq,
                                                       vehicleParameters,
                                                       data.input_brakePedalPosition,
                                                       data.input_acceleratorPedalPosition,
                                                       data.input_oldVal,
                                                       0.0_mps,
                                                       0.0_mps_sq,
                                                       0.0_m,
                                                       false,
                                                       100};
     // Call test
     calculations.ApplyFirstOrderTransientBehaviour();
     ASSERT_THAT(calculations.GetBrakePedalPosition(), DoubleEq(data.result_brakePedalPosition));
     ASSERT_THAT(calculations.GetAcceleratorPedalPosition(), DoubleEq(data.result_acceleratorPedalPosition));
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/
INSTANTIATE_TEST_SUITE_P(
    Default,
    AlgorithmLongitudinalScmCalculationsApplyFirstOrderTransientBehaviour,
    testing::Values(
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 0.0,
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.0,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 0.0,
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 1.0,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.4545454545454545},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 1.0,
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 2.0,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 1.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 0.0,
            .input_brakePedalPosition = 0.5,
            .input_acceleratorPedalPosition = 0.0,
            .result_brakePedalPosition = 0.2272727272727272,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 0.0,
            .input_brakePedalPosition = 1.0,
            .input_acceleratorPedalPosition = 0.0,
            .result_brakePedalPosition = 0.5555555555555555,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = -1.0,
            .input_brakePedalPosition = 2.0,
            .input_acceleratorPedalPosition = 0.0,
            .result_brakePedalPosition = 1.0,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = -1.0,
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 2.0,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.3636363636363636},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ApplyFirstOrderTransientBehaviour{
            .input_oldVal = 1.0,
            .input_brakePedalPosition = 2.0,
            .input_acceleratorPedalPosition = 0.0,
            .result_brakePedalPosition = 0.6666666666666666,
            .result_acceleratorPedalPosition = 0.0}));

/*****************************
 * CHECK ClosedLoopController *
 ******************************/

struct DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController
{
     double input_brakePedalPosition;
     double input_acceleratorPedalPosition;
     units::velocity::meters_per_second_t input_velocity;
     units::velocity::meters_per_second_t input_lastVelocity;
     units::acceleration::meters_per_second_squared_t input_lastAcceleration;
     units::length::meter_t input_integratedDeltaVelocity;
     double result_brakePedalPosition;
     double result_acceleratorPedalPosition;
};

class AlgorithmLongitudinalScmCalculationsClosedLoopController : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController>
{
};

TEST_P(AlgorithmLongitudinalScmCalculationsClosedLoopController, AlgorithmLongitudinalScmCalculations_CheckFunction_ClosedLoopController)
{
     // Get Resources for testing
     DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController data = GetParam();
     scm::common::vehicle::properties::EntityProperties vehicleParameters;
     AlgorithmLongitudinalScmCalculations calculations{data.input_velocity, 0.0_mps_sq, vehicleParameters, data.input_brakePedalPosition, data.input_acceleratorPedalPosition, 0.0, data.input_lastVelocity, data.input_lastAcceleration, data.input_integratedDeltaVelocity, false, 100};
     // Call test
     calculations.ApplyClosedLoopController();

  // Evaluate result
  ASSERT_TRUE(abs(calculations.GetBrakePedalPosition() - data.result_brakePedalPosition) <= std::numeric_limits<double>::epsilon());
  ASSERT_TRUE(abs(calculations.GetAcceleratorPedalPosition() - data.result_acceleratorPedalPosition) <= std::numeric_limits<double>::epsilon());
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/
INSTANTIATE_TEST_SUITE_P(
    Default,
    AlgorithmLongitudinalScmCalculationsClosedLoopController,
    testing::Values(
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.0,
            .input_velocity = 0.0_mps,
            .input_lastVelocity = 0.0_mps,
            .input_lastAcceleration = 0.0_mps_sq,
            .input_integratedDeltaVelocity = 0.0_m,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 5.0_mps_sq,
            .input_integratedDeltaVelocity = 1.0_m,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.045},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 5.0_mps_sq,
            .input_integratedDeltaVelocity = 0.5_m,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.02},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 5.0_mps_sq,
            .input_integratedDeltaVelocity = 0.0_m,
            .result_brakePedalPosition = 0.005,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 5.0_mps_sq,
            .input_integratedDeltaVelocity = -0.5_m,
            .result_brakePedalPosition = 0.03,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 5.0_mps_sq,
            .input_integratedDeltaVelocity = -1.0_m,
            .result_brakePedalPosition = 0.055,
            .result_acceleratorPedalPosition = 0.0},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.5_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 9.0_mps_sq,
            .input_integratedDeltaVelocity = 1.0_m,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.247},
        DataFor_AlgorithmAlgorithmLongitudinalScmCalculations_ClosedLoopController{
            .input_brakePedalPosition = 0.0,
            .input_acceleratorPedalPosition = 0.5,
            .input_velocity = 16.0_mps,
            .input_lastVelocity = 15.0_mps,
            .input_lastAcceleration = 9.0_mps_sq,
            .input_integratedDeltaVelocity = 1.0_m,
            .result_brakePedalPosition = 0.0,
            .result_acceleratorPedalPosition = 0.5}));
