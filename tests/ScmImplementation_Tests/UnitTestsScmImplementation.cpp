/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../ActionImplementation_Tests/TestActionImplementation.h"
#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeLongitudinalCalculations.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeScmDependencies.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "../Fakes/FakeSwerving.h"
#include "../Fakes/FakeTrajectoryPlanner.h"
#include "../GazeControl_Tests/TestGazeFollower.h"
#include "../MentalModel_Tests/TestMentalModel.h"
#include "ParametersScmSignal.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/MentalCalculations.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/***********************************************************
 * CHECK reaction base time for longitudinal action states *
 ***********************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_Longitudinal_Action_AdjustmentBaseTime
{
  units::time::second_t inputAdjustmentBaseTime;      // AdjustmentBaseTime at the start of the test
  bool expectedAdjustmentBaseTimeStep1;               // AdjustmentBaseTime after advancing (true = positive, false = negative)
  bool expectedExpiredAdjustmentBaseTimeQuerry;       // Expected return value of the querry of expired adjustment base time
  units::acceleration::meters_per_second_squared_t inputLastLongitudinalAccelerationWish;  // Value of the acceleration wish of the last cycle
  units::acceleration::meters_per_second_squared_t outputLongitudinalAccelerationWish;     // Return value for the newly calculated wish acceleration
  bool isNewCalculationOfAdjustmentBaseTimeExpected;                                       // Is the function CalculateAdjustmentBaseTime expected to return a new adjustment base time
};

class ScmImplementation_Longitudinal_AdjustmentBaseTime : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataFor_Longitudinal_Action_AdjustmentBaseTime>
{
};

TEST_P(ScmImplementation_Longitudinal_AdjustmentBaseTime, ScmImplementation_Check_Longitudinal_AdjustmentBaseTime)
{
  // Get resources for testing
  DataFor_Longitudinal_Action_AdjustmentBaseTime data = GetParam();

  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeSurroundingVehicleQueryFactory> fakeVehicleQueryFactory;
  NiceMock<FakeLongitudinalCalculations> fakeLongitudinalCalculations;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeScmDependencies> fakeScmDependencies;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeTrajectoryPlanner> fakeTrajectoryPlanner;

  DriverParameters testDriverParameters;
  testDriverParameters.desiredVelocity = 123.4_mps;
  ON_CALL(fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(testDriverParameters));
  ON_CALL(fakeScmDependencies, GetCycleTime()).WillByDefault(Return(123_ms));
  auto testVehicleParameters = std::make_unique<scm::common::vehicle::properties::EntityProperties>();
  auto stubMentalCalculations = std::make_unique<MentalCalculations>(fakeFeatureExtractor);

  auto stubMentalModel = std::make_unique<TestMentalModel>(testDriverParameters, *testVehicleParameters.get(), &fakeStochastics, fakeFeatureExtractor, fakeMentalCalculations, fakeScmDependencies);

  stubMentalCalculations->Initialize(stubMentalModel.get(), &fakeVehicleQueryFactory, &fakeLongitudinalCalculations);
  auto stubActionImplementation = std::make_unique<TestActionImplementation>(&fakeStochastics, fakeMentalModel, fakeFeatureExtractor, fakeMentalCalculations, fakeSwerving, fakeTrajectoryPlanner);

  // Execution of the segment of the code
  // Setting the initial conditions
  stubMentalModel->SetAdjustmentBaseTime(data.inputAdjustmentBaseTime);
  stubMentalModel->SetAccelerationWishLongitudinalLast(data.inputLastLongitudinalAccelerationWish);
  // Setting the return values of functions that are not being tested here
  stubActionImplementation->SetAccelerationWishLongitudinal(data.outputLongitudinalAccelerationWish);

  // Test the reduction of the adjustment base time
  stubMentalModel->AdvanceAdjustmentBaseTime();
  // Value to compare to the expexted Adjustment time
  bool adjustmentBaseTimeStep1 = stubMentalModel->GetAdjustmentBaseTime() >= 0_s;
  auto dAdjustmentBaseTimeStep1 = stubMentalModel->GetAdjustmentBaseTime();

  // Test the retrieval of adjustment base time and whether it is expired and store value for further comparison
  bool returnValueOfIsAdjustmentBaseTimeExpired = stubMentalModel->IsAdjustmentBaseTimeExpired();

  // Test the mechanism and its conditions to calculate a new adjustment base time
  stubMentalModel->CalculateAdjustmentBaseTime(stubActionImplementation.get());
  // Value to compare whether a new calculation of the adjustment base time is necessary
  // Note: Check for greater than 0 here becasue the mogged 'GetLogNormalDistributed' function returns '0'. A more
  // detailed approach is not necessary, because the fuctionality is confirmed with this approach.
  bool wasAdjustmentBaseTimeCalculated = stubMentalModel->GetAdjustmentBaseTime() != dAdjustmentBaseTimeStep1;

  // Compare actual values with expected values
  ASSERT_TRUE(adjustmentBaseTimeStep1 == data.expectedAdjustmentBaseTimeStep1);
  ASSERT_TRUE(returnValueOfIsAdjustmentBaseTimeExpired == data.expectedExpiredAdjustmentBaseTimeQuerry);
  ASSERT_TRUE(wasAdjustmentBaseTimeCalculated == data.isNewCalculationOfAdjustmentBaseTimeExpected);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmImplementation_Longitudinal_AdjustmentBaseTime,
    testing::Values(
        DataFor_Longitudinal_Action_AdjustmentBaseTime{
            .inputAdjustmentBaseTime = .05_s,
            .expectedAdjustmentBaseTimeStep1 = false,
            .expectedExpiredAdjustmentBaseTimeQuerry = true,
            .inputLastLongitudinalAccelerationWish = 2.5_mps_sq,
            .outputLongitudinalAccelerationWish = -.7_mps_sq,
            .isNewCalculationOfAdjustmentBaseTimeExpected = true},
        DataFor_Longitudinal_Action_AdjustmentBaseTime{
            .inputAdjustmentBaseTime = .05_s,
            .expectedAdjustmentBaseTimeStep1 = false,
            .expectedExpiredAdjustmentBaseTimeQuerry = true,
            .inputLastLongitudinalAccelerationWish = 2.5_mps_sq,
            .outputLongitudinalAccelerationWish = 2.5_mps_sq,
            .isNewCalculationOfAdjustmentBaseTimeExpected = false},
        DataFor_Longitudinal_Action_AdjustmentBaseTime{
            .inputAdjustmentBaseTime = .15_s,
            .expectedAdjustmentBaseTimeStep1 = true,
            .expectedExpiredAdjustmentBaseTimeQuerry = false,
            .inputLastLongitudinalAccelerationWish = 2.5_mps_sq,
            .outputLongitudinalAccelerationWish = 2.5_mps_sq,
            .isNewCalculationOfAdjustmentBaseTimeExpected = false}));

/*********************************************
 * CHECK GazeFollower::EvaluateGazeOverwrite *
 *********************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GazeFollower_EvaluateGazeOverwrite
{
  bool input_activationRequested;  // Is activation of a gaze event requested by the framework?
  bool input_gazeFollowerActive;   // Is the GazeFollower currently active?
  bool input_lastGazeTarget;       // Is the last gaze target in the time series reached?
  bool result_resetGazeOverwrite;  // Is the struct gazeOverwrite reset to default?

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GazeFollower_EvaluateGazeOverwrite& obj)
  {
    return os
           << "input_activationRequested (bool): " << ScmCommons::BooleanToString(obj.input_activationRequested)
           << " | input_gazeFollowerActive (bool): " << ScmCommons::BooleanToString(obj.input_gazeFollowerActive)
           << " | input_lastGazeTarget (bool): " << ScmCommons::BooleanToString(obj.input_lastGazeTarget)
           << " | result_resetGazeOverwrite (bool): " << ScmCommons::BooleanToString(obj.result_resetGazeOverwrite);
  }
};

class GazeFollower_EvaluateGazeOverwrite : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_GazeFollower_EvaluateGazeOverwrite>
{
};

TEST_P(GazeFollower_EvaluateGazeOverwrite, GazeFollower_Check_EvaluateGazeOverwrite)
{
  // get resources for testing
  DataFor_GazeFollower_EvaluateGazeOverwrite data = GetParam();
  OwnVehicleInformationSCM ownVehicleInformation;
  std::unique_ptr<TestGazeFollower> stubTestGazeFollower = std::make_unique<TestGazeFollower>(ownVehicleInformation);

  // construct result for no reset of struct gazeOverwrite
  GazeOverwrite resultNoReset;
  resultNoReset.gazeTarget = AreaOfInterest::EGO_FRONT;

  // apply testing conditions
  stubTestGazeFollower->SetGazeFollowerActive(data.input_gazeFollowerActive);
  resultNoReset.gazeTargetIsLastPointInTimeSeries = data.input_lastGazeTarget;
  stubTestGazeFollower->SetGazeOverwrite(resultNoReset);

  if (data.input_activationRequested)
  {
    ownVehicleInformation.gazeFollowerInformation.gazeFileName = "xxx";
  }
  else
  {
    ownVehicleInformation.gazeFollowerInformation.gazeFileName = "";
  }

  // set expected calls depending on testing conditions
  if (data.input_activationRequested && !data.input_gazeFollowerActive)
  {
    EXPECT_CALL(*stubTestGazeFollower, ParseGazeTargetTimeSeriesFromFile()).Times(1);
  }
  else
  {
    EXPECT_CALL(*stubTestGazeFollower, ParseGazeTargetTimeSeriesFromFile()).Times(0);
  }

  if (data.result_resetGazeOverwrite)
  {
    EXPECT_CALL(*stubTestGazeFollower, ProcessGazeTargetTimeSeries(_)).Times(0);
  }
  else
  {
    EXPECT_CALL(*stubTestGazeFollower, ProcessGazeTargetTimeSeries(_)).Times(1);
  }

  // execute test
  GazeOverwrite resultTest = stubTestGazeFollower->EvaluateGazeOverwrite(100_ms);

  // evaluate result
  bool wasGazeOverwriteReset = (resultNoReset.gazeTarget != resultTest.gazeTarget);
  ASSERT_EQ(data.result_resetGazeOverwrite, wasGazeOverwriteReset);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    GazeFollower_EvaluateGazeOverwrite,
    testing::Values(
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = true,
            .input_gazeFollowerActive = true,
            .input_lastGazeTarget = false,
            .result_resetGazeOverwrite = false},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = true,
            .input_gazeFollowerActive = false,
            .input_lastGazeTarget = false,
            .result_resetGazeOverwrite = false},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = false,
            .input_gazeFollowerActive = true,
            .input_lastGazeTarget = false,
            .result_resetGazeOverwrite = false},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = false,
            .input_gazeFollowerActive = false,
            .input_lastGazeTarget = false,
            .result_resetGazeOverwrite = true},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = true,
            .input_gazeFollowerActive = true,
            .input_lastGazeTarget = true,
            .result_resetGazeOverwrite = true},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = true,
            .input_gazeFollowerActive = false,
            .input_lastGazeTarget = true,
            .result_resetGazeOverwrite = true},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = false,
            .input_gazeFollowerActive = true,
            .input_lastGazeTarget = true,
            .result_resetGazeOverwrite = true},
        DataFor_GazeFollower_EvaluateGazeOverwrite{
            .input_activationRequested = false,
            .input_gazeFollowerActive = false,
            .input_lastGazeTarget = true,
            .result_resetGazeOverwrite = true}));

/***************************************************
 * CHECK GazeFollower::ProcessGazeTargetTimeSeries *
 ***************************************************/

TEST(GazeFollower_ProcessGazeTargetTimeSeries, GazeFollower_Check_ProcessGazeTargetTimeSeries)
{
  // get resources for testing
  OwnVehicleInformationSCM ownVehicleInformation;
  std::unique_ptr<TestGazeFollower2> stubTestGazeFollower = std::make_unique<TestGazeFollower2>(ownVehicleInformation);

  // set initial conditions and construct variables
  GazeOverwrite startGazeOverwrite;
  GazeOverwrite testGazeOverwrite = startGazeOverwrite;
  stubTestGazeFollower->SetTimeOfEventActivation(0_ms);
  units::time::millisecond_t currentTime;
  std::vector<GazeTimeSeriesPoint> testTimeSeries;
  GazeTimeSeriesPoint testTimeSeriesPoint;

  // Test case 1: gaze target has not changed in current time step
  testGazeOverwrite.hasGazeTargetChanged = true;
  stubTestGazeFollower->SetGazeOverwrite(testGazeOverwrite);
  currentTime = 0_ms;
  testTimeSeriesPoint.time = 100_ms;
  testTimeSeries.push_back(testTimeSeriesPoint);
  stubTestGazeFollower->SetGazeTimeSeries(testTimeSeries);

  stubTestGazeFollower->TestProcessGazeTargetTimeSeries(currentTime);

  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeFollowerActive);
  ASSERT_FALSE(stubTestGazeFollower->GetGazeOverwrite().hasGazeTargetChanged);
  ASSERT_FALSE(stubTestGazeFollower->TestGetThrowWarning());

  testTimeSeries.clear();
  testGazeOverwrite = startGazeOverwrite;

  // Test case 2: gaze target has changed and is last gaze target in time series
  currentTime = 100_ms;
  testTimeSeriesPoint.time = 100_ms;
  testTimeSeriesPoint.gazeTarget = AreaOfInterest::EGO_FRONT;
  testTimeSeries.push_back(testTimeSeriesPoint);
  stubTestGazeFollower->SetGazeTimeSeries(testTimeSeries);

  stubTestGazeFollower->TestProcessGazeTargetTimeSeries(currentTime);

  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeFollowerActive);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().hasGazeTargetChanged);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeTarget == AreaOfInterest::EGO_FRONT);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().durationGazeTarget != testGazeOverwrite.durationGazeTarget);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeTargetIsLastPointInTimeSeries);
  ASSERT_FALSE(stubTestGazeFollower->TestGetThrowWarning());

  testTimeSeries.clear();
  testGazeOverwrite = startGazeOverwrite;

  // Test case 3: gaze target has changed and is NOT last gaze target in time series
  currentTime = 100_ms;
  testTimeSeriesPoint.time = 100_ms;
  testTimeSeriesPoint.gazeTarget = AreaOfInterest::EGO_FRONT;
  testTimeSeries.push_back(testTimeSeriesPoint);
  testTimeSeriesPoint.time = 500_ms;
  testTimeSeriesPoint.gazeTarget = AreaOfInterest::LEFT_FRONT;
  testTimeSeries.push_back(testTimeSeriesPoint);
  stubTestGazeFollower->SetGazeTimeSeries(testTimeSeries);

  stubTestGazeFollower->TestProcessGazeTargetTimeSeries(currentTime);

  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeFollowerActive);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().hasGazeTargetChanged);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeTarget == AreaOfInterest::EGO_FRONT);
  ASSERT_EQ(stubTestGazeFollower->GetGazeOverwrite().durationGazeTarget, 400_ms);
  ASSERT_FALSE(stubTestGazeFollower->GetGazeOverwrite().gazeTargetIsLastPointInTimeSeries);
  ASSERT_FALSE(stubTestGazeFollower->TestGetThrowWarning());

  testTimeSeries.clear();
  testGazeOverwrite = startGazeOverwrite;

  // Test case 4: gaze target has changed and is NOT last gaze target in time series, but duration is too short
  currentTime = 100_ms;
  testTimeSeriesPoint.time = 100_ms;
  testTimeSeriesPoint.gazeTarget = AreaOfInterest::EGO_FRONT;
  testTimeSeries.push_back(testTimeSeriesPoint);
  testTimeSeriesPoint.time = 200_ms;
  testTimeSeriesPoint.gazeTarget = AreaOfInterest::LEFT_FRONT;
  testTimeSeries.push_back(testTimeSeriesPoint);
  stubTestGazeFollower->SetGazeTimeSeries(testTimeSeries);

  stubTestGazeFollower->TestProcessGazeTargetTimeSeries(currentTime);

  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeFollowerActive);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().hasGazeTargetChanged);
  ASSERT_TRUE(stubTestGazeFollower->GetGazeOverwrite().gazeTarget == AreaOfInterest::EGO_FRONT);
  ASSERT_EQ(stubTestGazeFollower->GetGazeOverwrite().durationGazeTarget, 100_ms);
  ASSERT_FALSE(stubTestGazeFollower->GetGazeOverwrite().gazeTargetIsLastPointInTimeSeries);
  ASSERT_TRUE(stubTestGazeFollower->TestGetThrowWarning());
}
