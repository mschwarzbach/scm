################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME ScmImplementation_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)
set(SENSOR_DIR ${ROOT_DIR}/module/sensor)

file(GLOB
    SRC_SUR_VEH
    CONFIGURE_DEPENDS
    FOLLOW_SYMLINKS false
    LIST_DIRECTORIES false
    "${DRIVER_DIR}/src/SurroundingVehicles/*.cpp")

file(GLOB
    HEADER_SUR_VEH
    CONFIGURE_DEPENDS
    FOLLOW_SYMLINKS false
    LIST_DIRECTORIES false
    "${DRIVER_DIR}/src/SurroundingVehicles/*.h")

add_scm_target(
    NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
    DEFAULT_MAIN
    LINKOSI

  SOURCES
    ${SRC_SUR_VEH}
    UnitTestsScmImplementation.cpp

    ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationAdjustmentParameters.cpp
    ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationCalculationsQuery.cpp
    ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationComponents.cpp
    ${DRIVER_DIR}/src/ActionImplementation.cpp
    ${DRIVER_DIR}/src/FeatureExtractor.cpp
    ${DRIVER_DIR}/src/GazeControl/GazeCone.cpp
    ${DRIVER_DIR}/src/GazeControl/GazeFollower.cpp
    ${DRIVER_DIR}/src/HighCognitive/AnticipatedLaneChangerModel.cpp
    ${DRIVER_DIR}/src/HighCognitive/ApproachFollowClassifier.cpp
    ${DRIVER_DIR}/src/HighCognitive/ApproachSaliencyClassifier.cpp
    ${DRIVER_DIR}/src/HighCognitive/FollowSaliencyClassifier.cpp
    ${DRIVER_DIR}/src/HighCognitive/LaneChangeTrajectories.cpp
    ${DRIVER_DIR}/src/HighCognitive/LaneChangerCachedData.cpp
    ${DRIVER_DIR}/src/HighCognitive/HighCognitive.cpp
    ${DRIVER_DIR}/src/InfrastructureCharacteristics.cpp
    ${DRIVER_DIR}/src/LaneChangeBehavior.cpp
    ${DRIVER_DIR}/src/LaneChangeBehaviorQuery.cpp
    ${DRIVER_DIR}/src/LaneChangeDimension.cpp
    ${DRIVER_DIR}/src/LaneQueryHelper.cpp
    ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.cpp
    ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.cpp
    ${DRIVER_DIR}/src/MentalModel.cpp
    ${DRIVER_DIR}/src/Merging/Merging.cpp
    ${DRIVER_DIR}/src/MentalCalculations.cpp
    ${DRIVER_DIR}/src/MicroscopicCharacteristics.cpp
    ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.cpp
    ${DRIVER_DIR}/src/Reliability.cpp
    ${DRIVER_DIR}/src/ScmCommons.cpp
    ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.cpp
    ${DRIVER_DIR}/src/TrajectoryCalculations/TrajectoryCalculations.cpp
    ${DRIVER_DIR}/src/TrajectoryCalculations/SteeringManeuver.cpp
    ../MentalModel_Tests/TestMentalModel.cpp
    ../GazeControl_Tests/TestGazeFollower.cpp
    ../ActionImplementation_Tests/TestActionImplementation.cpp
    ${ROOT_DIR}/include/common/XmlParser.cpp
    ${DRIVER_DIR}/src/TrafficFlow/TrafficFlow.cpp
    ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocity.cpp
    ${ROOT_DIR}/src/Logging/Logging.cpp

  HEADERS
    ${DRIVER_DIR}/src/FeatureExtractor.h
    ${DRIVER_DIR}/src/GazeControl/GazeCone.h
    ${DRIVER_DIR}/src/GazeControl/GazeFollower.h
    ${DRIVER_DIR}/src/HighCognitive/HighCognitive.h
    ${DRIVER_DIR}/src/HighCognitive/AnticipatedLaneChangerModel.h
    ${DRIVER_DIR}/src/HighCognitive/ApproachFollowClassifier.h
    ${DRIVER_DIR}/src/HighCognitive/ApproachSaliencyClassifier.h
    ${DRIVER_DIR}/src/HighCognitive/FollowSaliencyClassifier.h
    ${DRIVER_DIR}/src/HighCognitive/LaneChangeTrajectories.h
    ${DRIVER_DIR}/src/HighCognitive/LaneChangerCachedData.h
    ${DRIVER_DIR}/src/InfrastructureCharacteristics.h
    ${DRIVER_DIR}/src/LaneChangeBehaviorInterface.h
    ${DRIVER_DIR}/src/LaneChangeBehaviorQueryInterface.h
    ${DRIVER_DIR}/src/LaneChangeDimension.h
    ${DRIVER_DIR}/src/LaneQueryHelper.h
    ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.h
    ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.h
    ${DRIVER_DIR}/src/MicroscopicCharacteristics.h
    ${DRIVER_DIR}/src/MentalCalculations.h
    ${DRIVER_DIR}/src/MentalModel.h
    ${DRIVER_DIR}/src/Merging/Merging.h
    ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.h
    ${DRIVER_DIR}/src/Reliability.h
    ${DRIVER_DIR}/src/ScmCommons.h
    ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleInterface.h
    ${DRIVER_DIR}/src/TrajectoryCalculations/TrajectoryCalculations.h
    ${DRIVER_DIR}/src/TrajectoryCalculations/TrajectoryCalculationTypes.h
    ${DRIVER_DIR}/src/TrajectoryCalculations/SteeringManeuver.h
    ${DRIVER_DIR}/src/TrajectoryCalculations/SteeringManeuverTypes.h
    ../MentalModel_Tests/TestMentalModel.h
    ../GazeControl_Tests/TestGazeFollower.h
    ../ActionImplementation_Tests/TestActionImplementation.h
    ${ROOT_DIR}/include/common/XmlParser.h
    ${DRIVER_DIR}/src/TrafficFlow/TrafficFlow.h
    ${DRIVER_DIR}/src/TrafficFlow/TrafficFlowInterface.h
    ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocity.h
    ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocityInterface.h
    ${ROOT_DIR}/src/Logging/Logging.h

  INCDIRS
    ${DRIVER_DIR}
    ${DRIVER_DIR}/src
    ${DRIVER_DIR}/src/HighCognitive
    ${PARAMETER_DIR}/src
    ${PARAMETER_DIR}/src/Signals
    ${ROOT_DIR}/include/signal

  LIBRARIES
    Iconv::Iconv
    LibXml2::LibXml2
    Stochastics::Stochastics
)

target_compile_definitions(${COMPONENT_TEST_NAME} PRIVATE
    TESTING_ENABLED
)