/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "DesiredVelocityCalculations.h"
using namespace units::literals;

/*********************************
 * CHECK CalculateTargetVelocity *
 *********************************/

TEST(CalculateTargetVelocity, Check_CalculateTargetVelocity)
{
  DesiredVelocityCalculations::TargetVelocityParameters params{
      .vReason = 1.0_mps,
      .desiredVelocity = 2.0_mps,
      .desiredVelocityViolationDelta = 3.0_mps,
      .maximumVehicleVelocity = 4.0_mps,
      .legalVelocity = 5.0_mps,
      .legalVelocityViolationDelta = 6.0_mps};

  auto result = DesiredVelocityCalculations::CalculateTargetVelocity(params);
  ASSERT_EQ(result, 1.0_mps);
}