/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeStochastics.h"
#include "Fakes/FakeSurroundingVehicle.h"
#include "module/driver/src/ScmComponents.h"
#include "module/driver/src/ScmDependencies.h"
#include "module/driver/src/ScmSignalCollector.h"
#include "module/parameterParser/src/Signals/ParametersScmSignal.h"

using ::testing::NiceMock;
using ::testing::Return;

/******************
 * CHECK Creation *
 ******************/

class ScmComponents_Creation : public ::testing::Test
{
protected:
  std::unique_ptr<FakeStochastics> fakeStochastics = std::make_unique<FakeStochastics>();
  scm::signal::DriverInput driverInput;
  OwnVehicleInformationSCM ownVehicleInformationSCM;
  TrafficRuleInformationSCM trafficRuleInformationSCM;
  GeometryInformationSCM geometryInformationSCM;
  SurroundingObjectsSCM surroundingObjectsSCM;
  std::unique_ptr<ScmSignalCollector> signalCollector{nullptr};

  OwnVehicleInformationSCM signal_ownVehicleInformation;
  TrafficRuleInformationSCM signal_trafficRuleInformation;
  GeometryInformationSCM signal_geometryInformation;
  SurroundingObjectsSCM signal_surroundingObjects;

  virtual void SetUp()
  {
    signalCollector = std::make_unique<ScmSignalCollector>(fakeStochastics.get(), 100_ms);

    ownVehicleInformationSCM.id = 0;
    ownVehicleInformationSCM.lateralVelocity = 0.2_mps;
    ownVehicleInformationSCM.absoluteVelocity = 33.33_mps;

    ObjectInformationSCM ahead;
    ahead.id = 22;
    ahead.collision = true;
    surroundingObjectsSCM.ongoingTraffic.Ahead().Close().push_back(ahead);

    ObjectInformationSCM behind;
    ahead.id = 33;
    ahead.collision = false;
    surroundingObjectsSCM.ongoingTraffic.Behind().Close().push_back(behind);

    driverInput.sensorOutput.surroundingObjectsSCM = surroundingObjectsSCM;
    driverInput.sensorOutput.ownVehicleInformationSCM = ownVehicleInformationSCM;
    driverInput.sensorOutput.trafficRuleInformationSCM = trafficRuleInformationSCM;
    driverInput.sensorOutput.geometryInformationSCM = geometryInformationSCM;

    OwnVehicleRoutePose ownVehicleRoutePose{};
    driverInput.sensorOutput.ownVehicleRoutePose = ownVehicleRoutePose;

    driverInput.driverParameters.desiredVelocity = 123.4_mps;
  }

  ScmDependenciesInterface* GetValidScmDependencies()
  {
    UpdateDependencySignals();
    return signalCollector->GetDependencies();
  }

private:
  void UpdateDependencySignals()
  {
    signalCollector->Update(driverInput);
  }
};

TEST_F(ScmComponents_Creation, SetUp_OwnVehicleInformationScm)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetScmDependencies()->GetOwnVehicleInformationScm()->id, scmDependencies->GetOwnVehicleInformationScm()->id);
  ASSERT_EQ(scmComponents.GetScmDependencies()->GetOwnVehicleInformationScm()->lateralVelocity, scmDependencies->GetOwnVehicleInformationScm()->lateralVelocity);
}

TEST_F(ScmComponents_Creation, ChangesInScmComponentsOwnVehicleInformation_WillLeadToChangesInScmDependencies)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetScmDependencies()->GetOwnVehicleInformationScm()->lateralVelocity, scmDependencies->GetOwnVehicleInformationScm()->lateralVelocity);
  scmComponents.GetScmDependencies()->GetOwnVehicleInformationScm()->lateralVelocity = 3.0_mps;

  ASSERT_EQ(scmComponents.GetScmDependencies()->GetOwnVehicleInformationScm()->lateralVelocity, scmDependencies->GetOwnVehicleInformationScm()->lateralVelocity);
  ASSERT_EQ(3.0_mps, scmDependencies->GetOwnVehicleInformationScm()->lateralVelocity);
}

TEST_F(ScmComponents_Creation, SetUp_SurroundingObjectsSCM)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  auto result_objects = scmComponents.GetScmDependencies()->GetSurroundingObjectsScm()->ongoingTraffic;
  auto expected_objects = scmDependencies->GetSurroundingObjectsScm()->ongoingTraffic;

  ASSERT_EQ(result_objects.Ahead().Close().front().id, expected_objects.Ahead().Close().front().id);
  ASSERT_EQ(result_objects.Ahead().Close().front().collision, expected_objects.Ahead().Close().front().collision);
  ASSERT_EQ(result_objects.Behind().Close().front().id, expected_objects.Behind().Close().front().id);
  ASSERT_EQ(result_objects.Behind().Close().front().collision, expected_objects.Behind().Close().front().collision);
}

TEST_F(ScmComponents_Creation, ForcedLaneChange)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);
  ASSERT_EQ(scmComponents.GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->forcedLaneChangeStartTrigger, LaneChangeAction::None);
  scmComponents.GetMentalModel()->GetMicroscopicData()->UpdateOwnVehicleData()->forcedLaneChangeStartTrigger = LaneChangeAction::ForcePositive;
  ASSERT_EQ(scmComponents.GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->forcedLaneChangeStartTrigger, LaneChangeAction::ForcePositive);
}

/************************
 * CHECK GetGazeControl *
 ************************/

TEST_F(ScmComponents_Creation, Check_GetGazeControl)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetGazeControl()->GetCurrentGazeState(), scmComponents.GetGazeControl()->GetCurrentGazeState());
}

/*************************
 * CHECK GetGazeFollower *
 *************************/

TEST_F(ScmComponents_Creation, Check_GetGazeFollower)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_FALSE(scmComponents.GetGazeFollower()->EvaluateGazeOverwrite(100_ms).gazeFollowerActive);
}

/*****************************
 * CHECK GetSituationManager *
 *****************************/

TEST_F(ScmComponents_Creation, Check_GetSituationManager)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetSituationManager()->GetCurrentSituationDuration(), scmComponents.GetSituationManager()->GetCurrentSituationDuration());
}

/**************************
 * CHECK GetActionManager *
 **************************/

TEST_F(ScmComponents_Creation, Check_GetActionManager)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetActionManager()->GetLateralActionLastTick(), scmComponents.GetActionManager()->GetLateralActionLastTick());
}

/*********************************
 * CHECK GetActionImplementation *
 *********************************/

TEST_F(ScmComponents_Creation, Check_ActionImplementation)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetActionImplementation()->GetLateralDeviationGain(), scmComponents.GetActionImplementation()->GetLateralDeviationGain());
}

/********************************
 * CHECK GetAlgorithmSceneryCar *
 ********************************/

TEST_F(ScmComponents_Creation, Check_AlgorithmSceneryCar)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetAlgorithmSceneryCar()->ResetReactionBaseTime(), scmComponents.GetAlgorithmSceneryCar()->ResetReactionBaseTime());
}

/*******************************
 * CHECK GetAuditoryPerception *
 *******************************/

TEST_F(ScmComponents_Creation, Check_AuditoryPerception)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetAuditoryPerception()->GetAcousticStimulusDirection(), scmComponents.GetAuditoryPerception()->GetAcousticStimulusDirection());
}

/***********************************
 * CHECK GetInformationAcquisition *
 ***********************************/

TEST_F(ScmComponents_Creation, Check_InformationAcquisition)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetInformationAcquisition()->GetCycleTime(), scmComponents.GetInformationAcquisition()->GetCycleTime());
}

/*****************************
 * CHECK GetFeatureExtractor *
 *****************************/

TEST_F(ScmComponents_Creation, Check_FeatureExtractor)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetFeatureExtractor()->HasJamVelocityEgo(), scmComponents.GetFeatureExtractor()->HasJamVelocityEgo());
}

/*******************************
 * CHECK GetMentalCalculations *
 *******************************/

TEST_F(ScmComponents_Creation, Check_MentalCalculations)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetMentalCalculations()->GetInfDistance(AreaOfInterest::EGO_FRONT), scmComponents.GetMentalCalculations()->GetInfDistance(AreaOfInterest::EGO_FRONT));
}

/***************************************************
 * CHECK GetIgnoringOuterLaneOvertakingProhibition *
 ***************************************************/

TEST_F(ScmComponents_Creation, Check_IgnoringOuterLaneOvertakingProhibition)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetIgnoringOuterLaneOvertakingProhibition()->IsFulfilled(AreaOfInterest::EGO_FRONT), scmComponents.GetIgnoringOuterLaneOvertakingProhibition()->IsFulfilled(AreaOfInterest::EGO_FRONT));
}

/*********************
 * CHECK GetSwerving *
 *********************/

TEST_F(ScmComponents_Creation, Check_Swerving)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetSwerving()->GetSwervingState(), scmComponents.GetSwerving()->GetSwervingState());
}

/*************************************
 * CHECK GetLongitudinalCalculations *
 *************************************/

TEST_F(ScmComponents_Creation, Check_LongitudinalCalculations)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  NiceMock<FakeSurroundingVehicle> fakeObservedVehicle;

  ON_CALL(fakeObservedVehicle, GetLongitudinalVelocity()).WillByDefault(Return(10_mps));

  OwnVehicleInformationScmExtended egoVehicle{};

  ASSERT_EQ(scmComponents.GetLongitudinalCalculations()->CalculateVelocityDelta(fakeObservedVehicle, egoVehicle), scmComponents.GetLongitudinalCalculations()->CalculateVelocityDelta(fakeObservedVehicle, egoVehicle));
}

/***********************
 * CHECK GetZipMerging *
 ***********************/

TEST_F(ScmComponents_Creation, Check_ZipMerging)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  auto parameters = scmComponents.GetZipMerging()->GetZipMergeParameters();
  parameters.active = true;

  scmComponents.GetZipMerging()->SetZipMergeParameters(parameters);
  auto result = scmComponents.GetZipMerging()->GetZipMergeParameters();

  ASSERT_TRUE(result.active);
}

/**********************************
 * CHECK GetGazeControlComponents *
 **********************************/

TEST_F(ScmComponents_Creation, Check_GazeControlComponents)
{
  auto scmDependencies = GetValidScmDependencies();
  ScmComponents scmComponents(*scmDependencies);

  ASSERT_EQ(scmComponents.GetGazeControlComponents()->GetBottomUpRequest()->GetBottomUpIntensities(), scmComponents.GetGazeControlComponents()->GetBottomUpRequest()->GetBottomUpIntensities());
}