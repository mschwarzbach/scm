################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME ScmComponents_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)
set(SCM_INCLUDE_DIR ${ROOT_DIR}/include)

# Automated include of all SurroundingVehicles source files
file(GLOB 
     SRC_SUR_VEH
     CONFIGURE_DEPENDS
     FOLLOW_SYMLINKS false
     LIST_DIRECTORIES false
     "${DRIVER_DIR}/src/SurroundingVehicles/*.cpp")

file(GLOB 
     HEADER_SUR_VEH
     CONFIGURE_DEPENDS
     FOLLOW_SYMLINKS false
     LIST_DIRECTORIES false
     "${DRIVER_DIR}/src/SurroundingVehicles/*.h")

add_scm_target(
     NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
     DEFAULT_MAIN

     SOURCES
     ${SRC_SUR_VEH}
     UnitTestsScmComponents.cpp
     ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationAdjustmentParameters.cpp
     ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationCalculationsQuery.cpp
     ${DRIVER_DIR}/src/AccelerationCalculations/AccelerationComponents.cpp
     ${DRIVER_DIR}/src/ActionImplementation.cpp
     ${DRIVER_DIR}/src/ActionManager.cpp
     ${PARAMETER_DIR}/src/DriverParameterModifier.cpp
     ${DRIVER_DIR}/src/AlgorithmSceneryCar.cpp
     ${DRIVER_DIR}/src/ScmDriver.cpp
     ${DRIVER_DIR}/src/AoiAssigner.cpp
     ${DRIVER_DIR}/src/AuditoryPerception.cpp
     ${DRIVER_DIR}/src/Extrapolation.cpp
     ${DRIVER_DIR}/src/FeatureExtractor.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeControl.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeFollower.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeCone.cpp
     ${DRIVER_DIR}/src/GazeControl/TopDownRequest.cpp
     ${DRIVER_DIR}/src/GazeControl/BottomUpRequest.cpp
     ${DRIVER_DIR}/src/GazeControl/ImpulseRequest.cpp
     ${DRIVER_DIR}/src/GazeControl/StimulusRequest.cpp
     ${DRIVER_DIR}/src/GazeControl/OpticalInformation.cpp
     ${DRIVER_DIR}/src/GazeControl/CurrentGazeState.cpp
     ${DRIVER_DIR}/src/GazeControl/GazePeriphery.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeFieldQuery.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeUsefulFieldOfView.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeFovea.cpp
     ${DRIVER_DIR}/src/GazeControl/GazeControlComponents.cpp
     ${DRIVER_DIR}/src/InformationAcquisition.cpp
     ${DRIVER_DIR}/src/InfrastructureCharacteristics.cpp
     ${DRIVER_DIR}/src/LaneChangeBehavior.cpp
     ${DRIVER_DIR}/src/LaneChangeBehaviorQuery.cpp
     ${DRIVER_DIR}/src/LaneChangeDimension.cpp
     ${DRIVER_DIR}/src/LaneQueryHelper.cpp
     ${DRIVER_DIR}/src/LaneKeepingCalculations.cpp
     ${DRIVER_DIR}/src/MentalCalculations.cpp
     ${DRIVER_DIR}/src/MentalModel.cpp
     ${DRIVER_DIR}/src/Merging/Merging.cpp
     ${DRIVER_DIR}/src/Merging/ZipMerging.cpp
     ${DRIVER_DIR}/src/MicroscopicCharacteristics.cpp
     ${DRIVER_DIR}/src/PeriodicLogger.cpp
     ${DRIVER_DIR}/src/Reliability.cpp
     ${DRIVER_DIR}/src/ScmCommons.cpp
     ${DRIVER_DIR}/src/ScmComponents.cpp
     ${DRIVER_DIR}/src/ScmDependencies.cpp
     ${DRIVER_DIR}/src/ScmSignalCollector.cpp
     ${DRIVER_DIR}/src/ScmLifetimeState.cpp
     ${DRIVER_DIR}/src/SpawningState.cpp
     ${DRIVER_DIR}/src/Swerving/Swerving.cpp
     ${DRIVER_DIR}/src/LivingState.cpp
     ${DRIVER_DIR}/src/SituationManager.cpp
     ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.cpp
     ${DRIVER_DIR}/src/SurroundingVehicles/LeadingVehicleSelector.cpp
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicle.cpp
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicles.cpp
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehiclesModifier.cpp
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleQuery.cpp
     ${PARAMETER_DIR}/src/DriverCharacteristicsSampler.cpp
     ${PARAMETER_DIR}/src/ScmParameters.cpp
     ${PARAMETER_DIR}/src/TrafficRules/TrafficRules.cpp
     ${PARAMETER_DIR}/src/DriverBehavior/DriverBehavior.cpp
     ${DRIVER_DIR}/src/HighCognitive/HighCognitive.cpp
     ${DRIVER_DIR}/src/HighCognitive/AnticipatedLaneChangerModel.cpp
     ${DRIVER_DIR}/src/HighCognitive/ApproachFollowClassifier.cpp
     ${DRIVER_DIR}/src/HighCognitive/ApproachSaliencyClassifier.cpp
     ${DRIVER_DIR}/src/HighCognitive/FollowSaliencyClassifier.cpp
     ${DRIVER_DIR}/src/HighCognitive/LaneChangeTrajectories.cpp
     ${DRIVER_DIR}/src/HighCognitive/LaneChangerCachedData.cpp
     ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.cpp
     ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.cpp
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafety.cpp
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyQuery.cpp
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyCalculations.cpp
     ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.cpp
     ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.cpp
     ${DRIVER_DIR}/src/TrajectoryCalculations/TrajectoryCalculations.cpp
     ${DRIVER_DIR}/src/TrajectoryCalculations/SteeringManeuver.cpp
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlanner.cpp
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlanningQuery.cpp
     ${DRIVER_DIR}/src/TrafficFlow/TrafficFlow.cpp
     ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocity.cpp
     ${ROOT_DIR}/include/common/XmlParser.cpp
     ${ROOT_DIR}/src/Logging/Logging.cpp

     HEADERS
     ${HEADER_SUR_VEH}
     ${PARAMETER_DIR}/src/DriverParameterModifier.h
     ${DRIVER_DIR}/src/ScmDependencies.h
     ${DRIVER_DIR}/src/ScmComponents.h
     ${DRIVER_DIR}/src/ScmSignalCollector.h
     ${DRIVER_DIR}/src/ActionImplementation.h
     ${DRIVER_DIR}/src/ActionManager.h
     ${DRIVER_DIR}/src/AlgorithmSceneryCar.h
     ${DRIVER_DIR}/src/AlgorithmScmGlobal.h
     ${DRIVER_DIR}/src/ScmDriver.h
     ${DRIVER_DIR}/src/AoiAssigner.h
     ${DRIVER_DIR}/src/AuditoryPerception.h
     ${DRIVER_DIR}/src/Extrapolation.h
     ${DRIVER_DIR}/src/FeatureExtractor.h
     ${DRIVER_DIR}/src/GazeControl/GazeControl.h
     ${DRIVER_DIR}/src/GazeControl/GazeParameters.h
     ${DRIVER_DIR}/src/GazeControl/GazeFollower.h
     ${DRIVER_DIR}/src/GazeControl/GazeCone.h
     ${DRIVER_DIR}/src/GazeControl/TopDownRequest.h
     ${DRIVER_DIR}/src/GazeControl/BottomUpRequest.h
     ${DRIVER_DIR}/src/GazeControl/ImpulseRequest.h
     ${DRIVER_DIR}/src/GazeControl/StimulusRequest.h
     ${DRIVER_DIR}/src/GazeControl/OpticalInformation.h
     ${DRIVER_DIR}/src/GazeControl/CurrentGazeState.h
     ${DRIVER_DIR}/src/GazeControl/GazePeriphery.h
     ${DRIVER_DIR}/src/GazeControl/GazeFieldQuery.h
     ${DRIVER_DIR}/src/GazeControl/GazeUsefulFieldOfView.h
     ${DRIVER_DIR}/src/GazeControl/GazeFovea.h
     ${DRIVER_DIR}/src/GazeControl/GazeControlComponents.h
     ${DRIVER_DIR}/src/InformationAcquisition.h
     ${DRIVER_DIR}/src/InfrastructureCharacteristics.h
     ${DRIVER_DIR}/src/LaneChangeBehaviorInterface.h
     ${DRIVER_DIR}/src/LaneChangeBehaviorQueryInterface.h
     ${DRIVER_DIR}/src/LaneChangeDimension.h
     ${DRIVER_DIR}/src/LaneQueryHelper.h
     ${DRIVER_DIR}/src/LaneKeepingCalculations.h
     ${DRIVER_DIR}/src/MentalCalculations.h
     ${DRIVER_DIR}/src/MentalModel.h
     ${DRIVER_DIR}/src/MentalModelDefinitions.h
     ${DRIVER_DIR}/src/MentalModelInterface.h
     ${DRIVER_DIR}/src/Merging/Merging.h
     ${DRIVER_DIR}/src/Merging/ZipMerging.h
     ${DRIVER_DIR}/src/MicroscopicCharacteristics.h
     ${DRIVER_DIR}/src/PeriodicLogger.h
     ${DRIVER_DIR}/src/Reliability.h
     ${SCM_INCLUDE_DIR}/ScmSampler.h
     ${DRIVER_DIR}/src/ScmCommons.h
     ${DRIVER_DIR}/src/ScmDefinitions.h
     ${DRIVER_DIR}/src/ScmLifetimeState.h
     ${DRIVER_DIR}/src/SpawningState.h
     ${DRIVER_DIR}/src/LivingState.h
     ${DRIVER_DIR}/src/SituationManager.h
     ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.h
     ${DRIVER_DIR}/src/SurroundingVehicles/LeadingVehicleSelector.h
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicle.h
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicles.h
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehiclesModifier.h
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleQuery.h
     ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleQueryFactory.h
     ${PARAMETER_DIR}/src/DriverCharacteristics.h
     ${PARAMETER_DIR}/src/DriverCharacteristicsSampler.h
     ${PARAMETER_DIR}/src/ScmParameters.h
     ${PARAMETER_DIR}/src/Signals/ParametersScmSignal.h
     ${PARAMETER_DIR}/src/TrafficRules/TrafficRules.h
     ${PARAMETER_DIR}/src/DriverBehavior/DriverBehavior.h
     ${DRIVER_DIR}/src/HighCognitive/HighCognitive.h
     ${DRIVER_DIR}/src/HighCognitive/AnticipatedLaneChangerModel.h
     ${DRIVER_DIR}/src/HighCognitive/ApproachFollowClassifier.h
     ${DRIVER_DIR}/src/HighCognitive/ApproachSaliencyClassifier.h
     ${DRIVER_DIR}/src/HighCognitive/FollowSaliencyClassifier.h
     ${DRIVER_DIR}/src/HighCognitive/LaneChangeTrajectories.h
     ${DRIVER_DIR}/src/HighCognitive/LaneChangerCachedData.h
     ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.h
     ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.h
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafety.h
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyQuery.h
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyCalculations.h
     ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyCalculationsInterface.h
     ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h
     ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.h
     ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculationsInterface.h
     ${DRIVER_DIR}/src/TrajectoryCalculations/TrajectoryCalculations.h
     ${DRIVER_DIR}/src/TrajectoryCalculations/SteeringManeuver.h
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlannerInterface.h
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlanner.h
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlanningQuery.h
     ${DRIVER_DIR}/src/TrajectoryPlanner/TrajectoryPlanningQueryInterface.h
     ${DRIVER_DIR}/src/TrafficFlow/TrafficFlow.h
     ${DRIVER_DIR}/src/TrafficFlow/TrafficFlowInterface.h
     ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocity.h
     ${DRIVER_DIR}/src/TrafficFlow/LaneMeanVelocityInterface.h
     ${ROOT_DIR}/include/common/XmlParser.h
     ${ROOT_DIR}/include/common/ScmEnums.h
     ${ROOT_DIR}/src/Logging/Logging.h

     INCDIRS
     ${DRIVER_DIR}
     ${DRIVER_DIR}/src
     ${DRIVER_DIR}/src/Swerving
     ${DRIVER_DIR}/src/HighCognitive
     ${DRIVER_DIR}/src/Signals
     ${SCM_INCLUDE_DIR}
     ${PARAMETER_DIR}/src/Signals

     LIBRARIES
     Iconv::Iconv
     LibXml2::LibXml2
     Stochastics::Stochastics
     OsiQueryLibrary
)

