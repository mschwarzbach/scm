/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TestGroundtruthGenerator.h"

#include <OsiQueryLibrary/osiql.h>
osi3::GroundTruth CREATE_GROUNDTRUTH()
{
  using osiql::operator-;

  osi3::GroundTruth groundTruth;

  osi3::LogicalLane* logicalLane{groundTruth.add_logical_lane()};
  {
    // ReferenceLine
    osi3::ReferenceLine* referenceLine = groundTruth.add_reference_line();
    referenceLine->mutable_id()->set_value(0);
    referenceLine->set_type(osi3::ReferenceLine_Type_TYPE_POLYLINE);

    const auto addPoint = [referenceLine](const auto& xy)
    {
      osi3::ReferenceLine_ReferenceLinePoint* point{referenceLine->add_poly_line()};
      point->mutable_world_position()->set_x(xy.x);
      point->mutable_world_position()->set_y(xy.y);
    };

    osiql::Vector2d startpoint{0, 0};
    osiql::Vector2d endpoint{100, 0};
    addPoint(startpoint);
    addPoint(endpoint);

    referenceLine->mutable_poly_line(0)->set_s_position(0.0);
    for (auto next{std::next(referenceLine->mutable_poly_line()->begin())}; next != referenceLine->mutable_poly_line()->end(); ++next)
    {
      next->set_s_position(std::prev(next)->s_position() + (*std::prev(next) - *next).Length());
    }

    // LogicalLaneBoundary left
    osi3::LogicalLaneBoundary* boundaryLeft{groundTruth.add_logical_lane_boundary()};
    boundaryLeft->mutable_id()->set_value(1);
    const auto addPointLeft = [boundaryLeft](const auto& xy)
    {
      osi3::LogicalLaneBoundary_LogicalBoundaryPoint* point{boundaryLeft->add_boundary_line()};
      point->mutable_position()->set_x(xy.x);
      point->mutable_position()->set_x(xy.x);
    };

    osiql::Vector2d startpointBoundaryLeft{0, 0};
    osiql::Vector2d endpointBoundaryLeft{100, 0};
    addPointLeft(startpointBoundaryLeft);
    addPointLeft(endpointBoundaryLeft);

    // LogicalLaneBoundary right
    osi3::LogicalLaneBoundary* boundaryRight{groundTruth.add_logical_lane_boundary()};
    boundaryRight->mutable_id()->set_value(2);
    const auto addPointRight = [boundaryRight](const auto& xy)
    {
      osi3::LogicalLaneBoundary_LogicalBoundaryPoint* point{boundaryRight->add_boundary_line()};
      point->mutable_position()->set_x(xy.x);
      point->mutable_position()->set_x(xy.x);
    };

    osiql::Vector2d startpointBoundaryRight{0, -2};
    osiql::Vector2d endpointBoundaryRight{100, -2};
    addPointRight(startpointBoundaryRight);
    addPointRight(endpointBoundaryRight);

    // LogicalLaneBoundary center
    osi3::LogicalLaneBoundary* boundaryCenter{groundTruth.add_logical_lane_boundary()};
    boundaryCenter->mutable_id()->set_value(2);
    const auto addPointCenter = [boundaryCenter](const auto& xy)
    {
      osi3::LogicalLaneBoundary_LogicalBoundaryPoint* point{boundaryCenter->add_boundary_line()};
      point->mutable_position()->set_x(xy.x);
      point->mutable_position()->set_x(xy.x);
    };

    osiql::Vector2d startpointBoundaryCenter{0, -1};
    osiql::Vector2d endpointBoundaryCenter{100, -1};
    addPointCenter(startpointBoundaryCenter);
    addPointCenter(endpointBoundaryCenter);

    osiql::ReferenceLine localizer{*referenceLine};
    for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint& point : *boundaryLeft->mutable_boundary_line())
    {
      const auto coordinates{localizer.Localize(point.position())};
      point.set_s_position(coordinates.latitude);
      point.set_t_position(coordinates.longitude);
    }
    for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint& point : *boundaryRight->mutable_boundary_line())
    {
      const auto coordinates{localizer.Localize(point.position())};
      point.set_s_position(coordinates.latitude);
      point.set_t_position(coordinates.longitude);
    }

    // LogicalLane
    logicalLane->mutable_id()->set_value(3);
    logicalLane->mutable_source_reference()->Add()->add_identifier("road_1");
    logicalLane->mutable_reference_line_id()->set_value(referenceLine->id().value());
    logicalLane->set_start_s(referenceLine->poly_line().begin()->s_position());
    logicalLane->set_end_s(referenceLine->poly_line().rbegin()->s_position());
    logicalLane->add_left_boundary_id()->set_value(boundaryLeft->id().value());
    logicalLane->add_right_boundary_id()->set_value(boundaryCenter->id().value());
    logicalLane->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);

    osi3::LogicalLane* logicalLane2{groundTruth.add_logical_lane()};
    logicalLane2->mutable_id()->set_value(5);
    logicalLane2->mutable_source_reference()->Add()->add_identifier("road_1");
    logicalLane2->mutable_reference_line_id()->set_value(referenceLine->id().value());
    logicalLane2->set_start_s(referenceLine->poly_line().begin()->s_position());
    logicalLane2->set_end_s(referenceLine->poly_line().rbegin()->s_position());
    logicalLane2->add_left_boundary_id()->set_value(boundaryCenter->id().value());
    logicalLane2->add_right_boundary_id()->set_value(boundaryRight->id().value());
    logicalLane2->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);

    auto laneRelation = logicalLane->add_right_adjacent_lane();
    laneRelation->mutable_other_lane_id()->set_value(5);
    laneRelation->set_start_s(0);
    laneRelation->set_start_s_other(0);
    laneRelation->set_end_s(100);
    laneRelation->set_end_s_other(100);

    laneRelation = logicalLane2->add_left_adjacent_lane();
    laneRelation->mutable_other_lane_id()->set_value(3);
    laneRelation->set_start_s(0);
    laneRelation->set_start_s_other(0);
    laneRelation->set_end_s(100);
    laneRelation->set_end_s_other(100);
  }
  // ReferenceLine
  osi3::LogicalLane* logicalLane3{groundTruth.add_logical_lane()};
  {
    osi3::ReferenceLine* referenceLine = groundTruth.add_reference_line();
    referenceLine->mutable_id()->set_value(9);
    referenceLine->set_type(osi3::ReferenceLine_Type_TYPE_POLYLINE);

    const auto addPoint = [referenceLine](const auto& xy)
    {
      osi3::ReferenceLine_ReferenceLinePoint* point{referenceLine->add_poly_line()};
      point->mutable_world_position()->set_x(xy.x);
      point->mutable_world_position()->set_y(xy.y);
    };

    osiql::Vector2d startpoint{100, 0};
    osiql::Vector2d endpoint{200, 0};
    addPoint(startpoint);
    addPoint(endpoint);

    referenceLine->mutable_poly_line(0)->set_s_position(0.0);
    for (auto next{std::next(referenceLine->mutable_poly_line()->begin())}; next != referenceLine->mutable_poly_line()->end(); ++next)
    {
      next->set_s_position(std::prev(next)->s_position() + (*std::prev(next) - *next).Length());
    }

    // LogicalLaneBoundary left
    osi3::LogicalLaneBoundary* boundaryLeft{groundTruth.add_logical_lane_boundary()};
    boundaryLeft->mutable_id()->set_value(6);
    const auto addPointLeft = [boundaryLeft](const auto& xy)
    {
      osi3::LogicalLaneBoundary_LogicalBoundaryPoint* point{boundaryLeft->add_boundary_line()};
      point->mutable_position()->set_x(xy.x);
      point->mutable_position()->set_x(xy.x);
    };

    osiql::Vector2d startpointBoundaryLeft{100, 0};
    osiql::Vector2d endpointBoundaryLeft{200, 0};
    addPointLeft(startpointBoundaryLeft);
    addPointLeft(endpointBoundaryLeft);

    // LogicalLaneBoundary right
    osi3::LogicalLaneBoundary* boundaryRight{groundTruth.add_logical_lane_boundary()};
    boundaryRight->mutable_id()->set_value(7);
    const auto addPointRight = [boundaryRight](const auto& xy)
    {
      osi3::LogicalLaneBoundary_LogicalBoundaryPoint* point{boundaryRight->add_boundary_line()};
      point->mutable_position()->set_x(xy.x);
      point->mutable_position()->set_x(xy.x);
    };

    osiql::Vector2d startpointBoundaryRight{100, -1};
    osiql::Vector2d endpointBoundaryRight{200, -1};
    addPointRight(startpointBoundaryRight);
    addPointRight(endpointBoundaryRight);

    osiql::ReferenceLine localizer{*referenceLine};
    for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint& point : *boundaryLeft->mutable_boundary_line())
    {
      const auto coordinates{localizer.Localize(point.position())};
      point.set_s_position(coordinates.latitude);
      point.set_t_position(coordinates.longitude);
    }
    for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint& point : *boundaryRight->mutable_boundary_line())
    {
      const auto coordinates{localizer.Localize(point.position())};
      point.set_s_position(coordinates.latitude);
      point.set_t_position(coordinates.longitude);
    }

    // LogicalLane

    logicalLane3->mutable_id()->set_value(4);
    logicalLane3->mutable_source_reference()->Add()->add_identifier("road_2");
    logicalLane3->mutable_reference_line_id()->set_value(referenceLine->id().value());
    logicalLane3->set_start_s(referenceLine->poly_line().begin()->s_position());
    logicalLane3->set_end_s(referenceLine->poly_line().rbegin()->s_position());
    logicalLane3->add_left_boundary_id()->set_value(boundaryLeft->id().value());
    logicalLane3->add_right_boundary_id()->set_value(boundaryRight->id().value());
    logicalLane3->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);

    auto connection = logicalLane->add_successor_lane();
    connection->set_at_begin_of_other_lane(true);
    connection->mutable_other_lane_id()->set_value(4);

    connection = logicalLane3->add_predecessor_lane();
    connection->set_at_begin_of_other_lane(false);
    connection->mutable_other_lane_id()->set_value(3);
  }

  return groundTruth;
}