/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MicroscopicCharacteristics.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct MicroscopicCharacteristicsTester
{
  class MicroscopicCharacteristicsUnderTest : MicroscopicCharacteristics
  {
  public:
    template <typename... Args>
    MicroscopicCharacteristicsUnderTest(Args&&... args)
        : MicroscopicCharacteristics{std::forward<Args>(args)...} {};

    using ::MicroscopicCharacteristics::GetObjectVector;
    using ::MicroscopicCharacteristics::GetSideObjectsInformation;
    using ::MicroscopicCharacteristics::GetSideObjectVector;
    using ::MicroscopicCharacteristics::UpdateObjectInformation;

    void SET_SURROUNDING_OBJECT_INFORMATION(std::unique_ptr<SurroundingObjectsScmExtended> surroundingObjects)
    {
      _surroundingObjectInformation = std::move(surroundingObjects);
    }
  };

  MicroscopicCharacteristicsTester()
      : microscopicCharacteristics()
  {
  }

  MicroscopicCharacteristicsUnderTest microscopicCharacteristics;
};

/*********************************
 * CHECK UpdateObjectInformation *
 *********************************/

struct DataFor_UpdateObjectInformation
{
  AreaOfInterest aoi;
  int expectedResult;
};

class MicroscopicCharacteristics_UpdateObjectInformation : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_UpdateObjectInformation>
{
};

TEST_P(MicroscopicCharacteristics_UpdateObjectInformation, Check_UpdateObjectInformation)
{
  MicroscopicCharacteristicsTester TEST_HELPER;
  DataFor_UpdateObjectInformation data = GetParam();

  ObjectInformationScmExtended hud, infotainment, instrumentCluster, egoFront;
  hud.id = 1;
  infotainment.id = 2;
  instrumentCluster.id = 3;
  egoFront.id = 4;

  SurroundingObjectsScmExtended surroundingObjects;
  surroundingObjects.objectHUD = hud;
  surroundingObjects.objectInfotainment = infotainment;
  surroundingObjects.objectInstrumentCluster = instrumentCluster;
  surroundingObjects.Ahead().Close().push_back(egoFront);

  TEST_HELPER.microscopicCharacteristics.SET_SURROUNDING_OBJECT_INFORMATION(std::move(std::make_unique<SurroundingObjectsScmExtended>(surroundingObjects)));
  auto result = TEST_HELPER.microscopicCharacteristics.UpdateObjectInformation(data.aoi);

  if (data.aoi == AreaOfInterest::EGO_FRONT_FAR)
  {
    EXPECT_EQ(result, nullptr);
  }
  else
  {
    ASSERT_EQ(result->id, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MicroscopicCharacteristics_UpdateObjectInformation,
    testing::Values(
        DataFor_UpdateObjectInformation{
            .aoi = AreaOfInterest::HUD,
            .expectedResult = 1},
        DataFor_UpdateObjectInformation{
            .aoi = AreaOfInterest::INFOTAINMENT,
            .expectedResult = 2},
        DataFor_UpdateObjectInformation{
            .aoi = AreaOfInterest::INSTRUMENT_CLUSTER,
            .expectedResult = 3},
        DataFor_UpdateObjectInformation{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 4},
        DataFor_UpdateObjectInformation{
            .aoi = AreaOfInterest::EGO_FRONT_FAR,
            .expectedResult = 4}));

/*****************************
 * CHECK GetSideObjectVector *
 *****************************/

struct DataFor_GetSideObjectVector
{
  AreaOfInterest aoi;
  int expectedResult;
};

class MicroscopicCharacteristics_GetSideObjectVector : public ::testing::Test,
                                                       public ::testing::WithParamInterface<DataFor_GetSideObjectVector>
{
};
TEST_P(MicroscopicCharacteristics_GetSideObjectVector, Check_GetSideObjectVector)
{
  MicroscopicCharacteristicsTester TEST_HELPER;
  DataFor_GetSideObjectVector data = GetParam();

  ObjectInformationScmExtended left_side, right_side, leftleft_side, rightright_side;
  left_side.id = 1;
  right_side.id = 2;
  rightright_side.id = 3;
  leftleft_side.id = 4;

  SurroundingObjectsScmExtended surroundingObjects;
  surroundingObjects.Close().Left().push_back(left_side);
  surroundingObjects.Close().Right().push_back(right_side);
  surroundingObjects.Close().FarLeft().push_back(leftleft_side);
  surroundingObjects.Close().FarRight().push_back(rightright_side);

  TEST_HELPER.microscopicCharacteristics.SET_SURROUNDING_OBJECT_INFORMATION(std::move(std::make_unique<SurroundingObjectsScmExtended>(surroundingObjects)));

  if (data.aoi == AreaOfInterest::EGO_FRONT)
  {
    EXPECT_THROW(TEST_HELPER.microscopicCharacteristics.GetSideObjectVector(data.aoi), std::runtime_error);
  }
  else
  {
    auto result = TEST_HELPER.microscopicCharacteristics.GetSideObjectVector(data.aoi);
    ASSERT_EQ(result->at(0).id, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MicroscopicCharacteristics_GetSideObjectVector,
    testing::Values(
        DataFor_GetSideObjectVector{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = 1},
        DataFor_GetSideObjectVector{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .expectedResult = 2},
        DataFor_GetSideObjectVector{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .expectedResult = 3},
        DataFor_GetSideObjectVector{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .expectedResult = 4},
        DataFor_GetSideObjectVector{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 4}));

/*************************
 * CHECK GetObjectVector *
 *************************/

struct DataFor_GetObjectVector
{
  AreaOfInterest aoi;
  int expectedResult;
};

class MicroscopicCharacteristics_GetObjectVector : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_GetObjectVector>
{
};
TEST_P(MicroscopicCharacteristics_GetObjectVector, Check_GetSideObjectVector)
{
  MicroscopicCharacteristicsTester TEST_HELPER;
  DataFor_GetObjectVector data = GetParam();

  ObjectInformationScmExtended left_side;
  left_side.id = 1;

  SurroundingObjectsScmExtended surroundingObjects;
  surroundingObjects.Close().Left().push_back(left_side);

  TEST_HELPER.microscopicCharacteristics.SET_SURROUNDING_OBJECT_INFORMATION(std::move(std::make_unique<SurroundingObjectsScmExtended>(surroundingObjects)));

  if (data.aoi == AreaOfInterest::DISTRACTION)
  {
    EXPECT_THROW(TEST_HELPER.microscopicCharacteristics.GetObjectVector(data.aoi), std::runtime_error);
  }
  else
  {
    auto result = TEST_HELPER.microscopicCharacteristics.GetObjectVector(data.aoi);
    ASSERT_EQ(result->at(0).id, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MicroscopicCharacteristics_GetObjectVector,
    testing::Values(
        DataFor_GetObjectVector{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = 1},
        DataFor_GetObjectVector{
            .aoi = AreaOfInterest::DISTRACTION,
            .expectedResult = 1}));

/***********************************
 * CHECK GetSideObjectsInformation *
 ***********************************/

struct DataFor_GetSideObjectsInformation
{
  AreaOfInterest aoi;
  int expectedResult;
};

class MicroscopicCharacteristics_GetSideObjectsInformation : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_GetSideObjectsInformation>
{
};
TEST_P(MicroscopicCharacteristics_GetSideObjectsInformation, Check_GetSideObjectVector)
{
  MicroscopicCharacteristicsTester TEST_HELPER;
  DataFor_GetSideObjectsInformation data = GetParam();

  ObjectInformationScmExtended left_side, right_side, leftleft_side, rightright_side;
  left_side.id = 1;
  right_side.id = 2;
  rightright_side.id = 3;
  leftleft_side.id = 4;

  SurroundingObjectsScmExtended surroundingObjects;
  surroundingObjects.Close().Left().push_back(left_side);
  surroundingObjects.Close().Right().push_back(right_side);
  surroundingObjects.Close().FarLeft().push_back(leftleft_side);
  surroundingObjects.Close().FarRight().push_back(rightright_side);

  TEST_HELPER.microscopicCharacteristics.SET_SURROUNDING_OBJECT_INFORMATION(std::move(std::make_unique<SurroundingObjectsScmExtended>(surroundingObjects)));

  if (data.aoi == AreaOfInterest::EGO_FRONT)
  {
    EXPECT_THROW(TEST_HELPER.microscopicCharacteristics.GetSideObjectsInformation(data.aoi), std::runtime_error);
  }
  else
  {
    auto result = TEST_HELPER.microscopicCharacteristics.GetSideObjectsInformation(data.aoi);
    ASSERT_EQ(result->at(0).id, data.expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    MicroscopicCharacteristics_GetSideObjectsInformation,
    testing::Values(
        DataFor_GetSideObjectsInformation{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedResult = 1},
        DataFor_GetSideObjectsInformation{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .expectedResult = 2},
        DataFor_GetSideObjectsInformation{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .expectedResult = 3},
        DataFor_GetSideObjectsInformation{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .expectedResult = 4},
        DataFor_GetSideObjectsInformation{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedResult = 4}));