/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/*
 * Mock and test class for AoiAssigner
 */
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/AoiAssigner.h"
#include "module/driver/src/MicroscopicCharacteristics.h"

using ::testing::_;
using ::testing::An;
using ::testing::Return;
using ::testing::ReturnRef;

class TestAoiAssigner : public AoiAssigner
{
public:
  TestAoiAssigner(const InfrastructureCharacteristicsInterface& infrastructure, const OwnVehicleInformationSCM& egoInfo)
      : AoiAssigner(infrastructure, egoInfo){};
};