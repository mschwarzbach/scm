/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/*
 * Unit Tests for Class AoiAssigner
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <limits>
#include <ostream>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "ParametersScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "TestAoiAssigner.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/AoiAssigner.h"
#include "module/driver/src/MicroscopicCharacteristics.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRefOfCopy;

/******************************************
        Helper structs and functions
*******************************************/
struct SurroundingLaneWidths
{
  units::length::meter_t leftLeftLane{0._m};
  units::length::meter_t leftLane{0._m};
  units::length::meter_t egoLane{0._m};
  units::length::meter_t rightLane{0._m};
  units::length::meter_t rightRightLane{0._m};

  void Apply(NiceMock<FakeInfrastructureCharacteristics>* infrastructure) const
  {
    auto* geometryInformation{infrastructure->UpdateGeometryInformation()};
    LaneInformationGeometrySCM laneInfo;
    laneInfo.exists = leftLeftLane > 0_m;
    laneInfo.width = leftLeftLane;
    ON_CALL(*infrastructure, GetLaneInformationGeometry(RelativeLane::LEFTLEFT)).WillByDefault(ReturnRefOfCopy(laneInfo));
    laneInfo.exists = leftLane > 0_m;
    laneInfo.width = leftLane;
    ON_CALL(*infrastructure, GetLaneInformationGeometry(RelativeLane::LEFT)).WillByDefault(ReturnRefOfCopy(laneInfo));
    laneInfo.exists = egoLane > 0_m;
    laneInfo.width = egoLane;
    ON_CALL(*infrastructure, GetLaneInformationGeometry(RelativeLane::EGO)).WillByDefault(ReturnRefOfCopy(laneInfo));
    laneInfo.exists = rightLane > 0_m;
    laneInfo.width = rightLane;
    ON_CALL(*infrastructure, GetLaneInformationGeometry(RelativeLane::RIGHT)).WillByDefault(ReturnRefOfCopy(laneInfo));
    laneInfo.exists = rightRightLane > 0_m;
    laneInfo.width = rightRightLane;
    ON_CALL(*infrastructure, GetLaneInformationGeometry(RelativeLane::RIGHTRIGHT)).WillByDefault(ReturnRefOfCopy(laneInfo));
  }
};

struct SurroundingVehicleTestData
{
  units::length::meter_t relativeLateralPosition{units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)};
  units::length::meter_t relativeLongitudinalPosition{units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE)};
  bool toTheSideOfEgo{false};
};

void SetUpTestVehicle(::testing::NiceMock<FakeSurroundingVehicle>* vehicle, const SurroundingVehicleTestData testData)
{
  constexpr auto DEFAULT_LENGTH{3._m};
  ON_CALL(*vehicle, GetRelativeLateralPosition()).WillByDefault(Return(testData.relativeLateralPosition));
  ON_CALL(*vehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(testData.relativeLongitudinalPosition));
  ON_CALL(*vehicle, ToTheSideOfEgo()).WillByDefault(Return(testData.toTheSideOfEgo));
  ON_CALL(*vehicle, FrontDistance()).WillByDefault(Return(testData.relativeLongitudinalPosition));
  ON_CALL(*vehicle, RearDistance()).WillByDefault(Return(testData.relativeLongitudinalPosition - DEFAULT_LENGTH));
}

/***********************
 * CHECK AssignToLanes *
 ***********************/
struct DataFor_AssignToLane
{
  SurroundingLaneWidths laneWidths;
  units::length::meter_t egoPositionInLane;
  units::length::meter_t relativeLateralDistance;
  RelativeLane expectedLane;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_AssignToLane& obj)
  {
    return os
           << "Parameters: \n"
           << "laneWidths:"
           << "\n\t LeftLeft: " << obj.laneWidths.leftLeftLane
           << "\n\t Left: " << obj.laneWidths.leftLane
           << "\n\t Ego: " << obj.laneWidths.egoLane
           << "\n\t Right: " << obj.laneWidths.rightLane
           << "\n\t RightRight: " << obj.laneWidths.rightRightLane
           << "\n egoPositionInLane: " << obj.egoPositionInLane
           << "\n relativeLateralDistance: " << obj.relativeLateralDistance
           << "\n expectedLane: " << ScmCommons::to_string(obj.expectedLane);
  }
};

class AoiAssigner_AssignToLane : public ::testing::Test,
                                 public ::testing::WithParamInterface<DataFor_AssignToLane>
{
};

TEST_P(AoiAssigner_AssignToLane, AoiAssigner_CheckAssignToLane)
{
  DataFor_AssignToLane data = GetParam();

  ::testing::NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle;
  NiceMock<FakeInfrastructureCharacteristics> infrastructure;
  OwnVehicleInformationSCM egoInfo;

  data.laneWidths.Apply(&infrastructure);
  egoInfo.lateralPosition = data.egoPositionInLane;

  ON_CALL(fakeSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(data.relativeLateralDistance));

  const std::vector<SurroundingVehicleInterface*> vehicleVector{&fakeSurroundingVehicle};
  TestAoiAssigner testAoiAssigner(infrastructure, egoInfo);
  const auto result = testAoiAssigner.AssignToLanes(vehicleVector);

  ASSERT_EQ(1, result.count(data.expectedLane));
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    AoiAssigner_AssignToLane,
    testing::Values(
        /* Test 0: Ego in the middle of lane observed agent on right end of ego lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m},
            .egoPositionInLane = 0_m,
            .relativeLateralDistance = -2_m,
            .expectedLane = RelativeLane::EGO},
        /* Test 1: Ego in the middle of lane observed agent on left end of ego lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m},
            .egoPositionInLane = 0_m,
            .relativeLateralDistance = 2_m,
            .expectedLane = RelativeLane::EGO},
        /* Test 2: Ego to the left edge of ego lane, observed agent in the middle of ego lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m},
            .egoPositionInLane = 1.9_m,
            .relativeLateralDistance = -(4_m - 1.9_m),
            .expectedLane = RelativeLane::EGO},
        /* Test 3: Ego in the middle of ego lane, observed agent at right edge of left lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.leftLane = 4._m, .egoLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = 2.1_m,
            .expectedLane = RelativeLane::LEFT},
        /* Test 4: Ego in the middle ego lane, observed agent at left edge of left lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.leftLane = 4._m, .egoLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = 6._m,
            .expectedLane = RelativeLane::LEFT},
        /* Test 5: Ego in the middle ego lane, observed agent at right edge of leftleft lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.leftLeftLane = 4._m, .leftLane = 4._m, .egoLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = 6.1_m,
            .expectedLane = RelativeLane::LEFTLEFT},
        /* Test 6: Ego in the middle ego lane, observed agent at left edge of right lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m, .rightLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = -2.1_m,
            .expectedLane = RelativeLane::RIGHT},
        /* Test 7: Ego in the middle ego lane, observed agent at right edge of right lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m, .rightLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = -6.0_m,
            .expectedLane = RelativeLane::RIGHT},
        /* Test 8: Ego in the middle ego lane, observed agent at left edge of right right lane */
        DataFor_AssignToLane{
            .laneWidths = SurroundingLaneWidths{.egoLane = 4._m, .rightLane = 4._m, .rightRightLane = 4._m},
            .egoPositionInLane = 0._m,
            .relativeLateralDistance = -6.1_m,
            .expectedLane = RelativeLane::RIGHTRIGHT}));

/**********************
 * CHECK AssignToAois *
 **********************/
struct DataFor_AssignToAois
{
  AreaOfInterest aoi;
  std::vector<int> expectedVehicleIndices;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_AssignToAois& rhs)
  {
    os << "aoi: " << ScmCommons::AreaOfInterestToString(rhs.aoi);
    os << " expectedVehicleIndices: <";
    for (auto i : rhs.expectedVehicleIndices)
    {
      os << i << ", ";
    }
    os << ">\n";
    return os;
  }
};

constexpr auto ON_EGO_LANE{0._m};
constexpr auto ON_LEFT_LANE{3._m};
constexpr auto ON_LEFTLEFT_LANE{7._m};
constexpr auto ON_RIGHT_LANE{-3._m};
constexpr auto ON_RIGHTRIGHT_LANE{-7._m};
constexpr bool IS_SIDE_OF_EGO{true};
constexpr bool IS_NOT_SIDE_OF_EGO{false};
constexpr auto IN_FRONT_NEAR{3._m};
constexpr auto IN_FRONT_FAR{10._m};
constexpr auto SAME_HEIGHT_AS_EGO{0_m};
constexpr auto BEHIND_SIDE{-1._m};
constexpr auto BEHIND_NEAR{-5._m};
constexpr auto BEHIND_FAR{-10._m};

class AoiAssigner_AssignToAois : public ::testing::Test,
                                 public ::testing::WithParamInterface<DataFor_AssignToAois>
{
protected:
  void SetUp()
  {
    SurroundingLaneWidths laneWidths{
        .leftLeftLane = 4._m,
        .leftLane = 4._m,
        .egoLane = 4._m,
        .rightLane = 4._m,
        .rightRightLane = 4._m,
    };
    laneWidths.Apply(&infrastructure);

    const units::length::meter_t egoPositionInLane{0._m};
    egoInfo.lateralPosition = egoPositionInLane;
  }

  void DISTRIBUTE_VEHICLES_AROUND_EGO()
  {
    // Vehicle 0: Ego Front
    SetUpTestVehicle(&testVehicles[0], {ON_EGO_LANE, IN_FRONT_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 1: Ego Front Far
    SetUpTestVehicle(&testVehicles[1], {ON_EGO_LANE, IN_FRONT_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 2: Ego Front too far -> not an aoi
    SetUpTestVehicle(&testVehicles[2], {ON_EGO_LANE, IN_FRONT_FAR + 2._m, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 3: Ego Rear
    SetUpTestVehicle(&testVehicles[3], {ON_EGO_LANE, BEHIND_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 4: Ego Rear - Second ego rear vehicle -> gets dropped
    SetUpTestVehicle(&testVehicles[4], {ON_EGO_LANE, BEHIND_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 5: Left Front
    SetUpTestVehicle(&testVehicles[5], {ON_LEFT_LANE, IN_FRONT_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 6: Left Front Far
    SetUpTestVehicle(&testVehicles[6], {ON_LEFT_LANE, IN_FRONT_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 7: Left Side
    SetUpTestVehicle(&testVehicles[7], {ON_LEFT_LANE, IN_FRONT_NEAR, IS_SIDE_OF_EGO});

    // // Vehicle 8: Left Side
    SetUpTestVehicle(&testVehicles[8], {ON_LEFT_LANE, SAME_HEIGHT_AS_EGO, IS_SIDE_OF_EGO});

    // // Vehicle 9: Left Side
    SetUpTestVehicle(&testVehicles[9], {ON_LEFT_LANE, BEHIND_SIDE, IS_SIDE_OF_EGO});

    // // Vehicle 10: Left Rear
    SetUpTestVehicle(&testVehicles[10], {ON_LEFT_LANE, BEHIND_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 11:  - Second rear left vehicle -> gets dropped
    SetUpTestVehicle(&testVehicles[11], {ON_LEFT_LANE, BEHIND_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 12: - Right Front
    SetUpTestVehicle(&testVehicles[12], {ON_RIGHT_LANE, IN_FRONT_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 13: - Right Side 1
    SetUpTestVehicle(&testVehicles[13], {ON_RIGHT_LANE, IN_FRONT_NEAR, IS_SIDE_OF_EGO});

    // // Vehicle 14: - Right Side 2
    SetUpTestVehicle(&testVehicles[14], {ON_RIGHT_LANE, SAME_HEIGHT_AS_EGO, IS_SIDE_OF_EGO});

    // // Vehicle 15: - Right Rear
    SetUpTestVehicle(&testVehicles[15], {ON_RIGHT_LANE, BEHIND_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 16: - LeftLeft front
    SetUpTestVehicle(&testVehicles[16], {ON_LEFTLEFT_LANE, IN_FRONT_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 17: - LeftLeft Side
    SetUpTestVehicle(&testVehicles[17], {ON_LEFTLEFT_LANE, IN_FRONT_NEAR, IS_SIDE_OF_EGO});

    // // Vehicle 18: - LeftLeft Rear
    SetUpTestVehicle(&testVehicles[18], {ON_LEFTLEFT_LANE, BEHIND_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 19: - RightRight Front
    SetUpTestVehicle(&testVehicles[19], {ON_RIGHTRIGHT_LANE, IN_FRONT_NEAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 20: - RightRight Front second -> gets dropped
    SetUpTestVehicle(&testVehicles[20], {ON_RIGHTRIGHT_LANE, IN_FRONT_FAR, IS_NOT_SIDE_OF_EGO});

    // // Vehicle 21: - RightRight Side
    SetUpTestVehicle(&testVehicles[21], {ON_RIGHTRIGHT_LANE, BEHIND_SIDE, IS_SIDE_OF_EGO});

    // // Vehicle 22: - RightRight Rear
    SetUpTestVehicle(&testVehicles[22], {ON_RIGHTRIGHT_LANE, BEHIND_FAR, IS_NOT_SIDE_OF_EGO});
  }

  auto GET_VEHICLE_POINTERS()
  {
    std::vector<SurroundingVehicleInterface*> pointers;
    pointers.reserve(testVehicles.size());
    std::transform(begin(testVehicles), end(testVehicles), std::back_inserter(pointers), [](auto& vehicle)
                   { return &vehicle; });
    return pointers;
  }

  std::array<::testing::NiceMock<FakeSurroundingVehicle>, 23> testVehicles;
  NiceMock<FakeInfrastructureCharacteristics> infrastructure;
  OwnVehicleInformationSCM egoInfo;
};

TEST_P(AoiAssigner_AssignToAois, GivenDefaultVehicleDistirbution_AssignsVehicleToCorrectAoi)
{
  DISTRIBUTE_VEHICLES_AROUND_EGO();

  const auto data = GetParam();
  std::vector<SurroundingVehicleInterface*> expected_vector;
  for (const auto idx : data.expectedVehicleIndices)
  {
    expected_vector.push_back(&testVehicles.at(idx));
  }

  TestAoiAssigner testAoiAssigner(infrastructure, egoInfo);

  const auto pointers{GET_VEHICLE_POINTERS()};
  const auto resultVector{testAoiAssigner.AssignToAois(pointers).at(data.aoi)};

  ASSERT_EQ(resultVector, expected_vector);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    AoiAssigner_AssignToAois,
    testing::Values(
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::EGO_FRONT,
            .expectedVehicleIndices = {0}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::EGO_FRONT_FAR,
            .expectedVehicleIndices = {1}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .expectedVehicleIndices = {5}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedVehicleIndices = {6}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .expectedVehicleIndices = {7, 8, 9}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFT_REAR,
            .expectedVehicleIndices = {10}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .expectedVehicleIndices = {12}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .expectedVehicleIndices = {13, 14}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .expectedVehicleIndices = {15}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFTLEFT_FRONT,
            .expectedVehicleIndices = {16}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .expectedVehicleIndices = {17}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::LEFTLEFT_REAR,
            .expectedVehicleIndices = {18}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHTRIGHT_FRONT,
            .expectedVehicleIndices = {19}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .expectedVehicleIndices = {21}},
        DataFor_AssignToAois{
            .aoi = AreaOfInterest::RIGHTRIGHT_REAR,
            .expectedVehicleIndices = {22}}));

/****************************
 * CHECK AssignToAoisVector *
 ****************************/

TEST(AoiAssigner_AssignToAoisVector, AoiAssigner_CheckAssignToAoisVector)
{
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructure;

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicleSide;
  ON_CALL(fakeSurroundingVehicleSide, GetId()).WillByDefault(Return(1));
  ON_CALL(fakeSurroundingVehicleSide, ToTheSideOfEgo()).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicleFront;
  ON_CALL(fakeSurroundingVehicleFront, GetId()).WillByDefault(Return(2));
  ON_CALL(fakeSurroundingVehicleFront, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicleFront, GetRelativeLongitudinalPosition()).WillByDefault(Return(1.0_m));

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicleRear;
  ON_CALL(fakeSurroundingVehicleRear, GetId()).WillByDefault(Return(3));
  ON_CALL(fakeSurroundingVehicleRear, ToTheSideOfEgo()).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicleRear, GetRelativeLongitudinalPosition()).WillByDefault(Return(-1.0_m));

  const std::vector<SurroundingVehicleInterface*> vehicleVectorSide{&fakeSurroundingVehicleSide};
  const std::vector<SurroundingVehicleInterface*> vehicleVectorEgoLane{&fakeSurroundingVehicleFront, &fakeSurroundingVehicleRear};
  const std::vector<SurroundingVehicleInterface*> vehicleVectorRear{&fakeSurroundingVehicleRear};

  LaneVehicleMapping vehicleMap;
  vehicleMap.insert({RelativeLane::LEFT, vehicleVectorSide});
  vehicleMap.insert({RelativeLane::EGO, vehicleVectorEgoLane});

  OwnVehicleInformationSCM egoInfo;
  TestAoiAssigner testAoiAssigner(fakeInfrastructure, egoInfo);
  const auto result = testAoiAssigner.AssignToAoisVector(vehicleMap);

  ASSERT_EQ(result.at(AreaOfInterest::LEFT_SIDE).front()->GetId(), 1);
  ASSERT_EQ(result.at(AreaOfInterest::EGO_FRONT).front()->GetId(), 2);
  ASSERT_EQ(result.at(AreaOfInterest::EGO_REAR).front()->GetId(), 3);
}