################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME AoiAssigner_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN

  SOURCES
  UnitTestsAoiAssigner.cpp
  ${DRIVER_DIR}/src/AoiAssigner.cpp
  ${DRIVER_DIR}/src/ScmCommons.cpp
  ${DRIVER_DIR}/src/ScmDependencies.cpp

  HEADERS
  ../Fakes/FakeMentalCalculations.h
  ../Fakes/FakeInfrastructureCharacteristics.h
  ../Fakes/FakeSurroundingVehicle.h
  ${DRIVER_DIR}/src/AoiAssigner.h
  ${DRIVER_DIR}/src/ScmComponents.h
  ${DRIVER_DIR}/src/ScmCommons.h
  ${DRIVER_DIR}/src/ScmDependencies.h
  ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleInterface.h
  ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleDefinitions.h

  INCDIRS
  ${DRIVER_DIR}
  ${DRIVER_DIR}/src
  ${ROOT_DIR}/module/parameterParser/src/Signals
  ${ROOT_DIR}/module/sensor/src/Signals

  LIBRARIES
  Stochastics::Stochastics
)
