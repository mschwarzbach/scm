/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/TopDownRequestInterface.h"

/******************************************************************************
 * Mock class for testing functions and classes that depend on TopDownRequest *
 ******************************************************************************/
class FakeTopDownRequest : public TopDownRequestInterface
{
public:
  MOCK_CONST_METHOD3(Update, GazeStateIntensitiesAndDurations(LateralAction actionState, const std::map<AreaOfInterest, double> topDownRequestMap, bool isInit));
  MOCK_CONST_METHOD0(GetIntensitiesForLaneKeepingWithoutLeadingVehicle, std::map<GazeState, double>());
};