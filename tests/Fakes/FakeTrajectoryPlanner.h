/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

class FakeTrajectoryPlanner : public TrajectoryPlannerInterface
{
public:
  MOCK_CONST_METHOD1(PlanTrajectory, TrajectoryCalculations::Trajectory(const TrajectoryPlanning::TrajectoryDimensions&));
  MOCK_CONST_METHOD1(CalculateLaneChangeDimensions, TrajectoryCalculations::TrajectoryDimensions(RelativeLane));
};
