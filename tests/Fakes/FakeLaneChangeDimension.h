/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/LaneChangeDimension.h"

/***********************************************************************************
 * Mock class for testing functions and classes that depend LaneChangeDimension.    *
 ************************************************************************************/

class FakeLaneChangeDimension : public LaneChangeDimensionInterface
{
public:
  MOCK_CONST_METHOD0(EstimateLaneChangeLength, units::length::meter_t());
  MOCK_CONST_METHOD3(EstimateLaneChangeLength, units::length::meter_t(units::length::meter_t laneChangeWidth, units::acceleration::meters_per_second_squared_t lateralAcc, units::velocity::meters_per_second_t longitudinalVelocity));
};
