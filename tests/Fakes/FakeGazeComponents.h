/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/GazeControlComponentsInterface.h"

/*************************************************************************************
 * Mock class for testing functions and classes that depend on GazeControlComponents *
 *************************************************************************************/
class FakeGazeComponents : public GazeControlComponentsInterface
{
public:
  MOCK_METHOD0(GetBottomUpRequest, BottomUpRequestInterface*());
  MOCK_METHOD0(GetImpulseRequest, ImpulseRequestInterface*());
  MOCK_METHOD0(GetStimulusRequest, StimulusRequestInterface*());
  MOCK_METHOD0(GetTopDownRequest, TopDownRequestInterface*());
  MOCK_METHOD0(GetGazeFieldQuery, GazeFieldQueryInterface*());
  MOCK_METHOD0(GetGazePeriphery, GazePeripheryInterface*());
  MOCK_METHOD0(GetOpticalInformation, OpticalInformationInterface*());
  MOCK_METHOD0(GetUsefulFieldOfView, GazeUsefulFieldOfViewInterface*());
  MOCK_METHOD0(GetGazeFovea, GazeFoveaInterface*());
  MOCK_METHOD0(GetCurrentGazeState, CurrentGazeStateInterface*());
};