/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/LaneChangeBehavior.h"
#include "module/driver/src/LaneChangeBehaviorInterface.h"
#include "module/driver/src/MentalCalculationsInterface.h"
#include "module/driver/src/MentalModel.h"
#include "module/driver/src/ScmDefinitions.h"

/*****************************************************/
// Fake class to mock LaneChangeBehavior functionality
/*****************************************************/
class FakeLaneChangeBehavior : public LaneChangeBehaviorInterface
{
public:
  MOCK_METHOD1(CalculateLaneIntensities, void(const TrafficRulesScm& trafficRulesSc));
  MOCK_CONST_METHOD0(GetLaneChangeWish, SurroundingLane());
  MOCK_CONST_METHOD1(GetLaneConvenience, double(SurroundingLane));
  MOCK_CONST_METHOD1(CalculateBaseLaneConvenience, double(SurroundingLane));
};
