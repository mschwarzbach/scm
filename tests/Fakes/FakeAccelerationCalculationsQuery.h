/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "AccelerationCalculations/AccelerationCalculationsQueryInterface.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

/**********************************************************************************************
 * Mock class for testing functions and classes that depend on AccelerationCalculationsQuery. *
 **********************************************************************************************/

class FakeAccelerationCalculationsQuery : public AccelerationCalculationsQueryInterface
{
public:
  MOCK_CONST_METHOD0(IsEgoBelowJamSpeed, bool());
  MOCK_CONST_METHOD1(IsDetectedLaneChangerLeadingVehicle, bool(const SurroundingVehicleInterface &leadingVehicle));
  MOCK_CONST_METHOD1(GetEqDistance, units::length::meter_t(const SurroundingVehicleInterface &vehicle));
  MOCK_CONST_METHOD1(GetMinDistance, units::length::meter_t(const SurroundingVehicleInterface &vehicle));
  MOCK_CONST_METHOD1(GetMinDistanceDuringMerge, units::length::meter_t(const SurroundingVehicleInterface &vehicle));
  MOCK_CONST_METHOD0(GetReactionBaseTimeMean, units::time::second_t());
  MOCK_CONST_METHOD0(GetPedalChangeTimeMean, units::time::second_t());
  MOCK_CONST_METHOD0(GetCarQueuingDistance, units::length::meter_t());
  MOCK_CONST_METHOD0(GetUrgency, double());
  MOCK_CONST_METHOD0(IsEgoNearStandstill, bool());
  MOCK_CONST_METHOD0(GetEgoVelocity, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetComfortAcceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetMergingAcceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(IsEgoCooperative, bool());
  MOCK_CONST_METHOD0(GetMergeRegulateId, int());
  MOCK_CONST_METHOD0(GetMergeGap, Merge::Gap());
  MOCK_CONST_METHOD0(GetMentalModel, const MentalModelInterface&());
};