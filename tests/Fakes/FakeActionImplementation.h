/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock-function-mocker.h>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/ActionImplementationInterface.h"
#include "module/driver/src/ScmDefinitions.h"

/*************************************************************************************
 * Mock class for testing functions and classes that depend on ActionImplementation. *
 *************************************************************************************/

class FakeActionImplementation : public ActionImplementationInterface
{
public:
  MOCK_METHOD1(UpdateInformationFromActionManager, void(ActionManagerInterface* actionManagerInterface));
  MOCK_METHOD0(ImplementAction, void());
  MOCK_CONST_METHOD0(GetLateralDisplacementTraveled, units::length::meter_t());
  MOCK_CONST_METHOD0(GetDesiredLateralDisplacement, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLateralDisplacementReached, bool());
  MOCK_CONST_METHOD0(GetLongitudinalAccelerationWish, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetNextIndicatorState, scm::LightState::Indicator());
  MOCK_CONST_METHOD0(GetFlasherActiveNext, bool());
  MOCK_CONST_METHOD0(GetLateralDeviationGain, units::angular_acceleration::radians_per_second_squared_t());
  MOCK_CONST_METHOD0(GetLateralDeviationNext, units::length::meter_t());
  MOCK_CONST_METHOD0(GetHeadingErrorGain, units::frequency::hertz_t());
  MOCK_CONST_METHOD0(GetHeadingErrorNext, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetKappaManoeuvre, units::curvature::inverse_meter_t());
  MOCK_CONST_METHOD0(GetKappaRoad, units::curvature::inverse_meter_t());
  MOCK_CONST_METHOD0(GetKapaSet, units::curvature::inverse_meter_t());
  MOCK_CONST_METHOD0(GetDecelerationFromPowertrainDrag, units::acceleration::meters_per_second_squared_t());
};