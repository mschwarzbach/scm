/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/LaneChangeBehaviorQueryInterface.h"

/***********************************************************/
// Fake class to mock LaneChangeBehaviorQuery functionality
/***********************************************************/
class FakeLaneChangeBehaviorQuery : public LaneChangeBehaviorQueryInterface
{
public:
  MOCK_CONST_METHOD1(DetermineLaneChangeWishFromIntensities, SurroundingLane(const LaneChangeIntensities&));
  MOCK_CONST_METHOD2(AdjustLaneConvenienceDueToHighwayExit, LaneChangeIntensities(const LaneChangeIntensities&, bool isRightHandTraffic));
  MOCK_CONST_METHOD1(AdjustLaneChangeIntensitiesForJam, LaneChangeIntensities(const LaneChangeIntensities&));
  MOCK_CONST_METHOD2(CalculateLaneChangeIntensity, double(SurroundingLane, ComponentsForLaneChangeIntensity));
  MOCK_CONST_METHOD1(HasObstacleOnLane, bool(SurroundingLane));
  MOCK_CONST_METHOD1(ShouldChangeLaneInJam, bool(SurroundingLane));
  MOCK_CONST_METHOD1(GetRelativeNetDistance, units::length::meter_t(SurroundingLane));
  MOCK_CONST_METHOD1(IsLaneChangeSave, bool(SurroundingLane targetLane));
  MOCK_CONST_METHOD1(EgoFitsInLane, bool(SurroundingLane targetLane));
  MOCK_CONST_METHOD0(EgoBelowJamSpeed, bool());
  MOCK_CONST_METHOD1(IsOuterLaneOvertakingProhibited, bool(bool));
  MOCK_CONST_METHOD1(GetTimeAtTargetSpeedInLane, units::time::second_t(SurroundingLane lane));
  MOCK_CONST_METHOD1(CanVehicleCauseCollisionCourseForNonSideAreas, bool(const SurroundingVehicleInterface *vehicle));
  MOCK_CONST_METHOD1(GetIsInfluencingDistanceViolated, bool(const SurroundingVehicleInterface *vehicle));
  MOCK_CONST_METHOD1(GetIsLaneChangePermittedDueToLaneMarkings, bool(Side side));
  MOCK_CONST_METHOD1(NormalizeLaneChangeIntensities, LaneChangeIntensities(const LaneChangeIntensities&));
};