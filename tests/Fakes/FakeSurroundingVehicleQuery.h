/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryInterface.h"

class FakeSurroundingVehicleQueryFactory : public SurroundingVehicleQueryFactoryInterface
{
public:
  MOCK_CONST_METHOD1(GetQuery, std::shared_ptr<SurroundingVehicleQueryInterface>(const SurroundingVehicleInterface&));
};

class FakeSurroundingVehicleQuery : public SurroundingVehicleQueryInterface
{
public:
  MOCK_CONST_METHOD0(GetLateralObstructionDynamics, ObstructionDynamics());
  MOCK_CONST_METHOD0(GetLateralVelocityDelta, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetLongitudinalVelocityDelta, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetLongitudinalAccelerationDelta, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(EgoMovingFasterLateral, bool());
  MOCK_CONST_METHOD0(CannotClearLongitudinalObstructionBeforeCollision, bool());
  MOCK_CONST_METHOD1(DeltaDistancePerTime, units::length::meter_t(units::time::second_t));
  MOCK_CONST_METHOD1(WillEnterMinDistanceBeforeObstructionIsCleared, bool(units::length::meter_t));
  MOCK_CONST_METHOD1(HasCollisionCourse, bool(units::length::meter_t));
  MOCK_CONST_METHOD0(IsChangingLanesStillOnStartLane, bool());
  MOCK_CONST_METHOD0(IsChangingIntoEgoLane, bool());
  MOCK_CONST_METHOD0(IsSideSideVehicleChangingIntoSide, bool());
};