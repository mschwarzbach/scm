/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/TrafficFlow/TrafficFlowInterface.h"

class FakeTrafficFlow : public TrafficFlowInterface
{
public:
  MOCK_METHOD1(UpdateMeanVelocities, void(const std::vector<const SurroundingVehicleInterface*>& traffic));
  MOCK_METHOD1(GetMeanVelocity, units::velocity::meters_per_second_t(SurroundingLane surroundingLane));
  MOCK_METHOD0(Reset, void());
};