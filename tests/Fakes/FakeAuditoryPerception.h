/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/AuditoryPerceptionInterface.h"

/***********************************************************************************
 * Mock class for testing functions and classes that depend on AuditoryPerception. *
 ***********************************************************************************/

class FakeAuditoryPerception : public AuditoryPerceptionInterface
{
public:
  MOCK_CONST_METHOD1(IsAcousticStimulusDetected, bool(std::vector<AdasHmiSignal> acousticAdasSignals));
  MOCK_METHOD1(UpdateStimulusData, void(std::vector<AdasHmiSignal> acousticAdasSignals));
  MOCK_CONST_METHOD0(IsAcousticStimulusProcessed, bool());
  MOCK_CONST_METHOD0(GetAcousticStimulusDirection, AcousticStimulusDirection());
};