/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "TrajectoryPlanner/TrajectoryPlanningQueryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

class FakeTrajectoryPlanningQuery : public TrajectoryPlanningQueryInterface
{
public:
  MOCK_CONST_METHOD0(CreateDriverParameters, TrajectoryCalculations::DriverParameters());
  MOCK_CONST_METHOD0(CreateVehicleParameters, TrajectoryCalculations::VehicleParameters());
  MOCK_CONST_METHOD1(CreateLaneChangeWidthInput, TrajectoryPlanning::LaneChangeWidthParameters(RelativeLane targetLane));
  MOCK_CONST_METHOD2(CreateLaneChangeLengthInput, TrajectoryPlanning::LaneChangeLengthParameters(RelativeLane targetLane, const SurroundingVehicleInterface* laneChangeObstacle));
  MOCK_CONST_METHOD0(GetRelevantVehiclesForLaneChangeLength, TrajectoryPlanning::RelevantVehiclesForLaneChangeLength());
  MOCK_CONST_METHOD0(GetHeading, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetSituation, Situation());
};
