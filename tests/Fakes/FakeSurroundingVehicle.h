/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock-actions.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/ExtrapolationInterface.h"
#include "module/driver/src/InformationAcquisitionInterface.h"
#include "module/driver/src/ScmDefinitions.h"

class FakeSurroundingVehicle : public SurroundingVehicleInterface
{
public:
  FakeSurroundingVehicle()
  {
    ::testing::DefaultValue<VehicleParameter<bool>>::Set({false, 100});
    ::testing::DefaultValue<VehicleParameter<double>>::Set({0.0, 100});
  }
  MOCK_METHOD1(Extrapolate, void(const ExtrapolationInterface& extrapolation));
  MOCK_CONST_METHOD0(IsOverlapping, bool());
  MOCK_METHOD0(SetOverlapping, void());
  MOCK_CONST_METHOD0(GetId, int());
  MOCK_CONST_METHOD0(GetRelativeLongitudinalPosition, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(GetRelativeLateralPosition, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(FrontDistance, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(RearDistance, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(LeftDistance, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(RightDistance, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD1(Overlaps, VehicleParameter<bool>(const SurroundingVehicleInterface& other));
  MOCK_METHOD3(UpdateWithGroundTruth, void(const ObjectInformationSCM* gtObject, AreaOfInterest aoi, const InformationAcquisitionInterface* infoAcquisition));
  MOCK_CONST_METHOD0(ToTheSideOfEgo, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(BehindEgo, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(InFrontOfEgo, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(LateralObstructionOverlapping, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(WasPerceived, bool());
  MOCK_CONST_METHOD0(GetLaneOfPerception, VehicleParameter<RelativeLane>());
  MOCK_CONST_METHOD0(GetAssignedAoi, VehicleParameter<AreaOfInterest>());
  MOCK_CONST_METHOD0(GetObjectInformation, const ObjectInformationScmExtended*());
  MOCK_METHOD2(HandleTransitionFromSideArea, void(AreaOfInterest, const InformationAcquisitionInterface*));
  MOCK_METHOD1(HandleTransitionToSideArea, void(const InformationAcquisitionInterface*));
  MOCK_CONST_METHOD0(GetBoundingBox, VehicleParameter<Scm::BoundingBox>());
  MOCK_CONST_METHOD0(IsCollided, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(GetTtc, VehicleParameter<units::time::second_t>());
  MOCK_CONST_METHOD0(GetTauDot, VehicleParameter<double>());
  MOCK_CONST_METHOD0(GetThw, VehicleParameter<units::time::second_t>());
  MOCK_CONST_METHOD0(GetLateralObstruction, VehicleParameter<ObstructionScm>());
  MOCK_CONST_METHOD0(GetLongitudinalObstruction, VehicleParameter<ObstructionLongitudinal>());
  MOCK_CONST_METHOD0(GetLateralVelocity, VehicleParameter<units::velocity::meters_per_second_t>());
  MOCK_CONST_METHOD0(GetLongitudinalVelocity, VehicleParameter<units::velocity::meters_per_second_t>());
  MOCK_CONST_METHOD0(GetAbsoluteVelocity, VehicleParameter<units::velocity::meters_per_second_t>());
  MOCK_CONST_METHOD0(GetLongitudinalAcceleration, VehicleParameter<units::acceleration::meters_per_second_squared_t>());
  MOCK_CONST_METHOD0(IsExceedingLateralMotionThreshold, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(IsMergeRegulate, bool());
  MOCK_METHOD1(SetAsMergeRegulate, void(bool));
  MOCK_CONST_METHOD0(GetLength, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(GetWidth, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(GetHeight, VehicleParameter<units::length::meter_t>());
  MOCK_METHOD1(AssignAoi, void(AreaOfInterest));
  MOCK_METHOD1(AssignLane, void(RelativeLane));
  MOCK_CONST_METHOD0(GetLateralPositionInLane, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD0(IsStatic, VehicleParameter<bool>());
  MOCK_CONST_METHOD0(GetRelativeNetDistance, VehicleParameter<units::length::meter_t>());
  MOCK_CONST_METHOD1(GetDistanceToLaneBoundary, VehicleParameter<units::length::meter_t>(Side));
  MOCK_CONST_METHOD0(GetVehicleClassification, VehicleParameter<scm::common::VehicleClass>());
  MOCK_CONST_METHOD0(GetProjectedDimensions, VehicleParameter<ProjectedSpacialDimensions>());
  MOCK_CONST_METHOD0(GetIndicatorState, VehicleParameter<scm::LightState::Indicator>());
  MOCK_CONST_METHOD0(GetTtcThresholdLooming, double());
  MOCK_CONST_METHOD2(IsReliable, bool(double, ParameterChangeRate));
  MOCK_CONST_METHOD1(GetReliability, double(ParameterChangeRate));
  MOCK_METHOD1(SetReliability, void(double));
  MOCK_METHOD1(DecayReliability, void(units::time::millisecond_t));
  MOCK_CONST_METHOD0(GetAngleToOpticalAxis, units::angle::radian_t());
  MOCK_METHOD1(SetTimeOfLastPerception, void(units::time::millisecond_t));
  MOCK_CONST_METHOD0(GetTimeOfLastPerception, units::time::millisecond_t());
  MOCK_METHOD0(ResetWasPerceivedFlag, void());
};
