/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/GazeFieldQueryInterface.h"

/******************************************************************************
 * Mock class for testing functions and classes that depend on GazeFieldQuery *
 ******************************************************************************/

class FakeGazeFieldQuery : public GazeFieldQueryInterface
{
public:
  MOCK_CONST_METHOD1(FillSurroundingAoiObjects, std::map<AreaOfInterest, ObjectInformationSCM>(const OngoingTrafficObjectsSCM& ongoingTraffic));
  MOCK_CONST_METHOD5(AssignFieldOfViewToAoi, std::vector<AreaOfInterest>(AreaOfInterest aoiToCheck, const std::vector<AreaOfInterest>& vectorAoi, bool isUfov, const std::vector<units::angle::radian_t>& orientations, units::angle::radian_t gazeAngleHorizontal));
  MOCK_CONST_METHOD3(AssignUsefulFieldOfViewToAoi, AreaOfInterest(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal));
  MOCK_CONST_METHOD5(UpdateAreaOfInterest, std::vector<AreaOfInterest>(const ObjectInformationSCM& objectScmInformation, AreaOfInterest aoi, const std::vector<AreaOfInterest>& currentAoi, bool isUfov, units::angle::radian_t gazeAngleHorizontal));
};
