/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "src/FMU/src/OsmpShell/FmiLogInterface.h"

/********************************************************************************
 * Mock class for testing functions and classes that depend on FmiLogInterface. *
 ********************************************************************************/

class FakeFmiLogInterface : public FmiLogInterface
{
public:
  MOCK_METHOD2(Log, void(LogLevel logLevel, const std::string& message));
};