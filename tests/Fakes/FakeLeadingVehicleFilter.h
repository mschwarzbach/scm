/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "ScmDefinitions.h"
#include "SurroundingVehicles/LeadingVehicleFilterInterface.h"

class FakeLeadingVehicleFilter : public LeadingVehicleFilterInterface
{
public:
  MOCK_CONST_METHOD1(FilterRearVehicles, std::vector<const SurroundingVehicleInterface*>(const std::vector<const SurroundingVehicleInterface*>&));
  MOCK_CONST_METHOD1(FilterSideLaneVehicles, std::vector<const SurroundingVehicleInterface*>(const std::vector<const SurroundingVehicleInterface*>&));
  MOCK_CONST_METHOD1(FilterSideSideVehicles, std::vector<const SurroundingVehicleInterface*>(const std::vector<const SurroundingVehicleInterface*>&));
  MOCK_CONST_METHOD1(FilterSwervingTarget, std::vector<const SurroundingVehicleInterface*>(const std::vector<const SurroundingVehicleInterface*>&));
  MOCK_CONST_METHOD1(FilterVehiclesWithUnassignedAoi, std::vector<const SurroundingVehicleInterface*>(const std::vector<const SurroundingVehicleInterface*>&));
};