/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/ExtrapolationInterface.h"
#include "module/driver/src/MentalModelInterface.h"
#include "module/driver/src/ScmDefinitions.h"

class FakeExtrapolation : public ExtrapolationInterface
{
public:
  MOCK_CONST_METHOD2(Extrapolate, void(ObjectInformationScmExtended* objectInformation, AreaOfInterest aoi));
};