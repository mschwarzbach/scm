/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/GazeControlInterface.h"

/***************************************************************************
 * Mock class for testing functions and classes that depend on GazeControl *
 ***************************************************************************/
class FakeGazeControl : public GazeControlInterface
{
public:
  MOCK_METHOD1(AdvanceGazeState, void(GazeOverwrite gazeOverwrite));
  MOCK_CONST_METHOD0(GetCurrentGazeState, GazeState());
  MOCK_CONST_METHOD0(IsNewInformationAcquisitionRequested, bool());
  MOCK_METHOD6(UpdateGazeRequests, void(LateralAction actionState, const std::map<AreaOfInterest, double>& topDownRequestMap, const std::vector<AdasHmiSignal>& opticAdasSignals, const AdasHmiSignal& combinedSignal, AuditoryPerceptionInterface& auditoryPerception, double distractionPercentage));
  MOCK_METHOD1(SetKnownObjectIds, void(const std::vector<int>& knownIds));
  MOCK_CONST_METHOD0(GetHafSignalProcessed, bool());
  MOCK_METHOD1(SetHafSignalProcessed, void(bool));
};