/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "SituationManagerInterface.h"

/*********************************************************************************
 * Mock class for testing functions and classes that depend on SituationManager.  *
 **********************************************************************************/

class FakeSituationManager : public SituationManagerInterface
{
public:
  MOCK_METHOD1(SetIgnoringOuterLaneOvertakingProhibition, void(IgnoringOuterLaneOvertakingProhibitionInterface* ignoringOuterLaneOvertakingProhibition));
  MOCK_METHOD1(GenerateIntensitiesAndSampleSituation, void(ZipMergingInterface& zipMerging));
  MOCK_METHOD0(GetCurrentSituationDuration, units::time::millisecond_t());
  MOCK_METHOD1(SetCurrentSituationDuration, void(units::time::millisecond_t durationCurrentSituation));
  MOCK_METHOD1(SetSituationLastTick, void(Situation situation));
};
