/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

/*******************************************************************************************************
 * Mock class for testing functions and classes that depend on IgnoringOuterLaneOvertakingProhibition. *
 *******************************************************************************************************/

class FakeIgnoringOuterLaneOvertakingProhibition : public IgnoringOuterLaneOvertakingProhibitionInterface
{
public:
  MOCK_METHOD2(Update, void(int egoFrontId, int sideFrontId));
  MOCK_METHOD0(Update, void());
  MOCK_CONST_METHOD1(IsFulfilled, bool(AreaOfInterest aoi));
};