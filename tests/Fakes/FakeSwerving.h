/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Swerving/SwervingInterface.h"

/*************************************************************************
 * Mock class for testing functions and classes that depend on swerving.  *
 ************************************************************************/

class FakeSwerving : public SwervingInterface
{
public:
  MOCK_CONST_METHOD3(IsSwervingSafe, bool(units::length::meter_t lateralEvadeDistance, Side side, bool isUrgentSwerving));
  MOCK_CONST_METHOD1(IsPlannedSwervingPossible, bool(AreaOfInterest aoi));
  MOCK_CONST_METHOD0(DeterminePossibilityOfEvadingBySwerving, bool());
  MOCK_CONST_METHOD0(HasSwervingTargetBeenPassed, bool());

  MOCK_CONST_METHOD4(DetermineLateralDistanceToEvade, units::length::meter_t(AreaOfInterest aoi, Side side, bool isUrgentSwerving, int sideAoiIndex));
  MOCK_CONST_METHOD2(DetermineLateralDistanceToEvadeComfort, units::length::meter_t(AreaOfInterest aoi, Side side));
  MOCK_CONST_METHOD2(GetSwervingTimes, std::unordered_map<LateralAction, units::time::second_t, LateralActionHash>(AreaOfInterest aoiCollision, bool isUrgentSwerving));
  MOCK_CONST_METHOD2(DetermineTimeToSwervePast, units::time::second_t(AreaOfInterest aoi, int sideAoiIndex));
  MOCK_METHOD1(SetSwervingParameters, void(AreaOfInterest aoiCollision));

  MOCK_METHOD3(DetermineSwervingIntensities, void(AreaOfInterest aoi, bool isUrgentSwerving, std::unordered_map<LateralAction, double, LateralActionHash>& actionPatternIntensities));

  MOCK_CONST_METHOD1(DetermineAoiForSwervingReturnParameters, AreaOfInterest(const TrajectoryCalculations::TrajectoryDimensions& dimensions));
  MOCK_CONST_METHOD1(NoVehicleInFrontSideLaneVisible, bool(const AreaOfInterest& aoi));
  MOCK_CONST_METHOD2(DetermineSwervingReturnDistance, units::length::meter_t(const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations));

  MOCK_METHOD3(UpdateAndCheckForEndOfSwerving, void(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, units::time::millisecond_t cycleTime));
  MOCK_CONST_METHOD0(GetSwervingState, SwervingState());
  MOCK_METHOD1(SetSwervingState, void(SwervingState state));
  MOCK_CONST_METHOD0(IsPastSwervingEndTime, bool());
  MOCK_METHOD1(SetSwervingEndTime, void(units::time::millisecond_t time));
  MOCK_CONST_METHOD0(GetSwervingEndTime, units::time::millisecond_t());
  MOCK_METHOD1(SetAgentIdOfSwervingTarget, void(int id));
  MOCK_METHOD0(ResetAgentIdOfSwervingTarget, void());
  MOCK_METHOD1(SetLaneIdAtStartOfSwervingManeuver, void(int id));
  MOCK_CONST_METHOD0(GetLaneIdAtStartOfSwervingManeuver, int());
  MOCK_METHOD1(SetHasSwervingStateSwitchedToReturning, void(bool switchedToReturning));
  MOCK_CONST_METHOD0(GetAgentIdOfSwervingTarget, int());
  MOCK_METHOD1(SetRemainInSwervingState, void(bool remain));
  MOCK_CONST_METHOD0(GetRemainInSwervingState, bool());
  MOCK_CONST_METHOD0(HasSwervingStateSwitchedToReturning, bool());
  MOCK_CONST_METHOD0(IsSwervingPlanned, bool());
  MOCK_CONST_METHOD0(GetAoiToSwerve, std::pair<AreaOfInterest, bool>());
  MOCK_METHOD1(SetAoiToSwerve, void(AreaOfInterest aoi));
  MOCK_METHOD0(CheckForPlannedEvading, void());
};
