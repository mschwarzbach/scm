/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/AlgorithmSceneryCarInterface.h"

/************************************************************************************
 * Mock class for testing functions and classes that depend on AlgorithmSceneryCar. *
 ************************************************************************************/

class FakeAlgorithmSceneryCar : public AlgorithmSceneryCarInterface
{
public:
  MOCK_METHOD3(ForcedLaneChange, void(ActionManagerInterface* actionManager, ActionImplementationInterface* actionImplementation, LaneChangeAction laneChangeAction));
  MOCK_METHOD0(ResetReactionBaseTime, bool());
};