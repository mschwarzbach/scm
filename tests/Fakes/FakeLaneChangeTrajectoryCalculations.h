/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock-function-mocker.h>

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

/*************************************************************************************************
 * Mock class for testing functions and classes that depend on LaneChangeTrajectoryCalculations. *
 *************************************************************************************************/

class FakeLaneChangeTrajectoryCalculations : public LaneChangeTrajectoryCalculationsInterface
{
public:
  MOCK_CONST_METHOD2(DetermineReferenceObjectForLaneChange, const SurroundingVehicleInterface*(Situation currentSituation, const TrajectoryPlanning::RelevantVehiclesForLaneChangeLength& relevantVehicles));
  MOCK_CONST_METHOD2(DetermineLaneChangeLength, units::length::meter_t(const TrajectoryPlanning::LaneChangeLengthParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory));
  MOCK_CONST_METHOD1(DetermineLaneChangeWidth, units::length::meter_t(const TrajectoryPlanning::LaneChangeWidthParameters& parameters));
  MOCK_CONST_METHOD2(CanFinishLaneChangeWithoutEnteringMinDistance, bool(const TrajectoryPlanning::SafetyParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory));
};