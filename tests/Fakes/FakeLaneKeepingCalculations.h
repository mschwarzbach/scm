/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/LaneKeepingCalculationsInterface.h"

class FakeLaneKeepingCalculations : public LaneKeepingCalculationsInterface
{
public:
  MOCK_CONST_METHOD0(ChangingRightAppropriate, bool());
  MOCK_CONST_METHOD0(ChangingLeftAppropriate, bool());
  MOCK_CONST_METHOD0(IsAtEndOfLateralMovement, bool());
  MOCK_CONST_METHOD0(IsObjectInTargetSide, bool());
  MOCK_CONST_METHOD0(EgoFasterThanDeltaSide, bool());
};
