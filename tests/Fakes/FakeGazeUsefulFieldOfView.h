/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/GazeUsefulFieldOfViewInterface.h"

/*************************************************************************************
 * Mock class for testing functions and classes that depend on GazeUsefulFieldOfView *
 *************************************************************************************/

class FakeGazeUsefulFieldOfView : public GazeUsefulFieldOfViewInterface
{
public:
  MOCK_CONST_METHOD2(GetUsefulFieldOfViewAois, std::vector<AreaOfInterest>(const SurroundingObjectsSCM& surroundingObjects, units::angle::radian_t gazeAngleHorizontal));
};