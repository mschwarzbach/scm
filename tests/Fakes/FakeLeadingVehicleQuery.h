/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "SurroundingVehicles/LeadingVehicleQueryInterface.h"
#include "module/driver/src/ScmDefinitions.h"

class FakeLeadingVehicleQuery : public LeadingVehicleQueryInterface
{
public:
  MOCK_CONST_METHOD1(GetMergeRegulateIfDominating, std::optional<AreaOfInterest>(const std::vector<const SurroundingVehicleInterface*>&));
  MOCK_CONST_METHOD0(GetEgoLaneChangeDirection, std::optional<SurroundingLane>());
  MOCK_CONST_METHOD1(DoesOvertakingProhibitionApply, bool(const SurroundingVehicleInterface&));
  MOCK_CONST_METHOD0(GetSwervingTargetId, std::optional<int>());
  MOCK_CONST_METHOD1(GetEqDistance, units::length::meter_t(const SurroundingVehicleInterface&));
  MOCK_CONST_METHOD1(GetMinDistance, units::length::meter_t(const SurroundingVehicleInterface&));
  MOCK_CONST_METHOD0(GetCausingVehicleIdSideSituation, std::optional<int>());
  MOCK_CONST_METHOD0(GetCausingVehicleIdFrontSituation, std::optional<int>());
  MOCK_CONST_METHOD0(BelowJamVelocity, bool());
  MOCK_CONST_METHOD0(IsSwervingPlanned, bool());
};