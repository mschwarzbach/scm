/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <Stochastics/StochasticsInterface.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "ScmDependencies.h"

class FakeScmDependencies : public ScmDependenciesInterface
{
public:
  MOCK_METHOD1(UpdateScmSensorData, void(const scm::signal::DriverInput& driverInput));
  MOCK_METHOD1(UpdateParametersScmSignal, void(const scm::signal::DriverInput& driverInput));
  MOCK_METHOD1(UpdateParametersVehicleSignal, void(const scm::signal::DriverInput& driverInput));

  MOCK_METHOD0(GetDriverParameters, DriverParameters());
  MOCK_METHOD0(GetTrafficRulesScm, TrafficRulesScm*());
  MOCK_METHOD0(GetVehicleModelParameters, scm::common::vehicle::properties::EntityProperties());
  MOCK_METHOD0(GetStochastics, StochasticsInterface*());
  MOCK_METHOD0(GetSpawnVelocity, units::velocity::meters_per_second_t());
  MOCK_METHOD0(GetSpawnTime, units::time::millisecond_t());
  MOCK_METHOD1(SetSpawnTime, void(units::time::millisecond_t time));
  MOCK_METHOD0(GetCycleTime, units::time::millisecond_t());
  MOCK_METHOD0(GetOwnVehicleInformationScm, OwnVehicleInformationSCM*());
  MOCK_METHOD0(GetSurroundingObjectsScm, SurroundingObjectsSCM*());
  MOCK_METHOD0(GetTrafficRuleInformationScm, TrafficRuleInformationSCM*());
  MOCK_METHOD0(GetGeometryInformationScm, GeometryInformationSCM*());
  MOCK_METHOD0(GetOwnVehicleRoutePose, OwnVehicleRoutePose*());
};