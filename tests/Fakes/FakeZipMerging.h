/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "Merging/ZipMergeParameters.h"
#include "Merging/ZipMergingInterface.h"

class FakeZipMerging : public ZipMergingInterface
{
public:
  MOCK_METHOD0(UpdateMesoscopicSituation, void());
  MOCK_CONST_METHOD1(EstimateMergePoint, units::length::meter_t(const ZipMergeParameters&));
  MOCK_CONST_METHOD1(ActivationDistanceReached, bool(const ZipMergeParameters&));
  MOCK_CONST_METHOD0(DetermineZipMergeParameters, ZipMergeParameters());
  MOCK_CONST_METHOD0(GetZipMergeParameters, ZipMergeParameters());
  MOCK_METHOD1(SetZipMergeParameters, void(const ZipMergeParameters& ZipMergeParameters));
  MOCK_METHOD0(Reset, void());
  MOCK_CONST_METHOD2(CalculateFollowingAcceleration, units::acceleration::meters_per_second_squared_t(units::acceleration::meters_per_second_squared_t maxAcceleration, units::acceleration::meters_per_second_squared_t maxDeceleration));
};
