/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/MicroscopicCharacteristicsInterface.h"
#include "module/driver/src/ScmDefinitions.h"

class FakeMicroscopicCharacteristics : public MicroscopicCharacteristicsInterface
{
public:
  MOCK_METHOD2(UpdateObjectInformation, ObjectInformationScmExtended*(AreaOfInterest aoi, int sideIndex));
  MOCK_METHOD1(UpdateSideObjectsInformation, std::vector<ObjectInformationScmExtended>*(AreaOfInterest aoi));
  MOCK_CONST_METHOD2(GetObjectInformation, ObjectInformationScmExtended*(AreaOfInterest aoi, int sideIndex));
  MOCK_CONST_METHOD1(GetObjectVector, std::vector<ObjectInformationScmExtended>*(AreaOfInterest aoi));
  MOCK_CONST_METHOD1(GetSideObjectsInformation, std::vector<ObjectInformationScmExtended>*(AreaOfInterest aoi));

  MOCK_METHOD0(UpdateOwnVehicleData, OwnVehicleInformationScmExtended*());

  MOCK_CONST_METHOD0(GetSideAoiAoiRegulateIndex, int());
  MOCK_CONST_METHOD0(GetOwnVehicleInformation, OwnVehicleInformationScmExtended*());
  MOCK_METHOD1(UpdateSideAoiMergeRegulateIndex, void(int));

  MOCK_CONST_METHOD0(GetSideAoiMergeRegulateIndex, int());
  MOCK_METHOD1(UpdateSideAoiAoiRegulateIndex, void(int));

  MOCK_CONST_METHOD0(GetSurroundingVehicleInformation, const SurroundingObjectsScmExtended*());
  MOCK_CONST_METHOD1(GetSideObjectVector, std::vector<ObjectInformationScmExtended>*(AreaOfInterest));

  MOCK_METHOD1(Initialize, void(units::velocity::meters_per_second_t));
};