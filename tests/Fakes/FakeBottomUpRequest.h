/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/BottomUpRequestInterface.h"

/*******************************************************************************
 * Mock class for testing functions and classes that depend on BottomUpRequest *
 *******************************************************************************/
class FakeBottomUpRequest : public BottomUpRequestInterface
{
public:
  MOCK_METHOD8(Update, void(const std::vector<int>& knownIds, const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM, const std::vector<AdasHmiSignal>& opticAdasSignals, AuditoryPerceptionInterface& auditoryPerception, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl));
  MOCK_CONST_METHOD0(GetBottomUpIntensities, std::map<GazeState, double>());
};