/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock-function-mocker.h>

#include "LaneChangeTrajectoryCalculations/LaneChangeLengthCalculationsInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

/*********************************************************************************************
 * Mock class for testing functions and classes that depend on LaneChangeLengthCalculations. *
 *********************************************************************************************/

class FakeLaneChangeLengthCalculations : public LaneChangeLengthCalculationsInterface
{
public:
  MOCK_CONST_METHOD3(DetermineLaneChangeLengthFromObstacle, units::length::meter_t(const SurroundingVehicleInterface*, units::velocity::meters_per_second_t, const SurroundingVehicleQueryFactoryInterface&));
  MOCK_CONST_METHOD2(DetermineFreeLaneChangeLength, units::length::meter_t(units::time::second_t, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD2(DetermineLaneChangeLengthFromHighwayExit, units::length::meter_t(const TrajectoryPlanning::HighwayExitInformation&, units::velocity::meters_per_second_t));
};
