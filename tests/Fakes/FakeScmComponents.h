/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "MentalModelInterface.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/ScmComponents.h"
#include "module/driver/src/ScmDefinitions.h"

/*************************************************************************************
 * Mock class for testing functions and classes that depend on ActionImplementation. *
 *************************************************************************************/

class FakeScmComponents : public ScmComponentsInterface
{
public:
  MOCK_METHOD0(GetMentalModel, MentalModelInterface*());
  MOCK_METHOD0(GetGazeControl, GazeControlInterface*());
  MOCK_METHOD0(GetGazeFollower, GazeFollowerInterface*());
  MOCK_METHOD0(GetSituationManager, SituationManagerInterface*());
  MOCK_METHOD0(GetActionManager, ActionManagerInterface*());
  MOCK_METHOD0(GetActionImplementation, ActionImplementationInterface*());
  MOCK_METHOD0(GetAlgorithmSceneryCar, AlgorithmSceneryCarInterface*());
  MOCK_METHOD0(GetAuditoryPerception, AuditoryPerceptionInterface*());
  MOCK_METHOD0(GetInformationAcquisition, InformationAcquisitionInterface*());
  MOCK_METHOD0(GetFeatureExtractor, FeatureExtractorInterface*());
  MOCK_METHOD0(GetMentalCalculations, MentalCalculationsInterface*());
  MOCK_METHOD0(GetScmDependencies, ScmDependenciesInterface*());
  MOCK_METHOD0(GetIgnoringOuterLaneOvertakingProhibition, IgnoringOuterLaneOvertakingProhibitionInterface*());
  MOCK_METHOD0(GetSwerving, SwervingInterface*());
  MOCK_METHOD0(GetLongitudinalCalculations, LongitudinalCalculationsInterface*());
  MOCK_METHOD0(GetGazeControlComponents, GazeControlComponentsInterface*());
  MOCK_METHOD0(GetZipMerging, ZipMergingInterface*());
};