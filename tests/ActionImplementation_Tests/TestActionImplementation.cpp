/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "TestActionImplementation.h"

/**********************************************************/
// Define fake classes necessary for testing
/**********************************************************/

TestActionImplementation::TestActionImplementation(StochasticsInterface* testStochasticsInterface, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner)
    : ActionImplementation(testStochasticsInterface, mentalModel, featureExtractor, mentalCalculations, swerving, trajectoryPlanner, _zipMerging) {}

void TestActionImplementation::SwitchLongitudinalActionState()
{
  ActionImplementation::SwitchLongitudinalActionState();
}

void TestActionImplementation::SetLateralOutputKeys()
{
  ActionImplementation::SetLateralOutputKeys();
}

void TestActionImplementation::SetLateralActionLastTick(LateralAction state)
{
  _lateralActionLastTick = state;
}

void TestActionImplementation::SetAccelerationWishLongitudinal(units::acceleration::meters_per_second_squared_t acceleration)
{
  _accelerationWishLongitudinal = acceleration;
}

void TestActionImplementation::SetNextIndicatorState(scm::LightState::Indicator nextindicatorstate)
{
  _indicatorStateNext = nextindicatorstate;
}

bool TestActionImplementation::DoesMaxProbabilityApply() const
{
  return ActionImplementation::DoesMaxProbabilityApply();
}

units::acceleration::meters_per_second_squared_t TestActionImplementation::TestAdjustAccelerationWishDueToCooperation(units::acceleration::meters_per_second_squared_t accelerationWish) const
{
  return ActionImplementation::AdjustAccelerationWishDueToCooperation(accelerationWish);
}

std::tuple<units::time::millisecond_t, units::time::millisecond_t, double, double> TestActionImplementation::TestAdjustIndicatorParametersDueToCooperation(units::time::millisecond_t& startTime, units::time::millisecond_t& endTime, double& minProb, double& maxProb)
{
  return ActionImplementation::AdjustIndicatorParametersDueToCooperation(startTime, endTime, minProb, maxProb);
}

TestActionImplementation_2::TestActionImplementation_2(StochasticsInterface* testStochasticsInterface,
                                                       MentalModelInterface& mentalModel,
                                                       const FeatureExtractorInterface& featureExtractor,
                                                       const MentalCalculationsInterface& mentalCalculations,
                                                       SwervingInterface& swerving,
                                                       const TrajectoryPlannerInterface& trajectoryPlanner)
    : ActionImplementation(testStochasticsInterface, mentalModel, featureExtractor, mentalCalculations, swerving, trajectoryPlanner, _zipMerging) {}

scm::LightState::Indicator TestActionImplementation_2::GetIndicatorStateNext()
{
  return _indicatorStateNext;
}

void TestActionImplementation_2::ImplementStochasticIndicatorActivation(scm::LightState::Indicator indicatorState, double probIndicatorActivation)
{
  ActionImplementation::ImplementStochasticIndicatorActivation(indicatorState, probIndicatorActivation);
}

TestActionImplementation_3::TestActionImplementation_3(StochasticsInterface* testStochasticsInterface,
                                                       MentalModelInterface& mentalModel,
                                                       const FeatureExtractorInterface& featureExtractor,
                                                       const MentalCalculationsInterface& mentalCalculations,
                                                       SwervingInterface& swerving,
                                                       const TrajectoryPlannerInterface& trajectoryPlanner)
    : ActionImplementation(testStochasticsInterface, mentalModel, featureExtractor, mentalCalculations, swerving, trajectoryPlanner, _zipMerging) {}

void TestActionImplementation_3::TestDetermineLateralDisplacement()
{
  return ActionImplementation::DetermineLateralDisplacement();
}

TestActionImplementation_4::TestActionImplementation_4(StochasticsInterface* testStochasticsInterface,
                                                       MentalModelInterface& mentalModel,
                                                       const FeatureExtractorInterface& featureExtractor,
                                                       const MentalCalculationsInterface& mentalCalculations,
                                                       SwervingInterface& swerving,
                                                       const TrajectoryPlannerInterface& trajectoryPlanner)
    : ActionImplementation(testStochasticsInterface, mentalModel, featureExtractor, mentalCalculations, swerving, trajectoryPlanner, _zipMerging) {}

void TestActionImplementation_4::TestSetLateralOutputKeys()
{
  return ActionImplementation::SetLateralOutputKeys();
}
