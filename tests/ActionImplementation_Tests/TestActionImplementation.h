/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <Stochastics/StochasticsInterface.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeZipMerging.h"
#include "ScmDriver.h"

using ::testing::NiceMock;

class TestActionImplementation : public ActionImplementation
{
public:
  //! \brief Constructor.
  //! \param [in] testStochasticsInterface Provides access to the stochastics functionality of the framework.
  //! \param [in] testMentalModel         The mental processes of the stochastic cognitive model.
  //! \param [in] cycleTime
  TestActionImplementation(StochasticsInterface* testStochasticsInterface, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner);
  TestActionImplementation(const TestActionImplementation&) = delete;
  TestActionImplementation(TestActionImplementation&&) = delete;
  TestActionImplementation& operator=(const TestActionImplementation&) = delete;
  TestActionImplementation& operator=(TestActionImplementation&&) = delete;
  virtual ~TestActionImplementation() = default;

  void SwitchLongitudinalActionState();
  void SetLateralOutputKeys();

  void SetLateralActionLastTick(LateralAction state);
  void SetAccelerationWishLongitudinal(units::acceleration::meters_per_second_squared_t acceleration);
  void SetNextIndicatorState(scm::LightState::Indicator nextindicatorstate);

  bool DoesMaxProbabilityApply() const;
  units::acceleration::meters_per_second_squared_t TestAdjustAccelerationWishDueToCooperation(units::acceleration::meters_per_second_squared_t accelerationWish) const;
  std::tuple<units::time::millisecond_t, units::time::millisecond_t, double, double> TestAdjustIndicatorParametersDueToCooperation(units::time::millisecond_t& startTime, units::time::millisecond_t& endTime, double& minProb, double& maxProb);
  NiceMock<FakeZipMerging> _zipMerging;
};

class TestActionImplementation_2 : public ActionImplementation
{
public:
  //! \brief Constructor.
  //! \param [in] testStochasticsInterface Provides access to the stochastics functionality of the framework.
  //! \param [in] testMentalModel         The mental processes of the stochastic cognitive model.
  //! \param [in] cycleTime
  TestActionImplementation_2(StochasticsInterface* testStochasticsInterface, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner);

  TestActionImplementation_2(const TestActionImplementation_2&) = delete;
  TestActionImplementation_2(TestActionImplementation_2&&) = delete;
  TestActionImplementation_2& operator=(const TestActionImplementation_2&) = delete;
  TestActionImplementation_2& operator=(TestActionImplementation_2&&) = delete;
  virtual ~TestActionImplementation_2() = default;

  void SetLaneChangeWidth(double laneChangeWidth);
  void SetAbsoluteVelocityAct(double velocity);
  scm::LightState::Indicator GetIndicatorStateNext();
  void ImplementStochasticIndicatorActivation(scm::LightState::Indicator indicatorState, double probIndicatorActivation);

  MOCK_CONST_METHOD0(DoesMaxProbabilityApply, bool());
  MOCK_METHOD3(absoluteVelocityAct, double(double longitudinalVelocityAct, double laneChangeWidth, double lateralAcceleration));
  NiceMock<FakeZipMerging> _zipMerging;
};

class TestActionImplementation_3 : public ActionImplementation
{
public:
  TestActionImplementation_3(StochasticsInterface* testStochasticsInterface, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner);
  TestActionImplementation_3(const TestActionImplementation&) = delete;
  TestActionImplementation_3(TestActionImplementation&&) = delete;
  TestActionImplementation_3& operator=(const TestActionImplementation&) = delete;
  TestActionImplementation_3& operator=(TestActionImplementation&&) = delete;
  virtual ~TestActionImplementation_3() = default;

  void TestDetermineLateralDisplacement();

  MOCK_CONST_METHOD0(DetermineReferenceObjectForLaneChange, AreaOfInterest());
  NiceMock<FakeZipMerging> _zipMerging;
};

class TestActionImplementation_4 : public ActionImplementation
{
public:
  TestActionImplementation_4(StochasticsInterface* testStochasticsInterface, MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, SwervingInterface& swerving, const TrajectoryPlannerInterface& trajectoryPlanner);

  void TestSetLateralOutputKeys();
  MOCK_METHOD0(DetermineLateralDisplacement, void());
  MOCK_CONST_METHOD0(IsStartOfLaneChange, bool());
  MOCK_CONST_METHOD0(IsStartOfSwervingOrReturningToLaneCenter, bool());
  MOCK_METHOD0(DetermineManeuverValues, void());
  MOCK_METHOD0(DetermineOutputValues, void());
  NiceMock<FakeZipMerging> _zipMerging;
};
