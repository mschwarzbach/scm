/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utility>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSwerving.h"
#include "../Fakes/FakeTrajectoryPlanner.h"
#include "../Fakes/FakeZipMerging.h"
#include "ActionImplementation.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/AreaOfInterest.h"
#include "module/driver/src/InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/LateralAction.h"
#include "module/driver/src/MentalModelInterface.h"
#include "module/driver/src/MicroscopicCharacteristicsInterface.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct ActionImplementionTester
{
  class ActionImplementationUnderTest : ActionImplementation
  {
  public:
    template <typename... Args>
    ActionImplementationUnderTest(Args&&... args)
        : ActionImplementation{std::forward<Args>(args)...} {};

    using ActionImplementation::_currentTrajectoryState;
    using ActionImplementation::_isStartOfRealign;
    using ActionImplementation::_lateralDisplacementReached;
    using ActionImplementation::_neutralPosition;
    using ActionImplementation::_neutralPositionLast;
    using ActionImplementation::AccelerationWishWhileMerging;
    using ActionImplementation::AdjustAccelerationWishDueToCooperation;
    using ActionImplementation::AdjustIndicatorParametersDueToCooperation;
    using ActionImplementation::CheckForChangeInOffset;
    using ActionImplementation::DetermineLateralDisplacement;
    using ActionImplementation::DetermineManeuverValues;
    using ActionImplementation::DetermineOutputValues;
    using ActionImplementation::DoesMaxProbabilityApply;
    using ActionImplementation::GetAccelerationForSpeedAdjustment;
    using ActionImplementation::GetDesiredLateralDisplacement;
    using ActionImplementation::GetLongitudinalAccelerationWish;
    using ActionImplementation::GetNextIndicatorState;
    using ActionImplementation::ImplementStochasticIndicatorActivation;
    using ActionImplementation::LimitAccelerationDueToPlannedSwerving;
    using ActionImplementation::LimitAccelerationDueToSuspiciousVehicle;
    using ActionImplementation::SetFlasher;
    using ActionImplementation::SetIndicator;
    using ActionImplementation::SetLateralOutputKeys;
    using ActionImplementation::SetSwervingValues;
    using ActionImplementation::SwitchLongitudinalActionState;
    using ActionImplementation::UpdateInformationFromActionManager;

    void SET_ACCELERATION_WISH_LONGITUDINAL(units::acceleration::meters_per_second_squared_t accelerationWishLongitudinal)
    {
      _accelerationWishLongitudinal = accelerationWishLongitudinal;
    }

    void SET_LATERAL_ACTION_LAST_TICK(LateralAction lateralAction)
    {
      _lateralActionLastTick = lateralAction;
    }

    void SET_INDICATOR_STATE_NEXT(scm::LightState::Indicator indicatorStateNext)
    {
      _indicatorStateNext = indicatorStateNext;
    }
  };

  template <typename T>
  T& WITH_CYCLETIME(T& fakeMentalModel, units::time::millisecond_t cycleTime)
  {
    ON_CALL(fakeMentalModel, GetCycleTime()).WillByDefault(Return(cycleTime));
    return fakeMentalModel;
  }

  ActionImplementionTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeSwerving{},
        actionImplementation(&fakeStochastics,
                             WITH_CYCLETIME(fakeMentalModel, 100_ms),
                             fakeFeatureExtractor,
                             fakeMentalCalculations,
                             fakeSwerving,
                             fakeTrajectoryPlanner,
                             fakeZipMerging)
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeTrajectoryPlanner> fakeTrajectoryPlanner;
  NiceMock<FakeZipMerging> fakeZipMerging;

  ActionImplementationUnderTest actionImplementation;
};

/***********************************************
 * CHECK SetSwervingValues - SwervingInEvading *
 ***********************************************/

TEST(ActionImplementation, SetSwervingValues_SwervingInEvading)
{
  ActionImplementionTester TEST_HELPER;
  LateralAction lateralAction;
  lateralAction.state = LateralAction::State::COMFORT_SWERVING;

  const LateralActionQuery actionQuery{lateralAction};
  const bool isSwerving = actionQuery.IsSwerving();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(lateralAction));
  ON_CALL(TEST_HELPER.fakeSwerving, GetSwervingState()).WillByDefault(Return(SwervingState::NONE));
  EXPECT_CALL(TEST_HELPER.fakeSwerving, SetSwervingState(SwervingState::EVADING)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetObjectToEvade()).Times(1).WillRepeatedly(Return(AreaOfInterest::EGO_FRONT));
  EXPECT_CALL(TEST_HELPER.fakeSwerving, DetermineTimeToSwervePast(AreaOfInterest::EGO_FRONT, -1)).Times(1).WillRepeatedly(Return(5000_ms));
  EXPECT_CALL(TEST_HELPER.fakeSwerving, SetSwervingEndTime(6000_ms)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, HasSwervingStateSwitchedToReturning()).WillByDefault(Return(true));  // Set true to avoid if statement before tested logic (even though value does not make sense)
  TrajectoryCalculations::TrajectoryDimensions dimensions{};
  TEST_HELPER.actionImplementation.SetSwervingValues(isSwerving, dimensions);
}

/*****************************************************
 * CHECK SetSwervingValues - PlannedSwervingCanceled *
 *****************************************************/

TEST(ActionImplementation, SetSwervingValues_PlannedSwervingAborted)
{
  ActionImplementionTester TEST_HELPER;
  LateralAction lateralAction;
  lateralAction.state = LateralAction::State::LANE_KEEPING;

  const LateralActionQuery actionQuery{lateralAction};
  const bool isSwerving = actionQuery.IsSwerving();

  EXPECT_CALL(TEST_HELPER.fakeSwerving, IsSwervingPlanned()).Times(1).WillRepeatedly(Return(true));
  EXPECT_CALL(TEST_HELPER.fakeSwerving, SetAoiToSwerve(AreaOfInterest::NumberOfAreaOfInterests)).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, HasSwervingStateSwitchedToReturning()).WillByDefault(Return(true));  // Set true to avoid if statement before tested logic (even though value does not make sense)

  TrajectoryCalculations::TrajectoryDimensions lcd;
  TEST_HELPER.actionImplementation.SetSwervingValues(isSwerving, lcd);
}

/*************************************
 * CHECK DoesMaxProbabilityApply *
 ************************************/

// \brief Data table for definition of individual test cases
struct DataFor_DoesMaxProbabilityApply
{
  LateralAction lateralAction;
  MesoscopicSituation mesoscopicSituation;
  bool expected_Result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DoesMaxProbabilityApply& obj)
  {
    return os
           << "  lateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralAction)
           << "| expected_Result (bool)" << ScmCommons::BooleanToString(obj.expected_Result);
  }
};

class ActionImplementation_DoesMaxProbabilityApply : public ::testing::Test,
                                                     public ::testing::WithParamInterface<DataFor_DoesMaxProbabilityApply>
{
};

TEST_P(ActionImplementation_DoesMaxProbabilityApply, ActionImplementation_CheckDoesMaxProbabilityApply)
{
  ActionImplementionTester TEST_HELPER;
  DataFor_DoesMaxProbabilityApply data = GetParam();
  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(data.mesoscopicSituation)).WillByDefault(Return(true));

  // Run Test
  bool result_bool = TEST_HELPER.actionImplementation.DoesMaxProbabilityApply();
  ASSERT_EQ(data.expected_Result, result_bool);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_DoesMaxProbabilityApply,
    testing::Values(
        DataFor_DoesMaxProbabilityApply{
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .mesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .expected_Result = false},
        DataFor_DoesMaxProbabilityApply{
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .mesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .expected_Result = false},
        DataFor_DoesMaxProbabilityApply{
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .expected_Result = true},
        DataFor_DoesMaxProbabilityApply{
            .lateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::LEFT),
            .mesoscopicSituation = MesoscopicSituation::QUEUED_TRAFFIC,
            .expected_Result = false},
        DataFor_DoesMaxProbabilityApply{
            .lateralAction = LateralAction(LateralAction::State::PREPARING_TO_MERGE, LateralAction::Direction::RIGHT),
            .mesoscopicSituation = MesoscopicSituation::FREE_DRIVING,
            .expected_Result = false}));

/************************************************
 * CHECK ImplementStochasticIndicatorActivation *
 ***********************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_ImplementStochasticIndicatorActivation
{
  scm::LightState::Indicator indicatorState;
  scm::LightState::Indicator indicatorStateNext;
  units::time::millisecond_t durationSinceInitiation;
  double probIndicatorActivation;
  bool maxProbApplies;
  bool input_activate;
};

class ActionImplementation_ImplementStochasticIndicatorActivation : public ::testing::Test,
                                                                    public ::testing::WithParamInterface<DataFor_ImplementStochasticIndicatorActivation>
{
};

TEST_P(ActionImplementation_ImplementStochasticIndicatorActivation, ActionImplementation_CheckImplementStochasticIndicatorActivation)
{
  ActionImplementionTester TEST_HELPER;
  DataFor_ImplementStochasticIndicatorActivation data = GetParam();
  // Set up test
  double maxProb = 90.;
  bool hasStateChanged = false;
  bool activate = false;

  TEST_HELPER.actionImplementation.DoesMaxProbabilityApply();

  if (!data.maxProbApplies)
  {
    maxProb = 100.;
  }

  if (data.indicatorState != data.indicatorStateNext)
  {
    hasStateChanged = true;
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, ImplementStochasticActivation(0._ms, 2000._ms, data.durationSinceInitiation, hasStateChanged, 50., maxProb, 15., activate)).WillByDefault(Return(true));

  // Run Test
  TEST_HELPER.actionImplementation.ImplementStochasticIndicatorActivation(data.indicatorState, data.probIndicatorActivation);

  // Evaluate results
  auto result_indicatorStateNext = TEST_HELPER.actionImplementation.GetNextIndicatorState();
  if (!hasStateChanged || data.probIndicatorActivation > maxProb)
  {
    ASSERT_EQ(data.input_activate, activate);
  }
  if (activate)
  {
    ASSERT_EQ(data.indicatorState, result_indicatorStateNext);
    ASSERT_EQ(data.input_activate, activate);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_ImplementStochasticIndicatorActivation,
    testing::Values(
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Off,
            .indicatorStateNext = scm::LightState::Indicator::Left,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 50.0,
            .maxProbApplies = true,
            .input_activate = true},
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Left,
            .indicatorStateNext = scm::LightState::Indicator::Left,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 50.0,
            .maxProbApplies = true,
            .input_activate = false},
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Off,
            .indicatorStateNext = scm::LightState::Indicator::Right,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 100.0,
            .maxProbApplies = true,
            .input_activate = false},
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Right,
            .indicatorStateNext = scm::LightState::Indicator::Left,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 100.0,
            .maxProbApplies = false,
            .input_activate = true},
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Left,
            .indicatorStateNext = scm::LightState::Indicator::Warn,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 50.0,
            .maxProbApplies = false,
            .input_activate = true},
        DataFor_ImplementStochasticIndicatorActivation{
            .indicatorState = scm::LightState::Indicator::Off,
            .indicatorStateNext = scm::LightState::Indicator::Warn,
            .durationSinceInitiation = 1600.0_ms,
            .probIndicatorActivation = 50.0,
            .maxProbApplies = true,
            .input_activate = true}));

/******************************************
 * CHECK SetLateralOutputKeys Modify-Flag *
 ******************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_SetLateralOutputKeys
{
  LateralAction currentLateralAction;
  units::length::meter_t lateralNeutralPositionScaled;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_SetLateralOutputKeys& obj)
  {
    return os
           << "  currentLateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.currentLateralAction)
           << "| lateralNeutralPositionScaled (double): " << obj.lateralNeutralPositionScaled;
  }
};

class ActionImplementation_SetLateralOutputKeys : public ::testing::Test,
                                                  public ::testing::WithParamInterface<DataFor_SetLateralOutputKeys>
{
};

TEST_P(ActionImplementation_SetLateralOutputKeys, ActionImplementation_SetLateralOutputKeys)
{
  ActionImplementionTester TEST_HELPER;
  DataFor_SetLateralOutputKeys data = GetParam();

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  OwnVehicleInformationScmExtended mockInfo{};

  mockInfo.modifyLaneChange = true;
  mockInfo.lateralPosition = 0._m;
  mockInfo.absoluteVelocity = 30._mps;
  mockInfo.lateralVelocity = 0._mps;
  ON_CALL(fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&mockInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.properties.emplace(std::pair<std::string, std::string>("SteeringRatio", "5"));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleProperties));

  GeometryInformationSCM fakeGeometryInfo{};
  fakeGeometryInfo.Close().distanceToEndOfLane = ScmDefinitions::INF_DISTANCE;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  DriverParameters fakeDriverParameters{};
  LaneInformationGeometrySCM laneInfo{};
  laneInfo.width = 3_m;

  ON_CALL(fakeInfrastructureCharacteristsics, GetGeometryInformation()).WillByDefault(Return(&fakeGeometryInfo));
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(fakeDriverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.currentLateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInRealignState()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillByDefault(Return(data.lateralNeutralPositionScaled));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInLaneChangeState()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetEgoLaneWidth()).WillByDefault(Return(3.75_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralPosition()).WillByDefault(Return(mockInfo.lateralPosition));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetHeading()).WillByDefault(Return(0._rad));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurvatureOnCurrentPosition()).WillByDefault(Return(0.0_i_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(mockInfo.absoluteVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::FREE_DRIVING));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(mockInfo.lateralVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocityFrontAxle()).WillByDefault(Return(mockInfo.lateralVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetZipMerge()).WillByDefault(Return(false));

  std::optional<TrajectoryPlanning::TrajectoryDimensions> trajectoryDimensions;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetPlannedLaneChangeDimensions).WillByDefault(ReturnRef(trajectoryDimensions));

  TEST_HELPER.actionImplementation.SetLateralOutputKeys();

  ASSERT_FALSE(mockInfo.modifyLaneChange);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_SetLateralOutputKeys,
    testing::Values(
        DataFor_SetLateralOutputKeys{
            .currentLateralAction = LateralAction{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT},
            .lateralNeutralPositionScaled = 0._m},  // Reset due to modify lane change
        DataFor_SetLateralOutputKeys{
            .currentLateralAction = LateralAction{LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE},
            .lateralNeutralPositionScaled = .45_m},  // Reset due to modify into lane keeping with small offset
        DataFor_SetLateralOutputKeys{
            .currentLateralAction = LateralAction{LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE},
            .lateralNeutralPositionScaled = -.45_m}));  // Reset due to modify into lane keeping with small negative offset

/**************************************
 * CHECK DetermineLateralDisplacement *
 *************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_DetermineLateralDisplacement
{
  LateralAction lateralAction;
  AreaOfInterest causingVehicle;
  units::length::meter_t lateralDistanceToEvade;
  bool isModifiedLaneChange;
  bool isStartOfSwervingOrReturningToLaneCenter;
  bool input_RemainInLaneChangeState;
  bool input_RemainInSwervingState;
  units::length::meter_t input_LateralNeutralPositionScaled;
  bool input_isStartOfRealign;
  units::length::meter_t result_desiredLateralDisplacement;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineLateralDisplacement& obj)
  {
    return os
           << "  LateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateralAction)
           << "| causingVehicle (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.causingVehicle)
           << "| lateralDistanceToEvade (double): " << obj.lateralDistanceToEvade
           << "| isModifiedLaneChange (bool): " << ScmCommons::BooleanToString(obj.isModifiedLaneChange)
           << "| isStartOfSwervingOrReturningToLaneCenter (bool): " << ScmCommons::BooleanToString(obj.isStartOfSwervingOrReturningToLaneCenter)
           << "| input_RemainInLaneChangeState (bool): " << ScmCommons::BooleanToString(obj.input_RemainInLaneChangeState)
           << "| input_RemainInSwervingState (bool): " << ScmCommons::BooleanToString(obj.input_RemainInSwervingState)
           << "| input_LateralNeutralPositionScaled (double): " << obj.input_LateralNeutralPositionScaled
           << "| input_isStartOfRealign (bool): " << ScmCommons::BooleanToString(obj.input_isStartOfRealign)
           << "| result_desiredLateralDisplacement (double): " << obj.result_desiredLateralDisplacement;
  }
};

class ActionImplementation_DetermineLateralDisplacement : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataFor_DetermineLateralDisplacement>
{
};

TEST_P(ActionImplementation_DetermineLateralDisplacement, ActionImplementation_DetermineLateralDisplacement)
{
  ActionImplementionTester TEST_HELPER;
  DataFor_DetermineLateralDisplacement data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsFrontSituation()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObjectToEvade()).WillByDefault(Return(data.causingVehicle));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillByDefault(Return(data.input_LateralNeutralPositionScaled));
  ON_CALL(TEST_HELPER.fakeSwerving, DetermineLateralDistanceToEvade(_, _, _, _)).WillByDefault(Return(data.lateralDistanceToEvade));

  MicroscopicCharacteristics microscopicCharacteristics;
  microscopicCharacteristics.UpdateOwnVehicleData()->modifyLaneChange = data.isModifiedLaneChange;
  microscopicCharacteristics.UpdateOwnVehicleData()->lateralPosition = 0.5_m;

  TEST_HELPER.actionImplementation._currentTrajectoryState.desiredDisplacement = 3.75_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&microscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetEgoLaneWidth()).WillByDefault(Return(3.75_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInLaneChangeState()).WillByDefault(Return(data.input_RemainInLaneChangeState));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRemainInSwervingState()).WillByDefault(Return(data.input_RemainInSwervingState));

  TEST_HELPER.actionImplementation._neutralPosition = data.input_LateralNeutralPositionScaled;
  TEST_HELPER.actionImplementation._neutralPositionLast = .1_m;
  TEST_HELPER.actionImplementation._isStartOfRealign = data.input_isStartOfRealign;

  TEST_HELPER.actionImplementation.DetermineLateralDisplacement();
  auto result_desiredLateralDisplacement = TEST_HELPER.actionImplementation.GetDesiredLateralDisplacement();

  ASSERT_EQ(result_desiredLateralDisplacement, data.result_desiredLateralDisplacement);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_DetermineLateralDisplacement,
    testing::Values(
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 2_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 0.0_m},  // No lane change
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 0.0_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 3.75_m},  // Start of lane change
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 0.0_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 0._m},  // Start of lane change while active lane change (no modify, so continue previous maneuver)
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 0.0_m,
            .isModifiedLaneChange = true,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = -0.5_m},  // Abort active lane change into lane keeping
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 0.0_m,
            .isModifiedLaneChange = true,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = .25_m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 0._m},  // Abort active lane change into lane keeping (small offset)
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 2.0_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 2.0_m},  // Start of swerving maneuver
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 2.0_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = true,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 0.0_m},  // Swerving during active lateral maneuver will not update desiredLateralDisplacement (not a real scenario but good to make sure)
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 2.0_m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 0._m,
            .input_isStartOfRealign = false,
            .result_desiredLateralDisplacement = 2.0_m},  // Start of swerving maneuver
        DataFor_DetermineLateralDisplacement{
            .lateralAction = LateralAction(LateralAction::State::COMFORT_SWERVING, LateralAction::Direction::LEFT),
            .causingVehicle = AreaOfInterest::EGO_FRONT,
            .lateralDistanceToEvade = 0._m,
            .isModifiedLaneChange = false,
            .isStartOfSwervingOrReturningToLaneCenter = false,
            .input_RemainInLaneChangeState = false,
            .input_RemainInSwervingState = false,
            .input_LateralNeutralPositionScaled = 5._m,
            .input_isStartOfRealign = true,
            .result_desiredLateralDisplacement = 4.9_m}));  // Start of Realign

/************************************************
 * CHECK AdjustAccelerationWishDueToCooperation *
 ************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_AdjustAccelerationWishDueToCooperation
{
  units::acceleration::meters_per_second_squared_t accelerationWish;
  bool egoBelowJamSpeed;
  bool isCooperative;
  Situation currentSituation;
  AreaOfInterest aoiRegulate;
  AreaOfInterest causingVehicleAoi;
  units::acceleration::meters_per_second_squared_t maximumDeceleration;
  units::acceleration::meters_per_second_squared_t result_modifiedAccelerationWish;
};

class ActionImplementation_AdjustAccelerationWishDueToCooperation : public ::testing::Test,
                                                                    public ::testing::WithParamInterface<DataFor_AdjustAccelerationWishDueToCooperation>
{
};

TEST_P(ActionImplementation_AdjustAccelerationWishDueToCooperation, ActionImplementation_CheckAdjustAccelerationWishDueToCooperation)
{
  ActionImplementionTester TEST_HELPER;
  DataFor_AdjustAccelerationWishDueToCooperation data = GetParam();

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalDeceleration = data.maximumDeceleration;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(data.egoBelowJamSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(data.isCooperative));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.currentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationSideCluster()).WillByDefault(Return(AreaOfInterest::NumberOfAreaOfInterests));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCausingVehicleOfSituationSideCluster()).WillByDefault(Return(data.causingVehicleAoi));

  NiceMock<FakeSurroundingVehicle> leadingVehicle;
  ON_CALL(leadingVehicle, GetAssignedAoi).WillByDefault(Return(data.aoiRegulate));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicle()).WillByDefault(Return(&leadingVehicle));

  const auto result = TEST_HELPER.actionImplementation.AdjustAccelerationWishDueToCooperation(data.accelerationWish);
  ASSERT_EQ(result, data.result_modifiedAccelerationWish);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_AdjustAccelerationWishDueToCooperation,
    testing::Values(
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust acceleration wish */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -12._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Prerequisites for adjustment fulfilled -> adjust acceleration wish */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -12._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Prerequisites for adjustment fulfilled, acceleration wish adjusted higher than maxDeceleration -> return maximumDeceleration */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_RIGHT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 11._mps_sq,
            .result_modifiedAccelerationWish = -11._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Acceleration wish positive -> return same value */
            .accelerationWish = 10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = 10._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Agent not below jam speed -> return same value */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = false,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -10._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Agent not cooperative -> return same value */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = false,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -10._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Not a lane change situation -> return same value */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::FOLLOWING_DRIVING,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::EGO_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -10._mps_sq},
        DataFor_AdjustAccelerationWishDueToCooperation{
            /* Causing vehicle is not aoi regulate -> return same value */
            .accelerationWish = -10._mps_sq,
            .egoBelowJamSpeed = true,
            .isCooperative = true,
            .currentSituation = Situation::LANE_CHANGER_FROM_LEFT,
            .aoiRegulate = AreaOfInterest::EGO_FRONT,
            .causingVehicleAoi = AreaOfInterest::LEFT_FRONT,
            .maximumDeceleration = 100._mps_sq,
            .result_modifiedAccelerationWish = -10._mps_sq}));

/***************************************************
 * CHECK AdjustIndicatorParametersDueToCooperation *
 ***************************************************/

TEST(ActionImplementation_AdjustIndicatorParametersDueToCooperation, GivenEgoBelowJamSpeedAndIsCooperativeAndCooperationFactorNotZero_AdjustParameters)
{
  ActionImplementionTester TEST_HELPER;

  /* Test parameters */
  const double cooperationFactor{0.8};
  auto startTime{10._ms};
  auto endTime{11._ms};
  double minProb{12.};
  double maxProb{13.};
  const auto expected_startTimeAdjusted{startTime - startTime * cooperationFactor};
  const auto expected_endTimeAdjusted{endTime + endTime * cooperationFactor};
  const double expected_minProbAdjusted{minProb + (100. - minProb) * cooperationFactor};
  const double expected_maxProbAdjusted{maxProb + (100. - maxProb) * cooperationFactor};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor).WillByDefault(Return(cooperationFactor));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed).WillByDefault(Return(true));

  /* Call function */
  const auto [result_startTime, result_endTime, result_minProb, result_maxProb] = TEST_HELPER.actionImplementation.AdjustIndicatorParametersDueToCooperation(startTime, endTime, minProb, maxProb);

  /* Assert result */
  ASSERT_EQ(result_startTime, expected_startTimeAdjusted);
  ASSERT_EQ(result_endTime, expected_endTimeAdjusted);
  ASSERT_DOUBLE_EQ(result_minProb, expected_minProbAdjusted);
  ASSERT_DOUBLE_EQ(result_maxProb, expected_maxProbAdjusted);
}

TEST(ActionImplementation_AdjustIndicatorParametersDueToCooperation, GivenEgoBelowJamSpeedAndIsCooperativeAndCooperationFactorZero_DoNotAdjustParameters)
{
  ActionImplementionTester TEST_HELPER;
  /* Test parameters */
  const double cooperationFactor{0};
  auto startTime{10._ms};
  auto endTime{11._ms};
  double minProb{12.};
  double maxProb{13.};
  const auto expected_startTimeAdjusted{startTime};
  const auto expected_endTimeAdjusted{endTime};
  const double expected_minProbAdjusted{minProb};
  const double expected_maxProbAdjusted{maxProb};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor).WillByDefault(Return(cooperationFactor));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed).WillByDefault(Return(true));

  /* Call function */
  const auto [result_startTime, result_endTime, result_minProb, result_maxProb] = TEST_HELPER.actionImplementation.AdjustIndicatorParametersDueToCooperation(startTime, endTime, minProb, maxProb);

  /* Assert result */
  ASSERT_EQ(result_startTime, expected_startTimeAdjusted);
  ASSERT_EQ(result_endTime, expected_endTimeAdjusted);
  ASSERT_DOUBLE_EQ(result_minProb, expected_minProbAdjusted);
  ASSERT_DOUBLE_EQ(result_maxProb, expected_maxProbAdjusted);
}

TEST(ActionImplementation_AdjustIndicatorParametersDueToCooperation, GivenEgoBelowJamSpeedAndIsNotCooperativeAndCooperationFactorNotZero_DoNotAdjustParameters)
{
  ActionImplementionTester TEST_HELPER;

  /* Test parameters */
  const double cooperationFactor{0.8};
  auto startTime{10._ms};
  auto endTime{11._ms};
  double minProb{12.};
  double maxProb{13.};
  const auto expected_startTimeAdjusted{startTime};
  const auto expected_endTimeAdjusted{endTime};
  const double expected_minProbAdjusted{minProb};
  const double expected_maxProbAdjusted{maxProb};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor).WillByDefault(Return(cooperationFactor));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed).WillByDefault(Return(true));

  /* Call function */
  const auto [result_startTime, result_endTime, result_minProb, result_maxProb] = TEST_HELPER.actionImplementation.AdjustIndicatorParametersDueToCooperation(startTime, endTime, minProb, maxProb);

  /* Assert result */
  ASSERT_EQ(result_startTime, expected_startTimeAdjusted);
  ASSERT_EQ(result_endTime, expected_endTimeAdjusted);
  ASSERT_DOUBLE_EQ(result_minProb, expected_minProbAdjusted);
  ASSERT_DOUBLE_EQ(result_maxProb, expected_maxProbAdjusted);
}

TEST(ActionImplementation_AdjustIndicatorParametersDueToCooperation, GivenEgoNotBelowJamSpeedAndIsCooperativeAndCooperationFactorNotZero_DoNotAdjustParameters)
{
  ActionImplementionTester TEST_HELPER;

  /* Test parameters */
  const double cooperationFactor{0.8};
  auto startTime{10._ms};
  auto endTime{11._ms};
  double minProb{12.};
  double maxProb{13.};
  const auto expected_startTimeAdjusted{startTime};
  const auto expected_endTimeAdjusted{endTime};
  const double expected_minProbAdjusted{minProb};
  const double expected_maxProbAdjusted{maxProb};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor).WillByDefault(Return(cooperationFactor));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor).WillByDefault(Return(cooperationFactor));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed).WillByDefault(Return(false));

  /* Call function */
  const auto [result_startTime, result_endTime, result_minProb, result_maxProb] = TEST_HELPER.actionImplementation.AdjustIndicatorParametersDueToCooperation(startTime, endTime, minProb, maxProb);

  /* Assert result */
  ASSERT_EQ(result_startTime, expected_startTimeAdjusted);
  ASSERT_EQ(result_endTime, expected_endTimeAdjusted);
  ASSERT_DOUBLE_EQ(result_minProb, expected_minProbAdjusted);
  ASSERT_DOUBLE_EQ(result_maxProb, expected_maxProbAdjusted);
}

/********************************
 * CHECK CheckForChangeInOffset *
 ********************************/

TEST(ActionImplementation_CheckForChangeInOffset, CheckFunction_CheckForChangeInOffset)
{
  ActionImplementionTester TEST_HELPER;

  // Test 1: Change in offset below realign threshold
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRemainInRealignState()).WillOnce(Return(false));
  EXPECT_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillOnce(Return(.2_m));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillOnce(Return(LateralAction{LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE}));

  TEST_HELPER.actionImplementation.CheckForChangeInOffset();

  ASSERT_EQ(TEST_HELPER.actionImplementation._neutralPosition, .2_m);
  ASSERT_FALSE(TEST_HELPER.actionImplementation._isStartOfRealign);

  // Test 2: Change in offset above realign threshold
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRemainInRealignState()).WillOnce(Return(false));
  EXPECT_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillOnce(Return(-.4_m));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillOnce(Return(LateralAction{LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE}));

  TEST_HELPER.actionImplementation.CheckForChangeInOffset();

  ASSERT_EQ(TEST_HELPER.actionImplementation._neutralPosition, -.4_m);
  ASSERT_TRUE(TEST_HELPER.actionImplementation._isStartOfRealign);

  // Test 3: Realign true blocked by active lateral maneuver
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRemainInRealignState()).WillOnce(Return(false));
  EXPECT_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).WillOnce(Return(4.15_m));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillOnce(Return(LateralAction{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT}));

  TEST_HELPER.actionImplementation.CheckForChangeInOffset();

  ASSERT_EQ(TEST_HELPER.actionImplementation._neutralPosition, 4.15_m);
  ASSERT_FALSE(TEST_HELPER.actionImplementation._isStartOfRealign);

  // Test 4: Realign and neutralPosition update blocked due to active realign maneuver
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetRemainInRealignState()).WillOnce(Return(true));
  EXPECT_CALL(TEST_HELPER.fakeMentalCalculations, CalculateLateralOffsetNeutralPositionScaled()).Times(0);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).Times(0);

  TEST_HELPER.actionImplementation.CheckForChangeInOffset();

  ASSERT_EQ(TEST_HELPER.actionImplementation._neutralPosition, 4.15_m);
  ASSERT_FALSE(TEST_HELPER.actionImplementation._isStartOfRealign);
}

/***********************************************
 * CHECK LimitAccelerationDueToPlannedSwerving *
 ***********************************************/

struct DataFor_CheckIsAccelerationWishLongitudinalModified
{
  bool jamVelocityEgo;
  units::acceleration::meters_per_second_squared_t expected_result;
};

class ActionImplementation_LimitAccelerationDueToPlannedSwerving : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckIsAccelerationWishLongitudinalModified>
{
};

TEST_P(ActionImplementation_LimitAccelerationDueToPlannedSwerving, CheckIsAccelerationWishLongitudinalModified)
{
  DataFor_CheckIsAccelerationWishLongitudinalModified data = GetParam();
  ActionImplementionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(Situation::FOLLOWING_DRIVING));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasJamVelocityEgo()).WillByDefault(Return(data.jamVelocityEgo));
  ON_CALL(TEST_HELPER.fakeSwerving, IsSwervingPlanned()).WillByDefault(Return(true));

  TEST_HELPER.actionImplementation.SET_ACCELERATION_WISH_LONGITUDINAL(5.0_mps_sq);

  TEST_HELPER.actionImplementation.LimitAccelerationDueToPlannedSwerving();

  auto result = TEST_HELPER.actionImplementation.GetLongitudinalAccelerationWish();

  ASSERT_EQ(result, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_LimitAccelerationDueToPlannedSwerving,
    ::testing::Values(
        DataFor_CheckIsAccelerationWishLongitudinalModified{
            .jamVelocityEgo = false,
            .expected_result = 0.0_mps_sq},
        DataFor_CheckIsAccelerationWishLongitudinalModified{
            .jamVelocityEgo = true,
            .expected_result = 5.0_mps_sq}));

/**********************
 * CHECK SetIndicator *
 **********************/

struct DataFor_CheckIndicatorStateNext
{
  LateralAction lateralAction;
  LateralAction lateralActionLastTick;
  scm::LightState::Indicator expected_result;
};

class ActionImplementation_SetIndicator : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckIndicatorStateNext>
{
};

TEST_P(ActionImplementation_SetIndicator, CheckIndicatorStateNext)
{
  DataFor_CheckIndicatorStateNext data = GetParam();
  ActionImplementionTester TEST_HELPER;

  LateralAction lateralAction{data.lateralAction};

  const LateralActionQuery actionQuery{lateralAction};
  const bool isSwerving = actionQuery.IsSwerving();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(lateralAction));

  LateralAction lateralActionLastTick{data.lateralActionLastTick};
  TEST_HELPER.actionImplementation.SET_LATERAL_ACTION_LAST_TICK(lateralActionLastTick);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIndicatorActiviationProbability).WillByDefault(Return(5.0));

  StochasticActivationModelParameters parameters{};

  DriverParameters fakeDriverParameters{};
  fakeDriverParameters.nationalSpecifics.behaviouralPatterns.indicatorActivation = parameters;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(fakeDriverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC)).WillByDefault(Return(true));

  scm::LightState::Indicator indicatorStateNext = scm::LightState::Indicator::Off;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAgentCooperationFactor()).WillByDefault(Return(0.1));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, EgoBelowJamSpeed()).WillByDefault(Return(false));

  TEST_HELPER.actionImplementation.SET_INDICATOR_STATE_NEXT(indicatorStateNext);

  ON_CALL(TEST_HELPER.fakeMentalModel, ImplementStochasticActivation(_, _, _, _, _, _, _, _)).WillByDefault(Return(true));

  TEST_HELPER.actionImplementation.SetIndicator();
  auto indicatorState = TEST_HELPER.actionImplementation.GetNextIndicatorState();

  ASSERT_EQ(indicatorState, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_SetIndicator,
    ::testing::Values(
        DataFor_CheckIndicatorStateNext{
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .lateralActionLastTick = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::LEFT),
            .expected_result = scm::LightState::Indicator::Left},
        DataFor_CheckIndicatorStateNext{
            .lateralAction = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .lateralActionLastTick = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .expected_result = scm::LightState::Indicator::Right},
        DataFor_CheckIndicatorStateNext{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .lateralActionLastTick = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .expected_result = scm::LightState::Indicator::Off},
        DataFor_CheckIndicatorStateNext{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .lateralActionLastTick = LateralAction(LateralAction::State::INTENT_TO_CHANGE_LANES, LateralAction::Direction::RIGHT),
            .expected_result = scm::LightState::Indicator::Off}));

/********************
 * CHECK SetFlasher *
 ********************/
struct DataFor_CheckFlasherActiveNext
{
  Situation situation;
  Risk right;
  Risk left;
  int times;
};

class ActionImplementation_SetFlasher : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckFlasherActiveNext>
{
};

TEST_P(ActionImplementation_SetFlasher, CheckFlasherActiveNext)
{
  DataFor_CheckFlasherActiveNext data = GetParam();
  ActionImplementionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation).WillByDefault(Return(data.situation));

  auto durationCurrentSituation = 10.0_ms;
  double flasherActiviationProbability = 15.0;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDurationCurrentSituation).WillByDefault(Return(durationCurrentSituation));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetFlasherActiviationProbability).WillByDefault(Return(flasherActiviationProbability));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetSituationRisk(Situation::LANE_CHANGER_FROM_RIGHT)).WillByDefault(Return(data.right));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetSituationRisk(Situation::LANE_CHANGER_FROM_LEFT)).WillByDefault(Return(data.left));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ImplementStochasticActivation(_, _, durationCurrentSituation, true, flasherActiviationProbability, _, _, _)).Times(data.times);
  TEST_HELPER.actionImplementation.SetFlasher();
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_SetFlasher,
    ::testing::Values(
        DataFor_CheckFlasherActiveNext{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .right = Risk::HIGH,
            .left = Risk::LOW,
            .times = 1},
        DataFor_CheckFlasherActiveNext{
            .situation = Situation::LANE_CHANGER_FROM_LEFT,
            .right = Risk::LOW,
            .left = Risk::HIGH,
            .times = 1},
        DataFor_CheckFlasherActiveNext{
            .situation = Situation::LANE_CHANGER_FROM_LEFT,
            .right = Risk::LOW,
            .left = Risk::LOW,
            .times = 0},
        DataFor_CheckFlasherActiveNext{
            .situation = Situation::FREE_DRIVING,
            .right = Risk::LOW,
            .left = Risk::LOW,
            .times = 0}));

/***************************************
 * CHECK SwitchLongitudinalActionState *
 ***************************************/
struct DataFor_CheckLongitudinalAccelerationWish
{
  LongitudinalActionState longitudinalActionState;
  units::acceleration::meters_per_second_squared_t accelerationWishFromMerging;
  units::length::meter_t distanceToEndOfLane;
  bool mesoscopicSituationZipActive;
  double expected_result;
};

class ActionImplementation_SwitchLongitudinalActionState : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckLongitudinalAccelerationWish>
{
};

TEST_P(ActionImplementation_SwitchLongitudinalActionState, CheckLongitudinalAccelerationWish)
{
  DataFor_CheckLongitudinalAccelerationWish data = GetParam();
  ActionImplementionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10.0_mps));

  NiceMock<FakeSurroundingVehicle> leadingVehicle;
  ON_CALL(leadingVehicle, GetTtc()).WillByDefault(Return(5.0_s));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLeadingVehicle()).WillByDefault(Return(&leadingVehicle));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(1.0_mps_sq));

  DriverParameters driverParameters{};
  driverParameters.maximumLongitudinalDeceleration = 5.0_mps_sq;
  driverParameters.maximumLongitudinalAcceleration = 7.0_mps_sq;
  driverParameters.comfortLongitudinalAcceleration = 3.0_mps_sq;
  driverParameters.comfortLongitudinalDeceleration = 2.0_mps_sq;
  driverParameters.amountOfSpeedLimitViolation = 2.0_mps;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.properties.emplace(std::pair<std::string, std::string>("DecelerationFromPowertrainDrag", "5"));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleProperties));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalActionState()).WillByDefault(Return(data.longitudinalActionState));

  ON_CALL(TEST_HELPER.fakeZipMerging, CalculateFollowingAcceleration(_, _)).WillByDefault(Return(data.accelerationWishFromMerging));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(9.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(9.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalEgo()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(3.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsAbleToCooperateByAccelerating()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsOnEntryLane()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForAcceleration()).WillByDefault(Return(1.0));

  Merge::Gap mergeGap(nullptr, nullptr);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(0, true)).WillByDefault(Return(data.distanceToEndOfLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(4.0_mps_sq));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateFollowingAcceleration(_, _, _)).WillByDefault(Return(1_mps_sq));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsMergePreparationActive()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAccelerationLimitForMerging()).WillByDefault(Return(10.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::ACTIVE_ZIP_MERGING)).WillByDefault(Return(data.mesoscopicSituationZipActive));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::PASSIVE_ZIP_MERGING)).WillByDefault(Return(data.mesoscopicSituationZipActive));

  TEST_HELPER.actionImplementation.SwitchLongitudinalActionState();
  auto result = TEST_HELPER.actionImplementation.GetLongitudinalAccelerationWish();
  ASSERT_NEAR(result.value(), data.expected_result, 0.01);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_SwitchLongitudinalActionState,
    ::testing::Values(
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::STOPPED,
            .accelerationWishFromMerging = 0_mps_sq,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.0},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::ACTIVE_ZIP_MERGING,
            .accelerationWishFromMerging = 4.0_mps_sq,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 4.0},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::PASSIVE_ZIP_MERGING,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.5},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::PREPARING_TO_MERGE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.0},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::SPEED_ADJUSTMENT,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.5},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::BRAKING_TO_END_OF_LANE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 10_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = -4.04},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::BRAKING_TO_END_OF_LANE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 3_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = -5.0},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::APPROACHING,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 3_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.5},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::FOLLOWING_AT_DESIRED_DISTANCE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 3_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = 0.5},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 3_m,
            .mesoscopicSituationZipActive = true,
            .expected_result = -5.0},
        DataFor_CheckLongitudinalAccelerationWish{
            .longitudinalActionState = LongitudinalActionState::FALL_BACK_TO_DESIRED_DISTANCE,
            .accelerationWishFromMerging = ScmDefinitions::INF_ACCELELERATION,
            .distanceToEndOfLane = 3_m,
            .mesoscopicSituationZipActive = false,
            .expected_result = -5.0}));

/*****************************************************************************
 * CHECK AccelerationWishWhileMerging - CheckAccelerationWishWithoutMergeGap *
 *****************************************************************************/

TEST(ActionImplementation_AccelerationWishWhileMerging, CheckAccelerationWishWithoutMergeGap)
{
  ActionImplementionTester TEST_HELPER;

  Merge::Gap mergeGap(nullptr, nullptr);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));

  auto result = TEST_HELPER.actionImplementation.AccelerationWishWhileMerging();
  ASSERT_EQ(result, 0.0_mps_sq);
}

/**************************************************************
 * CHECK AccelerationWishWhileMerging - CheckAccelerationWish *
 **************************************************************/

struct DataFor_CheckAccelerationWish
{
  LateralAction lateralAction;
  MergeAction mergeAction;
  units::velocity::meters_per_second_t longitudinalVelocityEgo;
  AreaOfInterest aoi;
  units::acceleration::meters_per_second_squared_t expected_result;
};

class ActionImplementation_AccelerationWishWhileMerging : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckAccelerationWish>
{
};

TEST_P(ActionImplementation_AccelerationWishWhileMerging, CheckAccelerationWish)
{
  DataFor_CheckAccelerationWish data = GetParam();
  ActionImplementionTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(vehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(vehicle, GetAbsoluteVelocity()).WillByDefault(Return(20.0_mps));

  Merge::Gap mergeGap(&vehicle, nullptr);
  mergeGap.leaderId = 1;
  mergeGap.approachingVelocity = 12.0_mps;
  mergeGap.velocity = 15.0_mps;
  mergeGap.selectedMergePhases.push_back({data.mergeAction, 100.0_s, 0_m});

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMergeGap()).WillByDefault(Return(mergeGap));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(mergeGap.leaderId)).WillByDefault(Return(&vehicle));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralAction()).WillByDefault(Return(data.lateralAction));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(15.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalEgo()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.0_mps_sq));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsOnEntryLane()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForAcceleration()).WillByDefault(Return(1.0));

  DriverParameters driverParameters{};
  driverParameters.amountOfDesiredVelocityViolation = 1.0_mps;
  driverParameters.maximumLongitudinalDeceleration = 10.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsAbleToCooperateByAccelerating()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTime()).WillByDefault(Return(100_ms));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(data.longitudinalVelocityEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAccelerationLimitForMerging()).WillByDefault(Return(100_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDecelerationLimitForMerging()).WillByDefault(Return(100_mps_sq));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(data.aoi, _)).WillByDefault(Return(&vehicle));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateFollowingAcceleration(_, _, _)).WillByDefault(Return(10.0_mps_sq));

  auto result = TEST_HELPER.actionImplementation.AccelerationWishWhileMerging();
  ASSERT_EQ(result, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_AccelerationWishWhileMerging,
    ::testing::Values(
        DataFor_CheckAccelerationWish{
            .lateralAction = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .mergeAction = MergeAction::ACCELERATING,
            .longitudinalVelocityEgo = 5.0_mps,
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expected_result = 2_mps_sq},
        DataFor_CheckAccelerationWish{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .mergeAction = MergeAction::ACCELERATING,
            .longitudinalVelocityEgo = 10.0_mps,
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expected_result = 100_mps_sq},
        DataFor_CheckAccelerationWish{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .mergeAction = MergeAction::DECELERATING,
            .longitudinalVelocityEgo = 100.0_mps,
            .aoi = AreaOfInterest::NumberOfAreaOfInterests,
            .expected_result = -100_mps_sq},
        DataFor_CheckAccelerationWish{
            .lateralAction = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::NONE),
            .mergeAction = MergeAction::ACCELERATING,
            .longitudinalVelocityEgo = 10.0_mps,
            .aoi = AreaOfInterest::EGO_FRONT,
            .expected_result = 10_mps_sq}));

/*************************************************
 * CHECK LimitAccelerationDueToSuspiciousVehicle *
 *************************************************/

struct DataFor_CurrentAccelerationWish
{
  Situation situation;
  units::acceleration::meters_per_second_squared_t expected_result;
};

class ActionImplementation_LimitAccelerationDueToSuspiciousVehicle : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CurrentAccelerationWish>
{
};

TEST_P(ActionImplementation_LimitAccelerationDueToSuspiciousVehicle, CheckCurrentAccelerationWish)
{
  DataFor_CurrentAccelerationWish data = GetParam();
  ActionImplementionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCurrentSituation()).WillByDefault(Return(data.situation));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsDeceleratingDueToSuspiciousSideVehicles()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasJamVelocityEgo()).WillByDefault(Return(false));

  auto result = TEST_HELPER.actionImplementation.LimitAccelerationDueToSuspiciousVehicle(10_mps_sq);

  ASSERT_EQ(result, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_LimitAccelerationDueToSuspiciousVehicle,
    ::testing::Values(
        DataFor_CurrentAccelerationWish{
            .situation = Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE,
            .expected_result = -10_mps_sq},
        DataFor_CurrentAccelerationWish{
            .situation = Situation::FREE_DRIVING,
            .expected_result = 10_mps_sq}));

/*******************************************
 * CHECK GetAccelerationForSpeedAdjustment *
 *******************************************/
struct DataFor_CheckAccelerationForSpeedAdjustment
{
  bool isOnEntryLane;
  bool isAbleToCooperateByAccelerating;
  units::acceleration::meters_per_second_squared_t expected_result;
};

class ActionImplementation_GetAccelerationForSpeedAdjustment : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckAccelerationForSpeedAdjustment>
{
};

TEST_P(ActionImplementation_GetAccelerationForSpeedAdjustment, CheckAccelerationForSpeedAdjustment)
{
  DataFor_CheckAccelerationForSpeedAdjustment data = GetParam();
  ActionImplementionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVelocityLegalEgo()).WillByDefault(Return(20.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(1.0_mps_sq));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsOnEntryLane()).WillByDefault(Return(data.isOnEntryLane));

  DriverParameters driverParameters{};
  driverParameters.maximumLongitudinalAcceleration = 4.0_mps_sq;
  driverParameters.maximumLongitudinalDeceleration = 2.0_mps_sq;
  driverParameters.amountOfSpeedLimitViolation = 2.0_mps;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetUrgencyFactorForAcceleration()).WillByDefault(Return(1.0));

  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.properties.emplace(std::pair<std::string, std::string>("DecelerationFromPowertrainDrag", "5"));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleProperties));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsAbleToCooperateByAccelerating()).WillByDefault(Return(data.isAbleToCooperateByAccelerating));

  auto result = TEST_HELPER.actionImplementation.GetAccelerationForSpeedAdjustment(25_mps);

  ASSERT_EQ(result, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ActionImplementation_GetAccelerationForSpeedAdjustment,
    ::testing::Values(
        DataFor_CheckAccelerationForSpeedAdjustment{
            .isOnEntryLane = true,
            .isAbleToCooperateByAccelerating = true,
            .expected_result = 6_mps_sq},
        DataFor_CheckAccelerationForSpeedAdjustment{
            .isOnEntryLane = false,
            .isAbleToCooperateByAccelerating = false,
            .expected_result = 2_mps_sq}));