/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Fakes/FakePublisher.h"
#include "PeriodicLogger.h"

using ::testing::NiceMock;

/*******************
 * CHECK GetLogger *
 *******************/

TEST(PeriodicLogger_GetLogger, Check_GetLogger)
{
  NiceMock<FakePublisher>* fakePublisher;
  PeriodicLogger periodicLogger(fakePublisher);

  auto result = periodicLogger.GetLogger();

  ASSERT_EQ(result, fakePublisher);
}