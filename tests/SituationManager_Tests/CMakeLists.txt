################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME SituationManager_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(SCM_INCLUDE_DIR ${ROOT_DIR}/include)
set(SURR_VEH_DIR ${DRIVER_DIR}/src/SurroundingVehicles)

add_scm_target(
    NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
    DEFAULT_MAIN
    LINKOSI

    SOURCES
    UnitTestsSituationManager.cpp
    ${DRIVER_DIR}/src/LaneQueryHelper.cpp
    ${DRIVER_DIR}/src/ScmCommons.cpp
    ${DRIVER_DIR}/src/SituationManager.cpp
    ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.cpp

    HEADERS
    ${DRIVER_DIR}/src/LaneQueryHelper.h
    ${DRIVER_DIR}/src/ScmCommons.h
    ${DRIVER_DIR}/src/SituationManager.h
    ${DRIVER_DIR}/src/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.h
    ${SCM_INCLUDE_DIR}/ScmSampler.h

    INCDIRS
    ${SCM_INCLUDE_DIR}
    ${DRIVER_DIR}/
    ${DRIVER_DIR}/src
    ${DRIVER_DIR}/src/HighCognitive
    ${DRIVER_DIR}/../parameterParser/src/Signals

    LIBRARIES
    Stochastics::Stochastics
)

target_compile_definitions(${COMPONENT_TEST_NAME} PRIVATE
    TESTING_ENABLED
)