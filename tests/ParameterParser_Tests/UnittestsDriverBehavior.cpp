/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/parameterParser/src/DriverBehavior/DriverBehavior.h"
#include "Fakes/FakeStochastics.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/*********************
 * CHECK get_default *
 *********************/

struct DataFor_get_default
{
  std::string key;
  double expectedResult;
};

class DriverBehavior_get_default : public ::testing::Test,
                                   public ::testing::WithParamInterface<DataFor_get_default>
{
};

TEST_P(DriverBehavior_get_default, Check_get_default)
{
  DataFor_get_default data = GetParam();

  NiceMock<FakeStochastics> fakeStochastics;

  ON_CALL(fakeStochastics, SampleFromDiscreteDistribution(_)).WillByDefault(Return(1));

  DriverCharacteristicsSampler sampler(&fakeStochastics);

  scm::parameter::ParameterMapping parameters{
      {"DesiredVelocity", 5},
      {"AmountOfSpeedLimitViolation", 5},
      {"AgentCooperationFactor", 5},
      {"AgentSuspiciousBehaviourEvasionFactor", 5},
      {"ComfortLongitudinalAcceleration", 5},
      {"ComfortLongitudinalDeceleration", 5},
      {"OuterKeepingIntensity", 5},
      {"Error", 5},
  };

  GenerateDriverBehavior driverBehavior(sampler,
                                        parameters,
                                        &fakeStochastics);

  if (data.key != "Error")
  {
    auto result = driverBehavior.get_default<double>(data.key);

    EXPECT_NEAR(result, data.expectedResult, 0.01);
  }
  else
  {
    EXPECT_THROW(driverBehavior.get_default<double>(data.key), std::out_of_range);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    DriverBehavior_get_default,
    testing::Values(
        DataFor_get_default{
            .key = "DesiredVelocity",
            .expectedResult = 0.27},
        DataFor_get_default{
            .key = "AmountOfSpeedLimitViolation",
            .expectedResult = 0.27},
        DataFor_get_default{
            .key = "AgentCooperationFactor",
            .expectedResult = 0},
        DataFor_get_default{
            .key = "AgentSuspiciousBehaviourEvasionFactor",
            .expectedResult = 0},
        DataFor_get_default{
            .key = "ComfortLongitudinalAcceleration",
            .expectedResult = 1},
        DataFor_get_default{
            .key = "ComfortLongitudinalDeceleration",
            .expectedResult = 1},
        DataFor_get_default{
            .key = "OuterKeepingIntensity",
            .expectedResult = 1},
        DataFor_get_default{
            .key = "Error",
            .expectedResult = 1}));

/********************************************
 * CHECK CheckForMandatoryDistributionTypes *
 ********************************************/

struct DataFor_CheckForMandatoryDistributionTypes
{
  std::string parameter;
  scm::parameter::StochasticDistribution distribution;
};

class DriverBehavior_CheckForMandatoryDistributionTypes : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataFor_CheckForMandatoryDistributionTypes>
{
};

TEST_P(DriverBehavior_CheckForMandatoryDistributionTypes, Check_CheckForMandatoryDistributionTypes)
{
  DataFor_CheckForMandatoryDistributionTypes data = GetParam();
  NiceMock<FakeStochastics> fakeStochastics;

  ON_CALL(fakeStochastics, SampleFromDiscreteDistribution(_)).WillByDefault(Return(1));

  DriverCharacteristicsSampler sampler(&fakeStochastics);

  scm::parameter::ParameterMapping parameters;
  parameters[data.parameter] = data.distribution;

  GenerateDriverBehavior driverBehavior(sampler,
                                        parameters,
                                        &fakeStochastics);

  if (data.parameter.compare("MaxLateralAcceleration") == 0 || data.parameter.compare("ComfortLateralAcceleration") == 0)
  {
    if (std::holds_alternative<scm::parameter::LogNormalDistribution>(data.distribution))
    {
      EXPECT_THROW(driverBehavior.CheckForMandatoryDistributionTypes(), std::out_of_range);
    }
    else
    {
      EXPECT_NO_THROW(driverBehavior.CheckForMandatoryDistributionTypes());
    }
  }
  else
  {
    if (std::holds_alternative<scm::parameter::NormalDistribution>(data.distribution))
    {
      EXPECT_THROW(driverBehavior.CheckForMandatoryDistributionTypes(), std::out_of_range);
    }
    else
    {
      EXPECT_NO_THROW(driverBehavior.CheckForMandatoryDistributionTypes());
    }
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    DriverBehavior_CheckForMandatoryDistributionTypes,
    testing::Values(
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "MaxLateralAcceleration",
            .distribution = scm::parameter::LogNormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "MaxLateralAcceleration",
            .distribution = scm::parameter::NormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "ComfortLateralAcceleration",
            .distribution = scm::parameter::LogNormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "ComfortLateralAcceleration",
            .distribution = scm::parameter::NormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "ReactionBaseTime",
            .distribution = scm::parameter::NormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "ReactionBaseTime",
            .distribution = scm::parameter::LogNormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "DeltaVViolation",
            .distribution = scm::parameter::NormalDistribution(1, 2, 3, 4)},
        DataFor_CheckForMandatoryDistributionTypes{
            .parameter = "DeltaVViolation",
            .distribution = scm::parameter::LogNormalDistribution(1, 2, 3, 4)}));