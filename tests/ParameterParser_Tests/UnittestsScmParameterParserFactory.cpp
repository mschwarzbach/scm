/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/parameterParser/src/DriverCharacteristics.h"
#include "../module/parameterParser/src/DriverCharacteristicsSampler.h"
#include "../module/parameterParser/src/ScmParameters.h"
#include "Fakes/FakePublisher.h"
#include "Fakes/FakeStochastics.h"
#include "module/parameterParser/include/ScmParameterParserFactory.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::NotNull;
using ::testing::Return;

/****************
 * CHECK Create *
 ****************/

TEST(ScmParameterParserFactory_Create, Check_Create)
{
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakePublisher> fakePublisher;
  ON_CALL(fakeStochastics, SampleFromDiscreteDistribution(_)).WillByDefault(Return(1));

  scm::parameter::ParameterMapping mapping;

  auto result = scm::parameter_parser::factory::Create(&fakeStochastics,
                                                       &mapping,
                                                       &fakePublisher,
                                                       scm::common::VehicleClass::kCompact_car,
                                                       300_m);

  ASSERT_THAT(result, NotNull());
};