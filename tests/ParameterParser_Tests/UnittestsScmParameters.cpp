/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/parameterParser/src/DriverCharacteristics.h"
#include "../module/parameterParser/src/DriverCharacteristicsSampler.h"
#include "../module/parameterParser/src/ScmParameters.h"
#include "Fakes/FakePublisher.h"
#include "Fakes/FakeStochastics.h"
#include "include/common/Parameters.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/*****************
 * CHECK Trigger *
 *****************/

struct DataFor_Trigger
{
  double accelerationAdjustmentThresholdVelocity;
  double vWish;
  double discreteDistribution;
  double comfortLongitudinalAcceleration;
  double comfortLongitudinalDeceleration;
  double outerKeepingIntensity;
  units::velocity::meters_per_second_t expectedResult_DesiredVelocity;
  units::acceleration::meters_per_second_squared_t expectedResult_ComfortLongitudinalAcceleration;
  units::acceleration::meters_per_second_squared_t expectedResult_ComfortLongitudinalDeceleration;
  double expectedResult_OuterKeepingIntensity;
};

class ScmParameters_Trigger : public ::testing::Test,
                              public ::testing::WithParamInterface<DataFor_Trigger>
{
};

TEST_P(ScmParameters_Trigger, Check_Trigger)
{
  DataFor_Trigger data = GetParam();
  NiceMock<FakePublisher> fakePublisher;
  NiceMock<FakeStochastics> fakeStochastics;

  ON_CALL(fakeStochastics, GetUniformDistributed(0, 1)).WillByDefault(Return(1));
  ON_CALL(fakeStochastics, SampleFromDiscreteDistribution(_)).WillByDefault(Return(data.discreteDistribution));

  scm::parameter::ParameterMapping mapping;
  mapping["EnableHighCognitiveMode"] = true;
  mapping["IdealPerception"] = true;
  mapping["AccelerationAdjustmentThresholdVelocity"] = data.accelerationAdjustmentThresholdVelocity;
  mapping["AgentCooperationFactor"] = 2.0;
  mapping["AgentSuspiciousBehaviourEvasionFactor"] = 3.0;
  mapping["AnticipationQuota"] = 4.0;
  mapping["CarQueuingDistance"] = 5.0;
  mapping["ComfortAngularVelocitySteeringWheel"] = 6.0;
  mapping["ComfortLateralAcceleration"] = 7.0;
  mapping["ComfortLongitudinalAcceleration"] = data.comfortLongitudinalAcceleration;
  mapping["ComfortLongitudinalDeceleration"] = data.comfortLongitudinalDeceleration;
  mapping["DeltaVViolation"] = scm::parameter::LogNormalDistribution(1, 2, 3, 4);
  mapping["DeltaVWish"] = 11.0;
  mapping["DesiredTimeAtTargetSpeed"] = 12.0;
  mapping["EgoLaneKeepingIntensity"] = 13.0;
  mapping["EgoLaneKeepingQuota"] = 14.0;
  mapping["HysteresisForRestart"] = 15.0;
  mapping["LaneChangeProhibitionIgnoringQuota"] = 16.0;
  mapping["LateralOffsetNeutralPosition"] = 17.0;
  mapping["LateralOffsetNeutralPositionQuota"] = 18.0;
  mapping["LateralOffsetNeutralPositionRescueLane"] = 19.0;
  mapping["MaxComfortAccelerationFactor"] = 20.0;
  mapping["MaxComfortDecelerationFactor"] = 21.0;
  mapping["MaxLateralAcceleration"] = 22.0;
  mapping["MaxLongitudinalAcceleration"] = 23.0;
  mapping["MaxLongitudinalDeceleration"] = 24.0;
  mapping["MaximumAngularVelocitySteeringWheel"] = 25.0;
  mapping["OuterKeepingIntensity"] = data.outerKeepingIntensity;
  mapping["OuterKeepingQuota"] = 27.0;
  mapping["PreviewDistanceMin"] = 28.0;
  mapping["PreviewTimeHeadway"] = 29.0;
  mapping["ProportionalityFactorForFollowingDistance"] = 30.0;
  mapping["ReactionBaseTime"] = scm::parameter::LogNormalDistribution(1, 2, 3, 4);
  mapping["RightOvertakingProhibitionIgnoringQuota"] = 32.0;
  mapping["ThresholdLooming"] = 33.0;
  mapping["TimeHeadwayFreeLaneChange"] = 34.0;
  mapping["UseShoulderInTrafficJamQuota"] = 35.0;
  mapping["UseShoulderInTrafficJamToExitQuota"] = 36.0;
  mapping["UseShoulderWhenPeerPressuredQuota"] = 37.0;
  mapping["VWish"] = data.vWish;
  mapping["VelocityForMaxAcceleration"] = 10.0;
  mapping["indicatorActivation"] = 40;
  mapping["DistractionPercentage"] = 41.0;
  mapping["DistractionQuota"] = 42.0;

  scm::common::VehicleClass vehicleClass = scm::common::VehicleClass::kCompact_car;
  scm::ScmParameters parameters(&mapping,
                                &fakePublisher,
                                &fakeStochastics,
                                vehicleClass,
                                300_m);

  auto result = parameters.Trigger(20_mps);

  ASSERT_TRUE(result.driverParameters.highCognitiveMode);
  ASSERT_TRUE(result.driverParameters.idealPerception);
  ASSERT_EQ(result.driverParameters.agentCooperationFactor, 2.0);
  ASSERT_EQ(result.driverParameters.agentSuspiciousBehaviourEvasionFactor, 3.0);
  ASSERT_EQ(result.driverParameters.anticipationQuota, 4.0);
  ASSERT_EQ(result.driverParameters.carQueuingDistance, 5.0_m);
  ASSERT_EQ(result.driverParameters.comfortAngularVelocitySteeringWheel, 6.0_rad_per_s);
  ASSERT_EQ(result.driverParameters.comfortLateralAcceleration, 7.0_mps_sq);
  EXPECT_NEAR(result.driverParameters.comfortLongitudinalAcceleration.value(), data.expectedResult_ComfortLongitudinalAcceleration.value(), 0.01);
  EXPECT_NEAR(result.driverParameters.comfortLongitudinalDeceleration.value(), data.expectedResult_ComfortLongitudinalDeceleration.value(), 0.01);
  ASSERT_EQ(result.driverParameters.desiredVelocity, data.expectedResult_DesiredVelocity);
  ASSERT_EQ(result.driverParameters.desiredTimeAtTargetSpeed, 12.0_s);
  ASSERT_EQ(result.driverParameters.egoLaneKeepingIntensity.value(), 13);
  ASSERT_EQ(result.driverParameters.egoLaneKeepingQuota.value(), 14);
  ASSERT_EQ(result.driverParameters.laneChangeProhibitionIgnoringQuota.value(), 16);
  ASSERT_EQ(result.driverParameters.lateralOffsetNeutralPosition, 17_m);
  ASSERT_EQ(result.driverParameters.lateralOffsetNeutralPositionQuota.value(), 18);
  ASSERT_EQ(result.driverParameters.lateralOffsetNeutralPositionRescueLane, 19_m);
  ASSERT_EQ(result.driverParameters.carRestartDistance, 20_m);
  ASSERT_EQ(result.driverParameters.maximumLateralAcceleration, 22_mps_sq);
  ASSERT_EQ(result.driverParameters.maximumLongitudinalAcceleration, 23_mps_sq);
  ASSERT_EQ(result.driverParameters.maximumLongitudinalDeceleration, 24_mps_sq);
  ASSERT_EQ(result.driverParameters.maximumAngularVelocitySteeringWheel, 25_rad_per_s);
  ASSERT_EQ(result.driverParameters.outerKeepingIntensity.value(), data.expectedResult_OuterKeepingIntensity);
  ASSERT_EQ(result.driverParameters.outerKeepingQuota.value(), 27);
  ASSERT_EQ(result.driverParameters.proportionalityFactorForFollowingDistance, 30);
  ASSERT_EQ(result.driverParameters.thresholdLoomingFovea.value(), 33);
  ASSERT_EQ(result.driverParameters.timeHeadwayFreeLaneChange, 34_s);
  ASSERT_EQ(result.driverParameters.useShoulderInTrafficJamQuota.value(), 35);
  ASSERT_EQ(result.driverParameters.useShoulderInTrafficJamToExitQuota.value(), 36);
  ASSERT_EQ(result.driverParameters.useShoulderWhenPeerPressuredQuota.value(), 37);
  ASSERT_EQ(result.driverParameters.distractionPercentage.value(), 41);
  ASSERT_EQ(result.driverParameters.distractionQuota.value(), 42);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ScmParameters_Trigger,
    testing::Values(
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 1.0,
            .vWish = 38.0,
            .discreteDistribution = 2.0,
            .comfortLongitudinalAcceleration = 2.0,
            .comfortLongitudinalDeceleration = 3.0,
            .outerKeepingIntensity = 26.0,
            .expectedResult_DesiredVelocity = 38.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 2.0_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 3.0_mps_sq,
            .expectedResult_OuterKeepingIntensity = 26.0},
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 20.5,
            .vWish = 38.0,
            .discreteDistribution = 2.0,
            .comfortLongitudinalAcceleration = 2.0,
            .comfortLongitudinalDeceleration = 3.0,
            .outerKeepingIntensity = 26.0,
            .expectedResult_DesiredVelocity = 38.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 3.8_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 5.85_mps_sq,
            .expectedResult_OuterKeepingIntensity = 26.0},
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 1.0,
            .vWish = ScmDefinitions::DEFAULT_VALUE,
            .discreteDistribution = 18.0,
            .comfortLongitudinalAcceleration = 2.0,
            .comfortLongitudinalDeceleration = 3.0,
            .outerKeepingIntensity = 26.0,
            .expectedResult_DesiredVelocity = 5.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 2.0_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 3.0_mps_sq,
            .expectedResult_OuterKeepingIntensity = 26.0},
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 1.0,
            .vWish = 38.0,
            .discreteDistribution = 0.6,
            .comfortLongitudinalAcceleration = ScmDefinitions::DEFAULT_VALUE,
            .comfortLongitudinalDeceleration = 3.0,
            .outerKeepingIntensity = 26.0,
            .expectedResult_DesiredVelocity = 38.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 0.6_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 3.0_mps_sq,
            .expectedResult_OuterKeepingIntensity = 26.0},
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 1.0,
            .vWish = 38.0,
            .discreteDistribution = 0.7,
            .comfortLongitudinalAcceleration = 2.0,
            .comfortLongitudinalDeceleration = ScmDefinitions::DEFAULT_VALUE,
            .outerKeepingIntensity = 26.0,
            .expectedResult_DesiredVelocity = 38.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 2.0_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 0.7_mps_sq,
            .expectedResult_OuterKeepingIntensity = 26.0},
        DataFor_Trigger{
            .accelerationAdjustmentThresholdVelocity = 1.0,
            .vWish = 38.0,
            .discreteDistribution = 0.8,
            .comfortLongitudinalAcceleration = 2.0,
            .comfortLongitudinalDeceleration = 3.0,
            .outerKeepingIntensity = ScmDefinitions::DEFAULT_VALUE,
            .expectedResult_DesiredVelocity = 38.0_mps,
            .expectedResult_ComfortLongitudinalAcceleration = 2.0_mps_sq,
            .expectedResult_ComfortLongitudinalDeceleration = 3.0_mps_sq,
            .expectedResult_OuterKeepingIntensity = 0.8}));