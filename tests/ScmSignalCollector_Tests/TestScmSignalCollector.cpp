/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeStochastics.h"
#include "include/common/ScmDefinitions.h"
#include "include/signal/driverInput.h"
#include "module/driver/src/ScmSignalCollector.h"
#include "module/parameterParser/src/Signals/ParametersScmSignal.h"

class Test_ScmSignalCollector : public ::testing::Test
{
protected:
  std::unique_ptr<FakeStochastics> fakeStochastics = std::make_unique<FakeStochastics>();
  scm::signal::DriverInput driverInput;
  OwnVehicleInformationSCM ownVehicleInformationSCM;
  TrafficRuleInformationSCM trafficRuleInformationSCM;
  GeometryInformationSCM geometryInformationSCM;
  SurroundingObjectsSCM surroundingObjectsSCM;
  std::unique_ptr<ScmSignalCollector> signalCollector{nullptr};

  virtual void SetUp()
  {
    signalCollector = std::make_unique<ScmSignalCollector>(fakeStochastics.get(), 100_ms);

    // set up own vehicle information
    ownVehicleInformationSCM.id = 0;
    ownVehicleInformationSCM.lateralVelocity = 0.2_mps;
    ownVehicleInformationSCM.absoluteVelocity = 33.33_mps;

    // set up surrounding objects
    ObjectInformationSCM egoFront, egoRear;
    egoFront.id = 22;
    egoFront.collision = true;
    egoRear.id = 33;
    egoRear.collision = false;
    surroundingObjectsSCM.ongoingTraffic.Ahead().Close().push_back(egoFront);
    surroundingObjectsSCM.ongoingTraffic.Behind().Close().push_back(egoRear);

    // set up traffic signs
    std::vector<scm::CommonTrafficSign::Entity> trafficSigns;
    scm::CommonTrafficSign::Entity speedLimit;
    speedLimit.type = scm::CommonTrafficSign::Type::MaximumSpeedLimit;
    speedLimit.value = 120;
    trafficSigns.push_back(speedLimit);
    trafficRuleInformationSCM.Close().trafficSigns = trafficSigns;

    // set up geometryInformation
    geometryInformationSCM.Close().laneType = scm::LaneType::Driving;
    geometryInformationSCM.Right().laneType = scm::LaneType::Exit;
    geometryInformationSCM.Left().laneType = scm::LaneType::Undefined;

    geometryInformationSCM.visibilityDistance = 250.0_m;
    geometryInformationSCM.numberOfLanes = 2;

    OwnVehicleRoutePose ownVehicleRoutePose{};

    driverInput.sensorOutput.surroundingObjectsSCM = surroundingObjectsSCM;
    driverInput.sensorOutput.ownVehicleInformationSCM = ownVehicleInformationSCM;
    driverInput.sensorOutput.trafficRuleInformationSCM = trafficRuleInformationSCM;
    driverInput.sensorOutput.geometryInformationSCM = geometryInformationSCM;
    driverInput.sensorOutput.ownVehicleRoutePose = ownVehicleRoutePose;
  }

  void UpdateDependencies()
  {
    signalCollector->Update(driverInput);
  }
};

/*******************************
 * CHECK RegularInitialization *
 *******************************/

TEST_F(Test_ScmSignalCollector, RegularInitialization)
{
  UpdateDependencies();
  static constexpr int expected_egoId = 0;
  static constexpr units::velocity::meters_per_second_t expected_egoLateralVelocity = 0.2_mps;
  ASSERT_EQ(signalCollector->GetDependencies()->GetOwnVehicleInformationScm()->id, expected_egoId);
  ASSERT_EQ(signalCollector->GetDependencies()->GetOwnVehicleInformationScm()->lateralVelocity, expected_egoLateralVelocity);
}

/******************************************
 * CHECK SurroundingObjectsHasValidValues *
 ******************************************/

TEST_F(Test_ScmSignalCollector, AfterSucessfullUpdate_SurroundingObjectsHasValidValues)
{
  UpdateDependencies();

  auto frontObject = signalCollector->GetDependencies()->GetSurroundingObjectsScm()->ongoingTraffic.Ahead().Close().front();
  static constexpr int expected_frontId = 22;
  static constexpr bool expected_frontCollision = true;
  ASSERT_EQ(frontObject.id, expected_frontId);
  ASSERT_EQ(frontObject.collision, expected_frontCollision);

  static constexpr int expected_rearId = 33;
  static constexpr bool expected_rearCollision = false;
  auto rearObject = signalCollector->GetDependencies()->GetSurroundingObjectsScm()->ongoingTraffic.Behind().Close().front();
  ASSERT_EQ(rearObject.id, expected_rearId);
  ASSERT_EQ(rearObject.collision, expected_rearCollision);
}

/**************************************************************
 * CHECK NewOwnVehicleInformation GetsUpdatedInScmDependencie *
 **************************************************************/

TEST_F(Test_ScmSignalCollector, NewOwnVehicleInformation_GetsUpdatedInScmDependencies)
{
  UpdateDependencies();
  static constexpr int expected_egoId = 0;
  ASSERT_EQ(signalCollector->GetDependencies()->GetOwnVehicleInformationScm()->id, expected_egoId);

  ownVehicleInformationSCM.id = 222;

  driverInput.sensorOutput.ownVehicleInformationSCM = ownVehicleInformationSCM;
  signalCollector->Update(driverInput);

  ASSERT_EQ(signalCollector->GetDependencies()->GetOwnVehicleInformationScm()->id, 222);
}

/************************************************************
 * CHECK NewSurroundingObjects GetsUpdatedInScmDependencies *
 ************************************************************/

TEST_F(Test_ScmSignalCollector, NewSurroundingObjects_GetsUpdatedInScmDependencies)
{
  UpdateDependencies();
  static constexpr int expected_frontId = 22;

  auto frontObject = signalCollector->GetDependencies()->GetSurroundingObjectsScm()->ongoingTraffic.Ahead().Close().front();
  ASSERT_EQ(frontObject.id, expected_frontId);

  surroundingObjectsSCM.ongoingTraffic.Ahead().Close().front().id = 1337;
  driverInput.sensorOutput.surroundingObjectsSCM = surroundingObjectsSCM;

  signalCollector->Update(driverInput);

  frontObject = signalCollector->GetDependencies()->GetSurroundingObjectsScm()->ongoingTraffic.Ahead().Close().front();
  ASSERT_EQ(frontObject.id, 1337);
}

/****************************************
 * CHECK CheckForTrafficRuleInformation *
 ****************************************/

TEST_F(Test_ScmSignalCollector, CheckForTrafficRuleInformation)
{
  UpdateDependencies();

  auto egoLane = signalCollector->GetDependencies()->GetTrafficRuleInformationScm()->Close();
  ASSERT_EQ(egoLane.trafficSigns.at(0).type, scm::CommonTrafficSign::Type::MaximumSpeedLimit);
  ASSERT_EQ(egoLane.trafficSigns.at(0).value, 120);
}

/*********************************************************************
 * CHECK UpdatingTrafficRuleInformation GetsUpdatedInScmDependencies *
 *********************************************************************/

TEST_F(Test_ScmSignalCollector, UpdatingTrafficRuleInformation_GetsUpdatedInScmDependencies)
{
  UpdateDependencies();

  scm::CommonTrafficSign::Entity highwayExit;
  highwayExit.type = scm::CommonTrafficSign::Type::HighWayExit;
  trafficRuleInformationSCM.Close().trafficSigns.push_back(highwayExit);
  driverInput.sensorOutput.trafficRuleInformationSCM = trafficRuleInformationSCM;
  signalCollector->Update(driverInput);

  auto egoLane = signalCollector->GetDependencies()->GetTrafficRuleInformationScm()->Close();
  ASSERT_EQ(egoLane.trafficSigns.at(1).type, scm::CommonTrafficSign::Type::HighWayExit);
}

/*************************************
 * CHECK CheckForGeometryInformation *
 *************************************/

TEST_F(Test_ScmSignalCollector, CheckForGeometryInformation)
{
  UpdateDependencies();

  auto geometryInformation = signalCollector->GetDependencies()->GetGeometryInformationScm();

  ASSERT_EQ(geometryInformation->Close().laneType, scm::LaneType::Driving);
  ASSERT_EQ(geometryInformation->Right().laneType, scm::LaneType::Exit);
  ASSERT_EQ(geometryInformation->Left().laneType, scm::LaneType::Undefined);

  ASSERT_EQ(geometryInformationSCM.visibilityDistance, 250._m);
  ASSERT_EQ(geometryInformationSCM.numberOfLanes, 2);
}

/******************************************************************
 * CHECK UpdatingGeometryInformation GetsUpdatedInScmDependencies *
 ******************************************************************/

TEST_F(Test_ScmSignalCollector, UpdatingGeometryInformation_GetsUpdatedInScmDependencies)
{
  UpdateDependencies();

  geometryInformationSCM.Close().laneType = scm::LaneType::Curb;
  geometryInformationSCM.Left().laneType = scm::LaneType::Driving;
  geometryInformationSCM.Right().laneType = scm::LaneType::Stop;

  geometryInformationSCM.visibilityDistance = 200.0_m;
  geometryInformationSCM.numberOfLanes = 3;

  driverInput.sensorOutput.geometryInformationSCM = geometryInformationSCM;
  signalCollector->Update(driverInput);

  auto geometryInformation = signalCollector->GetDependencies()->GetGeometryInformationScm();

  ASSERT_EQ(geometryInformation->Close().laneType, scm::LaneType::Curb);
  ASSERT_EQ(geometryInformation->Left().laneType, scm::LaneType::Driving);
  ASSERT_EQ(geometryInformation->Right().laneType, scm::LaneType::Stop);

  ASSERT_EQ(geometryInformationSCM.visibilityDistance, 200._m);
  ASSERT_EQ(geometryInformationSCM.numberOfLanes, 3);
}

/**************************************
 * CHECK CheckInitialDriverParameters *
 **************************************/

TEST_F(Test_ScmSignalCollector, CheckInitialDriverParameters)
{
  UpdateDependencies();

  auto driverParams = signalCollector->GetDependencies()->GetDriverParameters();

  ASSERT_EQ(driverParams.desiredVelocity, units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));
  ASSERT_EQ(driverParams.previewDistance, units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE));

  if (driverParams.thresholdLoomingFovea.has_value())
  {
    ASSERT_DOUBLE_EQ(driverParams.thresholdLoomingFovea.value(), ScmDefinitions::DEFAULT_VALUE);
  }

  if (driverParams.anticipationQuota.has_value())
  {
    ASSERT_DOUBLE_EQ(driverParams.anticipationQuota.value(), ScmDefinitions::DEFAULT_VALUE);
  }

  ASSERT_FALSE(driverParams.idealPerception);
}

/***************************************************************
 * CHECK UpdatingDriverParameters GetsUpdatedInScmDependencies *
 ***************************************************************/

TEST_F(Test_ScmSignalCollector, UpdatingDriverParameters_GetsUpdatedInScmDependencies)
{
  UpdateDependencies();

  DriverParameters updatingDriverParams;
  updatingDriverParams.desiredVelocity = 37.0_mps;
  updatingDriverParams.previewDistance = 250.0_m;
  updatingDriverParams.thresholdLoomingFovea = 9.0;
  updatingDriverParams.anticipationQuota = 0.6;
  updatingDriverParams.idealPerception = true;

  driverInput.driverParameters = updatingDriverParams;
  signalCollector->Update(driverInput);

  auto driverParams = signalCollector->GetDependencies()->GetDriverParameters();
  ASSERT_EQ(driverParams.desiredVelocity, 37.0_mps);
  ASSERT_EQ(driverParams.previewDistance, 250.0_m);
  ASSERT_DOUBLE_EQ(driverParams.thresholdLoomingFovea.value(), 9.0);
  ASSERT_DOUBLE_EQ(driverParams.anticipationQuota.value(), 0.6);
  ASSERT_TRUE(driverParams.idealPerception);
}

/*********************************************
 * CHECK CheckInitialVehicleModelsParameters *
 *********************************************/

TEST_F(Test_ScmSignalCollector, CheckInitialVehicleModelsParameters)
{
  UpdateDependencies();

  auto vehicleModelParams = signalCollector->GetDependencies()->GetVehicleModelParameters();

  ASSERT_EQ(vehicleModelParams.classification, scm::common::VehicleClass::kOther);

  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.x, 0);
  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.y, 0);
  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.z, 0);
}

/*********************************************
 * CHECK CheckInitialVehicleModelsParameters *
 *********************************************/

TEST_F(Test_ScmSignalCollector, CheckInitialVehicleModelsParameters_)
{
  UpdateDependencies();

  scm::common::vehicle::properties::EntityProperties updatingVehicleModelParams;
  updatingVehicleModelParams.classification = scm::common::VehicleClass::kHeavy_truck;
  updatingVehicleModelParams.bounding_box.geometric_center = {1.0, 2.0, 3.0};

  driverInput.vehicleParameters = updatingVehicleModelParams;
  signalCollector->Update(driverInput);

  auto vehicleModelParams = signalCollector->GetDependencies()->GetVehicleModelParameters();
  ASSERT_EQ(vehicleModelParams.classification, scm::common::VehicleClass::kHeavy_truck);
  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.x, 1.0);
  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.y, 2.0);
  ASSERT_DOUBLE_EQ(vehicleModelParams.bounding_box.geometric_center.z, 3.0);
}