################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME Merging_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN
  LINKOSI

  SOURCES
  UnitTestMerging.cpp
  ${DRIVER_DIR}/src/MicroscopicCharacteristics.cpp
  ${DRIVER_DIR}/src/InfrastructureCharacteristics.cpp
  ${DRIVER_DIR}/src/LaneQueryHelper.cpp
  ${DRIVER_DIR}/src/ScmCommons.cpp
  ${DRIVER_DIR}/src/LaneChangeDimension.cpp
  ${DRIVER_DIR}/src/Merging/ZipMerging.cpp
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.cpp
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.cpp

  HEADERS
  ../Fakes/FakeMentalModel.h
  ${DRIVER_DIR}/src/FeatureExtractorInterface.h
  ${DRIVER_DIR}/src/MentalCalculationsInterface.h
  ${DRIVER_DIR}/src/MentalModel.h
  ${DRIVER_DIR}/src/MentalModelDefinitions.h
  ${DRIVER_DIR}/src/MentalModelInterface.h
  ${DRIVER_DIR}/src/Merging/Merging.h
  ${DRIVER_DIR}/src/Merging/ZipMerging.h
  ${DRIVER_DIR}/src/ScmCommons.h
  ${DRIVER_DIR}/src/MicroscopicCharacteristics.h
  ${DRIVER_DIR}/src/InfrastructureCharacteristics.h
  ${DRIVER_DIR}/src/LaneQueryHelper.h
  ${DRIVER_DIR}/src/LaneChangeDimension.h
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculations.h
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculations.h
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalCalculationsInterface.h
  ${DRIVER_DIR}/src/LongitudinalCalculations/LongitudinalPartCalculationsInterface.h
  ${ROOT_DIR}/include/common/ScmEnums.h

  INCDIRS
  ${DRIVER_DIR}/src
  ${DRIVER_DIR}/src/Signals

  LIBRARIES
  Stochastics::Stochastics
)

