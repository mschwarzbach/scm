/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "Merging/Merging.cpp"
#include "Merging/Merging.h"

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

/*****************************************
 * CHECK ChooseFastestReachableMergeGap  *
 *****************************************/

TEST(Merging_ChooseFastestReachableMergeGap, EmptyGaps)
{
  const std::vector<Merge::Gap>& mergeGaps{};

  auto result = ChooseFastestReachableMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, nullptr);
  ASSERT_EQ(result.follower, nullptr);
  ASSERT_EQ(result.selectedMergePhases.size(), 0.0);
}

TEST(Merging_ChooseFastestReachableMergeGap, SimpleTestWithOnePhase)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 2.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.selectedMergePhases = {{MergeAction::DECELERATING, 2.0_s, 1.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap};

  auto result = ChooseFastestReachableMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, &v1);
  ASSERT_EQ(result.follower, nullptr);
  ASSERT_EQ(result.selectedMergePhases, firstGap.selectedMergePhases);
}

TEST(Merging_ChooseFastestReachableMergeGap, FirstGapsAccumulatedPhases_AreLargerThanSecondGapsPhase)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 2.0_m}, {MergeAction::CONSTANT_DRIVING, 1.0_s, 2.0_m}, {MergeAction::DECELERATING, 1.0_s, 2.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.selectedMergePhases = {{MergeAction::DECELERATING, 2.0_s, 1.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap};

  auto result = ChooseFastestReachableMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, nullptr);
  ASSERT_EQ(result.follower, &v1);
  ASSERT_EQ(result.selectedMergePhases, secondGap.selectedMergePhases);
}

TEST(Merging_ChooseFastestReachableMergeGap, WhenPhasesDurationIsEqual_ChooseFirstGap)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 2.0_s, 2.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.selectedMergePhases = {{MergeAction::DECELERATING, 2.0_s, 1.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap};

  auto result = ChooseFastestReachableMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, &v1);
  ASSERT_EQ(result.follower, nullptr);
  ASSERT_EQ(result.selectedMergePhases, firstGap.selectedMergePhases);
}

/********************************
 * CHECK ChooseClosestMergeGap  *
 ********************************/

TEST(Merging_ChooseClosestMergeGap, EmptyGaps)
{
  const std::vector<Merge::Gap>& mergeGaps{};

  auto result = ChooseClosestMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, nullptr);
  ASSERT_EQ(result.follower, nullptr);
  ASSERT_EQ(result.selectedMergePhases.size(), 0.0);
}

TEST(Merging_ChooseClosestMergeGap, SimpleTestWithOnePhase)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.distance = 2.0_m;
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 0.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.distance = 1.0_m;
  secondGap.selectedMergePhases = {{MergeAction::DECELERATING, 2.0_s, 0.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap};

  auto result = ChooseClosestMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, nullptr);
  ASSERT_EQ(result.follower, &v1);
  ASSERT_EQ(result.selectedMergePhases, secondGap.selectedMergePhases);
}

TEST(Merging_ChooseClosestMergeGap, ThreeGaps)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));
  ON_CALL(v1, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v1, GetLength()).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetId()).WillByDefault(Return(2));
  ON_CALL(v2, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v2, GetLength()).WillByDefault(Return(5_m));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.distance = 2.0_m;
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 1.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.distance = 1.0_m;
  secondGap.selectedMergePhases = {{MergeAction::DECELERATING, 2.0_s, 2.0_m}};

  Merge::Gap thirdGap{&v2, &v1};
  thirdGap.distance = 0.5_m;
  thirdGap.selectedMergePhases = {{MergeAction::DECELERATING, 3.0_s, 3.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap, thirdGap};

  auto result = ChooseClosestMergeGap(mergeGaps);
  ASSERT_EQ(result.leader, &v2);
  ASSERT_EQ(result.follower, &v1);
  ASSERT_EQ(result.selectedMergePhases, thirdGap.selectedMergePhases);
}

/******************************
 * CHECK RemoveTooNarrowGaps  *
 ******************************/

TEST(Merging_RemoveTooNarrowGaps, RemoveGaps)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));
  ON_CALL(v1, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v1, GetLength()).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetId()).WillByDefault(Return(2));
  ON_CALL(v2, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v2, GetLength()).WillByDefault(Return(5_m));

  Merge::Gap firstGap{&v1, nullptr};
  firstGap.safetyClearanceToLeader = 10.0_m;
  firstGap.safetyClearanceToFollower = 10.0_m;
  firstGap.fullMergeSpace = 30.0_m;
  firstGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 0.0_m}};

  Merge::Gap secondGap{nullptr, &v1};
  secondGap.safetyClearanceToLeader = 10.0_m;
  secondGap.safetyClearanceToFollower = 10.0_m;
  secondGap.fullMergeSpace = 30.0_m;
  secondGap.selectedMergePhases = {};

  Merge::Gap thirdGap{&v1, &v2};
  thirdGap.safetyClearanceToLeader = 10.0_m;
  thirdGap.safetyClearanceToFollower = 10.0_m;
  thirdGap.fullMergeSpace = 22.0_m;
  thirdGap.selectedMergePhases = {{MergeAction::ACCELERATING, 1.0_s, 0.0_m}};

  const std::vector<Merge::Gap>& mergeGaps = {firstGap, secondGap, thirdGap};

  auto result = RemoveTooNarrowGaps(mergeGaps, 4.5_m);
  ASSERT_EQ(result.size(), 1);

  ASSERT_EQ(result.front().leader, &v1);
  ASSERT_EQ(result.front().follower, nullptr);
  ASSERT_EQ(result.front().selectedMergePhases, firstGap.selectedMergePhases);
}

/***************************************
 * CHECK DetermineSurroundingVehicles  *
 ***************************************/

TEST(Merging_DetermineSurroundingVehicles, CheckOrderOfVehiclesByRelativeDistance)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(v1, GetRelativeLongitudinalPosition()).WillByDefault(Return(2.0_m));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(v2, GetRelativeLongitudinalPosition()).WillByDefault(Return(1.0_m));

  std::vector<const SurroundingVehicleInterface*> allSurroundingVehicles;
  allSurroundingVehicles.push_back(&v1);
  allSurroundingVehicles.push_back(&v2);

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  auto resultSurroundingVehicles = DetermineSurroundingVehicles(RelativeLane::RIGHT, allSurroundingVehicles, surroundingVehicleQueryFactory);
  ASSERT_EQ(resultSurroundingVehicles.size(), 2);
  ASSERT_EQ(&v2, resultSurroundingVehicles.at(0));
  ASSERT_EQ(&v1, resultSurroundingVehicles.at(1));
}

TEST(Merging_DetermineSurroundingVehicles, OnlyAgentsWithGivenRelativeLaneShallBeInResultingVector)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(v1, GetRelativeLongitudinalPosition()).WillByDefault(Return(2.0_m));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(v2, GetRelativeLongitudinalPosition()).WillByDefault(Return(1.0_m));

  NiceMock<FakeSurroundingVehicle> v3;
  ON_CALL(v3, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(v3, GetRelativeLongitudinalPosition()).WillByDefault(Return(2.0_m));

  std::vector<const SurroundingVehicleInterface*> allSurroundingVehicles;
  allSurroundingVehicles.push_back(&v1);
  allSurroundingVehicles.push_back(&v2);
  allSurroundingVehicles.push_back(&v3);

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  auto resultSurroundingVehicles = DetermineSurroundingVehicles(RelativeLane::RIGHT, allSurroundingVehicles, surroundingVehicleQueryFactory);
  ASSERT_EQ(resultSurroundingVehicles.size(), 2);
  ASSERT_EQ(&v2, resultSurroundingVehicles.at(0));
  ASSERT_EQ(&v3, resultSurroundingVehicles.at(1));
}

TEST(Merging_DetermineSurroundingVehicles, NoSurroundingVehicleWithGivenRelativeLane_LeadsToEmptyVector)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));

  std::vector<const SurroundingVehicleInterface*> allSurroundingVehicles;
  allSurroundingVehicles.push_back(&v1);
  allSurroundingVehicles.push_back(&v2);

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  auto resultSurroundingVehicles = DetermineSurroundingVehicles(RelativeLane::EGO, allSurroundingVehicles, surroundingVehicleQueryFactory);
  ASSERT_EQ(resultSurroundingVehicles.size(), 0);
}

/****************************
 * CHECK DetermineMergeGap  *
 ****************************/

TEST(Merging_DetermineMergeGap, NoSurroundingVehicles_LeadsToZeroMergeGaps)
{
  std::vector<const SurroundingVehicleInterface*> surroundingVehicles{};

  auto resultGaps = DetermineMergeGaps(surroundingVehicles);

  ASSERT_EQ(resultGaps.size(), 0);
}

TEST(Merging_DetermineMergeGap, OneSurroundingVehicle_LeadsToTwoGaps_WithOnlyOneLeaderAndOneFollowerEach)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  surroundingVehicles.push_back(&v1);

  auto resultGaps = DetermineMergeGaps(surroundingVehicles);

  ASSERT_EQ(resultGaps.size(), 2);
  ASSERT_EQ(resultGaps.at(0).leader, &v1);
  ASSERT_EQ(resultGaps.at(0).follower, nullptr);
  ASSERT_EQ(resultGaps.at(1).leader, nullptr);
  ASSERT_EQ(resultGaps.at(1).follower, &v1);
}

TEST(Merging_DetermineMergeGap, n_SurroundingVehicles_LeadsTo_n_plus1_MergeGaps)
{
  NiceMock<FakeSurroundingVehicle> v1;
  ON_CALL(v1, GetId()).WillByDefault(Return(1));
  ON_CALL(v1, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v1, GetLength()).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> v2;
  ON_CALL(v2, GetId()).WillByDefault(Return(2));
  ON_CALL(v2, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v2, GetLength()).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> v3;
  ON_CALL(v3, GetId()).WillByDefault(Return(3));
  ON_CALL(v3, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v3, GetLength()).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> v4;
  ON_CALL(v4, GetId()).WillByDefault(Return(4));
  ON_CALL(v4, GetLongitudinalObstruction()).WillByDefault(Return(ObstructionLongitudinal()));
  ON_CALL(v4, GetLength()).WillByDefault(Return(5_m));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  surroundingVehicles.push_back(&v1);
  surroundingVehicles.push_back(&v2);
  surroundingVehicles.push_back(&v3);
  surroundingVehicles.push_back(&v4);

  auto resultGaps = DetermineMergeGaps(surroundingVehicles);

  ASSERT_EQ(resultGaps.size(), 5);
}

TEST(Merging_DetermineMergeGap, TestFullMergeSpace)
{
  static constexpr auto VEHICLE_LENGTH = 2_m;

  NiceMock<FakeSurroundingVehicle> v1;
  ObstructionLongitudinal obstructionV1(0.0_m, 0.0_m, 0.0_m);
  ON_CALL(v1, GetId()).WillByDefault(Return(1));
  ON_CALL(v1, GetLength()).WillByDefault(Return(VEHICLE_LENGTH));
  ON_CALL(v1, GetLongitudinalObstruction()).WillByDefault(Return(obstructionV1));

  NiceMock<FakeSurroundingVehicle> v2;
  ObstructionLongitudinal obstructionV2(0.0_m, 0.0_m, 10.0_m);
  ON_CALL(v2, GetId()).WillByDefault(Return(2));
  ON_CALL(v2, GetLength()).WillByDefault(Return(VEHICLE_LENGTH));
  ON_CALL(v2, GetLongitudinalObstruction()).WillByDefault(Return(obstructionV2));

  std::vector<const SurroundingVehicleInterface*> surroundingVehicles;
  surroundingVehicles.push_back(&v1);
  surroundingVehicles.push_back(&v2);

  auto resultGaps = DetermineMergeGaps(surroundingVehicles);

  ASSERT_TRUE(units::math::isinf(resultGaps.at(0).fullMergeSpace));
  ASSERT_EQ(resultGaps.at(1).fullMergeSpace, 8.0_m);
  ASSERT_TRUE(units::math::isinf(resultGaps.at(2).fullMergeSpace));
}

class MergingUnderTest : public Merging
{
public:
  template <typename... Args>
  MergingUnderTest(Args&&... args)
      : Merging{std::forward<Args>(args)...} {};

  using Merging::DetermineReachabilities;
  using Merging::DetermineVelocityLimits;
  using Merging::DetermineVMax;
  using Merging::DrawNewAccelerationLimits;
};

/************************
 * CHECK DetermineVMax  *
 ************************/

// \brief Data table for definition of individual test cases
struct DataFor_DetermineVMax
{
  std::string description;
  units::velocity::meters_per_second_t vEgo;
  units::velocity::meters_per_second_t desiredVelocity;
  units::velocity::meters_per_second_t amountOfDesiredVelocityViolation;
  units::velocity::meters_per_second_t velocityLegalEgo;
  units::velocity::meters_per_second_t amountOfSpeedLimitViolation;
  units::velocity::meters_per_second_t velocityReason;
  units::velocity::meters_per_second_t expected_vMax;
};

class Merging_DetermineVMax : public ::testing::Test,
                              public ::testing::WithParamInterface<DataFor_DetermineVMax>
{
};

TEST_P(Merging_DetermineVMax, Merging_DetermineVMax)
{
  auto data = GetParam();

  NiceMock<FakeMentalModel> mentalModel;
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  NiceMock<FakeFeatureExtractor> featureExtractor;
  MergingUnderTest merging(mentalModel, surroundingVehicleQueryFactory, featureExtractor);

  DriverParameters driverParameters;
  driverParameters.desiredVelocity = data.desiredVelocity;
  driverParameters.amountOfDesiredVelocityViolation = data.amountOfDesiredVelocityViolation;
  driverParameters.amountOfSpeedLimitViolation = data.amountOfSpeedLimitViolation;
  ON_CALL(mentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(mentalModel, GetVelocityLegalEgo()).WillByDefault(Return(data.velocityLegalEgo));
  ON_CALL(mentalModel, DetermineVelocityReason()).WillByDefault(Return(data.velocityReason));

  auto result_vMax = merging.DetermineVMax(data.vEgo);
  ASSERT_EQ(result_vMax, data.expected_vMax);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    Merging_DetermineVMax,
    testing::Values(
        DataFor_DetermineVMax{
            .description = "Everything 0.0",
            .vEgo = 0.0_mps,
            .desiredVelocity = 0.0_mps,
            .amountOfDesiredVelocityViolation = 0.0_mps,
            .velocityLegalEgo = 0.0_mps,
            .amountOfSpeedLimitViolation = 0.0_mps,
            .velocityReason = 0.0_mps,
            .expected_vMax = 0.0_mps},
        DataFor_DetermineVMax{
            .description = "Variable Set 1",
            .vEgo = 1.0_mps,
            .desiredVelocity = 5.0_mps,
            .amountOfDesiredVelocityViolation = 2.0_mps,
            .velocityLegalEgo = 2.0_mps,
            .amountOfSpeedLimitViolation = 2.0_mps,
            .velocityReason = 10.0_mps,
            .expected_vMax = 6.0_mps},
        DataFor_DetermineVMax{
            .description = "Variable Set 2",
            .vEgo = 5.0_mps,
            .desiredVelocity = 4.0_mps,
            .amountOfDesiredVelocityViolation = 3.0_mps,
            .velocityLegalEgo = 2.0_mps,
            .amountOfSpeedLimitViolation = 1.0_mps,
            .velocityReason = 1.0_mps,
            .expected_vMax = 5.0_mps},
        DataFor_DetermineVMax{
            .description = "Variable Set 3",
            .vEgo = 1.0_mps,
            .desiredVelocity = 5.0_mps,
            .amountOfDesiredVelocityViolation = 3.0_mps,
            .velocityLegalEgo = 2.0_mps,
            .amountOfSpeedLimitViolation = 1.0_mps,
            .velocityReason = 2.0_mps,
            .expected_vMax = 2.0_mps}));

/**********************************
 * CHECK DetermineReachabilities  *
 **********************************/

TEST(Merging_DetermineReachabilities, CheckApproachingVelocityAndCountMergePhases)
{
  NiceMock<FakeMentalModel> mentalModel;

  NiceMock<FakeInfrastructureCharacteristics> infrastructureCharacteristics;
  ON_CALL(mentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructureCharacteristics));

  LaneInformationGeometrySCM laneInformation;
  laneInformation.laneType = scm::LaneType::Entry;
  ON_CALL(infrastructureCharacteristics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInformation));

  DriverParameters driverParameters;
  driverParameters.desiredVelocity = 0.0_mps;
  driverParameters.amountOfDesiredVelocityViolation = 0.0_mps;
  driverParameters.amountOfSpeedLimitViolation = 0.0_mps;
  driverParameters.comfortLongitudinalAcceleration = 1.0_mps_sq;  // results in scaled for merging -> 3.0
  driverParameters.comfortLongitudinalDeceleration = 1.0_mps_sq;  // results in scaled for merging -> 1.5
  ON_CALL(mentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(mentalModel, GetVelocityLegalEgo()).WillByDefault(Return(0.0_mps));
  ON_CALL(mentalModel, DetermineVelocityReason()).WillByDefault(Return(0.0_mps));
  ON_CALL(mentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(0.0_mps));
  ON_CALL(mentalModel, GetMeanVelocityLaneLeft()).WillByDefault(Return(0.0_mps));
  ON_CALL(mentalModel, GetDistanceToEndOfLane(0, _)).WillByDefault(Return(ScmDefinitions::INF_DISTANCE));

  Merge::EgoInfo egoInfo;
  egoInfo.velocity = 12.0_mps;
  ON_CALL(mentalModel, GetEgoInfo()).WillByDefault(Return(egoInfo));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  NiceMock<FakeFeatureExtractor> featureExtractor;
  MergingUnderTest merging(mentalModel, surroundingVehicleQueryFactory, featureExtractor);

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetUniformDistributed(3, 4)).WillByDefault(Return(3.0));
  merging.DrawNewAccelerationLimits(&fakeStochastics);

  Merge::Gap gap(nullptr, nullptr);
  gap.distance = 1.0_m;
  gap.velocity = 123.0_mps;
  gap.position = Merge::GapPosition::inFront;

  merging.DetermineReachabilities(gap);

  ASSERT_EQ(gap.regularMergePhases.size(), 2);
  ASSERT_NEAR(gap.approachingVelocity.value(), 76.1, 1e-1);
}